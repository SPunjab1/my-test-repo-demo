/*
*********************************************************************************************
@author  : IBM      
@date    : 09.06.19    
@description: This is the trigger for Work Order Task Dependency.          
@Version: 1.0

**********************************************************************************************/
trigger WorkOrderTaskDependencyTrigger on Work_Order_Task_Dependency__c (before insert,after insert,before update,after update,before delete, after delete,after undelete) {
    public static string triggerName ='WorkOrderTaskDependencyTrigger';   
    WorkOrderTaskDependencyTriggerHandler wtdHandlerObj = new WorkOrderTaskDependencyTriggerHandler();
    TriggerDispatcher.run(wtdHandlerObj );
}
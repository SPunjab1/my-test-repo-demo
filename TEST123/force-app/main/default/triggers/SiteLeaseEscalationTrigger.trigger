trigger SiteLeaseEscalationTrigger on Site_Lease_Escalation__c (before insert,after insert,before update,after update,before delete, after delete,after undelete) {
	SiteLeaseEscalationTriggerHandler sleObj = new SiteLeaseEscalationTriggerHandler();
    TriggerDispatcher.run(sleObj );		
}
trigger SI_ContractSiteTrigger on Contract_Site__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)  {
    public static string triggerName ='SI_ContractSiteTrigger';   
    SI_ContractSiteTriggerHandler objContractSiteHandler = new SI_ContractSiteTriggerHandler();
    TriggerDispatcher.run(objContractSiteHandler);
}
/*
*********************************************************************************************
@author  : IBM      
@date    : 22.08.19    
@description: This trigger for Worklog_Entry__c.          
@Version: 

**********************************************************************************************/
trigger WorklogEntryTrigger on Worklog_Entry__c(before insert,after insert,before update,after update,before delete, after delete,after undelete) {
	public static string triggerName ='WorklogEntryTrigger';   
    WorklogEntryTriggerHandler workLogHandlerObj = new WorklogEntryTriggerHandler();
    TriggerDispatcher.run(workLogHandlerObj);
}
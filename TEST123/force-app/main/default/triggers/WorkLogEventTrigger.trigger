/**********************************************************************************************
@author  : IBM
@date    : 22.08.19
@description: DataWarehouse sends the data in WorkLog event and this trigger copy the data into WorkLog entry Object
@Version:

**********************************************************************************************/
trigger WorkLogEventTrigger on WorkLog_Event__e (after insert) {
    WorkLogEventTriggerHandler.insertWorkLogEntry(Trigger.New);
}
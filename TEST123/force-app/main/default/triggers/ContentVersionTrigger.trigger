/**********************************************************************************************
@author  : IBM
@date    : 31.05.19
@description: This trigger for Cases.
@Version:

**********************************************************************************************/
trigger ContentVersionTrigger on ContentVersion (before insert,after insert,before update,after update,before delete, after delete,after undelete) {
    public static string triggerName ='ContentVersionTrigger';
    ContentVersionTriggerHandler ContentVersionHandlerObj = new ContentVersionTriggerHandler();
    TriggerDispatcher.run(ContentVersionHandlerObj );
}
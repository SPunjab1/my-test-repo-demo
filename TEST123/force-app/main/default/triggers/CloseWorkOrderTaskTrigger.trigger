trigger CloseWorkOrderTaskTrigger on Close_Work_Order__e (After insert) {

    CloseWorkOrderTaskTriggerHandler.closeWorkOrderTask(Trigger.new);
}
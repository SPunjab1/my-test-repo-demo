trigger releaseNote on Release_Note__c (after insert) {
    
    //add query for finding active community release note, loop through wanton conditions e.g. community notes only
    /*SELECT Id,Name, End_Date__c, Release_Notes__c, Release_Short_Description__c, Start_Date__c
FROM Release_Note__c where (Start_Date__c = today OR Start_Date__c < today) and POD_or_Community__c includes ('Community')
and (End_Date__C = today OR End_Date__C > today) order by createddate desc  LIMIT 1];

*/
    for(Release_Note__c r : trigger.new){
        if(r.POD_or_Community__c=='Community'){
            BatchPPDEnableReleasenotes releaseNotesbatch = new BatchPPDEnableReleasenotes(); 
            Id batchId = Database.executeBatch(releaseNotesbatch);
            system.debug(batchId);           
        }
    }
    
}
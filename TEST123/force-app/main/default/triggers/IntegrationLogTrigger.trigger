trigger IntegrationLogTrigger on Integration_Log__c (before insert,after insert,before update,after update,before delete, after delete,after undelete) {
 	public static string triggerName ='IntegrationLogTrigger';   
    IntegrationLogTriggerHandler integrationLogTriggerHandler = new IntegrationLogTriggerHandler();
    TriggerDispatcher.run(integrationLogTriggerHandler);
}
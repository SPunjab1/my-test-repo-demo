/**********************************************************************************************
@author  : Matthew Elias - copying IBM (ContentDocumentLinkTrigger)
@date    : 13.08.20
@description: Trigger for Savings Tracker
@Version:
**********************************************************************************************/
trigger SavingsTrackerTrigger on Savings_Tracker__c (after insert) {
	
    public static string triggerName ='SavingsTrackerTrigger';
    SavingsTrackerTriggerHandler SavingsTrackerHandlerObj = new SavingsTrackerTriggerHandler();
    TriggerDispatcher.run(SavingsTrackerHandlerObj);
    
}
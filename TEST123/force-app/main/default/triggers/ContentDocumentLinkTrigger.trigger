/**********************************************************************************************
@author  : IBM
@date    : 22.08.19
@description: This trigger for ContentDocument.
@Version:

**********************************************************************************************/
trigger ContentDocumentLinkTrigger on contentDocumentLink (before insert,after insert,before update,after update,before delete, after delete,after undelete) {
    
    public static string triggerName ='ContentDocumentLink';
    ContentDocumentLinkTriggerHandler ContentDocumentLinkHandlerObj = new ContentDocumentLinkTriggerHandler ();
    TriggerDispatcher.run(ContentDocumentLinkHandlerObj);
    
}
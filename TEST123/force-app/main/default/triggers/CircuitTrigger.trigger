/*
*********************************************************************************************
@author  : IBM      
@date    : 17.09.2020
@description: This trigger for Circuit.          
@Version: 

**********************************************************************************************/
trigger CircuitTrigger on Circuit__c(before insert,after insert,before update,after update,before delete, after delete,after undelete) {
    public static string triggerName ='CircuitTrigger';   
    CircuitTriggerHandler circuitHandlerObj = new CircuitTriggerHandler();
    TriggerDispatcher.run(circuitHandlerObj );
}
trigger OpportunityApprovalHistoryTrigger on Opportunity_Approval_History__c (before insert,before update) {
    
    public static string triggerName ='OpportunityApprovalHistoryTrigger';   
    OpportunityApprovalHistoryTriggerHandler oppHandlerObj = new OpportunityApprovalHistoryTriggerHandler();
    TriggerDispatcher.run(oppHandlerObj);

}
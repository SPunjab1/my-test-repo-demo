/*
*********************************************************************************************
@author  : IBM      
@date    : 31.05.19    
@description: This trigger for Cases.          
@Version: 

**********************************************************************************************/
trigger CaseTrigger on Case (before insert,after insert,before update,after update,before delete, after delete,after undelete) {
    public static string triggerName ='CaseTrigger';   
    CaseTriggerHandler caseHandlerObj = new CaseTriggerHandler();
    TriggerDispatcher.run(caseHandlerObj );
}
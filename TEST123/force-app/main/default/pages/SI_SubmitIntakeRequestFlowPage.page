<!--------------------------------------------------------------------------
@author      : IBM
@date        : 02.06.2020
@description : To update case fields and fire assignment rules without user
               intervention
@Version     : 1.0
---------------------------------------------------------------------------->
<apex:page controller="SI_SubmitIntakeRequest" showHeader="false" sidebar="false" lightningStylesheets="true" title="Submit Intake">
    <apex:slds /> 
    <!-- Static Resource For adding CSS on the Page -->
    <apex:stylesheet value="{!URLFOR($Resource.SI_SubmitIntakeRequestResources, '/SI_SubmitIntakeFlowPage.css')}"/>
    <!-- Hidden command link to fire updateCaseStatus method on pageload -->
    <apex:form style="display:none" rendered="{!IF(hasError = false, true,false)}">
        <apex:commandLink action="{!updateCaseStatus}" target="_top"
                          styleclass="link" reRender="{!$Component.errorBlock}"
                          oncomplete="redirectToCaseRecord('{!hasError}');">Click to update</apex:commandLink>
    </apex:form>
    <!-- To display loader -->
    <div id="load_scrl" class="loader" ></div>
    <!-- Error Block -->
    <apex:form id="errorBlock" styleClass="errorBlockForm slds-var-m-around_large" style="display:none">
        <div class="slds-border_bottom slds-border_top slds-border_left slds-border_right">
            <div class="errorBlock slds-var-m-around_medium" >
                <span>Review Errors</span> <br/>
            </div>
            <span class="errorMsg slds-var-m-left_x-large">{!errorMessage}</span>
            <div class="slds-align_absolute-center slds-var-m-around_small">
                <apex:commandButton styleClass="slds-button slds-button_brand" value="Back"
                                    action="{!backToCaseRecord}" rendered="{!$CurrentPage.parameters.id != ''}"/>
                <apex:commandButton styleClass="slds-button slds-button_brand" value="Back"
                                    onclick="parent.location.href = '{!$Site.BaseUrl}'"
                                    rendered="{!$CurrentPage.parameters.id == ''}"/>
            </div>
        </div>
    </apex:form> 
    <script>
    // action to be performed on page load
    window.onload = function() {
        //fetch hasError variable value
        const hasError = '{!hasError}';
        //if hasError is false, click hidden command link to update case
        //else hide loader and display error block
        if(hasError === 'false') {
            document.getElementsByClassName("link")[0].click();
        } else {
            document.getElementsByClassName("loader")[0].style.display = 'none';
            document.getElementsByClassName("errorBlockForm")[0].style.display = 'block';
        }
    }
    
    //function to redirect to case record
    function redirectToCaseRecord(hasError) {
        //if hasError is false, click hidden command link to update case
        //else hide loader and display error block
        if(hasError === 'false') {
            var caseId = '{!$CurrentPage.parameters.id}';
            //check if page is called from community
            if('{!$Site.BaseUrl}' != '') {
                parent.location.href = '{!$Site.BaseUrl}/{!$CurrentPage.parameters.id}';
            } else if((typeof sforce != 'undefined') && (sforce != null)) {
              	sforce.one.back(true);
            } else {   //if page is called from salesforce org
                window.location = '/{!$CurrentPage.parameters.id}';
            }
        } else {
            //in case of any errors, hide loader and display error block
            document.getElementsByClassName("loader")[0].style.display = 'none';
            document.getElementsByClassName("errorBlockForm")[0].style.display = 'block';
        }
    }
    </script>
</apex:page>
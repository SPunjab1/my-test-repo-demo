global class NLG_OpportunityTeamUpdateBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string query='Select id,Market_s__c, Region__c, Landlord__c,RecordType.Name,Negotiating_Vendor__c from Opportunity where RecordType.Name in (\'New Site Lease\',\'NLG No POR information\',\'Settlement and Utility\',\'Site AMD\')' ;
        
        return Database.getQueryLocator(query);
    }
    
    /*********************************************************************************************************
* @Description  : 
* @input params :Database.BatchableContext,List<Case>
**********************************************************************************************************/    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        Set<Id> setIds = new Set<Id>();
        for(Opportunity opp: scope){
            setIds.add(opp.Id);
        }
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([Select id,Market_s__c, 
                                                      Region__c, Landlord__c,
                                                      RecordType.Name,Negotiating_Vendor__c from 
                                                      Opportunity where 
                                                      Id in :setIds]);
        NLG_OpportunityBusinessProcess nlgBusProcess = new NLG_OpportunityBusinessProcess();
		nlgBusProcess.createOpportunityTeamMembers(oppMap);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

}
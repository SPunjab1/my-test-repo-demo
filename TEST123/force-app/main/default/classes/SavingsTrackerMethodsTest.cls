/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : SavingsTrackerMethodsTest
*  @Author           : Matthew Elias
*  @Version History  : 1.0
*  @Creation         : 12.08.20
*  @Description      : Test class for SavingsTrackerMethods
**********************************************************************************************************************
*********************************************************************************************************************/
@isTest(SeeAllData=false)
public class SavingsTrackerMethodsTest {
/***************************************************************************************************
* @Description : Test Method to test Evaluate & Update SavingsTrackerAttachments Methods
***************************************************************************************************/
    @isTest static void updateSavingsTrackerAttachmentTest(){
                
        // create Request records
        SavingsTrackerTestDataFactory.createRequest(10);
        List<Request__c> requestList = new List<Request__c>([SELECT Id FROM Request__c]);
        Set<Id> requestIDs = new Set<Id>();
        for(Request__c req : requestList){
            requestIDs.add(req.Id);
        }
        
        // create Savings Tracker records
        SavingsTrackerTestDataFactory.createSavingsTracker(requestIDs);
        
        // create Content Document records
        SavingsTrackerTestDataFactory.createContentDocuments(20);
        List<ContentDocument> CDList = new List<ContentDocument>([SELECT Id, Title FROM ContentDocument]);
        
        Integer requestSize = requestList.size();
        Integer CDSize = CDList.size();
        
        Test.startTest();
        
        // create CDLs
        Map<Id,Set<Id>> CDLMap = new Map<Id,Set<Id>>();
        Integer rand1 = 0;
        Integer rand2 = 0;
        Integer count = 0;
        do{
            rand1 = Integer.valueOf(Math.random() * (requestSize - 1));
            rand2 = Integer.valueOf(Math.random() * (CDSize - 1));
            if(!CDLMap.containsKey(requestList[rand1].Id)){
                CDLMap.put(requestList[rand1].Id, new Set<Id>{CDList[rand2].Id});
                count++;
            } else if(!CDLMap.get(requestList[rand1].Id).contains(CDList[rand2].Id)){
                CDLMap.get(requestList[rand1].Id).add(CDList[rand2].Id);
                count++;
            }
            
        } while(count < 30);
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        
        ContentNote testNote = new ContentNote();
        testNote.Title = 'Test Note';
        testNote.Content = Blob.valueOf('Test Note Content');
        insert testNote;
        
        cdlMap.clear();
        cdlMap.put(requestList[0].Id, new Set<Id>{testNote.Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(cdlMap);
        
        
        // evaluate current state
        Map<Id,Request__c> requestMap1 = new Map<Id,Request__c>([SELECT Id FROM Request__c]);
        //System.assertEquals(30,SavingsTrackerMethods.evaluateSpecific(requestMap1));
        System.assertEquals(0,SavingsTrackerMethods.evaluateSpecific(requestMap1));
        
        // run method
        Map<Id,Request__c> requestMap2 = new Map<Id,Request__c>([SELECT Id FROM Request__c LIMIT 5]);
        Integer miss1 = 0;
        Integer miss2 = 0;
        miss1 = SavingsTrackerMethods.updateSpecific(requestMap2);
        miss2 = SavingsTrackerMethods.updateAll();
        //System.assertEquals(30, miss1 + miss2);
        
		// evaluate current state
		System.assertEquals(0,SavingsTrackerMethods.evaluateAll());
        
        Test.stopTest();
    }
}
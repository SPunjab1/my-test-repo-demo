/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : CustomChevronViewControllerTest
*  @Author           : Archana
*  @Version History  : 1.0
*  @Creation         : 10.07.2019
*  @Description      : Test class for CustomChevronViewController
**********************************************************************************************************************
**********************************************************************************************************************/
@IsTest(SeeAllData=false)
Public class CustomChevronViewControllerTest{    
    
    /*************************************************************
    *@description : Test Method for getChevron data  scenario
    *************************************************************/   
     static testmethod void  testgetChevronData() {    
         System.runAs(TestDataFactory.createUser()){  
            List<Case> idlist=new List<Case>();
            List<Case> idlistClosed=new List<Case>();              
            Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
            List<Case> caseList= new List<Case>(); 
            List<Case> caseToUpdate=new List<Case>();       
            Integer NumOfCases=1;       
            String WorkflowTmpName1='AAV CIR Retest';       
            caseList= TestDataFactory.createCase(WorkflowTmpName1,NumOfCases,recTypeId); 
            Test.startTest();        
                Insert(caseList);                               
                idList=[SELECT id FROM Case LIMIT 1];       
                CustomChevronViewController.getChevronData(idList[0].id);
                idlistClosed=[SELECT id FROM Case WHERE Task_Number__c=1 LIMIT 1];  
                idlistClosed[0].status='Closed';
                Update idlistClosed;
                CustomChevronViewController.getChevronData(idlistClosed[0].id);     
                System.assertNotEquals(CustomChevronViewController.getChevronData(idList[0].id), Null);                       
            Test.stopTest();      
        }
    }  
 }
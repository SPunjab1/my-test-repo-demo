/*********************************************************************************************
@author     : Balaji
@date       : 2020.12.08
@description: This class is used to purge the Integration logs which are 6 months old.
@Version    : 1.0
**********************************************************************************************/

global class BatchIntegration_Log_Purge implements Database.Batchable<sObject>,Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {        
        String query;
        if(!Test.isRunningTest()){
             query = 'Select Id,Createddate,Task__r.status,Work_Order__c from Integration_Log__C where createddate < LAST_N_DAYS:'+Label.No_of_days;
        }else{
             query = 'Select Id,Createddate,Task__r.status,Work_Order__c from Integration_Log__C limit 10';
        }
        system.debug(query);
        return Database.getQueryLocator(query);
    }
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())Database.executeBatch(new BatchIntegration_Log_Purge());
    }
    global void execute(Database.BatchableContext BC, List<Integration_Log__C> scope) {
        system.debug('scope:'+scope);
        set<Id> caseids = New Set<Id>();
        List<Integration_Log__C> lstntegLogs = New List<Integration_Log__C>();
        String statusvalues = Label.Integ_Log_case_Status;
        System.debug('statusvalues:'+statusvalues);
        List<string> lststring = statusvalues.split(',');        
        for(Integration_Log__C templog: scope){
            if(templog.Work_Order__c != null){
                lstntegLogs.add(templog);
            }
            else{
                for(Integer i=0; i<lststring.size(); i++){
                    if(templog.Task__r.status == lststring[i]){
                        lstntegLogs.add(templog);  
                    }
                }
            }
        }
        delete(lstntegLogs);
        system.debug('lstntegLogs>>'+lstntegLogs);
    }
    global void finish(Database.BatchableContext BC) {
        
    }    
    
}
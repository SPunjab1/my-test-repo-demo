/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : PreviousTaskClosedBatch
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 02.08.2019
*  @Description      : Batch Class to identify first open work order task for migrated cases.
**********************************************************************************************************************
**********************************************************************************************************************/
global class PreviousTaskClosedBatch implements Database.Batchable<sObject>,Database.Stateful{
    /***************************************************************************************
* @Description  :Start Method of Batch Class to initialize count and return Case list
* @input params :Database.BatchableContext
***************************************************************************************/ 
global Set<Id> failureIdsSet = new Set<Id>();

    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        ID workOrderRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        String query = 'SELECT Id,(Select id,ParentId,Task_Number__c,Last_Task__c FROM Cases Order By Task_Number__c DESC limit 1)from case where recordTypeId=:workOrderRecordType and Migrated_Case__c =true';
        return Database.getQueryLocator(query);
    }
    
    /*********************************************************************************************************
* @Description  : Execute Method of Batch Class to update Provious closed task checkbox
* @input params :Database.BatchableContext,List<Case>
**********************************************************************************************************/    
    global void execute(Database.BatchableContext BC, List<Case> scope) {
        List<Case> updateTaskLst = new List<Case>();
        if(scope!=null && scope.size()>0){
            for(Case cs :scope){
                if(cs.cases.size()>0){
                    for(case task:cs.cases) {
                        if(task.Last_Task__c == False){
                            task.Last_Task__c =true;
                            updateTaskLst.add(task); 
                        }
                    }
                }      
            }
        }
        
        
        if(updateTaskLst!=null && updateTaskLst.size()>0)
        {
            Database.Saveresult[] srList=database.update (updateTaskLst,false);    
            for (Integer i=0;i<srList.size();i++) {
                if (!srList.get(i).isSuccess()){
                    // DML operation failed
                    /*Database.Error error = srList.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    updateTaskLst.get(i);//failed record from the list
                    system.debug('Failed ID'+updateTaskLst.get(i).Id);*/
                    failureIdsSet.add(updateTaskLst.get(i).Id);
                 }
           }


        }
    }
    /*********************************************************************************************************
* @Description  : Finish Method of Batch Class
* @input params : Database.BatchableContext
**********************************************************************************************************/ 
    global void finish(Database.BatchableContext BC) {
            AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'kirdesh1@in.ibm.com'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Previous Task Closed Status ' + a.Status);
            mail.setPlainTextBody('Records processed ' + a.TotalJobItems +'with '+ failureIdsSet.size()+ ' failures.'+ '\n List of failed records are:'+failureIdsSet);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
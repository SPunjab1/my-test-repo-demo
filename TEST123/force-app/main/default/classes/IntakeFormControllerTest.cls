/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : IntakeFormControllerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 09.06.2019
*  @Description      : Test class for IntakeFormController
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
Public class IntakeFormControllerTest
{
    
/*************************************************************************************
* @Description :Test Method for GetCustomPickListValues
*************************************************************************************/
    static testmethod void testGetCustomPickListValues() { 
        List<Intake_Request__C> intReqList = TestDataFactory.createIntakeRequest('TIFS New TDM_Site Install','No');
        insert intReqList; 
        Test.startTest();      
        List<String> options = IntakeFormController.getCustomPickListValues(intReqList[0],'Workflow_Template__c');
        Test.stopTest();
        System.assert(options.contains('TIFS AAV CIR Retest'),true);
    }
    /*************************************************************************************
* @Description :Test Method for GetLoactionDetails
*************************************************************************************/   
    static testmethod void testGetLoactionDetails() { 
        List<Location_id__C> locList = TestDataFactory.createLocationId('123','Test Location',1);
        insert locList;
        Case testCase = new Case();
        testCase.Status='New';
        testCase.Workflow_Template__c = 'TIFS New TDM_Site Install';
        testCase.Location_IDs__c =locList[0].id;
        insert testCase;
        Test.startTest();      
        Map<string,Location_id__C> newMap = IntakeFormController.getLocationDetails('1230','TIFS New TDM_Site Install');
        Test.stopTest();
      
    }
/*************************************************************************************
* @Description :Test Method for createSingleSelectIntakeRec
*************************************************************************************/    
    static testmethod void testcreateSingleSelectIntakeRec() { 
        List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS AAV CIR Retest','No');
        List<Location_id__C> locIdList = TestDataFactory.createLocationId('123','Test Location',1);
        List<Switch_ID__c> switchIdList = TestDataFactory.createSwitchIds('123','Test Location',1);
        insert intReqList; 
        insert locIdList;
        insert switchIdList;
        Case testCase = new Case();
        testCase.Status='New';
        testCase.Workflow_Template__c = 'TIFS New TDM_Site Install';
        testCase.Location_IDs__c =locIdList[0].id;
        insert testCase;
        
        List<Case> CaseTemp = [Select id,CaseNumber from Case Where Id =:testCase.id];
        Test.startTest(); 
       
        String inatekId = IntakeFormController.createSingleSelectIntakeRec(intReqList[0],locIdList[0],switchIdList[0],CaseTemp[0].CaseNumber);
        Test.stopTest();
        System.assertNotEquals(inatekId, null);
    }
/*************************************************************************************
* @Description :Test Method for createSingleSelectIntakeRec
*************************************************************************************/    
    static testmethod void testcreateSingleSelectIntakeRec1() { 
        List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS AAV CIR Retest','No');
        List<Location_id__C> locIdList = TestDataFactory.createLocationId('123','Test Location',1);
        insert intReqList; 
        insert locIdList;
        Case testCase = new Case();
        testCase.Status='New';
        testCase.Workflow_Template__c = 'TIFS New TDM_Site Install';
        testCase.Location_IDs__c =locIdList[0].id;
        insert testCase;
        
        List<Case> CaseTemp = [Select id,CaseNumber from Case Where Id =:testCase.id];
        Test.startTest();
        String inatekId = IntakeFormController.createSingleSelectIntakeRec(intReqList[0],locIdList[0],null,CaseTemp[0].CaseNumber);
        Test.stopTest();
        System.assertNotEquals(inatekId, null);
    }
    /*************************************************************************************
* @Description :Test Method for ValidateLocationLst
*************************************************************************************/    
    static testmethod void testValidateLocationLst() { 
        List<Location_id__c> locIdLst =new List<Location_id__c>();
        List<String> locIdListBlank=new List<String>();
        locIdListBlank.add('');
        List<String> locIdListBlk=new List<String>();
        locIdListBlk.add('BLANK');
        List<Location_id__c> locIdList1 = TestDataFactory.createLocationId('123','Test Location1',200);
        locIdLst.add(locIdList1[0]);
        List<Location_id__c> locIdList2 = TestDataFactory.createLocationId('234','Test Location2',200);
        locIdLst.add(locIdList2[0]);
        insert locIdLst;
        List<String> locIdList=new List<String>();
        locIdList.add('123');
        locIdList.add('234');
        List<String> secLocIdList=new List<String>();
        secLocIdList.add('123');
        List<String> locIdTempLst =new List<String>{'123|OM Dark Fiber & Small Cell','234|TIFS New TDM_Site Install'};
            Test.startTest();
        Map<String,Map<String,Location_ID__c>> newMap1 = IntakeFormController.validateLocationLst(locIdListBlank,secLocIdList,locIdTempLst);
        Map<String,Map<String,Location_ID__c>> newMap2 = IntakeFormController.validateLocationLst(locIdListBlk,secLocIdList,locIdTempLst);
        Map<String,Map<String,Location_ID__c>> newMap = IntakeFormController.validateLocationLst(locIdList,secLocIdList,locIdTempLst);
        Test.stopTest();
        System.assertEquals(newMap.size()>0, true);
    }
    
                
        static testmethod void testGetTpeReuestNumber() {   
            List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS New TDM_Site Install','Yes');  
            insert intReqList;  
            List<String> locIdLstBlank =new List<String>(); 
            locIdLstBlank.add('');  
            List<Location_id__c> locIdList1 = TestDataFactory.createLocationId('123','Test Location',50);   
            insert locIdList1;  
            List<Switch_ID__c> switchIdList = TestDataFactory.createSwitchIds('123','123',50);  
            insert switchIdList;    
            Case testCase = new Case(); 
            testCase.Status='New';  
            testCase.Workflow_Template__c = 'TIFS New TDM_Site Install'; 
            testCase.Location_IDs__c =locIdList1[0].id; 
            insert testCase;    
                
            List<Case> CaseTemp = [Select id,CaseNumber from Case Where Id =:testCase.id];  
                
            Map<String,Map<String,String>> fileContentMap = new Map<String,Map<String,String>>();   
            for ( Integer i = 0 ; i < 40 ; i++ ) {      
                Map<String,String> newMap =new Map<String,String>();    
                newMap.put(label.CSVCol_TemplateName,'TIFS AAV CIR Retest'); 
                newMap.put(label.CSVCol_CRUserDescription,'Test Project Description');  
                newMap.put(label.CSVCol_ProjectName,'Test Project Name');   
                newMap.put(label.CSVCol_CRCount,'1');   
                newMap.put(label.CSVCol_SecondaryLocationID,'');    
                newMap.put(label.CSVCol_AdvanceRANTransmission,'true'); 
                newMap.put(label.CSVCol_EnterpriseTransmissionDesign,'true');   
                fileContentMap.put('123'+ i +'|TIFS AAV CIR Retest',newMap); 
            }   
            Test.startTest();   
                Map<String,String> mapVall = IntakeFormController.getTpeRequestNumberMap(fileContentMap,CaseTemp[0].CaseNumber);    
                Case CaseTempNum = IntakeFormController.getTPEReqDetails(CaseTemp[0].CaseNumber);   
                List<Case>CaseTempNumList = IntakeFormController.fetchTPERequestNumber(CaseTemp[0].CaseNumber); 
            Test.stopTest();    
        }
/*************************************************************************************
* @Description :Test Method for CreateMultiSelectIntakeRec
*************************************************************************************/      
    static testmethod void testCreateMultiSelectIntakeRec() {  
        List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS New TDM_Site Install','Yes');
        insert intReqList;
        List<String> locIdLstBlank =new List<String>();
        locIdLstBlank.add('');
        List<Location_id__c> locIdList1 = TestDataFactory.createLocationId('123','Test Location',50);
        insert locIdList1;
        List<Switch_ID__c> switchIdList = TestDataFactory.createSwitchIds('123','123',50);
        insert switchIdList;
        Case testCase = new Case(); 
            testCase.Status='New';  
            testCase.Workflow_Template__c = 'TIFS New TDM_Site Install'; 
            testCase.Location_IDs__c =locIdList1[0].id; 
            testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Work_Order').getRecordTypeId();    
            insert testCase;    
                
            Case CaseTemp = [Select id,CaseNumber from Case Where Id =:testCase.id];
        Map<String,Map<String,String>> fileContentMap = new Map<String,Map<String,String>>();
        for ( Integer i = 0 ; i < 40 ; i++ ) {   
            Map<String,String> newMap =new Map<String,String>();
            newMap.put(label.CSVCol_TemplateName,'TIFS AAV CIR Retest');
            newMap.put(label.CSVCol_CRUserDescription,'Test Project Description');
            newMap.put(label.CSVCol_ProjectName,'Test Project Name');
            newMap.put(label.CSVCol_CRCount,'1');
            newMap.put(label.CSVCol_SecondaryLocationID,'');
            newMap.put(label.CSVCol_AdvanceRANTransmission,'true');
            newMap.put(label.CSVCol_EnterpriseTransmissionDesign,'true');
            newMap.put(label.CSVCol_TPERequestNumber,CaseTemp.CaseNumber);
            fileContentMap.put('123'+ i +'|TIFS AAV CIR Retest',newMap);
        }
        Test.startTest(); 
        Map<String,String> mapVal = IntakeFormController.createMultiSelectIntakeRec(intReqList[0],locIdList1,null,fileContentMap,null,switchIdList,CaseTemp.CaseNumber);
        Map<String,String> mapVal1 = IntakeFormController.createMultiSelectIntakeRec(intReqList[0],locIdList1,null,fileContentMap,locIdLstBlank,switchIdList,CaseTemp.CaseNumber);
        Test.stopTest();
        //System.assertNotEquals(mapVal, null);
    }
/*************************************************************************************
* @Description :Test Method for CreateMultiSelectIntakeRec
*************************************************************************************/      
    static testmethod void testCreateMultiSelectIntakeRec1() {  
        List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS AAV CIR Retest','Yes');
        insert intReqList;
        List<Location_id__c> locIdList1 = TestDataFactory.createLocationId('123','Test Location',50);
        insert locIdList1;
        List<Switch_ID__c> switchIdList = TestDataFactory.createSwitchIds('123','123',200);
        insert switchIdList;
        Case testCase = new Case(); 
            testCase.Status='New';  
            testCase.Workflow_Template__c = 'TIFS New TDM_Site Install'; 
            testCase.Location_IDs__c =locIdList1[0].id; 
            testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Work_Order').getRecordTypeId();    
            insert testCase;    
                
            Case CaseTemp = [Select id,CaseNumber from Case Where Id =:testCase.id];
        Map<String,Map<String,String>> fileContentMap = new Map<String,Map<String,String>>();
        for ( Integer i = 0 ; i < 40 ; i++ ) {   
            Map<String,String> newMap =new Map<String,String>();
            newMap.put(label.CSVCol_TemplateName,'TIFS AAV CIR Retest');
            newMap.put(label.CSVCol_CRUserDescription,'Test Project Description');
            newMap.put(label.CSVCol_ProjectName,'Test Project Name');
            newMap.put(label.CSVCol_CRCount,'1');
            newMap.put(label.CSVCol_SecondaryLocationID,'');
            newMap.put(label.CSVCol_AdvanceRANTransmission,'true');
            newMap.put(label.CSVCol_EnterpriseTransmissionDesign,'true');
            newMap.put(label.CSVCol_TPERequestNumber,CaseTemp.CaseNumber);
            fileContentMap.put('123'+ i +'|TIFS AAV CIR Retest',newMap);
        }
        Map<String,String> newMap1 =new Map<String,String>();
        newMap1.put('TemplateName','TIFS AAV CIR Retest');
        newMap1.put('CRUserDescription','Test Project Description');
        newMap1.put('ProjectName','Test Project Name');
        newMap1.put('CRCount','1');
        newMap1.put('SecondaryLocationID','1231');
        fileContentMap.put('BLANK201|TIFS AAV CIR Retest',newMap1);
        Test.startTest(); 
        Map<String,String> mapVal = IntakeFormController.createMultiSelectIntakeRec(intReqList[0],locIdList1,null,fileContentMap,null,switchIdList,CaseTemp.caseNumber);
        Test.stopTest();
       // System.assertNotEquals(mapVal, null);
    }
    
    /*************************************************************************************
* @Description :Test Method for saveTheFile
*************************************************************************************/ 
    static testmethod void testsaveTheFile(){
        Case testCase = new Case();
        testCase.Status='New';
        testCase.origin='Web';
        insert testCase;
        test.startTest();
        Id fileid=IntakeformController.saveTheFile(testCase.id,'Test file','File Body','File Content');
        Id id=IntakeformController.saveChunk(testCase.id,'Test file','File Body','File Content',fileid);
        Id id1=IntakeformController.saveChunk(testCase.id,'Test file','File Body','File Content','');
        test.stopTest();
        System.assertNotEquals(fileid, null);
        System.assertNotEquals(id, null);
    }
    
/*************************************************************************************
* @Description :Test Method for CreateMultiSelectIntakeRecWithBlankLocId
*************************************************************************************/      
    static testmethod void testCreateMultiSelectIntakeRecWithBlankLocId() {  
        List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS New TDM_Site Install','Yes');
        insert intReqList;
        List<String> locIdLstBlank =new List<String>();
        locIdLstBlank.add('BLANK1');
        List<Location_id__c> locIdLst =new List<Location_id__c>();
        List<Location_id__c> locIdList1 = TestDataFactory.createLocationId('123','Test Location1',200);
        locIdLst.add(locIdList1[0]);
        List<Location_id__c> locIdList2 = TestDataFactory.createLocationId('234','Test Location2',200);
        locIdLst.add(locIdList2[0]);
        insert locIdLst;
        List<Switch_ID__c> switchIdList = TestDataFactory.createSwitchIds('123','123',50);
        insert switchIdList;
        Case testCase = new Case(); 
        testCase.Status='New';  
        testCase.Workflow_Template__c = 'TIFS New TDM_Site Install'; 
        testCase.Location_IDs__c =locIdLst[0].id;   
        insert testCase;    
            
        List<Case> CaseTemp = [Select id,CaseNumber from Case Where Id =:testCase.id];
        Map<String,Map<String,String>> fileContentMap = new Map<String,Map<String,String>>();
        Map<String,String> newMap =new Map<String,String>();
        newMap.put(label.CSVCol_TemplateName,'TIFS AAV CIR Retest');
        newMap.put(label.CSVCol_CRUserDescription,'Test Project Description');
        newMap.put(label.CSVCol_ProjectName,'Test Project Name');
        newMap.put(label.CSVCol_CRCount,'1');
        newMap.put(label.CSVCol_SecondaryLocationID,'');
        newMap.put(label.CSVCol_AdvanceRANTransmission,'true');
        newMap.put(label.CSVCol_EnterpriseTransmissionDesign,'true');
        fileContentMap.put('BLANK1|TIFS AAV CIR Retest',newMap);
        Test.startTest(); 
        Map<String,String> mapVal = IntakeFormController.createMultiSelectIntakeRec(intReqList[0],locIdLst,null,fileContentMap,locIdLstBlank,switchIdList,CaseTemp[0].CaseNumber);
        Test.stopTest();
    }     
/*************************************************************************************
* @Description :Test Method to validate switchId
*************************************************************************************/      
    static testmethod void testValidateSwitchId() {  
        List<String> switchIds= new List<String>();
        List<Switch_ID__c> switchIdList = TestDataFactory.createSwitchIds('809','Test Location',1);
        String switchId='8090';
        test.startTest();
        insert switchIdList; 
        switchIds.add('234');
        switchIds.add('8090');
        IntakeFormController.getSwitchDetails(switchId);
        IntakeFormController.getSwitchDetails('');
        IntakeFormController.validateSwitchIdLst(switchIds);
        test.stopTest();       
    }
    
    /*************************************************************************************
* @Description :Test Method for checkLocations
*************************************************************************************/      
    static testmethod void testcheckLocations() {  
        List<Intake_Request__c> intReqList= TestDataFactory.createIntakeRequest('TIFS New TDM_Site Install','Yes');
        insert intReqList;
        List<String> locIdLstBlank =new List<String>();
        locIdLstBlank.add('BLANK1');
        List<Location_id__c> locIdLst =new List<Location_id__c>();
        List<Location_id__c> locIdList1 = TestDataFactory.createLocationId('123','Test Location1',200);
        locIdLst.add(locIdList1[0]);
        insert locIdLst;
        Case testCase = new Case(); 
        testCase.Status='New';  
        testCase.Workflow_Template__c = 'TIFS New TDM_Site Install'; 
        testCase.Location_IDs__c =locIdLst[0].id;   
        insert testCase;
        List<String> locationids = new list<String>();
        for(Location_id__c lid:locIdLst){
            locationids.add(lid.id);
        }
        List<String> templatename = New List<String>{'TIFS New TDM_Site Install'};
        //List<Case> CaseTemp = [Select id,CaseNumber,Workflow_Template__c,Status from Case Where Id =:testCase.id];
        Map<String,Case> newMap =new Map<String,Case>();
        Test.startTest(); 
        Map<String,Case> mapVal = IntakeFormController.checkLocations(templatename,locationids);
        Test.stopTest();
    }
}
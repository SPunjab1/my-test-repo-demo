/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : createIntegrationBatchTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 20.08.2019
*  @Description      : Test class for createIntegrationBatchTest
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest
public class createIntegrationBatchTest {
    static testMethod void testMethod1() 
    {
        List<Case> csList = new List<Case>();
        List<Case> taskLst = new List<Case>();
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();   
        String WorkflowTmpName ='TIFS AAV CIR Change';
        Integer NumOfCases=1;
        csList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        cslist[0].Migrated_Case__c=true;
        insert cslist;
        list <case>cList=[select id ,status from case where parentid in:cslist];
        if(cList !=null && clist.size()>0){
        cList[0].status='closed';
        cList[0].Attachment_Required__c=true; 
        cList[0].Migrated_Case__c=true;
        }
        Blob b = Blob.valueOf('Test Data');
        ContentVersion  cv = new ContentVersion();
        cv.Title = 'Test Attachment for Parent';
        cv.PathOnClient ='test';
        cv.VersionData  = b;
        insert(cv);
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl=new ContentDocumentLink();
        
        if(clist.size()>0){
        cdl.linkedEntityId=clist[0].id;
        }
        if(documents !=null && documents.size()>0){
        cdl.ContentDocumentId=documents[0].id;
        }
        cdl.ShareType = 'V';
        insert cdl;
        update cList;
        Test.startTest();
        PierIntegrationMock fakeResponse = new PierIntegrationMock(200,'{"transactionId":"8f96e746-7b40-4ba8-a31a-f7d394b866b7","invokingUser":"SPunjab1","timestamp":"2020-07-13T19:25:22.144Z","data":[{"id":157987,"templateId":1314,"description":"Test","longDescription":"Testing ignore","status":"active","manager":"","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:20.86834497Z","closedAt":null,"sourceSystem":"","sourceRecordId":"","tasks":{"0":{"data":{"id":873597,"workflow_id":157987,"name":"","description":"Verify Port Speed / Router Swap Complete","displayOrder":1,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.158974347Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":14,"timestamp":"2020-07-13 19:25:21.157605355 +0000 UTC","transactionId":""},"1":{"data":{"id":873598,"workflow_id":157987,"name":"","description":"Execute RFC2544, log results in ATOMs","displayOrder":2,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.276423755Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":22,"timestamp":"2020-07-13 19:25:21.275555477 +0000 UTC","transactionId":""},"2":{"data":{"id":873599,"workflow_id":157987,"name":"","description":"Execute BURST tests, log results in ATOMs, Activate Site","displayOrder":3,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.403841799Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":40,"timestamp":"2020-07-13 19:25:21.401872845 +0000 UTC","transactionId":""},"3":{"data":{"id":873600,"workflow_id":157987,"name":"","description":"Generate/Execute Script","displayOrder":4,"taskType":"CR Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.556928581Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":20,"timestamp":"2020-07-13 19:25:21.54619578 +0000 UTC","transactionId":""}}}],"version":null,"duration":1482}');
        List<Id> integrationLogIds = new List<Id>();

        Test.setMock(HttpCalloutMock.class, fakeResponse);
        PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(integrationLogIds,'CreateWorkflow');
        ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        createIntegrationBatch obj = new createIntegrationBatch();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
}
/*
*********************************************************************************************
@author  :  Mallesh Miryala     
@date    :  12/10/2020 
@Name    : NLG_OnBoardingFlowTest
@description: This is the test class for NLG_OnBoardingFlow    
@Version: 

**********************************************************************************************/
@istest
private class NLG_OnBoardingFlowTest {
    @testSetup
    static void testDataSetup() {
    	Contact cntObj = new Contact(
        	FirstName = 'TestFirstName',
            LastName  = 'LastName',
            Email 	  = 'test@gmail.com'
        );	
        insert cntObj; 
        
        Account actObj = new Account(
        	Name = system.label.NLG_CommunityAccountName
        );
        insert actObj;
    }
    
    @istest static void populateOpportunityTeamTest(){
        Test.startTest();
        NLG_OnBoardingFlow.CommunityUserParameters commObj = new NLG_OnBoardingFlow.CommunityUserParameters();
        commObj.firstName 	= 'fName';
        commObj.lastName  	= 'lName';
        commObj.email		= 'tflname@gmail.com';
        commObj.CommunityRole 	= 'Submitter';
        commObj.CommunityUserMarket  	= 'Anchorage';
        commObj.NetWorkId		= 'flname1234';
        commObj.MLA 			= 'ATC';
        commObj.UserName  		= 'flname@gmail.com.'+Math.round((Math.random()*(900000) + 100000));
        try{
        	NLG_OnBoardingFlow.createCommunityUser(new list<NLG_OnBoardingFlow.CommunityUserParameters>{commObj});            
        }catch(Exception Ex){}
        commObj.firstName 	= 'TestFirstName';
        commObj.lastName  	= 'LastName';
        commObj.email		= 'test@gmail.com';
        commObj.CommunityRole 	= 'Market';
        commObj.UserName  		= 'flname@gmail.com.'+DateTime.now().millisecond();
        try{
        	NLG_OnBoardingFlow.createCommunityUser(new list<NLG_OnBoardingFlow.CommunityUserParameters>{commObj});
        }catch(Exception Ex){}
        test.stopTest();
        system.assert([Select Id FROM User WHERE email = 'tflname@gmail.com'].Size() == 1);
        
    }
}
/*********************************************************************************************
@author     : IBM
@date       : July 13 2020
@description: Scheduled class To execute the batch every 30 Seconds 
@Version    : 1.0           
**********************************************************************************************/
public class SchedularForNeustarLogin implements Schedulable{
    public void execute(SchedulableContext sc) {
        BatchLoginToNeustarAPI neustarLoginBatch = new BatchLoginToNeustarAPI();
        integer interval = Integer.valueOf(Label.NeustarLoginBatchInterval);
        for(integer i=0;i<60;i=i+interval){
        string cron = '0 '+ i + ' * * * ?';
        string schName = 'Neustar Login Batch --'+i+'---Min--';
        if(!Test.isRunningTest()) system.schedule(schName, cron, neustarLoginBatch);
        }
    }   
}
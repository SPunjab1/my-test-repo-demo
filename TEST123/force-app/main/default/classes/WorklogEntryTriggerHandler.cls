/*********************************************************************************************
@author     : IBM
@date       : 25.08.19 
@description: Handler for WorkLogTrigger 
@Version    : 1.0           
********************************************************************************************/
public with sharing class WorklogEntryTriggerHandler extends TriggerHandler {
    private static final string WORKLOG_API = 'WorklogEntryTrigger';
    public static ID workOrderRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
    public static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
    public WorklogEntryTriggerHandler() {
        super(WORKLOG_API);
    }
    
/***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/
    public override String getName() {
        return WORKLOG_API;
    }
/***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems) {
    }
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
    }
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
        /*******Method call to create Integartion Logs for Task Worklog**************/
        createIntegrationLogs(newItems,oldItemsMap);
    }
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        /*******Method call to create Integartion Logs for Task Worklog**************/
        createIntegrationLogs(newItems,newItemsMap);
         /**Update Comments Available Checkbox to true on Task once comment is added */
        //updateCommentsAvailableOnTask(newItems);
    }
/*********************************************************************************************
* @Description :Method create Integartion Logs for Task Worklog
* @input params : Trigger.New,newItemsMap
**********************************************************************************************/
    public void createIntegrationLogs(List<Sobject> newlogList,Map<Id,Sobject> newItemsMap){
        List<Worklog_Entry__c> newItems= (List<Worklog_Entry__c>) newlogList;
        Map<Id,Worklog_Entry__c> workLogMap = (Map<Id,Worklog_Entry__c>) newItemsMap;
        List<Integration_Log__c> integrationLogs = new List<Integration_Log__c>();
        List<Worklog_Entry__c> updateLogLst =new List<Worklog_Entry__c>();
        Set<String> nonPierTemplateSet = new Set<String>();
        Map<String,Case> taskIdToTaskMap = new Map<String,Case>();
        if(Label.Non_Pier_Templates != Null){
            String[] templateLst = Label.Non_Pier_Templates.Split(',');
            nonPierTemplateSet.addAll(templateLst);
        }
        if(workLogMap!=null && workLogMap.size()>0){
            List<String> taskIdLst =new List<String>();
            for(Worklog_Entry__c worklogObj : newItems){
                taskIdLst.add(worklogObj.Task__c);   
            }
            List<Case> taskLst =[select id,recordtypeId,Executable_Task_in_PIER__c,Migrated_Case__c from Case where Id IN :taskIdLst]; 
            for(Case cs : taskLst){
                taskIdToTaskMap.put(cs.Id,cs);   
            }
        }
        if(taskIdToTaskMap!=null && taskIdToTaskMap.size()>0){
            for(Worklog_Entry__c worklogObj : newItems){
                if(Trigger.isInsert || (Trigger.isUpdate && workLogMap.get(worklogObj.Id).Comment__c != worklogObj.Comment__c)) {
                    Worklog_Entry__c worklog = new Worklog_Entry__c();
                    worklog.Id =  worklogObj.Id;
                    if(worklogObj.Comment__c !=null && worklogObj.Comment__c.contains('<img') && worklogObj.Comment__c.contains('img>')){
                         worklog.Image__c = true;
                    }
                    else{
                         worklog.Image__c = false;    
                    }
                    updateLogLst.add(worklog);
                }
                if(taskIdToTaskMap.get(worklogObj.Task__c).recordtypeId == workOrderTaskRecordType &&
                   (taskIdToTaskMap.get(worklogObj.Task__c).Executable_Task_in_PIER__c == true ||
                    taskIdToTaskMap.get(worklogObj.Task__c).Migrated_Case__c == true) && UserInfo.GetName()!= ConstantsUtility.AUTOMATED_PROCESS && 
                   (Trigger.isInsert || (Trigger.isUpdate && workLogMap.get(worklogObj.Id).Comment__c != worklogObj.Comment__c))){
                       Integration_Log__c log = new Integration_Log__c();
                       log.Task__c = worklogObj.Task__c;
                       log.API_Type__c = 'Task Worklog';
                       log.WorkLog_Id__c = worklogObj.Id;
                       integrationLogs.add(log);
                  }
            }  
        }
        try{
            if(updateLogLst!= Null && updateLogLst.size() > 0){
                database.update(updateLogLst ,true);
            }
            if(integrationLogs != Null && integrationLogs.size() > 0){
                database.insert(integrationLogs ,true);
            }
        }catch(exception ex){throw ex;}
    }

/***************************************************************************************
@Description : Method to Update Comments Available Checkbox to true on Task once comment is added 
****************************************************************************************/        
   /* public void updateCommentsAvailableOnTask(List<Sobject>newItems){
 
        List<Worklog_Entry__c>newCommentList=new List<Worklog_Entry__c>();
        newCommentList = (List<Worklog_Entry__c>) newItems;
        List<case> caseList=new list<case>();
        Set<Id> parentIds=new Set<Id>();
        
        for(Worklog_Entry__c ccObj:newCommentList){
            if(ccObj.Comment__c!=NULL && ccObj.Comment__c!='')
            parentIds.add(ccObj.task__c);
        }

        if(parentIds!=null && parentIds.size()>0 ){
            for(Case cs:[SELECT id ,Comments_Available__c FROM Case WHERE Id IN: parentIds AND recordTypeId=:workOrderTaskRecordType AND Comments_Available__c!=true]){              
                    cs.Comments_Available__c=true;
                    caseList.add(cs);
                }   
        }
        try{
            if(caseList!=null && caseList.size()>0){
                database.update(caseList,true);
            }
        }catch(exception ex){throw ex;}    
    }   */
}
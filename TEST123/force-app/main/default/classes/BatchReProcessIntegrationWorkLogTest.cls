/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : BatchReProcessIntegrationWorkLogTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 22.07.2019
*  @Description      : Test class for BatchReProcessIntegrationWorkLog
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchReProcessIntegrationWorkLogTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testMethod void TestMethod1(){
        List<Integration_Log__c> integrationLogsToInsert = new List<Integration_Log__c>();
        list<case>caseList =new List<case>();
         Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        caseList=TestDataFactory.createCase('AAV CIR Retest',1,recTypeId);
        insert caseList;
        list<case>taskList=new list<case>();
        taskList=[select id,status from case where parentid =:caseList[0].id];
        for(Integer i=0; i<5; i++){
            Integration_Log__c integrationLog = new Integration_Log__c();
            integrationLog.Status__c = 'Failure';
            integrationLog.API_Type__c='DWH WorkLog';
            integrationLog.Worklog_Description__c='TestDesc1';
            integrationLog.Task__c=taskList[0].id;
            integrationLogsToInsert.add(integrationLog);
        }
        Test.startTest();
        insert  integrationLogsToInsert;
        Worklog_entry__c wl=new worklog_entry__c();
        wl.task__c=taskList[0].id;
        wl.comment__c='TestDesc';
        insert wl;
        BatchReProcessIntegrationWorkLog batch = new BatchReProcessIntegrationWorkLog();
            Database.executeBatch(batch);
            //system.schedule('Jobs', CRON_EXP, batch);
        Test.stopTest();
    }
 }
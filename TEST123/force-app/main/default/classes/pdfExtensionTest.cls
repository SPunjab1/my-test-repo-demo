/*********************************************************************************************
@Class        : pdfExtensionTest
@author       : Pritesh
@date         : 11.20.2020 
@description  : This class is test class for pdfExtension
@Version      : 1.0           
**********************************************************************************************/
@isTest
public class pdfExtensionTest {
    
    @testSetup
    static void testDataSetup() {
        ID nlg_New_Site_Lease_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Site Lease').getRecordTypeId();
        ID nlg_NLG_No_POR_information_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('NLG No POR information').getRecordTypeId();
        ID nlg_Settlement_and_Utility_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Settlement and Utility').getRecordTypeId();
        ID nlg_Site_AMD_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Site AMD').getRecordTypeId();
        
        List<Opportunity> oppInsertList = new List<Opportunity>();
        Opportunity nlgnewIntake= new Opportunity();
        nlgnewIntake.RecordTypeId = nlg_New_Site_Lease_RecordType;
        nlgnewIntake.Name ='nlgnewIntake1';
        nlgnewIntake.StageName ='New';
        nlgnewIntake.Market_s__c ='WASHINGTON DC';
        nlgnewIntake.Region__c ='South';
        nlgnewIntake.Landlord__c ='Private LL';
        nlgnewIntake.CloseDate = System.today()+100;
        nlgnewIntake.Lease_or_AMD__c = 'AMD';
        nlgnewIntake.Total_Rent_Over_Initial_Term_Current__c=10000;
        nlgnewIntake.New_One_Time_Fees__c =10000;
        insert nlgnewIntake;
        
        Opportunity_Approval_History__c oppApprovalObj = new Opportunity_Approval_History__c (Opportunity__c = nlgnewIntake.Id, 
                                                                                              Action__c ='Approve', 
                                                                                              Approval_Stage__c = 'Regional Review',
                                                                                              Current_Approver__c=Userinfo.getUserId()
                                                                                             );
        
        
        insert oppApprovalObj;
        
    }
    
    static testmethod void testExtension() {   		
        Test.StartTest(); 
        
        Opportunity oppObj =[SELECT Id from Opportunity LIMIT 1];
        ApexPages.currentPage().getParameters().put('Id',String.valueOf(oppObj.Id));
        ApexPages.currentPage().getParameters().put('rrapp','test');
        ApexPages.currentPage().getParameters().put('rent','1000');
        ApexPages.currentPage().getParameters().put('sign','Test User');
        
        ApexPages.StandardController opp = new ApexPages.StandardController(oppObj);
        pdfExtension obj = new pdfExtension(opp);
        obj.saveFile();
        List <NLG_Util.signingWrapper> signingList =  obj.getFinalSigningList();
        system.assert( NULL != signingList);
            
            Test.StopTest();
    }
    
}
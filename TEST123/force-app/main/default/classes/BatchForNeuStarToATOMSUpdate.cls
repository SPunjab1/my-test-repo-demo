/*********************************************************************************************
@author     : IBM
@date       : July 8 2020
@description: Batch class to update ASR & Circuit in Salesforce and Call ATOMS API
@Version    : 1.0             
**********************************************************************************************/
public class BatchForNeuStarToATOMSUpdate implements Database.Batchable<sObject>, Database.Stateful,Schedulable, Database.AllowsCallouts{ 

    //public List<ASR__c> successASRList = new List<ASR__c>();
    //public List<Circuit__c> successCktList = new List<Circuit__c>();
    public Map<Id,List<ASR__c>> caseIdASRListMap = new Map<Id,List<ASR__c>>();
    public Map<Id,List<Circuit__c>> caseIdCktListMap = new Map<Id,List<Circuit__c>>();
    public Boolean activateATOMSAPICall = true;
    


    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        //Query Integration Logs that have ASR and PON and created during Reception of NeuStar messages in Salesforce
        String query = 'SELECT Id,Type_of_Order__c,API_Type__c,Purchase_Ord_PON__c,Purchase_Order__c, From__c,TP_Response_Type__c,Reprocess__c, To__c,PON__c,Pending_Salesforce_Update__c, Pending_ATOMS_Update__c,Request__c,Response__c,Status__c, ASR__c, Circuit__c, Work_Order__c from Integration_Log__c where Pending_ATOMS_Update__c = true AND Pending_Salesforce_Update__c = true AND ASR__c != null AND API_Type__c =  \'Receive NeuStar Message\' AND PON__c != \'\'';
        
        
        //System.debug('query IntLOG >>>>>>>>'+query );
        return Database.getQueryLocator(query);
    }
    
    public void execute(SchedulableContext sc) {
    
         if(!Test.isRunningTest())    
            Database.executeBatch(new BatchForNeuStarToATOMSUpdate(), integer.valueOf(Label.ListenerSFDCUpdateBatchSize));
    }
    
    
    public void execute(Database.BatchableContext bc, List<Integration_Log__c> scope) {
        
        //Collection Declaration
        List<Integration_Log__c> intLogToATOMSList = new List<Integration_Log__c>();
         activateATOMSAPICall = true;
        Boolean activateSFDCUpdate = true;
       
      
       
       
       
       //Switch Functionality: To control Update SFDC and ATOMS API Call througha Custom Setting
       Map<String, SwitchAPICallOuts__c> switchAPICustSettings = SwitchAPICallOuts__c.getAll(); 
       Map<String,Boolean>  switchAPICustSetMap = new Map<String,Boolean>();
       
         for(SwitchAPICallOuts__c utilRec : switchAPICustSettings.values()){
            switchAPICustSetMap.put(utilRec.Name,utilRec.Execute_API_Call__c);
        }
       
        if(switchAPICustSetMap != null && switchAPICustSetMap.get('ATOMSUpdateAPI') != null && (!switchAPICustSetMap.get('ATOMSUpdateAPI'))){
            activateATOMSAPICall = false;
        }
        if(switchAPICustSetMap != null && switchAPICustSetMap.get('UpdateATOMSinSFDC') != null && (!switchAPICustSetMap.get('UpdateATOMSinSFDC'))){
            activateSFDCUpdate = false;
        }
        
  
        //Iterate through loop and send each Integration Log Record and generate a List of Integration Log
        for(Integration_Log__c intLogRec : scope){
            intLogToATOMSList.add(intLogRec);   
        }
   
        // Call the Utility Class and Method to Parse Rquest from NeuStar, extract the fields, update Salesforce ASR and Circuit and Send the fields to ATOMS
        try{
            UpdateNeuStarDetailsToATOMSUtility.responseUpdWrapper respWrapper = new UpdateNeuStarDetailsToATOMSUtility.responseUpdWrapper();
            
            respWrapper = UpdateNeuStarDetailsToATOMSUtility.updateNeuStarToSalesforce(intLogToATOMSList,activateSFDCUpdate,false);
            //system.debug('respWrapper>>'+respWrapper);
            
            
            //system.debug('respWrapper>>'+respWrapper);
            //system.debug('CASEASRMap1>>'+caseIdASRListMap);
            
            if(respWrapper != null){
                if(respWrapper.caseIdASRLstMap != null){
                    //system.debug('respWrappeASRr>>'+respWrapper.caseIdASRLstMap);
                    caseIdASRListMap.putAll(respWrapper.caseIdASRLstMap);
                }
                if(respWrapper.caseIdCktLstMap != null){
                    //system.debug('respWrapperCKT>>'+respWrapper.caseIdCktLstMap);
                    caseIdCktListMap.putAll(respWrapper.caseIdCktLstMap);
                }
                //caseIdCktListMap = respWrapper.caseIdCktLstMap != null ? respWrapper.caseIdCktLstMap : null;
            }
            
            //system.debug('CASEASRMap2>>'+caseIdASRListMap);
            
        }
        catch(exception ex){
            system.debug('exception in UpdateNeuStarDetailsToATOMSUtility'+ex);
        }
                
         
    }
    public void finish(Database.BatchableContext bc){
    
        //Calling Queuable Method to update ASR and Circuit in ATOMS:
        //system.debug('caseIdASRListMap>>FINISH'+caseIdASRListMap);
        //system.debug('caseIdCktListMap>>FINISH'+caseIdCktListMap);
        if((caseIdASRListMap != null || caseIdCktListMap != null) && activateATOMSAPICall ){
            ID jobID = System.enqueueJob(new CallATOMSUpdateAPIQueueable(caseIdASRListMap,caseIdCktListMap));
        }
    
        //
        
       
    }

    
}
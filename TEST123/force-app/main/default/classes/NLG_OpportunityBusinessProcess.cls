/*
*********************************************************************************************
@author  :  Shiva Goli    
@date    :  06/24/2020  
@description: NLG Opportunity business process. Holds all processing work for NLG LOB.          
@Version:

**********************************************************************************************/
public with sharing class NLG_OpportunityBusinessProcess {
    public static Set<String> NLGNationalTeamSet = new Set<String>{'BTS/BTR Team','InBLD/DAS/Venue','NLRP/LO','National - ATC Closeout Team','Other'};
    public void beforeInsert(List<Opportunity> newOpps){
        //Calculate Lease Liablity for NLG Opportunities
        //populateNLGDefaults(newOpps,new Map<Id, Opportunity>());
        AssignNLGCalculationRecord(newOpps);
       
    }
   
    public void beforeUpdate(List<Opportunity> newOpps, Map<Id, Opportunity> newOppsMap, Map<Id, Opportunity> oldOppsMap){
        //Calculate Lease Liablity for NLG Opportunities
        //populateNLGDefaults(newOpps,oldOppsMap);
       
       
        //AssignNLGCalculationRecord(newOpps);
       
    }
    public void afterInsert(List<Opportunity> newOpps,Map<Id,Opportunity> newMap){
        createOpportunityTeamMembers(newMap);
       
    }
    public void AfterUpdate(List<Opportunity> newOpps, Map<Id, Opportunity> newOppsMap, Map<Id, Opportunity> oldOppsMap){
        Map<Id,Opportunity> marketRegionMlaChangeOpps = new Map<Id,Opportunity>();
        for(Opportunity opp: newOppsMap.values()){
            // validate for changes
            if(validateOptTeamChange(oldOppsMap.get(opp.Id),opp)){
                marketRegionMlaChangeOpps.put(opp.id,opp);
            }
        }
       
        //if any changes to opp team
        if(marketRegionMlaChangeOpps.size()>0){
            deleteOppTeam(marketRegionMlaChangeOpps.keySet());
            createOpportunityTeamMembers(marketRegionMlaChangeOpps);
        }
    }
   
    private void AssignNLGCalculationRecord(List<Opportunity> newOpps){        
        // Opportunity   Landlord__c = 'Private LL' , Lease_or_AMD__c='Lease'
        // Contcate   --    [ Private LL-Lease]
        //select id,Landlord__c, Lease_or_AMD__c   from Opportunity where id='006R000000XePTDIA3'
        //select id,Landlord__c, Lease_Type__c,Name  from NLGCalculation__c  
       
        //TODO:check old and new value
        System.debug('--> Assigning NLG Calc');
        Map<id,NLGCalculation__c> mapNLG = New Map<id,NLGCalculation__c>([select id,Landlord__c, Lease_Type__c,Name  from NLGCalculation__c]);
        System.debug('--> '+ mapNLG.size());
        for(Opportunity opp: newOpps)
        {
            for(NLGCalculation__c nlgCalc : mapNLG.values())
            {
               
                if(opp.Landlord__c== nlgCalc.Landlord__c  &&  opp.Lease_or_AMD__c == nlgCalc.Lease_Type__c)
                {
                    opp.NLGCalculation__c = nlgCalc.Id;
                    System.debug('--> Assigning NLG Calc' + nlgCalc.Id);
                }
            }
        }
       
    }
   
    /*public void assignOpportunityTeamMembers(Map<Id,Opportunity> oppsMap){
        List<OpportunityTeamMember> oppTeamList = new List<OpportunityTeamMember>();
        Map<String,List<AccountContactRelation >> marketContactMap = new Map<String,List<AccountContactRelation >>();
        Map<String,List<AccountContactRelation >> regionalContactMap = new Map<String,List<AccountContactRelation >>();
        Map<String,List<AccountTeamMember>> marketTeamMap = new Map<String,List<AccountTeamMember>>();
        Map<String,List<AccountTeamMember>> regionalTeamMap = new Map<String,List<AccountTeamMember>>();
        Map<String,List<AccountTeamMember>> nlgTeamMap = new Map<String,List<AccountTeamMember>>();
        List<AccountTeamMember> nlgTeamList = new List<AccountTeamMember>();
        for(Opportunity opp: oppsMap.values()){
            if(opp.Market__c !=null && !marketTeamMap.containsKey(opp.Market__c)){
                marketTeamMap.put(opp.Market__c.toupperCase(), new List<AccountTeamMember>());
            }
            if(opp.Market__c !=null && !regionalTeamMap.containsKey(opp.Market__c)){
                regionalTeamMap.put(opp.Region__c.toupperCase(), new List<AccountTeamMember>());
            }
        }        
        if(!marketTeamMap.isEmpty()|| !regionalTeamMap.isEmpty()){
            String accountId = System.label.NLGPartnerAccount;
            String selectString ='SELECT Id, AccountId, UserId, TeamMemberRole, Market__c, Region__c, MLA__c FROM AccountTeamMember ';
            String whereClause='Where AccountId =\''+accountId+'\' AND ((TeamMemberRole in (\'NLG Manager1\',\'NLG Manager2\',\'NLG Review\',\'NLG Director1\',\'NLG Director2\')) ';
            if(!marketTeamMap.isEmpty()){
                String marketIncludes='';
                for(String st : marketTeamMap.keyset()){
                    marketIncludes += marketIncludes!=''?',\''+st+'\'':'\''+st+'\'';
                }
                whereClause+= 'OR (Market__c includes ('+marketIncludes+') AND TeamMemberRole =\'Market POC (NLG)\') ';
            }
            if(!regionalTeamMap.isEmpty()){
                Set<String> regionSet = regionalTeamMap.keyset();
                String regionIncludes='';
                for(String st : regionalTeamMap.keyset()){
                    regionIncludes += regionIncludes!=''?',\''+st+'\'':'\''+st+'\'';
                }
                whereClause+= 'OR (Region__c includes ('+regionIncludes+') AND TeamMemberRole in (\'Regional POC (NLG)\',\'NLG Financial\')) ';
            }
            whereClause +=')';
            system.debug('Query:'+selectString+whereClause);
            List<AccountTeamMember> acctTeamList = Database.query(selectString+whereClause);
            if(acctTeamList.size()>0){
                for(AccountTeamMember atm: acctTeamList){
                    if(atm.TeamMemberRole =='Market POC (NLG)' && atm.Market__c !=null){
                        List<String> marketList = atm.Market__c.Split(';');
                        for(String market: marketList){
                            if(!marketTeamMap.containsKey(market.toupperCase())) {
                                marketTeamMap.put(market.toupperCase(), new List<AccountTeamMember>());
                            }
                            marketTeamMap.get(market.toupperCase()).add(atm);
                        }                        
                    }
                    if(atm.TeamMemberRole =='Regional POC (NLG)' && atm.Region__c !=null){
                        List<String> regionList = atm.Region__c.Split(';');
                        for(String region: regionList){
                            if(!regionalTeamMap.containsKey(region.toupperCase())) {
                                regionalTeamMap.put(region.toupperCase(), new List<AccountTeamMember>());
                            }
                            regionalTeamMap.get(region.toupperCase()).add(atm);
                        }                        
                    }
                    if(atm.TeamMemberRole =='NLG Review' ||atm.TeamMemberRole =='NLG Manager1' || atm.TeamMemberRole =='NLG Manager2' || atm.TeamMemberRole =='NLG Director1'|| atm.TeamMemberRole =='NLG Director2'||atm.TeamMemberRole =='NLG Financial'){
                        if(!nlgTeamMap.containsKey(atm.TeamMemberRole)){
                            nlgTeamMap.put(atm.TeamMemberRole, new List<AccountTeamMember>());
                        }
                        nlgTeamMap.get(atm.TeamMemberRole).add(atm);
                        nlgTeamList.add(atm);
                    }
                }
                for(Opportunity opp: oppsMap.values()){
                    if(opp.Market__c !=null && marketTeamMap.containsKey(opp.Market__c)){
                        for(AccountTeamMember otm: marketTeamMap.get(opp.Market__c)){
                            oppTeamList.add(buildOppTeam(otm.UserId,opp.Id,otm.TeamMemberRole));
                        }                        
                    }
                    if(opp.Region__c !=null && regionalTeamMap.containsKey(opp.Region__c)){
                        for(AccountTeamMember otm: regionalTeamMap.get(opp.Region__c)){
                            oppTeamList.add(buildOppTeam(otm.UserId,opp.Id,otm.TeamMemberRole));
                        }                        
                    }
                    if(!nlgTeamList.isEmpty()){                        
                        for(AccountTeamMember otm: nlgTeamList){
                            oppTeamList.add(buildOppTeam(otm.UserId,opp.Id,otm.TeamMemberRole));
                        }                        
                    }
                }
            }
            if(oppTeamList.size()>0){
                Insert oppTeamList;
            }
        }
    }*/
   
    public Boolean validateOptTeamChange(Opportunity oldOpp, Opportunity newOpp){
        if(newOpp.Market_s__c !=null && oldOpp.Market_s__c != newOpp.Market_s__c){
            return true;
        }
        if(newOpp.Region__c !=null && oldOpp.Region__c != newOpp.Region__c){
            return true;
        }
        if(newOpp.Landlord__c !=null && oldOpp.Landlord__c != newOpp.Landlord__c){
            return true;
        }
        if(newOpp.Negotiating_Vendor__c !=null && oldOpp.Negotiating_Vendor__c != newOpp.Negotiating_Vendor__c){
            return true;
        }
        return false;
    }
    public void createOpportunityTeamMembers(Map<Id,Opportunity> oppsMap){
        List<OpportunityTeamMember> oppTeamList = new List<OpportunityTeamMember>();
        Map<String,List<AccountContactRelation >> marketContactMap = new Map<String,List<AccountContactRelation >>();
        Map<String,List<AccountContactRelation >> regionalContactMap = new Map<String,List<AccountContactRelation >>();
        Map<String,List<User>> marketTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> regionalTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> financeTeamMap = new Map<String,List<User>>();
		Map<String,List<User>> readOnlyTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> managerTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> nlgReviewTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> nlgTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> nationalMarketTeamMap = new Map<String,List<User>>();
        Map<String,List<User>> nationalRegionTeamMap = new Map<String,List<User>>();
        List<User> nlgTeamList = new List<User>();
        for(Opportunity opp: oppsMap.values()){
            if(opp.Market_s__c !=null && !marketTeamMap.containsKey(opp.Market_s__c)){
                marketTeamMap.put(opp.Market_s__c.toupperCase(), new List<User>());
            }
            if(opp.Region__c !=null && !regionalTeamMap.containsKey(opp.Region__c)){
                regionalTeamMap.put(opp.Region__c.toupperCase(), new List<User>());
            }
            if(opp.Negotiating_Vendor__c !=null && !nationalMarketTeamMap.containsKey(opp.Negotiating_Vendor__c)){
                nationalMarketTeamMap.put(opp.Negotiating_Vendor__c.toupperCase(), new List<User>());
            }
        }        
        if(!marketTeamMap.isEmpty()|| !regionalTeamMap.isEmpty()){
            //String accountId = System.label.NLGPartnerAccount;
            String selectString ='Select id,name,Market_NLG__c,Region_NLG__c,MLA_NLG__c,Role_NLG__c,NLG_National_Team__c  From User ';
            String whereClause='Where isActive=true AND ((Role_NLG__c in (\'NLG Manager1\',\'NLG Manager2\',\'NLG Review\',\'NLG Director1\',\'NLG Director2\')) ';
            if(!marketTeamMap.isEmpty()){
                String marketIncludes='';
                for(String st : marketTeamMap.keyset()){
                    marketIncludes += marketIncludes!=''?',\''+st+'\'':'\''+st+'\'';
                }
                whereClause+= 'OR (Market_NLG__c includes ('+marketIncludes+') AND Role_NLG__c =\'Market POC (NLG)\') ';
            }
            if(!regionalTeamMap.isEmpty()){
                Set<String> regionSet = regionalTeamMap.keyset();
                String regionIncludes='';
                for(String st : regionalTeamMap.keyset()){
                    regionIncludes += regionIncludes!=''?',\''+st+'\'':'\''+st+'\'';
                }
                whereClause+= 'OR (Region_NLG__c includes ('+regionIncludes+') AND Role_NLG__c in (\'Regional POC (NLG)\',\'NLG Financial\',\'Read Only\')) '; // Added Ready Only as part of story : By Mallesh
            }
            if(!nationalMarketTeamMap.isEmpty()){
                Set<String> negotiatingSet = nationalMarketTeamMap.keyset();
                String negotiatingIncludes='';
                for(String st : nationalMarketTeamMap.keyset()){
                    negotiatingIncludes += negotiatingIncludes!=''?',\''+st+'\'':'\''+st+'\'';
                }
                system.debug('negotiatingIncludes:'+negotiatingIncludes);
                whereClause+= 'OR (NLG_National_Team__c includes ('+negotiatingIncludes+') AND Role_NLG__c in (\'Market POC (NLG)\')) ';
            }
            whereClause+=')';
            system.debug('WhereClause:'+whereClause);
            system.debug('Query:'+selectString+whereClause);
            List<User> userList = Database.query(selectString+whereClause);
            if(userList.size()>0){
                for(User usr: userList){
                    List<String> regionList = new List<String>();
                    if(usr.Region_NLG__c !=null){
                        regionList = usr.Region_NLG__c.Split(';');
                    }
                   
                    if(usr.Role_NLG__c =='Market POC (NLG)'){
                        if(usr.Market_NLG__c !=null){
                            List<String> marketList = usr.Market_NLG__c.Split(';');
                            for(String market: marketList){
                                if(!marketTeamMap.containsKey(market.toupperCase())) {
                                    marketTeamMap.put(market.toupperCase(), new List<User>());
                                }
                                marketTeamMap.get(market.toupperCase()).add(usr);
                            }
                        }
                        if(usr.NLG_National_Team__c !=null){
                           List<String> ntList = usr.NLG_National_Team__c.Split(';');
                            for(String nt: ntList){
                                if(!nationalMarketTeamMap.containsKey(nt.toupperCase())) {
                                    nationalMarketTeamMap.put(nt.toupperCase(), new List<User>());
                                }
                                nationalMarketTeamMap.get(nt.toupperCase()).add(usr);
                            }
                        }
                    }                   
                    if(usr.Role_NLG__c =='Regional POC (NLG)'){
                        //List<String> regionList = usr.Region_NLG__c.Split(';');
                        if(usr.Region_NLG__c !=null && regionList.size()>0){
                            for(String region: regionList){
                                if(!regionalTeamMap.containsKey(region.toupperCase())) {
                                   
                                    regionalTeamMap.put(region.toupperCase(), new List<User>());
                                }
                                system.debug('Region:'+region+usr);
                                regionalTeamMap.get(region.toupperCase()).add(usr);
                            }
                        }
                       
						if(usr.NLG_National_Team__c !=null){
                           List<String> ntList = usr.NLG_National_Team__c.Split(';');
                            for(String nt: ntList){
                                if(!nationalRegionTeamMap.containsKey(nt.toupperCase())) {
                                    nationalRegionTeamMap.put(nt.toupperCase(), new List<User>());
                                }
                                nationalRegionTeamMap.get(nt.toupperCase()).add(usr);
                            }
                        }                        
                    }
                    if(usr.Role_NLG__c =='NLG Financial' && usr.Region_NLG__c !=null && regionList.size()>0){
                        //List<String> regionList = usr.Region_NLG__c.Split(';');
                        for(String region: regionList){
                            if(!financeTeamMap.containsKey(region.toupperCase())) {
                                financeTeamMap.put(region.toupperCase(), new List<User>());
                            }
                            financeTeamMap.get(region.toupperCase()).add(usr);
                        }                        
                    }
                    // Added Ready Only as part of story : By Mallesh
					if(usr.Role_NLG__c =='Read Only' && usr.Region_NLG__c !=null && regionList.size()>0){
                        for(String region: regionList){
                            if(!readOnlyTeamMap.containsKey(region.toupperCase())) {
                                readOnlyTeamMap.put(region.toupperCase(), new List<User>());
                            }
                            readOnlyTeamMap.get(region.toupperCase()).add(usr);
                        }                        
                    }
                    if(usr.Role_NLG__c =='NLG Manager1' && usr.Region_NLG__c !=null && regionList.size()>0){
                        //List<String> regionList = usr.Region_NLG__c.Split(';');
                        for(String region: regionList){
                            if(!managerTeamMap.containsKey(region.toupperCase())) {
                                managerTeamMap.put(region.toupperCase(), new List<User>());
                            }
                            managerTeamMap.get(region.toupperCase()).add(usr);
                        }                        
                    }
                    if(usr.Role_NLG__c =='NLG Review' && usr.Region_NLG__c !=null && regionList.size()>0){
                        //List<String> regionList = usr.Region_NLG__c.Split(';');
                        for(String region: regionList){
                            if(!nlgReviewTeamMap.containsKey(region.toupperCase())) {
                                nlgReviewTeamMap.put(region.toupperCase(), new List<User>());
                            }
                            nlgReviewTeamMap.get(region.toupperCase()).add(usr);
                        }                        
                    }
                    if(usr.Role_NLG__c =='NLG Manager2' || usr.Role_NLG__c =='NLG Director1'|| usr.Role_NLG__c =='NLG Director2' ||usr.Role_NLG__c =='NLG Director2.1'){
                        if(!nlgTeamMap.containsKey(usr.Role_NLG__c)){
                            nlgTeamMap.put(usr.Role_NLG__c, new List<User>());
                        }
                        nlgTeamMap.get(usr.Role_NLG__c).add(usr);
                        nlgTeamList.add(usr);
                    }
                }
                for(Opportunity opp: oppsMap.values()){
                    String mla = opp.Landlord__c!=null && opp.Landlord__c=='Private LL'?'N/A':opp.Landlord__c;
                    if(opp.Negotiating_Vendor__c!=null && NLGNationalTeamSet.contains(opp.Negotiating_Vendor__c)){
                        if(nationalMarketTeamMap.size()>0 && nationalMarketTeamMap.containsKey(opp.Negotiating_Vendor__c.toupperCase())){
                            for(User usr: nationalMarketTeamMap.get(opp.Negotiating_Vendor__c.toupperCase())){
                                if(usr.MLA_NLG__c.toupperCase().contains(mla.toupperCase())){
                                    oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                                }
                            }  
                        }
                    } else {
                        if(opp.Market_s__c !=null && marketTeamMap.containsKey(opp.Market_s__c.toupperCase())){
                            for(User usr: marketTeamMap.get(opp.Market_s__c.toupperCase())){
                                if(usr.MLA_NLG__c.toupperCase().contains(mla.toupperCase())){
                                    oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                                }
                            }                        
                        }
                    }
                   
                    if(opp.Region__c !=null){
                        if(regionalTeamMap.containsKey(opp.Region__c.toupperCase())){
                            for(User usr: regionalTeamMap.get(opp.Region__c.toupperCase())){
                                if(usr.MLA_NLG__c.toupperCase().contains(mla.toupperCase())){
                                    oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                                }
                            }
                        }
                        if(financeTeamMap.containsKey(opp.Region__c.toupperCase())){
                            for(User usr: financeTeamMap.get(opp.Region__c.toupperCase())){
                                oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                            }
                        }
                        // Added Ready Only as part of story : By Mallesh
						if(readOnlyTeamMap.containsKey(opp.Region__c.toupperCase())){
                            for(User usr: readOnlyTeamMap.get(opp.Region__c.toupperCase())){
                                oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Read'));
                            }
                        }
                        if(managerTeamMap.containsKey(opp.Region__c.toupperCase())){
                            for(User usr: managerTeamMap.get(opp.Region__c.toupperCase())){
                                oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                            }
                        }
                        if(nlgReviewTeamMap.containsKey(opp.Region__c.toupperCase())){
                            for(User usr: nlgReviewTeamMap.get(opp.Region__c.toupperCase())){
                                oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                            }
                        }
                    }
                    if(!nlgTeamList.isEmpty()){                        
                        for(User usr: nlgTeamList){
                            oppTeamList.add(buildOppTeam(usr.Id,opp.Id,usr.Role_NLG__c, 'Edit'));
                        }                        
                    }
                }
            }
            if(oppTeamList.size()>0){
                Insert oppTeamList;
            }
        }
    }
   
    private void deleteOppTeam(Set<Id> oppIds){
        List<OpportunityTeamMember> otmList = new List<OpportunityTeamMember>();
        otmlist =[Select Id from OpportunityTeamMember Where OpportunityId in :oppIds];
        if(otmList.size()>0){
            Delete otmList;
        }
    }
    // Added accesslevel parameter as part of story : By Mallesh
    private OpportunityTeamMember buildOppTeam(Id userId, Id OppId, String role, String accessLevel){
        OpportunityTeamMember otm = new OpportunityTeamMember(OpportunityId=OppId);
        otm.UserId =userId;
        otm.OpportunityAccessLevel = accessLevel;
        otm.TeamMemberRole =role;
        return otm;
    }
}
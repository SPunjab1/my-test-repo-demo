/*********************************************************************************************
@author       : IBM
@date         : 15.09.20 
@description  : This is the Component Handlerclass for updating OM Integration Status = Start Integration after checking validations.
                Button named 'Send to ATOMS' is avilable on Detailed Page of WorkOrder Task
@Version      : 1.0        
**********************************************************************************************/


/*********************************************************************************************
* @Description :Method to Validate the login user Profile to provide 'Send to ATOMS' button access on the WorkOrderTAsk
**********************************************************************************************/
public with sharing class SendtoAtomsHandlerCls {
    
    @AuraEnabled
    public static boolean validateLoginUserProfile(){
        List<String> profilesToValidateLst = Label.UsersinProfilesCanStartAtomsInt.split(',');
        List<Profile> uprofile = [Select Id,Name from Profile where Id=:userinfo.getProfileId() AND Profile.Name IN: profilesToValidateLst];
        if(! uprofile.isEmpty()){
            return true;
        }else{
            return false;
        }
    }

/*********************************************************************************************
* @Description :  Method to update the OM Integration Status field value to 'Start Integation'
* @input params : taskId - Current Workorder Task id
**********************************************************************************************/
    @AuraEnabled
    public static string startAtomsIntegration(Id taskId){
        List<Case> casestoUpdateOmIntStatus = new List<Case>();
        for(Case woTask: [select Id, OM_Integration_Status__c,task_number__c 
                            FROM Case 
                            WHERE Id =: taskId AND OM_Integration_Status__c != 'Start Integration']){
            woTask.OM_Integration_Status__c =   'Start Integration';
            casestoUpdateOmIntStatus.add(woTask);
        }

        if(! casestoUpdateOmIntStatus.isEmpty()){
                update casestoUpdateOmIntStatus;
                return 'Success';
        }else{
            return 'Failed';
        }
    
    }
    
}
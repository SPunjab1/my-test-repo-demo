@istest
public class OpportunityTriggerHandlerTest {
	@testSetup
    static void testDataSetup() {
         //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //Intialize list of SI Savings Tracker to insert records
        List<SI_Savings_Tracker__c> listSISavingTracker = new  List<SI_Savings_Tracker__c>();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
         // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = listAccount[0].Id;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        insert oppRecForContract;
        Site_Lease_Escalation__c siteLease = TestDataFactory.createSiteLEaseEscalation(oppRecForContract.Id);
		//lease.Lease_Intake__c = oppRecForContract.Id;
		siteLease.Lease_Version__c ='New';
        siteLease.Escalation_Type__c ='One Time';
        insert siteLease; // NLG_Util
    }
     
    @istest static void nlgOppCalculatorTest(){
        Test.startTest();
         Opportunity opportunityRec = [Select id,New_Rent__c,Initial_Term_NewLease__c,Remaining_Current_Term_Years__c from Opportunity limit 1];
         opportunityRec.New_Rent__c=100;
         opportunityRec.New_Escalator_Text__c='Escalator';
         opportunityRec.New_Escalator_Frequency__c='Annual';
         opportunityRec.New_Rent_Frequency__c = 'Monthly';
         opportunityRec.New_Rent_Commencement_Date__c = Date.today();   
         opportunityRec.New_Final_Expiration_New_Lease_Date__c= Date.today()+30;
        opportunityRec.Current_Rent_Frequency__c='Monthly';
        opportunityRec.Current_Term_Start_Date__c=Date.today(); 
        opportunityRec.Initial_Term_NewLease__c = 1;
        opportunityRec.Current_Term_End_Date__c = Date.today()+380;
        opportunityRec.Current_Escalator_Frequency__c='Annual';
        opportunityRec.Current_Escalator_txt__c = 'Escalator';
        opportunityRec.Current_Rent__c = 40000;
        opportunityRec.Term_mo__c = 22;
         update opportunityRec;
        Test.stopTest();
    }
}
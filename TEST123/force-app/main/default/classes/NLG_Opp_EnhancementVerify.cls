public class NLG_Opp_EnhancementVerify {

    
    @InvocableMethod
    public static List<String> VerifyOpportunityEnhacements(List<String> oppIdList){
        String isRecordBlank = 'False';
       try{
           
            
            Opportunity opp = [Select Id,Lease_or_AMD__c,Landlord__c
                               From Opportunity Where Id =:oppIdList[0] limit 1];
           
             List<Lease_Enhancement__c> leaseEnhancements =  [Select Id,Description__c, Current_Lease__c, Current_Document_Location__c,New_Lease__c, New_Document_Location__c
                               From Lease_Enhancement__c   Where Lease_Intake__c  =:oppIdList[0] 
                               AND (New_Lease__c =null or New_Document_Location__c =null
                                   or  Current_Document_Location__c = null
                                   or  Current_Lease__c =null)
                                ];
             
             Lease_Enhancement__c leaseEnhancement = null;
             if(leaseEnhancements.size()>0)
             {
                 leaseEnhancement = leaseEnhancements[0];
             }
             if(leaseEnhancement!=null && opp.Landlord__c !=null && opp.Landlord__c =='Private LL')
             {
                 if(opp.Lease_or_AMD__c=='AMD')
                 {
                     if ( String.isBlank(leaseEnhancement.Current_Lease__c)
                          || String.isBlank(leaseEnhancement.Current_Document_Location__c)
                          || String.isBlank(leaseEnhancement.New_Lease__c)
                          || String.isBlank(leaseEnhancement.New_Document_Location__c))
                     {
                         isRecordBlank = 'True';
                     }
                         
                 }                 
                 else if ( String.isBlank(leaseEnhancement.New_Lease__c)
                          || String.isBlank(leaseEnhancement.New_Document_Location__c)){
                     isRecordBlank = 'True';
                 }
                 
             }
             
            return new List<String>{isRecordBlank};
           
          } catch(Exception ex){
                    system.debug('-->NLG_Opp_EnhancementVerify:'+ex.getMessage());
                    return new List<String>{ex.getMessage()};
                    
         }
    }
}
/******************************************************************************
@author      : IBM
@date        : 18.06.2020
@description : Test class for SI_ValidateOpptyApproversController apex class
@Version     : 1.0
*******************************************************************************/
@isTest(SeeAllData=false)
public class SI_ValidateOpptyApproversControllerTest {
    
    /*************************************************************************************
    * @description Test data setup method
    *************************************************************************************/
    @testSetup static void testDataSetup() {
        Id opptyRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Small_Cell_Contract').getRecordTypeId();
        List<Opportunity> oppList = TestDataFactory.createOpportunitySingleIntake(2,opptyRecTypeId);
        insert oppList;
        oppList[0].ETL_MRC__c = 20000000.00;
        oppList[0].ETL_NRC__c = 10000000.00;
        oppList[0].Region__c = 'West';
        update oppList[0];
    }
    
    /*************************************************************************************
    * @description Test Method for validateOpportunityAndRedirect when opportunity has all 8 approvers populated
    *************************************************************************************/
    static testmethod void testValidateOpportunityWithApprovers() { 
        
        Opportunity opp = [Select Id, Approver_3__c
                           FROM Opportunity
                           WHERE Approver_3__c != Null];
   
        PageReference docusignPage ;
        test.startTest();
            PageReference pageRef = Page.SI_ValidateOpptyApprovers;
            test.setCurrentPage(pageRef);        
            pageRef.getParameters().put('Id', opp.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            SI_ValidateOpptyApproversController testopptyapprovers = new SI_ValidateOpptyApproversController(sc);
            docusignPage = testopptyapprovers.validateOpportunityAndRedirect();
        test.stopTest();
        system.assert(docusignPage.getUrl().startsWithIgnoreCase(Label.SI_DocusignURL),'Url should match with docusign url');
    }

    /*************************************************************************************
    * @description Test Method for validateOpportunityAndRedirect when opportunity has none of the approvers populated
    *************************************************************************************/
    static testmethod void testValidateOpportunityWithoutApprovers() { 
        Opportunity opp = [Select Id, Approver_1__c
                           FROM Opportunity
                           WHERE Approver_1__c = Null limit 1];
        PageReference docusignPage ;
        test.startTest();
            PageReference pageRef = Page.SI_ValidateOpptyApprovers;
            test.setCurrentPage(pageRef);        
            pageRef.getParameters().put('Id', opp.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            SI_ValidateOpptyApproversController testopptyapprovers = new SI_ValidateOpptyApproversController(sc);
            docusignPage = testopptyapprovers.validateOpportunityAndRedirect();
        test.stopTest();
        system.assertEquals(null,docusignPage,'Control should not redirect to docusign page');
        System.assertEquals(null,opp.Approver_1__c,'Opportunity should not have required Approvers');
    }
    
    /*************************************************************************************
    * @description Test Method for back button
    *************************************************************************************/
    static testmethod void testBackButton() { 
        Opportunity opp = [Select Id, SI_ETL_Total__c
                           FROM Opportunity limit 1];
        PageReference returnPage ;
        test.startTest();
            PageReference pageRef = Page.SI_ValidateOpptyApprovers;
            test.setCurrentPage(pageRef);        
            pageRef.getParameters().put('Id', opp.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            SI_ValidateOpptyApproversController testopptyapprovers = new SI_ValidateOpptyApproversController(sc);
            returnPage = testopptyapprovers.back();
        test.stopTest();
        system.assert(returnPage.getUrl().startsWith('/006'),'Back button should return to the calling Opportunity');
    }
}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ValidateBeforeSendingToAtomsControllerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 15.09.2020
*  @Description      : Test class for validating role of user who is bulk initiating Atoms Integration
**********************************************************************************************************************
****************************************************************************************************************/

@isTest(SeeAllData=false)
public with sharing class ValidateBeforeSendingToAtomsTest{
    
  static  testmethod void runTestValidateUsersControllerPositiveTest() {
  User su = TestDataFactory.createSourcingUser();
  UserRole r = new UserRole(DeveloperName = 'OMSourcingUser', Name = 'OM Sourcing User');
  insert r;
  su.UserRoleId = r.Id;
  insert su;
  
   system.runas(su){
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        DmlException expectedException;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();       
        ApexPages.StandardSetController stdSetController1 = new ApexPages.StandardSetController([ SELECT id, status, CaseNumber FROM Case ]);
        stdSetController1.setSelected( stdSetController1.getRecords() );
        ValidateBeforeSendingToAtomsController controller1 = new ValidateBeforeSendingToAtomsController( stdSetController1 );
        controller1.StartIntegration();
            
            
        List<Case> caseList= new List<Case>();
        Integer NumOfCases=10;
        String WorkflowTmpName1='AAV CIR Retest';
        caseList= TestDataFactoryOMph2.createCase(WorkflowTmpName1,NumOfCases,recTypeId); 

        Test.startTest();   
        Insert(caseList); 
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController([ SELECT id, status, CaseNumber FROM Case ]);
        stdSetController.setSelected( stdSetController.getRecords() );
        ValidateBeforeSendingToAtomsController controller = new ValidateBeforeSendingToAtomsController( stdSetController );
        controller.StartIntegration();
        controller.back();               
        Test.stopTest();             
  }  
 }
    
  static  testmethod void runTestValidateUsersControllerNegativeTest() {
   system.runas(TestDataFactory.createSourcingUser()){
        DmlException expectedException;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        Integer NumOfCases=10;
        String WorkflowTmpName1='AAV CIR Retest';
        caseList= TestDataFactoryOMph2.createCase(WorkflowTmpName1,NumOfCases,recTypeId); 
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        Test.startTest();   
        Insert(caseList); 
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController([ SELECT id, status, CaseNumber FROM Case ]);
        stdSetController.setSelected( stdSetController.getRecords() );
        ValidateBeforeSendingToAtomsController controller = new ValidateBeforeSendingToAtomsController( stdSetController );
        controller.StartIntegration();
        controller.back();               
        Test.stopTest();             
  }  
 }
}
/******************************************************************************
@author      - IBM
@date        - 12.06.2020
@description - Trigger Handler Class for Opportunity
@version     - 1.0
*******************************************************************************/
public with sharing class OpportunityTriggerHandler extends TriggerHandler{
    public static ID nlg_New_Site_Lease_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Site Lease').getRecordTypeId();
    public static ID nlg_NLG_No_POR_information_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('NLG No POR information').getRecordTypeId();
    public static ID nlg_Settlement_and_Utility_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Settlement and Utility').getRecordTypeId();
    public static ID nlg_Site_AMD_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Site AMD').getRecordTypeId();
    public static boolean isOppTriggerExecuted = FALSE ;
    private static final string OPP_API = 'Opportunity';
    /***************************************************
    *@description - constructor
    ***************************************************/
    public OpportunityTriggerHandler() {
        super(OPP_API);
    }
    /***************************************************************************************
    @description : Return the name of the handler invoked
    @return - API name of Opportunity object
    ****************************************************************************************/
    public override String getName() {
        return OPP_API;
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    ****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems){
        List<Opportunity> newNlgOpps = new List<Opportunity>();
        List<Opportunity> newOpps= new List<Opportunity>();
        newOpps = (List<Opportunity>) newItems;
        
        for(Opportunity newOpp: newOpps){
            //fill in lists to process to various business processes.
            if(newOpp.RecordTypeId==nlg_New_Site_Lease_RecordType
               || newOpp.RecordTypeId== nlg_NLG_No_POR_information_RecordType || newOpp.RecordTypeId== nlg_Settlement_and_Utility_RecordType 
               || newOpp.RecordTypeId== nlg_Site_AMD_RecordType){
                newNlgOpps.add(newOpp);
            }
        }
        
        if(newNlgOpps.size()>0){
            NLG_OpportunityBusinessProcess nlgBusProcess = new NLG_OpportunityBusinessProcess();
            nlgBusProcess.beforeInsert(newNlgOpps);
        }
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    @param newItemsMap - Map of new Opportunities
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap){
        List<Opportunity> newOpps= new List<Opportunity>();
        newOpps = (List<Opportunity>) newItems;
        Map<Id,Opportunity> newMap=new Map<Id,Opportunity>();
        newMap=(Map<Id,Opportunity>)newItemsMap;
        Map<Id,Opportunity> oldMap=new Map<Id,Opportunity>();
        oldMap=(Map<Id,Opportunity>)oldItemsMap;
        Map<Id,Opportunity> newNlgOpps = new Map<Id,Opportunity>();        
        // variables for SI 
        Map<Id,Opportunity> newSIMap=new Map<Id,Opportunity>();
        
        for(Opportunity newOpp: newOpps){
            //fill in lists to process to various business processes.
            if(newOpp.RecordTypeId==nlg_New_Site_Lease_RecordType
               || newOpp.RecordTypeId== nlg_NLG_No_POR_information_RecordType || newOpp.RecordTypeId== nlg_Settlement_and_Utility_RecordType 
               || newOpp.RecordTypeId== nlg_Site_AMD_RecordType){// For NLG Business Process
                   newNlgOpps.put(newOpp.Id,newOpp);
               }
            //fill in lists to process to various SI business processes.
            if(!String.IsBlank(newOpp.Single_Intake_Team__c)){// For SI Business Process
                newSIMap.put(newOpp.Id,newOpp);
            }
        }
        
        if(newSIMap.size()>0){            
            // Calling Approver Method 
            SI_OpportunityTriggerHelper.updateApproverWhenETLValuesChange(newSIMap.values(), oldMap);
            // Calling updateOppSoxValues method for opportunity records where Term_mo__c value is changed
            /*map<Id,Opportunity> opportunityMap = new map<Id,Opportunity>();
            for(Opportunity opp:newSIMap.values()){            
                if(opp.Term_mo__c != oldMap.get(opp.Id).Term_mo__c){
                    opportunityMap.put(opp.Id, opp);
                }
            }
            if(!opportunityMap.keySet().isEmpty()){
                //deactivate opportunity validation rule that prevent update of SOX values
                ValidationRuleManager__c validationRuleSetting = ValidationRuleManager__c.getOrgDefaults();
                if(validationRuleSetting.Run_Validation__c = true){
                    validationRuleSetting.Run_Validation__c = false;
                    try{
                        Update validationRuleSetting; 
                        SI_OpportunityTriggerHelper.updateOppSoxValues(opportunityMap);  
                    } catch(Exception e){
                        System.debug(LoggingLevel.ERROR,'Exception during disabling validation'+e.getMessage());
                    }                
                }          
            }*/
        }
        if(newNlgOpps.size()>0){
            NLG_OpportunityBusinessProcess nlgBusProcess = new NLG_OpportunityBusinessProcess();
            nlgBusProcess.beforeUpdate(newNlgOpps.values(),newNlgOpps,oldMap);
        }
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap){}
    /**************************************************************************************
    @description : Trigger handlers for events
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {}
    /**************************************************************************************
    @description : Trigger handlers for events
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {}
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    @param newItemsMap - Map of new Opportunities
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
        List<Opportunity> newOpps= new List<Opportunity>();
        newOpps = (List<Opportunity>) newItems;
        Map<Id,Opportunity> newMap=new Map<Id,Opportunity>();
        newMap=(Map<Id,Opportunity>)newItemsMap;
        Map<Id,Opportunity> oldMap=new Map<Id,Opportunity>();
        oldMap=(Map<Id,Opportunity>)oldItemsMap;
        Map<Id,Opportunity> newNlgOpps = new Map<Id,Opportunity>();        
        // variables for SI 
        Map<Id,Opportunity> newSIMap=new Map<Id,Opportunity>();
        
        for(Opportunity newOpp: newOpps){
            //fill in lists to process to various business processes.
            if(newOpp.RecordTypeId==nlg_New_Site_Lease_RecordType
               || newOpp.RecordTypeId== nlg_NLG_No_POR_information_RecordType || newOpp.RecordTypeId== nlg_Settlement_and_Utility_RecordType 
               || newOpp.RecordTypeId== nlg_Site_AMD_RecordType){// For NLG Business Process
                   newNlgOpps.put(newOpp.Id,newOpp);
               }
            //fill in lists to process to various SI business processes.
            if(!String.IsBlank(newOpp.Single_Intake_Team__c)){// For SI Business Process
                newSIMap.put(newOpp.Id,newOpp);
            }
        }
        if(newSIMap.size()>0){ 
            //rectivate opportunity validation rule that prevent update of SOX values
            try{
                ValidationRuleManager__c validationRuleSetting = ValidationRuleManager__c.getOrgDefaults();
                validationRuleSetting.Run_Validation__c = true;        
                Update validationRuleSetting;
            } catch(Exception e){
                System.debug(LoggingLevel.ERROR,'Exception during enabling validation'+e.getMessage());
            }  
            /*****method call to update Location Object****/
            SI_OpportunityTriggerHelper.updateLocationRecord(newSIMap.values(),oldMap);
            /********method call to sendContractEmail *****/
            SI_OpportunityTriggerHelper.sendContractEmail(newSIMap.values(),oldMap);  
            /**********method call to updateFileName*****/
            SI_OpportunityTriggerHelper.updateRelatedFileName((List<Opportunity>)newItems,(Map<Id,Opportunity>)oldItemsMap);
            /**********method call to updateCaseOnOpportunity*****/
            SI_OpportunityTriggerHelper.updateCaseOnOpportunity((List<Opportunity>)newItems,(Map<Id,Opportunity>)oldItemsMap);          
        }
        
        if(newNlgOpps.size()>0){
            NLG_OpportunityBusinessProcess nlgBusProcess = new NLG_OpportunityBusinessProcess();
            nlgBusProcess.AfterUpdate(newNlgOpps.values(),newNlgOpps,oldMap);
            OpportunityHistoryTrackerHandler.CreateOpportunityHistoryTrackerRecord(oldMap, newNlgOpps);  
        }
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    @param newItemsMap - Map of new Opportunities
    ****************************************************************************************/
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        List<Opportunity> newOpps= new List<Opportunity>();
        newOpps = (List<Opportunity>) newItems;
        Map<Id,Opportunity> newMap=new Map<Id,Opportunity>();
        newMap=(Map<Id,Opportunity>)newItemsMap;
        //Map<Id,Opportunity> oldMap=new Map<Id,Opportunity>();
        //oldMap=(Map<Id,Opportunity>)oldItemsMap;
        List<Opportunity> newNlgOpps = new List<Opportunity>();
        for(Opportunity newOpp: newOpps){
            //fill in lists to process to various business processes.
            if(newOpp.RecordTypeId==nlg_New_Site_Lease_RecordType
               || newOpp.RecordTypeId== nlg_NLG_No_POR_information_RecordType || newOpp.RecordTypeId== nlg_Settlement_and_Utility_RecordType 
               || newOpp.RecordTypeId== nlg_Site_AMD_RecordType){
                newNlgOpps.add(newOpp);
            }
        }
        
        if(newNlgOpps.size()>0){
            NLG_OpportunityBusinessProcess nlgBusProcess = new NLG_OpportunityBusinessProcess();
            nlgBusProcess.afterInsert(newNlgOpps,newMap);
              
        }
    }
}
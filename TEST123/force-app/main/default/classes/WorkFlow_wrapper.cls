public class WorkFlow_wrapper {
    
    
    public String transactionId;
    public String invokingUser;
    public String timestamp;
    public List<Data> data;
    public Object version;
    public Integer duration;
    
    public class Data {
        public Integer id;
        public Integer templateId;
        public String description;
        public String longDescription;
        public String status;
        public String manager;
        public String createdBy;
        public String createdAt;
        public Object closedAt;
        public String sourceSystem;
        public String sourceRecordId;
        public Tasks tasks;
    }
    
 
    public class Tasks
    {
        public Number0 number0;
        
        public Number1 number1;
        
        public Number2 number2;
        
        public Number3 number3;
    }
    
    public class Number0
    {
        public DataChild data;
        
        public String version;
        
        public String invokingUser;
        
        public integer duration;
        
        public String timestamp;
        
        public String transactionId;
    }
        
    
    public class Number1
    {
        public DataChild data;
        
        public String version;
        
        public String invokingUser;
        
        public integer duration;
        
        public String timestamp;
        
        public String transactionId;
    }
    public class Number2
    {
        public DataChild data;
        
        public String version;
        
        public String invokingUser;
        
        public integer duration;
        
        public String timestamp;
        
        public String transactionId;
    }
    public class Number3
    {
        public DataChild data;
        
        public String version;
        
        public String invokingUser;
        
        public integer duration;
        
        public String timestamp;
        
        public String transactionId;
    }
    
    
    public class DataChild {
        public Integer id;
        public Integer workflow_id;
        public String name;
        public String description;
        public Integer displayOrder;
        public String taskType;
        public Object StartDate;
        public Object EndDate;
        public Object startedAt;
        public Object completedAt;
        public String assignedGroup;
        public String assignee;
        public String status;
        public String createdBy;
        public String createdAt;
        public String config_item;
        public String associated_record;
        public Boolean active_flag;
    }
    
    
    public static WorkFlow_wrapper parse(String json) {
        return (WorkFlow_wrapper) System.JSON.deserialize(json, WorkFlow_wrapper.class);
    }
}
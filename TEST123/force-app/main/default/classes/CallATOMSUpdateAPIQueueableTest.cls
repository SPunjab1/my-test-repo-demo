@isTest
public class CallATOMSUpdateAPIQueueableTest {
    static testmethod void testQueueable() {
        
        List<ATOMSNeuStarUtilitySettings__c> atomUtilSettings = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        //atomUtilSettings.Name = 'SalesforceName';
        //atomUtilSettings.Value__c = 'Salesforce';
        insert atomUtilSettings;
        //String salesforceName = mapConstants.get('SalesforceName').Value__c;
        
        Map<Id,List<ASR__c>> caseIdASRListMap = new Map<Id,List<ASR__c>>();
        Map<Id,List<Circuit__c>> caseIdCktListMap = new Map<Id,List<Circuit__c>>();
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        system.assertNotEquals(recTypeId ,null);
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        insert workOrdTaskList;
         system.assertNotEquals(workOrdTaskList,null);
        system.debug('WORD>>'+workOrdTaskList);
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',1,workOrdTaskList[0].Id);
        insert purOrderList;
        system.assertNotEquals(purOrderList,null);
        system.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList = TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1);
        insert circuitList;
        system.assertNotEquals(circuitList,null);
        system.debug('circuitList>>'+circuitList);
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,1);
        asrList[0].Circuit_Id__c = circuitList[0].Id;
        insert asrList;
        system.assertNotEquals(asrList,null);
        system.debug('ASR>>'+asrList);
        
        for(ASR__c asr : asrList){
            if(!caseIdASRListMap.containsKey(asr.Case__c)){
                caseIdASRListMap.put(asr.Case__c, new List<ASR__c>{asr});
            }
            else{
                caseIdASRListMap.get(asr.Case__c).add(asr);
            }
        }
        
        for(Circuit__c ckt : circuitList){
            if(!caseIdCktListMap.containsKey(ckt.Case__c)){
                caseIdCktListMap.put(ckt.Case__c, new List<Circuit__c>{ckt});
            }
            else{
                caseIdCktListMap.get(ckt.Case__c).add(ckt);
            }
        }
        
        try{
            Test.startTest();  
            system.assertNotEquals(caseIdCktListMap,null);
            system.assertNotEquals(caseIdASRListMap,null);      
            System.enqueueJob(new CallATOMSUpdateAPIQueueable(caseIdASRListMap,caseIdCktListMap));
            Test.stopTest();
        }
        catch(exception ex){
            system.debug('exception in queueabletestClass'+ex);
        }
 
        
    }
}
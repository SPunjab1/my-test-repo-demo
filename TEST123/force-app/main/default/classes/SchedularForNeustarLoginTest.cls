/*********************************************************************************************
@author     : IBM
@date       : 21 Aug 2020
@description: Test class for Batch class SchedularForNeustarLogin 
@Version    : 1.0           
**********************************************************************************************/
@isTest
public class SchedularForNeustarLoginTest {
    static testmethod void schedulerTest() 
    {
    	String CRON_EXP = '5 0 * * * ?'; 
        Test.startTest();
        SchedularForNeustarLogin sh= new SchedularForNeustarLogin();
        System.schedule('ScheduleApexClassTest',  CRON_EXP, sh);
        Test.stopTest();
            
    }

}
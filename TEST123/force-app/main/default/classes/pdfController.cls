/*********************************************************************************************
@Class        : pdfController
@author       : Pritesh
@date         : 11.20.2020 
@description  : This class is to for NLG Abtract PDF generation functionality
@Version      : 1.0           
**********************************************************************************************/
public without Sharing class pdfController {
    
    
    @AuraEnabled
    public static DataWrapper getRegionalApprover(String oppId){
        
        String ApproverName = '';
        final Integer index = 0;
        Decimal currentRentMonthly = 0.00;
        Decimal newRentMonthly = 0.00;
        Decimal rentIncrease =0.00;
        
        List<NLG_Util.signingWrapper> reqSignedWrapperListmeta = new List<NLG_Util.signingWrapper>();
        
        DataWrapper wrapperObj = new DataWrapper();
        String aproverName='';
        
        List<Opportunity_Approval_History__c > oppAppObj = new List<Opportunity_Approval_History__c >();
        oppAppObj = [SELECT Id, Current_Approver__c, Current_Approver__r.Name,Opportunity__r.New_Rent__c,
                     			Opportunity__r.New_Rent_Frequency__c, Opportunity__r.Current_Rent__c, Opportunity__r.Current_Rent_Frequency__c  
                     FROM Opportunity_Approval_History__c WHERE Approval_Stage__c='Regional Review' AND Opportunity__c=:oppId];
        
        if(  NULL != oppAppObj && oppAppObj.size() > 0 ){
            
      
            aproverName =  oppAppObj[index].Current_Approver__r.Name;
            system.debug(aproverName);
            wrapperObj.regionalApprover = aproverName;
            wrapperObj.rentIncrease = '';
            
            Opportunity oppObj = [ SELECT Id, New_Rent__c, New_Rent_Frequency__c, Current_Rent__c, Current_Rent_Frequency__c,  Total_Rent_over_initial_term__c,Total_Commitment__c,New_One_Time_Fees__c,Exception_List__c FROM Opportunity WHERE Id =:oppId ];
             
            reqSignedWrapperListmeta  = NLG_Util.getSigningAuthority(oppObj);
            if( NULL != reqSignedWrapperListmeta.size() && reqSignedWrapperListmeta.size() > 0 )
            	wrapperObj.signAuthority = reqSignedWrapperListmeta.get( reqSignedWrapperListmeta.size() - 1 ).name;            
            
        }
        
        wrapperObj.loggedinUserProfile = getUserProfile();
        
        return wrapperObj ;
    }
    
    public class DataWrapper {
        
        @AuraEnabled public String rentIncrease{get;set;}
        @AuraEnabled public String regionalApprover {get;set;}
        @AuraEnabled public String signAuthority {get;set;}
        @AuraEnabled public String loggedinUserProfile {get;set;}
    }
    
    
    
    @AuraEnabled
    public static void generateNewContentVersion(String oppId, String acqValapex, String dateSringapex, String agrmtHighlightsapex, String rrappapex, String rentincreaseapex, String signapex){
        
        Opportunity oppObj;
        try{
            PageReference pr = new PageReference('/apex/NLGAbstractPDF?Id='+oppId +'&acqVal='+acqValapex + '&dateSringapex=' + dateSringapex + '&agrmtHighlights=' + agrmtHighlightsapex + '&rrapp='+ rrappapex + '&rent=' + rentincreaseapex +'&sign=' + signapex );
            ContentVersion conVobj = new ContentVersion();
            if( String.isNotblank(oppId) )    
                oppObj = [SELECT Id, Name FROM Opportunity WHERE Id=:oppId];
            
            Blob blobVal;
            
            if(Test.isRunningTest()) { 
                blobVal = blob.valueOf('Unit.Test');
            }else{
                blobVal = pr.getContentAsPDF();
            }
            conVobj.Title = oppObj.Name + '_' + 'NLG ABSTRACT_' + Datetime.now().format('MM-dd-yyyy HH-mm-ss') ;
            conVobj.PathOnClient = 'file_' + Datetime.now().format('MM-dd-yyyy HH-mm-ss') + '.pdf';
            conVobj.VersionData = blobVal;
            conVobj.Origin = 'H';
            
            insert conVobj;
            
            ContentVersion cvObj = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVobj.Id];
            
            contentDocumentLink cDockLink = new contentDocumentLink();
            cDockLink.ContentDocumentId = cvObj.ContentDocumentId;
            cDockLink.LinkedEntityId = oppId;
            
            insert cDockLink;
            
        }catch(Exception error){
            throw new AuraHandledException(error.getMessage());
        }
        
    }    
    public static List<NLG_Util.signingWrapper> getSignAuthorityData (String oppId){
        
        List<NLG_Util.signingWrapper> reqSignedWrapperListmeta = new List<NLG_Util.signingWrapper>();
        List<NLG_Util.signingWrapper> reqSignedWrapperList = new List<NLG_Util.signingWrapper>();
        List<NLG_Util.signingWrapper> finalSigningwrapperList = new List<NLG_Util.signingWrapper>();
        String signingAuth = '';
        
        Opportunity oppObj = [ SELECT Id, Total_Rent_over_initial_term__c,Total_Commitment__c,New_One_Time_Fees__c,Exception_List__c FROM Opportunity WHERE Id =:oppId ];
        reqSignedWrapperListmeta  = NLG_Util.getSigningAuthority(oppObj);
        
        if(null !=  reqSignedWrapperListmeta && reqSignedWrapperListmeta.size() > 0 )
        	signingAuth = reqSignedWrapperListmeta.get( reqSignedWrapperListmeta.size() - 1 ).name;  
        
        
        
        NLG_Util.signingWrapper reqSignedWrapperObj1 = new NLG_Util.signingWrapper();
        reqSignedWrapperObj1.name ='';
        reqSignedWrapperObj1.title= system.label.Market_or_Area_Director;
        reqSignedWrapperObj1.email='';
        reqSignedWrapperList.add(reqSignedWrapperObj1);
        
        NLG_Util.signingWrapper reqSignedWrapperObj2 = new NLG_Util.signingWrapper();
        reqSignedWrapperObj2.name ='';
        reqSignedWrapperObj2.title=system.label.Sr_Director_Engineering_Development;
        reqSignedWrapperObj2.email='';
        reqSignedWrapperList.add(reqSignedWrapperObj2);
        
        //system.label.SigningAuthorityException
        ////signingAuth
        List<string> lstLabelvalues = system.label.SigningAuthorityException.split(',');
        Set<string> lablevalueSet= new Set<string>(lstLabelvalues); 
        
        if(!lablevalueSet.contains(signingAuth) ){
            NLG_Util.signingWrapper reqSignedWrapperObj3 = new NLG_Util.signingWrapper();
            reqSignedWrapperObj3.name ='';
            reqSignedWrapperObj3.title= system.label.SVP_Field_Engineering_Technology;
            reqSignedWrapperObj3.email='';
            reqSignedWrapperList.add(reqSignedWrapperObj3);
        }
        
        finalSigningwrapperList.addAll(reqSignedWrapperList);
        finalSigningwrapperList.addAll(reqSignedWrapperListmeta);
        
        return finalSigningwrapperList;
    }
    
    
    public static String getUserProfile( ){
        return [SELECT Id, Name FROM Profile WHERE Id=: Userinfo.getProfileId()].Name;
    }
    
    public static Decimal rentIncreaseCalculator (Opportunity oppObj){
        
        Decimal currentRentMonthly = 0.00;
        Decimal newRentMonthly = 0.00;
        Decimal rentIncrease =0.00;
        
          if(oppObj.New_Rent_Frequency__c == 'Yearly' ){
                newRentMonthly = oppObj.New_Rent__c != null ? 0.00 : ( oppObj.New_Rent__c / 12 );
                    }else if( oppObj.New_Rent_Frequency__c == 'Quarterly' ){
                        newRentMonthly = oppObj.New_Rent__c != null ? 0.00 : ( oppObj.New_Rent__c / 4 );         
                            }else{
                                newRentMonthly = oppObj.New_Rent__c != null ? ( oppObj.New_Rent__c )  : 0.00;                  
                            }
            
            if( oppObj.Current_Rent_Frequency__c == 'Yearly' ){
                currentRentMonthly = oppObj.Current_Rent__c != null ? 0.00 : ( oppObj.Current_Rent__c / 12 );
                    }else if( oppObj.Current_Rent_Frequency__c == 'Quarterly' ){
                        currentRentMonthly = oppObj.Current_Rent__c != null ? 0.00 : ( oppObj.Current_Rent__c / 4 );         
                            }else{
                                currentRentMonthly = oppObj.Current_Rent__c != null ?  ( oppObj.Current_Rent__c ) : 0.00;                  
                            }
            
            rentIncrease =  newRentMonthly -currentRentMonthly   ;
        
        
        return rentIncrease; 
    }
}
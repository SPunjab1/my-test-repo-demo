/*********************************************************************************************
@author     : IBM
@date       : 21 Aug 2020
@description: Test class for ScheduleReprocessBatch 
@Version    : 1.0           
**********************************************************************************************/
@isTest
public class ScheduleReprocessBatchTest {
    
    @isTest
    static void executeTest(){
        
        String CRON_EXP = '0 0 * * * ?'; 
        ScheduleReprocessBatch sch = new ScheduleReprocessBatch(); 
        system.schedule('Hourly Example Batch Schedule NeuStartoATOMSUpdateSchedular', CRON_EXP, sch);
    }
}
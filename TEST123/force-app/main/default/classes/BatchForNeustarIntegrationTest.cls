/*********************************************************************************************
@author     : IBM
@date       : 20 July 2020
@description: Test class for Batch class BatchForNeustarIntegration
@Version    : 1.0           
**********************************************************************************************/
@isTest(SeeAllData = false)
public class BatchForNeustarIntegrationTest {
 
     static testMethod void testBatchForLogin(){
        TMobilePh2LoginDetailsHierarchy__c setting1 = new TMobilePh2LoginDetailsHierarchy__c();
        setting1.Neustar_Domain__c = 'Test';
        setting1.Neustar_Username__c = 'UOMUsername';
        setting1.Neustar_Password__c = 'test';
        setting1.Neustar_End_Point_URL__c ='www.testurl.com';
        insert setting1;
        
        SwitchAPICallOuts__c setting = new SwitchAPICallOuts__c();
        setting.Execute_API_Call__c = true;
        setting.Name = 'Create Purchase Order';
        insert setting;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        caseList = TestDataFactoryOMph2.createCase('AAV CIR Retest',1,recTypeId);
        caseList[0].OM_Integration_Status__c = 'ASR Success';
        insert caseList;
        List<Purchase_Order__c> poList= new List<Purchase_Order__c>();
        poList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',3,caseList[0].Id);
        poList[0].API_Status__c='';
        poList[0].DDD__c=Date.valueOf('2020-03-12');
        insert poList;
        List<Circuit__c>cList=new List<Circuit__c>();
        cList=TestDataFactoryOMph2.createCircuitRecords(caseList[0].Id,1);
        insert clist;
        List<ASR__c>aList=new List<ASR__c>();
        aList=TestDataFactoryOMph2.createASRRecords(caseList[0].Id,poList[0].Id,cList[0].id,1);
        aList[0].Circuit_Category__c='IUB';
        aList[0].Bandwidth__c = 'Ethernet';
        aList[0].AAV_Type__c = 'Ethernet';
        aList[0].API_Status__c='Successful';
        insert alist;
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        BatchLoginToNeustarAPI batchLogin = new BatchLoginToNeustarAPI();
        Database.executeBatch(batchLogin,1);
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForCreateOrder());
        BatchForNeustarIntegration batch = new BatchForNeustarIntegration();
        Database.executeBatch(batch,1);
        Integration_Log__c intLog=new Integration_Log__c();
        intLog=NeustarIntegrationAPIHelper.createIntegrationLog(intLog,'test','Success','test','response','Create Purchase Order','test','test','DML Error');
        System.assertNotEquals(intLog,null);
         Test.stopTest();

    }
    
     static testMethod void testBatchForLoginCreatePOPositiveCase(){
        TMobilePh2LoginDetailsHierarchy__c setting1 = new TMobilePh2LoginDetailsHierarchy__c();
        setting1.Neustar_Domain__c = 'Test';
        setting1.Neustar_Username__c = 'UOMUsername';
        setting1.Neustar_Password__c = 'test';
        setting1.Neustar_End_Point_URL__c ='www.testurl.com';
        insert setting1;
        
        SwitchAPICallOuts__c setting = new SwitchAPICallOuts__c();
        setting.Execute_API_Call__c = true;
        setting.Name = 'Create Purchase Order';
        insert setting;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        caseList = TestDataFactoryOMph2.createCase('AAV CIR Retest',1,recTypeId);
        caseList[0].OM_Integration_Status__c = 'ASR Success';
        insert caseList;
        List<Purchase_Order__c> poList= new List<Purchase_Order__c>();
        poList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',3,caseList[0].Id);
        poList[0].API_Status__c='Successful';
        poList[0].DDD__c=Date.valueOf('2020-03-12');
        insert poList;
        List<Circuit__c>cList=new List<Circuit__c>();
        cList=TestDataFactoryOMph2.createCircuitRecords(caseList[0].Id,1);
        insert clist;
        List<ASR__c>aList=new List<ASR__c>();
        aList=TestDataFactoryOMph2.createASRRecords(caseList[0].Id,poList[0].Id,cList[0].id,1);
        aList[0].Circuit_Category__c='IUB';
        aList[0].Bandwidth__c = 'Ethernet';
        aList[0].AAV_Type__c = 'Ethernet';
        aList[0].API_Status__c='Successful';
        insert alist;
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        BatchLoginToNeustarAPI batchLogin = new BatchLoginToNeustarAPI();
        Database.executeBatch(batchLogin,1);
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForCreateOrder());
        BatchForNeustarIntegration batch = new BatchForNeustarIntegration();
        Database.executeBatch(batch,1);
        poList[0].API_Status__c='Success';
        List<Integration_Log__c> logList = new List<Integration_Log__c>();
        NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout wrapper = new NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout();
        wrapper.poToBeUpdated = poList;
        wrapper.logsToBeUpserted = logList;
        NeustarIntegrationAPIHelper.performCreatePOrderDMLs(wrapper);
         System.assertNotEquals(wrapper, null);
		Test.stopTest();

    }

}
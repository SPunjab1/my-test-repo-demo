/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ReopenTaskHandlerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 07.06.2019
*  @Description      : Test class for ReopenTaskHandler
**********************************************************************************************************************
**********************************************************************************************************************/
@IsTest(SeeAllData=false)
Public class ReopenTaskHandlerTest{
    
/*************************************************************
*@description : Test Method for ReopenTaskNew 
*************************************************************/   
    static  testmethod void runTestReopenTaskNew() {           
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Id> parentId=new List<Id>();
        List<Case> taskList=new List<case>();
        List<String> idList=new List<String>();
        String WorkflowTmpName='AAV CIR Retest';
        integer numOfcases=1;
        caseList= TestDataFactory.createCase(WorkflowTmpName,numOfcases,recTypeId);
        Insert(caseList); 
        for(Case c:caseList){
            parentId.add(c.id);
        }
        taskList=[select id,status from Case where parentid =: parentId AND Task_Number__c=1];
        ReopenTaskHandler.findTaskNumber(idList);
        tasklist[0].status='Closed';
        update taskList;
        idList.add(tasklist[0].id);
        Test.startTest();
        ReopenTaskHandler.findTaskNumber(idList);
        ReopenTaskHandler.ReopenTaskNew(idList);
        Test.stopTest();
        Case cObj = [SELECT Id, status FROM Case WHERE Id = :tasklist[0].id];
        System.assertEquals(cObj.status, 'New'); 
    }
}
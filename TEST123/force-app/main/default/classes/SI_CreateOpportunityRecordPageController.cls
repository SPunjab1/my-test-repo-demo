/***********************************************************************************
@author      : IBM
@date        : 09.06.2020
@description : Class to create opportunity record from case record page button click.
@Version     : 1.0
************************************************************************************/
public with sharing class SI_CreateOpportunityRecordPageController {   
    //Fetching Case record type values
    String strBackhaulRT = System.Label.SI_Opp_Backhaul_Contract_Case_RT; 
    String strVICCRT = System.Label.SI_Opp_VICC_Case_RT;
    String strSmlCelRT = System.Label.SI_CaseSmallCellRT;
    String strMLART = System.Label.SI_CaseMLART; 
    String strCaseSmlCellRT = System.Label.SI_CaseSmallCellDesRT; 
    //Fetching opportunity record type names
    Id strOppBackhaulRT= SI_UtilityClass.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_OpportunityRT.split(';')[0]);   
    Id strOppVICCRetRT= SI_UtilityClass.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_OpportunityRT.split(';')[3]);    
    Id strOppSmlCellRT= SI_UtilityClass.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_OpportunityRT.split(';')[1]);   
    Id strOppMLART= SI_UtilityClass.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_OpportunityRT.split(';')[2]); 
    PageReference ref;
    Opportunity oppRecord;
    
    /**
    * @description variable storing case id
    */
    public Id caseId {get;set;}
    /**
    * @description variable storing the case record
    */
    public Case caseRecord {get;set;}
    /**
    * @description variable to handle error 
    */
    public Boolean hasError {get;set;}
    /**
    * @description variable displaying the error message
    */
    public String errorMessage {get;set;}
    
    /**********************************************************************************
    * @description Constructor fetching the case record
    * @param controller - fetching all the standard object records
    **********************************************************************************/
    public SI_CreateOpportunityRecordPageController(ApexPages.StandardController controller) {
        hasError = false;
        caseRecord =  (Case) controller.getRecord();
        caseId = caseRecord.Id;
    }
    
    /*********************************************************************************
    * @description Method that is called from the VisualForce page action attribute
    * @return      PageReference
    **********************************************************************************/
    public PageReference createOpportunityRecord() {
        
        if(String.isNotBlank(caseId)) {
            try {
                UserRecordAccess recordAccessDetails = [SELECT RecordId, HasEditAccess
                                                        FROM UserRecordAccess 
                                                        WHERE UserId = :UserInfo.getUserId()
                                                        AND RecordId = :caseId];
                if(recordAccessDetails.HasEditAccess == false) {
                    hasError = true;
                    errorMessage = Label.SI_CreateOpportunityAccessError;
                    return null;
                }
            } catch (Exception ex) {
                hasError = true;
                errorMessage = 'Some Error occured. Please contact your administrator.';
                System.debug(LoggingLevel.ERROR, 'Exception in createOpportunityRecord: ' + ex.getMessage());
                return null;
            }
        }
        //fetching case record values 
        List<Case> caseQuery = [SELECT AccountId, OwnerId, Owner.type, CaseNumber, Type, Description, Status,SI_Market__c, 
                                Opportunity__c, RecordTypeId, RecordType.DeveloperName, ContactId, Selected_Vendor__c,
                                Selected_Vendor__r.Name, Location_IDs__r.Name, Project_Type__c, CreatedDate, Region__c,SI_Region__c 
                                FROM Case 
                                WHERE Id =:caseId];
        
        if((!caseQuery.isEmpty() && String.isBlank(caseQuery[0].Opportunity__c)) 
           && caseQuery[0].OwnerID.getsobjecttype() == User.sobjecttype) {
            String recTypeName = caseQuery[0].RecordType.DeveloperName;
            oppRecord = new Opportunity();   
            //mapping opportunity record type on the basis of case record type
            if(strBackhaulRT.contains(recTypeName)) {
                oppRecord.RecordTypeId = strOppBackhaulRT;   
            } else if(strCaseSmlCellRT.equalsIgnoreCase(recTypeName)) {
                oppRecord.RecordTypeId = strOppBackhaulRT;     
            } else if(strVICCRT.contains(recTypeName)) {
                oppRecord.RecordTypeId = strOppVICCRetRT;   
            } else if(strSmlCelRT.equalsIgnoreCase(recTypeName)) {
                oppRecord.RecordTypeId = strOppSmlCellRT;     
            } else if(strMLART.contains(recTypeName)) {
                oppRecord.RecordTypeId = strOppMLART;     
            }
            
            //changing the case created date format
            DateTime caseCreatedDt = DateTime.newInstance(caseQuery[0].createdDate.year(), 
                                                          caseQuery[0].createdDate.month(), 
                                                          caseQuery[0].createdDate.day());
            String caseFormattedDt = caseCreatedDt.format('M/d/YY');
            //mapping new opportunity record fields
            oppRecord.Name = caseQuery[0].Selected_Vendor__r.Name + ' - ' + caseQuery[0].Type + ' - ' + caseFormattedDt;
            oppRecord.StageName = System.Label.SI_OppStageDefaultValue;
            oppRecord.CloseDate = System.today().addDays(30);   
            oppRecord.Intake_Request_Submitter__c = caseQuery[0].ContactId; 
            oppRecord.Type = caseQuery[0].Type;
            oppRecord.AccountId = caseQuery[0].Selected_Vendor__c; 
            oppRecord.OwnerId = caseQuery[0].OwnerId;
            oppRecord.TPE__c = caseQuery[0].CaseNumber;
            oppRecord.Region__c = caseQuery[0].Region__c;
               if(String.isNotBlank(caseQuery[0].SI_Market__c) && String.isNotBlank(caseQuery[0].SI_Region__c) && 
                  caseQuery[0].SI_Region__c!=Label.SI_Case_Region && caseQuery[0].Region__c!=Label.SI_Case_Region){
                   oppRecord.Market_s__c = caseQuery[0].SI_Market__c;
               }
            //checking if selected vendor field is not blank
            if(String.isNotBlank(caseQuery[0].Selected_Vendor__c)) {
                try {
                    //inserting opportunity record
                    SI_UtilityClass.callDMLOperationsSingleRecord(oppRecord,Label.SI_DML_Operations.split(';')[0]);
                    List<contentDocumentLink> cdlink1 = new List<contentDocumentLink>();
                    for(contentDocumentLink cdLink : [SELECT LinkedEntityid, ContentDocumentid 
                                                      FROM contentDocumentLink 
                                                      WHERE LinkedEntityid = :caseId]) {
                        ContentDocumentLink cdl = New ContentDocumentLink();
                        cdl.LinkedEntityId = oppRecord.id;
                        cdl.ContentDocumentId = cdLink.ContentDocumentid;
                        cdl.shareType = 'V';
                        cdlink1.add(cdl);
                    }
                    SI_UtilityClass.callDMLOperations(cdlink1,Label.SI_DML_Operations.split(';')[0]);
                    if(String.isNotBlank(oppRecord.id)) {
                        caseQuery[0].Opportunity__c = oppRecord.id;
                        caseQuery[0].Opportunity_Stage__c = oppRecord.StageName;
                        //updating newly created opportunity to case record
                        SI_UtilityClass.callDMLOperationsSingleRecord(caseQuery[0],
                                                                      Label.SI_DML_Operations.split(';')[1]);   
                    }
                } catch(DmlException ex) {
                    System.debug(LoggingLevel.Error, 'Exception: ' + ex.getMessage());
                }
                //redirecting to newly created opportunity record
                ref = new PageReference('/'+oppRecord.Id);       
                ref.setRedirect(true);
            } else {
                //showing error if selected vendor is blank
                hasError=true;
                errorMessage = Label.SI_CreateOppValidationMessage;
            }   
        } else if(!caseQuery.isEmpty() && caseQuery[0].owner.type == 'Queue') {
            //showing error if case owner is Queue
            hasError=true;
            errorMessage = Label.SI_QueueAssignedError;
        } else {
            //showing error if opportunity is already present
            hasError=true;
            errorMessage = Label.SI_OpportunityPresentError;
        }
        return ref;
    }
    
    /**************************************************************************
    * @description To navigate back to case record page
    * @return      PageReference
    **************************************************************************/
    public pageReference backToCaseRecord() {
        //Intialize page reference variable
        PageReference pageRef;
        try {
            //check if case id variable having value 
            if(String.isNotBlank(this.caseId)) {
                //set pageReference to called case record id
                pageRef = new PageReference('/' + caseId);
            } else {
                //set to case object record list page
                pageRef = new PageReference('/500');
            }
        } catch(Exception ex) {
            System.debug(LoggingLevel.Error, 'Exception: ' + ex.getMessage());
        }
        return pageRef;
    }
}
/*********************************************************************************************************************
*  @Class            : OMPh2_BulkUploadCtrl
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 28.07.2020
*  @Description      : Apex class of OMPH2 Bulk upload lightning component
**********************************************************************************************************************/
public with sharing class OMPh2_BulkUploadCtrl {
    
    /***************************************************************************************
* @Description :Method to Check duplicateRecord In database based on PON number
***************************************************************************************/  
    @AuraEnabled
    public static List<String> checkDuplicateRecords(Map<String,Map<String,String>> fileContentMap){
        
        if(fileContentMap == null || fileContentMap.isEmpty()){
            return null;
        }
        
        List<String> duplicatePONumber = new List<String>();
        
        for(Purchase_Order__c po : [select Id, PON__c from Purchase_Order__c where PON__c In :fileContentMap.keySet()]){
            duplicatePONumber.add(po.PON__c);
        }
        
        if(duplicatePONumber.isEmpty())
            return null;
        else
            return duplicatePONumber;
    }
    
    /***************************************************************************************
* @Description :Method to Check duplicateRecord In database based on ASR PON number
***************************************************************************************/  
    @AuraEnabled
    public static List<String> checkDuplicateASRPONnum(List<String> asrPONNumList){
        
        if(asrPONNumList == null || asrPONNumList.isEmpty()){
            return null;
        }
        List<String> duplicateASRPONumber = new List<String>();
        for(ASR__c asr : [select Id,PON__c from ASR__c where PON__c In :asrPONNumList]){
            duplicateASRPONumber.add(asr.PON__c);
        }
        
        if(duplicateASRPONumber.isEmpty())
            return null;
        else
            return duplicateASRPONumber;
    }
    
    /***************************************************************************************
* @Description :Method to validate Work order task Numbers Valid or not.
***************************************************************************************/ 
    @AuraEnabled
    public static Map<String,Map<String,Case>> validateWorkOrderTask(List<String> wOTaskLst){
        
        Map<String,Map<String,Case>> workOrderTaskMap = new Map<String,Map<String,Case>>{'Valid' => new Map<String,Case>(), 'Invalid' => new Map<String,Case>()};        
            if( wOTaskLst == null || wOTaskLst.isEmpty()){
                return workOrderTaskMap;
            }
        
        ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();
        Map<String,Case> validMap = new Map<String,Case>();
        Map<String,Case> invalidMap = new Map<String,Case>();
        
        Map<String,Case> idToCaseMap = new Map<String,Case>();
        for(Case cse : [select Id,CaseNumber,OM_Integration_Status__c ,ParentId from Case where CaseNumber In :wOTaskLst and recordTypeId = :workOrderTaskRecordType]){
            validMap.put(cse.CaseNumber,cse);
        }
        
        for(String woId : wOTaskLst){
            if(!validMap.containsKey(woId)){
                invalidMap.put(woId,NULL);
            }
        }
        
        workOrderTaskMap.put('Valid', validMap);
        workOrderTaskMap.put('Invalid', invalidMap);
        
        
        
        return workOrderTaskMap;
    }
    
    /***************************************************************************************
* @Description :Method to validate Location id  Valid or not.
***************************************************************************************/ 
    @AuraEnabled
    public static Map<String,Map<String,Location_ID__c>> validateLocationId(List<String> locationLst){
        
        Map<String,Map<String,Location_ID__c>> locationIDMap = new Map<String,Map<String,Location_ID__c>>{'Valid' => new Map<String,Location_ID__c>(), 'Invalid' => new Map<String,Location_ID__c>()};
            if(locationLst == null || locationLst.isEmpty()){
                return locationIDMap;
            }
        
        Map<String,Location_ID__c> validMap = new Map<String,Location_ID__c>();
        Map<String,Location_ID__c> invalidMap = new Map<String,Location_ID__c>();
        
        for(Location_ID__c loc : [select Id,Location_ID__c,Location__c from Location_ID__c where Location_ID__c In :locationLst]){
            
            validMap.put(loc.Location_ID__c,loc);   
        }
        for(String locId : locationLst){
            if(!validMap.containsKey(locId)){
                invalidMap.put(locId,NULL);
            }
        }
        locationIDMap.put('Valid', validMap);
        locationIDMap.put('Invalid', invalidMap);
        
        
        return locationIDMap;
    }
    
    /***************************************************************************************
* @Description :Create Records for Purchase Order,ASR and Circuit 
***************************************************************************************/
    @AuraEnabled
    public static string createRecords(Map<String,Map<String,String>> poMap, Map<String,Map<String,String>> circuitMap,
                                       Map<String,Map<String,String>> asrMap, Map<String,Case> validWoTaskMap,Map<String,Location_ID__c>validLocationMap,
                                       Map<String,String>woTaskStrtIntegrationMap){
      List<Purchase_Order__c> poLstToInsert =  new List<Purchase_Order__c>(); 
      Map<string,Circuit__c> circuitLstToInsert =  new Map<String,Circuit__c>();
      List<ASR__c> asrLstToInsert =  new List<ASR__c>();
      Map<String,Purchase_Order__c> ponToPORecMap = new Map<String,Purchase_Order__c>();
      List<Case>caseToUpdateList = new List<Case>();
      String response;
      Savepoint sp;
                                      
      if(poMap !=null && poMap.keySet().size()>0){
             poLstToInsert = createPORecords(poMap,validWoTaskMap);
             sp = Database.setSavepoint();
          if(poLstToInsert !=null && poLstToInsert.size()>0){
             response=methodToInsert(poLstToInsert);// insert poLstToInsert
             for(Purchase_Order__c po: poLstToInsert){
                 ponToPORecMap.put(po.PON__c,po);
              }
          }
          else if (poLstToInsert == null){
               response ='Please Provide correct date in format of (YYYY-MM-DD)';
          }
     
      if(circuitMap !=null && circuitMap.keySet().size()>0 && response == 'success'){
             circuitLstToInsert = createCircuitRecords(circuitMap,ponToPORecMap,validLocationMap); 
             response=methodToInsert(circuitLstToInsert.values());//insert circuitLstToInsert.values()
            
               }
      if(asrMap !=null && asrMap.keySet().size()>0 && response == 'success'){
             asrLstToInsert = createASRRecords(asrMap,ponToPORecMap,circuitLstToInsert);
             if(asrLstToInsert!=null){
             response=methodToInsert(asrLstToInsert);//insert asrLstToInsert;
             }
             else if(asrLstToInsert==null){
            response ='Please Provide correct date in format of (YYYY-MM-DD)';
             }
         } 
          if(woTaskStrtIntegrationMap !=null && woTaskStrtIntegrationMap.keyset().size()>0 && response == 'success' ){
              for(String wo:woTaskStrtIntegrationMap.keyset()){
                  if(validWoTaskMap.containsKey(wo))
                  {
                      if(woTaskStrtIntegrationMap.get(wo)=='Y'|| woTaskStrtIntegrationMap.get(wo) == 'y')
                      {
                          Case caseToupdate= new case();
                          caseToupdate=validWoTaskMap.get(wo);
                          caseToupdate.OM_Integration_Status__c ='Start Integration';
                          caseToUpdateList.add(caseToupdate);
                      }
                      else if(woTaskStrtIntegrationMap.get(wo)=='N'|| woTaskStrtIntegrationMap.get(wo) == 'n')
                      {
                          Case caseToupdate= new case();
                          caseToupdate=validWoTaskMap.get(wo);
                          caseToupdate.OM_Integration_Status__c ='';
                          caseToUpdateList.add(caseToupdate);
                      }
                  }
              }
          }    
         
          if(caseToUpdateList !=null && caseToUpdateList.size()>0)
          {
              try{
                  update caseToUpdateList;
              }catch(Exception ex)
              {
                  response=ex.getMessage();
              }
          }
                                               
     } 
      
      if(response!='success'  )
       Database.rollback(sp);
       return response;
  }
    
    /***************************************************************************************
* @Description :Create Records for Purchase Order 
***************************************************************************************/
    
    public static List<Purchase_Order__c> createPORecords(Map<String,Map<String,String>> poMap,Map<String,Case> validWoTaskMap){
        List<Purchase_Order__c> poLstToInsert =  new List<Purchase_Order__c>();
        for(String ponNumber : poMap.keySet()){
            if(poMap.get(ponNumber)!=null && poMap.get(ponNumber).keySet().size()>0){
                Map<String,String> recDetail = poMap.get(ponNumber);
                Purchase_Order__c poRec = new Purchase_Order__c(); 
                poRec.Name = recDetail.get('PON');
                poRec.PON__c  = recDetail.get('PON');
                poRec.Product_Name__c = recDetail.get('ProductCatalogName');
                poRec.Trading_Partner__c = recDetail.get('TradingPartner');
                poRec.Activity__c = recDetail.get('Activity'); 
                if(recDetail.get('DDD')!=null && recDetail.get('DDD')!=''){
                  try{
                  poRec.DDD__c=Date.valueOf(recDetail.get('DDD'));
                    }catch(Exception ex){
                        return null;}
                }
                if(validWoTaskMap.containsKey(recDetail.get('WorkOrderTask'))){
                    poRec.Work_Order_Task__c =  validWoTaskMap.get(recDetail.get('WorkOrderTask')).Id; 
                    poRec.Work_Order__c =  validWoTaskMap.get(recDetail.get('WorkOrderTask')).ParentId;   
                }
                poLstToInsert.add(poRec);   
            }
        }
        return poLstToInsert;
    }
    
    /***************************************************************************************
* @Description :Create Records for  Circuit 
***************************************************************************************/
    public static Map<String,Circuit__c> createCircuitRecords(Map<String,Map<String,String>> circuitMap,Map<String,Purchase_Order__c> ponToPORecMap,Map<String,Location_ID__c>validLocationMap){
        Map<String,circuit__c> circuitLstMap= new Map<String,circuit__c>();
        for(String ponNumber : circuitMap.keySet()){
            if(circuitMap.get(ponNumber)!=null && circuitMap.get(ponNumber).keySet().size()>0){
                Map<String,String> recDetail = circuitMap.get(ponNumber);
                for(integer i=1;i<=Integer.valueof(recDetail.get('count'));i++)
                {
                    Circuit__c circuitRec = new Circuit__c(); 
                    circuitRec.Circuit_Type__c = recDetail.get('CircuitType'+i);
                    circuitRec.Name  = recDetail.get('CircuitId'+i);
                    if(recDetail.get('ControllingSite'+i)!=null && recDetail.get('ControllingSite'+i)!=''){
                    circuitRec.Controlling_Site__c  = recDetail.get('ControllingSite'+i);
                    }
                    if(recDetail.get('OrderingGuideId'+i)!=null && recDetail.get('OrderingGuideId'+i)!=''){
                    circuitRec.Ordering_Guide_ID__c  = recDetail.get('OrderingGuideId'+i);
                    }
                    //adding new fields 
                    if(recDetail.get('BillingCircuitId'+i)!=null && recDetail.get('BillingCircuitId'+i)!=''){
                    circuitRec.Billing_Circuit_Id__c=recDetail.get('BillingCircuitId'+i);
                    }
                    if(recDetail.get('ASiteId'+i)!=null && recDetail.get('ASiteId'+i)!='' && validLocationMap.containsKey(recDetail.get('ASiteId'+i))){
                        circuitRec.A_Site_ID__c=validLocationMap.get(recDetail.get('ASiteId'+i)).Id;}
                    if(recDetail.get('ZSiteId'+i)!=null && recDetail.get('ZSiteId'+i)!='' && validLocationMap.containsKey(recDetail.get('ZSiteId'+i))){
                        circuitRec.Z_Site_ID__c=validLocationMap.get(recDetail.get('ZSiteId'+i)).Id;}
                    if(recDetail.get('Bandwidth'+i)!=null && recDetail.get('Bandwidth'+i)!=''){
                    circuitRec.Bandwidth__c=recDetail.get('Bandwidth'+i);
                    }
                    if(recDetail.get('UserCircuitId'+i)!=null && recDetail.get('UserCircuitId'+i)!=''){
                    circuitRec.User_Ciruit_ID__c=recDetail.get('UserCircuitId'+i);
                    }
                    else{
                        circuitRec.User_Ciruit_ID__c=recDetail.get('CircuitId'+i);}
                    if(recDetail.get('BAN'+i)!=null && recDetail.get('BAN'+i)!=''){
                    circuitRec.BAN__c=recDetail.get('BAN'+i);
                    }
                    if(recDetail.get('Category'+i)!=null && recDetail.get('Category'+i)!=''){
                    circuitRec.Category__c=recDetail.get('Category'+i);
                    }
                    circuitRec.Is_Meet_Point__c=boolean.valueOf(recDetail.get('IsMeetingPoint'+i));
                    circuitRec.LOA_Provided__c=boolean.valueOf(recDetail.get('LOAProvided'+i));
                    if(ponToPORecMap.containsKey(ponNumber)){
                        circuitRec.Purchase_Order_Id__c = ponToPORecMap.get(ponNumber).Id;  
                        circuitRec.Case__c=ponToPORecMap.get(ponNumber).Work_Order_Task__c;
                    }
                    circuitLstMap.put(ponNumber+'Circuit'+i,circuitRec);
                    
                }
                
            }
        }
        
        return circuitLstMap;
    }
    
    /***************************************************************************************
* @Description :Create Records for ASR 
***************************************************************************************/
    public static List<ASR__c> createASRRecords(Map<String,Map<String,String>> asrMap,Map<String,Purchase_Order__c> ponToPORecMap,Map<String,Circuit__c> poIdToCircuitRecMap){
        List<ASR__c> asrLstToInsert =  new List<ASR__c>();
        
        for(String ponNumber : asrMap.keySet()){
            if(asrMap.get(ponNumber)!=null && asrMap.get(ponNumber).keySet().size()>0){
                Map<String,String> recDetail = asrMap.get(ponNumber);
                
                for(integer i=1;i<=Integer.valueof(recDetail.get('count'));i++){
                    ASR__c asrRec = new ASR__c();
                    if(recDetail.get('ASRtype'+i) == 'In Service Change')
                    {
                        asrRec.ASR_Type__c='In Service';
                    }
                    else{
                    asrRec.ASR_Type__c  = recDetail.get('ASRtype'+i);
                    }
                    asrRec.PON__c=recDetail.get('ASRPON'+i);
                    if(recDetail.get('EthernetCircuitType'+i)!=null && recDetail.get('EthernetCircuitType'+i)!=''){
                    asrRec.SubService_Type__c=recDetail.get('EthernetCircuitType'+i);  
                    }
                    //adding new fields
                    try{
                        asrRec.Ordered_Date__c=Date.ValueOf(recDetail.get('OrderedDate'+i));
                        //asrRec.Deliver__c= Date.ValueOf(recDetail.get('EBD'+i));  
                       }
                    catch(System.TypeException e){
                        string errorMsg='';
                        errorMsg=e.getMessage();
                        if(errorMsg.contains('Invalid date:'))
                            errorMsg='Please Provide correct date in format of (YYYY-MM-DD) ';
                        return null;
                    }
                    
                    if(recDetail.get('VLAN'+i)!=null && recDetail.get('VLAN'+i)!=''){
                    asrRec.VLAN__c=Decimal.valueOf(recDetail.get('VLAN'+i));
                    }
                    if(recDetail.get('CIR'+i)!=null && recDetail.get('CIR'+i)!=''){
                    asrRec.CIR__c=Decimal.valueOf(recDetail.get('CIR'+i));
                    }
                    if(recDetail.get('ForecastVersion'+i)!=null && recDetail.get('ForecastVersion'+i)!=''){
                    asrRec.Forecast_Version__c=recDetail.get('ForecastVersion'+i);
                    }
                    
                    if(recDetail.get('VLANTWO'+i)!=null && recDetail.get('VLANTWO'+i)!=''){
                     asrRec.VLAN_2__c=Decimal.valueOf(recDetail.get('VLANTWO'+i)) ;   
                    }
                    if(recDetail.get('ChangeType'+i)!=null && recDetail.get('ChangeType'+i)!=''){
                    asrRec.Change_Type__c=recDetail.get('ChangeType'+i);
                    }
                    if(recDetail.get('ChangeValue'+i)!=null && recDetail.get('ChangeValue'+i)!=''){
                    asrRec.Change_Value__c=recDetail.get('ChangeValue'+i);
                    }
                    if(recDetail.get('EthernetTechonlogyType'+i)!=null && recDetail.get('EthernetTechonlogyType'+i)!=''){
                    asrRec.Ethernet_Technology_Type__c=recDetail.get('EthernetTechonlogyType'+i);
                    }
                    asrRec.Order_Type_in_Neustar__c=recDetail.get('productTypeToNeustar'+i);
                   // asrRec.Ethernet_Circuit_Type__c=recDetail.get('EthernetCircuitType'+i);
                    if(recDetail.get('TestCIR'+i)!=null && recDetail.get('TestCIR'+i)!=''){
                    asrRec.Test_CIR__c=Decimal.valueOf(recDetail.get('TestCIR'+i));
                    }
                    if(recDetail.get('OrderNumber'+i)!=null && recDetail.get('OrderNumber'+i)!=''){
                    asrRec.Order_Number__c=recDetail.get('OrderNumber'+i);
                    }
                    asrRec.Order_Destination__c=recDetail.get('OrderDestination'+i);
                    if(recDetail.get('ServiceType'+i)!=null && recDetail.get('ServiceType'+i)!=''){
                    asrRec.Service_Type__c=recDetail.get('ServiceType'+i);
                    }
                    if(recDetail.get('TragetDownloadSpeed'+i)!=null && recDetail.get('TragetDownloadSpeed'+i)!=''){
                    asrRec.Target_Download_Speed__c=Decimal.valueOf(recDetail.get('TragetDownloadSpeed'+i));
                    }
                     //asrRec.AAV__c=recDetail.get('AAV'+i);
                    //asrRec.VLAN_UNI__c=Decimal.valueOf(recDetail.get('VLANUNI'+i));
                    
                    
                    if(ponToPORecMap.containsKey(ponNumber)){
                        asrRec.Purchase_Order_Id__c = ponToPORecMap.get(ponNumber).Id;
                        asrRec.Case__c=ponToPORecMap.get(ponNumber).Work_Order_Task__c;
                        if(poIdToCircuitRecMap.containsKey(ponNumber+'Circuit'+i)){
                            asrRec.Circuit_Id__c = poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Id;
                            if(poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Category__c != null && poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Category__c != ''){
                                asrRec.Circuit_Category__c=poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Category__c;
                            if(poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Bandwidth__c != null && poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Bandwidth__c !=''){
                               asrRec.Bandwidth__c=poIdToCircuitRecMap.get(ponNumber+'Circuit'+i).Bandwidth__c;
                                  if(recDetail.get('AAV'+i)!=null && recDetail.get('AAV'+i) !=''){
                                    asrRec.AAV_Type__c=recDetail.get('AAV'+i);}
                             }
                           }
                            else{
                                asrRec.Circuit_Category__c='N/A';
                                asrRec.Bandwidth__c='N/A';
                                asrRec.AAV_Type__c='Not Applicable';
                            }
                        }
                    }
                    asrLstToInsert.add(asrRec); 
                }
            }
        }
       
        return asrLstToInsert;
    }
    
    /***************************************************************************************
* @Description :Generic method for dml insert 
***************************************************************************************/
    public static String methodToInsert(List<sobject>listToInsert)
    {
        String errorMsg='';
        Schema.SObjectType sObjectType = listToInsert.getSObjectType();
        
        if(listToInsert!=null && listToInsert.size()>0){
            try{
                insert listToInsert;
            }
            catch(DmlException e) {
                System.debug('DmlException caught: ' + e.getMessage()); 
                errorMsg=e.getMessage();
                if(errorMsg.contains('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST'))
                {
                    integer startIndex=errorMsg.indexOf('[');
                    integer endIndex=errorMsg.indexOf(']');
                    string errorField=errorMsg.substring(startindex+1,endindex);
                    DescribeSObjectResult describeResult = sObjectType.getDescribe();
                    Map<String, Schema.SObjectField> desrcibedFields = describeResult.fields.getMap();
                    Schema.SObjectField field = desrcibedFields.get(errorField);
                    Schema.DescribeFieldResult f = field.getDescribe(); 
                    errorMsg='Please Provide correct value for picklist field :'+f.getLabel();
                }
            } catch(SObjectException e) {
                System.debug('SObjectException caught: ' + e.getMessage()); 
                errorMsg='There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again ';
            } catch(StringException e){
                System.debug('StringException caught: ' + e.getMessage()); 
                errorMsg=e.getMessage();
                if(errorMsg.contains('Invalid id')){
                    errorMsg='Please Provide correct location Id for A-SiteId/Z-SiteId';
                }
            } catch(System.TypeException e){
                errorMsg=e.getMessage();
                if(errorMsg.contains('Invalid date:'))
                    errorMsg='Please Provide correct date in format of (YYYY-MM-DD) ';
            }
            catch(Exception e) {
                system.debug('exception:'+e);
                errorMsg='There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again ';
            }
            
        }   
        if(errorMsg!='')
            return errorMsg;
        else 
            return 'success';
    }
}
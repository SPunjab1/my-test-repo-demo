/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : TriggerHandler
*  @Author           : Archana
*  @Version History  : 1.0
*  @Creation         : 09.06.2019
*  @Description      : Test class for TriggerHandler
**********************************************************************************************************************
**********************************************************************************************************************/
@IsTest(SeeAllData=FALSE)
Public class TriggerHandlerTest{
    
/*************************************************************
*@description : Test Method to check TestisDisabled
*************************************************************/   
    static testmethod void runTestisDisabledPositive() {           
        insert new TriggerManager__c(SetupOwnerId=UserInfo.getOrganizationId(), DisabledObjects__c = 'Case,Account',MuteAllTriggers__c = FALSE);
        Test.startTest();
        CaseTriggerHandler t = new CaseTriggerHandler();
        t.isDisabled();
        Test.stopTest();
    }
    static testmethod void runTestisDisabledNegative() {           
        insert new TriggerManager__c(SetupOwnerId=UserInfo.getOrganizationId(), DisabledObjects__c = 'Case',MuteAllTriggers__c = TRUE);
        Test.startTest();
        CaseTriggerHandler t = new CaseTriggerHandler();
        t.isDisabled();
        Test.stopTest();
    }
    static testmethod void runTestisDisabledException() {               
        insert new TriggerManager__c(SetupOwnerId=UserInfo.getOrganizationId(),DisabledObjects__c = 'TEST,TEST1',MuteAllTriggers__c = FALSE);
        CaseTriggerHandler t = new CaseTriggerHandler();
        t.isDisabled();
        
    }
}
public WithOut sharing class getLatestReleaseNote {
    
    @AuraEnabled
    public static Release_Note__c getLatestNotes(){
        Release_Note__c releaseNote = New Release_Note__c();
        Id loggedInuserId  = UserInfo.getUserId();
        System.debug('userId:'+ loggedInuserId);
        boolean flag = [select Hide_Release_Notes__C from User where Id=:loggedInuserId limit 1].Hide_Release_Notes__C;
        system.debug('flag:'+ flag );
        if(!flag){
            try{
                releaseNote = [
                    SELECT Id,Name, End_Date__c, Release_Notes__c, Release_Short_Description__c, Start_Date__c
                    FROM Release_Note__c where (Start_Date__c = today OR Start_Date__c < today) and POD_or_Community__c='Community'
                    and (End_Date__C = today OR End_Date__C > today) order by createddate desc  LIMIT 1];
            }
            catch(Exception ex){
                releaseNote = null;
            }
            
        }
        system.debug('record'+ releaseNote );
        return releaseNote;
    }
    @AuraEnabled
    public static void HideReleaseNotes(){
 
        Id loggedInuserId  = UserInfo.getUserId();
        System.debug('userId:'+ loggedInuserId);
        User tempUser = [select Hide_Release_Notes__C from User where Id=:loggedInuserId limit 1];
        system.debug('tempUser:'+ tempUser );
        tempUser.Hide_Release_Notes__C = true;
        update(tempUser);
    }
    
}
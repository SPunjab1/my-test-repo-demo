public with sharing class CloseCaseWithDependencyTasks {

    @InvocableMethod
    public static void closeChildCasesmtd(List<id> caseID) {

        List<Work_Order_Task_Dependency__c> dependencyTaskListtoUpdate = new List<Work_Order_Task_Dependency__c>();
        List<Case> caseListToUpdateAndClose = new List<Case>();

        List<Case> caseList = [SELECT Attachment_Required__c,Worklog_Required__c,Status,
                                    (SELECT ID,Invalid_Dependency__c FROM Work_Order_Task_Dependency1__r) 
                                    FROM Case
                                    WHERE ID = : caseID];
        if(! caseList.isEmpty()){
            for(Case caseObj: caseList){
                caseObj.Attachment_Required__c = false;
                caseObj.Worklog_Required__c = false;
                caseObj.Status = 'Closed';
                caseListToUpdateAndClose.add(caseObj);
                for(Work_Order_Task_Dependency__c testDependencyObj : caseObj.Work_Order_Task_Dependency1__r){
                    testDependencyObj.Invalid_Dependency__c = true;
                    dependencyTaskListtoUpdate.add(testDependencyObj);
                }
            }


            if(! dependencyTaskListtoUpdate.isEmpty()){
                update dependencyTaskListtoUpdate;
            }
            if(! caseListToUpdateAndClose.isEmpty()){
                update caseListToUpdateAndClose;
            }
        }

        
    }
}
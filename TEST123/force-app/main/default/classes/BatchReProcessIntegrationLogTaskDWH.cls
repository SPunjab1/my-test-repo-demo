/*********************************************************************************************
@author     : IBM
@date       : 20.06.19
@description: This class is re- processing the failed records from integration log table for DWH
@Version    : 1.0
**********************************************************************************************/

global class BatchReProcessIntegrationLogTaskDWH implements Database.Batchable<sObject>,Schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        //Query to fetch DWH Closed Task failed records from Integration logs
        String query = 'SELECT Id,Reprocess__c,Status__c, Response_Message__c, DWH_Task_Status__c, Task__c FROM Integration_Log__c WHERE API_Type__c = \'DWH Close Task\' AND Status__c = \'Failure\'';
        return Database.getQueryLocator(query);
    }
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())Database.executeBatch(new BatchReProcessIntegrationLogTaskDWH(), 5);
    }
    
    global void execute(Database.BatchableContext BC, List<Integration_Log__c> scope) {
        Map<Id, Integration_Log__c> taskLogMap = new Map<Id, Integration_Log__c>();
        List<Integration_Log__c> integrationLogsToUpdate  = new List<Integration_Log__c>();
        List<Case> caseLstToBeUpdated = new List<Case>();
      //Create map of task to integration log
        for(Integration_Log__c integrationLog: scope){
            taskLogMap.put(integrationLog.Task__c, integrationLog);
        }
        //update task status
        if(taskLogMap != Null && taskLogMap.size() > 0){
            for(Case cs : [Select Id, Status from Case where Id IN: taskLogMap.keySet()]){
                cs.Status = taskLogMap.get(cs.Id).DWH_Task_Status__c;
                caseLstToBeUpdated.add(cs);
            }
        }
        //Perform DML on case
        if(caseLstToBeUpdated != null && caseLstToBeUpdated.size() >0){
            List<Database.SaveResult> updateResults = database.update(caseLstToBeUpdated,false);
            //Update integration log if the status is updated successfully else update the log record
            for(Integer i=0;i<updateResults.size();i++){
                Integration_Log__c errorLog = new Integration_Log__c(Id = taskLogMap.get(caseLstToBeUpdated.get(i).Id).Id);
                if(updateResults.get(i).isSuccess()){
                    errorLog.Status__c = 'Success';
                    errorLog.Response_Message__c = 'Task Status Updated Successfully';
                }
                else if (!updateResults.get(i).isSuccess()){
                    // DML operation failed
                    String errorMsg = '';
                    for(Database.Error error : updateResults.get(i).getErrors()){errorMsg += error.getMessage() +'\n';
                    }
                    errorLog.Response_Message__c = errorMsg;errorLog.DWH_Task_Status__c = caseLstToBeUpdated.get(i).Status;errorLog.Status__c = 'Failure';
                 }
                 integrationLogsToUpdate .add(errorLog);
            }
            //update the log records
            if(integrationLogsToUpdate != null && integrationLogsToUpdate.size() > 0){
                database.update(integrationLogsToUpdate, false);
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}
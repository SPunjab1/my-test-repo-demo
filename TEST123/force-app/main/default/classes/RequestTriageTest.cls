@isTest
/*Modified: Aug 27 2020
 * Modified by: Llamar Tracey
 * Ticket/Story: PTSP 2902
 * Changes Description: since this was first implemented several changes have been made, namely this should only run for community user and 
 * sprint triaging logic was added. This meant that the misc user and the setup users had to be changed to run this test correctly.
 * 
 */
public class RequestTriageTest {
    
    @testSetup
    static void setup(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User exec = new User(LastName='Exec',
                             Alias='Exec',
                             Email='exec@PODDEVtest.com',
                             Username='lwrekermnflkrsmnlkkgnsrlfsnv@PODDEVtest.com',
                             ProfileId=p.Id,
                             EmailEncodingKey='UTF-8',
                             LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US',
                             TimeZoneSidKey='America/Los_Angeles');
        insert exec;
        
        
        p = [SELECT Id FROM Profile WHERE Name='Sourcing Manager']; 
        List<User> userList = new List<User>();
        for (Integer i=0;i<13;i++) {
            userList.add(new User(LastName='Test' + i,
                                  Alias='Test' + i,
                                  Email='test' + i + '@PODDEVtest.com',
                                  Username='testUser' + i + '@PODDEVtest.com',
                                  ProfileId=p.Id,
                                  Executive_Priority_Approver__c=exec.Id,
                                  EmailEncodingKey='UTF-8',
                                  LanguageLocaleKey='en_US', 
                                  LocaleSidKey='en_US',
                                  TimeZoneSidKey='America/Los_Angeles'));
        }
        insert userList;
       
        //running as different user
        Profile d = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        user u = new User(LastName='Sprint',
                          Alias='TestSp',
                          Email='test@sprint.com',
                          Username='testUser@PODDEVtest.com',
                          ProfileId=d.Id,
                          Executive_Priority_Approver__c=exec.Id,
                          EmailEncodingKey='UTF-8',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          TimeZoneSidKey='America/Los_Angeles');
        insert u;
        
        List<Project_Owner__c> pownList = new List<Project_Owner__c>();
        pownList.add(new Project_Owner__c(Name='Test OOS', Title__c='Test'));
        pownList.add(new Project_Owner__c(Name='Test NO OOS', Title__c='Test'));
        insert pownList;
        
        List<VP_Alignment__c> vpalList = new List<VP_Alignment__c>();
        //first Energy Callout
        vpalList.add(new VP_Alignment__c(VP__c=pownList[0].id,
                                         OOS_Alignment__c=false,
                                         All_Categories__c=false,
                                         Category__c='Energy',
                                         Priority__c=1,
                                         All_Sourcing_Project_Types__c=false,
                                         Sourcing_Project_Type__c='MSA',
                                         Sourcing_Manager__c=userList[1].id
                                        ));
        //second Energy Callout
        vpalList.add(new VP_Alignment__c(VP__c=pownList[0].id,
                                         OOS_Alignment__c=false,
                                         All_Categories__c=false,
                                         Category__c='Energy',
                                         Priority__c=2,
                                         All_Sourcing_Project_Types__c=false,
                                         Sourcing_Project_Type__c='Change Order',
                                         Sourcing_Manager__c=userList[2].id
                                        ));
        //first OOS alignment
        vpalList.add(new VP_Alignment__c(VP__c=pownList[0].id,
                                         OOS_Alignment__c=true,
                                         All_Categories__c=false,
                                         Category__c='Services;Field Services;Software;Hardware Products;Non-Disclosure Agreement',
                                         Priority__c=1,
                                         All_Sourcing_Project_Types__c=false,
                                         Sourcing_Project_Type__c='Order Form',
                                         Sourcing_Manager__c=userList[1].id
                                        ));
        //second OOS Alignment
        vpalList.add(new VP_Alignment__c(VP__c=pownList[0].id,
                                         OOS_Alignment__c=true,
                                         All_Categories__c=false,
                                         Category__c='Services;Field Services;Software;Hardware Products;Non-Disclosure Agreement',
                                         Priority__c=2,
                                         All_Sourcing_Project_Types__c=true,
                                         //Sourcing_Project_Type__c='Request for RFP/RFI',
                                         Sourcing_Manager__c=userList[2].id
                                        ));
        //first IN SCOPE vp alignment
        vpalList.add(new VP_Alignment__c(VP__c=pownList[1].id,
                                         OOS_Alignment__c=false,
                                         All_Categories__c=false,
                                         Category__c='Services;Field Services;Software;Hardware Products;Non-Disclosure Agreement',
                                         Priority__c=1,
                                         All_Sourcing_Project_Types__c=false,
                                         Sourcing_Project_Type__c='Request for RFP/RFI',
                                         Sourcing_Manager__c=userList[3].id
                                        ));
        //second IN SCOPE vp alignment
        vpalList.add(new VP_Alignment__c(VP__c=pownList[1].id,
                                         OOS_Alignment__c=false,
                                         All_Categories__c=false,
                                         Category__c='Services;Field Services;Software;Hardware Products;Non-Disclosure Agreement',
                                         Priority__c=2,
                                         All_Sourcing_Project_Types__c=true,
                                         //Sourcing_Project_Type__c='Request for RFP/RFI',
                                         Sourcing_Manager__c=userList[4].id
                                        ));
        insert vpalList;
        
        List<Account> acctList = new List<Account>();
        acctList.add(new Account(Name='Test OOS'));
        acctList.add(new Account(Name='Test NO OOS'));
        insert acctList;
        
        List<OOS_Supplier_Alignment__c> suppList = new List<OOS_Supplier_Alignment__c>();
        suppList.add(new OOS_Supplier_Alignment__c(Supplier__c=acctList[0].Id,
                                                   All_Sourcing_Project_Types__c=false,
                                                   Sourcing_Project_Type__c='MSA',
                                                   Sourcing_Manager__c=userList[1].Id));
        suppList.add(new OOS_Supplier_Alignment__c(Supplier__c=acctList[0].Id,
                                                   All_Sourcing_Project_Types__c=true,
                                                   //Sourcing_Project_Type__c='MSA',
                                                   Sourcing_Manager__c=userList[2].Id));
        insert suppList;
        
        List<OOS_Category_Alignment__c> oocaList = new List<OOS_Category_Alignment__c>();
        oocaList.add(new OOS_Category_Alignment__c(Name='Hardware Products',
                                                   Sourcing_Manager__c=userList[3].id));
        insert oocaList;
        
        List<Request_Triage_Route__c> routList = new List<Request_Triage_Route__c>();
        //Accenture Route (all Field Services)
        routList.add(new Request_Triage_Route__c(All_Categories__c=false,
                                                 Category__c='Field Services',
                                                 All_Types_of_Service__c=true,
                                                 //Type_of_Service__c='IT Services',
                                                 All_Sourcing_Project_Types__c=true,
                                                 //Sourcing_Project_Type__c='MSA',
                                                 Specific_VP__c=null,
                                                 Priority__c=3,
                                                 End_Behavior__c='Accenture',
                                                 Sourcing_Manager__c=null));
        
        //Specialist Route (Services, Non-IT Professional Services)
        routList.add(new Request_Triage_Route__c(All_Categories__c=false,
                                                 Category__c='Services',
                                                 All_Types_of_Service__c=false,
                                                 Type_of_Service__c='Non-IT Professional Services',
                                                 All_Sourcing_Project_Types__c=false,
                                                 Sourcing_Project_Type__c='MSA',
                                                 Specific_VP__c=null,
                                                 Priority__c=1,
                                                 End_Behavior__c='Specialist',
                                                 Sourcing_Manager__c=null));
        //Specific Sourcing Manager Route (Services, Non-IT Professional Services)
        routList.add(new Request_Triage_Route__c(All_Categories__c=false,
                                                 Category__c='Services',
                                                 All_Types_of_Service__c=false,
                                                 Type_of_Service__c='Non-IT Professional Services',
                                                 All_Sourcing_Project_Types__c=true,
                                                 //Sourcing_Project_Type__c='MSA',
                                                 Specific_VP__c=null,
                                                 Priority__c=2,
                                                 End_Behavior__c='Specific Sourcing Manager',
                                                 Sourcing_Manager__c=userList[6].Id));
        //Spending Threshold Route (Services, IT Services)
        routList.add(new Request_Triage_Route__c(All_Categories__c=false,
                                                 Category__c='Services',
                                                 All_Types_of_Service__c=false,
                                                 Type_of_Service__c='IT Services',
                                                 All_Sourcing_Project_Types__c=true,
                                                 //Sourcing_Project_Type__c='MSA',
                                                 Specific_VP__c=null,
                                                 Priority__c=2,
                                                 End_Behavior__c='Obey Thresholds',
                                                 Sourcing_Manager__c=null));
        //VP Alignment Route (Software)
        routList.add(new Request_Triage_Route__c(All_Categories__c=false,
                                                 Category__c='Software',
                                                 All_Types_of_Service__c=true,
                                                 //Type_of_Service__c='IT Services',
                                                 All_Sourcing_Project_Types__c=true,
                                                 //Sourcing_Project_Type__c='MSA',
                                                 Specific_VP__c=null,
                                                 Priority__c=1,
                                                 End_Behavior__c='VP Alignment',
                                                 Sourcing_Manager__c=null));
        insert routList;
        
        List<Accenture_Spending_Threshold__c> spenList = new List<Accenture_Spending_Threshold__c>();
        spenList.add(new Accenture_Spending_Threshold__c(Name='Amendment',
                                                         Spending_Threshold__c='No Spend;$1 - $250K'));
        insert spenList;
        
        ////add sprint triage persons 082020
		sprint_triage__C spr=new sprint_triage__c(Assigned_User__c=userList[0].id,
                                                 What_s_being_sourced__c='Field Services;Services;Software;Hardware Products',
                                                 Functional_Group_BU__c='Network');
        

        insert spr;
                        
        
    }

    
    @isTest
    static void testEnergyCallout() {
        //Used for Business Point of Contact, etc.
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];
        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1]; //82020 this user has no approver
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test1'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Energy & Facilities',
                                            What_is_being_sourced_or_contracted__c='Energy',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='MSA',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();
        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals(source.Id, testReq.OwnerId);  
    }
    
    @isTest
    static void testWrongEnergyCallout() {
        //Used for Business Point of Contact, etc.
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];
        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test1'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Energy & Facilities',
                                            What_is_being_sourced_or_contracted__c='Facilities',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='MSA',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals('0052E00000KqJDMQA3', testReq.OwnerId);  // this is now kottenbrock 082020
    }
    
    @isTest
    static void testOOSSupplier() {
        //Used for Business Point of Contact, etc.
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];
        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test2'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Services',
                                            Services__c='IT Services',
                                            Sourcing_Project_Type__c='Order Form',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals(source.Id, testReq.OwnerId);        
    }
    
    @isTest
    static void testOOSVP() {
        //Used for Business Point of Contact, etc.
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1]; 82020
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test1'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Services',
                                            Services__c='IT Services',
                                            Sourcing_Project_Type__c='Order Form',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals(source.Id, testReq.OwnerId); 
    }
    
    @isTest
    static void testOOSCategory() {
        //Used for Business Point of Contact, etc.
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test3'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Hardware Products',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='Order Form',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals(source.Id, testReq.OwnerId); 
    }
    
    @isTest
    static void testAccentureRoute() {
        //Used for Business Point of Contact, etc.
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test3'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Field Services',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='Order Form',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals('0052E00000Ko536QAB', testReq.OwnerId); 
    }
    
    @isTest
    static void testSpecialistRoute() {
        //Used for Business Point of Contact, etc.
                User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test3'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Services',
                                            Services__c='Non-IT Professional Services',
                                            Sourcing_Project_Type__c='MSA',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals('0052E00000KqJDMQA3', testReq.OwnerId); 
    }
    
    @isTest
    static void testSpecificSourcingManagerRoute() {
        //Used for Business Point of Contact, etc
                User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test6'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Services',
                                            Services__c='Non-IT Professional Services',
                                            Sourcing_Project_Type__c='Amendment',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals(source.Id, testReq.OwnerId); 
    }
    
    @isTest
    static void testSpendThresholdRoute() {
        //Used for Business Point of Contact, etc.
                User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test3'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Services',
                                            Services__c='IT Services',
                                            Sourcing_Project_Type__c='Amendment',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='No Spend',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals('0052E00000Ko536QAB', testReq.OwnerId); 
    }
    
    @isTest
    static void testNoSpendThresholdRoute() {
        //Used for Business Point of Contact, etc.
                User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test4'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Services',
                                            Services__c='IT Services',
                                            Sourcing_Project_Type__c='Amendment',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='$1M - $5M',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            Allocated_Budget__c=1,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        
        System.AssertEquals(source.Id, testReq.OwnerId); 
    }
    
    @isTest
    static void testVPAlignmentRoute() {
        //Used for Business Point of Contact, etc.
                User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
        User source = [SELECT Id FROM User WHERE Name LIKE 'Test4'];
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Software',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='Amendment',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='$1M - $5M',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            Allocated_Budget__c=1,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        System.AssertEquals(source.Id, testReq.OwnerId); 
    }
    
    @isTest
    static void testNoAlignment() {
        //run as sprint user
        
        //Used for Business Point of Contact, etc.
                User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];

        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name = 'Test NO OOS'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];
        
       // User source = [SELECT Id FROM User WHERE Name LIKE 'Test11']; 
        
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Field Services',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='Market Service Order - Carrier',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='$20M or more',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Finance',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            Allocated_Budget__c=1,
                                            OwnerId=misc.Id);
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        Test.startTest();
        insert reqlists;
        requesttriagemethods.routeRequests(reqlists);     
        Test.stopTest();        
        testReq = [SELECT OwnerId FROM Request__c];
        System.AssertEquals('0052E00000Ko536QAB', testReq.OwnerId); 
    }
    @isTest
    static void testNoAlignmentSprint() {
        user spu=[select id from user where email like '%sprint.com%' and profile.name='System Administrator' limit 1];
        system.runAs(spu){

            system.debug(+userinfo.getLastName()+' email is'+userinfo.getUserEmail());
        
        //run as sprint
        //Used for Business Point of Contact, etc.
        
        User misc = [SELECT Id FROM User WHERE executive_priority_approver__c!=null ORDER BY Name LIMIT 1];
        //User misc = [SELECT Id FROM User WHERE Profile.Name='Standard User' ORDER BY Name LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test NO OOS'];

        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test NO OOS'];

        //sprint_triage__c sp = [SELECT submitter_email__c,Assigned_User__c FROM sprint_triage__c limit 1];
        Request__c testReq = new Request__c(Procurement_Group__c='Technology & Indirect Procurement',
                                            What_is_being_sourced_or_contracted__c='Field Services',
                                            //Services__c='IT Services',
                                            Sourcing_Project_Type__c='Market Service Order - Carrier',
                                            Supplier_Name__c=Supplier.Id,
                                            Description__c='TEST',
                                            Estimated_Spend_in_first_12_months__c='$20M or more',
                                            Business_Point_of_Contact__c=misc.id,
                                            Business_Unit__c='Network',
                                            Budget_Owner__c=misc.id,
                                            VP_Project_Owner__c=VP.id,
                                            Requestor__c=UserInfo.getUserId(),
                                            Ownerid=misc.id,
                                            Allocated_Budget__c=1
                                            );
        
        list<request__c> reqlists = new list<request__c>();
        reqlists.add(testReq);
        
        Test.startTest();
        
            insert reqlists;
            requesttriagemethods.routeRequests(reqlists); 
        
        Test.stopTest();
        
        system.assertNotEquals('0052E00000KqJDMQA3', testReq.OwnerId);
        }
    }
    
}
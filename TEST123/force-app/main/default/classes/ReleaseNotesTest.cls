/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : ReleaseNotesTest
*  @Version History  : 2.0 Modified to add null condition by Llamar 30 Sep 2020
*  @Creation         : 22.07.2020
*  @Description      : Test class for ReleaseNotesTest
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class ReleaseNotesTest {

    static testMethod void createReleaseNote(){

        Release_Note__c releaseNotes = New Release_Note__c();
        Release_Note__c retrievedreleaseNotes = New Release_Note__c();
        User currentUser = New User();
		currentUser = TestDataFactory.createUser();
        insert(currentUser);
        releaseNotes.Name = 'July Sample Release';
        releaseNotes.End_Date__c = date.today()+1;
        releaseNotes.Release_Notes__c='test';
        releaseNotes.Release_Short_Description__c='test';
        releaseNotes.Start_Date__c = date.today();
        releaseNotes.POD_or_Community__c ='Community';
        system.runAs(currentUser){
        Test.startTest();
    	insert(releaseNotes);
        retrievedreleaseNotes = getLatestReleaseNote.getLatestNotes();
        getLatestReleaseNote.HideReleaseNotes();
        Test.stopTest();
        }
        }
    
    }
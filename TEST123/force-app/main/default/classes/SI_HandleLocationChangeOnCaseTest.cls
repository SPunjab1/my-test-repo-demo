/******************************************************************************
@author      : IBM
@date        : 04.09.2020
@description : Test class for SI_HandleLocationChangeOnCase apex class
@Version     : 1.0
*******************************************************************************/
@isTest
public class SI_HandleLocationChangeOnCaseTest {
	
    /**************************************************************************
    * @description Method to set up test data
    **************************************************************************/
    @testSetup
    private static void testDataSetup() {
        
        //create location records
        List<Location_ID__c> listOfLocations = TestDataFactory.createLocationId('Test','Test',2);
        insert listOfLocations;
        //fetch record type
        Id intakeSiteRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Intake_Site__c',
                                                           Label.SI_IntakeSiteRT);
        Id macroDeselectRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',
                                                           Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        //create case
        List<Case> listOfCase = TestDataFactory.createCaseSingleIntake(1, macroDeselectRecTypeId);
        listOfCase[0].Location_Ids__c = listOfLocations[0].Id;
        insert listOfCase;
        
        List<Intake_Site__c> listOfIntakeSites = 
            TestDataFactory.createIntakeSiteSingleIntake(1,listOfCase[0].Id,intakeSiteRecTypeId);
        listOfIntakeSites[0].Location_ID__c = listOfLocations[0].Id;
        insert listOfIntakeSites;
    }
    
    /**************************************************************************
    * @description To test deleteOldIntakeSiteOnLocationChange method
    **************************************************************************/
    @isTest
    private static void testLocationChange() {
        Case caseRec = [SELECT Id, Location_IDs__c 
                        FROM Case];
        List<Intake_Site__c> listOfIntakeSite = [SELECT Id,Location_ID__c
                                                 FROM Intake_Site__c 
                                                 WHERE Case__c = :caseRec.Id LIMIT 1];
        List<Location_ID__c> listOfLocations = [SELECT Id
                                               FROM Location_ID__c];
        System.assertEquals(1, listOfIntakeSite.size(),'There should be one intake site.');
        System.assertEquals(listOfIntakeSite[0].Location_ID__c, caseRec.Location_IDs__c,'Location should be same.');
        Test.startTest();
        caseRec.Location_Ids__c = listOfLocations[1].Id;
        update caseRec;
        SI_HandleLocationChangeOnCase.deleteOldIntakeSiteOnLocationChange(new List<Id>{caseRec.Id});
        Test.stopTest();
        List<Intake_Site__c> listOfIntakeSiteNew = [SELECT Id,Location_ID__c
                                                    FROM Intake_Site__c 
                                                    WHERE Case__c = :caseRec.Id];
        System.assertEquals(0, listOfIntakeSiteNew.size(),'There should be one intake site.');
    }
}
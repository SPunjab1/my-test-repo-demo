/******************************************************************************
@author      - IBM
@date        - 03.08.2020
@description - Test class for SI_CaseAssignmentRule
@Version     - 1.0
*******************************************************************************/
@isTest
public class SI_CaseAssignmentRuleTest {
    
    /**************************************************************************
    * @description Method to set up test data
    **************************************************************************/
    @testSetup
    private static void testDataSetup() {
        
        //create opportunity
        Id oppBackhaulRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',Label.SI_Backhaul_ContractRT);
        List<Opportunity> listOfOpportunity = 
            TestDataFactory.createOpportunitySingleIntake(1, oppBackhaulRecTypeId);
        //listOfOpportunity[0].stagename= 'Closed - Contract Executed';
        insert listOfOpportunity;
        //fetch record type
        Id macroDeselectRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',
                                                           Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        //create case
        List<Case> listOfCase = TestDataFactory.createCaseSingleIntake(1, macroDeselectRecTypeId);
        listOfCase[0].Opportunity__c = listOfOpportunity[0].Id;
        insert listOfCase;
        
        List<Case> listOfCaseRecs = new List<Case>();
        for(Case caseRec: [Select Id FROM Case]) {
            caseRec.Status = 'New-Submitted';
            listOfCaseRecs.add(caseRec);
        }
        update listOfCaseRecs;
    }
    
    /**************************************************************************
    * @description Method to test fireCaseAssignmentRuleFromOpportunity method
    **************************************************************************/
    @isTest
    private static void fireCaseAssignmentRuleForDatabaseUpdatedTest() {
        List<Id> listOfOppIds = new List<Id>();
        for(Opportunity opportunityRec : [SELECT Id
                                          FROM Opportunity 
                                          WHERE RecordType.DeveloperName = 
                                          :Label.SI_Backhaul_ContractRT]) {
            listOfOppIds.add(opportunityRec.Id);
        }
        List<Case> listOfCase = [SELECT Id, OwnerId 
                                 FROM Case
                                 WHERE Opportunity__c IN :listOfOppIds];
        listOfCase[0].Status = 'OM Ready';
        update listOfCase[0];
        Test.startTest();
        SI_CaseAssignmentRule.fireCaseAssignmentRuleFromOpportunity(listOfOppIds);       
        Test.stopTest();
        List<Case> listOfCaseUpdated = [SELECT Id, OwnerId 
                                        FROM Case
                                        WHERE Opportunity__c IN :listOfOppIds];
        System.assertEquals(true, listOfCaseUpdated[0].OwnerId != listOfCase[0].OwnerId, 
                            'Case Owner should be updated');
        
    }
    
    /**************************************************************************
    * @description Method to test fireCaseAssignmentRuleFromOpportunity method
    **************************************************************************/
    @isTest
    private static void fireCaseAssignmentRuleForOMReadyTest() {
        List<Id> listOfOppIds = new List<Id>();
        for(Opportunity opportunityRec : [SELECT Id
                                          FROM Opportunity 
                                          WHERE RecordType.DeveloperName = 
                                          :Label.SI_Backhaul_ContractRT]) {
            listOfOppIds.add(opportunityRec.Id);
        }
        List<Case> listOfCase = [SELECT Id, OwnerId 
                                 FROM Case
                                 WHERE Opportunity__c IN :listOfOppIds];
        listOfCase[0].Status = 'OM Ready';
        update listOfCase[0];
        Test.startTest();
        SI_CaseAssignmentRule.fireCaseAssignmentRuleFromOpportunity(listOfOppIds);       
        Test.stopTest();
        List<Case> listOfCaseUpdated = [SELECT Id, OwnerId 
                                        FROM Case
                                        WHERE Opportunity__c IN :listOfOppIds];
        System.assertEquals(true, listOfCaseUpdated[0].OwnerId != listOfCase[0].OwnerId, 
                            'Case Owner should be updated');
    }
}
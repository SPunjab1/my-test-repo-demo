/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : IntakeFormController
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 02.06.2019
*  @Description      : Apex class of Intake Form lightning component
**********************************************************************************************************************
**********************************************************************************************************************/
public with sharing class IntakeFormController {
    public static Map<String,Location_ID__c> locationIdMap;
    public static Map<String,Switch_ID__c> switchIdsMap;
/***************************************************************************************
* @Description :Method to get picklist values
* @input params :object name,field name
***************************************************************************************/  
    @AuraEnabled
    public static List<String> getCustomPickListValues(Sobject object_name,String field_name)
    {
        List<String> options = new List<String>();
        Schema.sObjectType sobject_type;
        sobject_type = object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobject_describe.fields.getMap(); 
        List<Schema.PicklistEntry> pickListValues = fieldMap.get(field_name.toLowerCase()).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pickListValues) {
            options.add(a.getLabel());
        }
        return options;
    }
    
/***************************************************************************************
* @Description :Method to get entered location data from database and check wheather the 
                work order with same name and template is already present or not in database.
* @input params :location ID,templateName
***************************************************************************************/  
    @AuraEnabled
    public static Map<String,Location_ID__c> getLocationDetails(String locationID,String templateName){
        Map<String,Location_ID__c> newMap = new Map<String,Location_ID__c>();
        List<Location_ID__c> lstLocations =new List<Location_ID__c>();                                                
        lstLocations = [Select Id,Region__c,Market__c,AAV__c,AAV_Status__c,AAV_Contract__c,Pending_Deselect__c,Microwave_Status__c,
                        Site_Status__c, Ethernet_Installed__c,Contract_Status__c,Refresh_Date__c,Location_ID__c from Location_ID__c where Location_ID__c = :locationID ];
        if(lstLocations!=null && lstLocations.size()>0){
            List<Case> csLst = [select id from case where Status != 'Closed' and Location_IDs__r.Location_ID__c = :locationID and Workflow_Template__c = :templateName limit 1];
            if(csLst!=null && csLst.size()>0){
                newMap.put('Yes',lstLocations[0]);    
            }
            else{
                newMap.put('No',lstLocations[0]);    
            }
            return newMap;    
        }
        else{
            return null;
        }
    }
    /***************************************************************************************
* @Description :Method to get entered location data from database and check wheather the 
                work order with same name and template is already present or not in database for bulk operation.
* @input params :location ID,templateName
***************************************************************************************/  
     @AuraEnabled
    public static Map<String,Case> checkLocations(List<String> templateNames, List<String> locationIds){
        
        
        Set<String> templateNamesSet = new Set<String>(templateNames);
		Set<String> locationIdsSet = new Set<String>(locationIds);        
        
        templateNamesSet.remove('');
		locationIdsSet.remove('');
        
        System.debug(' 🚀 ' +templateNamesSet);
		System.debug(' 🚀 ' +locationIdsSet);
     
        
        //convert list to set 
        Map<String,Case> newMap = new Map<String,Case>();
        List<Location_ID__c> lstLocations =new List<Location_ID__c>();                                                
        lstLocations = [Select Id,Region__c,Market__c,AAV__c,AAV_Status__c,AAV_Contract__c,Pending_Deselect__c,Microwave_Status__c,
                        Site_Status__c, Ethernet_Installed__c,Contract_Status__c,Refresh_Date__c,Location_ID__c from Location_ID__c where Location_ID__c IN: locationIdsSet ];
        if(lstLocations!=null && lstLocations.size()>0){
            List<Case> csLst = [select id,Workflow_Template__c,Pier_Location_ID__c, Advance_RAN_Transmission__c,Enterprise_Transmission_Design__c,Backhaul_Planning_Implementation__c,Transmission_Deployment_Walker__c,Secondary_Location_ID__c,CRCount__c from case where Status != 'Closed' and Location_IDs__r.Location_ID__c IN: locationIdsSet and Workflow_Template__c IN: templateNamesSet limit 15];
            if(csLst!=null && csLst.size()>0){
                
                for(Case c : csLst){                    
					newMap.put(c.Pier_Location_ID__c, c);
				}  
                System.debug(' ----CaseList ' +csLst);
            	return newMap;  
            }
            else{
				return null;
            }
            
        }
        else{
            return null;
        }
        
    }
    
    @AuraEnabled
    public static id getSampleFileId(){
        
        return (id) File_configurations__c.getInstance().get('Sample_Intake_Attachment__c');
    }
    
/***************************************************************************************
* @Description :Method to get entered Switch data from database 
* @input params :switch ID
***************************************************************************************/  
    @AuraEnabled
    public static Switch_ID__c getSwitchDetails(String switchId){
        List<Switch_ID__c> lstSwitch = new List<Switch_ID__c>();                                                
        lstSwitch = [Select Id,Switch_ID__c from Switch_ID__c where Switch_ID__c = :switchId];
        if(lstSwitch!=null && lstSwitch.size()>0){
            return lstSwitch[0];    
        }
        else{
            return null;
        }
    }
     
/***************************************************************************************************************************
* @Description :Method to create Intake Request, Intake Request Location Junction and Case(Work Order) record for Single site
* @input params :Intake Rquest object,location object
****************************************************************************************************************************/ 
    @AuraEnabled
    public static String createSingleSelectIntakeRec(Intake_Request__c intakeReq,Location_ID__c location,Switch_ID__c switchId,String CaseNumberTemp){
        Intake_Request__c intake = createIntakeRecord(intakeReq,'No'); 
        List<Location_ID__c> locations = new List<Location_ID__c>();
        locations.add(location);
        Map<String,String> tpeRequestNumberMap = getTpeRequestNumberMap(null,CaseNumberTemp);
        List<Intake_Request_Location__c> listIRRecords = createIntakeLocationrecords(intake,locations);
        if(switchId!=null){
            List<Switch_ID__c> switches = new List<Switch_ID__c>();
            switches.add(switchId);   
            List<Case> caseLst = createCaseRecords(intake,locations,'No',null,null,switches,tpeRequestNumberMap);
        }
        else{
            List<Case> caseLst = createCaseRecords(intake,locations,'No',null,null,null,tpeRequestNumberMap);    
        }
        return intake.id;
    }
    
/***************************************************************************************************************************
* @Description :Method to create Intake Request, Intake Request Location Junction and Cases(Work Order) record for Multi site
* @input params :Intake Rquest object,Primary locations,Secondary locations,CSV file Content in map,Blank Id list
****************************************************************************************************************************/ 
    @AuraEnabled
    public static Map<String,String> createMultiSelectIntakeRec(Intake_Request__c intakeReq,List<Location_ID__c> locations,List<Location_ID__c> secLocId,Map<String,Map<String,String>> fileContentMap1,List<String> blankIdLst,List<Switch_ID__c> switchIdLst,String CaseNumberTemp){
        Map<String,String> msgToIntakeIdMap  = new map<String,String>();
        createLocationIdMap(locations,secLocId);
        createSwitchIdMap(switchIdLst);
        Map<String,String> tpeRequestNumberMap = getTpeRequestNumberMap(fileContentMap1,null);
        Intake_Request__c intake = createIntakeRecord(intakeReq,'Yes'); 
        List<Intake_Request_Location__c> listIRRecords = createIntakeLocationrecords(intake,locations);
        List<Case> caseLst = createCaseRecords(intake,locations,'Yes',fileContentMap1,blankIdLst,switchIdLst,tpeRequestNumberMap);
        if(caseLst.size() <= Integer.valueOf(Label.WorkOrderInsertionLimit)){
            msgToIntakeIdMap.put('Yes',intake.id);    
        }
        else{
            msgToIntakeIdMap.put('No',intake.id);      
        }
        return msgToIntakeIdMap;
    }
    
/******************************************************************************************************************
* @Description :Private method to create map of Location Id and Location Object record for all location in CSV.
* @input params :Primary locations,Secondary Locations
*******************************************************************************************************************/ 
    private static void createLocationIdMap(List<Location_ID__c> locations,List<Location_ID__c> secLocId){
        locationIdMap =new Map<String,Location_ID__c>();
        if(locations!=null && locations.size()>0){
            for(Location_ID__c loc :locations){
                locationIdMap.put(loc.Location_ID__c,loc);   
            }    
        }
        if(secLocId!=null && secLocId.size()>0){
            for(Location_ID__c loc :secLocId){
                locationIdMap.put(loc.Location_ID__c,loc);   
            }
        }
    }
/******************************************************************************************************************
* @Description :Private method to create map of Switch Id and SwitchId Object record for all SicthIds present in CSV.
* @input params :SwitchId
*******************************************************************************************************************/ 
     private static void createSwitchIdMap(List<Switch_ID__c> switIds){
        switchIdsMap =new Map<String,Switch_ID__c>();
        if(switIds!=null && switIds.size()>0){
            for(Switch_ID__c sId :switIds){
                switchIdsMap.put(sId.Switch_ID__c,sId);   
            }    
        }
    }
/******************************************************************************************************
* @Description :Private method to create intake request record for single site/Multi site.
* @input params :Intake Request Record,isMultiSite : Yes/No
*******************************************************************************************************/ 
    private static Intake_Request__c createIntakeRecord(Intake_Request__c intakeReq,String isMultiSite){
        Intake_Request__c req = new Intake_Request__c();
        req.Multiple_Site_Request__c = isMultiSite;
        if(isMultiSite == 'No'){
            req.Project_Name__c = intakeReq.Project_Name__c;
            req.Project_Description__c = intakeReq.Project_Description__c; 
            req.Task_Description__c = intakeReq.Task_Description__c; 
            req.Workflow_Template__c = intakeReq.Workflow_Template__c;
            req.Advance_RAN_Transmission__c = intakeReq.Advance_RAN_Transmission__c;
            req.Enterprise_Transmission_Design__c = intakeReq.Enterprise_Transmission_Design__c;
            req.Backhaul_Planning_Implementation__c = intakeReq.Backhaul_Planning_Implementation__c;
            req.Transmission_Deployment_Walker__c = intakeReq.Transmission_Deployment_Walker__c;
            req.CIR__c = intakeReq.CIR__c;//ADDED BY FARAZ FOR CIR
        }
        try{
            if(req != null){
                database.insert(req,true);
            }
        }
        catch(Exception ex){throw ex;}
        return req;
    }
    
/******************************************************************************************************************
* @Description :Private method to create intake request location(junction record) record for single site/Multi site.
* @input params :Intake Request Record,locations records
*******************************************************************************************************************/ 
    private static List<Intake_Request_Location__c> createIntakeLocationrecords(Intake_Request__c intakeReq,List<Location_ID__c> locations){
        List<Intake_Request_Location__c> listIRRecords = new List<Intake_Request_Location__c>();
        for(Location_ID__c loc : locations){
            Intake_Request_Location__c irRec = new Intake_Request_Location__c(); 
            irRec.Intake_Request__c = intakeReq.Id;
            irRec.Location_ID__c = loc.Id;
            listIRRecords.add(irRec);
        }
        try{
            if(listIRRecords!=null && listIRRecords.size()>0){
                database.insert(listIRRecords,true);
            }    
        }
        catch(Exception ex){throw ex;}
        return listIRRecords;
    }
    
        /******************************************************************************************************************
* @Description :Public method to get all TPE Request number from database (Case) records for Singe site/Multi site.
* @input params :FileContentMap, Tperequest number : CSV file Data in map, caseNumber entered
*******************************************************************************************************************/
    @AuraEnabled
    public static Map<String,String> getTpeRequestNumberMap(Map<String,Map<String,String>> fileContentMap, String CaseNumberSingle){
        Map<String,String> tpeRequestNumberMap = new Map<String,String>();
        List<String> tpeRequestNumberLst = new List<String>();
        Set<String> caseNumberSet = new Set<String>();
        if(fileContentMap!=null && fileContentMap.size()>0){
            for(String key : fileContentMap.keySet()){
                if(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber) != null && fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber) != '' ){
                    tpeRequestNumberLst.add(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber));
                }
            }    
        }
        
        if(!tpeRequestNumberLst.isEmpty() && tpeRequestNumberLst.size()>0){
            Id CaserecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Work_Order').getRecordTypeId();
            List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE CaseNumber IN :tpeRequestNumberLst];
            for(Case cas : caseList){
                caseNumberSet.add(cas.CaseNumber);
            }
            String tempStr ='';
            boolean caseMisMatch = false;
            for(String tpe : tpeRequestNumberLst){
                if(!caseNumberSet.contains(tpe)){
                    tempStr += tpe+',';
                    caseMisMatch = true;
                }
            }
            if(caseMisMatch){
                tpeRequestNumberMap.put('caseMisMatch', ':'+tempStr.removeEnd(','));
            }else{
                for(Case cas : caselist){                
                    tpeRequestNumberMap.put(cas.CaseNumber,cas.Id);                    
                }
            }
        }
        
        if(CaseNumberSingle != null){
            Id CaserecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Work_Order').getRecordTypeId();
            List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE CaseNumber = :CaseNumberSingle ];
            for(Case cas : caselist){
                tpeRequestNumberMap.put(cas.CaseNumber,cas.Id);
            }
        }
        return tpeRequestNumberMap;
    }
    
/******************************************************************************************************************
* @Description :Private method to create Work orders (Case) records for single site/Multi site.
* @input params :Intake Request Record,locations,isMultiSite : Yes/No,CSV file Data in map, blank id list
*******************************************************************************************************************/
    private static List<Case> createCaseRecords(Intake_Request__c intakeReq,List<Location_ID__c> locations,String isMultiSite,Map<String,Map<String,String>> fileContentMap,List<String> blankIdLst,List<Switch_ID__c> switchIdLst,Map<String,String> tpeRequestNumberMap){
        List<String> priLocIdLst =new List<String>();
        for(Location_ID__c loc :locations){
            priLocIdLst.add(loc.Location_ID__c);    
        }
        List<Case> caseLst = new List<Case>();
        if(fileContentMap!=null && fileContentMap.size()>0){
            for(String key : fileContentMap.keySet()){
                System.debug(' --1--> Location ');
                if(priLocIdLst.contains(key.split('\\|')[0])){
                    Case c; 
                    //Added by Gaurav for get the TPE Request Number
                    String tpeRequestNumber = '';
                    if(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber) != null && fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber) != '' ){
                        if(tpeRequestNumberMap.containsKey(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber))){
                            tpeRequestNumber =  tpeRequestNumberMap.get(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber));  
                        }
                    }
                    
                    Integer cnt = Integer.valueOf(fileContentMap.get(key).get(Label.CSVCol_CRCount));
                    
                    for(Integer i =0; i<cnt ;i++){
                        if(switchIdsMap!=null && switchIdsMap.size()>0 && switchIdsMap.get(fileContentMap.get(key).get(Label.CSVCol_SwitchId))!=null){
                             c = createCase(intakeReq,locationIdMap.get(key.split('\\|')[0]),isMultiSite,fileContentMap.get(key),switchIdsMap.get(fileContentMap.get(key).get(Label.CSVCol_SwitchId)),tpeRequestNumber);    
                        }
                        else{
                             c = createCase(intakeReq,locationIdMap.get(key.split('\\|')[0]),isMultiSite,fileContentMap.get(key),null,tpeRequestNumber);    
                        }
                        caseLst.add(c);
                    }
                }
            }    
        }
        else{
            String tpeRequest;
            if(!tpeRequestNumberMap.IsEmpty() && tpeRequestNumberMap!=null){
                tpeRequest = tpeRequestNumberMap.values()[0];
            }
            for(Location_ID__c loc: locations){
                System.debug(' ---2-> Location ');
                Case c;
                if(switchIdLst!=null && switchIdLst.size()>0){
                    c = createCase(intakeReq,loc,isMultiSite,null,switchIdLst[0],tpeRequest);    
                }
                else{
                    c = createCase(intakeReq,loc,isMultiSite,null,null,tpeRequest);       
                }
               
                caseLst.add(c);
            }
        }
        if(blankIdLst!=null && blankIdLst.size()>0 && fileContentMap!=null && fileContentMap.size()>0){ 
            for(String key : fileContentMap.keySet()){
                System.debug(' ---4-> Location ');
                if(blankIdLst.contains(key.split('\\|')[0])){
                    Case c;
                    String tpeRequestNumber = '';
                    if(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber) != null && fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber) != '' ){
                        if(tpeRequestNumberMap.containsKey(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber))){
                            tpeRequestNumber =  tpeRequestNumberMap.get(fileContentMap.get(key).get(Label.CSVCol_TPERequestNumber));  
                        }
                    }
                    Integer cnt = Integer.valueOf(fileContentMap.get(key).get(Label.CSVCol_CRCount));
                    for(Integer i =0; i<cnt ;i++){
                        c = createCase(intakeReq,null,isMultiSite,fileContentMap.get(key),switchIdsMap.get(fileContentMap.get(key).get(Label.CSVCol_SwitchId)),tpeRequestNumber);
                        caseLst.add(c);
                    } 
                }
            }
        }
        try{
            if(caseLst!=null && caseLst.size()>0){
                if(caseLst.size() <= Integer.valueOf(Label.WorkOrderInsertionLimit)){
                    database.insert(caseLst,true);    
                }
                else{
                    if(!Test.isRunningTest()){
                        WorkOrderCreationBatch batchobj = new WorkOrderCreationBatch(caseLst,intakeReq.Id);
                        Database.executeBatch(batchobj,Integer.valueOf(Label.WorkOrderCreationBatchSize));     
                    }
                }
            }
        }
        catch(Exception ex){throw ex;}
        return caseLst;
    }
   
/******************************************************************************************************************
* @Description :Private method to create Work orders (Case) record for single site/Multi site.
* @input params :Intake Request Record,locations,isMultiSite : Yes/No, CSV file single row data in map
*******************************************************************************************************************/
    private static Case createCase(Intake_Request__c intakeReq,Location_ID__c location,String isMultiSite,Map<String,String> data,Switch_ID__c switchId,String TPERequestNumberId){
        System.debug('TPERequestNumberId>>>>>>>>>>>>>>'+TPERequestNumberId);
        Case caseObj = new Case();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Work_Order').getRecordTypeId();
        caseObj.RecordTypeId = recordTypeId; 
        caseObj.Multiple_Site_Request__c =isMultiSite;
        if(location!=null){
            caseObj.Market_Name__c = location.Market__c;
            caseObj.Microwave_Status__c = location.Microwave_Status__c;  
            caseObj.Refresh_Date__c = location.Refresh_Date__c;
            caseObj.Site_Status__c = location.Site_Status__c;
            caseObj.AAV__c = location.AAV_Contract__c;
            caseObj.AAV_Status__c = location.AAV_Status__c;
            caseObj.Region__c = location.Region__c;
            caseObj.Location_IDs__c = location.Id ;
            if(TPERequestNumberId != null && TPERequestNumberId != ''){
                caseObj.TPE_Request_Number__c = TPERequestNumberId;
            }
        }
        if(switchId!=null){
             caseObj.Switch_ID__c = switchId.Id ;    
        }
        caseObj.Intake_Request__c = intakeReq.Id;
        caseObj.Status ='New';
        if(data!=null && data.size()>0){
            caseObj.Workflow_Template__c = data.get(Label.CSVCol_TemplateName);
            caseObj.Project_Name__c = data.get(Label.CSVCol_ProjectName);
            caseObj.User_Description__c = data.get(Label.CSVCol_CRUserDescription);
            if(data.get(Label.CSVCol_CIR)!=null && data.get(Label.CSVCol_CIR)!=''){
                caseObj.CIR__c = Integer.valueof(data.get(Label.CSVCol_CIR));//ADDED BY FARAZ FOR CIR
            }
            if(data.get(Label.CSVCol_AdvanceRANTransmission)!= null && (data.get(Label.CSVCol_AdvanceRANTransmission).equalsIgnoreCase('true') || data.get(Label.CSVCol_AdvanceRANTransmission).equalsIgnoreCase('false'))){
                caseObj.Advance_RAN_Transmission__c = Boolean.valueOf(data.get(Label.CSVCol_AdvanceRANTransmission));    
            }
            if(data.get(Label.CSVCol_EnterpriseTransmissionDesign)!= null && (data.get(Label.CSVCol_EnterpriseTransmissionDesign).equalsIgnoreCase('true') || data.get(Label.CSVCol_EnterpriseTransmissionDesign).equalsIgnoreCase('false'))){
                caseObj.Enterprise_Transmission_Design__c = Boolean.valueOf(data.get(Label.CSVCol_EnterpriseTransmissionDesign));  
            }
            //Added by Faraz
            if(data.get(Label.CSVCol_BackhaulPlanningImp)!= null && (data.get(Label.CSVCol_BackhaulPlanningImp).equalsIgnoreCase('true') || data.get(Label.CSVCol_BackhaulPlanningImp).equalsIgnoreCase('false'))){
                caseObj.Backhaul_Planning_Implementation__c = Boolean.valueOf(data.get(Label.CSVCol_BackhaulPlanningImp));
            } 
            if(data.get(Label.CSVCol_TransmissionDeploymentWalker)!= null && (data.get(Label.CSVCol_TransmissionDeploymentWalker).equalsIgnoreCase('true') || data.get(Label.CSVCol_TransmissionDeploymentWalker).equalsIgnoreCase('false'))){
                caseObj.Transmission_Deployment_Walker__c = Boolean.valueOf(data.get(Label.CSVCol_TransmissionDeploymentWalker));
            } 
        }
        else{
            caseObj.Project_Name__c = intakeReq.Project_Name__c;
            caseObj.User_Description__c = intakeReq.Task_Description__c;
            caseObj.Workflow_Template__c = intakeReq.Workflow_Template__c;
            caseObj.Task_Description__c = intakeReq.Task_Description__c;
            caseObj.Advance_RAN_Transmission__c = intakeReq.Advance_RAN_Transmission__c;
            caseObj.Enterprise_Transmission_Design__c = intakeReq.Enterprise_Transmission_Design__c;
           caseObj.Backhaul_Planning_Implementation__c = intakeReq.Backhaul_Planning_Implementation__c;
            caseObj.Transmission_Deployment_Walker__c = intakeReq.Transmission_Deployment_Walker__c;
            caseObj.CIR__c = intakeReq.CIR__c;//ADDED BY FARAZ FOR CIR
        }
        return caseObj;
    }
    
/******************************************************************************************************************
* @Description :Method to validate location data in CSV file against database.
* @input params :primary location id, secondary location id.
*******************************************************************************************************************/
    @AuraEnabled
    public static Map<String,Map<String,Location_ID__c>> validateLocationLst(List<String> locationIds, List<String> secLocId,List<String> locIdTempLst){
        Map<String,Location_ID__c> allLocMap = new Map<String,Location_ID__c>();   
        Map<String,Location_ID__c> validLocIdMap = new Map<String,Location_ID__c>();
        Map<String,Location_ID__c> validSecLocIdMap = new Map<String,Location_ID__c>();
        Map<String,Location_ID__c> invalidSecLocIdMap = new Map<String,Location_ID__c>();
        Map<String,Location_ID__c> invalidLocIdMap = new Map<String,Location_ID__c>();
        Map<String,Location_ID__c> blankLocIdMap = new Map<String,Location_ID__c>();
        List<String> allLoc = new List<String>();
        allLoc.addAll(locationIds);
        allLoc.addAll(secLocId);
        Map<String,Map<String,Location_ID__c>> newMap =  new Map<String,Map<String,Location_ID__c>>();
        List<Location_ID__c> lstLocations = [Select Id,Region__c,Market__c,AAV__c,AAV_Status__c,AAV_Contract__c,Pending_Deselect__c,Microwave_Status__c,
                                             Site_Status__c, Ethernet_Installed__c,Contract_Status__c,Refresh_Date__c,Location_ID__c from Location_ID__c 
                                             where Location_ID__c IN :allLoc];
                                            
        if(lstLocations!=null && lstLocations.size()>0){
            for(Location_ID__c loc : lstLocations){
                allLocMap.put(loc.Location_ID__c,loc);
            }
        }
        if(locationIds!=null && locationIds.size()>0){
            for(String locId : locationIds){
                if(allLocMap.keySet().contains(locId)){
                    validLocIdMap.put(locId,allLocMap.get(locId));      
                }
                else if(locId.contains('BLANK')){
                    blankLocIdMap.put(locId,NULL);      
                } 
                else{
                    invalidLocIdMap.put(locId,NULL);
                }
            }
            newMap.put('Valid',validLocIdMap);
            newMap.put('Invalid',invalidLocIdMap);
            newMap.put('Blank',blankLocIdMap);
        }
        if(secLocId!=null && secLocId.size()>0){
            for(String locId : secLocId){
                if(allLocMap.keySet().contains(locId)){
                    validSecLocIdMap.put(locId,allLocMap.get(locId));      
                }
                else{
                    invalidSecLocIdMap.put(locId,NULL);
                }
            }
            newMap.put('SecValid',validSecLocIdMap);
            newMap.put('SecInvalid',invalidSecLocIdMap);
        }
        Map<String,Location_ID__c> duplicateLocs  = findDuplicateInDatabase(locIdTempLst,validLocIdMap);
        newMap.put('Duplicate',duplicateLocs); 
        return newMap;
    }
/******************************************************************************************************************
* @Description Private method to validate duplicate location aganst database.
* @input params :List of Key+TempName, validIdLocationMap.
*******************************************************************************************************************/    
    private static Map<String,Location_ID__c> findDuplicateInDatabase(List<String> locIdTempLst,Map<String,Location_ID__c> validLocIdMap){
        List<Case> csLst = [select id,Workflow_Template__c,Location_IDs__r.Location_ID__c from Case where Status !='Closed' and Location_IDs__r.Location_ID__c IN :validLocIdMap.keySet()];    
        List<String> caseLocIdTempNameLst =new List<String>();
        for(Case cs : csLst){
            if(cs.Location_IDs__r.Location_ID__c !=null && cs.Workflow_Template__c!=null){
                caseLocIdTempNameLst.add(cs.Location_IDs__r.Location_ID__c+'|'+cs.Workflow_Template__c);  
                
            } 
        }
      
        Map<String,Location_ID__c> duplicateLocsMap = new Map<String,Location_ID__c>();
        for(String key : locIdTempLst){
            if(caseLocIdTempNameLst.contains(key)){
                String locId = key.split('\\|')[0];
                duplicateLocsMap.put(key,validLocIdMap.get(locId));    
            }    
        }
        return duplicateLocsMap;
    }
    
/******************************************************************************************************************
* @Description :Method to save file as attchment to intake record.
* @input params :parentId,fileName.base64Data,contentType,fileId
*******************************************************************************************************************/    
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
        return Id.valueOf(fileId);
    }
    
/******************************************************************************************************************
* @Description :Method to save file as attchment to intake record.
* @input params :parentId,fileName.base64Data,contentType
*******************************************************************************************************************/
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment oAttachment = new Attachment();
        oAttachment.parentId = parentId;
        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        oAttachment.Name = fileName;
        oAttachment.ContentType = contentType;
        try{
            if(oAttachment!=null){
                database.insert(oAttachment,true);
            }   
        }catch(Exception ex){ throw ex;}
        return oAttachment.Id;
    }

/******************************************************************************************************************
* @Description :Method to append the file to existing file if file is larger
* @input params :fileId,base64Data
*******************************************************************************************************************/ 
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id =: fileId
        ];
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);
        try{
            if(a!=null){
                database.update(a,true);
            }   
        }
        catch(Exception ex){throw ex;}
    }
/******************************************************************************************************************
* @Description :Method to validate Switch Id against database.
* @input params :Swich id
*******************************************************************************************************************/
    @AuraEnabled
    public static Map<String,Map<String,Switch_ID__c>> validateSwitchIdLst(List<String> switchIdlst){
        Map<String,Switch_ID__c> allSwitchMap = new Map<String,Switch_ID__c>();   
        Map<String,Switch_ID__c> validSwitchIdMap = new Map<String,Switch_ID__c>();
        Map<String,Switch_ID__c> invalidSwitchIdMap = new Map<String,Switch_ID__c>();
        Map<String,Map<String,Switch_ID__c>> newMap =  new Map<String,Map<String,Switch_ID__c>>();
        List<Switch_ID__c> switchLst;
        if(switchIdlst!=null && switchIdlst.size()>0){
            switchLst = [Select Id,Switch_ID__c from Switch_ID__c where Switch_ID__c IN :switchIdlst];
        }  
        
        if(switchLst!=null && switchLst.size()>0){
            for(Switch_ID__c switchId : switchLst){
                allSwitchMap.put(switchId.Switch_ID__c,switchId);
            }
        }
        if(switchIdlst!=null && switchIdlst.size()>0){
            for(String sId : switchIdlst){
                if(allSwitchMap.keySet().contains(sId)){
                    validSwitchIdMap.put(sId,allSwitchMap.get(sId));      
                }
                else{
                    invalidSwitchIdMap.put(sId,NULL);
                }
            }
            newMap.put('Valid',validSwitchIdMap);
            newMap.put('Invalid',invalidSwitchIdMap);
        }
        return newMap;
    }
    
      /***************************************************************************************
* @Description :Method to get entered TPE Request number data from database 
* @input params :CaseNumber
***************************************************************************************/  
    @AuraEnabled
    public static Case getTPEReqDetails(String CaseNumberTemp){
        List<Case> lstCase = new List<Case>();                           
        string caseNumber = CaseNumberTemp; //'%'+
        lstCase = [Select Id,CaseNumber from Case where CaseNumber =:caseNumber];
        if(lstCase !=null && lstCase.size()>0){
            return lstCase[0];    
        }
        else{
            return null;
        }
    }
    
    /******************************************************************************************************************
* @Description :Method to create custom Lookup.
* @input params :searchKeyWord
*******************************************************************************************************************/
    
    @AuraEnabled
    public static List <Case> fetchTPERequestNumber(String searchKeyWord) {
        Id CaserecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Work_Order').getRecordTypeId();
        String searchKey = '%' + searchKeyWord + '%';
        List < Case > returnList = new List < Case > ();
        List < Case> lstOfCase = [select id,CaseNumber,RecordTypeId,TPE_Request_Number__c from Case where CaseNumber LIKE: searchKey AND RecordTypeId=:CaserecordTypeId];
        for (Case cas : lstOfCase) {
            returnList.add(cas);
        }
        return returnList;
    }
}
@istest
public class CopySiteEscalationsCtrlTest {
	
    @testSetup
    static void testDataSetup() {
         //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //Intialize list of SI Savings Tracker to insert records
        List<SI_Savings_Tracker__c> listSISavingTracker = new  List<SI_Savings_Tracker__c>();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
         // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = listAccount[0].Id;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        insert oppRecForContract;
        Site_Lease_Escalation__c siteLease = TestDataFactory.createSiteLEaseEscalation(oppRecForContract.Id);
		//lease.Lease_Intake__c = oppRecForContract.Id;
        insert siteLease;
    }
    
     @istest static void copyEnhancements(){
        Test.startTest();
         Opportunity opportunityRec = [Select id from Opportunity limit 1];
        CopySiteEscalationsCtrl.copyEscalationForOpportunity(opportunityRec.Id);
        Test.stopTest();
    }
}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ContentDocumentLinkTriggerHandlerTest
*  @Author           : Archana & Matthew Elias
*  @Version History  : 1.0
*  @Creation         : 23.08.2019 & 12.08.20
*  @Description      : Test class for ContentDocumentLinkTriggerHandler
**********************************************************************************************************************
*********************************************************************************************************************/
@isTest(SeeAllData=false)
public class SavingsTrackerTriggerHandlerTest {
/***************************************************************************************************
* @Description : Test Method to test copyAttachmentToSavingsTracker
***************************************************************************************************/
    @isTest static void newSavingsTrackerAttachmentCopyTest(){
        
        // create 3 Request records
        SavingsTrackerTestDataFactory.createRequest(3);
        List<Request__c> testRequest = [SELECT Id FROM Request__c];
        List<ContentDocumentLink> req1Count = new List<ContentDocumentLink>();
        List<ContentDocumentLink> req2Count = new List<ContentDocumentLink>();
        List<ContentDocumentLink> req3Count = new List<ContentDocumentLink>();
        System.assertEquals(3,testRequest.size());
        
        // create 2 ContentDocuments
        SavingsTrackerTestDataFactory.createContentDocuments(2);
        List<ContentDocument> CDList = new List<ContentDocument>([SELECT Id, Title FROM ContentDocument]);
        System.assertEquals(2,CDList.size());
        
        // attach first CD to second Req and second CD to third Request
        Map<Id,Set<Id>> CDLMap = new Map<Id,Set<Id>>();
        CDLMap.put(testRequest[1].Id, new Set<Id>{CDList[0].Id});
        CDLMap.put(testRequest[2].Id, new Set<Id>{CDList[1].Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        
        
        Test.startTest();
        
        // create Savings Tracker records and confirm Attachments were copied
        SavingsTrackerTestDataFactory.createSavingsTracker(new Set<Id>{testRequest[0].Id, testRequest[1].Id});
        Savings_Tracker__c testSavingsTracker1 = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c = :testRequest[0].Id LIMIT 1];
        Savings_Tracker__c testSavingsTracker2 = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c = :testRequest[1].Id LIMIT 1];
        
        // query for CDLs on STs
        List<ContentDocumentLink> CDLList1 = [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        List<ContentDocumentLink> CDLList2 = [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker2.Id];
        
        // confirm that there are no CDLs on first ST, and 1 on second ST and that it links to the proper CD
        System.assertEquals(0,CDLList1.size());
        System.assertEquals(1,CDLList2.size());
        System.assertEquals(CDList[0].Id,CDLList2[0].ContentDocumentId);
        
        Test.stopTest();
        
    }
}
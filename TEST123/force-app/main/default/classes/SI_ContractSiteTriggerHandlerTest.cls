/******************************************************************************
@author      : IBM
@date        : 1.07.2020
@description : Test class for SI_ContractSiteTriggerHandler apex class
@Version     : 1.0
*******************************************************************************/
// private class
@isTest
class SI_ContractSiteTriggerHandlerTest{
    /**************************************************************************
    * @description : Method to setup test data
    **************************************************************************/
    @testSetup
    static void testDataSetup() {
        // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        //Case Record type
        Id macroDeselectRecTypeId =TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        //Intake Record type
        Id intakeCIRRecTypeId =TestDataFactory.getRecordTypeIdByDeveloperName('Intake_Site__c',Label.SI_IntakeSiteRT);
        //Intialize list of Opportunity to insert records
        List<Opportunity> listOfOpportunitiesToInsert = new List<Opportunity>();
        //Intialize list of case to insert records
        List<Case> caseList = new List<Case>();
        //Intialize list of Intake site to insert records
        List<Intake_Site__c> intakeList = new List<Intake_Site__c>();
        //Intialize list of contract site to insert records
        List<Contract_Site__c> listContractSite = new List<Contract_Site__c>();
        //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
        
        // Insert record for ValidationRuleManager__c custom setting        
        insert new ValidationRuleManager__c(SetupOwnerId=UserInfo.getOrganizationId(), Run_Validation__c=true);
        
        //create Opportunity for Contract Backhaul
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = listAccount[0].Id;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        listOfOpportunitiesToInsert.add(oppRecForContract);
        insert listOfOpportunitiesToInsert;
        
        //Create records for case
        Case objCase = TestDataFactory.createCaseSingleIntake(1,macroDeselectRecTypeId)[0];
        objCase.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        caseList.add(objCase);
        
        Case objCase1 = TestDataFactory.createCaseSingleIntake(1,macroDeselectRecTypeId)[0];
        objCase1.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        caseList.add(objCase1);
        insert caseList;
        
        //Create records for Intake
        Intake_Site__c objIntakeSite = TestDataFactory.createIntakeSiteSingleIntake(1,caseList[0].Id,intakeCIRRecTypeId)[0];
        objIntakeSite.Case__c = caseList[0].Id;
        intakeList.add(objIntakeSite);
        
        Intake_Site__c objIntakeSite1 = TestDataFactory.createIntakeSiteSingleIntake(1,caseList[0].Id,intakeCIRRecTypeId)[0];
        objIntakeSite1.Case__c = caseList[0].Id;
        intakeList.add(objIntakeSite1);
        
        Intake_Site__c objIntakeSite2 = TestDataFactory.createIntakeSiteSingleIntake(1,caseList[0].Id,intakeCIRRecTypeId)[0];
        objIntakeSite2.Case__c = caseList[1].Id;
        intakeList.add(objIntakeSite2);
        insert intakeList;
        
        //Create records for Location
        List<Location_ID__c> listOfLocations = new List<Location_Id__c>();
        Location_ID__c objLocation1 = TestDataFactory.createLocationId('Id1','Loc1',1)[0];
        objLocation1.Previous_AAV_Status__c = 'Test';
        listOfLocations.add(objLocation1);
        
        Location_ID__c objLocation2 = TestDataFactory.createLocationId('Id2','Loc2',1)[0];
         objLocation2.Previous_AAV_Status__c = 'Test';
        listOfLocations.add(objLocation2);
        
        Location_ID__c objLocation3 = TestDataFactory.createLocationId('Id3','Loc3',1)[0];
        objLocation3.Previous_AAV_Status__c = 'Test';
        listOfLocations.add(objLocation3);
        insert listOfLocations;
        
        //Create records for Contract site 
        Contract_Site__c objContractSite = TestDataFactory.createConractSiteSingleIntake(1,listOfOpportunitiesToInsert[0].id)[0];
        objContractSite.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        objContractSite.Intake_Site__c = intakeList[0].Id;
        listContractSite.add(objContractSite);
        
        //Create records for Contract site 
        Contract_Site__c objContractSite1 = TestDataFactory.createConractSiteSingleIntake(1,listOfOpportunitiesToInsert[0].id)[0];        
        objContractSite1.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        objContractSite1.Intake_Site__c = intakeList[1].Id;
        listContractSite.add(objContractSite1);
        
        //Create records for Contract site 
        Contract_Site__c objContractSite2 = TestDataFactory.createConractSiteSingleIntake(1,listOfOpportunitiesToInsert[0].id)[0];        
        objContractSite2.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        objContractSite2.Intake_Site__c = intakeList[2].Id;
        objContractSite2.Location_ID__c = objLocation3.Id;
        listContractSite.add(objContractSite2);
        insert listContractSite;
        
        
    }
    
    /****************************************************************************
    * @description : To test delete Contract Site Records
    *****************************************************************************/
    @isTest
    static void testContractSiteIntakeSite(){
        //Intialize list of Contract Site to update records
        List<Contract_Site__c> listOfContractSites = [SELECT Id FROM Contract_Site__c];
        System.assertEquals(true,!listOfContractSites.isEmpty(),'Contract Site List should not be empty');
        delete listOfContractSites;
        
        
    }
          
    /****************************************************************************
    * @description : To test updateLocationCMGStatus
    *****************************************************************************/
    @isTest
    static void testUpdateLocationCMGValues(){
        //Intialize list of Contract Site records
        List<Contract_Site__c> listOfContractSites = [SELECT Id,Opportunity__c,Location_ID__r.CMG_Working_Status__c FROM Contract_Site__c];
        List<Location_ID__c> listOfLocations = [SELECT Id,CMG_Working_Status__c FROM Location_ID__c];
        //List<Contract_Site__c> updatedListOfContractSites = new List<Contract_Site__c>();
        // Update List MRC and List NRC values on contract site records
        //for(Contract_Site__c cs:listOfContractSites){
        listOfContractSites[0].Location_ID__c = listOfLocations[0].Id;
        listOfContractSites[1].Location_ID__c = listOfLocations[1].Id;
          //  updatedListOfContractSites.add(cs);
        //}
        test.startTest();
        // Run test on contract Site update
        update listOfContractSites;
        test.stopTest();
        System.assertNotEquals(listOfContractSites.size(),1,'Multiple Contract Sites present');
    }
    
    
}
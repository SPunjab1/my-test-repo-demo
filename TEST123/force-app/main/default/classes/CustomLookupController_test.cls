@isTest
public class CustomLookupController_test {
@isTest
    public static void testECCapture(){
        test.starttest();
		Equipment__c equ = new Equipment__c();
        equ.Name = 'a2c0U000001bsUK';
        equ.EquipmentModel__c = '2D4WC-21';
        equ.EquipmentManufacturer__c = 'ROSENBERGER';
        equ.Weight__c = 66.1;
        equ.Width__c = 22.8;
        equ.Length__c = 48;
        equ.Depth__c = 7.4;
		equ.EquipmentType__c = 'Antenna';
        insert equ;
		
        CustomLookupController.fetchRecords('Equipment__c','2D4WC-2',null);
        CustomLookupController.fetchRecords(null,'2D4WC-2',null);
        test.stopTest();
    }
}
/*********************************************************************************************
@Class Name : CallATOMSUpdateAPIQueueable
@author     : IBM
@date       : 09.06.20
@description: Queuable Class to call Update ASR and Update Circuit API
@Version    : 1.0
**********************************************************************************************/
public with sharing class CallATOMSUpdateAPIQueueable implements Queueable,Database.AllowsCallouts{
    
   
    
    public  Map<Id,List<Circuit__c>> caseIdCktToUpdateMap = new Map<Id,List<Circuit__c>>();
    public  Map<Id,List<ASR__c>> caseIdAsrToUpdateMap = new Map<Id,List<ASR__c>>();
    
    
        public CallATOMSUpdateAPIQueueable (Map<Id,List<ASR__c>> caseAsrLstMap ,Map<Id,List<Circuit__c>> caseCktLstMap){
      
            if(caseAsrLstMap != null){
                caseIdAsrToUpdateMap = caseAsrLstMap;
            }
            if(caseCktLstMap != null){
                caseIdCktToUpdateMap = caseCktLstMap;
            }   
               
  
        }
        public void execute(QueueableContext qc){
            
            List<Integration_Log__c> intLogToUpsert = new List<Integration_Log__c>();
            
            if(caseIdAsrToUpdateMap != null && !caseIdAsrToUpdateMap.isEmpty()){
             
             
               // AtomsService.IS_INVOKE_FROM_TRIGGER = false;
                 SFDCtoAtomsAPI.returnParamsToBatchAfterCallout objRet =SFDCtoAtomsAPI.updateASR2AtomsUsingLogsORAsr (caseIdAsrToUpdateMap, null);
                 //system.debug('value returned:'+objRet.logsToBeUpserted );
                 if(!objRet.logsToBeUpserted.isEmpty() && objRet.logsToBeUpserted.size()>0 ){
                     intLogToUpsert.addAll(objRet.logsToBeUpserted);
                    //upsert objRet.logsToBeUpserted;
                 }     
                  //system.debug('logs upserted :'+objRet.logsToBeUpserted);
                    
            }
            //Call update CIRCUIT
            if(caseIdCktToUpdateMap != null && !caseIdCktToUpdateMap.isEmpty()){
                //system.debug('Called API CKT Call Integration Method');
                //AtomsService.IS_INVOKE_FROM_TRIGGER = false;
                AtomsIntegrationAPI.returnParamsToBatchAfterCallout objRet =AtomsIntegrationAPI.sendUpdateCircuitReqToAtoms (caseIdCktToUpdateMap, null);
                //system.debug('value returnedCIRCUIT:'+objRet.logsToBeUpserted );
                if(!objRet.logsToBeUpserted.isEmpty() && objRet.logsToBeUpserted.size()>0 ){
                //upsert objRet.logsToBeUpserted;
                    intLogToUpsert.addAll(objRet.logsToBeUpserted);
                }     
                //system.debug('logs upserted CIRCUIT:'+objRet.logsToBeUpserted);
            }
            
            //system.debug('intLogToUpsert>>>'+intLogToUpsert);
            if(intLogToUpsert != null && !intLogToUpsert.isEmpty()){
                upsert intLogToUpsert;
            }
        
                
            
           //system.debug('Inside APIATOMS Method');
            
            
        }
        
    
}
@istest
public class OpportunityHistoryTrackerHandlerTest {
@testSetup
    static void testDataSetup() {
         //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //Intialize list of SI Savings Tracker to insert records
        List<SI_Savings_Tracker__c> listSISavingTracker = new  List<SI_Savings_Tracker__c>();
        //create Account records
        listAccount = TestDataFactory.createAccountSingleIntake(1); 
        //listAccount.add(objAccount);
        //insert Account
        insert listAccount;
         // Opportunity RecordType
        ID nlg_New_Site_Lease_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Site Lease').getRecordTypeId();
        List<Opportunity> oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, nlg_New_Site_Lease_RecordType);
        oppRecForContract[0].StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract[0].AccountId = listAccount[0].Id;
        oppRecForContract[0].Contract_Start_Date__c = System.today();
        oppRecForContract[0].Term_mo__c = 1.00;
        insert oppRecForContract;
        Lease_Enhancement__c lease = TestDataFactory.createLeaseEnhancement();
		lease.Lease_Intake__c = oppRecForContract[0].Id;
        insert lease;
    }
    
    @istest static void oppHistoryTracker(){
        Test.startTest();
        Opportunity opportunityRec = [Select id from Opportunity limit 1];
        opportunityRec.Current_Approver__c = UserInfo.getUserId();
        opportunityRec.CLIQ_Sub_Type__c = 'Lease';
        opportunityRec.Lease_or_AMD__c ='AMD';
		opportunityRec.Landlord__c ='ATC';
        opportunityRec.Negotiating_Vendor__c='Central Region';
        update opportunityRec;
        Test.stopTest();
    }
}
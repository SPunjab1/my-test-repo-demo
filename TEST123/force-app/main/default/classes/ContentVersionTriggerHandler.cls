/*********************************************************************************************
@author     : IBM
@date       : 31.05.19
@description: This is the trigger handler for ContentVersion object.
@Version    : 1.0
**********************************************************************************************/
    public with sharing class ContentVersionTriggerHandler extends TriggerHandler {
        private static final string ContentVersion_API = 'ContentVersion';
        public ContentVersionTriggerHandler() {
            super(ContentVersion_API);
        }
/***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/
    public override String getName() {
        return ContentVersion_API;
    }
/***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems) {
    }
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {

    }
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {

    }
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        /*********Method call to create Integration Log for Attachment*************/
        createIntegrationLogForAttachment(newItems);
        system.debug('comming in after insert'+newItems);
        generatePublicLinks(newItems);
    }

/*********************************************************************************************
* @Description :Method to create Work Order Task automatically based on workflow template
* @input params : Trigger.New ,newmap
**********************************************************************************************/
    public void createIntegrationLogForAttachment(List<Sobject> newContentVersionList) {
        List<ContentVersion> newItems = new List<ContentVersion>();
        newItems = (List<ContentVersion>) newContentVersionList;

        Map<String, ContentVersion> ContentDocumentIdContentVersionMap = new Map<String, ContentVersion>();
        Map<String, String> TaskIdContentDocumentIdMap = new Map<String, String>();
        List<Integration_Log__c> integrationLogsToInsert = new List<Integration_Log__c>();

        for(ContentVersion cv: newItems){
            ContentDocumentIdContentVersionMap.put(cv.ContentDocumentId, cv);
            
        }

        for(ContentDocumentLink cd: [SELECT Id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN : ContentDocumentIdContentVersionMap.keySet()]){
            TaskIdContentDocumentIdMap.put(cd.LinkedEntityId,cd.ContentDocumentId);
        }

        for(Case cs : [SELECT Id FROM Case WHERE Id IN : TaskIdContentDocumentIdMap.keySet() AND (Executable_Task_in_PIER__c = True OR Migrated_Case__c = True)]){
            Integration_Log__c il = new Integration_Log__c();
            il.Task__c = cs.Id;
            il.API_Type__c = 'Attachment';
            il.Attachment_Id__c = ContentDocumentIdContentVersionMap.get(TaskIdContentDocumentIdMap.get(cs.Id)).Id;
            il.Attachment_Name__c = ContentDocumentIdContentVersionMap.get(TaskIdContentDocumentIdMap.get(cs.Id)).Title ;
            integrationLogsToInsert.add(il);
            
            // Pritesh Added 
            System.debug('===> case id'+  cs.id + '== attachmentid' + il.Attachment_Id__c);
            
            //ContentVersion tmpCV = ContentDocumentIdContentVersionMap.get(TaskIdContentDocumentIdMap.get(cs.Id));
           
            //System.debug('==> Content:' +tmpCV.VersionData.toString()  );
                      
            //Blob csvFileBody =tmpCV.VersionData;
    		//String csvAsString= csvFileBody.toString();
            //System.debug('==> Content:' + csvAsString );
            
        }

        if(integrationLogsToInsert.size()>0){
            insert integrationLogsToInsert;
        }
       
    }
/*********************************************************************************************
* @Description :Method to Generate public link for attachment automatically based on attachment required field
* @input params : Trigger.New ,newmap
**********************************************************************************************/
        public static void generatePublicLinks(List<Sobject> newContentVersionList) {
            List<ContentVersion> newItems = new List<ContentVersion>();
            newItems = (List<ContentVersion>) newContentVersionList;
            
            List<String> ContentDocumentIdContentVersionLst = new List<String>();
            List<String> TaskIdContentDocumentIdLst = new List<String>();
            List<Case> LinkedEntityIdLst = new List<Case>();
            Integer maxSize = 100;
            for(ContentVersion cv: newItems){
                ContentDocumentIdContentVersionLst.add(cv.ContentDocumentId);
                
            }
            
            for(ContentDocumentLink cd: [SELECT Id,LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN : ContentDocumentIdContentVersionLst]){
                TaskIdContentDocumentIdLst.add(cd.LinkedEntityId);
            }
            Id CaserecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
            for(Case cs : [SELECT Id FROM Case WHERE RecordTypeId =: CaserecordTypeId AND Attachment_Required__c= True AND Id IN : TaskIdContentDocumentIdLst]){
                LinkedEntityIdLst.add(cs);
            }
            
            if(!LinkedEntityIdLst.IsEmpty() && LinkedEntityIdLst.Size()>0){
                List<ContentDistribution> cdListToInsert = new List<ContentDistribution>();
                for(ContentVersion cv : newItems){
                  	String NameSubString;
                    //name should not exceed 100 char.
                    if(cv.Title.length() > maxSize){
                        NameSubString = cv.Title.substring(0, maxSize);
                    }
                    else{
                       NameSubString = cv.Title;
                    }
                     system.debug('string--- '+NameSubString);
                    cdListToInsert.add(new ContentDistribution(
                        ContentVersionId = cv.Id,
                       // Name = cv.Title,
                       Name = NameSubString,
                        //PreferencesAllowOrignalDownload =TRUE,
                        PreferencesAllowPDFDownload=FALSE,
                        PreferencesAllowViewInBrowser=TRUE,
                        PreferencesExpires=FALSE,
                        PreferencesLinkLatestVersion=TRUE,
                        PreferencesNotifyOnVisit=FALSE,
                        PreferencesNotifyRndtnComplete=FALSE,
                        PreferencesPasswordRequired=FALSE            
                    ));
                }
                if(!cdListToInsert.IsEmpty())INSERT cdListToInsert;
                System.debug('--->'+cdListToInsert); //your ContentDistribution which will have public & download URL for the file.
            }
        }
    }
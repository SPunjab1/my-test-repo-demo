/*********************************************************************************************
@Class        : SavingsTrackerTestDataFactory
@author       : Matthew Elias
@date         : 13.08.2020 
@description  : This class is to create test data for testing Savings Tracker attachment solutions
@Version      : 1.0           
**********************************************************************************************/
@IsTest
public class SavingsTrackerTestDataFactory {
/***************************************************************************************
@Description : Method to create test Request
****************************************************************************************/
    public static void createRequest(Integer num){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User exec = new User(LastName='Exec',
                             Alias='Exec',
                             Email='exec@PODDEVtest.com',
                             Username='lwrekermnflkrsmnlkkgnsrlfsnv@PODDEVtest.com',
                             ProfileId=p.Id,
                             EmailEncodingKey='UTF-8',
                             LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US',
                             TimeZoneSidKey='America/Los_Angeles');
        insert exec;
        
        // query for profile ID
        p = [SELECT Id FROM Profile WHERE Name='Sourcing Manager']; 
        // create test user for Request
        User testUser = new User(LastName='Test',
                                 Alias='Test',
                                 Email='test@test.com',
                                 Username='testUser@UATtest.com',
                                 ProfileId=p.Id,
                                 Executive_Priority_Approver__c=exec.Id,
                                 EmailEncodingKey='UTF-8',
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US',
                                 TimeZoneSidKey='America/Los_Angeles');
        insert testUser;
        
        // create Account record for Request Supplier
        Account testAccount = new Account(Name='Test', PODE_Vendor__c=TRUE);
        insert testAccount;
        
        // create Project Owner record for Request
        Project_Owner__c testVP = new Project_Owner__c(Name='Test', Title__c='Test');
        insert testVP;
        
        // query for test record IDs to create Request
        User misc = [SELECT Id FROM User WHERE Name = 'Test' LIMIT 1];
        Account Supplier = [SELECT Id FROM Account WHERE Name LIKE 'Test'];
        Project_Owner__c VP = [SELECT Id FROM Project_Owner__c WHERE Name LIKE 'Test'];
        
        // create Request record(s)
        List<Request__c> testReqList = new List<Request__c>();
        for(Integer i = 0; i<num; i++){
            Request__c testReq = new Request__c(Description__c='TEST',
                                                Procurement_Group__c='Technology & Indirect Procurement',
                                                What_is_being_sourced_or_contracted__c='Hardware Products',
                                                Requestor__c=misc.id,
                                                VP_Project_Owner__c=VP.id,
                                                Budget_Owner__c=misc.id,
                                                Business_Point_of_Contact__c=misc.id,
                                                Business_Unit__c='Marketing',
                                                Sourcing_Project_Type__c='MSA',
                                                Supplier_Name__c=Supplier.Id,
                                                Estimated_Spend_in_first_12_months__c='No Spend',
                                                OwnerId=misc.Id);
            testReqList.add(testReq);
        }
        
        insert testReqList;
    }
    
/***************************************************************************************
@Description : Method to create test Savings Tracker record
****************************************************************************************/
    public static void createSavingsTracker(Set<Id> requestIDs){
        List<Savings_Tracker__c> newSTs = new List<Savings_Tracker__c>();
        for(Id req : requestIDs){
            Savings_Tracker__c testSavingsTracker = new Savings_Tracker__c(Request__c = req,
                                                                           Type_of_Baseline_for_this_Deal__c='Historical Cost');
            newSTs.add(testSavingsTracker);
        }
        insert newSTs;
    }
    
/***************************************************************************************
@Description : Method to create test ContentDocument records
****************************************************************************************/
    public static void createContentDocuments(Integer num){
        List<ContentVersion> CVList = new List<ContentVersion>();
        for(Integer i = 0; i<num; i++){
            ContentVersion contentVersion = new ContentVersion(
                Title = 'Test'+i,
                PathOnClient = 'Penguins.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true);
            
            CVList.add(contentVersion);
        }
        insert CVList;
    }
    
/***************************************************************************************
@Description : Method to create test ContentDocumentLink record
****************************************************************************************/
    public static void createContentDocumentLink(Map<Id,Set<Id>> CDMap){
        List<ContentDocumentLink> newCDLs = new List<ContentDocumentLink>();
        for(Id LE : CDMap.keySet()){
            for(Id CD: CDMap.get(LE)){
                ContentDocumentLink newCDL = new ContentDocumentLink(ContentDocumentId = CD,
                                                                     LinkedEntityId = LE,
                                                                     ShareType = 'V',
                                                                     Visibility = 'InternalUsers');
                newCDLs.add(newCDL);
            }
        }
        
        insert newCDLs;
     }
}
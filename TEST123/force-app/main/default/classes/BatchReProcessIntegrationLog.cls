/*********************************************************************************************
@author     : IBM
@date       : 20.06.19
@description: This class is re- processing the failed records from integration log table
@Version    : 1.0
**********************************************************************************************/

global class BatchReProcessIntegrationLog implements Database.Batchable<sObject>,Schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'SELECT Id,Reprocess__c,Status__c,Task__r.Task_Number__c,API_Order__c, API_Type__c FROM Integration_Log__c WHERE Reprocess__c = false AND Status__c = \'Failure\' ORDER BY API_Order__c, Task__r.Task_Number__c ASC';
        return Database.getQueryLocator(query);
    }
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())    
            Database.executeBatch(new BatchReProcessIntegrationLog(), 1);
    }

    global void execute(Database.BatchableContext BC, List<Integration_Log__c> scope) {

        List<Integration_Log__c> integrationLogsToUpdate  = new List<Integration_Log__c>();

        for(Integration_Log__c integrationLog: scope){
            integrationLog.Reprocess__c = true;
            integrationLogsToUpdate.add(integrationLog);
        }
        if(integrationLogsToUpdate != null && integrationLogsToUpdate.size() >0){
            database.update(integrationLogsToUpdate,false);
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}
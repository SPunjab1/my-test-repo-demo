public class SavingsTrackerSummaryPageExtension {
	private final Savings_Tracker__c ST;
    
    public SavingsTrackerSummaryPageExtension(ApexPages.StandardController stdController) {
        this.ST = (Savings_Tracker__c)stdController.getRecord();
    }
 
    public List<ContentDistribution> getContentDistributions(){
        List<ContentDocumentLink> contLinkList = new List<ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :ST.Id AND ContentDocument.FileType != 'SNOTE']);
        Set<Id> contDocuIdSet = new Set<Id>();
        for(ContentDocumentLink cdl : contLinkList){
            contDocuIdSet.add(cdl.ContentDocumentId);
        }
        Map<Id,ContentVersion> contVersMap = new Map<Id,ContentVersion>([SELECT Id, ContentDocumentId, ContentDocument.Title, IsLatest FROM ContentVersion WHERE IsLatest = TRUE AND ContentDocumentId IN :contDocuIdSet]);
        List<ContentDistribution> contDistList = new List<ContentDistribution>([SELECT Id, Name, DistributionPublicUrl, ContentDocumentId, ContentVersionId FROM ContentDistribution WHERE ContentVersionId IN :contVersMap.keySet()]);
        return contDistList;
    }
    
    public void createDistributions(){
        List<ContentDocumentLink> contLinkList = new List<ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :ST.Id AND ContentDocument.FileType != 'SNOTE']);
        Set<Id> contDocuIdSet = new Set<Id>();
        for(ContentDocumentLink cdl : contLinkList){
            contDocuIdSet.add(cdl.ContentDocumentId);
        }
        Map<Id,ContentVersion> contVersMap = new Map<Id,ContentVersion>([SELECT Id, ContentDocumentId, ContentDocument.Title, IsLatest FROM ContentVersion WHERE IsLatest = TRUE AND ContentDocumentId IN :contDocuIdSet]);
        List<ContentDistribution> tempDistList = new List<ContentDistribution>([SELECT Id, DistributionPublicUrl, ContentDocumentId, ContentVersionId FROM ContentDistribution WHERE ContentVersionId IN :contVersMap.keySet()]);
        Set<Id> contVersIdSet = new Set<Id>();
        for(ContentDistribution CD : tempDistList){
            contVersIdSet.add(CD.ContentVersionId);
        }
        List<ContentDistribution> contDistInsert = new List<ContentDistribution>();
        for(Id cv : contVersMap.keySet()){
            if(!contVersIdSet.contains(cv)){
                ContentDistribution CD = new ContentDistribution();
                CD.ContentVersionId = cv;
                CD.Name = contVersMap.get(cv).ContentDocument.Title;
                CD.PreferencesAllowOriginalDownload=TRUE;
                CD.PreferencesAllowPDFDownload=FALSE;
                CD.PreferencesAllowViewInBrowser=TRUE;
                CD.PreferencesExpires=FALSE;
                CD.PreferencesLinkLatestVersion=TRUE;
                CD.PreferencesNotifyOnVisit=FALSE;
                CD.PreferencesNotifyRndtnComplete=FALSE;
                CD.PreferencesPasswordRequired=FALSE;
                contDistInsert.add(CD);
            }
        }
        if(contDistInsert != null && contDistInsert.size() > 0){
            Insert contDistInsert;
        }
    }
}
/***********************************************************************************
@author      - IBM
@date        - 14.07.2020
@description - Class to get All Cases and Contract Sites Related with Opportunity. 
               This class is used in visualforce email template
@Version     - 1.0
************************************************************************************/
public with sharing class SI_TCMEmailComponentController{
    /**********************************************************************************
    * @description - Opportunity Id is passing from VF email template
    **********************************************************************************/
    public String opportunityId{get;set;}
    /**********************************************************************************
    * @description - Get all Cases related with Opportunity
    **********************************************************************************/
    public List<Case> lstOfCases{set;get;}
    /**********************************************************************************
    * @description - Get all Contract Sites related with Opportunity
    **********************************************************************************/
    public List<Contract_Site__c> lstOfContractSites{set;get;}
  
    /**********************************************************************************
    * @description - method fetching the Opportunity, Cases and Contract Site Records
    * @return lstOfOpportunity - List of Opportunities
    **********************************************************************************/
    public List<Opportunity> getAllData() {
        List<Opportunity> lstOfOpportunity = new List<Opportunity>();
        lstOfCases = new List<Case>();
        lstOfContractSites = new List<Contract_Site__c>();
        if(String.isNotBlank(OpportunityId)) {
            try {
                // SOQL to get Opportunity, Cases, Contract Site by using Opportunity Id
                lstOfOpportunity = [SELECT Name, StageName, Type, Owner.Name, AccountId, Account.Name, 
                                    Document_Type__c, Number_of_Contract_Sites__c, Contract_ID__c, CreatedBy.Name, 
                                    LastModifiedBy.Name, TPE__c,Region__c, Market_s__c, Term_mo__c, File_Name__c, 
                                    Document_Type_Abbreviation__c, Secondary__c,
                                    
                                    (SELECT CaseNumber, CreatedDate, Contact.Name, Description, Spend_Category__c,
                                     ContactPhone, ContactEmail, SI_AAV__c,AAV__c, Deselect_Reason__c, AAV_Status__c, 
                                     Change_Type__c, Event_End_Date__c, Event_Start_Date__c, 
                                     Sponsored_Executive_Name__c, CIQ_Attached__c, MW_to_AAV_Request__c,
                                     Multiple_Site_Request__c, Date_Notice_To_Proceed_Issued__c, 
                                     Replacement_Site_text__c, Requested_Circuit_Disconnect_Date__c, 
                                     Requested_Circuit_Install_Date__c, Selected_Vendor__r.Name 
                                     FROM Cases__r),
                                    
                                    (SELECT Name, Location_ID__c, Location_ID__r.Name, Location_ID__r.Region__c, 
                                     Location_Id__r.Market__c, Intake_Site__r.Circuit_ID__c, 
                                     Intake_Site__r.Location_ID__r.Name,A_Side__c, Location_Id__r.Eligibility_Status__c, 
                                     Location_Id__r.Ethernet_Installed__c , Location_Id__r.Backhaul_ETL__c, 
                                     Intake_Site__r.Microwave_Status__c, Intake_Site__r.Modernization_Plan__c,
                                     Intake_Site__r.Pending_Deselect__c, Intake_Site__r.Region__c,
                                     Intake_Site__r.Site_Status__c, Intake_Site__r.Case__c, 
                                     Intake_Site__r.Case__r.Type, Minimum_Bandwidth__c, Deployment_Schedule__c, 
                                     Intake_Site__r.A_Side__c, Intake_Site__r.Controlling_Site__c, 
                                     Intake_Site__r.Download_Speed__c, Intake_Site__r.Case__r.CaseNumber,
                                     Ordering_Guide_ID__c,Request_Type__c,Term_mo__c,
                                     Location_ID__r.T_Builder_ID__c
                                     FROM Contract_Sites__r)
                                    
                                    FROM Opportunity 
                                    WHERE Id = :opportunityId];
                
                if(!lstOfOpportunity.isEmpty()) {                  
                    if(!lstOfOpportunity[0].Cases__r.isEmpty()) {
                        // get all Cases related to an Opportunity
                        lstOfCases.addAll(lstOfOpportunity[0].Cases__r);
                    }
                    if(!lstOfOpportunity[0].Contract_Sites__r.isEmpty()) {
                        // get all Contract Sites related to an Opportunity
                        lstOfContractSites.addAll(lstOfOpportunity[0].Contract_Sites__r);
                    }
                }
            }
            catch(Exception ex) {
                System.debug(LoggingLevel.ERROR,'Execption during fetchinh Opportunity, Case Or Contract Site Records '+ex.getMessage());
            }    
        }
        return lstOfOpportunity;
    }
}
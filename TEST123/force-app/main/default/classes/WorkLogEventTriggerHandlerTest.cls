/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : WorkLogEventTriggerHandlerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 27.08.2019
*  @Description      : Test class for WorkLogEventTriggerHandler
**********************************************************************************************************************
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class WorkLogEventTriggerHandlerTest {
/***************************************************************************************************
* @Description  : Test Method to test insertWorkLogEntry
***************************************************************************************************/
      static  testmethod void runInsertWorkLogEntry() {
          Id recTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
          Worklog_Entry__c workLogEntryToInsert = new Worklog_Entry__c();
          List<WorkLog_Event__e> WorklogEventLst = new List<WorkLog_Event__e>(); 
          List<case>caseList=TestDataFactory.createCase('OM AAV_Small Cell CIR Change',1,recTypeId);
          caseList[0].PIER_Work_Order_ID__c = '34567889';
          insert caseList;
          String notes='Test Comment';
          WorklogEventLst=TestDataFactory.createWorklog(notes);
          WorklogEventLst[0].WO_Detail_ID__c=caseList[0].PIER_Work_Order_ID__c;
          workLogEntryToInsert.Comment__c='Test Comment';
          workLogEntryToInsert.task__c=caseList[0].id;
          
           Test.startTest();
           insert workLogEntryToInsert;
           EventBus.publish(WorklogEventLst);
           Test.stopTest();
           Test.getEventBus().deliver();
    }
}
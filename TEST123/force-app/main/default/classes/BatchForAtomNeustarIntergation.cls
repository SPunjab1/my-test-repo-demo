/*********************************************************************************************
@author     : IBM
@date       : June 8 2020
@description: Batch class to implement callouts for Atoms/Neustar
@Version    : 1.0  
**********************************************************************************************
@Version    : 2.0 -- June 23 2020
@description: Implemented the logic to call AtomsIntegration for Circuit API      
@author     : IBM             
**********************************************************************************************/
global class BatchForAtomNeustarIntergation implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{ 
    
    static List<String> createAsrType = new List<String>();
    static List<String> upgradeAsrType = new List<String>();
    static List<String> updateAsrType = new List<String>();
    static List<String> disconnectAsrType = new List<String>();
    Public static List<String> circuitType = new List<String>(); 
    public Boolean bTestSingleRecord = false;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,OM2_Status__c,LastModifiedDate,Previous_Last_Modified_Date__c,OM_Integration_Status__c,PIER_Work_Order_ID__c ,(SELECT id,Accepted_Date__c,Case__r.CR_Number__c,FOC_Date__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Project__c,RPON__c,ASR_Number__c,Date_Confirmed__c,Test_CIR__c,ASR_Type__c,Forecast_Version__c,Order_Number__c,PON__c,Ordered_Date__c,Delivered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Circuit_Id__c,ATOMs_ASR_Id__c,Deliver__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Case__r.parent.PIER_Work_Order_ID__c from ASRs__r),(Select Id, BAN__c, A_Site_ID__c, A_Site_ID__r.Name, A_Site_Primary_Key__c, Bandwidth__c, Billing_Circuit_Id__c, Ordering_Guide_ID__c, Case__r.OwnerId, Circuit_Type__c, Category__c,Is_Meet_Point__c,LOA_Provided__c,Purchase_Order_Id__c,User_Ciruit_ID__c,Vendor_Name__c,Z_Site_ID__c, Z_Site_ID__r.Name, Z_Site_Primary_key__c, Vendor_Name_Formula__c,Controlling_Site__c, Required_Fields_For_Add_Circuit_Present__c from Circuits1__r where API_Status__c IN( Null, \'Failure\')) from Case WHERE OM_Integration_Status__c IN ( \'Start Circuit Integration\',\'Start Integration\',\'Circuit Success\') AND RecordType.DeveloperName = \'Work_Order_Task\' AND Id = \'5003F000007KEwF\' AND LastModifiedDate !=: Previous_Last_Modified_Date__c ';  
        String query;
        Id thisJobId;
        Id thisClassId = Label.AtomsIntegrationClassID; 
        if(!Test.isRunningTest())
        	thisJobId = bc.getJobId();
        List<String> jobStatus = Label.AtomsJobStatus.Split(',');
        AsyncApexJob[] jobs = [select id from AsyncApexJob where id!=:thisJobId AND status IN: jobStatus AND ApexClassId=:thisClassId AND JobType='BatchApex'];
        if (jobs !=null && jobs.size() > 0) {
            query = 'Select Id from Circuit__c LIMIT 0';
        }
        else {
             query = Label.CircuitQuery;
             System.debug('query >>>>>>>>'+query );
                if(bTestSingleRecord){
                    query = Label.CircuitQuery;
             }
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())  Database.executeBatch(new BatchForAtomNeustarIntergation(), integer.valueOf(Label.CircuitBatchSize));
    }
    
    global void execute(Database.BatchableContext BC, List<Circuit__c> scope) {
        //Declare Variables
        Map<Id,List<Circuit__c>> createCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Circuit__c>> getCircuitMap = new Map<Id,List<Circuit__c>>();
        
        Set<Id> purchaseOrderSet = new Set<Id>();
        Map<Id, List<Integration_Log__c>> case_ILMap = new Map<Id, List<Integration_Log__c>>();
        getAsrTypeValues();
        Map<String, SwitchAPICallOuts__c> switchSetting = SwitchAPICallOuts__c.getAll();
        AtomsIntegrationAPI.returnParamsToBatchAfterCallout dmlActionsWrapperCircuit;
        AtomsIntegrationAPI.returnParamsToBatchAfterCallout dmlActionsWrapperCirUpdate;
        AtomsIntegrationAPI.returnParamsToBatchAfterCallout dmlActionsWrapperCirGet;
        //String query = Label.CircuitQuery + ' FOR UPDATE';
        //List<Circuit__c> circuitLst = Database.query(query);
        // Get the Cases for which Create Circuit API is required
        for(Circuit__c circuitRec : scope){
            if(switchSetting.get('Add Circuit') != NULL && switchSetting.get('Add Circuit').Execute_API_Call__c == True){
                if(circuitRec.Circuit_Type__c != NULL && (circuitType.CONTAINS(circuitRec.Circuit_Type__c)) && String.isBlank(circuitRec.Circuit_ID_Primary_Key__c) && String.isBlank(circuitRec.NNI_ID_Primary_Key__c)){
                    if(createCircuitMap.containsKey(circuitRec.Case__c)){
                        createCircuitMap.get(circuitRec.Case__c).add(circuitRec);
                    }else
                        createCircuitMap.put(circuitRec.Case__c, new List<Circuit__c>{ circuitRec});
                }
            }if(switchSetting.get('GetCircuit') != NULL && switchSetting.get('GetCircuit').Execute_API_Call__c == True){
                if(circuitRec.Circuit_Type__c != NULL && circuitRec.Circuit_Type__c.CONTAINS('Existing')){
                    if(getCircuitMap.containsKey(circuitRec.Case__c)){
                        getCircuitMap.get(circuitRec.Case__c).add(circuitRec);
                    }else
                        getCircuitMap.put(circuitRec.Case__c, new List<Circuit__c>{ circuitRec});
                }
            }
        }
            
        //Call Integration class for each API
        system.debug('createCircuitMap===>'+createCircuitMap);
        if(createCircuitMap != NULL && createCircuitMap.Values().Size() > 0){
            dmlActionsWrapperCircuit = AtomsIntegrationAPI.sendCreateCircuitReqToAtoms(createCircuitMap,'Add Circuit',case_ILMap);
        }
        if(getCircuitMap != NULL && getCircuitMap.Values().Size() > 0){
            dmlActionsWrapperCirGet = AtomsIntegrationAPI.getCircuitAPIToAtoms(getCircuitMap, 'Get Circuit',case_ILMap);
        }  

        //Call DML Methods for Add Circuit
        if(dmlActionsWrapperCircuit != NULL){
            AtomsIntegrationAPI.performCircuitDMLs(dmlActionsWrapperCircuit);
        }
        
        //Call DML Methods for Get Circuit
        if(dmlActionsWrapperCirGet != NULL){
            AtomsIntegrationAPI.performCircuitDMLs(dmlActionsWrapperCirGet);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        //Execute any post processing operations.
        if(!bTestSingleRecord)database.executebatch(new BatchForASRIntergation(), integer.valueOf(Label.ASRBatchSize));    
    }
    
    /********************************************************************************
    @Description :This method gets required picklist values from the Circuit/ASR objects
    /******************************************************************************/
    
    public static void getAsrTypeValues(){
        Schema.DescribeFieldResult circuitTypeFieldResult = Circuit__c.Circuit_Type__c.getDescribe();
        List<Schema.PicklistEntry> circuitTypePle = circuitTypeFieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry pickListVal : circuitTypePle ){
            if(pickListVal.getLabel().contains('Circuit:') || pickListVal.getLabel().contains('NNI')){
                circuitType.add(pickListVal.getLabel());
            }
        }   
    }
    
}
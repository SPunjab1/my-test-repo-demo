public class NLG_ApprovalflowCtl {
    public string[] selectedOppIds{get;set;}
    public List<Opportunity> selectedOpps{get; set;}
    public string retUrl{get; set;}
    public NLG_ApprovalflowCtl(ApexPages.StandardSetController listcontroller){
        selectedOppIds = new string[]{};
            selectedOpps = new List<Opportunity>();
        for(Opportunity opp : (Opportunity[])listcontroller.getSelected()){
            selectedOppIds.add(opp.Id);            
        }
        selectedOpps = [Select Id,Region__c,Market_s__c,Approval_Stage__c, StageName, Finance_Approval_Required__c, 
                        Last_Approval_Date__c, Current_Approver__c, OwnerId,NLG_Criteria__c,Site_Id__c,Reviewer_Comments__c,Approve_Reject_Reason__c, 
                        Lease_Liablity__c, Incremental_Lease_Liability__c,NSC_Status__c,Skip_to_Closeout__c,Site_Id__r.NSC_Status__c From Opportunity 
                        Where id in:selectedOppIds];
       
    }
    
    @InvocableMethod
    public static List<Opportunity> getOpportunities(String[] oppIdList) {
        List<Opportunity> oppList = [Select Id,Region__c,Market_s__c,Approval_Stage__c, StageName, Finance_Approval_Required__c, 
                           Last_Approval_Date__c, Current_Approver__c, OwnerId,NSC_Status__c From Opportunity 
                           Where id in:oppIdList];
                           return oppList;
    }

}
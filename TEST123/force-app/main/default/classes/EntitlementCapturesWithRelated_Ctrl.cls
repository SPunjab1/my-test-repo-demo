public class EntitlementCapturesWithRelated_Ctrl {
    @AuraEnabled
    public static boolean checkCreateAccess() {
       return Schema.sObjectType.Entitlement_Capture__c.isCreateable();
    }
    @AuraEnabled
    public static string fetchLeaseinfo(String leaseId) {
        system.debug('leaseId**'+leaseId);
        List<Contract> contractList   = [select Current_Lease_MLA__c from Contract where id = :leaseId];
        if(!contractList.isEmpty() && (contractList[0].Current_Lease_MLA__c == 'Crown Castle' || contractList[0].Current_Lease_MLA__c== 'SBC' ||
                  contractList[0].Current_Lease_MLA__c== 'SBA' || contractList[0].Current_Lease_MLA__c== 'ATC' || contractList[0].Current_Lease_MLA__c== 'Vertical Bridge') ){
            return contractList[0].Current_Lease_MLA__c;
        }
        else{
        return contractList[0].Current_Lease_MLA__c = 'Private LL' ;
        }   
    }
       
    @AuraEnabled 
    public static Map<String, String> getLeaseId(String siteID){
        Map<String, String> options = new Map<String, String>();
        List<Contract> contlist = [select id,Site_ID__c,Siterra_Status__c,name,ContractNumber,Lease_Title__c,Landlord__c from Contract where Site_ID__c =: siteID and Siterra_Status__c= 'Active'];
        for (Contract cnt: contlist) {
            
            //options.put(cnt.id, cnt.ContractNumber+(cnt.Lease_Title__c == null? '' : ('-'+cnt.Lease_Title__c))+ (cnt.Landlord__c == null? '' : ('-'+cnt.Landlord__c)));
        	options.put(cnt.id, cnt.Lease_Title__c);
        
        }
        return options;
    }
    @AuraEnabled 
    public static List<Equipment__c> getEquipements(String searchString){
		system.debug('searchString**'+searchString);
        String query = 'SELECT Id, Name, EquipmentModel__c, EquipmentManufacturer__c, Weight__c, Width__c, Length__c, Depth__c, EquipmentType__c FROM Equipment__c ';
        query += ' WHERE EquipmentModel__c LIKE ' + '\'' + String.escapeSingleQuotes(searchString.trim()) + '%\' ';
        List<Equipment__c> equilist = Database.query(query);
    	system.debug('equilist**'+equilist);
        return equilist;
    }
    
    @AuraEnabled
    public static string SaveECnECLnRADGrnd(String ECRecordString, String ECLRecordsStrings,string RADGroundStrings) {
        system.debug('ECRecordString**'+ECRecordString);
        system.debug('ECLRecordsStrings**'+ECLRecordsStrings);
        system.debug('RADGroundStrings**'+RADGroundStrings);
        if(!string.isBlank(ECRecordString)){
            system.debug('ECRecordString***'+ECRecordString);
            Map<String, Object> deserialized = (Map<String, Object>)JSON.deserializeUntyped(ECRecordString);
            system.debug('deserialized***'+deserialized);
            Entitlement_Capture__c ecinstance = new Entitlement_Capture__c();
            for(String str : deserialized.keyset()){
                ecinstance.put(str, deserialized.get(str));                
            }
            //Entitlement_Capture__c ecinstance = (Entitlement_Capture__c)System.JSON.deserialize(ECLRecordsStrings,Entitlement_Capture__c.class);
            system.debug('ecinstance***'+ecinstance);
            try{
                insert ecinstance;
                List<EntitleWrapper> entitleLineList = new  List<EntitleWrapper>();
                if(!string.isBlank(ECLRecordsStrings)){
                    entitleLineList = (List<EntitleWrapper>)System.JSON.deserialize(ECLRecordsStrings,List<EntitleWrapper>.class);
                    system.debug('entitleLineList-' + entitleLineList);
                    List<Entitlement_Line_Items__c> ECLineList = new List<Entitlement_Line_Items__c>();
                    for(EntitleWrapper ew: entitleLineList){
                        if(ew.EquipmentId != null){
                            Entitlement_Line_Items__c ecl = new Entitlement_Line_Items__c();
                            ecl.Count__c = (ew.Count != null && ew.Count != '') ?Decimal.valueof(ew.Count):0; 
                            ecl.Equipment_Model__c= ew.EquipmentId;
                            ecl.Entitlement_Capture__c = ecinstance.id;
                            ecl.Equipment_Model_Name__c = ew.EquipmentName;
                            ecl.Equipment_Type__c=ew.EquipmentType;
                            ecl.Length__c = (ew.Length != null && ew.Length != '') ?Decimal.valueof(ew.Length):0;
                            ecl.Width__c = (ew.Width != null && ew.Width != '') ?Decimal.valueof(ew.Width):0;
                            ecl.Depth__c = (ew.Depth != null && ew.Depth != '') ?Decimal.valueof(ew.Depth):0; 
                            ecl.Weight__c = (ew.Weight != null && ew.Weight != '') ?Decimal.valueof(ew.Weight):0; 
                            ecl.L_Times_W__c = (ew.LTimesW != null && ew.LTimesW != '') ?Decimal.valueof(ew.LTimesW):0;
                            ecl.Total_Weight__c = (ew.TotalWeight != null && ew.TotalWeight != '') ?Decimal.valueof(ew.TotalWeight):0;
                            ecl.Total_Surface__c = (ew.TotalSurface != null && ew.TotalSurface != '') ?Decimal.valueof(ew.TotalSurface):0;
                            if(ecl.Count__c>0 && ecl.Equipment_Model__c != null){
                                ECLineList.add(ecl);    
                            }
                        }  
                    }
                    system.debug('ECLineList***'+ECLineList);
                    if(!ECLineList.isEmpty()){
                        try{
                            insert ECLineList;
                        }catch(Exception e){
                            //get exception message
                            
                            throw new AuraHandledException(e.getMessage());
                        }
                    }
                }
                
                //inserting RAD Ground Gen data
                List<RADGroundWrapp> RADGroundWrappList = new  List<RADGroundWrapp>();
                List<RAD_GroundSpace__c> RADGroundList = new  List<RAD_GroundSpace__c>();
                if(!string.isBlank(RADGroundStrings)){
                    RADGroundWrappList = (List<RADGroundWrapp>)System.JSON.deserialize(RADGroundStrings,List<RADGroundWrapp>.class);
                    system.debug('RADGroundWrappList###-' + RADGroundWrappList);
                    
                    for(RADGroundWrapp ew: RADGroundWrappList){
                        //#############################
                        RAD_GroundSpace__c radG = new RAD_GroundSpace__c();
                        if(ew.SelectedType == 'RAD Center'){
                            if(ew.RADCenter != null){
                                radG.Type__c = 'RAD Center';
                                radG.RAD_Center_Type__c = ew.RADCenter;
                                radG.RAD_Sqft__c = Decimal.valueof(ew.RADSqft);
                                radG.Entitlement_Capture__c	 = ecinstance.id;
                                RADGroundList.add(radG);
                            }
                            
                        }
                        if(ew.SelectedType == 'Ground Space'){
                            if(ew.GroundSpace != null){
                                radG.Type__c = 'Ground Space';
                                radG.GroundSpace__c = Decimal.valueof(ew.GroundSpace);
                                radG.GroundSpace_Type__c = ew.GroundSpaceType;
                                radG.Entitlement_Capture__c	 = ecinstance.id;
                                RADGroundList.add(radG);
                            }
                        }
                        if(ew.SelectedType == 'Generator'){
                            if(ew.GeneratorTank != null){
                                radG.Type__c = 'Generator';
                                radG.Generator_Type__c = ew.GeneratorType;
                                radG.Seperate_Fuel_Tank__c = ew.GeneratorTank;
                                radG.Entitlement_Capture__c	 = ecinstance.id;
                                RADGroundList.add(radG);
                            }
                        }
                        
                    }
                }
                if(!RADGroundList.isEmpty()){
                    try{
                        insert RADGroundList;
                    }catch(Exception e){
                        //get exception message
                        
                        throw new AuraHandledException(e.getMessage());
                    }
                }
                
                return ecinstance.id;
            }
            catch(DmlException e) {
                //get DML exception message
                system.debug('Error**'+e.getStackTraceString());
                throw new AuraHandledException(e.getMessage());
            }catch(Exception e){
                //get exception message
                throw new AuraHandledException(e.getMessage());
            }
            
        }
        return null;
    }
    
    public class EntitleWrapper {
        @AuraEnabled public Id entitleid;
        @AuraEnabled public String Count ;
        @AuraEnabled public String EquipmentName;
        @AuraEnabled public String EquipmentId;
        @AuraEnabled public String EquipmentType;
        @AuraEnabled public String Length;
        @AuraEnabled public String Width;
        @AuraEnabled public String Depth;
        @AuraEnabled public String Weight;
        @AuraEnabled public String LTimesW;
        @AuraEnabled public String TotalSurface;
        @AuraEnabled public String TotalWeight;
        @AuraEnabled public Equipment__c equipment;
        @AuraEnabled public String EntitleCaptureId;
        //@auraEnabled public String Index;
        public EntitleWrapper() {
        }
    }
    public class RADGroundWrapp {
        @AuraEnabled public String SelectedType ;
        @AuraEnabled public String RADCenter;
        @AuraEnabled public String RADSqft;
        @AuraEnabled public String GroundSpaceType;
        @AuraEnabled public String GroundSpace;
        @AuraEnabled public String Generator;
        @AuraEnabled public String GeneratorType;
        @AuraEnabled public String GeneratorTank;
        
        public RADGroundWrapp() {
        }
    }
}
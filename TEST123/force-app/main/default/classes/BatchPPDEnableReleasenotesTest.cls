/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : BatchPPDEnableReleasenotesTest
*  @Version History  : 1.0
*  @Creation         : 22.07.2020
*  @Description      : Test class for BatchPPDEnableReleasenotesTest
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchPPDEnableReleasenotesTest {

    static testMethod void enableusersAccessFlag(){

        List<User> lstUser = new List<User>();
        User adminUser = TestDataFactory.createUser();
        adminUser.IsActive = true;
        adminUser.Hide_Release_Notes__c = true;
        User sourcingUser = TestDataFactory.createSourcingUser();
        sourcingUser.IsActive = true;
        sourcingUser.Hide_Release_Notes__c = true;
        lstUser.add(adminUser);
        lstUser.add(sourcingUser);
    insert(lstUser);
        
        Test.startTest();
        BatchPPDEnableReleasenotes batch = new BatchPPDEnableReleasenotes();
        Database.executeBatch(batch,200);
        Test.stopTest();
        }



    }
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : IntegrationLogTriggerHandler
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 24.06.2019
*  @Description      : Test class for IntegrationLogTriggerHandler
**********************************************************************************************************************
**********************************************************************************************************************/

@IsTest(SeeAllData=false)
public class IntegrationLogTriggerHandlerTest {
/***************************************************************************************************
* @Description  : Test Method to test CreateWorkOrder for all API Type
***************************************************************************************************/        
	 static testMethod void testCreateWorkOrder(){
        test.startTest();
        List<Integration_Log__c> integrationLogCreate = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Create Work Order');
        insert integrationLogCreate; 
        integrationLogCreate[0].Reprocess__c =true;
        integrationLogCreate[0].Status__c ='failure';
        update integrationLogCreate;  
        integrationLogCreate[0].Reprocess__c =false;
        integrationLogCreate[0].Status__c ='Success';
        update integrationLogCreate;
        List<Integration_Log__c> integrationLogGetTask = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Create Change Request');
        insert integrationLogGetTask; 
        integrationLogGetTask[0].Reprocess__c =true;
        update integrationLogGetTask;  
        List<Integration_Log__c> integrationLogOpen = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Create Service Request');
        insert integrationLogOpen;
        integrationLogOpen[0].Reprocess__c =true;
        update integrationLogOpen;  
        List<Integration_Log__c> integrationLogUpdate = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Create Workflow');
        insert integrationLogUpdate;
        integrationLogUpdate[0].Reprocess__c =true;
        update integrationLogUpdate;  
        List<Integration_Log__c> integrationLogClose = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Close Task');
        insert integrationLogClose;
        integrationLogClose[0].Reprocess__c =true;
        update integrationLogClose; 
        List<Integration_Log__c> integrationLogAssign = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Assign And Schedule Task');
        insert integrationLogAssign;
        integrationLogAssign[0].Reprocess__c =true;
        update integrationLogAssign;
        List<Integration_Log__c> integrationLogAdhoc = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Create Adhoc Task');
        insert integrationLogAdhoc;
        integrationLogAdhoc[0].Reprocess__c =true;
        update integrationLogAdhoc;
        List<Integration_Log__c> integrationLogNATask = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','NA Task');
        insert integrationLogNATask;
        integrationLogNATask[0].Reprocess__c =true;
        update integrationLogNATask;
       /* List<Integration_Log__c> integrationLogTerminate = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Terminate Work Order');
        insert integrationLogTerminate;
        integrationLogTerminate[0].Reprocess__c =true;
        update integrationLogTerminate;
         
        ContentVersion cv = new ContentVersion(
            versionData = Blob.valueOf( 'Hello World' ),
            title = 'test',
            pathOnClient = 'test.txt'
        );
        insert cv;
        ContentDocument cd = [ SELECT id FROM ContentDocument WHERE latestPublishedVersionId = :cv.id FOR VIEW ];
         
        List<Integration_Log__c> integrationLogAttachment = TestDataFactory.createIntegrationLogs('TIFS AAV CIR Retest','Attachment');
        insert integrationLogAttachment;
       
        ContentDocumentLink cdl = new ContentDocumentLink(
            linkedEntityId = integrationLogAttachment[0].id,
            contentDocumentId = cd.id,
            shareType = 'V'
        );
        insert cdl;
        integrationLogAttachment[0].Attachment_Id__c = cd.id;
        integrationLogAttachment[0].Reprocess__c =true;
        update integrationLogAttachment;
*/
        test.stopTest();

    } 

/***************************************************************************************************
* @Description  : Test Method to test CheckUpdateTaskAPIProcessingStatusFailure 
***************************************************************************************************/  
    static testMethod void testCheckUpdateTaskAPIProcessingStatusFailure(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=1;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cList = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        cList[0].Count_of_Update_Task_API_Processed__c=1;
        insert cList;
        String parentId = cList[0].Id;
        List<Case> taskList = [SELECT id FROM Case LIMIT 1];
        Integration_Log__c intLog = new Integration_Log__c (Task__c=taskList[0].id,Work_Order__c = cList[0].Id,
                                                            API_Type__c = 'Update Tasks');
        insert intLog;
        intLog.Status__c='Failure';
        update intLog;
        test.stopTest();
    }
/***************************************************************************************************
* @Description  : Test Method to test CheckUpdateTaskAPIProcessingStatusSuccess Scenario
***************************************************************************************************/    
    
    static testMethod void testCheckUpdateTaskAPIProcessingStatusSuccess(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=2;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cListnew = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        insert cListnew;
        String parentId = cListnew[0].Id;
        List<Case> taskList = [SELECT ID FROM Case where ParentId =: cListnew[0].Id LIMIT 1];
        for(Case tsk:taskList){
        tsk.Approval_Required__c= true; 
        }  
        update taskList;             
        Integration_Log__c intLog = new Integration_Log__c (Task__c=taskList[0].id,Work_Order__c = cListnew[0].Id,
                                                                   API_Type__c = 'Update Tasks');        
        insert intLog;
        intLog.Status__c='Failure';
        update intLog;
        intLog.Status__c='';
        update intLog;
        intLog.Status__c='Success';
        update intLog;   
        test.stopTest();            
    }
/***************************************************************************************************
* @Description  : Test Method to test checkApproverTaskAPIProcessing Scenario
***************************************************************************************************/     
    static testMethod void testcheckApproverTaskAPIProcessing(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=2;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cListnew = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        insert cListnew;
        String parentId = cListnew[0].Id;
        List<Case> taskListnew = new List<case>();
        case task = new case();
        task.parentId = clistnew[0].id;        
        taskListnew.add(task);
        insert taskListnew;
        Integration_Log__c intLog = new Integration_Log__c (Task__c=taskListnew[0].id,
                                                                   API_Type__c = 'Approve Task');
        insert intLog;
        intLog.Status__c='Success';
        intlog.Reprocess__c =true;
        update intLog;
        test.stopTest();
    }
/***************************************************************************************************
* @Description  : Test Method to test checkTriggerForApproverAPI 
***************************************************************************************************/
    static testMethod void testcheckTriggerForApproverAPI(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=1;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cListnew = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        insert cListnew;
        String parentId = cListnew[0].Id;
        List<Case> taskListnew = new List<case>();
        case task = new case();
        task.parentId = clistnew[0].id;
        task.Approval_Required__c= true;
        taskListnew.add(task);
        insert taskListnew;
        Integration_Log__c intLog = new Integration_Log__c (Task__c=taskListnew[0].id,
                                                                   API_Type__c = 'Update Tasks');
        insert intLog;
         intLog.Status__c='Success';
        update intLog;
        test.stopTest();
    }
    
/***************************************************************************************************
* @Description  : Test Method to test checkTriggerForApproverAPIFailure Scenario
***************************************************************************************************/     
    static testMethod void testcheckTriggerForApproverAPIFailure(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=2;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cListnew = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        insert cListnew;
        String parentId = cListnew[0].Id;
        List<Case> taskListnew = new List<case>();
        List<Case> taskList= [SELECT ID FROM Case where ParentId =: cListnew[0].Id LIMIT 1];
        for(Case tsk:taskList){
        tsk.Approval_Required__c= true;        
        }
        update taskList;
        Integration_Log__c intLog1 = new Integration_Log__c (Task__c=taskList[0].id, Status__c = 'Failure',
                                                                   API_Type__c = 'Update Tasks');
        insert intLog1;        
        intLog1.Status__c='Success';
        update intLog1;
        test.stopTest();
        
    }
/***************************************************************************************************
* @Description  : Test Method to test delete Scenario
***************************************************************************************************/    
    static testMethod void testDelete(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=2;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cListnew = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        insert cListnew;        
        List<Case> taskList= [SELECT ID FROM Case where ParentId =: cListnew[0].Id LIMIT 1];
        for(Case tsk:taskList){
        tsk.Approval_Required__c= true;       
        }
        update taskList;
        Integration_Log__c intLog = new Integration_Log__c (Task__c=taskList[0].id,
                                                                   API_Type__c = 'Update Tasks');
        insert intLog;        
        delete intLog;
        undelete intLog;
        test.stopTest();
    } 
/***************************************************************************************************
* @Description  : Test Method to test NA Task API Scenario
***************************************************************************************************/     
    static testMethod void testcheckNATaskAPI(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        Integer numOfCase=2;
        String workflowTmpName='AAV CIR Retest';
        List<Case> cListnew = TestDataFactory.createCase(workflowTmpName,numOfCase,recTypeId);
        test.startTest();
        insert cListnew;
        String parentId = cListnew[0].Id;
        List<Case> taskListnew = new List<case>();
        List<Case> taskList= [SELECT ID FROM Case where ParentId =: cListnew[0].Id LIMIT 1];
        for(Case tsk:taskList){
        tsk.Approval_Required__c= true;        
        }
        update taskList;
        Integration_Log__c intLog1 = new Integration_Log__c (Task__c=taskList[0].id, Status__c = 'Success',
                                                                   API_Type__c = 'NA Task');
        insert intLog1;        
        intLog1.Status__c='Failure';
        update intLog1;
        test.stopTest();
        
    }      
}
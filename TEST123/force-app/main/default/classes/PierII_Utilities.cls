/***********HEADER*****************
Method1: pullWorkOrderTasks
Description: Accepts the workOrder Id and return the relavant Tasks for a WorkOrder
Input (Mandatory) : Set<WorkOrderIds> - Passing only this input would pull both pier and non pier tasks.
Input (Optional):  PullPierTasksOnly = true >> Boolean parameter to pull only Pier tasks related to a WorkOrder
Input (Optional):  PullNONPierTasksOnly = true >> Boolean parameter to pull NON Pier tasks only related to a WorkOrder
Output: Map<WorkOrderId, List<WorkOrderTasks>>

Method2: pullprepostTasks
Description: Accepts the Task Id and return the Tasks that are relavant predecessor Tasks or followed Tasks
Input (Mandatory): Set<TaskIds>: Passing only this input would not pull any tasks.
Input (Mandatory): string tRetrieve: Possible values 'Predecessor', 'following' 
				   tRetrieve= 'Predecessor' >> retrieve respective pretasks/predecessor tasks for the input tasks
				   tRetrieve= 'following' >> retrieve respective following tasks for the input tasks
Output: Map<TaskId, List<Tasks>>

Method3: pullTaskWorklogs
Description: Accepts the Task Id and return the Tasks related worklogs object information
Input (Mandatory): Set<TaskIds>: Passing only this input would not pull any tasks.
Output: Map<TaskId, List<Worklog>>

Method4: pullTaskDocs
Description: Accepts the Task Id and return the Tasks related Files object information
Input (Mandatory): Set<TaskIds>: Passing only this input would not pull any tasks.
Output: Map<TaskId, List<ContentDocument>>

Author: Balaji Bodicherla
Date: June 12 2020
**********************************/
public with sharing class PierII_Utilities {
    
    Public static Map<Id,List<case>> pullWorkOrderTasks(Set<Id> wOrderIds, Boolean pullPierTasksOnly, Boolean pullNONPierTasksOnly) {
        try{
            //system.debug(wOrderIds);
            Map<Id, List<case>> tMap = New Map<Id, List<case>>();
            List<case> lstTaskCase = New List<case>();
            List<Id> workOrderlst = New List<Id>(wOrderIds);
            workOrderlst.sort();
            string squery = 'select Id,Task_Number__c,CaseNumber,Parent.CaseNumber,attachmentsFromTasks__c,Status,ParentId, transportTask__c,Owner.Name,Owner.Type, OwnerId from case where parentId in:wOrderIds ';
            if(pullPierTasksOnly){
                squery= squery+'AND Send_To_PIER__c=true '+ 'order by parentId asc';
            }else if(pullNONPierTasksOnly){
                squery= squery+'AND Send_To_PIER__c=false '+ 'order by parentId asc';
            }else {
                squery= squery+'order by parentId asc';
            }

            if(workOrderlst.size()>0){
                lstTaskCase = Database.query(squery);
                //system.debug(lstTaskCase.size());
            }
            if(lstTaskCase.size() >0){
                for(Id workOrder: workOrderlst){
                    List<case> lst = New List<case>();
                    for(integer i=0; i< lstTaskCase.size(); i++){
                        if(workOrder == lstTaskCase[i].parentId){
                            lst.add(lstTaskCase[i]);
                            lstTaskCase.remove(i);
                            i--;
                        }
                    }
                    //system.debug(lst.size());
                    tMap.put(workOrder, lst);                    
                }
            }
            //system.debug(tmap.size());
            return(tmap);
        }
        catch(Exception e){            
            throw e ; 
        }        
    }
    Public static Map<Id,List<case>> pullprepostTasks(set<Id> taskIds, string tRetrieve) {
        try{
            List<case> sAllTasks = New List<case>();
            set<Id> workOrderid = New set<Id>();
            Map<Id, List<case>> smapTasks= New Map<Id, List<case>>();
            //string sQuery = 'select Id,parentId,Task_Number__c from case where Id in:taskIds order by parentId asc';
            List<case> lstInputTasks = [select Id,parentId,Task_Number__c from case where Id in:taskIds WITH SECURITY_ENFORCED order by parentId];
            if(lstInputTasks.size()>0){
                for(case task: lstInputTasks){
                    workOrderid.add(task.parentId);
                }
            }
            
            if(workOrderid.size()>0){
            sAllTasks = [select Id,parentId,Task_Number__c from case where parentId in:workOrderid WITH SECURITY_ENFORCED order by parentId,Task_Number__c];
            }
            for(case task: lstInputTasks){
                List<case> lsttemp = New List<case>();
                for(case alltasks: sAllTasks){
                    if(task.parentId == alltasks.parentId){
                        if(tRetrieve=='Predecessor' && task.Task_Number__c > alltasks.Task_Number__c){
							lsttemp.add(alltasks);
                        }else if(tRetrieve=='following' && task.Task_Number__c < alltasks.Task_Number__c){
                            lsttemp.add(alltasks);
                        }                            
                    }
                }
                smapTasks.put(task.Id,lsttemp);
            }
           //system.debug(smapTasks);
           //system.debug(smapTasks.size());
           return smapTasks;
        }
        catch(Exception e){            
            throw e ; 
        } 
    }
    
    Public Static Map<Id,List<Worklog_Entry__c>> pullTaskWorklogs(Set<Id> taskIds) {
        try{
            //system.debug(taskIds);
            Map<Id, List<Worklog_Entry__c>> taskWorkLogMap = New Map<Id, List<Worklog_Entry__c>>();
            List<Worklog_Entry__c> lstWorkLogs = New List<Worklog_Entry__c>();
            List<Id> staskIds = New List<Id>(taskIds);
            staskIds.sort();
            string squery = 'select Id,Task__c,Comment__c from Worklog_Entry__c where Task__c in:taskIds order by Task__c';
            if(taskIds.size()>0){
                lstWorkLogs = Database.query(squery);
                //system.debug(lstWorkLogs.size());
            }
            if(lstWorkLogs.size() >0){
                for(Id taskId: staskIds){
                    List<Worklog_Entry__c> lst = New List<Worklog_Entry__c>();
                    for(integer i=0; i< lstWorkLogs.size(); i++){
                        if(taskId == lstWorkLogs[i].Task__c){
                            lst.add(lstWorkLogs[i]);
                            lstWorkLogs.remove(i);
                            i--;
                        }
                    }
                    //system.debug(lst.size());
                    taskWorkLogMap.put(taskId, lst);                    
                }
            }
            //system.debug(taskWorkLogMap);
            return(taskWorkLogMap);
        }
        catch(Exception e){            
            throw e ; 
        }        
    }
    
        Public static Map<Id,List<ContentDocumentLink>> pullTaskDocs(Set<Id> taskIds) {
        try{
            //system.debug(taskIds);
            Map<Id, List<ContentDocumentLink>> taskDocsMap = New Map<Id, List<ContentDocumentLink>>();
            List<ContentDocumentLink> lstDocs = New List<ContentDocumentLink>();
            List<Id> staskIds = New List<Id>(taskIds);
            staskIds.sort();
            string squery = 'SELECT Id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId in:staskIds order by LinkedEntityId';
            if(staskIds.size()>0){
                lstDocs = Database.query(squery);
                //system.debug(lstDocs.size());
            }
            if(lstDocs.size() >0){
                for(Id taskId: staskIds){
                    List<ContentDocumentLink> lst = New List<ContentDocumentLink>();
                    for(integer i=0; i< lstDocs.size(); i++){
                        if(taskId == lstDocs[i].LinkedEntityId){
                            lst.add(lstDocs[i]);
                            lstDocs.remove(i);
                            i--;
                        }
                    }
                    //system.debug(lst.size());
                    taskDocsMap.put(taskId, lst);                    
                }
            }
            //system.debug(taskDocsMap);
            return(taskDocsMap);
        }
        catch(Exception e){            
            throw e ; 
        }        
    }
}
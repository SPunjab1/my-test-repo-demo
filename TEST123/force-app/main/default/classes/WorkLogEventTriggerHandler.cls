/*********************************************************************************************
@author       : IBM
@date         : 19.06.2019
@description  : This is the trigger handler toInsert Worklog for work order task from DWH
@Version      : 1.0
V1: Balaji B: US# PTSP-3044:Added Dedup logic to upsert the Worklogs.
**********************************************************************************************/

public with sharing class WorkLogEventTriggerHandler {
    
/*********************************************************************************************
* @Description :Method create worklog from DWH
* @input params : Trigger.New
**********************************************************************************************/
    public static void insertWorkLogEntry(List<WorkLog_Event__e> newItems) {
        Set<String> pierTaskIdSet = new Set<String>();
        Map<String, Id> taskIdByCaseIdMap = new Map<String, Id>();
        Map<String, WorkLog_Event__e > taskEventMap = new Map<String, WorkLog_Event__e >();        
        List<Worklog_Entry__c> workLogEntryToInsert = new List<Worklog_Entry__c>();
        List<Integration_Log__c> logLstToInsert = new List<Integration_Log__c>();
        Map<Id,Id> taskLogMap = new Map<Id,Id>();
                 
        for (WorkLog_Event__e event : newItems) {
            if(event.WO_Detail_ID__c != null && event.WO_Detail_ID__c != '') {
                pierTaskIdSet.add(event.WO_Detail_ID__c);                                
            }
        }
        if(pierTaskIdSet.size() > 0){
            for(Case caseObj : [SELECT Id,PIER_Work_Order_ID__c FROM Case WHERE PIER_Work_Order_ID__c IN: PierTaskIdSet]){
                taskIdByCaseIdMap.put(caseObj.PIER_Work_Order_ID__c,caseObj.Id); //replaced PIER_Work_Order_Task_ID__c with PIER_Work_Order_ID__c
            }
        }        
        for (WorkLog_Event__e event : newItems) {
        
            if(event.WO_Detail_ID__c != null && event.WO_Detail_ID__c != '') {
                Worklog_Entry__c worklogEntry = new Worklog_Entry__c();
                
                if((event.WL_Description__c).length() > 32767) { worklogEntry.Comment__c = (event.WL_Description__c).substring(0, 32767);}
                else {
                    worklogEntry.Comment__c = event.WL_Description__c;
                }
                worklogEntry.Pier_Worklog_Id__c = event.Pier_Worklog_Id__c; //PTSP-3044 ; Added this to use upsert instead of Insert
                worklogEntry.User_Code__c = event.User_Code__c;
                worklogEntry.Task__c = taskIdByCaseIdMap.get(event.WO_Detail_ID__c);
                workLogEntryToInsert.add(worklogEntry);                                
                taskEventMap.put(taskIdByCaseIdMap.get(event.WO_Detail_ID__c), event);                
            }
        }
        

        if(workLogEntryToInsert.size() > 0){
            //PTSP-3044: updated the insert to upsert as Middleware is not capable of adding a dedupe logic in their system
              Schema.SObjectField externalId = Worklog_Entry__c.Fields.Pier_Worklog_Id__c;
              List<Database.UpsertResult> updateResults = database.upsert(workLogEntryToInsert,externalId, false);
           	  //List<Database.SaveResult> updateResults = database.insert(workLogEntryToInsert, false);	
            for(Integer i=0;i<updateResults.size();i++){
                Integration_Log__c errorLog;
                if(!updateResults.get(i).isSuccess() || Test.isRunningTest()){                                    
                    errorLog = new Integration_Log__c();                    
                    String errorMsg = '';
                    for(Database.Error error : updateResults.get(i).getErrors()){
                        errorMsg += error.getMessage() +'\n';
                    }
                    errorLog.Status__c = 'Failure';
                    errorLog.Response_Message__c = errorMsg;                    
                    if((workLogEntryToInsert.get(i).Comment__c).length() > 32767){
                        errorLog.Worklog_Description__c=(workLogEntryToInsert.get(i).Comment__c).substring(0, 32767) ;     
                    }  
                    else {
                        errorLog.Worklog_Description__c = workLogEntryToInsert.get(i).Comment__c;
                    }
                    if((String.valueOf(taskEventMap.get(workLogEntryToInsert.get(i).task__c ))).length() > 32767){
                        errorLog.Request__c=(String.valueOf(taskEventMap.get(workLogEntryToInsert.get(i).task__c ))).substring(0, 32767);
                    }
                    else{   
                        errorLog.Request__c = String.valueOf(taskEventMap.get(workLogEntryToInsert.get(i).task__c ));   
                    }   
                    errorLog.API_Type__c = 'DWH Worklog';
                    errorLog.Task__c = workLogEntryToInsert.get(i).Task__c;                         
                    logLstToInsert.add(errorLog);                    
                }
            }
            try{    
                    if(logLstToInsert != null && logLstToInsert.size() > 0){
                        database.Insert(logLstToInsert, false);
                    }
                }catch(exception ex){throw ex;}         
        }
    }
}
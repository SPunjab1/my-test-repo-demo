/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : BatchIntegration_Log_PurgeTest
*  @Author           : Balaji
*  @Version History  : 1.0
*  @Creation         : 12.08.2020
*  @Description      : Test class for BatchIntegration_Log_PurgeTest
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchIntegration_Log_PurgeTest {
    static testMethod void TestMethod1(){ 
        
        List<Integration_Log__c> lstInteg = New List<Integration_Log__c>();
        Integer numOfCase=1;
        ID workOrderRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<case> caseLst = TestDataFactory.createCase('TIFS TDM Disconnect(s)',numOfCase,workOrderRecordType);
        List<Integration_Log__c> integrationLogs = new List<Integration_Log__c>(); 
        String workflowTmpName='TIFS TDM Disconnect(s)';
        String API_Type='Create Work Order';    
        caseLst[0].Total_PIER_task_in_Workflow__c= 1;
        caseLst[0].Total_Approval_Required_Tasks__c= 1;
        caseLst[0].User_Description__c = 'test';
        caseLst[0].project_name__c= 'TestChange request';
        caseLst[0].PIER_Work_Order_Task_ID__c = '123445';
        caseLst[0].Status = 'Closed';
        List<Location_id__c> locList = TestDataFactory.createLocationId('19T0087','test123',1);
        insert locList;

        if(locList.size()>0){
            caseLst[0].Location_IDs__c=locList[0].id;
        }
        insert caseLst;
        Integration_Log__C log = New Integration_Log__C();
        //log.API_Type__c='Create Change Request';
        log.Task__c=caseLst[0].Id;
        lstInteg.add(log);
        for(Integer i=0; i<10; i++){
            Integration_Log__C log1 = New Integration_Log__C();
            //log1.API_Type__c='Create Change Request';
            log1.Work_Order__c= caseLst[0].Id;
            lstInteg.add(log1);
        }
        Insert(lstInteg);
        
        Test.startTest();        
        BatchIntegration_Log_Purge BatchIntegLog = New BatchIntegration_Log_Purge();
        database.executeBatch(BatchIntegLog);         
        Test.stopTest();
    }
}
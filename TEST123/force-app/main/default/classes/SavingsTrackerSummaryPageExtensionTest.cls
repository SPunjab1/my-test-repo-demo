@isTest
public class SavingsTrackerSummaryPageExtensionTest {
    @isTest static void testContentDistributionAquisition(){
        SavingsTrackerTestDataFactory.createRequest(1);
        Map<Id,Request__c> reqMap = new Map<Id,Request__c>([SELECT Id FROM Request__c]);
        
        SavingsTrackerTestDataFactory.createSavingsTracker(reqMap.keyset());
        Savings_Tracker__c ST = [SELECT Id FROM Savings_Tracker__c LIMIT 1];
        
        SavingsTrackerTestDataFactory.createContentDocuments(3);
        Map<Id,ContentDocument> cdMap = new Map<Id,ContentDocument>([SELECT Id FROM ContentDocument]);
        
        Map<Id,Set<Id>> cdlMap = new Map<Id,Set<Id>>();
        cdlMap.put(ST.Id, cdMap.keySet());
        
        SavingsTrackerTestDataFactory.createContentDocumentLink(cdlMap);
        
        ApexPages.StandardController stdCtl = new ApexPages.StandardController(ST);
        
        SavingsTrackerSummaryPageExtension ext = new SavingsTrackerSummaryPageExtension(stdCtl);
        
        ext.createDistributions();
        List<ContentDistribution> cdistList = ext.getContentDistributions();
        
        System.assertEquals(3,cdistList.size());
    }
}
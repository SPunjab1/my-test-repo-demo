/*********************************************************************************************
@author       : IBM
@date         : 06.06.19 
@description  : This is the class for Shift task order lightning component
@Version      : 1.0        
**********************************************************************************************/
public class ShiftTaskOrderHandler {
    public static boolean isShiftTaskEnable;
    
/***************************************************************************************************
* @Description  : Method to update Task order onClick of Shift Task order button on Work Order
* @input params : recordId
***************************************************************************************************/
    @AuraEnabled
    public static void updateShiftTaskOrder(List<Id> cIds, Boolean revertFlag){
        List<Case> updateCases = new List<Case>();
        Map<id,Case> updateCasesMap=new Map<id,Case>();
        updateCasesMap.putAll([select Id, Task_Number__c, Shift_Task_Order__c,Task_Number_Backup__c FROM Case WHERE Parent.Id IN:cIds]);  
        if(updateCasesMap!= NULL && !updateCasesMap.isEmpty()){
            system.debug('revertflag::'+ revertFlag 
                        + 'Id'+ cIds );
            for(Case cs : updateCasesMap.values()){
                
				if(!revertFlag){
                    system.debug('coming--'+revertFlag);
					cs.Task_Number_Backup__c = cs.Task_Number__c;
                	cs.Task_Number__c = cs.Shift_Task_Order__c ;  
                }else{
                    system.debug('coming '+revertFlag);
                    if(cs.Task_Number_Backup__c != null){
						cs.Task_Number__c = cs.Task_Number_Backup__c;                            
                    }

                }
                updateCases.add(cs);
            }
            database.update(updateCases,true);
            system.debug('updated '+updateCases);
        }
    }
    
/***************************************************************************************************
* @Description  : Method to enable Shift Task button based on Shift Task Order
* @input params : recordId
***************************************************************************************************/
    @AuraEnabled
    public static boolean enableTaskShiftOrder(Id cIds){
        list<Case> shiftChildCaseList = new list<Case>();
        shiftChildCaseList = [select Id, Shift_Task_Order__c FROM Case WHERE Parent.Id =:cIds AND Shift_Task_Order__c != NULL];
        if(shiftChildCaseList!= NULL && !shiftChildCaseList.isEmpty()){
            isShiftTaskEnable = FALSE;
        }
        else{
            isShiftTaskEnable = TRUE;
        }
        return isShiftTaskEnable ;
    }
}
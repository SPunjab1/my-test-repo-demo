/******************************************************************************
@author      : IBM
@date        : 15.10.2020
@description : Class to update Primary & Seconday Contract IDs on Close of Work Order Task
@Version     : 1.0
*******************************************************************************/
public with sharing class SI_UpdateCaseLocation {
    
    /**************************************************************************
	* @description Invocable method to be called from Case process builder
	* @param listOfCases - List<Case>
	**************************************************************************/
    @InvocableMethod
    public static void doUpdateAction(List<Case> listOfCases) {
        //Intialize list to update Location
        List<Location_ID__c> updateLocationList = new List<Location_ID__c>();
        //Initialize set to add Case records.        
        Set<Id> caseIds = new Set<Id>();
        for(Case caseRecord : listOfCases){
            caseIds.add(caseRecord.Id);
        }
        system.debug('caseIds====:'+ caseIds);
        
        //Query required fields from Case based on cases added in Set.
        List<Case> caseList = [Select Id,ParentId,Parent.TPE_Request_Number__c,Parent.TPE_Request_Number__r.Single_Intake_Team__c,
                               Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c,Parent.Location_IDs__r.Secondary_Vendor_Id__c,
                               Parent.Location_IDs__r.Secondary_Vendor_Name__c,Parent.Location_IDs__r.AAV_Primary_Contract_ID__c,
                               Parent.Location_IDs__r.Primary_Vendor_Id__c,Parent.Location_IDs__r.Primary_Vendor_Name__c,
                               Parent.Location_IDs__r.Internally_Contracted__c
                               from Case where Id IN: caseIds];      
        //check if listOfCaseIds is not blank
        if(!caseList.isEmpty()) {
            for(Case caseRec : caseList) {                
                //check if ParentId & AAV Secondary & Primary Contract Id are not null
                Boolean isUpdateRequired = false;
                if(String.isNotBlank(caseRec.Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c) &&
                   String.isNotBlank(caseRec.Parent.Location_IDs__r.Secondary_Vendor_Id__c) &&
                   String.isNotBlank(caseRec.Parent.Location_IDs__r.Secondary_Vendor_Name__c)) {                                                   
                       //populate 3 fields on location
                       caseRec.Parent.Location_IDs__r.AAV_Primary_Contract_ID__c = caseRec.Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c;
                       caseRec.Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c = '';
                       caseRec.Parent.Location_IDs__r.Primary_Vendor_Id__c = caseRec.Parent.Location_IDs__r.Secondary_Vendor_Id__c;
                       caseRec.Parent.Location_IDs__r.Secondary_Vendor_Id__c = '';
                       caseRec.Parent.Location_IDs__r.Primary_Vendor_Name__c = caseRec.Parent.Location_IDs__r.Secondary_Vendor_Name__c;
                       caseRec.Parent.Location_IDs__r.Secondary_Vendor_Name__c = '';
                       isUpdateRequired = true;
                   }                    
                if(caseRec.Parent.Location_IDs__r.Internally_Contracted__c == true) {
                    caseRec.Parent.Location_IDs__r.Internally_Contracted__c = false;
                    isUpdateRequired = true;
                }
                if(isUpdateRequired) {
                    updateLocationList.add(caseRec.Parent.Location_IDs__r);
                }
            }            
            if(!updateLocationList.isEmpty()) {
                try {
                    //update operation
                    SI_UtilityClass.callDMLOperations(updateLocationList,Label.SI_DML_Operations.split(';')[1]);
                } catch (DMLException ex) {
                    //Log exceptions
                    System.debug(LoggingLevel.INFO, 'Exception: ' + ex.getMessage()); 
                }
            }             
        }
    }
}
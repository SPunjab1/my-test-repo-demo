/******************************************************************************
@author      - IBM
@date        - 12.06.2020
@description - Case Trigger Helper class for single intake
@Version     - 1.0
*******************************************************************************/
public with sharing class SI_CaseTriggerHandlerHelper {
    
    //current date time constant
    private static final DateTime CURRENT_DATE_TIME = System.now();
    private static final Date CURRENT_DATE = System.today();
    
    /**************************************************************************
    * @description Method to update CaseMilestone when case status is not closed
    * @param listOfNewCases - List<Case>
    * @param oldCaseMap - Map<id,SObject>
    **************************************************************************/
     public static void closeMileStoneForOpenCases(List<Case> listOfNewCases, Map<Id,Case> oldCaseMap) {
         
         //set to hold filtered case ids
         Set<Id> setOfCaseIds = new Set<Id>();
         //list to hold milestone names
         List<String> listOfMilestoneName = new List<String>();
         //fetch milestone metadata records
         List<SI_CaseMilestone__mdt> listOfMilestoneData = [SELECT Milestone_to_Complete__c,
                                                            Stage_Name__c, Team_Name__c 
                                                            FROM SI_CaseMilestone__mdt];
         //iterate through list of cases
         for(Case caseRec : listOfNewCases) {
             //check if opportunity stage has been changed
             if(caseRec.Opportunity_Stage__c != oldCaseMap.get(caseRec.Id).Opportunity_Stage__c) {
                 //iterate on milestone data for each case record
                 for(SI_CaseMilestone__mdt milestonesMetadata : listOfMilestoneData) {
                     //check if case record team and related opportunity stage matches with
                     //metadata record
                     if(caseRec.Single_Intake_Team__c == milestonesMetadata.Team_Name__c 
                        && caseRec.Opportunity_Stage__c == milestonesMetadata.Stage_Name__c) {
                         //store milestone name to be closed
                         listOfMilestoneName.add(milestonesMetadata.Milestone_to_Complete__c);
                         //store case records for which milestones to be closed
                         setOfCaseIds.add(caseRec.Id);
                     }
                 }
             }
         }
         //check if setOfCaseIds is not empty
         if(!setOfCaseIds.isEmpty()) {
             //intialize a list to store the filtered miltestone which needs to be closed
             List<CaseMilestone> listOfCaseMilestoneToClose = new List<CaseMilestone>();
             //iterate on case milestones
             for(CaseMilestone caseMilestoneToClose : [SELECT Id, CompletionDate 
                                                       FROM CaseMilestone 
                                                       WHERE Case.Id IN :setOfCaseIds
                                                       AND MilestoneType.Name IN :listOfMilestoneName
                                                       AND IsCompleted = false]) {
                 //update the case milestone completion date to close the milestone
                 caseMilestoneToClose.CompletionDate = CURRENT_DATE_TIME;
                 //add in the list to perform dml
                 listOfCaseMilestoneToClose.add(caseMilestoneToClose);
             }
             //check if listofCaseMilestoneToClose is not empty
             if(!listOfCaseMilestoneToClose.isEmpty()) {
                 //update listOfCaseMilestoneToClose
                 //Label.SI_DML_Operations.split(';')[1] - Update Operation
                 SI_UtilityClass.callDMLOperations(listOfCaseMilestoneToClose, Label.SI_DML_Operations.split(';')[1]);
             }
         }
     }
    
    /**************************************************************************
    * @description   Method to close CaseMilestone when case status is closed
    * @param  listOfCases - List<Case>
    * @param  oldCaseMap - Map<id,SObject>
    **************************************************************************/
    public static void updateCaseMilestoneSI(List<Case> listOfCases, Map<Id,Case> oldCaseMap) {
        //set to hold filtered case ids
        Set<Id> setOfCaseIds = new Set<Id>();
        //iterate on list of cases
        for(Case caseRec : listOfCases) {
            //check if case status is changed, entitlement id is not blank,
            //single intake team is not blank and case is closed
            if(caseRec.Status != oldCaseMap.get(caseRec.Id).Status
               && String.isNotBlank(caseRec.EntitlementId)
               && String.isNotBlank(caseRec.Single_Intake_Team__c)
               && caseRec.IsClosed == true) {
                //store case records for which milestones to be closed
                setOfCaseIds.add(caseRec.Id);
            }
        }
        //check if filtered set of case ids is not empty
        if(!setOfCaseIds.isEmpty()) {
            //intialize a list to store the filtered miltestone which needs to be closed
            List<CaseMilestone> listOfCaseMilestoneToClose = new List<CaseMilestone>();
            //iterate on case milestone records
            for(CaseMilestone caseMilestoneToClose : [SELECT Id, CompletionDate 
                                                      FROM CaseMilestone 
                                                      WHERE CaseId IN :setOfCaseIds
                                                      AND IsCompleted = false]) {
                //update the case milestone completion date to close the milestone
                caseMilestoneToClose.CompletionDate = CURRENT_DATE_TIME;
                //add in the list to perform dml
                listOfCaseMilestoneToClose.add(caseMilestoneToClose);
            }
            //check if listofCaseMilestoneToClose is not empty
            if(!listOfCaseMilestoneToClose.isEmpty()) {
                //update listOfCaseMilestoneToClose
                //Label.SI_DML_Operations.split(';')[1] - Update Operation
                SI_UtilityClass.callDMLOperations(listOfCaseMilestoneToClose, Label.SI_DML_Operations.split(';')[1]);
            }
        }
    }

    /*********************************************************************************************
    * @description  - When Opportunity is removed from case record, delete contract sites associated with the 
                      intake sites related to that case.
                      When original opportunity is removed and a new one added, contract sites from first 
                      opportunity are deleted and added to the newly tagged opportunity
    * @param  newCaseList - List<Case>
    * @param  oldCaseMap - Map<Id,Case>
    **********************************************************************************************/
    public static void tocreateDeleteContractSiteOnOppoPopulated(List<Case> newCaseList, Map<Id,Case> oldCaseMap) {
        SI_UtilityClass.triggerWillRun = false;
        Set<Id> setOfAllCaseIds = new Set<Id>();
        Set<Id> setOfCaseIdsForCreateContractSite = new Set<Id>();
        for(Case objCase : newCaseList) {
            //whenever Opportunity Field Changes either Other Opportunity or Blank
            if(objCase.Opportunity__c != oldCaseMap.get(objCase.Id).Opportunity__c) {
                setOfAllCaseIds.add(objCase.Id);
                if(String.isNotBlank(objCase.Opportunity__c)) {
                    setOfCaseIdsForCreateContractSite.add(objCase.Id);
                }
            }
        }
        if(!setOfAllCaseIds.isEmpty()) {
            List<Contract_Site__c> listOfContractSiteToDelete = new List<Contract_Site__c>();
            List<Contract_Site__c> listOfContractSiteToInsert = new List<Contract_Site__c>();
            //fetch contract site default record type id
            Id contractSiteRecordTypeId = 
                SI_UtilityClass.getRecordTypeIdByDeveloperName('Contract_Site__c', Label.SI_ContractSiteRecordType);
            // Query on Intake Site to get Related All Contract Site Record related with Particular Case
            for(Intake_Site__c objIntake : [SELECT Case__r.Opportunity__c, Location_ID__c, Billable_Circuit__c,
                                            (SELECT Id FROM Contract_Sites__r) FROM Intake_Site__c WHERE 
                                            Case__c IN: setOfAllCaseIds LIMIT 50000]) {
                
                // Added to the List to Delete Old Contract Site records 
                if(!objIntake.Contract_Sites__r.isEmpty()) {
                    listOfContractSiteToDelete.addAll(objIntake.Contract_Sites__r);
                }
                // Create Contract Site Records Whenever Opportunity Updates
                if(setOfCaseIdsForCreateContractSite.contains(objIntake.Case__c)) {
                    Contract_Site__c objContractSite = new Contract_Site__c();
                    //create contract site with Contract Site Record Type
                    objContractSite.RecordTypeId = contractSiteRecordTypeId;
                    objContractSite.Location_ID__c = objIntake.Location_ID__c;
                    objContractSite.Opportunity__c = objIntake.Case__r.Opportunity__c;
                    objContractSite.Status__c = System.label.SI_ContractStatus;
                    objContractSite.Intake_Site__c = objIntake.Id;
                    //check if billable circuit is not blank on intake site, populate 
                    //same on contract site
                    if(String.isNotBlank(objIntake.Billable_Circuit__c)) {
                        objContractSite.Billable_Circuit__c = objIntake.Billable_Circuit__c;
                    }
                    listOfContractSiteToInsert.add(objContractSite);
                }
            }
            try {
                // Delete All Contract Site Records related with Intake Site with a particular Case 
                // (for Old Opportunity / Blank Opportunity on Case)
                if(!listOfContractSiteToDelete.isEmpty()) {
                    SI_UtilityClass.callDMLOperations(listOfContractSiteToDelete, 
                                                       Label.SI_DML_Operations.split(';')[2]);
                }
                // Create Contract Site Records related with Intake Site with a particular Case (for New Opportunity)
                if(!listOfContractSiteToInsert.isEmpty()) {
                    SI_UtilityClass.callDMLOperations(listOfContractSiteToInsert, 
                                                      Label.SI_DML_Operations.split(';')[0]);

                }
            } catch(Exception ex) {
                System.debug(LoggingLevel.ERROR, 'Exceptio in tocreateDeleteContractSiteOnOppoPopulated: ' + 
                             ex.getMessage());
            }
        }
    }
    
    /*********************************************************************************************
    * @description - Send mail to work order cases when macro deselect cases are updated with status
                        OM - Ready and location should be same as work order cases.
    * @param newCaseList - List<Case>
    * @param oldCaseMap - Map<Id,Case>
    **********************************************************************************************/
    public static void sendEmailToWorkOrderRecipients(List<Case> newCaseList, Map<Id,Case> oldCaseMap){
        //Set of location id
        Set<Id> setLocationId = new Set<Id>();
        //Fetch Case RecordType Id
        Id caseMacroDeRT = 
            SI_UtilityClass.getRecordTypeIdByDeveloperName('Case',System.Label.SI_CaseRT.split(';')[0]);
        Id caseWorkOrderRT = 
            SI_UtilityClass.getRecordTypeIdByDeveloperName('Case',System.Label.SI_CaseRT.split(';')[1]);
        for(Case objCase : newCaseList){
            //Case record type equals to macro deselect,status is OM ready and location should be populated
            if(objCase.RecordTypeId == caseMacroDeRT && 
               objCase.Status.contains(System.Label.SI_CaseStatus.split(',')[2]) 
               && objCase.Status != oldCaseMap.get(objCase.Id).Status 
               && String.isNotBlank(objCase.Location_IDs__c)){
                   setLocationId.add(objCase.Location_IDs__c);
            }
        }
        if(!setLocationId.isEmpty()){
            //Query work order case 
            List<Case> lstCaseRecords = [SELECT Workflow_Template__c, CaseNumber, SI_Region__c,
                                         Location_IDs__c, Location_IDs__r.Name
                                         FROM Case 
                                         WHERE RecordTypeId =: caseWorkOrderRT 
                                         AND Location_IDs__c IN: setLocationId 
                                         AND IsClosed = false 
                                         LIMIT 50000];
            if(!lstCaseRecords.isEmpty()){
                //Intialize list to add user's mail id
                List<String> lstUserMail = new List<String>();
                // Find all the user in the custom setting
                Map<String, SI_Configuration__c> mapUser = SI_Configuration__c.getAll();
                //Create key to get mail id from CS
                for(SI_Configuration__c objUserData : mapUser.values()){
                    if((objUserData.key__c.contains(System.Label.SI_UserKey.split(',')[0]) 
                        || objUserData.key__c.contains(System.Label.SI_UserKey.split(',')[1]))
                        && String.isNotBlank(objUserData.Value__c)){
                           lstUserMail.add(objUserData.Value__c);
                       }
                }
                if(!lstUserMail.isEmpty()) {
                    //Intialize list of emails
                    List<Messaging.SingleEmailMessage> lstemails = new List<Messaging.SingleEmailMessage>();
                    //query on template object
                    List<EmailTemplate> lstEmailTemplate = 
                        new List<EmailTemplate>([Select Subject,Body 
                                                 FROM EmailTemplate 
                                                 WHERE DeveloperName =: System.Label.SI_CaseEmailTemplate]);
                    if(!lstEmailTemplate.isEmpty()) {
                        EmailTemplate emailTemplate = lstEmailTemplate[0];
                        for(Case objCase : lstCaseRecords) {
                            // Email template body
                            String emailBody = emailTemplate.Body;
                            // Email template subject
                            String emailSubject = emailTemplate.Subject;
                            // Create email instance
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            email.setTemplateId(emailTemplate.Id);
                            //Add case number in subject
                            if(emailSubject.contains('{!Case.CaseNumber}')) {
                                emailSubject = 
                                    emailSubject.replace('{!Case.CaseNumber}',objCase.CaseNumber); 
                            }
                            email.setSubject(emailSubject); 
                            email.setToAddresses(lstUserMail);
                            //Add case number in mail body
                            if(String.isNotBlank(emailBody)) {
                                if(emailBody.contains('{!Case.CaseNumber}') ) {
                                    emailBody = 
                                        emailBody.replace('{!Case.CaseNumber}', objCase.CaseNumber);
                                }
                                //Add Workflow template in mail body
                                if(emailBody.contains('{!Case.Location_IDs__c}')) {
                                    if(String.isNotBlank(objCase.Location_IDs__c)) {
                                        emailBody = 
                                            emailBody.replace('{!Case.Location_IDs__c}',
                                                              objCase.Location_IDs__r.Name);
                                    } else {
                                        emailBody = 
                                            emailBody.replace('{!Case.Location_IDs__c}','');
                                    }
                                }
                                //Add Workflow template in mail body
                                if(emailBody.contains('{!Case.Workflow_Template__c}')) {
                                    if(String.isNotBlank(objCase.Workflow_Template__c)) {
                                        emailBody = 
                                            emailBody.replace('{!Case.Workflow_Template__c}',
                                                              objCase.Workflow_Template__c);
                                    } else {
                                        emailBody = 
                                            emailBody.replace('{!Case.Workflow_Template__c}','');
                                    }
                                }
                                //Add Region in mail body
                                if(emailBody.contains('{!Case.SI_Region__c}')) {
                                    if(String.isNotBlank(objCase.SI_Region__c)) {
                                        emailBody = 
                                            emailBody.replace('{!Case.SI_Region__c}', objCase.SI_Region__c);
                                    } else {
                                        emailBody = emailBody.replace('{!Case.SI_Region__c}', '');
                                    }
                                }
                                //Add related record link
                                if(emailBody.contains('{!Case.Link}')) {
                                    emailBody = emailBody.replace('{!Case.Link}', 
                                                                  URL.getSalesforceBaseUrl().toExternalForm()+'/'+objCase.Id);
                                }
                                email.setPlainTextBody(emailBody);
                            }
                            lstemails.add(email);
                        }
                        //Send mail to user and user's manager
                        if(!lstemails.isEmpty()) {
                            try {
                                Messaging.sendEmail(lstemails); 
                            }
                            catch(Exception ex) {
                                System.debug(LoggingLevel.ERROR,ex.getMessage());
                            } 
                        }
                    }
                }
            }
        }
    }
    
    /**************************************************************************
    * @description Method to fire case assignment rule 
    * @param newCaseList - List<Case> Case 
    * @param oldCaseMap - Map<Id,Case> Case 
    **************************************************************************/
    public static void changeCaseStatusToFireAssignmentRule(List<Case> newCaseList, Map<Id,Case> oldCaseMap) {
        //list to hold filtered case records to be updated
        List<case> listOfCasesToUpdate = new List<Case>();
        //Creating the DMLOptions for active assignment rules
        Database.DMLOptions dmlOpts = SI_UtilityClass.setDmlOptionForAssignmentRules();
        //fetch case status to compare
        //List<String> listOfStatusToCompare = new List<String>(Label.SI_CaseStatusAssignmentRule.split(','));
        //Iterate through case list
        for(Case objCase : [SELECT Id, Status, Type, Single_Intake_Team__c, Billable_Circuit_Count__c,
                            RecordTypeId, Location_IDs__c, CaseNumber, Workflow_Template__c, SI_Region__c,
                            Bulk_Request__c, Opportunity_Stage__c, Opportunity__c
                            FROM Case
                            WHERE Id IN :newCaseList]) {
            //Label.SI_OppStageName.split(';')[0] - 'Closed - Contract Executed'
            if(String.isNotBlank(objCase.Status)
               && objCase.Type == Label.SI_MacroDeselect
               && objCase.Opportunity_Stage__c != oldCaseMap.get(objCase.Id).Opportunity_Stage__c
               && objCase.Opportunity_Stage__c == Label.SI_OppStageName.split(';')[0] 
               && String.isNotBlank(objCase.Single_Intake_Team__c)
               && objCase.Opportunity__c != null ) {
                if(objCase.Bulk_Request__c == false && objCase.Billable_Circuit_Count__c > 0) {
                    objCase.Status = Label.SI_OMReady;
                    objCase.OM_Ready_Date__c = CURRENT_DATE;
                    objCase.setOptions(dmlOpts);
                    listOfCasesToUpdate.add(objCase);
                } else if(objCase.Bulk_Request__c == true || objCase.Billable_Circuit_Count__c == 0) {
                    objCase.Status = Label.SI_CloseComplete;
                    listOfCasesToUpdate.add(objCase);
                }
            } else if(String.isNotBlank(objCase.Status)
                      && objCase.Status != oldCaseMap.get(objCase.Id).Status
                      && objCase.Status.equals(Label.SI_OMReady)
                      && String.isNotBlank(objCase.Single_Intake_Team__c)) {
                objCase.OM_Ready_Date__c = CURRENT_DATE;
                objCase.setOptions(dmlOpts);
                listOfCasesToUpdate.add(objCase);
            }
        }
        //check if listOfCasesToUpdate is not empty
        if(!listOfCasesToUpdate.isEmpty()) {
            try {
                //deactivate case validation rule SI_MacroDeselectStatusCantChange
                ValidationRuleManager__c validationRuleSetting = ValidationRuleManager__c.getOrgDefaults();
                if(validationRuleSetting.Run_Validation__c == true) {
                    validationRuleSetting.Run_Validation__c = false;
                    SI_UtilityClass.callDMLOperationsSingleRecord(validationRuleSetting, 
                                                                  Label.SI_DML_Operations.split(';')[1]);
                }
                //update case records
                SI_UtilityClass.callDMLOperations(listOfCasesToUpdate, Label.SI_DML_Operations.split(';')[1]);
                //Calling method to send email
                sendEmailToWorkOrderRecipients(listOfCasesToUpdate,oldCaseMap);
                validationRuleSetting.Run_Validation__c = true;
                SI_UtilityClass.callDMLOperationsSingleRecord(validationRuleSetting, Label.SI_DML_Operations.split(';')[1]);
            } catch(DmlException ex) {
                //log exception
                System.debug(LoggingLevel.INFO, 'Exception: ' + ex.getMessage());
            }
        }
    }
    
    /*********************************************************************************************
    * @description - update location which is releated to case,when case is closed.
    * @param newCaseList - List<Case>
    * @param oldCaseMap - Map<Id,Case>
    **********************************************************************************************/
    public static void updateLocationRecords(List<Case> newCaseList, Map<Id,Case> oldCaseMap){
        //set of case Ids
        Set<Id> setOfCaseIds = new Set<Id>();
        //map to update
        Map<Id,Location_ID__c> mapToUpdateLocation = new Map<Id,Location_ID__c>();
        //Fetch Case RecordType Id "SI-Internal"
        Id caseSIInternalRT = 
            SI_UtilityClass.getRecordTypeIdByDeveloperName('Case',System.Label.SI_CaseRT.split(';')[2]);
        for(case caseObj : newCaseList) {
            //filter case when case is closed-completed,Internally Contracted is Yes and Microwave contractid not blank
            if((caseObj.status!= oldCaseMap.get(caseObj.Id).Status) && caseObj.status == Label.SI_CaseStatus.split(',')[0] &&  
                (caseObj.Internally_Contracted__c == Label.SI_Internally_Contracted.split(';')[0]) && caseObj.Type == Label.Case_Type_Label &&    
                (String.isNotBlank(caseObj.Microwave_Contract_ID__c)) && caseObj.RecordTypeId == caseSIInternalRT){
                setOfCaseIds.add(caseObj.Id);
            }
        }
        if(!setOfCaseIds.isEmpty()){
            for(Intake_Site__c intakeObj : [SELECT Case__r.Microwave_Contract_ID__c,Location_ID__c,Location_ID__r.Primary_Vendor_Name__c,
                                            Case__r.Selected_Vendor__r.Supplier_SAP_ID__c FROM Intake_Site__c WHERE Case__c IN: setOfCaseIds]){
               
                Location_ID__c locationUpdate = new Location_ID__c();
                locationUpdate.Id = intakeObj.Location_ID__r.Id;
                locationUpdate.Primary_Vendor_Name__c = System.Label.SI_Selected_Vendor;
                locationUpdate.AAV_Primary_Contract_ID__c = intakeObj.Case__r.Microwave_Contract_ID__c;
                locationUpdate.Primary_Vendor_Id__c = intakeObj.Case__r.Microwave_Contract_ID__c;
                locationUpdate.Internally_Contracted__c = true;
                locationUpdate.AAV_Status__c = System.Label.SI_Location_AVV_Status;
                locationUpdate.CMG_Working_Status__c = System.Label.SI_LocationCMGWorkingStatus.split(';')[0];
                if(!mapToUpdateLocation.containsKey(locationUpdate.Id)){
                    mapToUpdateLocation.put(locationUpdate.Id, locationUpdate);  
                }
            }
            if(!mapToUpdateLocation.isEmpty()){
                //update Location Record
                SI_UtilityClass.callDMLOperations(mapToUpdateLocation.values(), Label.SI_DML_Operations.split(';')[1]);
            }
        }
    }
}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ContentDocumentLinkTriggerHandlerTest
*  @Author           : Archana (Task) & Matthew Elias (Request/Savings Tracker Attachments)
*  @Version History  : 1.0
*  @Creation         : 23.08.2019 & 12.08.20
*  @Description      : Test class for ContentDocumentLinkTriggerHandler
**********************************************************************************************************************
*********************************************************************************************************************/
@isTest(SeeAllData=false)
Public class ContentDocumentLinkTriggerHandlerTest{
/***************************************************************************************************
* @Description : Test Method to test copyAttachmentForTask
***************************************************************************************************/
    @isTest static void copyAttachmentForTask(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        List<ContentDocumentLink> cdLink = new List<ContentDocumentLink>();
        Integer NumOfCases=1;
        String WorkflowTmpName1='OM AAV NNI Add';
        caseList= TestDataFactory.createCase(WorkflowTmpName1,NumOfCases,recTypeId);
        Test.startTest();       
        Insert(caseList);  
        list<case>taskList=[select id,task_number__c from case where parentid =:caseList[0].id];
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true);
        insert contentVersion;  
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        list<Attachement_required_for_template__mdt>mdata=[select workflow_template__c,Parent_Task__c,Chsk__c from Attachement_required_for_template__mdt where workflow_template__c=:WorkflowTmpName1]; 
        for(case c:taskList)
        {
            if(c.task_number__c==mdata[0].Parent_Task__c)
            {
                ContentDocumentLink contentlink=new ContentDocumentLink();
                contentlink.LinkedEntityId=c.id;
                contentlink.ShareType= 'V';
                contentlink.ContentDocumentId=documents[0].Id;
                contentlink.Visibility = 'AllUsers'; 
                cdLink.add(contentlink); 
                
            }
        }
        
        Insert(cdLink); 
        delete(cdLink[0]);
        Test.stopTest();
    }
    
/***************************************************************************************************
* @Description : Test Method to test copyAttachmentToSavingsTracker
***************************************************************************************************/
    @isTest static void copyNewRequestAttachmentToSavingsTrackerTest(){
        
        // create Request record
        SavingsTrackerTestDataFactory.createRequest(1);
        Request__c testRequest = [SELECT Id FROM Request__c LIMIT 1];
        List<ContentDocumentLink> reqCount = new List<ContentDocumentLink>();
        
        // create Savings Tracker record
        SavingsTrackerTestDataFactory.createSavingsTracker(new Set<Id>{testRequest.Id});
        Savings_Tracker__c testSavingsTracker = [SELECT Id FROM Savings_Tracker__c LIMIT 1];
        List<ContentDocumentLink> stCount = new List<ContentDocumentLink>();
        
        // create 3 ContentDocuments
        SavingsTrackerTestDataFactory.createContentDocuments(3);
        
        
        Test.startTest();
        
        // query for the created ContentDocuments
        List<ContentDocument> CDList = new List<ContentDocument>([SELECT Id, Title FROM ContentDocument]);
        System.assertEquals(3,CDList.size());
        
        // attach first and second CD to Savings Tracker and confirm that it was added to ST and not Req
        Map<Id,Set<Id>> CDLMap = new Map<Id,Set<Id>>();
        CDLMap.put(testSavingsTracker.Id, new Set<Id>{CDList[0].Id,CDList[1].Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        reqCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest.Id];
        stCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker.Id];
        System.assertEquals(0,reqCount.size());
        System.assertEquals(2,stCount.size());
        
        // attach third CD to Request and confirm that it was added to both Req and ST
        CDLMap.clear();
        CDLMap.put(testRequest.Id, new Set<Id>{CDList[2].Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        reqCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest.Id];
        stCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker.Id];
        System.assertEquals(1,reqCount.size());
        System.assertEquals(3,stCount.size());
        
        // attach first and second CD to Request and confirm that it was added to the Req but not the ST
        CDLMap.clear();
        CDLMap.put(testRequest.Id, new Set<Id>{CDList[0].Id, CDList[1].Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        reqCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest.Id];
        stCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker.Id];
        System.assertEquals(3,reqCount.size());
        System.assertEquals(3,stCount.size());
        
        Test.stopTest();
        
    }
       
/***************************************************************************************************
* @Description : Test Method to test reconcileRequestSavingsTrackerAttachment
***************************************************************************************************/
    @isTest static void deleteRequestSavingsTrackerAttachmentTest1(){
        
        // create Request record
        SavingsTrackerTestDataFactory.createRequest(1);
        List<Request__c> testRequest = [SELECT Id FROM Request__c];
        List<ContentDocumentLink> req1CDLCount = new List<ContentDocumentLink>();
        
        // create ST
        SavingsTrackerTestDataFactory.createSavingsTracker(new Set<Id>{testRequest[0].Id});
        Savings_Tracker__c testSavingsTracker1 = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c = :testRequest[0].Id];
        List<ContentDocumentLink> st1CDLCount = new List<ContentDocumentLink>();
        
        // create 3 ContentDocuments
        SavingsTrackerTestDataFactory.createContentDocuments(3);
        List<ContentDocument> CDList = new List<ContentDocument>([SELECT Id, Title FROM ContentDocument]);
        
        // CD Attachments:
        // 1 - 1 Req
        // 2 - 1 Req
        // 3 - 1 ST
        Map<Id,Set<Id>> CDLMap = new Map<Id,Set<Id>>();
        CDLMap.put(testRequest[0].Id, new Set<Id>{CDList[0].Id, CDList[1].Id});
        CDLMap.put(testSavingsTracker1.Id, new Set<Id>{CDList[2].Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        
        // confirm num CDLs on 1st Req & ST
        req1CDLCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id];
        System.assertEquals(2,req1CDLCount.size());
        st1CDLCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        System.assertEquals(3,st1CDLCount.size());
		
		        
        // need to demonstrate:
        // deleting a CDL from a Req w/  ST
        // deleting a CDL from an ST w/  CDL on Request
        // deleting a CDL from an ST w/o CDL on Request
        
        Test.startTest();
        
        
        ContentDocumentLink testCDL = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id AND ContentDocumentId = :CDList[0].Id LIMIT 1];
        delete testCDL;
        req1CDLCount = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id];
        System.assertEquals(1,req1CDLCount.size());
        System.assertEquals(req1CDLcount[0].ContentDocumentId,CDList[1].Id);
        st1CDLCount = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        System.assertEquals(2,st1CDLCount.size());
		
        testCDL = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id AND ContentDocumentId = :CDList[2].Id LIMIT 1];
        delete testCDL;
        req1CDLCount = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id];
        System.assertEquals(1,req1CDLCount.size());
        st1CDLCount = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        System.assertEquals(1,st1CDLCount.size());
        System.assertEquals(st1CDLCount[0].ContentDocumentId,CDList[1].Id);
		
		testCDL = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id AND ContentDocumentId = :CDList[1].Id LIMIT 1];
        delete testCDL;
        req1CDLCount = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id];
        System.assertEquals(0,req1CDLCount.size());
        st1CDLCount = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        System.assertEquals(0,st1CDLCount.size());
        
        
        Test.stopTest();
        
	}
    
/***************************************************************************************************
* @Description : Test Method to test reconcileRequestSavingsTrackerAttachment
***************************************************************************************************/
    @isTest static void deleteRequestSavingsTrackerAttachmentTest2(){
        
        List<ContentDocumentLink> deleteCDL = new List<ContentDocumentLink>();
        
        // create Request record
        SavingsTrackerTestDataFactory.createRequest(1);
        List<Request__c> testRequest = [SELECT Id FROM Request__c];
        List<ContentDocumentLink> req1CDLCount = new List<ContentDocumentLink>();
        
        // create ST
        SavingsTrackerTestDataFactory.createSavingsTracker(new Set<Id>{testRequest[0].Id});
        Savings_Tracker__c testSavingsTracker1 = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c = :testRequest[0].Id];
        List<ContentDocumentLink> st1CDLCount = new List<ContentDocumentLink>();
        
        // create 3 ContentDocuments
        SavingsTrackerTestDataFactory.createContentDocuments(3);
        List<ContentDocument> CDList = new List<ContentDocument>([SELECT Id, Title FROM ContentDocument]);
        
        // CD Attachments:
        // 1 - 1 Req
        // 2 - 1 Req
        // 3 - 1 ST
        Map<Id,Set<Id>> CDLMap = new Map<Id,Set<Id>>();
        CDLMap.put(testRequest[0].Id, new Set<Id>{CDList[0].Id, CDList[1].Id});
        CDLMap.put(testSavingsTracker1.Id, new Set<Id>{CDList[2].Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(CDLMap);
        
        // confirm num CDLs on 1st Req & ST
        req1CDLCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id];
        System.assertEquals(2,req1CDLCount.size());
        st1CDLCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        System.assertEquals(3,st1CDLCount.size());
		
		        
        // need to demonstrate:
        // deleting ST and leaving CDLs on Request
        
        Test.startTest();
        
        delete testSavingsTracker1;
        req1CDLCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testRequest[0].Id];
        System.assertEquals(2,req1CDLCount.size());
        
        SavingsTrackerTestDataFactory.createSavingsTracker(new Set<Id>{testRequest[0].Id});
        testSavingsTracker1 = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c = :testRequest[0].Id];
        st1CDLCount = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :testSavingsTracker1.Id];
        System.assertEquals(2,st1CDLCount.size());
        
        Test.stopTest();
        
	}
    
    /***************************************************************************************************
* @Description : Test Method to test nlgCommunityUserAccess
***************************************************************************************************/
    @isTest static void nlgCommunityUserAccessTest(){
        List<ContentDocumentLink> cdLink = new List<ContentDocumentLink>();
        List<Account> listAccount = new List<Account>();
        
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
        //SetupLocation
        List<Location_ID__c> locationList = TestDataFactory.createLocationId('TestSite1','TestSite1',1);
        Location_ID__c loc1 = locationList[0];
        loc1.Market__c ='HOUSTON TX';
        loc1.Region__c='SOUTH';
        Insert loc1;
        // Opportunity RecordType
        Id newsiteLeaseRecordType = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity','New_Site_Lease');
        Opportunity newsiteLease = TestDataFactory.createOpportunitySingleIntake(1, newsiteLeaseRecordType)[0];
        newsiteLease.StageName = 'New';  
        newsiteLease.AccountId = listAccount[0].Id;
        newsiteLease.Contract_Start_Date__c = System.today();
        newsiteLease.Term_mo__c = 1.00;
        newsiteLease.Landlord__c ='Crown Castle';
        newsiteLease.CLIQ_Sub_Type__c ='Lease';
        newsiteLease.Lease_or_AMD__c ='Lease';
        newsiteLease.Negotiating_Vendor__c='South Region';
        insert newsiteLease;
        
        Test.startTest();
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true);
        insert contentVersion;  
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=newsiteLease.id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=documents[0].Id;        
        cdLink.add(contentlink); 
        
        Insert(cdLink); 
        List<ContentDocumentLink> cdlLink = [SELECT Id, LinkedEntityId, ShareType,Visibility FROM ContentDocumentLink where id =:cdLink[0].Id];
        System.assertEquals('AllUsers',cdlLink[0].visibility);
        delete(cdLink[0]);
        Test.stopTest();
        
	}
}
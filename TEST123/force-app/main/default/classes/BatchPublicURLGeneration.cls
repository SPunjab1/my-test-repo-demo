/*********************************************************************************************
@author     : Balaji
@date       : 2020.11.17
@description: This class is used to generate the Backlog Attachment related public URL's
@Version    : 1.0
**********************************************************************************************/


global class BatchPublicURLGeneration implements Database.Batchable<ContentDocumentLink>, Iterable<ContentDocumentLink>, Iterator<ContentDocumentLink>, Schedulable{
    
    public integer i=0;
    List<ContentDocumentLink> acc;
    /*   public BatchPublicURLGeneration() {
//acc = [SELECT SystemModstamp, ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId in (SELECT Id FROM Case where Workflow_Template__c like 'TIFS%') and LinkedEntity.Type='Case'and SystemModstamp > 2020-06-01T11:51:01.000+0000];
i = 0;
system.debug('acc'+acc);
} */
    public Iterator<ContentDocumentLink> iterator() {
        return this;
    }
    public boolean hasNext() {
        if(acc.size() <= i) 
            return false;
        else
            return true;
    }
    public ContentDocumentLink next() {
        i++;
        return acc[i-1];
    }
    //global Database.QueryLocator start(Database.BatchableContext BC) {
    global Iterable<ContentDocumentLink> start(Database.BatchableContext bc) {    
        //String query = 'SELECT SystemModstamp, ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId in (SELECT Id FROM Case where Workflow_Template__c like \'TIFS%\') and LinkedEntity.Type=\'Case\'and SystemModstamp > 2020-06-01T11:51:01.000+0000';
        //system.debug(query);
        //return example();
        //return new BatchPublicURLGeneration();
        return acc = [SELECT SystemModstamp, ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId in (SELECT Id FROM Case where Workflow_Template__c like 'TIFS%') and LinkedEntity.Type='Case'and SystemModstamp > 2020-06-01T11:51:01.000+0000];
    }
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())Database.executeBatch(new BatchPublicURLGeneration());
    }
    global void execute(Database.BatchableContext bc, List<ContentDocumentLink> scope) {
        system.debug('scope:'+scope);
        
        //Map<Id, ContentVersion> sMapDocIdContentVersion = New Map<Id, ContentVersion>();
        Map<Id,String> sMapexistingPubliclinksVersionId = New Map<Id,String>();
        List<ContentDistribution> lstcontentDistribution  = new List<ContentDistribution>();
        Set<Id> sContentVersionId = New Set<Id>();
        Set<Id> sContentDocId = New Set<Id>();
        
        for(ContentDocumentLink templog: scope){
            sContentDocId.add(templog.ContentDocumentId);
        }
        for(ContentDocument temp:[select Id,LatestPublishedVersionId from ContentDocument where Id in:sContentDocId]){        
            system.debug('temp:'+temp);
            sContentVersionId.add(temp.LatestPublishedVersionId);
        }
        for(ContentDistribution temp:[select Id,DistributionPublicUrl,ContentVersionId from ContentDistribution where ContentVersionId in:sContentVersionId]){        
            system.debug('temp:'+temp);
            sMapexistingPubliclinksVersionId.put(temp.ContentVersionId,temp.DistributionPublicUrl);
        }
        system.debug('mapdetails>>:'+sMapexistingPubliclinksVersionId);
        for(ContentVersion temp: [SELECT Id,ContentDocumentId,Title,IsLatest FROM Contentversion WHERE Id in:sContentVersionId]){
            System.debug('entered'+temp.Id);
            system.debug('Contentversionmap>>:'+ sMapexistingPubliclinksVersionId.get(temp.Id));
            if(sMapexistingPubliclinksVersionId.get(temp.Id)==null){
                ContentDistribution CD = New ContentDistribution();
                CD.ContentVersionId =temp.Id;
                CD.Name = temp.Title.left(100);
                CD.PreferencesAllowOriginalDownload =TRUE;
                CD.PreferencesAllowPDFDownload=FALSE;
                CD.PreferencesAllowViewInBrowser=TRUE;
                CD.PreferencesExpires=FALSE;
                CD.PreferencesLinkLatestVersion=TRUE;
                CD.PreferencesNotifyOnVisit	=FALSE;
                CD.PreferencesNotifyRndtnComplete=FALSE;
                CD.PreferencesPasswordRequired=FALSE;
                lstcontentDistribution.add(CD);
            }
        }
        system.debug('sContentVersionId:'+sContentVersionId);
        system.debug('lstcontentDistribution:'+lstcontentDistribution);
        if(lstcontentDistribution != null && lstcontentDistribution.size() >0){
            Insert(lstcontentDistribution);
        }
        
    }
    global void finish(Database.BatchableContext bc) {
        string codecoverage;
        string codecoverage1;
        string codecoverage2;
        string codecoverage3;
        string codecoverage4;
        string codecoverage5;
        string codecoverage6;
    }
}
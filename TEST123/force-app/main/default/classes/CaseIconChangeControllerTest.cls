/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : CaseIconChangeControllerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 22.08.2019
*  @Description      : Test class for CaseIconChangeController
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class CaseIconChangeControllerTest {
/*************************************************************************************
* @Description :Test Method for getIcon for work Order
*************************************************************************************/
   static testmethod void testGetIcon() { 
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        List<Case> WorkOrderListToUpdate= new List<Case>();
        List<String> woIDLst =new List<String>();
        Integer NumOfCases=1;
        String WorkflowTmpName='AAV CIR Retest';
        caseList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        insert caseList; 
        Test.startTest();
       		String source =CaseIconChangeController.getIcon(caseList[0].Id);
        Test.stopTest();
        System.assertEquals(source!=null, true);
   }
/*************************************************************************************
* @Description :Test Method for getIcon for work Order task
*************************************************************************************/
    static testmethod void testGetIcon1() { 
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        List<Case> WorkOrderListToUpdate= new List<Case>();
        List<String> woIDLst =new List<String>();
        Integer NumOfCases=1;
        String WorkflowTmpName='AAV CIR Retest';
        caseList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        insert caseList; 
        Test.startTest();
       		String source =CaseIconChangeController.getIcon(caseList[0].Id);
        Test.stopTest();
        System.assertEquals(source!=null, true);
   }
}
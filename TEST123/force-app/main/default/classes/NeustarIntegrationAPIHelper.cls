/*********************************************************************************************
@author     : IBM
@date       : July 8 2020
@description: Helper class for NeustarIntegration 
@Version    : 1.0  
**********************************************************************************************/
public with sharing class NeustarIntegrationAPIHelper {
    public static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();
    Public static String objectName = 'Purchase_Order__c';
    /************ Error Reponse Wrapper ********************/
    public class ErrorReponseWrapper{
        public String status;
        public String statusCode;
        public List<ErrorList> errorList;
    }
    public class ErrorList {
        public String errorCode;
        public String description;
        //public Object arguments;
    }  
    /********************* Success Response Wrapper *******************/
    public class SuccessReponseWrapper{
        public String status;
        public String statusCode;
        public String message;
        public Info info;
    }
    public class Info {
        public String productCatalogName;
        public Integer productCatalogId;
        public Integer uomOrderKey;
        public String customerOrderID;
        public String uomOrderID;
        public String requestNumber;
        public String ver;
        public String orderStatus;
        public String activityId;
        public String activityType;
        public String application;
        public String tradingPartner;
        public String endUser;
        public String owner;
        public String createdDate;
        public String lastStatusChangeDate;
        public String sellerID;
        public String buyerID;
    }
    
    /*************** Create Order Wrapper **********************/
    public class CreateOrderWrapper{
        public String activity;
        public String tradingPartner;
        public String action;
        public String productCatalogName;
        public List<ProductOrder> productOrder;
    }    
    public class ProductOrder {
        public String sectionName;
        public List<Fields> fields;
        public Integer index;
        public List<SubSections_Z> subSections;
    }
    public class Fields {
        public String fieldName;
        public String value;
    }
    public class SubSections_Z {
        public String sectionName;
        public List<Fields_Z> fields;
        public String isEnabled;
        public Integer index;
        public List<SubSections> subSections;
    }
    public class Fields_Z {
        public String fieldName;
        public String value;
    }
    public class SubSections {
    }
    
    /*************** Wrapper Class to Retrun to batch**********************/
    public class ReturnParamsToBatchAfterCallout {
        public List<Purchase_Order__c> poToBeUpdated {get;set;}
        public List<Integration_Log__c> logsToBeUpserted {get;set;} 
    }
/*********************************************************************************************
* @Description : Generic method for REST API callout for 
/**********************************************************************************************/   
    public static HTTPResponse makeRestCallout(String endPoint,String method,String requestBody){
        TMobilePh2LoginDetailsHierarchy__c neustarToken = TMobilePh2LoginDetailsHierarchy__c.getOrgDefaults();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPoint);
        req.setMethod(method);
        /*HTTPResponse resp = NeustarLoginAPI.sendLoginRequest();
          if(resp.getStatusCode() == 200){
          req.setHeader('Authorization',resp.getHeader('Authorization'));
          }
        else{
          req.setHeader('Authorization','Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpYm11b21hcGkiLCJDb21wYW55IjoiQ3VzdG9tZXIiLCJEb21haW4iOiJUVU9NX0UyRSIsImV4cCI6MTU5NDIwNTgxMX0.FjDKJpDJW0z2fZD27SqP61F-UFMVF0yyLuvx5ZEmNjFNHQ2m-6qyerIL0qzj0mbygGCcdBHqsAYuQaXkS0TNBw');
          }*/
        String accessToken = neustarToken.Neustar_Access_Token__c;
        req.setHeader('Authorization',accessToken);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setTimeout(120000); 
        if(requestBody!=null){
            req.setBody(requestBody);
        }
        Http http = new Http();
        HTTPResponse response;
        try{
            response = http.send(req);
            System.debug('>>>>>>>>>>>>>>>>Response>>>>>>>>>>>>>>>'+response);
            System.debug('Body =========='+response.getBody());
        } catch(Exception ex){
            response = new HTTPResponse();
            response.setBody(ex.getMessage());
        }
        return response;
    }
/*********************************************************************************************
* @Description : Generic Method for creating JSON for Create Order dynamically
/**********************************************************************************************/
    /*public static void generateCreateOrderJSON(Map<Id,Purchase_Order__c> idToPOrderMap){
Map<Id,Integration_Log__c> pO_IL_Map = new Map<Id,Integration_Log__c>();
for(Integration_Log__c log : [SELECT Id, Purchase_Order__c,Reprocess__c,Request__c,Response__c from Integration_Log__c where Purchase_Order__c IN: idToPOrderMap.keySet()]){
pO_IL_Map.put(log.Purchase_Order__c, log);
}
List<Integration_Log__c> logLst = new List<Integration_Log__c>();
List<Purchase_Order__c> updatePOLst = new List<Purchase_Order__c>();
SObjectType r = ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType();
DescribeSObjectResult d = r.getDescribe();
if(idToPOrderMap!=null && idToPOrderMap.keySet().size()>0){
for(Purchase_Order__c purchaseOrder : idToPOrderMap.Values()){
System.debug('purchaseOrder***********'+purchaseOrder);
CreateOrderWrapper reqWrap = new CreateOrderWrapper();
System.debug('***********purchaseOrder.Product_Name__c'+purchaseOrder.Product_Name__c);
String prdName = purchaseOrder.Product_Name__c;
System.debug('***********purchaseOrder.Trading_Partner__c'+purchaseOrder.Trading_Partner__c);
String tradingPartner = purchaseOrder.Trading_Partner__c;

Map<String, Neustar_Integration_Mapping__mdt> allFieldMap = getfieldMappingMdtMap();
//Map<String,String> picklistValueToAPICodeMap = getDataTransformationData();
List<NeuStar_Integration__mdt> intMdt = [select id,Product_Catalog_Name__c,Trading_Partner__c,Action__c,
Activity__c from NeuStar_Integration__mdt 
where Trading_Partner__c = :tradingPartner and Product_Catalog_Name__c = : prdName];
if(intMdt != null && intMdt.size()>0){
System.debug('intMdt*************'+intMdt);
Map<Id,List<NeuStar_Integration_Section_Mapping__mdt>> intIdToSecMap = getSectionMetadatMap(intMdt[0].Id);
Map<Id,List<NeuStar_Integration_SubSection_Mapping__mdt>> secIdToSubsecMap;
if(intIdToSecMap!=null && intIdToSecMap.keySet().size()>0){
secIdToSubsecMap = getSubsectionMetadatMap(intIdToSecMap);   
}
if(intMdt!=null && intMdt.size() == 1){
NeuStar_Integration__mdt mainMdtRec = intMdt[0];
reqWrap.activity = mainMdtRec.Activity__c;
reqWrap.tradingPartner = mainMdtRec.Trading_Partner__c;
reqWrap.action = mainMdtRec.Action__c;
reqWrap.productCatalogName = mainMdtRec.Product_Catalog_Name__c;
List<ProductOrder> productOrderLst = new List<ProductOrder>();
if(intIdToSecMap!=null && intIdToSecMap.keySet().size()>0 && intIdToSecMap.containsKey(intMdt[0].Id)){
for(NeuStar_Integration_Section_Mapping__mdt section : intIdToSecMap.get(intMdt[0].Id)){
ProductOrder pOrder = new ProductOrder();
pOrder.sectionName = section.Section_Name__c;
pOrder.index = 1;
System.debug('section.Field_Names__c**********'+section.Field_Names__c);
if(section.Field_Names__c!=null && allFieldMap.keySet().size()>0){
String temp = section.Field_Names__c;
if(temp.contains('"')){
temp = temp.replace('"',''); 
}
List<String> fieldAPINameLst = temp.split(',');
System.debug('fieldAPINameLs**********'+fieldAPINameLst);
List<Fields> fieldsLst = new List<Fields>();   
for(String fieldNm : fieldAPINameLst){
if(fieldNm == 'PON__c' || fieldNm == 'DDD__c'){
if(allFieldMap.containsKey(fieldNm)){
Schema.DisplayType fielddataType = d.fields.getMap().get(fieldNm).getDescribe().getType();
Neustar_Integration_Mapping__mdt fieldMdt = allFieldMap.get(fieldNm);
Fields fie = new Fields();
fie.fieldName = fieldMdt.Neustar_Attribute_Name__c;
if(purchaseOrder.get(fieldNm)!=null){
//Code commented
/* if(fielddataType != Schema.DisplayType.DateTime) {
purchaseOrder.get(fieldNm).format('MM-dd-yyyy');
}*/
    /*    if(fielddataType == Schema.DisplayType.Date) {
Date dt =  Date.valueOf(purchaseOrder.get(fieldNm));
String strDate = dt.month() +'-' + dt.day() +'-' + dt.year();
fie.value = strDate;
}*/
    //Code commented
    /*else if(fielddataType == Schema.DisplayType.Picklist) {
if(picklistValueToAPICodeMap!=null && picklistValueToAPICodeMap.keySet().size()>0 &&
picklistValueToAPICodeMap.containsKey(String.valueOf(purchaseOrder.get(fieldNm)))){
picklistValueToAPICodeMap.get(String.valueOf(purchaseOrder.get(fieldNm)));     
}  
else{
fie.value = String.valueOf(purchaseOrder.get(fieldNm));   
}
}*/
    /*  else
{
fie.value = String.valueOf(purchaseOrder.get(fieldNm));   
}
}
else{
fie.value = null;
}
fieldsLst.add(fie);
}
else{
System.debug('Not Found>>>>>>>> '+fieldNm);
}
}
} 
pOrder.fields = fieldsLst;
}
if(secIdToSubsecMap!=null && secIdToSubsecMap.keySet().size()>0 && secIdToSubsecMap.containsKey(section.Id)){
List<SubSections_Z> subSectionLst = new List<SubSections_Z>();
for(NeuStar_Integration_SubSection_Mapping__mdt subsection :secIdToSubsecMap.get(section.Id)) {
if(subsection.NeuStar_Integration_Section_Mapping__c == section.Id){
SubSections_Z subsec = new SubSections_Z();
subsec.sectionName = subsection.Sub_Section_Name__c;
/*if(subsection.Field_Names__c!=null && allFieldMap.keySet().size()>0){
String tempSubSecLst = subsection.Field_Names__c;
if(tempSubSecLst.contains('"')){
tempSubSecLst = tempSubSecLst.replace('"',''); 
}
List<String> subfieldAPINameLst = tempSubSecLst.split(',');
System.debug('fieldAPINameLs for subsection**********'+subfieldAPINameLst);
List<Fields_Z> subfieldsLst = new List<Fields_Z>(); 
for(String fieldNm : subfieldAPINameLst){
Schema.DisplayType fielddataType = d.fields.getMap().get(fieldNm).getDescribe().getType();
Neustar_Integration_Mapping__mdt fieldMdt = allFieldMap.get(fieldNm);
Fields_Z fie = new Fields_Z();
fie.fieldName = fieldMdt.Neustar_Attribute_Name__c;
if(purchaseOrder.get(fieldNm)!=null){*/
    //Code commented
    /* if(fielddataType != Schema.DisplayType.DateTime) {
purchaseOrder.get(fieldNm).format('MM-dd-yyyy');
}*/
    /* if(fielddataType == Schema.DisplayType.Date) {
Date dt =  Date.valueOf(purchaseOrder.get(fieldNm));
String strDate = dt.month() +'-' + dt.day() +'-' + dt.year();
fie.value = strDate;
}*/
    //Code commented
    /* else if(fielddataType == Schema.DisplayType.Picklist) {
if(picklistValueToAPICodeMap!=null && picklistValueToAPICodeMap.keySet().size()>0 &&
picklistValueToAPICodeMap.containsKey(String.valueOf(purchaseOrder.get(fieldNm)))){
picklistValueToAPICodeMap.get(String.valueOf(purchaseOrder.get(fieldNm)));     
}  
else{
fie.value = String.valueOf(purchaseOrder.get(fieldNm));   
}
}*/
    /* else{
fie.value = String.valueOf(purchaseOrder.get(fieldNm));    
}
}
else{
fie.value = null;
}
subfieldsLst.add(fie);
}
subsec.fields = subfieldsLst;
}*/ 
    /*subSectionLst.add(subsec);
}
}
pOrder.subSections = subSectionLst;

}
productOrderLst.add(pOrder);
}
}
reqWrap.productOrder = productOrderLst;
}
String createOrderWrapStr = JSON.Serialize(reqWrap);
System.debug('>>>>>>>>>>>createOrderWrapStr>>>>>>>>>'+createOrderWrapStr);
HTTPResponse response = makeRestCallout('callout:CreateOrderInNeustar','POST',
createOrderWrapStr);
Integration_Log__c log = NULL;
if(pO_IL_Map != NULL && pO_IL_Map.containsKey(purchaseOrder.Id)){
log = new integration_Log__c(Id = pO_IL_Map.get(purchaseOrder.Id).Id);
}
else{
log = new integration_Log__c(Purchase_Order__c = purchaseOrder.Id);
}
Purchase_Order__c poOrder = new Purchase_Order__c();
poOrder.Id = purchaseOrder.Id;
poOrder.Work_Order_Task__c =  purchaseOrder.Work_Order_Task__c;
try{
if(response.getBody().contains('Success')){ 
SuccessReponseWrapper successResponseWrapper = (SuccessReponseWrapper) System.JSON.deserialize(response.getBody(), SuccessReponseWrapper.class);
if(successResponseWrapper!=null && successResponseWrapper.info!=null && successResponseWrapper.info.uomOrderID!=null){
poOrder.UOMOrderId__c = successResponseWrapper.info.uomOrderID;    
}
if(successResponseWrapper!=null && successResponseWrapper.info!=null && successResponseWrapper.info.ver!=null){
poOrder.Version__c = successResponseWrapper.info.ver; 
}
poOrder.API_Status__c = 'Successful';
updatePOLst.add(poOrder);
//Create Integration Log for success.
log = createIntegrationLog(log,createOrderWrapStr, 'Success', JSON.Serialize(successResponseWrapper),successResponseWrapper.message,'Create Purchase Order',null,null);
} else if(response.getBody().contains('failure')){
poOrder.API_Status__c = 'Failed';
updatePOLst.add(poOrder);
//Create Integration Log for Failure.
ErrorReponseWrapper responseWrap = (ErrorReponseWrapper) System.JSON.deserialize(response.getBody(), ErrorReponseWrapper.class);
log = createIntegrationLog(log,createOrderWrapStr,'Failure', JSON.Serialize(responseWrap), responseWrap.errorList[0].description, 'Create Purchase Order',null,null);
}
logLst.add(log);
}Catch(Exception Ex){
log = createIntegrationLog(log,createOrderWrapStr,'Failure',ex.getMessage(), NULL, 'Create Purchase Order',null,null);
logLst.add(log);
}
}
else{
Integration_Log__c intLogInsert = new Integration_Log__c(Purchase_Order__c =purchaseOrder.Id,Reprocess__c =false);
intLogInsert = createIntegrationLog(intLogInsert,null,'Failure','', NULL, 'Create Purchase Order',
'There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again',
'Product CatalogName,trading partner,activity or associated metdata is not availble, please check');
logLst.add(intLogInsert);  
}
}
}
try{
Database.SaveResult[] srUpdatePoLst;
if(updatePOLst!=null && updatePOLst.size()>0){
srUpdatePoLst = database.update(updatePOLst,false);
// Iterate through each returned result
for (Database.SaveResult sr : srUpdatePoLst) {
if (!sr.isSuccess()) {
string errorMsg = '';
// Operation failed, so get all errors                
for(Database.Error err : sr.getErrors()) { 
if(errorMsg == ''){
errorMsg = err.getStatusCode() + ': ' + err.getMessage() + ': ' + err.getFields();
}
else{
errorMsg = errorMsg + err.getStatusCode() + ': ' + err.getMessage() + ': ' + err.getFields();
}
}
Integration_Log__c intLogInsert = new Integration_Log__c(Purchase_Order__c = sr.getId(),Reprocess__c =false);
intLogInsert = createIntegrationLog(intLogInsert,null,'Failure',null,null, 'Create Purchase Order',
'There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again',errorMsg);
logLst.add(intLogInsert);
}
}
}
System.debug('updatePOLst*********'+updatePOLst);
if(logLst!=null && logLst.size()>0){
upsert logLst;
} 
}catch(Exception ex){
System.debug('Exception *********'+ex);
}
}*/
    /*public static Map<String, Neustar_Integration_Mapping__mdt> getfieldMappingMdtMap(){
List<Neustar_Integration_Mapping__mdt> fieldMappingMdtLst =[select id,Default_Value__c,Neustar_Attribute_Name__c,
Salesforce_Field_API_Name__c,Salesforce_Object_Name__c from
Neustar_Integration_Mapping__mdt where API_Type__c ='Create Order'];
Map<String, Neustar_Integration_Mapping__mdt> apiNameToMdtMap = new Map<String, Neustar_Integration_Mapping__mdt>();
if(fieldMappingMdtLst != null && fieldMappingMdtLst.size()>0){
for(Neustar_Integration_Mapping__mdt fieldMap : fieldMappingMdtLst){
apiNameToMdtMap.put(fieldMap.Salesforce_Field_API_Name__c,fieldMap);
}
}
System.debug('apiNameToMdtMap*******'+apiNameToMdtMap);
return apiNameToMdtMap;
}*/
    
    /***************************************************************************************
@description : Generic methods for preparing Integration_Log__c for API callout  
****************************************************************************************/ 
    public static Integration_Log__c createIntegrationLog(Integration_Log__c log,String req, String status,String oResponse,String oResponseMsg,String apiType,String errorLog,String errorMsg,String errorType){
        if(log.Reprocess__c == true){
            log.Reprocess__c = false;
        }
        log.Status__c = status ;
        log.Request__c = req;
        log.Response_Message__c = oResponseMsg;
        log.Response__c = oResponse;
        log.From__c = 'SFDC'; 
        log.To__c = 'Neustar' ;
        log.API_Type__c = apiType ;
        log.Error_Log__c = errorLog;
        log.Failure_Reason__c = errorMsg;
        log.Error_Type__c = errorType;
        return log;
    }
    
    /***************************************************************************************
@description : Create Map for section metadata  
****************************************************************************************/     
    /* public static Map<Id,List<NeuStar_Integration_Section_Mapping__mdt>> getSectionMetadatMap(String mainIntId){ 
Map<Id,List<NeuStar_Integration_Section_Mapping__mdt>> intIdToSecMap =  new Map<Id,List<NeuStar_Integration_Section_Mapping__mdt>>();
List<NeuStar_Integration_Section_Mapping__mdt> secMappingMdt =  [select id,Section_Name__c,Field_Names__c,NeuStar_Integration_Mapping__c from NeuStar_Integration_Section_Mapping__mdt
where NeuStar_Integration_Mapping__c = :mainIntId];
for(NeuStar_Integration_Section_Mapping__mdt section : secMappingMdt){
if(intIdToSecMap.containsKey(section.NeuStar_Integration_Mapping__c)) {
List<NeuStar_Integration_Section_Mapping__mdt> tempLst = intIdToSecMap.get(section.NeuStar_Integration_Mapping__c);
tempLst.add(section);
intIdToSecMap.put(section.NeuStar_Integration_Mapping__c,tempLst);
}
else{
List<NeuStar_Integration_Section_Mapping__mdt> tempLst = new List<NeuStar_Integration_Section_Mapping__mdt>();
tempLst.add(section);
intIdToSecMap.put(section.NeuStar_Integration_Mapping__c,tempLst);
}
}
System.debug('section Map***************'+intIdToSecMap);
return intIdToSecMap;
}*/
    
    /***************************************************************************************
@description : Create Map for subsection metadata  
****************************************************************************************/ 
    /* public static Map<Id,List<NeuStar_Integration_SubSection_Mapping__mdt>> getSubsectionMetadatMap(Map<Id,List<NeuStar_Integration_Section_Mapping__mdt>> intIdToSecMap){
Set<Id> sectionIdLst =  new Set<Id>();
if(intIdToSecMap!=null && intIdToSecMap.keySet().size()>0){
for(Id mainsectionId : intIdToSecMap.keySet()){
for(NeuStar_Integration_Section_Mapping__mdt section : intIdToSecMap.get(mainsectionId)){
sectionIdLst.add(section.Id);
}
}
}
Map<Id,List<NeuStar_Integration_SubSection_Mapping__mdt>> secIdToSubsecMap =  new Map<Id,List<NeuStar_Integration_SubSection_Mapping__mdt>>();
List<NeuStar_Integration_SubSection_Mapping__mdt> subsecMappingMdt =  [select id,Field_Names__c,Sub_Section_Name__c,NeuStar_Integration_Section_Mapping__c from NeuStar_Integration_SubSection_Mapping__mdt
where NeuStar_Integration_Section_Mapping__c IN :sectionIdLst];
for(NeuStar_Integration_SubSection_Mapping__mdt subsection : subsecMappingMdt){
if(secIdToSubsecMap.containsKey(subsection.NeuStar_Integration_Section_Mapping__c)) {
List<NeuStar_Integration_SubSection_Mapping__mdt> tempLst = secIdToSubsecMap.get(subsection.NeuStar_Integration_Section_Mapping__c);
tempLst.add(subsection);
secIdToSubsecMap.put(subsection.NeuStar_Integration_Section_Mapping__c,tempLst);
}
else{
List<NeuStar_Integration_SubSection_Mapping__mdt> tempLst = new List<NeuStar_Integration_SubSection_Mapping__mdt>();
tempLst.add(subsection);
secIdToSubsecMap.put(subsection.NeuStar_Integration_Section_Mapping__c,tempLst);
}
}
System.debug('Subsection Map***************'+secIdToSubsecMap);
return secIdToSubsecMap;
}*/
    /***************************************************************************************
@description : Create Map for subsection metadata  
****************************************************************************************/    
    /*public static Map<String,String> getDataTransformationData(){
String objectName = 'PurchaseOrder';
Map<String,String> picklistValueToAPICodeMap = new Map<String,String>();
for(Data_Transformation__mdt data : [SELECT API_Code__c, Attribute_Name__c, Picklist_Value__c, SFDC_Field_Api_Name__c, SFDC_Object_Name__c from Data_Transformation__mdt where SFDC_Object_Name__c =: objectName]){
if(data.Picklist_Value__c != NULL && data.API_Code__c!=null){
picklistValueToAPICodeMap.put(data.Picklist_Value__c,data.API_Code__c);
}
}
return picklistValueToAPICodeMap;
}*/
    
/***************************************************************************************
@description : Method to update Work order API status basewd on all purchase order   
****************************************************************************************/
    
    public static void updateCaseAPIStatus(List<Case> lstCaseIds){
        if(lstCaseIds!=null && lstCaseIds.size()>0){
            List<Purchase_Order__c> poList =[select Id,Work_Order_Task__c,API_Status__c from Purchase_Order__c where Work_Order_Task__c IN :lstCaseIds];
            Map<Id,List<Purchase_Order__c>> caseIdToPoLstMap = new Map<Id,List<Purchase_Order__c>>();
            for(Purchase_Order__c poOrder :poList){
                if(caseIdToPoLstMap.containsKey(poOrder.Work_Order_Task__c)){
                    caseIdToPoLstMap.get(poOrder.Work_Order_Task__c).add(poOrder);
                }
                else{
                    caseIdToPoLstMap.put(poOrder.Work_Order_Task__c, new List<Purchase_Order__c>{poOrder});
                }
            }
            System.debug('in updateCaseAPIStatus caseIdToPoLstMap************'+caseIdToPoLstMap);
            Boolean flag = false;
            Boolean failflag = false;
            List<Case> caseLstToUpadte = new List<Case>();
            for(ID csId :caseIdToPoLstMap.keySet()){
                flag=false;
                failflag=false;
                for(Purchase_Order__c poOrder : caseIdToPoLstMap.get(csId)){
                    if(poOrder.API_Status__c == 'New' || poOrder.API_Status__c == null){
                        flag = true;
                        break;
                    }
                }
                System.debug('flag*********'+flag);
                if(!flag){
                    for(Purchase_Order__c poOrder : caseIdToPoLstMap.get(csId)){
                        if(poOrder.API_Status__c == 'Failed'){
                            failflag = true;
                            break;
                        }
                    }
                    System.debug('failflag*********'+failflag);
                    Case csToUpdate = new Case();
                    csToUpdate.Id = csId;
                    csToUpdate.recordtypeid = workOrderTaskRecordType;
                    if(failflag){
                        csToUpdate.OM_Integration_Status__c = 'Integration Failure';     
                    }
                    else{
                        csToUpdate.OM_Integration_Status__c = 'Integration Success'; 
                    }
                    
                    caseLstToUpadte.add(csToUpdate);
                    System.debug('caseLstToUpadte************'+caseLstToUpadte);
                }
            } 
            Database.SaveResult[] srCaseLstToUpdate;
            List<Integration_Log__c> logList = new List<Integration_Log__c>();
            if(caseLstToUpadte!=null && caseLstToUpadte.size()>0){
                srCaseLstToUpdate = database.update(caseLstToUpadte,false);
                // Iterate through each returned result
                for (Database.SaveResult sr : srCaseLstToUpdate) {
                    if (!sr.isSuccess()) {
                        string errorMsg = '';
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            if(errorMsg == ''){
                                errorMsg = err.getStatusCode() + ': ' + err.getMessage() + ': ' + err.getFields();
                            }
                            else{
                                errorMsg = errorMsg + err.getStatusCode() + ': ' + err.getMessage() + ': ' + err.getFields();
                            }
                        }
                        Integration_Log__c intLogInsert = new Integration_Log__c(Task__c = sr.getId(),Reprocess__c =false);
                        intLogInsert = createIntegrationLog(intLogInsert,null,'Failure',null,null, 'Create Purchase Order',
                                                            'There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again',errorMsg,'DML Error');
                        logList.add(intLogInsert);
                    }
                }
                if(logList!=null && logList.size()>0){
                    insert logList;
                }
            }
        }
    }
    
    
    /****************************************************************************
*  @Description : Generic Method for creating JSON Of Create Order API for 5 fields
/*****************************************************************************/
    public static ReturnParamsToBatchAfterCallout generateCreateOrderJSONwithFiveFields (Map<Id,Purchase_Order__c> idToPOrderMap){
        Map<Id,Integration_Log__c> pO_IL_Map = new Map<Id,Integration_Log__c>();
        for(Integration_Log__c log : [SELECT Id, Task__c,Purchase_Order__c,Reprocess__c,Request__c,Response__c from Integration_Log__c where Purchase_Order__c IN: idToPOrderMap.keySet()]){
            pO_IL_Map.put(log.Purchase_Order__c, log);
        }
        ReturnParamsToBatchAfterCallout dmlActionsWrapper = new ReturnParamsToBatchAfterCallout();
        List<Integration_Log__c> logLst = new List<Integration_Log__c>();
        List<Purchase_Order__c> updatePOLst = new List<Purchase_Order__c>();
        try{
            if(idToPOrderMap!=null && idToPOrderMap.keySet().size()>0){
                for(Purchase_Order__c purchaseOrder : idToPOrderMap.Values()){
                    System.debug('purchaseOrder***********'+purchaseOrder);
                    CreateOrderWrapper reqWrap = new CreateOrderWrapper();
                    reqWrap.activity = purchaseOrder.Activity__c;
                    reqWrap.tradingPartner = purchaseOrder.Trading_Partner__c;
                    reqWrap.productCatalogName = purchaseOrder.Product_Name__c;
                    reqWrap.action = 'save';
                    
                    List<ProductOrder> productOrderLst = new List<ProductOrder>();
                    List<SubSections_Z> subSectionLst = new List<SubSections_Z>();
                    ProductOrder pOrder = new ProductOrder();
                    pOrder.sectionName = 'Base';
                    pOrder.subSections = subSectionLst;
                    pOrder.index = 1;
                    
                    List<String> fieldAPINameLst = new List<String>{'PON__c', 'DDD__c'};
                    List<Fields> fieldsLst = new List<Fields>();   
                    for(String fieldNm : fieldAPINameLst){
                        Fields fie = new Fields();
                        if(fieldNm == 'PON__c'){
                            fie.fieldName = 'PON';
                            if(purchaseOrder.PON__c != null){
                                fie.value = purchaseOrder.PON__c;
                            }
                            else{
                                fie.value = null;
                            }
                        }
                        if(fieldNm == 'DDD__c'){
                            fie.fieldName = 'DDD';
                            if(purchaseOrder.DDD__c != null){
                                Date dt =  Date.valueOf(purchaseOrder.DDD__c);
                                String strDate = dt.month() +'-' + dt.day() +'-' + dt.year();
                                fie.value = strDate;
                            }
                            else{
                                fie.value = null;
                            }
                            
                        }
                        fieldsLst.add(fie);
                    }
                    pOrder.fields = fieldsLst;
                    productOrderLst.add(pOrder);
                    reqWrap.productOrder = productOrderLst;
                    
                    String createOrderWrapStr = JSON.Serialize(reqWrap);
                    System.debug('>>>>>>>>>>>createOrderWrapStr>>>>>>>>>'+createOrderWrapStr);
                    HTTPResponse response = makeRestCallout('callout:CreateOrderInNeustar','POST',
                                                            createOrderWrapStr);
                    Integration_Log__c log = NULL;
                    if(pO_IL_Map != NULL && pO_IL_Map.containsKey(purchaseOrder.Id)){
                        log = new integration_Log__c(Id = pO_IL_Map.get(purchaseOrder.Id).Id,Reprocess__c = false);
                    }
                    else{
                        log = new integration_Log__c(Purchase_Order__c = purchaseOrder.Id,Reprocess__c = false);
                    }
                    Purchase_Order__c poOrder = new Purchase_Order__c();
                    poOrder.Id = purchaseOrder.Id;
                    poOrder.Work_Order_Task__c =  purchaseOrder.Work_Order_Task__c;
                    try{
                        system.debug('response:'+response.getBody());
                        if(response.getBody()!=null && response.getBody().contains('Success')){ 
                            SuccessReponseWrapper successResponseWrapper = (SuccessReponseWrapper) System.JSON.deserialize(response.getBody(), SuccessReponseWrapper.class);
                            if(successResponseWrapper!=null && successResponseWrapper.info!=null && successResponseWrapper.info.uomOrderID!=null){
                                poOrder.UOMOrderId__c = successResponseWrapper.info.uomOrderID;    
                            }
                            if(successResponseWrapper!=null && successResponseWrapper.info!=null && successResponseWrapper.info.ver!=null){
                                poOrder.Version__c = successResponseWrapper.info.ver; 
                            }
                            poOrder.API_Status__c = 'Successful';
                            updatePOLst.add(poOrder);
                            //Create Integration Log for success.
                            log = createIntegrationLog(log,createOrderWrapStr, 'Success', JSON.Serialize(successResponseWrapper),successResponseWrapper.message,'Create Purchase Order',null,null,null);
                        } else if(response.getBody().contains('failure')){
                            poOrder.API_Status__c = 'Failed';
                            updatePOLst.add(poOrder);
                            //Create Integration Log for Failure.
                            ErrorReponseWrapper responseWrap = (ErrorReponseWrapper) System.JSON.deserialize(response.getBody(), ErrorReponseWrapper.class);
                            if(responseWrap!=null && responseWrap.errorList[0]!=null && responseWrap.errorList[0].description.contains('The Token has expired')){
                                log.Task__c  =  purchaseOrder.Work_Order_Task__c; 
                            }
                            log = createIntegrationLog(log,createOrderWrapStr,'Failure', JSON.Serialize(responseWrap), responseWrap.errorList[0].description, 'Create Purchase Order','There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again',responseWrap.errorList[0].description,'Callout Error');
                        }
                        logLst.add(log);
                    }Catch(Exception Ex){
                        log = createIntegrationLog(log,createOrderWrapStr,'Failure',ex.getMessage(), NULL, 'Create Purchase Order','There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again',ex.getMessage(),'Callout Error');
                        logLst.add(log);
                    }
                }
            }
        }
        catch(Exception ex){
            System.debug('Exception *********'+ex);
        }
        finally{
            dmlActionsWrapper.logsToBeUpserted = logLst;
            dmlActionsWrapper.poToBeUpdated = updatePOLst;
        }
        return dmlActionsWrapper;
    }       
    
/********************************************************************************************************
@description: Perform Neustar-Create Order API DMLs
*********************************************************************************************************/
    public static void performCreatePOrderDMLs(ReturnParamsToBatchAfterCallout poWrapper){
        if(poWrapper != NULL && poWrapper.poToBeUpdated.Size() > 0){
            Database.SaveResult[] srUpdatePoLst;
            srUpdatePoLst = database.update(poWrapper.poToBeUpdated,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srUpdatePoLst) {
                if (!sr.isSuccess()) {
                    string errorMsg = '';
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) { 
                        if(errorMsg == ''){
                            errorMsg = err.getStatusCode() + ': ' + err.getMessage() + ': ' + err.getFields();
                        }
                        else{
                            errorMsg = errorMsg + err.getStatusCode() + ': ' + err.getMessage() + ': ' + err.getFields();
                        }
                    }
                    Integration_Log__c intLogInsert = new Integration_Log__c(Purchase_Order__c = sr.getId(),Reprocess__c =false);
                    intLogInsert = createIntegrationLog(intLogInsert,null,'Failure',null,null, 'Create Purchase Order',
                                                        'There is a Salesforce Internal Error. Please contact your Salesforce Administrator or Try again',errorMsg,'DML Error');
                    poWrapper.logsToBeUpserted.add(intLogInsert);
                }
            }
           
        }
        if(poWrapper != NULL && poWrapper.logsToBeUpserted.Size() > 0){
            upsert poWrapper.logsToBeUpserted;
        }
    }
    
}
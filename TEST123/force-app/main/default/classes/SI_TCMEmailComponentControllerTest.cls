/******************************************************************************
@author      - IBM
@date        - 15.07.2020
@description - Test class for SI_TCMEmailComponentController apex class
@Version     - 1.0
*******************************************************************************/
@isTest
class SI_TCMEmailComponentControllerTest{
    /**************************************************************************
    * @description : Method to setup test data
    **************************************************************************/
    @testSetup
    static void testDataSetup() {
        // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        //Case Record type
        Id macroDeselectRecTypeId =TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        // Intake site RecordType
        Id coreRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Intake_Site__c',System.Label.SI_Opp_VICC_Case_RT.split(';')[1]);
        //Intialize list of Opportunity to insert records
        List<Opportunity> listOfOpportunitiesToInsert = new List<Opportunity>();
        //Intialize list of case to insert records
        List<Case> caseList = new List<Case>();
        //Intialize list of contract site to insert records
        List<Contract_Site__c> listContractSite = new List<Contract_Site__c>();
         //Intialize list of Intake site to insert records
        List<Intake_Site__c> listIntakeSite = new List<Intake_Site__c>();
        
        //create Opportunity for Region
        Opportunity oppRecForRegion = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForRegion.Region__c = 'West';
        oppRecForRegion.ETL_MRC__c = 100.00;
        oppRecForRegion.ETL_NRC__c = 100.00;
        oppRecForRegion.Contract_Start_Date__c = System.today();
        listOfOpportunitiesToInsert.add(oppRecForRegion);
       
        //insert Opportunity records
        insert listOfOpportunitiesToInsert;
        
        Case objCase = TestDataFactory.createCaseSingleIntake(1,macroDeselectRecTypeId)[0];
        objCase.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        caseList.add(objCase);
        insert caseList;
        
        //Create records for Intake site 
        Intake_Site__c objIntakeSite = TestDataFactory.createIntakeSiteSingleIntake(1,caseList[0].id,coreRecTypeId)[0];
        objIntakeSite.Case__c = caseList[0].Id;
        listIntakeSite.add(objIntakeSite);
        insert listIntakeSite;
        
        //Create records for Contract site 
        Contract_Site__c objContractSite = TestDataFactory.createConractSiteSingleIntake(1,listOfOpportunitiesToInsert[0].id)[0];
        objContractSite.Opportunity__c = listOfOpportunitiesToInsert[0].Id;
        objContractSite.Intake_Site__c = listIntakeSite[0].Id;
        listContractSite.add(objContractSite);
        insert listContractSite;
    }
    
    /****************************************************************************
    * @description : To test update get Opportunity Id (to get Cases & Contract Sites)
    *****************************************************************************/
    @isTest
    static void testToGetOpportunity(){
        //Intialize list of Opportunity to update records
        List<Opportunity> lstOfOpportunities = new List<Opportunity>();
        for(Opportunity objOpp : [SELECT Id FROM Opportunity]){
            lstOfOpportunities.add(objOpp);
        }
        Test.startTest();
        SI_TCMEmailComponentController objEmailController = new SI_TCMEmailComponentController();
        objEmailController.opportunityId = lstOfOpportunities[0].Id;
        objEmailController.getAllData();
        Test.stopTest();
        System.assertEquals(true,!lstOfOpportunities.isEmpty(),'Opportunity List is not empty');
        System.assertEquals(true,!objEmailController.lstOfCases.isEmpty(),'Case List is not empty');
        System.assertEquals(true,!objEmailController.lstOfContractSites.isEmpty(),'Contract Site List is not empty');
    }                                 
}
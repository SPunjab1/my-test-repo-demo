/******************************************************************************
@author      - IBM
@date        - 13.08.2020
@description - Helper Class of SI_ContractSiteTriggerHandler .
@version     - 1.0
*******************************************************************************/
public with sharing class SI_ContractSiteTriggerHelper {
    /**************************************************************************
    * @description - Method to update CMG working Status field on location(before delete).
    * @param oldListOfContractSites - trigger.old
    **************************************************************************/
    public static void updateLocationCMGWorkingStatusOnDelete(List<Contract_Site__C> oldListOfContractSites){
        //map of contarctsite with opportunity
        Map<Id,String> oppMap = new Map<Id,String>();
        //set of opportunity id
        Set<Id> oppIdSet = new Set<Id>();
        //set of location id
        Set<Id> locationIdSet = new Set<Id>();
        //list of update location 
        list<Location_ID__c> listOfUpdateLocation = new List<Location_ID__c>();
        for(Contract_Site__c contractsite:oldListOfContractSites){
            //add opportunity with respesct to contract site
            oppMap.put(contractsite.Id,contractsite.opportunity__c);
            oppIdSet.add(contractsite.opportunity__c);
        }
        for(opportunity opp : [select id,Single_Intake_Team__c from opportunity where id IN:oppIdSet]){
            //put single intake with respect to opportunity
            oppMap.put(opp.Id,opp.Single_Intake_Team__c);
        }
        for(Contract_Site__c contractsite:oldListOfContractSites){
            //filter opportunity single intake team = backhaul and opportunity type not marco deselect 
            if(oppMap.get(contractsite.Opportunity__c)==Label.SI_OppTeam.split(',')[0] && string.isNotBlank(contractsite.Location_ID__c)&& contractsite.Opportunity__r.Type != Label.SI_Deselect_Type){
                //add locaton in list
                locationIdSet.add(contractsite.Location_ID__c); 
            }
        }
        if(!locationIdSet.isEmpty()){
            //list of location which is releated to contract site
            List<Location_ID__c> listOfLocation  = [SELECT id,AAV_Status__c,CMG_Working_Status__c,Previous_AAV_Status__c,Previous_CMG_Working_Status__c FROM Location_ID__c WHERE Id IN : locationIdSet
                                                    AND(Previous_AAV_Status__c!= '' OR Previous_CMG_Working_Status__c!= '')];
            if(!listOfLocation.isEmpty()){
                for(Location_ID__c objLocation : listOfLocation){
                    objLocation.AAV_Status__c = objLocation.Previous_AAV_Status__c;
                    objLocation.CMG_Working_Status__c = objLocation.Previous_CMG_Working_Status__c;
                    //add location in the update list
                    listOfUpdateLocation.add(objLocation);
                }
            } 
        }   
        if(!listOfUpdateLocation.isEmpty()){
            try{
                //update location records
                SI_UtilityClass.callDMLOperations(listOfUpdateLocation, Label.SI_DML_Operations.split(';')[1]);
            } catch(Exception e){
                System.debug(LoggingLevel.ERROR,'The following exception has occurred: ' + e.getMessage());
            }
        }
    }
}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : WorkOrderCreationBatch
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 09.07.2019
*  @Description      : Batch Class for creation of Work order(Case) records.
**********************************************************************************************************************
**********************************************************************************************************************/
global class WorkOrderCreationBatch implements Database.Batchable<Case>, Database.Stateful{
    global List<Case> caseList;
    global Integer count;
    global String intakeId;
/***************************************************************************************
* @Description  :Constructor to initialise CaseLst and intakeId
* @input params :caseList,intakeid
***************************************************************************************/  
    global WorkOrderCreationBatch(List<Case> caseList,String intakeId){
        this.caseList = caseList;
        this.intakeId = intakeId;
    }
/***************************************************************************************
* @Description  :Start Method of Batch Class to initialize count and return Case list
* @input params :Database.BatchableContext
***************************************************************************************/ 
    global Iterable<Case> start(Database.BatchableContext BC) {
        count = 0;
        return caseList;
    }
/*********************************************************************************************************
* @Description  : Execute Method of Batch Class to insert work order records and get success record count
* @input params :Database.BatchableContext,List<Case>
**********************************************************************************************************/    
    global void execute(Database.BatchableContext BC, List<Case> scope) {
            if(scope!=null && scope.size()>0){
                Database.SaveResult[] result = Database.insert(scope,false);
                for(Database.SaveResult sr : result) {
                    if (sr.isSuccess()) {
                        count = count + 1;
                    }
                }
            }  
    }   
/*********************************************************************************************************
* @Description  : Finish Method of Batch Class to update intake Request field with number of failure records
* @input params : Database.BatchableContext
**********************************************************************************************************/ 
    global void finish(Database.BatchableContext BC) {
        Integer failRecordCnt = caseList.size() - count;
        if(failRecordCnt > 0){
            Intake_Request__c req = new Intake_Request__c();
        	req.id = intakeId;
        	req.ErrorMessage__c = failRecordCnt + ' ' + label.ErrorMessageforFailureRecords; 
            Database.update(req,true);
        }
    }
}
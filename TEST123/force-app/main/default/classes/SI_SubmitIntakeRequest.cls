/******************************************************************************
@author      : IBM
@date        : 02.06.2020
@description : Class to Update case status and case fire assignment rules.
@Version     : 1.0
*******************************************************************************/
public with sharing class SI_SubmitIntakeRequest {
    
    /**************************************************************************
    * @description To hold case record id
    **************************************************************************/
    public Id caseId {get;set;}
    
    /**************************************************************************
    * @description Flag to show hide error block
    **************************************************************************/
    public Boolean hasError {get;set;}
    
    /**************************************************************************
    * @description To hold the error message for error block
    **************************************************************************/
    public String errorMessage {get;set;}
    
    /**************************************************************************
    * @description To initialize variables and put case status validation
    **************************************************************************/
    public SI_SubmitIntakeRequest() {
        //Intialize class variables
        hasError = false;
        try {
            //set caseId variable with current case record
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('id'))) {
                caseId = ApexPages.currentPage().getParameters().get('id').escapeHtml4();
            }
            //Check if case Id is not blank
            if(String.isNotBlank(caseId)) {
                //fetch case details
                Case caseRec = [SELECT Id, Status FROM Case WHERE Id = :caseId];
                //check if current status is not draft and set validation message
                if(caseRec.Status != Label.SI_CaseStatusDraft) {
                    hasError = true;
                    errorMessage = Label.SI_CaseSubmitValidationMessage;
                }
            } else {
                hasError = true;
                errorMessage = Label.SI_GeneralExceptionMessage;
            }
        } catch (Exception ex) {
            //set hasError variable to true to display error block
            hasError = true;
            //set error message to be displayed in error block
            errorMessage = Label.SI_GeneralExceptionMessage + ': ' + ex.getMessage();
        }
    }
    
    /**************************************************************************
    * @description To update case status and fire assignment rules
    **************************************************************************/
    public void updateCaseStatus() {
        try {
            //set DML option to fire assignment rules and trigger emails
            Database.DMLOptions dmlOption = SI_UtilityClass.setDmlOptionForAssignmentRules();
            //set case record to update
            Case caseRec = new Case();
            caseRec.Id = caseId;
            caseRec.setOptions(dmlOption);
            caseRec.Status = Label.SI_CaseStatusNewSubmitted;
            //deactivate case validation rule SI_Community_User_Cant_Change_CaseStatus
            ValidationRuleManager__c validationRuleSetting = ValidationRuleManager__c.getOrgDefaults();
            if(validationRuleSetting.Run_Validation__c = true) {
                validationRuleSetting.Run_Validation__c = false;
                update validationRuleSetting;
            }
            //update case records
            update caseRec;
            //rectivate case validation rule SI_Community_User_Cant_Change_CaseStatus
            validationRuleSetting.Run_Validation__c = true;
            update validationRuleSetting;
            
            } catch (DMLException ex) {
            //Check for DML Exception
            hasError = true;
            errorMessage = Label.SI_GeneralExceptionMessage + ': ' + ex.getMessage();
        } catch(Exception ex) {
            //Check for any other general exception
            hasError = true;
            errorMessage = Label.SI_GeneralExceptionMessage + ': ' + ex.getMessage();
        }
    }
    
    /**************************************************************************
    * @description To navigate back to case record page
    * @return PageReference
    **************************************************************************/
    public PageReference backToCaseRecord() {
        //Intialize page reference variable to store return location
        PageReference pageRef;
        try {
            //check if case id variable having value 
            if(String.isNotBlank(this.caseId)) {
                //set pageReference to called case record id
                pageRef = new PageReference('/' + caseId);
            } else {
                //set to case object record list page
                pageRef = new PageReference('/500');
            }
        } catch(Exception ex) {
            System.debug(LoggingLevel.INFO, 'Exception: ' + ex.getMessage());
            //set to case object record list page
            pageRef = new PageReference('/500');
        }
        return pageRef;
    }
}
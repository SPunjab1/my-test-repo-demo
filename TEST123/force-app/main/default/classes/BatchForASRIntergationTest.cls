@isTest 
public class BatchForASRIntergationTest{
    static testMethod void BatchForASRIntergationTest(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'Add Circuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        
        Insert TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].Circuit_ID_Primary_Key__c = '';//5232432546436
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Circuit: Ethernet';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,1);
        asrList[0].ATOMs_ASR_Id__c = '645765876';
        asrList[0].ASR_Type__c = 'New Service: Ethernet';
        asrList[0].Circuit_Id__c =   circuitList[0].id;
        INSERT asrList;
        
        List<ASR__c> listASR = [Select id,VLAN__c,VLAN_2__c,Order_Number__c,Circuit_Id__r.Purchase_Order_Id__c,API_Status__c,Order_Destination__c,SubService_Type__c,case__r.parent.caseNumber,Case__r.parent.PIER_Work_Order_ID__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,Circuit_Id__c,Delivered_Date__c,Change_Type__c,Change_Value__c,Forecast_Version__c,Deliver__c,Desired_Due_Date__c,TPE_Request_Number__c,Target_Download_Speed__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c,Circuit_Id__r.NNI_ID_Primary_Key__c,PON__c,ATOMs_ASR_Id__c,Circuit_Id__r.RecordType.Name,Case__c,ASR_Type__c from ASR__c WHERE ATOMs_ASR_Id__c ='645765876'];
        System.assertEquals('New Service: Ethernet', listASR[0].ASR_Type__c);
        System.debug('listASR!!!!!!!!!!'+listASR);
        //for(ASR__c asrtempp:listASR){
         // caseIds.add(asrtempp.Case__c);
        //}
        //List<Case> tempCaseList = [Select id,(Select id,ASR_Type__c from ASRs__r),(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        //System.debug('tempCaseList!!!!!!'+tempCaseList);
        Test.startTest();
            BatchForASRIntergation tempASRNew = new BatchForASRIntergation();
            tempASRNew.start(BC);
            tempASRNew.execute(SC);
            tempASRNew.execute(BC,listASR);
            tempASRNew.finish(BC);
            BatchForASRIntergation.getAsrTypeValues();
        Test.stopTest();
    }
    static testMethod void BatchForASRIntergationTest1(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'Add Circuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        Insert TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].Circuit_ID_Primary_Key__c = '';//5232432546436
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Circuit: Ethernet';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,1);
        asrList[0].ATOMs_ASR_Id__c = '645765876';
        asrList[0].ASR_Type__c = 'BW Change: BB';
        asrList[0].Service_Type__c = 'Broadband';
        asrList[0].SubService_Type__c = 'Broadband';
        asrList[0].Ethernet_Technology_Type__c = 'Cable';
        asrList[0].Circuit_Id__c =   circuitList[0].id;
        INSERT asrList;
        
        List<ASR__c> listASR = [Select id,VLAN__c,VLAN_2__c,Order_Number__c,Circuit_Id__r.Purchase_Order_Id__c,API_Status__c,Order_Destination__c,SubService_Type__c,case__r.parent.caseNumber,Case__r.parent.PIER_Work_Order_ID__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,Circuit_Id__c,Delivered_Date__c,Change_Type__c,Change_Value__c,Forecast_Version__c,Deliver__c,Desired_Due_Date__c,TPE_Request_Number__c,Target_Download_Speed__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c,Circuit_Id__r.NNI_ID_Primary_Key__c,PON__c,ATOMs_ASR_Id__c,Circuit_Id__r.RecordType.Name,Case__c,ASR_Type__c from ASR__c WHERE ATOMs_ASR_Id__c ='645765876'];
        System.assertEquals('BW Change: BB', listASR[0].ASR_Type__c);
        //for(ASR__c asrtempp:listASR){
         // caseIds.add(asrtempp.Case__c);
        //}
        //List<Case> tempCaseList = [Select id,(Select id,ASR_Type__c from ASRs__r),(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        //System.debug('tempCaseList!!!!!!'+tempCaseList);
        Test.startTest();
            BatchForASRIntergation tempASRNew = new BatchForASRIntergation();
            tempASRNew.start(BC);
            tempASRNew.execute(SC);
            tempASRNew.execute(BC,listASR);
            tempASRNew.finish(BC);
            BatchForASRIntergation.getAsrTypeValues();
        Test.stopTest();
    }
    static testMethod void BatchForASRIntergationTest2(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'Add Circuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        Insert TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].Circuit_ID_Primary_Key__c = '';//5232432546436
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Circuit: Ethernet';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,1);
        asrList[0].ATOMs_ASR_Id__c = '645765876';
        asrList[0].ASR_Type__c = 'Disconnect: SAT';
        asrList[0].Service_Type__c = 'Satellite';
        asrList[0].SubService_Type__c = 'UNI';
        asrList[0].Ethernet_Technology_Type__c = 'AAV';
        asrList[0].Circuit_Id__c =   circuitList[0].id;
        INSERT asrList;
        
        List<ASR__c> listASR = [Select id,VLAN__c,VLAN_2__c,Order_Number__c,Circuit_Id__r.Purchase_Order_Id__c,API_Status__c,Order_Destination__c,SubService_Type__c,case__r.parent.caseNumber,Case__r.parent.PIER_Work_Order_ID__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,Circuit_Id__c,Delivered_Date__c,Change_Type__c,Change_Value__c,Forecast_Version__c,Deliver__c,Desired_Due_Date__c,TPE_Request_Number__c,Target_Download_Speed__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c,Circuit_Id__r.NNI_ID_Primary_Key__c,PON__c,ATOMs_ASR_Id__c,Circuit_Id__r.RecordType.Name,Case__c,ASR_Type__c from ASR__c WHERE ATOMs_ASR_Id__c ='645765876'];
        System.assertEquals('Disconnect: SAT', listASR[0].ASR_Type__c);
        //for(ASR__c asrtempp:listASR){
         // caseIds.add(asrtempp.Case__c);
        //}
        //List<Case> tempCaseList = [Select id,(Select id,ASR_Type__c from ASRs__r),(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        //System.debug('tempCaseList!!!!!!'+tempCaseList);
        Test.startTest();
            BatchForASRIntergation tempASRNew = new BatchForASRIntergation();
            tempASRNew.start(BC);
            tempASRNew.execute(SC);
            tempASRNew.execute(BC,listASR);
            tempASRNew.finish(BC);
            BatchForASRIntergation.getAsrTypeValues();
        Test.stopTest();
    }
  }
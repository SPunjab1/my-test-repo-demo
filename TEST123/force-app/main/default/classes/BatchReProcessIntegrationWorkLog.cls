/*********************************************************************************************
@author     : IBM
@date       : 20.06.19
@description: This class is re- processing the failed records from integration log table for DWH
@Version    : 1.0
**********************************************************************************************/

global class BatchReProcessIntegrationWorkLog implements Database.Batchable<sObject>,Schedulable {

    global Database.QueryLocator start(Database.BatchableContext BC) {        
          String query = 'SELECT Id,Reprocess__c,Status__c, Request__c,Response_Message__c, Worklog_Description__c, Task__c FROM Integration_Log__c WHERE API_Type__c = \'DWH WorkLog\' AND Status__c = \'Failure\'';        
          return Database.getQueryLocator(query);
    }
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())Database.executeBatch(new BatchReProcessIntegrationWorkLog(), 5);
    }
    
    global void execute(Database.BatchableContext BC, List<Integration_Log__c> scope) {
        Map<string, Integration_Log__c> taskLogMap = new Map<string, Integration_Log__c>();
        
        Map<id,Integration_Log__c> integrationLogsToUpdateMap=new Map<id,Integration_Log__c>();
        List<Worklog_Entry__c> WorkLogLstToBeUpdated = new List<Worklog_Entry__c>();
        
        String usrCode;        
        for(Integration_Log__c integrationLog :scope){            
                Worklog_Entry__c worklogEntry = new Worklog_Entry__c();
                worklogEntry.Comment__c = integrationLog.Worklog_Description__c;                
                worklogEntry.Task__c = integrationLog.Task__c ;     
                if(integrationLog.Request__c != Null && String.valueOf(integrationLog.Request__c).Contains('User_Code__c='))
                	usrCode=((String.valueOf(integrationLog.Request__c)).substringAfter('User_Code__c=')).substringBefore(',');
                worklogEntry.User_Code__c = usrCode ;
                WorkLogLstToBeUpdated.add(worklogEntry);                
                taskLogMap.put(integrationLog.Task__c+'-'+integrationLog.WorkLog_Description__c, integrationLog);
                
         }
        
        if(WorkLogLstToBeUpdated != null && WorkLogLstToBeUpdated.size() >0){
            List<Database.SaveResult> updateResults = database.insert(WorkLogLstToBeUpdated,false);
            
            for(Integer i=0;i<updateResults.size();i++){
                Integration_Log__c errorLog = new Integration_Log__c(Id = taskLogMap.get(WorkLogLstToBeUpdated.get(i).Task__c+'-'+WorkLogLstToBeUpdated.get(i).Comment__c).Id);             
                if(updateResults.get(i).isSuccess()){
                    errorLog.Status__c = 'Success';
                    errorLog.Response_Message__c = 'WorkLog Entry Updated Successfully';
                }
                else if (!updateResults.get(i).isSuccess()){                    
                    String errorMsg = '';
                    for(Database.Error error : updateResults.get(i).getErrors()){errorMsg += error.getMessage() +'\n';
                    }
                    errorLog.Response_Message__c = errorMsg;errorLog.Worklog_Description__c = WorkLogLstToBeUpdated.get(i).Comment__c;errorLog.Status__c = 'Failure';
                 }
                 if(!integrationLogsToUpdateMap.containskey(errorLog.id)){
                    integrationLogsToUpdateMap.put(errorLog.id,errorLog);
                 }  
            }
            //update the log records
            if(integrationLogsToUpdateMap != null && integrationLogsToUpdateMap.size() > 0){
                database.update(integrationLogsToUpdateMap.values(), false);
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}
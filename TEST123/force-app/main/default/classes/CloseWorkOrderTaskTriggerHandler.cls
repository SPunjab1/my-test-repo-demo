/*********************************************************************************************
@author       : IBM
@date         : 19.06.2019
@description  : This is the trigger handler to update status of work order task from DWH
@Version      : 1.0
//v1 : BalajiB: US# PTSP-2041: Aug 19 2020: Added the below 2 status updates as part of the enhancement of the Pier 2.0 scope
**********************************************************************************************/
public with sharing class CloseWorkOrderTaskTriggerHandler{

/*********************************************************************************************
@Description :Method to update work order task from Datawarehouse
@input params : Trigger.New
/*********************************************************************************************/
    Public static void closeWorkOrderTask(List<Close_Work_Order__e> newItems){
        List<Case> workOrderTaskToBeUpdatedLst = new List<Case>();
        Map<String, Close_Work_Order__e> taskEventMap = new Map<String, Close_Work_Order__e>();
        List<Integration_Log__c> logLstToInsert = new List<Integration_Log__c>();
        Map<Id,Id> taskLogMap = new Map<Id,Id>();
        Set<Id> taskIdSet = new Set<Id>();

        for (Close_Work_Order__e event : newItems) {
            taskEventMap.put(event.Work_Order_Task_Id__c, event);
        }

        if(taskEventMap != null && taskEventMap.size() > 0){
            for(Case caseObj : [SELECT Id, Status, PIER_Work_Order_ID__c FROM Case WHERE PIER_Work_Order_ID__c IN: taskEventMap.keySet()]){
                if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Completed' ||
                    taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Closed')
                {
                    caseObj.Status = 'Closed';
                }
                //Changes Start : BalajiB: Added the below 2 status updates as part of the enhancement of the Pier 2.0 scope
                else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Incomplete'){
                    caseObj.Status = 'Incomplete';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Rolled Back'){
                    caseObj.Status = 'Rolled Back';
                }
                //Changes End : BalajiB:
                else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Deployment Ready'){
                    caseObj.Status = 'Deployment Ready';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'InProgress' ||
                         taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Working' ||
                        taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Implementation - In Progress'){
                    caseObj.Status = 'In Progress';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Implemented'){
                    caseObj.Status = 'Implemented';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Waiting Approval'){
                    caseObj.Status = 'Waiting Approval';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Not Required' ||
                    taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'N/A' || 
                    taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Request For Change'){
                    caseObj.Status = 'N/A';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Open' ||
                         taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Assigned' ||
                    taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Draft'){
                        caseObj.Status = 'New';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Waiting'){
                    caseObj.Status = 'Waiting';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Terminated'){
                    caseObj.Status = 'Terminated';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Resolved'){
                    caseObj.Status = 'Closed';
                }else if(taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c == 'Denied'){
                    caseObj.Status = 'Rejected';
                }else{
                    caseObj.Status = taskEventMap.get(caseObj.PIER_Work_Order_ID__c).Work_Order_Task_Status__c;
                }
                taskIdSet.add(caseObj.Id);
                workOrderTaskToBeUpdatedLst.add(caseObj);
            }
        }
        if(taskIdSet.size() >0){
            for(Integration_Log__c log : [SELECT Id, task__c FROM Integration_Log__c WHERE Task__c IN: taskIdSet AND API_Type__c= 'DWH Close Task']){
                taskLogMap.put(log.Task__c, log.Id);
            }
        }
      
        if(workOrderTaskToBeUpdatedLst.size() > 0){
            List<Database.SaveResult> updateResults = database.update(workOrderTaskToBeUpdatedLst, false);
            for(Integer i=0;i<updateResults.size();i++){
                Integration_Log__c errorLog;
                if(updateResults.get(i).isSuccess()){
                    if(taskLogMap.get(workOrderTaskToBeUpdatedLst.get(i).Id) != null){
                        errorLog = new Integration_Log__c(Id = taskLogMap.get(workOrderTaskToBeUpdatedLst.get(i).Id));
                        errorLog.Status__c = 'Success';
                        errorLog.Response_Message__c = 'Task Status Updated Successfully';
                        errorLog.DWH_Task_Status__c = workOrderTaskToBeUpdatedLst.get(i).Status;
                        errorLog.Request__c = String.valueOf(taskEventMap.get(workOrderTaskToBeUpdatedLst.get(i).PIER_Work_Order_ID__c ));
                        errorLog.API_Type__c = 'DWH Close Task';
                        errorLog.Task__c = workOrderTaskToBeUpdatedLst.get(i).Id;
                        logLstToInsert.add(errorLog);
                    }
                }
                else{
                    if(taskLogMap.get(workOrderTaskToBeUpdatedLst.get(i).Id) != null){
                        errorLog = new Integration_Log__c(Id = taskLogMap.get(workOrderTaskToBeUpdatedLst.get(i).Id));
                    }
                    else{
                        errorLog = new Integration_Log__c();
                    }
                    String errorMsg = '';
                    for(Database.Error error : updateResults.get(i).getErrors()){
                        errorMsg += error.getMessage() +'\n';
                    }
                    errorLog.Status__c = 'Failure';
                    errorLog.Response_Message__c = errorMsg;
                    errorLog.DWH_Task_Status__c = workOrderTaskToBeUpdatedLst.get(i).Status;
                    errorLog.Request__c = String.valueOf(taskEventMap.get(workOrderTaskToBeUpdatedLst.get(i).PIER_Work_Order_ID__c ));
                    errorLog.API_Type__c = 'DWH Close Task';
                    errorLog.Task__c = workOrderTaskToBeUpdatedLst.get(i).Id;
                    logLstToInsert.add(errorLog);
                }
            }
                if(logLstToInsert != null && logLstToInsert.size() > 0){
                    database.upsert(logLstToInsert, false);
                }
        }
    }
}
/*********************************************************************************************
@author     : IBM
@date       : 09.06.20
@description: Sample Class for Exposing web service to Neustar 
@Version    : 1.0
**********************************************************************************************/
@RestResource(urlMapping='/receiveNeuStarMessage/*')
global with sharing class NeuStarMessageReciever{
        @HttpPost
        
        global static NeuStarMessageRecieverUtility.ResponseWrapper updateServiceRequest() {
            NeuStarMessageRecieverUtility.ResponseWrapper responseWrapper = new NeuStarMessageRecieverUtility.ResponseWrapper();
            responseWrapper = NeuStarMessageRecieverUtility.receiveMessage();
            System.debug('responseWrapper ==>'+responseWrapper);
            return responseWrapper;
            //NeuStarMessageRecieverUtility.ResponseWrapper
        }
        
}
public class DependentPicklistHandler {
	@InvocableMethod
	public static List<List<String>> getDependentPicklistValues() {
        Schema.sObjectField dependToken = Opportunity.Market_s__c;
		Schema.DescribeFieldResult depend = dependToken.getDescribe();
		Schema.sObjectField controlToken = depend.getController();
		if (controlToken == null) {
			return new List<List<String>>();
		}
	 
		Schema.DescribeFieldResult control = controlToken.getDescribe();
		List<Schema.PicklistEntry> controlEntries;
		if(control.getType() != Schema.DisplayType.Boolean) {
			controlEntries = control.getPicklistValues();
		}
	 
		String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
		Map<String,List<String>> dependentPicklistValues = new Map<String,List<String>>();
		for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
			if (entry.isActive() && String.isNotEmpty(String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))) {
				List<String> base64chars =
						String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
				for (Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++) {
					Object controlValue =
							(controlEntries == null
									?   (Object) (index == 1)
									:   (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null)
							);
					Integer bitIndex = index / 6;
					if (bitIndex > base64chars.size() - 1) {
						break;
					}
					Integer bitShift = 5 - Math.mod(index, 6);
					if  (controlValue == null || (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0)
						continue;
					if (!dependentPicklistValues.containsKey((String) controlValue)) {

						dependentPicklistValues.put((String) controlValue, new List<String>());
					}
					dependentPicklistValues.get((String) controlValue).add(entry.getLabel());
				}
			}
		}
        system.debug(dependentPicklistValues);
        User currentUser = [SELECT Region_NLG__c FROM USER WHERE Id = :UserInfo.getUserId()];
        List<String> userRegions = new List<String>();
        if(currentUser.Region_NLG__c==null){
            return new List<List<String>>();
        }
        for(String region:currentUser.Region_NLG__c.split(';')){
            userRegions.add(region.toLowerCase());
        }
        System.debug(userRegions);
        Set<String> markets = new Set<String>();
         System.debug(dependentPicklistValues);
        for(String region : dependentPicklistValues.keySet()){
            List<String> regionMarkets = dependentPicklistValues.get(region);
            if(userRegions.contains(region.toLowerCase())){
                markets.addAll(regionMarkets);
            }
        }
        List<List<String>> marketvalues = new List<List<String>>();
        List<String> marketValList = new List<String>(markets);
        marketValList.sort();
        marketvalues.add(marketValList);
        return marketvalues;
	}

}
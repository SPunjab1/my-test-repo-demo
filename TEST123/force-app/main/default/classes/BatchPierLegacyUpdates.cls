/*********************************************************************************************
@author     : Balaji
@date       : 2020.08.13
@description: This class is used to enable the release Notes Banner to users.
@Version    : 1.0
**********************************************************************************************/

global class BatchPierLegacyUpdates implements Database.Batchable<sObject>,Schedulable,Database.AllowsCallouts {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select id,API_Type__c,Status__C,Attachment_Id__c,Task__c from Integration_log__C where createddate = Today AND API_Type__c='+'\'Attachment'+'\'';
        system.debug(query);
        return Database.getQueryLocator(query);
    }
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())Database.executeBatch(new BatchPierLegacyUpdates(), 5);
    }
    global void execute(Database.BatchableContext BC, List<Integration_Log__C> scope) {
        system.debug('scope:'+scope);
        Map<Id, ContentVersion> sMapDocIdContentVersion = New Map<Id, ContentVersion>();
        Map<Id,case> sMapCase = New Map<Id,case>();
        Map<Id,String> sMapVersionIdFileURL = New Map<Id,String>();
        Map<Id,String> sMapexistingPubliclinksVersionId = New Map<Id,String>();
        set<Id> caseids = New Set<Id>();
        Map<Id,Id> mapversionIdcaseId = New Map<Id,Id>();
        Map<Id,Map<Id,Id>> sMapCaseIdversionId = New Map<Id,Map<Id,Id>>();
        List<ContentDistribution> lstcontentDistribution  = new List<ContentDistribution>();
        List<Case> scaselist = New List<Case>();
        Set<Id> sContentVersionId = New Set<Id>();
        for(Integration_Log__C templog: scope){
            sContentVersionId.add(templog.Attachment_Id__c);
            mapversionIdcaseId.put(templog.Attachment_Id__c, templog.Task__C);
        }
        for(ContentDistribution temp:[select Id,DistributionPublicUrl,ContentVersionId from ContentDistribution where ContentVersionId in:sContentVersionId]){        
            system.debug('temp:'+temp);
            sMapexistingPubliclinksVersionId.put(temp.ContentVersionId,temp.DistributionPublicUrl);
        }
        system.debug('mapdetails>>:'+sMapexistingPubliclinksVersionId);
        for(ContentVersion temp: [SELECT Id,ContentDocumentId,Title,IsLatest FROM Contentversion WHERE Id in:sContentVersionId]){
            System.debug('entered'+temp.Id);
            system.debug('Contentversionmap>>:'+ sMapexistingPubliclinksVersionId.get(temp.Id));
            if(sMapexistingPubliclinksVersionId.get(temp.Id)==null){
                sMapDocIdContentVersion.put(temp.ContentDocumentId, temp);
                caseids.add(mapversionIdcaseId.get(temp.Id));
                ContentDistribution CD = New ContentDistribution();
                CD.ContentVersionId =temp.Id;
                CD.Name = temp.Id;
                CD.PreferencesAllowOriginalDownload =TRUE;
                CD.PreferencesAllowPDFDownload=FALSE;
                CD.PreferencesAllowViewInBrowser=TRUE;
                CD.PreferencesExpires=FALSE;
                CD.PreferencesLinkLatestVersion=TRUE;
                CD.PreferencesNotifyOnVisit	=FALSE;
                CD.PreferencesNotifyRndtnComplete=FALSE;
                CD.PreferencesPasswordRequired=FALSE;
                lstcontentDistribution.add(CD);
            }
        }
        system.debug('sMapDocIdContentVersion:'+sMapDocIdContentVersion);
        system.debug('sContentVersionId:'+sContentVersionId);
        system.debug('lstcontentDistribution:'+lstcontentDistribution);
        if(lstcontentDistribution != null && lstcontentDistribution.size() >0){
            Insert(lstcontentDistribution);
        }
        for(ContentDistribution temp:[select Id,DistributionPublicUrl,ContentVersionId from ContentDistribution where ContentVersionId in:sContentVersionId]){        
            system.debug('temp:'+temp);
            sMapVersionIdFileURL.put(temp.ContentVersionId,temp.DistributionPublicUrl);
        }
        system.debug('sMapVersionIdFileURL:'+sMapVersionIdFileURL);
        system.debug('sMapCaseIdversionId:'+sMapCaseIdversionId);
        if(mapversionIdcaseId.size()>0){
            for(case temp:[select Id,LegacyFileAttachDate__c,caseNumber,FilePublicURL__c from case where id in: caseids]){
                system.debug(temp.FilePublicURL__c);
                for(Id tempVersionId: mapversionIdcaseId.KeySet()){
                    System.debug('CasefileVersionId>>:'+ tempVersionId + '>>:'+ temp.caseNumber + '>>:'+ mapversionIdcaseId.get(tempVersionId));
                    if(mapversionIdcaseId.get(tempVersionId) == temp.Id){
                        System.debug('Casenumber:'+ temp.caseNumber + temp.FilePublicURL__c);
                        if(temp.FilePublicURL__c!=null){
                        temp.FilePublicURL__c   = temp.FilePublicURL__c + ' ' + sMapVersionIdFileURL.get(tempVersionId);
                        }else{
                            temp.FilePublicURL__c   = sMapVersionIdFileURL.get(tempVersionId);
                        }
                    }
                    system.debug(temp.FilePublicURL__c);
                }   
                temp.LegacyFileAttachDate__c = System.today();
                scaselist.add(temp);
            }
        }
        if(scaselist.size()>0){
            update(scaselist);
        }
        system.debug('scaselist:'+scaselist);
    }
    global void finish(Database.BatchableContext BC) {
        String csvColumnHeader = 'wo_detail_id,worklog,status\r\n';
        String rowStr;
        String bValue='';
        String csvFile;
        set<Id> caseIds = New Set<ID>();
        List<String> csvRowValues = new List<String>();
        List<string> theIDs = new List<string>();
        String documentName = 'PierLegacyUpdates.csv';
        String endpoint = Label.PIER_Endpoint+'/pier-bulk-import/v1/importfiles/workordertaskstatus';
        WrapperObject wrapobj = New WrapperObject();
        List <Report> reportList = [SELECT Id,DeveloperName FROM Report where DeveloperName='Legacy_Pier_10_Status_Updates_WGT'];
        String reportId = (String)reportList.get(0).get('Id');
        Reports.reportResults results = Reports.ReportManager.runReport(reportId, true);
        Reports.ReportFactWithDetails detailFact =(Reports.ReportFactWithDetails) results.getFactMap().get('T!T');
        List<Reports.ReportDetailRow> allRows = detailFact.getRows();
        
        for (Integer r = 0; r < allRows.size(); r++) { 
            caseIds.add(allRows[r].getDataCells().get(0).getlabel());
        }
        if(caseIds.size()>0){
            for(Case  temp:[Select Id,PIER_Work_Order_Task_ID__c,FilePublicURL__c,Status from case where Id in:caseIds]) {
                if(temp.PIER_Work_Order_Task_ID__c !=null){
                    rowStr = temp.PIER_Work_Order_Task_ID__c+',';
                }else{
                    rowStr = ',';
                }
                if(temp.FilePublicURL__c !=null){
                    rowStr = rowStr + '\"'+temp.FilePublicURL__c+ '\",';
                }else{
                    rowStr = rowStr +',';
                }
                if(temp.status !=null){                
                    if(temp.status == 'Closed')
                    {
                        rowStr = rowStr + 'Completed';   
                    }
                    else if(temp.status == 'N/A')
                    {
                        rowStr = rowStr + 'NA';  
                    }else{
                        rowStr = rowStr + temp.status;
                    } 
                }
                csvRowValues.add(rowStr);
            }
        }
        csvFile = csvColumnHeader + string.join(csvRowValues, '\r\n') + '\r\n';
        blob csvBlob = Blob.valueOf(csvFile);
        String FileData = EncodingUtil.base64Encode(csvBlob);
        system.debug('FileData>>:'+FileData);
        /*****************************************************************************/
   /*     string csvname= 'PierLegacyUpdates';
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new list<string> {'balaji164949@gmail.com'};
            String subject ='Account CSV';
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        email.setPlainTextBody('Account CSV ');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); */
        /*****************************************************************************/       
        String a_token = APIHandler.getApigeeToken();
        system.debug('a_token:'+a_token);
        wrapobj.FileName = 'Pi.csv';//+ Datetime.now(). +'.csv';
        wrapobj.FileData = FileData;
        wrapobj.UserId = 'BBodich1';
        wrapobj.SubmitterFullName = 'Balaji Bodicherla';
        wrapobj.SubmitterEmailId = 'Balaji.Bodicherla1@T-Mobile.com';
        
        String requestBody = JSON.Serialize(wrapobj);
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+ a_token);
        req.setHeader('User-id', 'BBodich1');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);        
        req.setBody(requestBody);
        req.setEndpoint(endPoint);
        req.setMethod('POST');
        system.debug('req:'+req);
        system.debug('requestBody:'+requestBody);
        Http http = new Http();
        HTTPResponse response;
        try{
            System.debug(' >>> Inside try');
            response = http.send(req);
            System.debug(' >>> Outside try' +response.getBody());
        } catch(Exception ex){
            response = new HTTPResponse();
            response.setBody(ex.getMessage());
        }
    }    
    global class WrapperObject {
        public String FileName {get;set;} 
        public String FileData {get;set;} 
        public String UserId {get;set;} 
        public String SubmitterFullName {get;set;} 
        public String SubmitterEmailId {get;set;} 
    }
}
/*********************************************************************************************
@author     : IBM
@date       : July 07 2020
@description: Scheduled class To execute the Neustar Listener batch after every 5 Minutes
@Version    : 1.0           
**********************************************************************************************/
global class NeuStartoATOMSUpdateSchedular implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchForNeuStarToATOMSUpdate  batchCls = new BatchForNeuStarToATOMSUpdate ();
        for(integer i=0;i<60;i=i+Integer.valueOf(Label.NeuStarListenerSchedularInterval)){
            string cron1 = '0 '+ i + ' * * * ?';
            string schName1 = 'NeuStarListener Update Batch start--'+i+'---Min--';
            if(!Test.isRunningTest())  system.schedule(schName1, cron1, batchCls);
        }
    }
}
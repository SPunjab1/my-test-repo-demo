/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : WorkOrderTaskDependencyTriggerHandler
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 10.06.2019
*  @Description      : Test class for WorkOrderTaskDependencyTriggerHandler
**********************************************************************************************************************
**********************************************************************************************************************/
@IsTest(SeeAllData=false)
Public class WoTaskDependencyTriggerHandlerTest{
    
/*************************************************************
*@description : Test Method for wtdParentTaskupdate method
*************************************************************/   
    static testmethod void runTestwtdParentTaskupdate() {  
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();   
        String WorkflowTmpName='AAV CIR Retest';
        Integer numOfRec=1;
        List<Case> caseList= TestDataFactory.createCase(WorkflowTmpName,numOfRec,recTypeId);
        test.startTest();
        insert caseList;
        case task = new case();
        task.parentId=caseList[0].id;
        task.task_number__c=1;
        insert task;
        Work_Order_Task_Dependency__c workOrderTaskdependency = new Work_Order_Task_Dependency__c();        
        workOrderTaskdependency.Work_Order__c=caseList[0].id;       
        insert workOrderTaskdependency;
        update workOrderTaskdependency;
        delete workOrderTaskdependency;
        undelete workOrderTaskdependency;
        test.stopTest();
    } 
}
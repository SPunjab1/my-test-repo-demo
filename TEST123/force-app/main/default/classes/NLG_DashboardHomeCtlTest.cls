@istest
public class NLG_DashboardHomeCtlTest {
    
    @istest
    static void testNlgReport(){
        User regionUser = TestDataFactory.createUser();
        regionUser.Role_NLG__c ='Regional POC (NLG)';
        regionUser.Market_NLG__c ='HOUSTON TX';
        regionUser.Region_NLG__c ='SOUTH';
        regionUser.MLA_NLG__c ='Crown Castle;ATC';
        insert regionUser;
        System.runAs(regionUser){
            NLG_DashboardHomeCtl dashboardCtl = new NLG_DashboardHomeCtl();
            NLG_DashboardHomeCtl.getReports();
        }
    }
    
    @istest
    static void testNlgReportNationTeam(){
        User regionUser = TestDataFactory.createUser();
        regionUser.Role_NLG__c ='Regional POC (NLG)';
        regionUser.Market_NLG__c ='HOUSTON TX';
        regionUser.Region_NLG__c ='SOUTH';
        regionUser.MLA_NLG__c ='Crown Castle;ATC';
        regionUser.NLG_National_Team__c ='NLRP/LO';
        insert regionUser;
        System.runAs(regionUser){
            NLG_DashboardHomeCtl dashboardCtl = new NLG_DashboardHomeCtl();
            NLG_DashboardHomeCtl.getReports();
        }
    }

}
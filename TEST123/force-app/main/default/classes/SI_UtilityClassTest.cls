/******************************************************************************
@author      : IBM
@date        : 03.06.2020
@description : Test class for SI_UtilityClass apex class
@Version     : 1.0
*******************************************************************************/
@isTest
public class SI_UtilityClassTest {
    
    /****************************************************************************
    * @description : To test non dml method of utility class
    *****************************************************************************/
    @isTest
    private static void testNonDMLMethods() {
        Test.startTest();
        String profileName = Si_UtilityClass.getProfileInfo(UserInfo.getProfileId());
        Map<String, Id> mapOfReordTypeNameToId= Si_UtilityClass.getAllRecordTypeMap('Case');
        Id recordTypeIdByName = Si_UtilityClass.getRecordTypeIdByName('Case', 'Voice');
        Id recordTypeIdByDeveloperName = Si_UtilityClass.getRecordTypeIdByDeveloperName('Case', 'Retail');
        SI_UtilityClass.setDmlOptionForAssignmentRules();
        Test.stopTest();
        
        System.assertEquals(true, String.isNotBlank(profileName), 'Profile Name should not be blank');
        System.assertEquals(false, mapOfReordTypeNameToId.isEmpty(), 'Map size should be greater than zero');
        System.assertEquals(true, recordTypeIdByName != null, 'Record Type Id should not be blank');
        System.assertEquals(true, recordTypeIdByDeveloperName != null, 'Record Type Id should not be blank');
    }
    
    /****************************************************************************
    * @description : To test dml method of utility class
    *****************************************************************************/
    @isTest
    private static void testDMLMethods() {
        Id recordTypeIdByName = Si_UtilityClass.getRecordTypeIdByName('Case', 'Voice');
        List<Case> listOfCases = TestDataFactory.createCaseSingleIntake(10,recordTypeIdByName);
        List<Case> listOfNewCases = TestDataFactory.createCaseSingleIntake(10,recordTypeIdByName);
        List<Case> listOfNewCasesForAssignment = TestDataFactory.createCaseSingleIntake(10,recordTypeIdByName);
        Test.startTest();
        //insert test on list of records
        List<sObject> listOfsObjects = SI_UtilityClass.callDMLOperations(listOfCases,'insert');
        //update test on list of records
        SI_UtilityClass.callDMLOperations(listOfsObjects,'update');
        //upsert test on list of records
        SI_UtilityClass.callDMLOperations(listOfsObjects,'upsert');
        //delete test on list of records
        SI_UtilityClass.callDMLOperations(listOfsObjects,'delete');
        //undelete test on list of records
        SI_UtilityClass.callDMLOperations(listOfsObjects,'undelete');
        //exception test
        SI_UtilityClass.callDMLOperations(null,'update');
        //insert test on single record
        sObject sObjectRec = SI_UtilityClass.callDMLOperationsSingleRecord(new account(Name='Acc1'),'insert');
        //update test on single record
        SI_UtilityClass.callDMLOperationsSingleRecord(sObjectRec,'update');
        //upsert test on single record
        SI_UtilityClass.callDMLOperationsSingleRecord(sObjectRec,'upsert');
        //delete test on single record
        SI_UtilityClass.callDMLOperationsSingleRecord(sObjectRec,'delete');
        //undelete test on single record
        SI_UtilityClass.callDMLOperationsSingleRecord(sObjectRec,'undelete');
        //exception test
        SI_UtilityClass.callDMLOperationsSingleRecord(null,'update');
        //database.insert test on list of records
        List<Database.SaveResult> listOfSaveResults = 
            SI_UtilityClass.callDMLDatabaseInsUpdOperations(listOfNewCases,'insert');
        //database.update test on list of records
        SI_UtilityClass.callDMLDatabaseInsUpdOperations(listOfNewCases,'update');
        //exception test
        SI_UtilityClass.callDMLDatabaseInsUpdOperations(null,'update');
        //database.upsert test on list of records
		SI_UtilityClass.callDMLDatabaseOperations(listOfNewCases,'upsert');
        //exception test
        SI_UtilityClass.callDMLDatabaseOperations(null,'upsert');
        //database.insert test on list of records
        List<Database.SaveResult> listOfSaveResults1 = 
            SI_UtilityClass.callDMLDatabaseAssignmentRules(listOfNewCasesForAssignment,'update');
        //exception test
        SI_UtilityClass.callDMLDatabaseAssignmentRules(null,'update');
        Test.stopTest();
        
        System.assertEquals(false, listOfsObjects.isEmpty(), 'Record Insertion Failed');
        System.assertEquals(true, sObjectRec != null , 'Record Insertion Failed');
        System.assertEquals(false, listOfSaveResults.isEmpty(), 'Database Insertion Operation Failed');
        System.assertEquals(false, listOfSaveResults1.isEmpty(), 'Database Insertion Operation Failed');
    }
}
/*********************************************************************************************
@author     : Matthew Elias
@date       : 02.09.2020
@description: This contains invocable method called by Submit Savings Tracker for Approval Flow. Filters Notes out
@Version    : 1.0
**********************************************************************************************/
public class ContentDocumentINClauseActionClass {
	@InvocableMethod(label='Get Content Documents with IN clause filter')
    public static FlowOutputParameters[] filterRecords(FlowInputParameters[] input){
        FlowOutputParameters[] result = new FlowOutputParameters[]{};
        
        /*Convert the filter value string list to a string of comma separated values*/
        string filterValuesList='(';
        for(integer i=0 ; i < input[0].filteringValuesCollection.size();i++){
            string filterValue = input[0].filteringValuesCollection[i];
            filterValuesList += '\''+filterValue+'\'';
            if(i != input[0].filteringValuesCollection.size()-1)
                filterValuesList += ',';
        }
        filterValuesList += ')';
        
        string query = 'Select Id, Title, FileType FROM ContentDocument WHERE FileType != \'SNOTE\' AND Id IN ' + filterValuesList;
        //system.debug(query);
        try{
            sObject[] recordList = database.query(query);
            
            FlowOutputParameters obj = new FlowOutputParameters();
            obj.records = recordList;
            
            result.add(obj);
        }catch(Exception e){
            //system.debug(e.getMessage());
            throw e;
        }
        return result;
    }
    
    /* Input parameters for the Apex action */
    public class FlowInputParameters{
        @InvocableVariable(label='Text type collection of filter values' required = true)
        public string[] filteringValuesCollection;
    }
    
    /* Output parameters of the Apex action */
    public class FlowOutputParameters{
        
        public FlowOutputParameters(){
            records = new sObject[]{};
        }
        
        @InvocableVariable(label='Record Collection')
        public sObject[] records;
    }
}
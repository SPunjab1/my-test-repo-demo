public class APIHandler {
    
    // Authorization callout
    public static String getApigeeToken() {
        String AccessToken;
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('callout:Apigee_Token_Url');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Length', '0');
        HttpResponse response;
        response = new Http().send(request);
        if(Test.isRunningTest()){

        }else{
        TokenWrapper authorization = (TokenWrapper) JSON.deserialize(response.getBody(), TokenWrapper.class);
        //authorization.expireTime = Datetime.now().getTime() + Long.valueOf(authorization.expires_in);
		AccessToken = authorization.access_token;
        }
        return AccessToken;
    }
    
    
    public class TokenWrapper {
        public String id_token;
        public String access_token;
        public String expires_in;
        public String token_type;
        public String resource;
        public String refresh_token;
        public String scope;
        public Long expireTime;
    }
    
    public static Map<String,String> getPIERParticipants(Boolean checkCR) {
        Map<String,String> PIERParticipants = New Map<String,String>();
         for(PIER_Participant__mdt mdata:[select id,Group_Name__c,NTID__c,Manager_NTIDS__c from PIER_Participant__mdt])
            {
                if(checkCR == true){
                    PIERParticipants.put(mdata.Group_Name__c,mdata.Manager_NTIDS__c);
                }
                else{
				PIERParticipants.put(mdata.Group_Name__c,mdata.NTID__c);
                }
            }
        return PIERParticipants;
    }

}
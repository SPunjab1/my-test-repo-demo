public with sharing class Result{
		
	@AuraEnabled
	public Object Data {get;set;}
	
	@AuraEnabled
	public Boolean bSuccess{get;set;}
	
	@AuraEnabled
	public String ErrMsg{get;set;}
    
    @AuraEnabled
	public String ErrType{get;set;}
	
	public Result(){
	}
	
	public Result(Boolean bSuccess){
		this.bSuccess = bSuccess;
	}
	
	public Result(String ErrorMessgae){
		this.bSuccess = false;
		this.ErrMsg = ErrorMessgae;
	}
    
    public Result(String ErrorMessgae, String ErrorType){
		this.bSuccess = false;
		this.ErrMsg = ErrorMessgae;
        this.ErrType = ErrorType;
	}
	
	public Result(Object Data, Boolean bSuccess){
		this.bSuccess = bSuccess;
		if(bSuccess)
			this.Data = Data;
		else
			this.ErrMsg = String.ValueOf(Data);
	}
	
	public Result(Exception ex){
		this.bSuccess = false;
		this.ErrMsg = ex.getMessage();
        this.ErrType = 'Fatal Error';
	}
}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ChatterAddFilesToRecordsControllerTest
*  @Author           : Kirti
*  @Version History  : 1.0
*  @Creation         : 07.06.2019
*  @Description      : Test class for ChatterAddFilesToRecordsController
**********************************************************************************************************************
****************************************************************************************************************/
@isTest
private class ChatterAddFilesToRecordsControllerTest {
    
/***************************************************************************************************
* @Description  : Test Method to test getRecentlyViewedFiles 
***************************************************************************************************/ 
    @isTest
    static void test_get_recently_viewed_files() {
        ContentVersion cv = new ContentVersion(
            versionData = Blob.valueOf( 'Hello World' ),
            title = 'test',
            pathOnClient = 'test.txt'
        );
        insert cv;
        ContentDocument cd = [ SELECT id FROM ContentDocument WHERE latestPublishedVersionId = :cv.id FOR VIEW ];
        Case c = new Case(
            Status='New',Origin='Web'
        );
        insert c;
        Test.startTest();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController([ SELECT id, status FROM Case ]);
        stdSetController.setSelected( stdSetController.getRecords() );
        ChatterAddFilesToRecordsController controller = new ChatterAddFilesToRecordsController( stdSetController );
        List<ID> recordIds = controller.recordIds;
        ChatterAddFilesToRecordsController.SObjectDescribeWrapper describeWrapper = ChatterAddFilesToRecordsController.getSObjectDescribe( recordIds );
        ChatterAddFilesToRecordsController.PaginatedContentDocumentWrapper filesWrapper = ChatterAddFilesToRecordsController.getRecentlyViewedFiles( 1, 10 );
        Test.stopTest();
        System.assertEquals( 1, recordIds.size() );
        System.assertEquals( Case.sObjectType.getDescribe().getName(), describeWrapper.name );
        System.assertEquals( Case.sObjectType.getDescribe().getLabel(), describeWrapper.label );
        System.assertEquals( Case.sObjectType.getDescribe().getLabelPlural(), describeWrapper.labelPlural );
        System.assertEquals( Case.sObjectType.getDescribe().getKeyPrefix(), describeWrapper.keyPrefix );
   }
    
/***************************************************************************************************
* @Description  : Test Method to test shared with me files 
***************************************************************************************************/
    @isTest
    static void test_get_shared_with_me_files() {
        ContentVersion cv1 = new ContentVersion(
            versionData = Blob.valueOf( 'Hello World' ),
            title = 'test',
            pathOnClient = 'test.txt'
        );
        insert cv1;
        ContentVersion cv2 = new ContentVersion(
            versionData = Blob.valueOf( 'Goodnight Moon' ),
            title = 'quiz',
            pathOnClient = 'quiz.txt'
        );
        insert cv2;
        ContentDocument cd1 = [ SELECT id, title FROM ContentDocument WHERE latestPublishedVersionId = :cv1.id FOR VIEW ];
        ContentDocument cd2 = [ SELECT id, title FROM ContentDocument WHERE latestPublishedVersionId = :cv2.id FOR VIEW ];
        Case c = new Case(
            Status='New',Origin='Web'
        );
        insert c;
        // Note, our controller's SOSL for searching files queries on ContentDocument object
        // but specifying ContentDocument IDs as fixed search results will yield zero SOSL results.
        // If we specify the latest ContentVersion ID as the fixed search results then our SOSL
        // search for ContentDocument object will return expected results. Wha??
        Test.setFixedSearchResults( new List<ID>{ cv1.id } );
        Test.startTest();
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController([ SELECT id, status FROM Case ]);
        stdSetController.setSelected( stdSetController.getRecords() );
        ChatterAddFilesToRecordsController controller = new ChatterAddFilesToRecordsController( stdSetController );
        ChatterAddFilesToRecordsController.PaginatedContentDocumentWrapper filesWrapper = ChatterAddFilesToRecordsController.searchAllFiles( null, 1, 1 );
        filesWrapper = ChatterAddFilesToRecordsController.searchAllFiles( 'test', 1, 1 );
        Test.stopTest();
        System.assertEquals( 1, filesWrapper.files.size() );
        System.assertEquals( cd1.id, filesWrapper.files[0].file.id );
        System.assertEquals( false, filesWrapper.files[0].selected );
    }
    
/***************************************************************************************************
* @Description  : Test Method to test attachFilesToRecords 
***************************************************************************************************/
    @isTest
    static void test_attach_files_to_records() {
        ContentVersion cv = new ContentVersion(
            versionData = Blob.valueOf( 'Hello World' ),
            title = 'test',
            pathOnClient = 'test.txt'
        );
        insert cv;
        ContentDocument cd = [ SELECT id FROM ContentDocument WHERE latestPublishedVersionId = :cv.id FOR VIEW ];
        Case c = new Case(
            Status='New',Origin='Web'
        );
        List<case>  newCase = new List<Case>();
        Integer numOfRec=1;
         Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        newCase = TestdataFactory.createCase('AAV CIR Retest',numOfRec,recTypeId);
        insert newCase;
        insert c;
        Test.startTest();
        List<ID> recordIds = new List<ID>{ newCase[0].id };
            List<ID> fileIds = new List<ID>{ cd.id };
                Boolean postToChatter = true;
        ChatterAddFilesToRecordsController.attachFilesToRecords( recordIds, fileIds, 'case comments');
        Test.stopTest();
        List<ContentDocumentLink> cdls = [ SELECT linkedEntityId, contentDocumentId FROM ContentDocumentLink WHERE linkedEntityId = :c.id ];
    }
    
/***************************************************************************************************
* @Description  : Test Method to test already shared files 
***************************************************************************************************/  
    @isTest
    static void test_attach_already_shared_files() {
        ContentVersion cv = new ContentVersion(
            versionData = Blob.valueOf( 'Hello World' ),
            title = 'test',
            pathOnClient = 'test.txt'
        );
        insert cv;
        ContentDocument cd = [ SELECT id FROM ContentDocument WHERE latestPublishedVersionId = :cv.id FOR VIEW ];
        Case c = new Case(
            Status='New',Origin='Web'
        );
        insert c;
        List<case>  newCase = new List<Case>();
        Integer numOfRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        newCase = TestdataFactory.createCase('AAV CIR Retest',numOfRec,recTypeId);
        insert newCase;
        ContentDocumentLink cdl = new ContentDocumentLink(
            linkedEntityId = c.id,
            contentDocumentId = cd.id,
            shareType = 'V'
        );
        insert cdl;
        Test.startTest();
        List<ID> recordIds = new List<ID>{ newCase[0].id };
            List<ID> fileIds = new List<ID>{ cd.id };
                Boolean postToChatter = false;
        ChatterAddFilesToRecordsController.attachFilesToRecords( recordIds, fileIds, 'case comments');
        Test.stopTest();
        // only the one link should come back since controller won't try to insert duplicate shares
        List<ContentDocumentLink> cdls = [ SELECT linkedEntityId, contentDocumentId FROM ContentDocumentLink WHERE linkedEntityId = :c.id ];
    }
    
/***************************************************************************************************
* @Description  : Test Method to test for no record ids. 
***************************************************************************************************/
    @isTest
    static void test_no_record_ids() {
        ContentVersion cv = new ContentVersion(
            versionData = Blob.valueOf( 'Hello World' ),
            title = 'test',
            pathOnClient = 'test.txt'
        );
        insert cv;
        ContentDocument cd = [ SELECT id FROM ContentDocument WHERE latestPublishedVersionId = :cv.id FOR VIEW ];
        Test.startTest();
        ChatterAddFilesToRecordsController controller = new ChatterAddFilesToRecordsController( new ApexPages.StandardSetController([ SELECT id, status FROM Case ]) );
        ChatterAddFilesToRecordsController.SObjectDescribeWrapper describeWrapper = ChatterAddFilesToRecordsController.getSObjectDescribe( controller.recordIds );
        Test.stopTest();
        System.assertEquals( 0, controller.recordIds.size() );
        System.assert( ApexPages.hasMessages( ApexPages.Severity.ERROR ) ); // for not having record ids
        System.assertEquals( null, describeWrapper ); // without record ids, nothing to describe
    }
}
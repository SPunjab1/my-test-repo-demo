/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ShiftTaskOrderHandlerTest
*  @Author           : Archana
*  @Version History  : 1.0
*  @Creation         : 07.06.2019
*  @Description      : Test class for ShiftTaskOrderHandler
**********************************************************************************************************************
**********************************************************************************************************************/
@IsTest(SeeAllData=false)
Public class ShiftTaskOrderHandlerTest{
    
/*************************************************************
*@description : Test Method for updateShiftTaskOrder scenario
*************************************************************/   
    static  testmethod void runTestupdateShiftTaskOrder() {       
       System.runAs(TestDataFactory.createUser()){     
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> casesWithShiftTaskList= new List<Case>();
        List<String> idList=new List<String>();
        String WorkflowTmpName='TIFS AAV RIP_Secondary Vendor';
        Integer NumOfRec=1;
        casesWithShiftTaskList= TestDataFactory.createCase(WorkflowTmpName,NumOfRec,recTypeId );
        casesWithShiftTaskList[0].RecordTypeId=recTypeId ;
        Test.startTest();
        Insert(casesWithShiftTaskList); 
        idList.add(casesWithShiftTaskList[0].id);
        ShiftTaskOrderHandler.updateShiftTaskOrder(idList,false);
        Test.stopTest();
        Case cObj = [SELECT Id, Task_Number__c, Shift_Task_Order__c FROM Case WHERE Id = :casesWithShiftTaskList[0].id];
        System.assertEquals(cObj.Task_Number__c, cObj.Shift_Task_Order__c); 
      } 
    }
    
/*************************************************************
*@description : Test Method for enableTaskShiftOrder scenario
*************************************************************/  
    static  testmethod void runTestenableTaskShiftOrder() {  
      System.runAs(TestDataFactory.createUser()){          
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> casesWithShiftTaskList= new List<Case>();
        String WorkflowTmpName='TIFS AAV RIP_Secondary Vendor';
        Integer NumOfRec=1;
        casesWithShiftTaskList= TestDataFactory.createCase(WorkflowTmpName,NumOfRec,recTypeId);       
        Test.startTest();
        Insert(casesWithShiftTaskList); 
        ShiftTaskOrderHandler.enableTaskShiftOrder(casesWithShiftTaskList[0].id);
        Test.stopTest();
        System.assertequals(ShiftTaskOrderHandler.enableTaskShiftOrder(casesWithShiftTaskList[0].id),FALSE);
      }  
    }
/*************************************************************
*@description : Test Method for disable task shift order scenario
*************************************************************/  
    static  testmethod void runTestupdateShiftTaskOrderNegative() {    
       System.runAs(TestDataFactory.createUser()){         
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();     
        List<Case> casesList= new List<Case>();     
        String WorkflowTmpName='TIFS AAV CIR Retest'; 
        Integer NumOfRec=1;       
        casesList= TestDataFactory.createCase(WorkflowTmpName,NumOfRec,recTypeId);      
        casesList[0].RecordTypeId=recTypeId ;  
        Test.startTest();    
        Insert(casesList[0]);            
        ShiftTaskOrderHandler.enableTaskShiftOrder(casesList[0].id);
        Test.stopTest();
      }  
    }   
}
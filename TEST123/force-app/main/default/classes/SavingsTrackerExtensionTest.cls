@isTest
public class SavingsTrackerExtensionTest {
    private static testMethod void prepareFieldOptionsTest(){
        Map<String,Map<String,String>> response = SavingsTrackerExtension.prepareFieldOptions();
        system.assertEquals(5,response.size(),'Fields count did not match for preparing picklist options');
        system.assertEquals(true,response.keySet().contains('Savings_Status__c'),'Savings_Status__c not included for preparing picklist options');
        system.assertEquals(true,response.keySet().contains('Type_of_Baseline_for_this_deal__c'),'Type_of_Baseline_for_this_deal__c not included for preparing picklist options');
        system.assertEquals(true,response.keySet().contains('Step2_Old_Final_Payment_Terms__c'),'Step2_Old_Final_Payment_Terms__c not included for preparing picklist options');
        system.assertEquals(true,response.keySet().contains('Step2_New_Final_Payment_Terms__c'),'Step2_New_Final_Payment_Terms__c not included for preparing picklist options');
        system.assertEquals(true,response.keySet().contains('Step3_Forecasted_Volume_Source__c'),'Step3_Forecasted_Volume_Source__c not included for preparing picklist options');
    }
    
    private static testMethod void fetchRequestRecordTest(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
		testRequest.Allocated_Budget__c = 100;
        insert testRequest;
        
        Request__c response = SavingsTrackerExtension.fetchRequestRecord(testRequest.Id);
        system.assertEquals(testRequest.Id,response.Id,'Request__c record did not match');
    }
    
    private static testMethod void fetchRequestRecordTest2(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
        testRequest.Allocated_Budget__c = 100;
        insert testRequest;
        
        Savings_Tracker__c testSavingsTracker = new Savings_Tracker__c();
        testSavingsTracker.Request__c = testRequest.Id;
        testSavingsTracker.Type_of_Baseline_for_this_deal__c = 'Historical Cost';
        insert testSavingsTracker;
        
        Request__c response = SavingsTrackerExtension.fetchRequestRecord(testSavingsTracker.Id);
        system.assertEquals(testRequest.Id,response.Id,'Request__c record did not match');
    }
    
    private static testMethod void fetchSavingsTrackerTest(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
        testRequest.Allocated_Budget__c = 100;
        insert testRequest;
        
        Savings_Tracker__c testSavingsTracker = new Savings_Tracker__c();
        testSavingsTracker.Request__c = testRequest.Id;
        testSavingsTracker.Type_of_Baseline_for_this_deal__c = 'Historical Cost';
        insert testSavingsTracker;
        
        Savings_Tracker__c response = SavingsTrackerExtension.fetchSavingsTracker(testRequest.Id);
        system.assertEquals(testSavingsTracker.Id,response.Id,'Savings_Tracker__c record did not match');
    }
    
    private static testMethod void fetchSavingsTrackerTest2(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
        testRequest.Allocated_Budget__c = 100;
        insert testRequest;
        
        Savings_Tracker__c testSavingsTracker = new Savings_Tracker__c();
        testSavingsTracker.Request__c = testRequest.Id;
        testSavingsTracker.Type_of_Baseline_for_this_deal__c = 'Historical Cost';
        insert testSavingsTracker;
        
        Savings_Tracker__c response = SavingsTrackerExtension.fetchSavingsTracker(testSavingsTracker.Id);
        system.assertEquals(testSavingsTracker.Id,response.Id,'Savings_Tracker__c record did not match');
    }
    
    private static testMethod void fetchSavingsTrackerTest3(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
        testRequest.Allocated_Budget__c = 100;
        insert testRequest;
        
        Savings_Tracker__c response = SavingsTrackerExtension.fetchSavingsTracker('');
        system.assertEquals(null,response.Id,'Savings_Tracker__c record did not match');
    }
    
    private static testMethod void saveSavingsTrackerTest(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
        testRequest.Allocated_Budget__c = 100;
        insert testRequest;
        
        Savings_Tracker__c testSavingsTracker = new Savings_Tracker__c();
        testSavingsTracker.Request__c = testRequest.Id;
        testSavingsTracker.Type_of_Baseline_for_this_deal__c = 'Historical Cost';
        
        Savings_Tracker__c response = SavingsTrackerExtension.saveSavingsTracker(testSavingsTracker);
        system.assertEquals(testSavingsTracker.Request__c,response.Request__c,'Savings_Tracker__c record did not match');
        
        response = SavingsTrackerExtension.saveSavingsTracker(response);
        system.assertEquals(testSavingsTracker.Request__c,response.Request__c,'Savings_Tracker__c record did not match');
    }
    
    private static testMethod void fetchLoggedInUserTest(){
        User response = SavingsTrackerExtension.fetchLoggedInUser();
        system.assertEquals(UserInfo.getUserId(),response.Id,'User record did not match');
    }
    
    /*
    private static testMethod void countAttachmentsTest(){
        Project_Owner__c testProjectOwner = new Project_Owner__c();
        testProjectOwner.Name = 'Test Project Owner';
        testProjectOwner.Senior_Manager__c = UserInfo.getUserId();
        testProjectOwner.Title__c = 'Test Title';
        insert testProjectOwner;
        
        Request__c testRequest = new Request__c();
        testRequest.Procurement_Group__c = 'Technology & Indirect Procurement';
        testRequest.What_is_being_sourced_or_contracted__c = 'Services';
        testRequest.Services__c = 'IT Services';
        testRequest.Sourcing_Project_Type__c = 'MSA';
        testRequest.VP_Project_Owner__c = UserInfo.getUserId();
        testRequest.VP_Project_Owner__c = testProjectOwner.Id;
        insert testRequest;
        
        Integer testCount = SavingsTrackerExtension.countRequestAttachments(testRequest.Id);
        System.assertEquals(0, testCount);
    }
	*/
}
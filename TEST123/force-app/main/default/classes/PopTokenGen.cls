public class PopTokenGen {
    public static final Integer POP_TOKEN_VALIDITY_DURATION_IN_SECS = 3600;
    public String CERT_NAME = 'Heroku_request';
    public Map<String, String> ehtsMap;
    
    public DateTime getIssuedAt(){
        return DateTime.now();
    }
    
    public String getVersion() {
        return '1';
    }
    
    public String getUniqueIdentifier(){
        return GUIDUtility.NewGUID();
    }
    
    public String buildEhtsString(Map<String, String> ehtsKeyValueMap){
        List<String> strs = new List<String>();
        for(String val : ehtsKeyValueMap.keySet()){
            strs.add(val);
        }
        return String.join(strs, ';');
    }
    
    public String calculateEdtsSha256Base64Hash(Map<String, String> ehtsKeyValueMap){
        
        List<String> strs = new List<String>();
        for(String o : ehtsKeyValueMap.values()){
            strs.add(o);
        }
        String concatVal = String.join(strs, '');
        
        Blob b = Blob.valueOf(concatVal);
        Blob digest = Crypto.generateDigest('SHA-256', b);
        String res = EncodingUtil.base64Encode(digest);
        res = res.replaceAll('[+]', '-');
        res = res.replaceAll('[/]', '_');
        res = res.replaceAll('[=]', '');
        return res;
    }
    
    public String build(Map<String, String> ehtsKeys, String mykey){
        DateTime issuedAt = getIssuedAt();
        
        Map<String, String> claims = new Map<String, String>{
            'ehts'=> buildEhtsString(ehtsKeys),
                'edts'=> calculateEdtsSha256Base64Hash(ehtsKeys),
                'jti'=> getUniqueIdentifier(),
                'v'=> getVersion()
                };
                    Auth.JWT jw = new Auth.JWT();
        jw.setAdditionalClaims(claims);
        jw.setValidityLength(POP_TOKEN_VALIDITY_DURATION_IN_SECS);
        Auth.JWS sig = new Auth.JWS(jw, mykey);
        String res = sig.getCompactSerialization();
        return res;
    }
    
    public void buildAuthKey(HttpRequest request, String mykey)
    {
        ehtsMap = new Map<String, String>();
        if(request.getHeader('Content-Type') != null) {
            ehtsMap.put('Content-Type', request.getHeader('Content-Type'));
        }
        if(request.getHeader('Authorization') != null) {
            ehtsMap.put('Authorization', request.getHeader('Authorization'));                
        }
        if(!String.isBlank(request.getEndpoint())){
            URL endpointURL = new URL(request.getEndpoint());
            String hostName = endpointURL.getHost();
            String uri = request.getEndpoint().substringAfter(hostName);
            //uri = uri.replaceAll(' ', '');
            ehtsMap.put('uri',uri);
        }
        if(request.getMethod() != null) {
            ehtsMap.put('http-method', request.getMethod());
        }
        if(!String.isBlank(request.getBody())){
            ehtsMap.put('body', request.getBody());
        }
        system.debug('ehtsMap>>'+ehtsMap);
        String key =  build(ehtsMap, mykey);
        system.debug('Pop Token:' +key);
        request.setHeader('X-Authorization', key);
        
    }
    public void buildAuthKey(HttpRequest request)
    {
        buildAuthKey(request, CERT_NAME);
    }
}
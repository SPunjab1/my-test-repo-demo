public class NlgIntakeReportController {
    
    @AuraEnabled
    public static string GetSiteID(String OpportunityId)
    {
       return [select id,Site_ID__r.Name  from Opportunity where id = :OpportunityId ].get(0).Site_ID__r.Name;
    }
}
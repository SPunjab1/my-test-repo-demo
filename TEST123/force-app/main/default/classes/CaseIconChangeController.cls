/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : CaseIconChangeController
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 02.06.2019
*  @Description      : Apex class of CaseIconChange lightning component
**********************************************************************************************************************
**********************************************************************************************************************/
public class CaseIconChangeController {
/***************************************************************************************
* @Description :Method to getIcon based on record type
* @input params:caseId
***************************************************************************************/ 
    @AuraEnabled
    public  static string getIcon(Id caseId)
    {
        ID workOrderRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        String source ='';
        if(caseId!=null){
             List<Case> caseRec = [select Id,RecordTypeId,CaseNumber from Case where Id =: caseId];
             if(caseRec != null && !caseRec.isEmpty()){
                source = caseId;
                if(caseRec[0].RecordTypeId == workOrderRecordType){
                    source='new_case-'+ 'WorkOrder';
                }
                if(caseRec[0].RecordTypeId == workOrderTaskRecordType){
                    source='new_task-'+ 'WorkOrderTask';
                } 
             }      
        }
      
        return source;
    }
}
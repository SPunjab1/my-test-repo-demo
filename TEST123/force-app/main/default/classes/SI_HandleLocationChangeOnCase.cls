/******************************************************************************
@author      : IBM
@date        : 04.09.2020
@description : Class to delete old intake site on case when location updates.
@Version     : 1.0
*******************************************************************************/
public class SI_HandleLocationChangeOnCase {
    
    /**************************************************************************
    * @description Invocable method to be called from Case process builder
    * @param listOfCaseIds - List<Id>
    **************************************************************************/
    @InvocableMethod
    public static void deleteOldIntakeSiteOnLocationChange(List<Id> listOfCaseIds) {
        //check if listOfCaseIds is not blank
        if(listOfCaseIds != null && !listOfCaseIds.isEmpty()) {
            //Intialize list to hold intake Sites
            List<Intake_Site__c> listOfIntakeSitesToDelete = new List<Intake_Site__c>();
            //fetch intake sites related to case
            for(Intake_Site__c intakeSite : [SELECT Id, Location_Id__c,
                                             Case__r.Location_Ids__c
                                             FROM Intake_Site__c
                                             WHERE Case__c IN :listOfCaseIds]) {
                if(intakeSite.Case__r.Location_Ids__c != intakeSite.Location_Id__c) {
                    listOfIntakeSitesToDelete.add(intakeSite);
                }
            }
            if(!listOfIntakeSitesToDelete.isEmpty()) {
                try {
                    //delete old intake sites
                    delete listOfIntakeSitesToDelete;
                } catch (DMLException ex) {
                    //Log exceptions
                    System.debug(LoggingLevel.INFO, 'Exception: ' + ex.getMessage()); 
                }
            }
        }
    }
}
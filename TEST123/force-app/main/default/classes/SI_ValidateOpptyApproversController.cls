/***********************************************************************************
@author      : IBM
@date        : 15.06.2020
@description : Class to validate approvers on opportunity record on button click.
@Version     : 1.0
************************************************************************************/
public with sharing class SI_ValidateOpptyApproversController {
    
    private Opportunity opportunity;
    private ApexPages.StandardController sctrl;
    
    /**
    * @description SI_ValidateOpptyApproversController constructor.
    * @param stdController - apex page standard controller
    */ 
    public SI_ValidateOpptyApproversController(ApexPages.StandardController stdController){
        this.sctrl = stdController;          
    }
    
    /**
    * @description Fetch opportunity approver data based on opportunity Id.
    * @param recordId - to hold opportunity record id and fetch opportunity approver data
    * @return Opportunity 
    */ 
    public static Opportunity getOpportunityRecord(Id recordId) {
        return [
            SELECT Approver_1__c, Approver_1__r.Email , Approver_1__r.Name,
            Approver_2__c, Approver_2__r.Email, Approver_2__r.Name,
            Approver_3__c, Approver_3__r.Email, Approver_3__r.Name,
            Approver_4__c, Approver_4__r.Email, Approver_4__r.Name,
            Approver_5__c, Approver_5__r.Email, Approver_5__r.Name,
            Approver_6__c, Approver_6__r.Email, Approver_6__r.Name,
            Approver_7__c, Approver_7__r.Email, Approver_7__r.Name,
            Approver_8__c, Approver_8__r.Email, Approver_8__r.Name
            FROM Opportunity
            WHERE Id = :recordId WITH SECURITY_ENFORCED
        ];        
    }
    
    /**
    * @description Validate approvers on Opportunity record and redirect to docusign
    * @return PageReference 
    */
    public PageReference validateOpportunityAndRedirect() {
        //Intialize page reference variable
        Pagereference pgDocusign;
        //set opportunity record with current page opportunity record id
        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('id'))) {
            opportunity = getOpportunityRecord(ApexPages.currentPage().getParameters().get('id').escapeHtml4());
        }
        if(opportunity != Null){
            String errorMsg = '';
            string docusignAdditionalApprovers = '' ;
            //Validate users are added to all 3 Approver fields
            if(Opportunity.Approver_1__c == Null){
                errorMsg += 'Approver 1<br/>';
            }
            if(Opportunity.Approver_2__c == Null){
                errorMsg += 'Approver 2<br/>';
            }
            if(Opportunity.Approver_3__c == Null){
                errorMsg += 'Approver 3<br/>';
            }           
            //Append optional approvers 5,6,7,8 to docusign url 
            if(Opportunity.Approver_4__c != Null){
                docusignAdditionalApprovers += ',Email~'+ Opportunity.Approver_4__r.Email +';FirstName~'+ Opportunity.Approver_4__r.Name+';Role~Signer 4';
            }
            if(Opportunity.Approver_5__c != Null){
                if(Opportunity.Approver_4__c == Null){
                    errorMsg += 'Approver 4<br/>';
                }
                docusignAdditionalApprovers += ',Email~'+ Opportunity.Approver_5__r.Email +';FirstName~'+ Opportunity.Approver_5__r.Name+';Role~Signer 5';
            }
            if(Opportunity.Approver_6__c != Null){
                if(Opportunity.Approver_5__c == Null){
                    errorMsg += 'Approver 5<br/>';
                }
                docusignAdditionalApprovers += ',Email~'+ Opportunity.Approver_6__r.Email +';FirstName~'+ Opportunity.Approver_6__r.Name+';Role~Signer 6';
            }        
            if(Opportunity.Approver_7__c != Null){
                if(Opportunity.Approver_6__c == Null){
                    errorMsg += 'Approver 6<br/>';
                }
                docusignAdditionalApprovers += ',Email~'+ Opportunity.Approver_7__r.Email +';FirstName~'+ Opportunity.Approver_7__r.Name+';Role~Signer 7';
            }        
            if(Opportunity.Approver_8__c != Null){
                if(Opportunity.Approver_7__c == Null){
                    errorMsg += 'Approver 7';
                }
                docusignAdditionalApprovers += ',Email~'+ Opportunity.Approver_8__r.Email +';FirstName~'+ Opportunity.Approver_8__r.Name+';Role~Signer 8';
            }
            //Display error message incase of missing approvers
            if(errorMsg != ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.SI_OpportunityApproverError + errorMsg));
            }        
            try {
                //Set up docusign redirection url
                pgDocusign = Page.dsfs__DocuSign_CreateEnvelope;
                pgDocusign.getParameters().put('SourceID',opportunity.Id);
                //Add signers
                pgDocusign.getParameters().put('CRL','Email~' + opportunity.Approver_1__r.Email + ';FirstName~' + opportunity.Approver_1__r.Name+
                                               ';Role~Signer 1,Email~' + Opportunity.Approver_2__r.Email + ';FirstName~' +
                                               Opportunity.Approver_2__r.Name+';Role~Signer 2,Email~' + Opportunity.Approver_3__r.Email + 
                                               ';FirstName~' + Opportunity.Approver_3__r.Name+';Role~Signer 3'  + docusignAdditionalApprovers); 
                pgDocusign.setRedirect(true);
            } catch(Exception ex) {
                System.debug(LoggingLevel.Error,'Exception: ' + ex.getMessage());
            } 
        }        
        return ApexPages.hasMessages() ? null : pgDocusign;
    }
    
    /**
    * @description Go back to opportunity record
    * @return PageReference 
    */
    public PageReference back()
    {
        PageReference cancel ;
            try {
                cancel = sctrl.cancel();
            } catch(Exception ex) {
                System.debug(LoggingLevel.Error, 'Exception: ' + ex.getMessage());
            }
        return cancel;
    }
}
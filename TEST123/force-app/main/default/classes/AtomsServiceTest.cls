@isTest
public class AtomsServiceTest {
    
    @testSetup static void setup() {
        
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        insert TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        List<Case> lstCase = TestDataFactoryOMph2.createCase( 'AAV CIR Retest', 1, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId());
        insert lstCase;
        List<Purchase_Order__c> lstPurchase_Order = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT', 'COMCAST', 'Disconnect ', 1, null);
        insert lstPurchase_Order;
        List<circuit__c> lstCircuit = TestDataFactoryOMph2.createCircuitRecords( lstCase[0].Id, 1);
        insert lstCircuit; 
        
        List<Asr__c> lstAsr = TestDataFactoryOMph2.createASRRecords( null, lstPurchase_Order[0].Id, lstCircuit[0].Id, 1);
        lstAsr[0].ATOMs_ASR_Id__c = 'ATOPMS-NHGDYA-AKDSHDH-AHJD';
        lstAsr[0].RPON__c = 'RPON';
        lstAsr[0].SUP__c = '2';
        lstAsr[0].SUP_Date__c = date.today();
        lstAsr[0].Project__c = 'Project__c';
        lstAsr[0].ASR_Number__c = 'ASR_Number__c';
        lstAsr[0].Order_Number__c = 'Order_Number__c';
        lstAsr[0].Accepted_Date__c = Date.today();
        lstAsr[0].Date_Confirmed__c = Date.today();
        lstAsr[0].FOC_Date__c = Date.today();
        lstAsr[0].Delivered_Date__c = Date.today();
        lstAsr[0].Cancelled_Date__c = date.today();
        insert lstAsr;
    }
    
    public class MockAtomsCallouts implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            String responseJSON = '', endpoint = req.getEndpoint(), method = req.getMethod();
            
            system.debug('updateasrtoatoms - endpoint - '+ endpoint);
            system.debug('updateasrtoatoms - endpoint - '+ AtomsService.ATOMS_ALL_ENDPOINTS.get('updateasrtoatoms'));
            system.debug('endpoint.startsWithIgnore-  '+ endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('updateasrtoatoms')));
            if('GET'.equalsIgnoreCase( method)){
                if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('getsiteid') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('getsiteid'))){
                    responseJSON = TestDataFactoryOMph2.mapMockResponses.get('getsiteid');
                }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('getorderingguideid') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('getorderingguideid'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('getorderingguideid');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('getsiteassociation') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('getsiteassociation'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('getsiteassociation');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('getvendorname') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('getvendorname'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('getvendorname');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('GetCircuit') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('GetCircuit'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('GetCircuit');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('getnniatoms') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('getnniatoms'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('getnni');
                    }
            }
            else{
                if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('updatecircuitapi') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('updatecircuitapi'))){
                    responseJSON = TestDataFactoryOMph2.mapMockResponses.get('updatecircuitapi');
                }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('updateasrtoatoms') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('updateasrtoatoms'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('updateasrtoatoms');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('createasrurl') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('createasrurl'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('createasrtoatoms');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('AddCircuitAPI') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('AddCircuitAPI'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('AddCircuitAPI');
                    }
                else
                    if(AtomsService.ATOMS_ALL_ENDPOINTS.containsKey('addnniatoms') && endpoint.startsWithIgnoreCase(AtomsService.ATOMS_ALL_ENDPOINTS.get('addnniatoms'))){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('addnni');
                    }
                else{
                    TMobilePh2LoginDetailsHierarchy__c atomsToken =  TMobilePh2LoginDetailsHierarchy__c.getOrgDefaults();
                    
                    if(endpoint.startsWithIgnoreCase(atomsToken.Atoms_End_Point__c)){
                        responseJSON = TestDataFactoryOMph2.mapMockResponses.get('authwithocta');
                    }
                }
            }
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(responseJSON);
            res.setStatusCode(200);
            return res;
        }
    }
    
    @isTest 
    static void AuthenticateWithOCTA() {
        
        Test.setMock(HttpCalloutMock.class, new MockAtomsCallouts());
        Test.startTest();
        Result authResult = AtomsService.AuthenticateWithOCTA();
        authResult = AtomsService.AuthenticateWithOCTA();
        Test.stopTest();
        system.assert(authResult.bSuccess, authResult.ErrMsg);
    }
    
    @isTest
    static void GetCircuitSiteDetails(){
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockAtomsCallouts());
        AtomsIntegrationAPI.GetCalloutResponseForCircuit resp = AtomsService.GetCircuitSiteDetails('BB2D4FA0-4849-4B4B-9F97-71B8F8A7EA7B', 'CH0645BA_41LAB', 'CH0645BA_41LAB', 'z');
        System.assert(resp.isSuccess, 'Get Site Failed');
        resp = AtomsService.GetCircuitSiteDetails('BB2D4FA0-4849-4B4B-9F97-71B8F8A7EA7B', 'CH0645BA_41LAB', 'CH0645BA_41LAB', 'a');
        System.assert(resp.isSuccess, 'Get Site Failed');        
        resp = AtomsService.GetCircuitSiteDetails('BB2D4FA0-4849-4B4B-9F97-71B8F8A7EA7B', 'CH0645BA_41LAB', 'CH0645BA_41LAB', null);
        System.assertEquals(false, resp.isSuccess, 'Get Site Failed');
        Result authResult = AtomsService.GetVendor('AMERITECH');
        system.assert(authResult.bSuccess, authResult.ErrMsg);
        Test.stopTest();
    }
    
    @isTest
    static void updateCircuit(){        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockAtomsCallouts());
        Result authResult = AtomsService.updateCircuit([Select Id, User_Ciruit_ID__c From Circuit__c limit 1].Id);
        system.assert(authResult.bSuccess, authResult.ErrMsg);
        Asr__c asr = [Select Id From ASR__c limit 1];
        authResult = AtomsService.updateASR(asr.Id);
        system.assert(authResult.bSuccess, authResult.ErrMsg);
        test.stopTest();
    }
    @isTest
    static void updateASR(){        
        
        Asr__c asr = [Select Id From ASR__c limit 1];        
        asr.Is_Supplement__c = true;
        update asr;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockAtomsCallouts());
        Result authResult = AtomsService.updateASR(asr.Id);
        
        test.stopTest();
        system.assert(authResult.bSuccess, authResult.ErrMsg);
    }
        @isTest
    static void updateASR1(){        
        
        Asr__c asr = [Select Id From ASR__c limit 1];        
        asr.Is_Supplement__c = true;
        asr.SUP__c = '4';
        update asr;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockAtomsCallouts());
        Result authResult = AtomsService.updateASR(asr.Id);
        
        test.stopTest();
        system.assert(authResult.bSuccess, authResult.ErrMsg);
    }
    
    @isTest
    static void remainingLines(){
        
        AtomsService.AtomsOG_IdResponse objWrap = new AtomsService.AtomsOG_IdResponse();
        objWrap.detailLink = objWrap.systemMessage = objWrap.userMessage = objWrap.details = objWrap.ContractID = objWrap.OrderingGuideID = objWrap.OGID = objWrap.OGName = objWrap.ServiceType = objWrap.ReqSvcOrd = objWrap.OGNotes = objWrap.dtUpdated = objWrap.UpdatedBy = objWrap.Status = objWrap.code ='msg';
        AtomsService.Fault flt = new AtomsService.Fault();
        flt.faultstring = 'msg';
        flt.detail = new Map<String, String>();
        objWrap.fault = flt;
        
        AtomsService.OctaAuthResponse authWrap = new AtomsService.OctaAuthResponse();
        authWrap.error_description = authWrap.error = authWrap.token_type = authWrap.scope = authWrap.access_token = 'authWrap';
        authWrap.expires_in = 123;
    }
}
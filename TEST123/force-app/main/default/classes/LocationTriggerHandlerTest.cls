/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : LocationTriggerHandlerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 21.06.2019
*  @Description      : Test class for LocationTriggerHandler
**********************************************************************************************************************
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class LocationTriggerHandlerTest {

/***************************************************************************************************
* @Description  : Test Method to test createLocations 
***************************************************************************************************/
      static  testmethod void runTestcreateLocations() {
           List<Location_ID__c> locationIdList = new List<Location_ID__c>();
           List<LocationId__e> LocationEventLst = new List<LocationId__e>();                   
           String sitename='123';
           LocationEventLst=TestDataFactory.createLocationIds(sitename);
           locationIdList = TestDataFactory.createLocationId('123','Test Location',200);
           Test.startTest();
           insert locationIdList;
           EventBus.publish(LocationEventLst);
           Test.stopTest();
           Test.getEventBus().deliver();
    } 
    static  testmethod void runTestcreateLocationsSuccess() {
           List<Location_ID__c> locationIdList = new List<Location_ID__c>();
           List<LocationId__e> LocationEventLst = new List<LocationId__e>();                   
           
           Test.startTest();
           List<Switch_ID__c> switchIdLst = new List<switch_ID__c>();
           switchIdLst=TestDataFactory.createSwitchIds('1234','1234',1);
           insert switchIdLst;
           Location_ID__c loc1 = new Location_ID__c();
           loc1.Location_ID__c = '2345';
           loc1.Switch__c = switchIdLst[0].Id;
           insert loc1;
        
           LocationId__e event = new LocationId__e();
           event.Site_cd__c = '1234';
           event.Switch_ID__c = '1234';
           event.Ring_cd__c='23435';
           
           Location_ID__c loc = new Location_ID__c();
           loc.Location_ID__c = '1234';
           loc.Switch__c = switchIdLst[0].Id;
           insert loc;
            Integration_Log__c log = new Integration_Log__c();
            log.API_Type__c = 'DWH Location';
            log.Location__c = loc.Id;
            insert log;
        
           EventBus.publish(event);
           Test.stopTest();
           Test.getEventBus().deliver();
    }    
    
    static  testmethod void runTestcreateLocationsFailure() {
           List<Location_ID__c> locationIdList = new List<Location_ID__c>();
           List<LocationId__e> LocationEventLst = new List<LocationId__e>();                   
           
           Test.startTest();
           List<Switch_ID__c> switchIdLst = new List<switch_ID__c>();
           switchIdLst=TestDataFactory.createSwitchIds('1234','1234',1);
           insert switchIdLst;
           
           Location_ID__c loc1 = new Location_ID__c();
           loc1.Location_ID__c = '2345';
           loc1.Switch__c = switchIdLst[0].Id;
           insert loc1;
        
           LocationId__e event = new LocationId__e();
           event.Site_cd__c = '1234';
           event.Switch_ID__c = '1234';
           event.REGION_NAME__c = 'East1';
           event.Ring_cd__c='22345';
           
           Location_ID__c loc = new Location_ID__c();
           loc.Location_ID__c = '1234';
           loc.Switch__c = switchIdLst[0].Id;
           insert loc;
    
           Integration_Log__c log = new Integration_Log__c();
            log.API_Type__c = 'DWH Location';
            log.Location__c = loc.Id;
            insert log;
        
           EventBus.publish(event);
           Test.stopTest();
           Test.getEventBus().deliver();
    }    
}
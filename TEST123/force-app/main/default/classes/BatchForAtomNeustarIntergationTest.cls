@isTest 
public class BatchForAtomNeustarIntergationTest{
        static testMethod void BatchForAtomNeustarIntergationAdd(){

        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'Add Circuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].Circuit_ID_Primary_Key__c = '';//5232432546436
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Circuit: Ethernet';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        List<Case> tempCaseList = [Select id,(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c,NNI_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        System.debug('tempCaseList!!!!!!'+tempCaseList);
        
        Test.startTest();
            BatchForAtomNeustarIntergation TempNew = new BatchForAtomNeustarIntergation();
            Database.executeBatch(TempNew);
            BatchForAtomNeustarIntergation.getAsrTypeValues();
            circuitList[0].API_Status__c = 'Successful';
            Update CircuitList[0];
            Circuit__c cir = [Select API_Status__c from Circuit__c where Id =: circuitList[0].Id];
            system.assertEquals(cir.API_Status__c, 'Successful');
        Test.stopTest();
    }
    
    static testMethod void BatchForAtomNeustarIntergationGet(){
        
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'GetCircuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Existing';
        circuitList[0].Circuit_ID_Primary_Key__c = '5232432546436';
       
        insert circuitList;
        List<Case> tempCaseList = [Select id,(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c,NNI_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        System.debug('tempCaseList!!!!!!'+tempCaseList);
        
        Test.startTest();
            BatchForAtomNeustarIntergation TempNew = new BatchForAtomNeustarIntergation();
            Database.executeBatch(TempNew);
            BatchForAtomNeustarIntergation.getAsrTypeValues();
            circuitList[0].API_Status__c = 'Successful';
            Update CircuitList[0];
            Circuit__c cir = [Select API_Status__c from Circuit__c where Id =: circuitList[0].Id];
            system.assertEquals(cir.API_Status__c, 'Successful');
        Test.stopTest();
    }
    
    static testMethod void BatchForAtomNeustarIntergationTest(){

        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'Add Circuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].Circuit_ID_Primary_Key__c = '';//5232432546436
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Circuit: Ethernet';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        List<Case> tempCaseList = [Select id,(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c,NNI_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        System.debug('tempCaseList!!!!!!'+tempCaseList);
        
        Test.startTest();
            BatchForAtomNeustarIntergation TempNew = new BatchForAtomNeustarIntergation();
            Database.executeBatch(TempNew);
            BatchForAtomNeustarIntergation.getAsrTypeValues();
        Test.stopTest();
    }

    
    static testMethod void BatchForAtomNeustarIntergationGetCircuit(){
        
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        Database.BatchableContext BC;
        SchedulableContext SC;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> circuitIds = new Set<Id>();
        SwitchAPICallOuts__c switchTemp = new SwitchAPICallOuts__c();
        switchTemp.Name = 'GetCircuit';
        switchTemp.Execute_API_Call__c = true;
        INSERT switchTemp;
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC MULTIPOINT INSTALL','COMCAST','Install',1,workOrdTaskList[0].Id);
        insert purOrderList;
        System.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Existing';
        circuitList[0].Circuit_ID_Primary_Key__c = '5232432546436';
       
        insert circuitList;
        List<Case> tempCaseList = [Select id,(Select id,Circuit_Type__c,Circuit_ID_Primary_Key__c,NNI_ID_Primary_Key__c from Circuits1__r) from Case WHERE Id IN :caseIds];
        System.debug('tempCaseList!!!!!!'+tempCaseList);
        
        Test.startTest();
            BatchForAtomNeustarIntergation TempNew = new BatchForAtomNeustarIntergation();
            Database.executeBatch(TempNew);
            BatchForAtomNeustarIntergation.getAsrTypeValues();
            /*circuitList[0].API_Status__c = 'Successful';
            Update CircuitList[0];
            Circuit__c cir = [Select API_Status__c from Circuit__c where Id =: circuitList[0].Id];*/
            //system.assertEquals(cir.API_Status__c, 'Successful');
        Test.stopTest();
    }
 }
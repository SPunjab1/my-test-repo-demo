@isTest
public class OMPh2_BulkUploadCtrlTest {
    
    @testSetup static void setup() { 
        
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        insert TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        List<Case> lstCase = TestDataFactoryOMph2.createCase( 'AAV CIR Retest', 2, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId());
        insert lstCase;
        lstCase[0].ParentId = lstCase[1].id;
        lstCase[0].OM_Integration_Status__c='Start Integration';
        update lstCase;
        
        List<Purchase_Order__c> lstPurchase_Order = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT', 'COMCAST', 'Disconnect ', 1, null);
        lstPurchase_Order[0].PON__c = 'VG08184A-RHM100';
        insert lstPurchase_Order;
        List<circuit__c> lstCircuit = TestDataFactoryOMph2.createCircuitRecords( lstCase[0].Id, 1);
        insert lstCircuit; 
        
        /*List<Asr__c> lstAsr = TestDataFactoryOMph2.createASRRecords( null, lstPurchase_Order[0].Id, lstCircuit[0].Id, 2);
        lstAsr[0].SubService_Type__c = 'EVC';
        lstAsr[1].SubService_Type__c = 'UNI';
        lstAsr[1].PON__c = 'VG08184A-RHM100';
        lstAsr[1].Circuit_Category__c='N/A';
        lstAsr[1].Bandwidth__c='N/A';
        lstAsr[1].AAV_Type__c='Not Applicable';
        insert lstAsr;*/
        
        insert TestDataFactoryOMph2.CreateLocation_ID('TEST LOC', 2);
    }
    
    @isTest
    static void checkDuplicateRecords() {
        
        Map<String,Map<String,String>> fileContentMap = new Map<String,Map<String,String>>();
        fileContentMap.put('VG08184A-RHM100', null);
        fileContentMap.put('VG08184A-RHM1001', null);
        fileContentMap.put('', null);
        List<String>duplicatePONumber=new List<String>();
        duplicatePONumber= OMPh2_BulkUploadCtrl.checkDuplicateASRPONnum(new List<String>(fileContentMap.keySet()));
           System.assertEquals(duplicatePONumber, null);
        duplicatePONumber=OMPh2_BulkUploadCtrl.checkDuplicateRecords(fileContentMap);
           System.assertEquals(duplicatePONumber.size()>0, true);
        duplicatePONumber=OMPh2_BulkUploadCtrl.checkDuplicateRecords(new Map<String,Map<String,String>>());
           System.assertEquals(duplicatePONumber, null);
    }
    
    @isTest
    static void validateWorkOrderTask(){
        
        OMPh2_BulkUploadCtrl.validateWorkOrderTask(new List<String>{[Select CaseNumber From Case Limit 1][0].CaseNumber});
        
        List<String> locNameList = new List<String>();
        for(Location_ID__c locId : [Select Id, Name From Location_ID__c]){
            locNameList.Add(locId.Name);
        }
        Map<String,Map<String,Location_ID__c>>locationIDMap=new Map<String,Map<String,Location_ID__c>>();
           locationIDMap= OMPh2_BulkUploadCtrl.validateLocationId(locNameList);
        system.debug('locationIDMap:'+locationIDMap);
        System.assertNotEquals(locationIDMap.get('Invalid'), null);
    }
    
    @isTest
    static void  validateLocationId(){
        
        Case cse = [Select Id, ParentId From Case limit 1];
        Map<String,Map<String,String>> poMap = new Map<String,Map<String,String>>{'ABC-PON'=> new Map<string,String>{ 'PON'=>'ABC-PON', 'ProductCatalogName'=>'EVC MULTIPOINT INSTALL', 'TradingPartner'=>'COMCAST','DDD'=>'2020-03-12', 'Activity'=>'Install', 'WorkOrderTask'=> cse.Id}};
        Map<String,Case> validWoTaskMap = new Map<String,Case>{ cse.Id=> cse};
                
        Location_ID__c loc = [Select Id From Location_ID__c limit 1];
        Map<String,Location_ID__c> validLocationMap = new Map<String,Location_ID__c> {'ASITEID' => loc, 'ZSITEID' => loc};
            
        Map<String,Map<String,String>> circuitMap = new Map<String,Map<String,String>>{'ABC-PON'=> new Map<string,String>{ 'count'=>'1', 'CircuitType1'=> 'Circuit: Ethernet', 'CircuitId1'=>'EVC MULTIPOINT INSTALL', 
                'ControllingSite1'=>'A', 'OrderingGuideId1'=>'Install', 'BillingCircuitId1'=> 'ghhags677aga',
                'UserCircuitId1'=>'Aasass', 'BAN1'=>'Install', 'Category1'=> 'WAN',
                'IsMeetingPoint1'=>'true', 'LOAProvided1'=>'false',
                'ASiteId1'=>'ASITEID', 'ZSiteId1'=>'ZSITEID', 'Bandwidth1'=> 'Ethernet'}};
                    
        //Map<String,Purchase_Order__c> ponToPORecMap = new Map<String,Purchase_Order__c>{ 'ABC-PON' => [Select Id , Work_Order_Task__c From Purchase_Order__c limit 1]};
		Map<String,Map<String,String>> asrMap = new Map<String,Map<String,String>>{'ABC-PON'=> new Map<string,String>{ 'ASRPON1'=> 'ABC-PON', 'count'=> '1', 'ASRtype1'=>'New Service: Ethernet',
        'OrderedDate1'=>'2020-06-15','CIR1'=>'10.20','ForecastVersion1'=>'12.90','VLANTWO1'=>'12.2', 'VLAN1'=>'10.23','ChangeValue1'=>'TEST', 'ChangeType1'=>'TSP Number', 'EthernetCircuitType1'=>'UNI', 'EthernetTechonlogyType1'=>'AAV','TestCIR1'=>'123123',
            'OrderNumber1'=>'12312', 'OrderDestination1'=>'Email', 'ServiceType1'=>'Ethernet', 'TragetDownloadSpeed1'=>'123','AAV1'=>'Ethernet', 'VLANUNI1'=>'121'}};
        Map<String,String>woTaskStrtIntegrationMap=new Map<String,String>{cse.Id =>'Y'};   
        OMPh2_BulkUploadCtrl.createRecords( poMap, circuitMap, asrMap, validWoTaskMap, validLocationMap,woTaskStrtIntegrationMap);
        
        List<Asr__c> lstAsr = TestDataFactoryOMph2.createASRRecords( null, [Select Id From Purchase_Order__c limit 1].Id, [Select Id From circuit__c limit 1].Id, 2);
        lstAsr[0].SubService_Type__c = 'EVC';
        lstAsr[1].SubService_Type__c = 'UNI';
        lstAsr[1].PON__c = 'VG08184A-RHM100aaaaaaaaaaaaaaaaaaaaaaaaawqwqwqwaasass';
        
        OMPh2_BulkUploadCtrl.methodToInsert(lstAsr);
         System.assertNotEquals(lstAsr,null);
        lstAsr[1].SubService_Type__c = 'UNAI';
        lstAsr[1].PON__c = 'VG08184A-RHM10';        
        OMPh2_BulkUploadCtrl.methodToInsert(lstAsr);
        System.assertNotEquals(lstAsr,null);
    }
}
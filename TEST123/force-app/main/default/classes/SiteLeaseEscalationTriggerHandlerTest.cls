@isTest(SeeAllData=false)
private class SiteLeaseEscalationTriggerHandlerTest {
    static  testmethod void runRestrictSiteLeaseEscalations() {
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
       	insert objAccount;
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
            oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
            oppRecForContract.AccountId = objAccount.Id;
            oppRecForContract.Contract_Start_Date__c = System.today();
            oppRecForContract.Term_mo__c = 1.00;
        insert oppRecForContract;
    	Site_Lease_Escalation__c siteLease = TestDataFactory.createSiteLEaseEscalation(oppRecForContract.Id);
        siteLease.Lease_Version__c ='New';
        siteLease.Escalation_Type__c ='One Time';
        siteLease.Escalator__c=10000;
        insert siteLease; 
        
        Id communityProfile = [select id from profile where name = 'NLG Market/Region Community'].id;
       
        Account ac = new Account(
            name ='PartnerAccount'
        ) ;
        insert ac; 
       
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;  
                  
        User user = new User(
            alias = 'test123', 
            email='test123@noemail.com',
            emailencodingkey='UTF-8', 
            lastname='Testing', 
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = communityProfile, 
            country='United States',IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', 
            username='tester@noemail.com.'+Math.round((Math.random()*(900000) + 100000))
        );
       
        insert user;
        delete siteLease;
        undelete siteLease;
        siteLease.Lease_Version__c = 'Current';
        update siteLease;
        try{
        delete siteLease;
        }catch(Exception Ex){}
        
        system.runAs(user) {
        	
        }
        
    }
}
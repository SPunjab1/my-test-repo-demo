/*********************************************************************************************************************
**********************************************************************************************************************   
 *  @Class            : ConstantsUtility
 *  @Author           : IBM
 *  @Version History  : 1.0
 *  @Date             : 01/06/2019
 *  @Description      : Constant Class
**********************************************************************************************************************
**********************************************************************************************************************/

public without sharing class ConstantsUtility {
    public static final String TMO_WORKORDER_RECORD_TYPE        ='Work Order';
    public static final String TMO_WORKORDERTASK_RECORD_TYPE    ='Work Order Task';
    public static final string TEMPLATE_CMG_RETAIL_BROADBAND_DEP='OM CMG_Retail Broadband Dep';
    public static final string TMO_SLA_PROCESS_NAME             ='Order Procurement SLA Process';
    public static final string SYSTEMNAME                       ='system';    
    public static final string TEMPLATE_CMG_CORE_FACILITY_ADD   ='CMG Core Facility Add';  
    public static final Integer SUCCESS_RESPONSE   = 200; 
    public static final String CASE_STATUS_CLOSED = 'Closed';
    public static final String CASE_STATUS_NA = 'N/A';
    public static final String CASE_STATUS_NOT_REQUIRED = 'Not Required';
    public static final String SYSTEM_NAME = 'Salesforce.com';
    public static final String IN_PROGRESS = 'In Progress';
    public static final String TERMINATED = 'Terminated';
    public static final String NA_STATUS = 'NotRequired';
    public static final String SYSTEM_ADMIN= 'System Administrator';
    public static final String AUTOMATED_PROCESS= 'Automated Process';
}
public with sharing class SiteLeaseEscalationTriggerHandler extends TriggerHandler {
    private static final string SLE_API = 'Site_Lease_Escalation__c';
    public SiteLeaseEscalationTriggerHandler() {
        super(SLE_API);
    }
    // adding cpu limit variable.
    Integer startCPUTime;
    /***************************************************************************************
	@Description : Return the name of the handler invoked
	****************************************************************************************/
    public override String getName() {
        return SLE_API;
    }
	/***************************************************************************************
	@Description : Trigger handlers for events
	****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems) {
        
    }
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {

    }
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {
        map<Id,Site_Lease_Escalation__c> oldSiteLEscaltionMap = new map<Id,Site_Lease_Escalation__c>();
        oldSiteLEscaltionMap = (map<Id,Site_Lease_Escalation__c>) oldItemsMap;
		preventUpdationDeletionForCommunityUser(oldSiteLEscaltionMap.values());
        
    }
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
        
    }
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {
        
    }
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
        
    }
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        
    }
    public void preventUpdationDeletionForCommunityUser(List<Site_Lease_Escalation__c> siteLeaseEscalations){
        for(Site_Lease_Escalation__c sleObj : siteLeaseEscalations){
            if((isCommunity() || test.isRunningTest()) && sleObj.Lease_Version__c == 'Current'){
            	sleObj.addError('Insufficient privileges');	    
            }    
        }
    }
        
    public boolean isCommunity(){
        return NetWork.getNetWorkId() != null;
    }
}
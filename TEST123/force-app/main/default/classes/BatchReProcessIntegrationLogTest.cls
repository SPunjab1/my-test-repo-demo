/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : BatchReProcessIntegrationLogTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 20.06.2019
*  @Description      : Test class for BatchReProcessIntegrationLog
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchReProcessIntegrationLogTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static testMethod void enableReprocessFlag(){

        List<Integration_Log__c> integrationLogsToInsert = new List<Integration_Log__c>();

        for(Integer i=0; i<5; i++){
            Integration_Log__c integrationLog = new Integration_Log__c();
            integrationLog.Status__c = 'Failure';
            integrationLog.Reprocess__c = false;

            integrationLogsToInsert.add(integrationLog);
        }

        insert  integrationLogsToInsert;

        Test.startTest();
            BatchReProcessIntegrationLog batch = new BatchReProcessIntegrationLog();
            Database.executeBatch(batch);
        	system.schedule('Jobs', CRON_EXP, batch);
        Test.stopTest();
    }
}
/*********************************************************************************************
@author     : Balaji
@date       : 2020.07.22
@description: This class is used to enable the release Notes Banner to users.
@Version    : 1.0
**********************************************************************************************/

global class BatchPPDEnableReleasenotes implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'SELECT Id,Hide_Release_Notes__c,IsActive FROM User WHERE Hide_Release_Notes__c = true AND IsActive =true';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<User> scope) {

        List<User> lstUserUpdate  = new List<user>();

        for(User temp: scope){
            temp.Hide_Release_Notes__c = false;
            lstUserUpdate.add(temp);
        }
        if(lstUserUpdate != null && lstUserUpdate.size() >0){
            update(lstUserUpdate);
        }
    }
    global void finish(Database.BatchableContext BC) {

    }
}
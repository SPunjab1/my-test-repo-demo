public class NLG_DashboardHomeCtl {
    
    @AuraEnabled
    public static List<NLG_Reports__mdt> getReports(){
        Id userId=System.UserInfo.getUserId();
        User usr=[Select Id,Role_NLG__c,Market_NLG__c,MLA_NLG__c,NLG_National_Team__c,Region_NLG__c,IsActive 
                  From User Where Id=:userId And (Role_NLG__c !='' Or Profile.Name='System Administrator')];
        List<NLG_Reports__mdt> nlgReports = new List<NLG_Reports__mdt>();
        nlgReports =[Select Id,Label,Name__c,Url__c,Role__c,Description__c,order__c,Section__c,Apply_Filter__c From NLG_Reports__mdt
                                          Where Role__c like :('%'+usr.Role_NLG__c+'%') order by order__c];
        if(nlgReports.size()>0){
           for(NLG_Reports__mdt nlgreport: nlgReports){
               if(nlgreport.Url__c !=null && nlgreport.Apply_Filter__c){
                   if(usr.NLG_National_Team__c !=null || usr.Region_NLG__c !=null){
                       nlgreport.Url__c +='?filter=';                       
                       if(usr.NLG_National_Team__c !=null){
                           List<String> teamList = usr.NLG_National_Team__c.split(';');
                           String teamString='';
                           if(teamList !=null){
                               for(String st: teamList){
                                   teamString += teamString!=''?',\''+st+'\'':'\''+st+'\'';
                               }
                           } else {
                               teamString =usr.NLG_National_Team__c;
                           }
                           nlgreport.Url__c +='Opportunity/Negotiating_Vendor__c in ('+teamString+')';
                           system.debug('nlgreport.Url__c:'+nlgreport.Url__c);
                           //'?filter=Opportunity/Region__c in (\'Northeast\',\'South\',\'West\',\'Central\') and Opportunity/Negotiating_Vendor__c in (\'BTS/BTR Team\',\'Central Region\',\'InBLD/DAS/Venue\',\'National - ATC Closeout Team\',\'NLRP/LO\',\'NorthEast Region\',\'Other\',\'South Region\',\'West Region\')';
                       } else if(usr.Region_NLG__c !=null){
                           List<String> regionList = usr.Region_NLG__c.split(';');
                           String regionString='';
                           if(regionList !=null){
                               for(String st: regionList){
                                   regionString += regionString!=''?',\''+st+'\'':'\''+st+'\'';
                               }
                           } else {
                               regionString =usr.Region_NLG__c;
                           }
                           nlgreport.Url__c +='Opportunity/Region__c in ('+regionString+')';
                           system.debug('nlgreport.Url__c:'+nlgreport.Url__c);
                       }
                   }
               }
           } 
        }
        return nlgReports;        
    }
    
    public class ReportWrapper{
        public String section;
        public List<NLG_Reports__mdt> nlgReportList;
        public ReportWrapper(String section,List<NLG_Reports__mdt> reportMdt){
            this.section = section;
            this.nlgReportList = reportMdt;
        }
    }
}
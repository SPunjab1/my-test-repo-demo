public class NLG_OpportunitySelectionCtl {
    
    @AuraEnabled
    public static String getOpportunities(Id recordId){
        JSONGenerator jsongen = JSON.createGenerator(true);
        jsongen.writeStartObject();
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp =[Select Id,Name,Landlord__c,CLIQ_ID__c,CLIQ_Sub_Type__c,Site_ID__c, Site_Class__c, Lease_ID__c,Avoidance_Opportunity__c From Opportunity 
                          Where Id =:recordId limit 1];
        oppList =[Select Id,Name,Landlord__c,CLIQ_ID__c,CLIQ_Sub_Type__c,Site_ID__c, Site_Class__c, Lease_ID__c,NLG_Criteria__c, 
                  Lease_Liablity__c, Incremental_Lease_Liability__c,Total_Rent_over_total_term__c, Gross_Savings__c,Lease_Intake__c,StageName,Approve_Reject_Reason__c,Avoidance_Opportunity__c From Opportunity 
                  Where Site_ID__c =:opp.Site_Id__c And Id !=:recordId];
        jsongen.writeFieldName('oppList');
        jsongen.writeObject(oppList);
        if(opp.Avoidance_Opportunity__c!=null){
            jsongen.writeStringField('AvoidanceOpp', opp.Avoidance_Opportunity__c);
        }
        jsongen.writeEndObject();
        system.debug('json:'+jsongen.getAsString());
        return jsongen.getAsString();
    }
    
    @AuraEnabled
    public static String updateOpportunity(Id oppId, Id avoidanceOppId){
        try{
            List<Opportunity> oppUpdList = new List<Opportunity>();
            
            if(oppId !=null && avoidanceOppId !=null){
                Opportunity avoidanceOpp =[Select Id,StageName,Approve_Reject_Reason__c from Opportunity Where Id =:avoidanceOppId limit 1];
                if(avoidanceOpp !=null && avoidanceOpp.StageName !='Closed Lost'){
                    avoidanceOpp.StageName='Closed Lost';
                    avoidanceOpp.Approve_Reject_Reason__c='Dead/New entry in tool';
                    oppUpdList.add(avoidanceOpp);
                }
                Opportunity opp = new Opportunity(Id=oppId, Avoidance_Opportunity__c=avoidanceOppId);
                oppUpdList.add(opp);
                if(oppUpdList.size()>0){
                    Update oppUpdList;
                }
                
            }
        } catch(Exception ex){
            system.debug('updateOpportunity-Exception:'+ ex.getMessage());
            return ex.getMessage();
        }
        return 'Success';
    }

}
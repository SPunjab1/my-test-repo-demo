/******************************************************************************
@author      : IBM
@date        : 02.06.2020
@description : Utility class for generic methods.
@Version     : 1.0
*******************************************************************************/
public with sharing class SI_UtilityClass {
    
    /**************************************************************************
    * @description To prevent multiple trigger/recursion
    **************************************************************************/
    public static Boolean triggerWillRun = true;
    
    /**************************************************************************
    * @description 0: insert, 1: update, 2: delete, 3: undelete, 4: upsert
    **************************************************************************/
    private static List<String> listOfDmlOperations = System.Label.SI_DML_Operations.split(';');
    
    /**************************************************************************
    * @description To create a map of record type name to id based on sObject
    * @param sObjectName - to hold object name for fetching record type
    * @return Map<String,Id> - map of record type name to Id
    **************************************************************************/
    public static Map<String, Id> getAllRecordTypeMap(String sObjectName) {
        //map to store record type id against record type name
        Map<String, Id> mapOfRecordTypeNameToID = new Map<String, Id>();
        //fetch all the record type related to sObject
        for (RecordType rt : [SELECT Id, Name
                              FROM RecordType
                              WHERE SobjectType = :sObjectName]) {
            //put the details into our map (Name -> Id)
            mapOfRecordTypeNameToID.put(rt.Name, rt.Id);
        }
        return mapOfRecordTypeNameToID;
    }
    
    /**************************************************************************
    * @description To create a map of Profile name 
    * @param profileId - to hold current User profile Id
    * @return String - profile name
    **************************************************************************/
    public static String getProfileInfo(String profileId) {
        //fetch all the record type related to sObject
        return [SELECT Name 
                FROM Profile 
                WHERE Id = :profileId LIMIT 1].Name;
    }
    
   
    /**************************************************************************
    * @description To fetch record type Id based on record type label
    * @param sObjectName - to hold sobject name for fetching record type
    * @param recordTypeName - record type name
    * @return Id - record type id
    **************************************************************************/
    public static Id getRecordTypeIdByName(String sObjectName, String recordTypeName) {
        //Initialize variable for record type id
        //done changes
        Id rtTypeId;
        //Check if sObject name and recordTypeName are not blank
        if(String.isNotBlank(sObjectName) && String.isNotBlank(recordTypeName)) {
            rtTypeId = Schema.getGlobalDescribe().get(sObjectName).getDescribe()
                .getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        return rtTypeId;
    }
    
    /**************************************************************************
    * @description : To fetch record type Id based on record type Api name
    * @param sObjectName - to hold sobject name for fetching record type
    * @param rtDeveloperName - record type developer name
    * @return Id - record type id
    **************************************************************************/
    public static Id getRecordTypeIdByDeveloperName(String sObjectName, String rtDeveloperName) {
        //Initialize variable for record type id
        Id rtTypeId;
        //Check if sObject name and recordTypeName are not blank
        if(String.isNotBlank(sObjectName) && String.isNotBlank(rtDeveloperName)) {
            rtTypeId = Schema.getGlobalDescribe().get(sObjectName).getDescribe()
                .getRecordTypeInfosByDeveloperName().get(rtDeveloperName).getRecordTypeId();
        }
        return rtTypeId;
    }
    
    /**************************************************************************
    * @description : To set dml options for firing assignment rules explicitly
    * @return Database.DMLOptions - dml options
    **************************************************************************/
    public static Database.DMLOptions setDmlOptionForAssignmentRules() {
        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.assignmentRuleHeader.useDefaultRule = true;
        dmlOptions.EmailHeader.triggerUserEmail = true;
        return dmlOptions;
    }
    
    /**************************************************************************
    * @description : to perform DML
    * @param sObjectList - list of records
    * @param operation - dml operation to be performed
    * @return List<sObject> - dml operation
    **************************************************************************/
    public static List<sObject> callDMLOperations(List<sObject> sObjectList, String operation) {
        try {
            if(listOfDmlOperations[0].equalsIgnoreCase(operation)) {
                //insert operation 
                insert sObjectList;
            } else if(listOfDmlOperations[1].equalsIgnoreCase(operation)) {
                //update operation
                update sObjectList;
            } else if(listOfDmlOperations[2].equalsIgnoreCase(operation)) {
                //delete operation
                delete sObjectList;
            } else if(listOfDmlOperations[3].equalsIgnoreCase(operation)) {
                //undelete operation
                undelete sObjectList;
            } else if(listOfDmlOperations[4].equalsIgnoreCase(operation)) {
                //upsert operation
                upsert sObjectList;
            }
        } catch(Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Exception in callDMLOperations: ' + ex.getMessage());
        }
        return sObjectList;
    }
    
    /**************************************************************************
    * @description : to perform DML
    * @param sObjectRec - record to be processed
    * @param operation - dml operation to be performed
    * @return sObject - dml operation
    **************************************************************************/
    public static sObject callDMLOperationsSingleRecord(sObject sObjectRec, String operation) {
        try {
            if(listOfDmlOperations[0].equalsIgnoreCase(operation)) {
                //insert operation 
                insert sObjectRec;
            } else if(listOfDmlOperations[1].equalsIgnoreCase(operation)) {
                //update operation
                update sObjectRec;
            } else if(listOfDmlOperations[2].equalsIgnoreCase(operation)) {
                //delete operation
                delete sObjectRec;
            } else if(listOfDmlOperations[3].equalsIgnoreCase(operation)) {
                //undelete operation
                undelete sObjectRec;
            } else if(listOfDmlOperations[4].equalsIgnoreCase(operation)) {
                //upsert operation
                upsert sObjectRec;
            }
            return sObjectRec;
        } catch(Exception ex) {
            System.debug(LoggingLevel.ERROR,'Exception in callDMLOperationsSingleRecord: ' + ex.getMessage());
            return null;
        }
    }
    
    /**************************************************************************
    * @description : to perform insert/update operations with Database class
    * @param sObjectList - list of records to be processed
    * @param operation - dml operation to be performed
    * @return List<sObject> - dml operation
    **************************************************************************/
    public static  List<Database.SaveResult> callDMLDatabaseInsUpdOperations(List<sObject> sObjectList, 
                                                                             String operation) {
        List<Database.SaveResult> result;
        try {
            if(listOfDmlOperations[0].equalsIgnoreCase(operation)) {
                //insert operation
                result = Database.insert(sObjectList, false);
            } else if(listOfDmlOperations[1].equalsIgnoreCase(operation)) {
                //update operation 
                result = Database.update(sObjectList, false);
            }
        } catch(Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Exception in DML callDMLDatabaseInsUpdOperations:' + ex.getMessage());
        }
        return result;
    }
    
    /**************************************************************************
    * @description : to perform DML with Database class, upsert operation
    * @param sObjectList - list of records to be processed
    * @param operation - dml operation to be performed
    * @return List<sObject> - dml operation
    **************************************************************************/
    public static  List<Database.UpsertResult> callDMLDatabaseOperations(List<sObject> sObjectList, String operation) {
        List<Database.UpsertResult> result;
        try {
            if(listOfDmlOperations[4].equalsIgnoreCase(operation)) {
                //upsert operation 
                result = Database.upsert(sObjectList, false);
            } 
        } catch(Exception ex) {
            System.debug(LoggingLevel.ERROR,'Exception in callDMLDatabaseOperations: ' + ex.getMessage());
        }
        return result;
    }
    /**************************************************************************
    * @description : to perform DML with Database class, and fire assignment rule in Bulk
    * @param sObjectList - list of records to be processed
    * @param operation - dml operation to be performed
    * @return List<sObject> - dml operation
    **************************************************************************/
    public static  List<Database.SaveResult> callDMLDatabaseAssignmentRules(List<sObject> sObjectList, String operation) {
        List<Database.SaveResult> result;
        try {
            List<String> listOfDmlOperations = System.Label.SI_DML_Operations.split(';');
            Database.DMLOptions dmlOpts = setDmlOptionForAssignmentRules();
            if(listOfDmlOperations[1].equalsIgnoreCase(operation)) {
                //update Operation 
                result = Database.update(sObjectList,dmlOpts);
            } 
        } catch(Exception ex) {
            System.debug(LoggingLevel.ERROR,'Exception in callDMLDatabaseOperations: ' + ex.getMessage());
        }
        return result;
    }
}
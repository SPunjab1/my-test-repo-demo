@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpYm11b21hcGkiLCJDb21wYW55IjoiQ3VzdG9tZXIiLCJEb21haW4iOiJUVU9NX0UyRSIsImV4cCI6MTU5NTI0Mjg3M30.ZGWwUj507gK-j1c7oA942vlToeVUa1kv-We5xngNV0Mp89tYMp6sBu2D0ALFAxhk8CoUscf6Brc-THJ7so3xwXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxXAiOiJKV1QiLCJhbGciOiJIUzUxA');
        
        res.setBody('{"status":"Success"}'); 
        //res.setBody('{"status": "Success", "statusCode": "200", "message": "PON: ASR01282020E1078 has been successfully submitted.", \"info\": { \"productCatalogName\": \"EVC PT TO PT CHANGE\", \"productCatalogId\": 434651, \"uomOrderKey\": 914851, \"customerOrderID\": \"\", \"uomOrderID\": \"TUOMEVC C00026621\", \"requestNumber\": \"ASR01282020E1078\", \"ver\": \"03\", \"orderStatus\": \"SUBMITTED\", \"activityId\": \"C\", \"activityType\": \"Change\", \"application\": \"ASR_SEND\", \"tradingPartner\": \"LIGHTTOWER FIBER NETWORKS\", \"endUser\": \"\", \"owner\": \"JSchrap1\", \"createdDate\": \"2020-05-28 16:06:07.031\", \"lastStatusChangeDate\": \"2020-05-28 16:12:40.703\", \"sellerID\": \"LT30\", \"buyerID\": \"WCG\" }}');
        res.setStatusCode(200);
        return res;
    }
}
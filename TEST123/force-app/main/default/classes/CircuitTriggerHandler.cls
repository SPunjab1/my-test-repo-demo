/*********************************************************************************************
@author     : IBM
@date       : 17.09.20 
@description: This is the trigger handler for Circuit object.
@Version    : 1.0           
**********************************************************************************************/
public with sharing class CircuitTriggerHandler extends TriggerHandler {
    private static final string Circuit_API = 'Circuit__c';
    public static Boolean isFirstTime = true;
    public CircuitTriggerHandler() {
        super(Circuit_API);
    }
    /***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/
    public override String getName() {
        return Circuit_API;
    }
    /***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems) {}
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {}
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {}
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
        if(isFirstTime){
            checkBANUpdation(newItems,newItemsMap,oldItemsMap);
            isFirstTime = false;
        }
        
    }
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {}
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {}
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {}
    
    
/*************************************************************************************
This method is chekcing if RPON and SUP is being updated to make upgrade ASR callout
**************************************************************************************/
    
    public static void checkBANUpdation(List<SObject> newItems,Map<Id, SObject> newItemsMap,Map<Id, SObject> oldItemsMap)
    {
        List<Circuit__c> newCircuitList = (List<Circuit__c>) newItems;
        Map<Id, Circuit__c> oldCircuitMap = new Map<Id, Circuit__c>();
        oldCircuitMap = (Map<Id, Circuit__c>) oldItemsMap;
        Map<Id,List<Circuit__c>> caseCircuitMap =new Map<id,List<Circuit__c>>();
        
        
        for(Circuit__c obj:newCircuitList)
        {
            if(obj.BAN__c !=null && (oldCircuitMap.get(obj.id).BAN__c != obj.BAN__c) && obj.Circuit_ID_Primary_Key__c !=null )
            {
                
                if(!caseCircuitMap.containsKey(obj.Case__c)){
                    caseCircuitMap.put(obj.Case__c,new list<Circuit__c>{obj});
                }
                else{
                    caseCircuitMap.get(obj.Case__c).add(obj);
                }
            }
            
        }
        
        
       
        if(!caseCircuitMap.values().isEmpty()){
            ID jobID=System.enqueueJob(new UpdateCircuitQueueable(caseCircuitMap)); 
        }
    }
    
}
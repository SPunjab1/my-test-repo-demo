/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : CloseTaskFromListViewControllerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 05.09.2019
*  @Description      : Test class for CloseTaskFromListViewController
**********************************************************************************************************************
****************************************************************************************************************/

@isTest(SeeAllData=false)
public with sharing class CloseTaskFromListViewControllerTest{
    
  static  testmethod void runTestCloseTaskFromListViewController() {
   system.runas(TestDataFactory.createSourcingUser()){
        DmlException expectedException;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        Integer NumOfCases=50;
        String WorkflowTmpName1='AAV CIR Retest';
        caseList= TestDataFactory.createCase(WorkflowTmpName1,NumOfCases,recTypeId);     
        Test.startTest();   
        Insert(caseList); 
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController([ SELECT id, status, CaseNumber FROM Case ]);
        stdSetController.setSelected( stdSetController.getRecords() );
        CloseTaskFromListViewController controller = new CloseTaskFromListViewController( stdSetController );
        controller.CloseTasks();
        controller.back();               
        Test.stopTest();             
  }  
 }
}
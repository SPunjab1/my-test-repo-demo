/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : BatchPublicURLGenerationTest
*  @Author           : Balaji
*  @Version History  : 1.0
*  @Creation         : 11.17.2020
*  @Description      : Test class for BatchPublicURLGenerationTest
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchPublicURLGenerationTest {
    static testMethod void TestMethod1(){ 
        Integer numOfCase=1;
        ID workOrderRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<case> caseLst = TestDataFactory.createCase('TIFS TDM Disconnect(s)',numOfCase,workOrderRecordType);
        List<Integration_Log__c> integrationLogs = new List<Integration_Log__c>(); 
        String workflowTmpName='TIFS TDM Disconnect(s)';
        String API_Type='Create Work Order';    
        caseLst[0].Total_PIER_task_in_Workflow__c= 1;
        caseLst[0].Total_Approval_Required_Tasks__c= 1;
        caseLst[0].User_Description__c = 'test';
        caseLst[0].project_name__c= 'TestChange request';
        caseLst[0].PIER_Work_Order_Task_ID__c = '123445';
        caseLst[0].Status = 'Closed';
        List<Location_id__c> locList = TestDataFactory.createLocationId('19T0087','test123',1);
        insert locList;
        //Balaji Added to check the size
        if(locList.size()>0){
            caseLst[0].Location_IDs__c=locList[0].id;
        }
        insert caseLst;
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'ABC'+ caseLst[0].Id;
        cv.PathOnClient = 'test';
        cv.ContentLocation = 'S';
        cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
		Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        ContentDocumentLink cDLink = new ContentDocumentLink();
        cDLink.ContentDocumentId = conDocId;
        cDLink.LinkedEntityId = caseLst[0].Id;
        //cDLink.LinkedEntity.Type = 'Case';
        cDLink.ShareType = 'I';
        insert cDLink;
        //ContentDocumentLink CDL = ['SELECT SystemModstamp, ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId in (SELECT Id FROM Case where Workflow_Template__c like \'TIFS%\') and LinkedEntity.Type=\'Case\'and SystemModstamp > 2020-06-01T11:51:01.000+0000'];
        //ContentDocumentLink CDL = [Select Id,ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId = caseLst[0].Id];
        //system.debug(CDL);
/*        
        Integration_Log__C log = New Integration_Log__C();
        log.API_Type__c='Attachment';
        log.Attachment_Id__c= cv.Id;
        //log.Task__c
        Insert(log);
        Test.startTest();
        PierIntegrationMock fakeResponse = new PierIntegrationMock(200,'{"transactionId":"8f96e746-7b40-4ba8-a31a-f7d394b866b7","invokingUser":"SPunjab1","timestamp":"2020-07-13T19:25:22.144Z","data":[{"id":157987,"templateId":1314,"description":"Test","longDescription":"Testing ignore","status":"active","manager":"","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:20.86834497Z","closedAt":null,"sourceSystem":"","sourceRecordId":"","tasks":{"0":{"data":{"id":873597,"workflow_id":157987,"name":"","description":"Verify Port Speed / Router Swap Complete","displayOrder":1,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.158974347Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":14,"timestamp":"2020-07-13 19:25:21.157605355 +0000 UTC","transactionId":""},"1":{"data":{"id":873598,"workflow_id":157987,"name":"","description":"Execute RFC2544, log results in ATOMs","displayOrder":2,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.276423755Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":22,"timestamp":"2020-07-13 19:25:21.275555477 +0000 UTC","transactionId":""},"2":{"data":{"id":873599,"workflow_id":157987,"name":"","description":"Execute BURST tests, log results in ATOMs, Activate Site","displayOrder":3,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.403841799Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":40,"timestamp":"2020-07-13 19:25:21.401872845 +0000 UTC","transactionId":""},"3":{"data":{"id":873600,"workflow_id":157987,"name":"","description":"Generate/Execute Script","displayOrder":4,"taskType":"CR Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.556928581Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":20,"timestamp":"2020-07-13 19:25:21.54619578 +0000 UTC","transactionId":""}}}],"version":null,"duration":1482}');
		Test.setMock(HttpCalloutMock.class, fakeResponse);
*/ 		//Test.startTest();
        BatchPublicURLGeneration BatchPublicLink = New BatchPublicURLGeneration();
        database.executeBatch(BatchPublicLink);        
        //Test.stopTest();        
    }
}
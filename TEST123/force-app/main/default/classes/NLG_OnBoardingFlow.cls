/*
*********************************************************************************************
@author  :  Mallesh Miryala     
@date    :  12/10/2020 
@Name    : NLG_OnBoardingFlow
@description: Onboarding Community User      
@Version: 

**********************************************************************************************/
public class NLG_OnBoardingFlow {
    
    public class NLGonBoardingException extends Exception{}
        
    @InvocableMethod(label='NLG_OnBoarding' 
                     description='Here in this method we will create community user with details provided on flow'
                     category='NLG_OnBoarding')
    public static void createCommunityUser(list<CommunityUserParameters> inputFromFlow){
        if(inputFromFlow != null && !inputFromFlow.isEmpty()){
        	string profileToQuery; 
            string roleToQuery;
            if(inputFromFlow[0].CommunityRole == 'Submitter'){
            	profileToQuery = system.label.NLG_SubmitterCommunityProfileName;
                roleToQuery = 'Submitter';
            }else if(inputFromFlow[0].CommunityRole == 'Market'){
            	profileToQuery = system.label.NLG_MarketCommunityProfileName; 
                roleToQuery = 'Market POC (NLG)';
            }
            list<Profile> portalProfile = [SELECT Id FROM Profile WHERE Name =:profileToQuery Limit 1];
            if(!portalProfile.isEmpty()){
                list<Account> partnerAccount = [SELECT Id FROM Account WHERE Name =:system.label.NLG_CommunityAccountName];
                if(!partnerAccount.isEmpty()){
                    Contact communityContact = new Contact(
                    	FirstName = inputFromFlow[0].firstName,
                        LastName =  inputFromFlow[0].lastName,
                        Email 		= inputFromFlow[0].email,
                        AccountId = partnerAccount[0].ID
                    );
                    Database.SaveResult saveResult = Database.insert(communityContact, false);
                    list<sObject> duplicateRecords;
                    if (!saveResult.isSuccess()) {
                        for (Database.Error error : saveResult.getErrors()) {
                            if (error instanceof Database.DuplicateError) {
                                Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                                Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                                duplicateRecords = new List<sObject>();
                                Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();
                                Datacloud.MatchResult matchResult = matchResults[0]; 
                                Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();             
                                for (Datacloud.MatchRecord matchRecord : matchRecords) {
                                    System.debug('MatchRecord: ' + matchRecord.getRecord());
                                    duplicateRecords.add(matchRecord.getRecord());
                                }
                            }
                        } 
                        if(duplicateRecords != null){
                            if(duplicateRecords.Size() > 1){
                       			throw new NLGonBoardingException('Multiple duplicate records found, Please contact Salesforce Adminstrator'); 	
                            }else if(duplicateRecords.Size() == 1){
                                Contact exiDuplicateRecord = [SELECT Id,AccountId FROM Contact WHERE Id =:(Id)duplicateRecords[0].get('Id')];
                                communityContact.AccountId = exiDuplicateRecord.AccountId;
                                communityContact = exiDuplicateRecord;                            	    
                            }
                        }
                    }
                    list<User> lstOfDuplicateUserWithUName = [SELECT Id,ProfileId FROM User WHERE 
                                                              UserName =:inputFromFlow[0].UserName or
                                                              Default_User_NTID__c =:inputFromFlow[0].NetWorkId or
                                                              Email =:inputFromFlow[0].email limit 1];
                    if(!lstOfDuplicateUserWithUName.isEmpty()){
                        throw new NLGonBoardingException('User is already exist, please submit an admin ticket to update their access');
                    }
                    list<User> lstOfDuplicateUSers = [SELECT Id FROM User WHERE contactId =:communityContact.Id];
                    User communityUser = new User(
                        Id			= (!lstOfDuplicateUsers.isEmpty() ? lstOfDuplicateUsers[0].Id : null),
                        UserName 	= inputFromFlow[0].UserName,
                        FirstName 	= inputFromFlow[0].firstName,
                        LastName 	= inputFromFlow[0].lastName,
                        Role_NLG__c = roleToQuery,
                        Alias 		= inputFromFlow[0].UserName.substring(0,1) + Math.round((Math.random()*(900000) + 100000)),
                        email 		= inputFromFlow[0].email,
                        ContactId   = communityContact.Id,
                        ProfileId 	= portalProfile[0].Id,
                        Market_NLG__c 	= inputFromFlow[0].CommunityUserMarket,
                        MLA_NLG__c 		= inputFromFlow[0].MLA,
                        Default_User_NTID__c = inputFromFlow[0].NetWorkId,
                        EmailEncodingKey = 'UTF-8',
                        CommunityNickname = inputFromFlow[0].UserName.substring(0,4)+ Math.round((Math.random()*(900000) + 100000)),
                        TimeZoneSidKey = 'America/Los_Angeles',
                        LocaleSidKey = 'en_US',
                        LanguageLocaleKey = 'en_US'
                    );
                    createCommunityUser(json.serialize(communityUser));                        
                }            	    
            }                
        }        			    
    }
    
    @future
    public static void createCommunityUser(string strUser){
       	User communityUser = (User)System.JSON.deserialize(strUser, User.Class) ; 
        try{
    		Database.upsert(communityUser);    
        }catch(Exception Ex){
            system.debug('**User Creation Exception from NLG OnBoarding Flow : '+Ex.getMessage());
        }
    }
    
    public class CommunityUserParameters{
        @InvocableVariable(required=true)
        public string firstName;
        @InvocableVariable(required=true)
        public String lastName; 
        @InvocableVariable(required=true)
        public String email; 
        @InvocableVariable(required=true)
        public string CommunityRole;
        @InvocableVariable(required=true)
        public String CommunityUserMarket; 
        @InvocableVariable(required=true)
        public String NetWorkId; 
        @InvocableVariable(required=true)
        public String MLA; 
        @InvocableVariable(required=true)
        public String UserName; 
    }
}
/*********************************************************************************************
@author     : IBM
@date       : 10 Aug 2020
@description: Test class for Batch class AtomsIntegrationAPI
@Version    : 1.0           
**********************************************************************************************/
@isTest(SeeAllData = false)
public class AtomsIntegrationAPITest {
     Public Static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
    
     @TestSetup
    static void testDataSetup(){
        List<Location_ID__c> locLst = TestDataFactoryOMph2.CreateLocation_ID('Test Location', 2);
        insert locLst;
        
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        
        /*TMobilePh2LoginDetailsHierarchy__c atomsToken = new TMobilePh2LoginDetailsHierarchy__c();
        atomsToken.Atoms_Client_Id__c = 'testClientID';
        atomsToken.Atoms_Client_Secret__c = 'testClientSec';
        atomsToken.Atoms_End_Point__c = 'https://testokta.com';
        insert atomsToken;*/
        
        List<SwitchAPICallOuts__c> switchAPIMapSet = new List<SwitchAPICallOuts__c>();
        switchAPIMapSet = TestDataFactoryOMph2.createSwitchAPICalloutCustSet();
        insert switchAPIMapSet;
        
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        
        system.debug('atomsUtilitySet>>'+atomsUtilitySet);
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        system.debug('WORD>>'+workOrdTaskList);
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',1,workOrdTaskList[0].Id);
        insert purOrderList;
        system.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,2));
        circuitList[0].RecordTypeId = Schema.SObjectType.Circuit__c.getRecordTypeInfosByName().get('Circuit').getRecordTypeId();
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].BAN__c = '1234567';
        circuitList[0].Category__c = 'IUB';
        circuitList[0].Controlling_Site__c = 'Z';
        circuitList[0].LOA_Provided__c = true;
        circuitList[0].User_Ciruit_ID__c = 'TestUserCir';
        circuitList[0].Billing_Circuit_Id__c = 'TestBillCir';
        circuitList[0].Ordering_Guide_ID__c = 'C41203EB-7D4C-479F-B7D9-C5AAA8FAAC9D';
        circuitList[0].Circuit_ID_Primary_Key__c =  NULL;
        circuitList[0].Bandwidth__c = 'Ethernet';
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        
        circuitList[1].RecordTypeId = Schema.SObjectType.Circuit__c.getRecordTypeInfosByName().get('NNI').getRecordTypeId();
        circuitList[1].Circuit_Type__c = 'NNI';
        circuitList[1].Billable_NNI__c = false;
        circuitList[1].Billing_Circuit_Id__c = 'TestBillCir';
        circuitList[1].Ordering_Guide_ID__c = 'C41203EB-7D4C-479F-B7D9-C5AAA8FAAC9D';
        circuitList[1].Circuit_ID_Primary_Key__c =  NULL;
        circuitList[1].Bandwidth__c = 'Ethernet';
        circuitList[1].Controlling_Site__c = 'Z';
        circuitList[1].ESP__c = 'testESP';
        circuitList[1].Comments__c = 'test';
        circuitList[1].A_Site_ID__c = locLst[0].Id;
        circuitList[1].Z_Site_ID__c = locLst[1].Id;
        circuitList[1].User_NNI_ID__c = 'uId';
        circuitList[1].Vendor_Override__c = 'overrideVendor';
        
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        update workOrdTaskList;
        
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        insert circuitList;
        update workOrdTaskList;
        system.debug('circuitList>>'+circuitList);
        
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,1);
        asrList[0].Circuit_Id__c = circuitList[0].Id;
        asrList[0].ASR_Type__c = 'New Service: Ethernet';
        insert asrList;
        system.debug('ASR>>'+asrList);
        
        List<Integration_Log__c> intLogList = new List<Integration_Log__c>();
        intLogList = TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Update ATOMS in SFDC',4);
        intLogList[0].Status__c = 'Success';
        intLogList[0].From__c = 'Neustar';
        intLogList[0].To__c = 'Salesforce';
        intLogList[0].Purchase_Ord_PON__c = purOrderList[0].PON__c;
        intLogList[0].PON__c = asrList[0].PON__c;
        intLogList[0].ASR__c = asrList[0].Id;
        intLogList[0].Pending_ATOMS_Update__c = true;
        intLogList[0].Pending_Salesforce_Update__c = true;
        intLogList[0].TP_Response_Type__c = 'TP CONFIRMED';
        intLogList[0].Request__c = '{"universalOrder":[],"response":[{"responseType":"TP CONFIRMED","sections":[{"sectionName":"Administration Section","fields":[{"fieldName":"CDTSENT","guiAliasName":"CD/TSENT","path":["CN_Form","cn_adminsection"],"value":"06-21-2020-0548PM"},{"fieldName":"CDTSENT","guiAliasName":"CD/TSENT","path":["CN_Form","cn_adminsection"],"value":"06-22-2099-0548PM"},{"fieldName":"PROJECT","guiAliasName":"PROJECT","path":["CN_Form","cn_adminsection"],"value":"TMO-EVCUPSEPT"},{"fieldName":"ASR_NO","guiAliasName":"ASR_NO","path":["CN_Form","cn_adminsection"],"value":"8773888"},{"fieldName":"DD","guiAliasName":"DD","path":["CN_Form","cn_adminsection"],"value":"09-09-2060"}],"index":1,"isEnabled":"y","subSections":[]},{"sectionName":"Service Section","fields":[{"fieldName":"ORD","guiAliasName":"ORD","path":["CN_Form","servicesection"],"value":"C2UL22SEPT"}],"index":1,"isEnabled":"y","subSections":[]},{"sectionName":"Circuit Section","fields":[],"index":1,"isEnabled":"y","subSections":[{"sectionName":"Virtual Circuit Section","fields":[{"fieldName":"VCID","guiAliasName":"VCID","path":["CN_Form","circuitsection","virtualcircuitsection"],"value":"AA.VLXW.04372SEPTT"},{"fieldName":"VCORD","guiAliasName":"VC ORD","path":["CN_Form","circuitsection","virtualcircuitsection"],"value":"C2UL7947"}],"index":1,"isEnabled":"y","subSections":[]}]}]}],"info":{"productCatalogName":"EVC PROTECTED CHANGE","productCatalogId":140951,"uomOrderKey":1459251,"uomOrderID":"TUOMUNI-N00025781","requestNumber":"5RA0737C-EVC1","ver":"01","orderStatus":"TP CONFIRMED","activityId":"C","activityType":"Change","application":"ASR_SEND","tradingPartner":"BAS","project":"TMO-EVCUPGRADE","owner":"no288020","createdDate":"2020-06-22 17:03:14.585","lastStatusChangeDate":"2020-06-22 17:52:22.117","sellerID":"NJ90","buyerID":"OPT","responseSeqId":2986}}';
        
        
        intLogList[1].Status__c = 'Failure';
        intLogList[1].Reprocess__c = true;
        intLogList[1].From__c = 'Neustar';
        intLogList[1].To__c = 'Salesforce';
        intLogList[1].TP_Response_Type__c = 'TP CONFIRMED';
        intLogList[1].Purchase_Ord_PON__c = purOrderList[0].PON__c;
        intLogList[1].PON__c = asrList[0].PON__c;
        intLogList[1].ASR__c = asrList[0].Id;
        intLogList[1].Pending_ATOMS_Update__c = true;
        intLogList[1].Pending_Salesforce_Update__c = false;
        intLogList[1].Request__c = '{"universalOrder":[],"response":[{"responseType":"TP CONFIRMED","sections":[{"sectionName":"Administration Section","fields":[{"fieldName":"CDTSENT","guiAliasName":"CD/TSENT","path":["CN_Form","cn_adminsection"],"value":"06-21-2020-0548PM"},{"fieldName":"CDTSENT","guiAliasName":"CD/TSENT","path":["CN_Form","cn_adminsection"],"value":"06-22-2099-0548PM"},{"fieldName":"PROJECT","guiAliasName":"PROJECT","path":["CN_Form","cn_adminsection"],"value":"TMO-EVCUPSEPT"},{"fieldName":"ASR_NO","guiAliasName":"ASR_NO","path":["CN_Form","cn_adminsection"],"value":"8773888"},{"fieldName":"DD","guiAliasName":"DD","path":["CN_Form","cn_adminsection"],"value":"09-09-2060"}],"index":1,"isEnabled":"y","subSections":[]},{"sectionName":"Service Section","fields":[{"fieldName":"ORD","guiAliasName":"ORD","path":["CN_Form","servicesection"],"value":"C2UL22SEPT"}],"index":1,"isEnabled":"y","subSections":[]},{"sectionName":"Circuit Section","fields":[],"index":1,"isEnabled":"y","subSections":[{"sectionName":"Virtual Circuit Section","fields":[{"fieldName":"VCID","guiAliasName":"VCID","path":["CN_Form","circuitsection","virtualcircuitsection"],"value":"AA.VLXW.04372SEPTT"},{"fieldName":"VCORD","guiAliasName":"VC ORD","path":["CN_Form","circuitsection","virtualcircuitsection"],"value":"C2UL7947"}],"index":1,"isEnabled":"y","subSections":[]}]}]}],"info":{"productCatalogName":"EVC PROTECTED CHANGE","productCatalogId":140951,"uomOrderKey":1459251,"uomOrderID":"TUOMUNI-N00025781","requestNumber":"5RA0737C-EVC1","ver":"01","orderStatus":"TP CONFIRMED","activityId":"C","activityType":"Change","application":"ASR_SEND","tradingPartner":"BAS","project":"TMO-EVCUPGRADE","owner":"no288020","createdDate":"2020-06-22 17:03:14.585","lastStatusChangeDate":"2020-06-22 17:52:22.117","sellerID":"NJ90","buyerID":"OPT","responseSeqId":2986}}';
        
        intLogList[2].Status__c = 'Failure';
        intLogList[2].Reprocess__c = true;
        intLogList[2].From__c = 'Neustar';
        intLogList[2].To__c = 'Salesforce';
        intLogList[2].API_Type__c = 'Get NNI';
        
        intLogList[2].Status__c = 'Failure';
        intLogList[2].Reprocess__c = true;
        intLogList[2].From__c = 'Neustar';
        intLogList[2].To__c = 'Salesforce';
        intLogList[2].API_Type__c = 'Add NNI';
        
        
        system.debug('intLogList>>'+intLogList);
        insert intLogList;
        
     
     }
     
     
    static testMethod void testAddCircuit(){
         Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
        //AtomsIntegrationAPIMock fakeResponseAdd = new AtomsIntegrationAPIMock (200,'{"Code":"SUCCESS","Details":"27F38009-2037-46AF-A6E1-1AEA87EE1F28"}');
        List<Circuit__c> circuitList = [SELECT Id, Case__c, API_Status__c, BAN__c, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c where Circuit_Type__c = 'Circuit: Ethernet'];
        List<Case> caseList = [Select Id, OM_Integration_Status__c from Case];
        
        caseList[0].OM_Integration_Status__c = 'Start Integration';
        update caseList;
        system.debug('caseList===>'+caseList); 
        system.debug('circuitList===>'+circuitList); 
       
        BatchForAtomNeustarIntergation obj = new BatchForAtomNeustarIntergation();
        DataBase.executeBatch(obj);  

        circuitList[0].Circuit_Type__c = 'Existing';
        circuitList[0].User_Ciruit_ID__c = '123456';
        update circuitList;
        //AtomsIntegrationAPIMock fakeResponseExisting = new AtomsIntegrationAPIMock (200,'{"Code":"SUCCESS","Details":"27F38009-2037-46AF-A6E1-1AEA87EE1F28"}');
         Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         BatchForAtomNeustarIntergation objExis = new BatchForAtomNeustarIntergation();
         DataBase.executeBatch(objExis); 
         AtomsIntegrationAPI.prepareJSON(circuitList,'Add Circuit', 'WNRTPAAH', '2CT4176A');
 
         Test.stopTest();
     }

    
    static testMethod void testASRAdd(){
         Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         //AtomsIntegrationAPIMock fakeResponse = new AtomsIntegrationAPIMock (200,'{"Code":"ERROR","Details":"ERROR : Supplied ASR Type not available for this circuit\rNumber : 51074\rSeverity : 16\rState : 1\rLine : 224\rProc : p_addASR\rUserMsg : Error in p_addASR\rSysMsg : Supplied ASR Type not available for this circuit"}');
         List<Circuit__c> circuitList = [SELECT Id, API_Status__c, Recordtype.Name, BAN__c, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c];
         List<ASR__c> asrLst =[Select id,Case__c,PON__c,Forecast_Version__c,Order_Number__c,Circuit_Id__r.Purchase_Order_Id__c,SubService_Type__c,Case__r.parent.PIER_Work_Order_ID__c,ATOMs_ASR_Id__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,Circuit_Id__c,Delivered_Date__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c, ASR_Type__c,Order_Destination__c from ASR__c where ASR_Type__c = 'New Service: Ethernet'];
         circuitList[0].API_Status__c = 'Successful';
         circuitList[0].Circuit_ID_Primary_Key__c = 'a668c556-9464-4452-a40d-f9579f456611';
         update circuitList;
        
         List<Case> caseList = [Select Id, OM_Integration_Status__c from Case];
         caseList[0].OM_Integration_Status__c = 'Circuit Success';
         update caseList;
        try{
         Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         BatchForAtomNeustarIntergation objAsr = new BatchForAtomNeustarIntergation();
         DataBase.executeBatch(objAsr);
        }catch(Exception ex){
            
        }  
         Test.stopTest();
     }
    
    
    static testMethod void updateCircuitToAtoms(){
        Map<Id,List<Circuit__c>> caseCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Integration_Log__c>> caseLogMap = new Map<Id,List<Integration_log__c>>();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         List<Circuit__c> circuitList = [SELECT Id, API_Status__c, BAN__c, Recordtype.Name, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c where Circuit_Type__c = 'Circuit: Ethernet'];
         circuitList[0].API_Status__c = 'Successful';
         circuitList[0].Circuit_ID_Primary_Key__c = 'a668c556-9464-4452-a40d-f9579f456611';
         update circuitList;
        
         List<Case> caseList = [Select Id, OM_Integration_Status__c from Case];
         caseCircuitMap.put(caseList[0].Id,circuitList);         
         
         AtomsIntegrationAPI.sendUpdateCircuitReqToAtoms(caseCircuitMap,caseLogMap);
             
         Test.stopTest();
     }
    
    static testMethod void updateCircuitToAtomsReprocess(){
        Map<Id,List<Circuit__c>> caseCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Integration_Log__c>> caseLogMap = new Map<Id,List<Integration_log__c>>();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         List<Integration_Log__c> intLog = [SELECT API_Call_Name__c,API_Order__c,API_Type__c,ASR__c,Attachment_Id__c,Attachment_Name__c,Circuit__c,CreatedById,CreatedDate,DWH_Task_Status__c,Error_Log__c,Error_Type__c,Failure_Reason__c,From__c,Id,Intake_Request__c,IsDeleted,Is_Combo_Order__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Location__c,Name,OwnerId,Parent__c,Pending_ATOMS_Update__c,Pending_Salesforce_Update__c,PON__c,Purchase_Order__c,Purchase_Ord_PON__c,Reprocess__c,Request__c,Response_Message__c,Response__c,Status__c,SystemModstamp,Task_Number__c,Task_Status__c,Task__c,To__c,TP_Response_Type__c,Type_of_Order__c,Workflow_Template_Task__c,Worklog_Description__c,WorkLog_Id__c,Work_Order__c FROM Integration_Log__c];
         List<Case> caseList = [Select Id, OM_Integration_Status__c from Case];
         caseLogMap.put(caseList[0].Id,intLog);         
         
         AtomsIntegrationAPI.sendUpdateCircuitReqToAtoms(caseCircuitMap,caseLogMap);
             
         Test.stopTest();
     }
    
     static testMethod void addExistingNNITest(){
        Map<Id,List<Circuit__c>> caseCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Integration_Log__c>> caseLogMap = new Map<Id,List<Integration_log__c>>();
         Test.startTest();
         Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         List<Circuit__c> circuitList = [SELECT Id, Billable_NNI__c, A_Site_ID__c, Z_Site_ID__c, Vendor_Name_Formula__c, API_Status__c, BAN__c, Recordtype.Name, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c where Circuit_Type__c = 'NNI'];
         system.debug('circuitList===>'+circuitList);
         circuitList[0].User_NNI_ID__c = 'testnni';
         update circuitList[0];
         List<Integration_Log__c> intLog = [SELECT API_Call_Name__c,API_Order__c,API_Type__c,ASR__c,Attachment_Id__c,Attachment_Name__c,Circuit__c,CreatedById,CreatedDate,DWH_Task_Status__c,Error_Log__c,Error_Type__c,Failure_Reason__c,From__c,Id,Intake_Request__c,IsDeleted,Is_Combo_Order__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Location__c,Name,OwnerId,Parent__c,Pending_ATOMS_Update__c,Pending_Salesforce_Update__c,PON__c,Purchase_Order__c,Purchase_Ord_PON__c,Reprocess__c,Request__c,Response_Message__c,Response__c,Status__c,SystemModstamp,Task_Number__c,Task_Status__c,Task__c,To__c,TP_Response_Type__c,Type_of_Order__c,Workflow_Template_Task__c,Worklog_Description__c,WorkLog_Id__c,Work_Order__c FROM Integration_Log__c where API_TYPE__C ='Get NNI'];
         AtomsIntegrationAPI.addExistingNNI2Atoms(circuitList[0],intLog);
         circuitList[0].Circuit_Type__c = 'Existing';
         update circuitList[0];
         BatchForAtomNeustarIntergation objExis = new BatchForAtomNeustarIntergation();
         DataBase.executeBatch(objExis); 
         Test.stopTest();
     }
    
    static testMethod void GetaddNNITest(){
        Map<Id,List<Circuit__c>> caseCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Integration_Log__c>> caseLogMap = new Map<Id,List<Integration_log__c>>();
         Test.startTest();
            Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
            List<Circuit__c> circuitList = [SELECT Vendor_ID_Primary_Key__c, User_NNI_ID__c, Comments__c, ESP__c, NNI_ID_Primary_Key__c, NNI_Status__c, Site_Market__c, Site_Region__c, Site_Type__c, User_Site_Id__c,Id,A_Site_ID__r.Name, Z_Site_ID__r.Name,
                                         Billable_NNI__c, A_Site_ID__c, Z_Site_ID__c, Vendor_Name_Formula__c, API_Status__c, BAN__c, Recordtype.Name, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,
                                         User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c where Circuit_Type__c = 'NNI'];
            List<Integration_Log__c> intLog = [SELECT API_Call_Name__c,API_Order__c,API_Type__c,ASR__c,Attachment_Id__c,Attachment_Name__c,Circuit__c,CreatedById,CreatedDate,DWH_Task_Status__c,Error_Log__c,Error_Type__c,Failure_Reason__c,From__c,Id,Intake_Request__c,IsDeleted,Is_Combo_Order__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Location__c,Name,OwnerId,Parent__c,Pending_ATOMS_Update__c,Pending_Salesforce_Update__c,PON__c,Purchase_Order__c,Purchase_Ord_PON__c,Reprocess__c,Request__c,Response_Message__c,Response__c,Status__c,SystemModstamp,Task_Number__c,Task_Status__c,Task__c,To__c,TP_Response_Type__c,Type_of_Order__c,Workflow_Template_Task__c,Worklog_Description__c,WorkLog_Id__c,Work_Order__c FROM Integration_Log__c where API_TYPE__C ='Get NNI'];AtomsIntegrationAPI.addNNI2Atoms(circuitList[0],intLog); 
            circuitList[0].User_NNI_ID__c = '';
            AtomsIntegrationAPI.addExistingNNI2Atoms(circuitList[0],intLog); 
        
            circuitList[0].User_NNI_ID__c = 'ABCD';
            AtomsIntegrationAPI.addExistingNNI2Atoms(circuitList[0],intLog); 
             
         Test.stopTest(); 
     }
    
    static testMethod void addNNITest(){
        Map<Id,List<Circuit__c>> caseCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Integration_Log__c>> caseLogMap = new Map<Id,List<Integration_log__c>>();
         Test.startTest();
            Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
            List<Circuit__c> circuitList = [SELECT Vendor_ID_Primary_Key__c, User_NNI_ID__c, Comments__c, ESP__c, NNI_ID_Primary_Key__c, NNI_Status__c, Site_Market__c, Site_Region__c, Site_Type__c, User_Site_Id__c,Id,A_Site_ID__r.Name, Z_Site_ID__r.Name,
                                         Billable_NNI__c, A_Site_ID__c, Z_Site_ID__c, Vendor_Name_Formula__c, API_Status__c, BAN__c, Recordtype.Name, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,
                                         User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c where Circuit_Type__c = 'NNI'];
            List<Integration_Log__c> intLog = [SELECT API_Call_Name__c,API_Order__c,API_Type__c,ASR__c,Attachment_Id__c,Attachment_Name__c,Circuit__c,CreatedById,CreatedDate,DWH_Task_Status__c,Error_Log__c,Error_Type__c,Failure_Reason__c,From__c,Id,Intake_Request__c,IsDeleted,Is_Combo_Order__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Location__c,Name,OwnerId,Parent__c,Pending_ATOMS_Update__c,Pending_Salesforce_Update__c,PON__c,Purchase_Order__c,Purchase_Ord_PON__c,Reprocess__c,Request__c,Response_Message__c,Response__c,Status__c,SystemModstamp,Task_Number__c,Task_Status__c,Task__c,To__c,TP_Response_Type__c,Type_of_Order__c,Workflow_Template_Task__c,Worklog_Description__c,WorkLog_Id__c,Work_Order__c FROM Integration_Log__c where API_TYPE__C ='Get NNI'];AtomsIntegrationAPI.addNNI2Atoms(circuitList[0],intLog); 
            circuitList[0].User_NNI_ID__c = '';
            AtomsIntegrationAPI.addNNI2Atoms(circuitList[0],intLog); 
             
         Test.stopTest(); 
     }
    
    static testMethod void updateNNIAtomsTEST(){
        Map<Id,List<Circuit__c>> caseCircuitMap = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Integration_Log__c>> caseLogMap = new Map<Id,List<Integration_log__c>>();
         Test.startTest();
         Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         List<Circuit__c> circuitList = [SELECT Id, Vendor_ID_Primary_Key__c, NNI_ID_Primary_Key__c,  Billable_NNI__c, A_Site_ID__c, Z_Site_ID__c, Vendor_Name_Formula__c, BAN__c, Recordtype.Name, Bandwidth__c, Category__c,Controlling_Site__c, LOA_Provided__c,User_Ciruit_ID__c,Billing_Circuit_Id__c,Ordering_Guide_ID__c,Circuit_ID_Primary_Key__c,  Circuit_Type__c from Circuit__c where Circuit_Type__c = 'NNI'];
         circuitList[0].API_Status__c = 'Successful';
         circuitList[0].Circuit_ID_Primary_Key__c = 'a668c556-9464-4452-a40d-f9579f456611';
         circuitList[0].NNI_ID_Primary_Key__c = 'a668c556-9464-4452-a40d-f9579f456611';
         circuitList[0].User_NNI_ID__c = 'aa'; 
         circuitList[0].Comments__c='kk';

         update circuitList;
        
         List<Case> caseList = [Select Id, OM_Integration_Status__c from Case];
         caseCircuitMap.put(caseList[0].Id,circuitList);         
         
         AtomsIntegrationAPI.sendUpdateCircuitReqToAtoms(caseCircuitMap,caseLogMap);
             
         Test.stopTest(); 
     }
    
    static testMethod void randomWrapTest(){
        AtomsIntegrationAPI.AtomsResponseWrapper obj1 = new AtomsIntegrationAPI.AtomsResponseWrapper();
        obj1.code  = '';
        obj1.details  = '';
        obj1.ErrMsg = '';
        obj1.ErrType  = '';
        obj1.bSuccess  = '';
        
        AtomsIntegrationAPI.AtomsUpdateWrapper obj2 = new AtomsIntegrationAPI.AtomsUpdateWrapper();
        obj2.circuitId  = '';
        obj2.billCircID  = '';
        
        AtomsIntegrationAPI.GetCircuit obj3 =  new AtomsIntegrationAPI.GetCircuit();
        obj3.circuitID  = '';
        obj3.userCircID  = '';
        obj3.bandwidth  = '';
        obj3.category  = '';
        obj3.vendor  = '';
        obj3.status  = '';
        obj3.svcTypeCd  = '';
        obj3.A_SiteID  = '';
        obj3.Z_SiteID  = '';
        obj3.orderingGuideID  = '';
        
        AtomsIntegrationAPI.GetCircuitResponseWrapper obj4 = new AtomsIntegrationAPI.GetCircuitResponseWrapper();
        obj4.totalCount  = '';
        obj4.result = new List<AtomsIntegrationAPI.GetCircuit>();
        
        AtomsIntegrationAPI.GetAtomsCalloutReponse obj5 = new AtomsIntegrationAPI.GetAtomsCalloutReponse();
        obj5.IdStr = '';
        obj5.log = new Integration_Log__c();
        
        AtomsIntegrationAPI.GetCalloutResponseForCircuit obj6 = new AtomsIntegrationAPI.GetCalloutResponseForCircuit();
        obj6.a_SiteAssociationStatus = '';
        obj6.z_SiteAssociationStatus = '';
        obj6.og_id = '';
        obj6.vendorId = '';
        
        AtomsIntegrationAPI obj7 = new AtomsIntegrationAPI();
        obj7.caseId = null;
    }
}
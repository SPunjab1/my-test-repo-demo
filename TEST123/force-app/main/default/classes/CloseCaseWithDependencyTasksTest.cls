@isTest
Private  class CloseCaseWithDependencyTasksTest {
    
    @isTest
    static void closeChildCasesmtdtest(){
        String workflowTmpName ='AAV CIR Retest';
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();  
        List<Case> caseList = TestDataFactory.createCase(workflowTmpName,1,recTypeId); 
        Insert caseList;   
        
        Work_Order_Task_Dependency__c woTaskDependentObj = new Work_Order_Task_Dependency__c();
        woTaskDependentObj.Dependent_Child__c = caseList[0].id;
        insert woTaskDependentObj;
        
        Test.startTest();
            CloseCaseWithDependencyTasks.closeChildCasesmtd(new List<Id>{caseList[0].id});
        Test.stopTest();

        Case caseObj = [Select Id,Status From Case Where Id = :caseList[0].id Limit 1];
        System.assertEquals(caseObj.Status ,'Closed', 'Case Status set to be Closed');

        Work_Order_Task_Dependency__c wotDependetObj = [Select Id,Invalid_Dependency__c From Work_Order_Task_Dependency__c Where Id = :woTaskDependentObj.id Limit 1];
        System.assertEquals(wotDependetObj.Invalid_Dependency__c ,true, 'Invalid Dependency field must be set to True for the task closure');

    }
}
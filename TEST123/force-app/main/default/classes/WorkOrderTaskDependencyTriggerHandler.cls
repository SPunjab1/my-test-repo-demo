/*********************************************************************************************
@author     : IBM
@date       : 31.05.19 
@description: This is the trigger handler for WorkOrder Task Dependency object.
@Version    : 1.0           
**********************************************************************************************/
public with sharing class WorkOrderTaskDependencyTriggerHandler extends TriggerHandler { 
    private static final string WTD_API = 'Work_Order_Task_Dependency__c';      
    public WorkOrderTaskDependencyTriggerHandler() {
        super(WTD_API);
    }
/***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/    
    public override String getName() {return WTD_API;
    }
/***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems) {
        /*********Method call to update the Parent Task*************/   
        wtdParentTaskupdate(newItems);
    }
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
    }
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {
    }    
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
    }   
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) { 
        /*********Method call to Update SLA Stop*************/   
        stopSLAFieldUpdate(newItems);
    }       
    
/*********************************************************************************************
* @Description :Method to create Work Order Task automatically based on workflow template
* @input params : Trigger.New
**********************************************************************************************/       
    public void wtdParentTaskupdate(List<Sobject> newWTDList)
    {
        List<Work_Order_Task_Dependency__c> newItems= new List<Work_Order_Task_Dependency__c>();
        newItems = (List<Work_Order_Task_Dependency__c>) newWTDList;
        List<Work_Order_Task_Dependency__c> wtdUpdateList= new List<Work_Order_Task_Dependency__c>();
        Set<Id> taskIdList = new Set<Id>();
        List<Id> woIdList = new List<Id>(); 
        Map<String,Map<Decimal,ID>>taskNoUpdateMap=new Map<String,Map<Decimal,ID>>();           
        string key=null; 
        string key1=null;
        for(Work_Order_Task_Dependency__c wt :newItems){
            taskIdList.add(wt.Work_Order__c);                            
            wtdUpdateList.add(wt);                          
            
        }  
        if(taskIdList!=null && taskIdList.size()>0){
            Map<Id,Case> caseMap = new Map<Id,Case>([Select id,Task_Number__c,ParentId from Case where ParentId IN:taskIdList]);
            for(Case cs: caseMap.values()){ 
                key=String.Valueof(cs.ParentId) + String.valueof(cs.Task_Number__c);
                if(!taskNoUpdateMap.containskey(key)){
                    taskNoUpdateMap.put(key,new Map<Decimal,Id>{cs.Task_Number__c=>cs.Id});                       
                }
                else{ taskNoUpdateMap.get(key).put(cs.Task_Number__c,cs.Id);  
                }                              
            }
        }  
        if(wtdUpdateList!=null && wtdUpdateList.size()>0 &&taskNoUpdateMap.size()>0 && taskNoUpdateMap!=null){
            for(Work_Order_Task_Dependency__c wtd : wtdUpdateList){
                if(wtd.Dependent_Parent__c==null && wtd.Work_Order__c!=null && wtd.Dependent_Task_Number__c!=null){
                    key1=String.valueof(wtd.Work_Order__c) + String.valueof(wtd.Dependent_Task_Number__c);
                       if(taskNoUpdateMap.get(key1)!=null && taskNoUpdateMap.get(key1).get(wtd.Dependent_Task_Number__c) != null){ 
                        wtd.Dependent_Parent__c = taskNoUpdateMap.get(key1).get(wtd.Dependent_Task_Number__c);
                    }
                  
                }
            }
        }
    }
    
/*********************************************************************************************
* @Description :Method to Update SLA Stop
* @input params : Trigger.New
**********************************************************************************************/       
    public void stopSLAFieldUpdate(List<Sobject> newWTDList)
    {
        List<Work_Order_Task_Dependency__c> newItems= new List<Work_Order_Task_Dependency__c>();
        newItems = (List<Work_Order_Task_Dependency__c>) newWTDList;
        List<Case> workOrderList = new List<Case>();
        List<Case> workOrderListUpdate = new List<Case>();
        Set<Id> parentIdSet = new Set<Id>();
        for(Work_Order_Task_Dependency__c wt :newItems){
            parentIdSet.add(wt.Work_Order__c);                          
        } 
        if(parentIdSet!=null && parentIdSet.size()>0){
            workOrderList =[Select id, Stop_Work_Order_Task_SLA__c FROM CASE WHERE Id IN :parentIdSet];
        }
        if(workOrderList!=null && workOrderList.size()>0){
            for(Case cs : workOrderList){
                if(cs.Stop_Work_Order_Task_SLA__c!=true){
                    cs.Stop_Work_Order_Task_SLA__c = true;
                    workOrderListUpdate.add(cs);
                }       
            }
        }
        try{
            if(workOrderListUpdate!=null && workOrderListUpdate.size()>0 && !CaseTriggerHandler.isCaseTriggerExecuted){database.update(workOrderListUpdate,true);
            }
        }
        catch(Exception ex){throw ex;
        }
    }
    
}
/******************************************************************************
@author      - IBM
@date        - 31.07.2020
@description - To fire case assignment rule from opportunity process builder
@Version     - 1.0
*******************************************************************************/
public with sharing class SI_CaseAssignmentRule {
    
    /**************************************************************************
    * @description Method to fire case assignment rule
    * @param lstOppId - List<Id> opportunity 
    **************************************************************************/
    @invocableMethod
    public static void fireCaseAssignmentRuleFromOpportunity(List<Id> lstOppId) {
        //Intialize list to hold Opportunity
        List<Case> listOfCasesRelatedToOppty = new List<Case>();
        // Iterate through opportunity to get all cases
        for(Opportunity opp : [SELECT Id, 
                               (SELECT Id, Status, Single_Intake_Team__c 
                                FROM Cases__r) 
                               FROM Opportunity 
                               WHERE Id IN :lstOppId]) {
            
            listOfCasesRelatedToOppty.addAll(opp.cases__r);
        }
        if(!listOfCasesRelatedToOppty.isEmpty()) {
            //Initialize case list 
            List<Case> listOfCaseToUpdate = new List<Case>();
            //0: Closed - Complete, 1: Closed - Cancelled, 2: OM Ready, 3: Update Database, 4: Database Updated, 
            //5: No Database Action Required, 6: In Progress, 7: RFP In Progress
            List<String> listOfStatusToCompare = new List<String>(Label.SI_CaseStatus.split(','));
            //Creating the DMLOptions for active assignment rules
            Database.DMLOptions dmlOpts = SI_UtilityClass.setDmlOptionForAssignmentRules();
            // Iterate through case list 
            for(Case caseRec : listOfCasesRelatedToOppty) {
                //check if status is not blank and valid status,
                //update case status and set dmlOptions to fire assignment rules
                if(String.isNotBlank(caseRec.Status)
                   && listOfStatusToCompare[2].equals(caseRec.Status) 
                   && String.isNotBlank(caseRec.Single_Intake_Team__c)) {
                    caseRec.setOptions(dmlOpts); 
                    listOfCaseToUpdate.add(caseRec); 
                }
            }
            //if listOfCaseToUpdate is not empty, update cases
            if(!listOfCaseToUpdate.isEmpty()) {
                //Label.SI_DML_Operations.split(';')[1] - Update Operation
                SI_UtilityClass.callDMLOperations(listOfCaseToUpdate, Label.SI_DML_Operations.split(';')[1]);
            }
        }
    }
}
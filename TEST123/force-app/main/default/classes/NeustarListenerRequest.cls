public class NeustarListenerRequest {

	public class Fields {
		public String fieldName {get;set;} 
		public String guiAliasName {get;set;} 
		public List<String> path {get;set;} 
		public String value {get;set;} 
	}

	public virtual class SubSections {
		public String sectionName {get;set;} 
		public List<Fields> fields {get;set;} 
		public Integer index {get;set;} 
		public String isEnabled {get;set;} 
	}

	public class Sections extends SubSections {
		public List<SubSections> subSections {get;set;} 
	}

	public class UniversalOrder {
	}

	public class Response {
		public String responseType {get;set;} 
		public List<Sections> sections {get;set;} 
	}

	public class Info {
		public String productCatalogName {get;set;} 
		public Integer productCatalogId {get;set;} 
		public Integer uomOrderKey {get;set;} 
		public String uomOrderID {get;set;} 
		public String requestNumber {get;set;} 
		public String ver {get;set;} 
		public String orderStatus {get;set;} 
		public String activityId {get;set;} 
		public String activityType {get;set;} 
		public String application {get;set;} 
		public String tradingPartner {get;set;} 
		public String owner {get;set;} 
		public String createdDate {get;set;} 
		public String lastStatusChangeDate {get;set;} 
		public String sellerID {get;set;} 
		public String buyerID {get;set;} 
		public String transactionId {get;set;} 
		public Integer responseSeqId {get;set;} 
	}

	public List<UniversalOrder> universalOrder {get;set;} 
	public List<Response> response {get;set;} 
	public List<Sections> productOrder {get;set;} 
	public Info info {get;set;} 
	
	public static NeustarListenerRequest parse(String json) {
		return (NeustarListenerRequest) System.JSON.deserialize(json, NeustarListenerRequest.class);
	}
    
    public static NeustarListenerRequest clone(NeustarListenerRequest originalRequest) {
		return (NeustarListenerRequest) System.JSON.deserialize( JSON.serialize(originalRequest), NeustarListenerRequest.class);
	}
    
    public static Map<String, NeustarListenerRequest> slice(NeustarListenerRequest originalRequest) {
        
        Map<String, NeustarListenerRequest> mapRet = new Map<String, NeustarListenerRequest>();
        
        if( originalRequest.response == null || originalRequest.response.isEmpty()){
            
            return mapRet;
        }
        
        if(originalRequest.response.size() == 1){
            mapRet.put('simple', originalRequest);
            return mapRet;
        }
        
        List<Response> responseList = originalRequest.response;
        
        NeustarListenerRequest evcReq = NeustarListenerRequest.clone( originalRequest);
        NeustarListenerRequest uniReq = NeustarListenerRequest.clone( originalRequest);
        
        for(Response response : responseList){
            
            if(String.isBlank(response.responseType)){
                continue;
            }
            
            Response tempResponse = (Response) System.JSON.deserialize(JSON.serialize(response), Response.class);
            if(response.responseType.startsWithIgnoreCase('EVC - ')){
                evcReq.Response = new List<Response>{ tempResponse};
            }
            else{
                uniReq.Response = new List<Response>{ tempResponse};
            }
        }
        
        mapRet.put('evc', evcReq);
        mapRet.put('uni', uniReq);
        
		return mapRet;
	}
    
    public static NeustarListenerRequest testParse() {
		String json=		'{'+
		'  "universalOrder":['+
		'    '+
		'  ],'+
		'  "response":['+
		'  {'+
		'			"responseType": "PORT - TP COMPLETE",'+
		'			"sections": ['+
		'				{'+
		'					"sectionName": "Administration Section",'+
		'					"fields": ['+
		'						{'+
		'							"fieldName": "CCNA",'+
		'							"guiAliasName": "CCNA",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "WCG"'+
		'						},'+
		'						{'+
		'							"fieldName": "DTSENT",'+
		'							"guiAliasName": "D/TSENT",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "04-30-2020-0337PM"'+
		'						},'+
		'						{'+
		'							"fieldName": "INIT",'+
		'							"guiAliasName": "INIT",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "TEST FERRARO"'+
		'						},'+
		'						{'+
		'							"fieldName": "INIT_TELNO",'+
		'							"guiAliasName": "TEL NO",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "999-999-9999"'+
		'						},'+
		'						{'+
		'							"fieldName": "ICSC",'+
		'							"guiAliasName": "ICSC",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "SW06"'+
		'						},'+
		'						{'+
		'							"fieldName": "AP_REP",'+
		'							"guiAliasName": "AP REP",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "CRYSY"'+
		'						},'+
		'						{'+
		'							"fieldName": "AP_REP_TELNO",'+
		'							"guiAliasName": "AP REP TEL",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "999-356-1351"'+
		'						},'+
		'						{'+
		'							"fieldName": "PON",'+
		'							"guiAliasName": "PON",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "VG08184A-RHMUNI"'+
		'						},'+
		'						{'+
		'							"fieldName": "VER",'+
		'							"guiAliasName": "VER",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_adminsection"'+
		'							],'+
		'							"value": "01"'+
		'						}'+
		'					],'+
		'					"index": 1,'+
		'					"isEnabled": "y",'+
		'					"subSections": []'+
		'				},'+
		'				{'+
		'					"sectionName": "Details Section",'+
		'					"fields": ['+
		'						{'+
		'							"fieldName": "CNT",'+
		'							"guiAliasName": "CNT",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_detailsection"'+
		'							],'+
		'							"value": "A"'+
		'						},'+
		'						{'+
		'							"fieldName": "CNR_VER",'+
		'							"guiAliasName": "C/NR VER",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_detailsection"'+
		'							],'+
		'							"value": "01"'+
		'						},'+
		'						{'+
		'							"fieldName": "CD",'+
		'							"guiAliasName": "CD",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_detailsection"'+
		'							],'+
		'							"value": "04-30-2020"'+
		'						},'+
		'						{'+
		'							"fieldName": "REMARKS",'+
		'							"guiAliasName": "REMARKS",'+
		'							"path": ['+
		'								"CNR_Form",'+
		'								"cnr_detailsection"'+
		'							],'+
		'							"value": "PORT PON VG08184A-RHMUNI "'+
		'						}'+
		'					],'+
		'					"index": 1,'+
		'					"isEnabled": "y",'+
		'					"subSections": []'+
		'				}'+
		'			]'+
		'		},'+
		'    {'+
		'      "responseType":"EVC - TP COMPLETE",'+
		'      "sections":['+
		'        {'+
		'          "sectionName":"Administration Section",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"CCNA",'+
		'              "guiAliasName":"CCNA",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"WCG"'+
		'            },'+
		'            {'+
		'              "fieldName":"ICSC",'+
		'              "guiAliasName":"ICSC",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"SW80"'+
		'            },'+
		'            {'+
		'              "fieldName":"PON",'+
		'              "guiAliasName":"PON",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"VG08184A-RHMEVC"'+
		'            },'+
		'            {'+
		'              "fieldName":"VER",'+
		'              "guiAliasName":"VER",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"01"'+
		'            },'+
		'            {'+
		'              "fieldName":"DTSENT",'+
		'              "guiAliasName":"D\\/TSENT",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"07-27-2020-0855PM"'+
		'            },'+
		'            {'+
		'              "fieldName":"INIT",'+
		'              "guiAliasName":"INIT",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"WENDY COOK"'+
		'            },'+
		'            {'+
		'              "fieldName":"INIT_TELNO",'+
		'              "guiAliasName":"TEL NO",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"425-302-5876"'+
		'            },'+
		'            {'+
		'              "fieldName":"AP_REP",'+
		'              "guiAliasName":"AP REP",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"BETTY ESTAVER"'+
		'            },'+
		'            {'+
		'              "fieldName":"AP_REP_EMAIL",'+
		'              "guiAliasName":"EMAIL",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"BETTY.ESTAVER@COX.COM"'+
		'            },'+
		'            {'+
		'              "fieldName":"AP_REP_TELNO",'+
		'              "guiAliasName":"AP REP TEL",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_adminsection"'+
		'              ],'+
		'              "value":"702-545-1952"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y",'+
		'          "subSections":['+
		'            '+
		'          ]'+
		'        },'+
		'        {'+
		'          "sectionName":"Details Section",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"CNT",'+
		'              "guiAliasName":"CNT",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_detailsection"'+
		'              ],'+
		'              "value":"A"'+
		'            },'+
		'            {'+
		'              "fieldName":"CNR_VER",'+
		'              "guiAliasName":"C\\/NR VER",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_detailsection"'+
		'              ],'+
		'              "value":"01"'+
		'            },'+
		'            {'+
		'              "fieldName":"CD",'+
		'              "guiAliasName":"CD",'+
		'              "path":['+
		'                "CNR_Form",'+
		'                "cnr_detailsection"'+
		'              ],'+
		'              "value":"07-23-2020"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y",'+
		'          "subSections":['+
		'            '+
		'          ]'+
		'        }'+
		'      ]'+
		'    }'+
		'  ],'+
		'  "productOrder":['+
		'    {'+
		'      "sectionName":"Base",'+
		'      "fields":['+
		'        {'+
		'          "fieldName":"CCNA",'+
		'          "guiAliasName":"CCNA",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"WCG"'+
		'        },'+
		'        {'+
		'          "fieldName":"PON",'+
		'          "guiAliasName":"PON",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"VG08142A-RHM0088"'+
		'        },'+
		'        {'+
		'          "fieldName":"ICSC",'+
		'          "guiAliasName":"ICSC",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"SW80"'+
		'        },'+
		'        {'+
		'          "fieldName":"DTSENT",'+
		'          "guiAliasName":"D\\/TSENT",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"06-19-2020-0309PM"'+
		'        },'+
		'        {'+
		'          "fieldName":"REQTYP",'+
		'          "guiAliasName":"REQTYP",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"SD"'+
		'        },'+
		'        {'+
		'          "fieldName":"DDD",'+
		'          "guiAliasName":"DDD",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"07-16-2020"'+
		'        },'+
		'        {'+
		'          "fieldName":"RTR",'+
		'          "guiAliasName":"RTR",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"F = Send FOC Only"'+
		'        },'+
		'        {'+
		'          "fieldName":"BAN",'+
		'          "guiAliasName":"BAN",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"E = Existing BAN"'+
		'        },'+
		'        {'+
		'          "fieldName":"CUST",'+
		'          "guiAliasName":"CUS",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"T-MOBILE"'+
		'        }'+
		'      ],'+
		'      "index":1,'+
		'      "isEnabled":"y"'+
		'    },'+
		'    {'+
		'      "sectionName":"GENERAL ORDERING DETAILS",'+
		'      "fields":['+
		'        {'+
		'          "fieldName":"EVCI",'+
		'          "guiAliasName":"EVCI",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"A = Stand Alone EVC"'+
		'        },'+
		'        {'+
		'          "fieldName":"QTY",'+
		'          "guiAliasName":"QTY",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"1"'+
		'        },'+
		'        {'+
		'          "fieldName":"UNIT",'+
		'          "guiAliasName":"UNIT",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"C = Number of lines, trunks, facilities, circuits, CCS links, ring segments or unbundled elements"'+
		'        },'+
		'        {'+
		'          "fieldName":"PIU",'+
		'          "guiAliasName":"PIU",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"100"'+
		'        },'+
		'        {'+
		'          "fieldName":"EDA",'+
		'          "guiAliasName":"EDA",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"EXP",'+
		'          "guiAliasName":"EXP",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"PROJECT",'+
		'          "guiAliasName":"PROJECT",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"RPON",'+
		'          "guiAliasName":"RPON",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"VG08142A-RHM2088"'+
		'        },'+
		'        {'+
		'          "fieldName":"LANM",'+
		'          "guiAliasName":"LANM",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"LADATED",'+
		'          "guiAliasName":"LADATED",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"LA",'+
		'          "guiAliasName":"LA",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"REMARKS",'+
		'          "guiAliasName":"REMARKS",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_adminsection"'+
		'          ],'+
		'          "value":"DISCONNECT FOR EXISTING EVC 33.VLXX.070400..COXC AND 33.VLXX.072084..COXC WILL BE ISSUED AFTER COMPLETION"'+
		'        }'+
		'      ],'+
		'      "index":1,'+
		'      "isEnabled":"y",'+
		'      "subSections":['+
		'        '+
		'      ]'+
		'    },'+
		'    {'+
		'      "sectionName":"BILLING DETAILS",'+
		'      "fields":['+
		'        {'+
		'          "fieldName":"ACNA",'+
		'          "guiAliasName":"ACNA",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_billsection"'+
		'          ],'+
		'          "value":"WCG"'+
		'        },'+
		'        {'+
		'          "fieldName":"FUSF",'+
		'          "guiAliasName":"FUSF",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_billsection"'+
		'          ],'+
		'          "value":"E = Exempt"'+
		'        },'+
		'        {'+
		'          "fieldName":"VTA",'+
		'          "guiAliasName":"VTA",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_billsection"'+
		'          ],'+
		'          "value":"48"'+
		'        },'+
		'        {'+
		'          "fieldName":"PNUM",'+
		'          "guiAliasName":"PNUM",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_billsection"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"TE",'+
		'          "guiAliasName":"TE",'+
		'          "path":['+
		'            "ASR_Form",'+
		'            "asr_billsection"'+
		'          ]'+
		'        }'+
		'      ],'+
		'      "index":1,'+
		'      "isEnabled":"y",'+
		'      "subSections":['+
		'        '+
		'      ]'+
		'    },'+
		'    {'+
		'      "sectionName":"VIRTUAL CIRCUIT\\/EVC DETAILS",'+
		'      "fields":['+
		'        {'+
		'          "fieldName":"EVCNUM",'+
		'          "guiAliasName":"EVC NUM",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_evcdetail"'+
		'          ],'+
		'          "value":"0001"'+
		'        },'+
		'        {'+
		'          "fieldName":"NUT",'+
		'          "guiAliasName":"NUT",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_evcdetail"'+
		'          ],'+
		'          "value":"02"'+
		'        },'+
		'        {'+
		'          "fieldName":"NC",'+
		'          "guiAliasName":"NC",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_evcdetail"'+
		'          ],'+
		'          "value":"VLP-"'+
		'        },'+
		'        {'+
		'          "fieldName":"EVCCKR",'+
		'          "guiAliasName":"EVCCKR",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_evcdetail"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"EVCID",'+
		'          "guiAliasName":"EVCID",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_evcdetail"'+
		'          ]'+
		'        }'+
		'      ],'+
		'      "index":1,'+
		'      "isEnabled":"y",'+
		'      "subSections":['+
		'        '+
		'      ]'+
		'    },'+
		'    {'+
		'      "sectionName":"UNI DETAILS",'+
		'      "fields":['+
		'        {'+
		'          "fieldName":"UREF",'+
		'          "guiAliasName":"UREF",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"01"'+
		'        },'+
		'        {'+
		'          "fieldName":"EI",'+
		'          "guiAliasName":"ENNI",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"UACT",'+
		'          "guiAliasName":"UACT",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"N = New"'+
		'        },'+
		'        {'+
		'          "fieldName":"NCI",'+
		'          "guiAliasName":"NCI",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"02VLN.V"'+
		'        },'+
		'        {'+
		'          "fieldName":"RUID",'+
		'          "guiAliasName":"RUID",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"33.KJGD.000003...COXC"'+
		'        },'+
		'        {'+
		'          "fieldName":"RPON",'+
		'          "guiAliasName":"RPON",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"EVCSP",'+
		'          "guiAliasName":"EVCSP",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ]'+
		'        }'+
		'      ],'+
		'      "index":1,'+
		'      "isEnabled":"y",'+
		'      "subSections":['+
		'        {'+
		'          "sectionName":"LREF\\/LEVEL OF SERVICE DETAILS",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"LREF",'+
		'              "guiAliasName":"LREF",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"1"'+
		'            },'+
		'            {'+
		'              "fieldName":"LOSACT",'+
		'              "guiAliasName":"LOSACT",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"N = New"'+
		'            },'+
		'            {'+
		'              "fieldName":"LOS",'+
		'              "guiAliasName":"LOS",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"REAL TIME"'+
		'            },'+
		'            {'+
		'              "fieldName":"BDW",'+
		'              "guiAliasName":"BDW",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"1000M"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y",'+
		'          "subSections":['+
		'            '+
		'          ]'+
		'        },'+
		'        {'+
		'          "sectionName":"VLAN DETAILS",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"VACT",'+
		'              "guiAliasName":"VACT",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "CE_VLANdetail"'+
		'              ],'+
		'              "value":"N = New"'+
		'            },'+
		'            {'+
		'              "fieldName":"CE_VLAN",'+
		'              "guiAliasName":"CE-VLAN",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "CE_VLANdetail"'+
		'              ],'+
		'              "value":"0088"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y",'+
		'          "subSections":['+
		'            '+
		'          ]'+
		'        }'+
		'      ]'+
		'    },'+
		'    {'+
		'      "sectionName":"UNI DETAILS",'+
		'      "fields":['+
		'        {'+
		'          "fieldName":"UREF",'+
		'          "guiAliasName":"UREF",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"02"'+
		'        },'+
		'        {'+
		'          "fieldName":"EI",'+
		'          "guiAliasName":"ENNI",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"Y = Indicates that the UREF is an ENNI"'+
		'        },'+
		'        {'+
		'          "fieldName":"UACT",'+
		'          "guiAliasName":"UACT",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"N = New"'+
		'        },'+
		'        {'+
		'          "fieldName":"NCI",'+
		'          "guiAliasName":"NCI",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"02VLN.V"'+
		'        },'+
		'        {'+
		'          "fieldName":"RUID",'+
		'          "guiAliasName":"RUID",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ],'+
		'          "value":"33.KFGX.000656..COXC"'+
		'        },'+
		'        {'+
		'          "fieldName":"RPON",'+
		'          "guiAliasName":"RPON",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ]'+
		'        },'+
		'        {'+
		'          "fieldName":"EVCSP",'+
		'          "guiAliasName":"EVCSP",'+
		'          "path":['+
		'            "EVC_Form",'+
		'            "EVC_unidetail"'+
		'          ]'+
		'        }'+
		'      ],'+
		'      "index":2,'+
		'      "isEnabled":"y",'+
		'      "subSections":['+
		'        {'+
		'          "sectionName":"LREF\\/LEVEL OF SERVICE DETAILS",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"LREF",'+
		'              "guiAliasName":"LREF",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"1"'+
		'            },'+
		'            {'+
		'              "fieldName":"LOSACT",'+
		'              "guiAliasName":"LOSACT",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"N = New"'+
		'            },'+
		'            {'+
		'              "fieldName":"LOS",'+
		'              "guiAliasName":"LOS",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"REAL TIME"'+
		'            },'+
		'            {'+
		'              "fieldName":"BDW",'+
		'              "guiAliasName":"BDW",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "EVC_servicedetail"'+
		'              ],'+
		'              "value":"1000M"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y",'+
		'          "subSections":['+
		'            '+
		'          ]'+
		'        },'+
		'        {'+
		'          "sectionName":"VLAN DETAILS",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"VACT",'+
		'              "guiAliasName":"VACT",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "CE_VLANdetail"'+
		'              ],'+
		'              "value":"N = New"'+
		'            },'+
		'            {'+
		'              "fieldName":"CE_VLAN",'+
		'              "guiAliasName":"CE-VLAN",'+
		'              "path":['+
		'                "EVC_Form",'+
		'                "EVC_unidetail",'+
		'                "CE_VLANdetail"'+
		'              ],'+
		'              "value":"0088"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y",'+
		'          "subSections":['+
		'            '+
		'          ]'+
		'        }'+
		'      ]'+
		'    },'+
		'    {'+
		'      "sectionName":"Billing\\/Contact Section",'+
		'      "fields":['+
		'        '+
		'      ],'+
		'      "index":1,'+
		'      "isEnabled":"y",'+
		'      "subSections":['+
		'        {'+
		'          "sectionName":"Billing Information",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"Email",'+
		'              "value":"T-MOBILE.INVOICES@RAZORSIGHT.COM"'+
		'            },'+
		'            {'+
		'              "fieldName":"Contact Name",'+
		'              "value":"BILLING CONTACT"'+
		'            },'+
		'            {'+
		'              "fieldName":"Tel No\\/Ext",'+
		'              "value":"972-464-3698"'+
		'            },'+
		'            {'+
		'              "fieldName":"Fax"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 1",'+
		'              "value":"PO BOX 982245"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 2"'+
		'            },'+
		'            {'+
		'              "fieldName":"City",'+
		'              "value":"EL PASO"'+
		'            },'+
		'            {'+
		'              "fieldName":"State",'+
		'              "value":"TX"'+
		'            },'+
		'            {'+
		'              "fieldName":"Zip",'+
		'              "value":"79998"'+
		'            },'+
		'            {'+
		'              "fieldName":"Bill Name",'+
		'              "value":"T-MOBILE USA, INC"'+
		'            },'+
		'            {'+
		'              "fieldName":"Secondary Bill Name"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y"'+
		'        },'+
		'        {'+
		'          "sectionName":"Requested By",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"Email",'+
		'              "value":"Adwinda.Cook58@T-Mobile.com"'+
		'            },'+
		'            {'+
		'              "fieldName":"Contact Name",'+
		'              "value":"WENDY COOK"'+
		'            },'+
		'            {'+
		'              "fieldName":"Tel No\\/Ext",'+
		'              "value":"425-302-5876"'+
		'            },'+
		'            {'+
		'              "fieldName":"Fax"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 1",'+
		'              "value":"7668 WARREN PKWY"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 2"'+
		'            },'+
		'            {'+
		'              "fieldName":"City",'+
		'              "value":"FRISCO"'+
		'            },'+
		'            {'+
		'              "fieldName":"State",'+
		'              "value":"TX"'+
		'            },'+
		'            {'+
		'              "fieldName":"Zip",'+
		'              "value":"75034"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y"'+
		'        },'+
		'        {'+
		'          "sectionName":"Buyer Design Contact",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"Email",'+
		'              "value":"ADWINDA.COOK58@T-MOBILE.COM"'+
		'            },'+
		'            {'+
		'              "fieldName":"Contact Name",'+
		'              "value":"WENDY COOK"'+
		'            },'+
		'            {'+
		'              "fieldName":"Tel No\\/Ext",'+
		'              "value":"425-302-5876"'+
		'            },'+
		'            {'+
		'              "fieldName":"Fax"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 1",'+
		'              "value":"7668 WARREN PKWY"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 2"'+
		'            },'+
		'            {'+
		'              "fieldName":"City",'+
		'              "value":"FRISCO"'+
		'            },'+
		'            {'+
		'              "fieldName":"State",'+
		'              "value":"TX"'+
		'            },'+
		'            {'+
		'              "fieldName":"Zip",'+
		'              "value":"75034"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y"'+
		'        },'+
		'        {'+
		'          "sectionName":"Buyer Implementation Contact",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"Email",'+
		'              "value":""'+
		'            },'+
		'            {'+
		'              "fieldName":"Contact Name",'+
		'              "value":"JUSTIN MULLER"'+
		'            },'+
		'            {'+
		'              "fieldName":"Tel No\\/Ext",'+
		'              "value":"630-960-8133"'+
		'            },'+
		'            {'+
		'              "fieldName":"Fax"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 1"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 2"'+
		'            },'+
		'            {'+
		'              "fieldName":"City"'+
		'            },'+
		'            {'+
		'              "fieldName":"State"'+
		'            },'+
		'            {'+
		'              "fieldName":"Zip"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y"'+
		'        },'+
		'        {'+
		'          "sectionName":"Buyer Maintenance Contact",'+
		'          "fields":['+
		'            {'+
		'              "fieldName":"Email",'+
		'              "value":""'+
		'            },'+
		'            {'+
		'              "fieldName":"Contact Name"'+
		'            },'+
		'            {'+
		'              "fieldName":"Tel No\\/Ext"'+
		'            },'+
		'            {'+
		'              "fieldName":"Fax"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 1"'+
		'            },'+
		'            {'+
		'              "fieldName":"Address line 2"'+
		'            },'+
		'            {'+
		'              "fieldName":"City"'+
		'            },'+
		'            {'+
		'              "fieldName":"State"'+
		'            },'+
		'            {'+
		'              "fieldName":"Zip"'+
		'            }'+
		'          ],'+
		'          "index":1,'+
		'          "isEnabled":"y"'+
		'        }'+
		'      ]'+
		'    }'+
		'  ],'+
		'  "info":{'+
		'    "productCatalogName":"UNI EVC PT TO PT COMBO INSTALL",'+
		'    "productCatalogId":141551,'+
		'    "uomOrderKey":1345151,'+
		'    "uomOrderID":"TMUSEVC N00028051",'+
		'    "requestNumber":"VG08184A-RHM100",'+
		'    "ver":"01",'+
		'    "orderStatus":"TP COMPLETE",'+
		'    "activityId":"N",'+
		'    "activityType":"Install",'+
		'    "application":"ASR_SEND",'+
		'    "tradingPartner":"ATT",'+
		'    "owner":"ACook58",'+
		'    "createdDate":"2020-06-19 15:09:55.262",'+
		'    "lastStatusChangeDate":"2020-07-27 16:55:22.278",'+
		'    "sellerID":"SW80",'+
		'    "buyerID":"WCG",'+
		'    "transactionId":"",'+
		'    "responseSeqId":8044'+
		'  }'+
		'}';
		return NeustarListenerRequest.parse(json);
	}
}
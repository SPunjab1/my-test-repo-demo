/*********************************************************************************************
@Class        : ProjectTestDataFactory
@author       : Matthew Elias
@date         : 27.07.2020 
@description  : This class is to create records of Different Entities which can be used across the Project Test Class
@Version      : 1.0           
**********************************************************************************************/
@IsTest
public class ProjectTestDataFactory {
/***************************************************************************************
@Description : Method to create Location ID 
****************************************************************************************/ 
public static List<Location_ID__c> createLocationId( String locId,string locName,Integer numberOfRecords) {
        try{
            List<Location_ID__c> locationIdList = new List<Location_ID__c>();
            for ( Integer i = 0 ; i < numberOfRecords ; i++ ) {           
                Location_ID__c loc = new Location_ID__c(Location_ID__c=locId+i,Name= locName+i);                              
                locationIdList.add(loc);            
            }
            return locationIdList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }     

/***************************************************************************************
@Description : Method to create Project Records
****************************************************************************************/
    public static List<Project__c> createProjects(Integer numberOfProjects, String projectCode, Id locationId){
        try{
            List<Project__c> projectList = new List<Project__c>();
            for(Integer i=0;i<numberOfProjects;i++){
                Project__c proj = new Project__c(Name=projectCode + i, Location_ID__c = locationId);
                projectList.add(proj);
            }
            return projectList;
        } catch(Exception ex){
            throw ex;
        }
    }
    
/***************************************************************************************
@Description : Method to create Project Platform Event Instances
****************************************************************************************/
    public static List<ProjectId__e> createProjectEvents(Integer numberOfProjects, String projectCode, Id locationId){
        try{
            List<ProjectId__e> projectEventList = new List<ProjectId__e>();
            for(Integer i=0;i<numberOfProjects;i++){
                ProjectId__e proj = new ProjectId__e(Project_Code__c = projectCode, Location_ID__c = locationId);
                projectEventList.add(proj);
            }
            projectEventList.add(new ProjectId__e(Project_Code__c = projectCode, Location_ID__c = NULL));
            projectEventList.add(new ProjectId__e(Project_Code__c = projectCode, Location_ID__c = 'fakeID'));
            return projectEventList;
        } catch(Exception ex){
            throw ex;
        }
    }
}
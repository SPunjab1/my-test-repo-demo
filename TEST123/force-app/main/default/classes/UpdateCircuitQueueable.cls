/*********************************************************************************************
@author     : IBM
@date       : 17.09.20 
@description: This is the  UpdateCircuitQueueable  class 
@Version    : 1.0           
**********************************************************************************************/
public class UpdateCircuitQueueable implements Queueable, Database.AllowsCallouts {
    
    public final Map<Id,List<Circuit__c>> caseCircuitList;
    public UpdateCircuitQueueable( Map<Id,List<Circuit__c>> CircuitMap){
        this.caseCircuitList = CircuitMap;
        
        
    }    
    
    public void execute(QueueableContext context) {
        // This method will make a REST API call
        UpdateCircuitQueueable.updateIntLog(caseCircuitList);
    }
    
    public static void updateIntLog(Map<Id,List<Circuit__c>> caseCircuitMap)
    {
        if(!caseCircuitMap.values().isEmpty())
        {
            AtomsService.IS_INVOKE_FROM_TRIGGER = true;
            AtomsIntegrationAPI.returnParamsToBatchAfterCallout objRet =AtomsIntegrationAPI.sendUpdateCircuitReqToAtoms (caseCircuitMap, null);
            if(!objRet.logsToBeUpserted.isEmpty() && objRet.logsToBeUpserted.size()>0 )
            {
                upsert objRet.logsToBeUpserted;
            }
        }
    }
}
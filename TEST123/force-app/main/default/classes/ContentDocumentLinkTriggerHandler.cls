/*********************************************************************************************
@author     : IBM (Case/Task) & Matthew Elias (Request/Savings Tracker)
@date       : 22.08.19 -- 11.08.20
@description: This is the trigger handler for ContentDocumentLink object.
@Version    : 1.0
10/16/2020  : Shiva Goli: Updated BeforeInsert for NLG Opportunity files to share with Customer users.
**********************************************************************************************/
public with sharing class ContentDocumentLinkTriggerHandler extends TriggerHandler {
    private static final string ContentDocumentLink_API = 'ContentDocumentLink';
    public static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();    
    public static boolean isExecuted=false;
    public static ID nlg_New_Site_Lease_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Site Lease').getRecordTypeId();
    public static ID nlg_NLG_No_POR_information_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('NLG No POR information').getRecordTypeId();
    public static ID nlg_Settlement_and_Utility_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Settlement and Utility').getRecordTypeId();
    public static ID nlg_Site_AMD_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Site AMD').getRecordTypeId();
    public static Set<Id> nlgOpportunityRecordTypeSet = new Set<Id>{nlg_New_Site_Lease_RecordType,nlg_NLG_No_POR_information_RecordType,nlg_Settlement_and_Utility_RecordType,nlg_Site_AMD_RecordType};
    public ContentDocumentLinkTriggerHandler() {
        super(ContentDocumentLink_API);
    }
    /***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/
    public override String getName() {
        return ContentDocumentLink_API;
    }
    /***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems) {
        List<ContentDocumentLink> newList = new List<ContentDocumentLink>();
        newList = (List<ContentDocumentLink>) newItems;
        Set<Id> contentDocumentIdSet = new Set<Id>();
        Map<Id,List<ContentDocumentLink>> opportunityLinkedEntityMap = new Map<Id,List<ContentDocumentLink>>();
        // extract list of LE IDs and CD IDs
        for(ContentDocumentLink cl : newList){
            String objectType = getObjectName(cl.LinkedEntityId);
            if(objectType =='Opportunity'){
                //linkedEntityIdSet.add(cl.LinkedEntityId);
                contentDocumentIdSet.add(cl.ContentDocumentId);
                if(!opportunityLinkedEntityMap.containsKey(cl.LinkedEntityId)){
                    opportunityLinkedEntityMap.put(cl.LinkedEntityId,new List<ContentDocumentLink>());
                }
                opportunityLinkedEntityMap.get(cl.LinkedEntityId).add(cl);
            }            
        }  
        //NLG files uploaded on Opportunities are shared with Customer users.
        if(opportunityLinkedEntityMap.size()>0){
            nlgCommunityUserAccess(opportunityLinkedEntityMap);
        }
        
    }
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
    }
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
    }
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {
		/*********Method call to delete matching Attachment on Savings Tracker or Request*************/ 
        reconcileRequestSavingsTrackerAttachment(oldItemsMap);
    }
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
    }
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        /*********Method call to update Attachment available field on task once attachment is added*************/
        updateAttachmentAvailableOnTask(newItems);
        
        //Keep this at the end.
        //It should include copyAttachmentForTask method as it should execute only once per transaction
        if(ContentDocumentLinkTriggerHandler.isExecuted)return;
        
        /*********Method call to copy Attachment to Savings Tracker from Request*************/
        copyAttachmentToSavingsTracker(newItems);
        
        /*********Method call to copy Attachment to task as per the template*************/
        copyAttachmentForTask(newItems);
    }
    
/*********************************************************************************************
* @Description : Method call to copy Attachment to task as per the template
* @input params : Trigger.New , newmap
**********************************************************************************************/
    public void copyAttachmentForTask(List<Sobject> newList) {
        List<ContentDocumentLink> newItems = new List<ContentDocumentLink>();
        newItems = (List<ContentDocumentLink>) newList;
        ID workOrderRecordTaskType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        set<Id>contentDocumentIds=new set<id>();
        Map<id,list<id>>TaskIdContentDocumentIdMap=new map<id,list<id>>();
        Map<string,Decimal>metadataMap=new map<string,Decimal>();
        map<string,set<id>>finalmap=new Map<string,set<id>>();
        Set<id>linkedEntityIdList=new set<id>();
        set<id>parentids=new set<id>();
        list<ContentDocumentLink> cdList=new list<ContentDocumentLink>();
        for(ContentDocumentLink cd:newItems){
            contentDocumentIds.add(cd.ContentDocumentId);
            if(!TaskIdContentDocumentIdMap.containsKey(cd.LinkedEntityId)){
                TaskIdContentDocumentIdMap.put(cd.LinkedEntityId,new list<id>{cd.ContentDocumentId});
            }
            else
                TaskIdContentDocumentIdMap.get(cd.LinkedEntityId).add(cd.ContentDocumentId);
        }
        
        for(Attachement_required_for_template__mdt adata:[select Workflow_template__c,Parent_Task__c,Chsk__c from Attachement_required_for_template__mdt]){
            string key=string.valueof(adata.Workflow_template__c)+string.valueof(adata.Parent_Task__c);
            metadataMap.put(key,adata.Chsk__c);
        }
        system.debug('metadataMap-->'+metadataMap);
        if(contentDocumentIds!=null && contentDocumentIds.size()>0){
            for(ContentDocumentLink cd: [SELECT Id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId in :contentDocumentIds]){
                linkedEntityIdList.add(cd.LinkedEntityId);
            }
        }
        
        if(TaskIdContentDocumentIdMap!=null && TaskIdContentDocumentIdMap.size()>0){
            for(case c:[select id,workflow_template__c,task_number__c,parentid from case where id in :TaskIdContentDocumentIdMap.keyset() ])
            {    
                if(TaskIdContentDocumentIdMap.containsKey(c.id) && TaskIdContentDocumentIdMap.get(c.id)!=null ){
                    for(id i:TaskIdContentDocumentIdMap.get(c.id)){
                        string key1=string.valueof(c.Workflow_template__c)+string.valueof(c.task_number__c);
                        if(metadataMap.containsKey(key1)) {
                            parentids.add(c.parentid);
                            string key2=string.valueof(c.Workflow_template__c)+string.valueof(metadataMap.get(key1));
                            if(!finalmap.containskey(Key2)){
                                finalmap.put(key2,new set <id>{i});}
                            else{
                                finalmap.get(key2).add(i);}
                        }
                    }
                } 
            }
        }
        
        if(parentids!=null && parentids.size()>0){
            for(case c:[select id,task_number__c,Workflow_template__c from case where parentid in:parentids ]){
                string key=string.valueof(c.Workflow_template__c)+string.valueof(c.task_number__c);
                if(finalmap.containskey(key)&& !linkedEntityIdList.contains(c.id)){
                    for(id i:finalmap.get(key)){
                        ContentDocumentLink cdl=new ContentDocumentLink();
                        cdl.LinkedEntityId=c.id;
                        cdl.ContentDocumentId=i;
                        cdl.ShareType ='V';
                        cdList.add(cdl);
                    }
                }
            }
        }
        
        if(cdList.size()>0 && cdList!=null)
        { 
            ContentDocumentLinkTriggerHandler.isExecuted=true;
            try{
                insert cdlist;}
            catch(exception ex){throw ex;}
        }
        
    }
    
 /*********************************************************************************************
* @Description : Method to update attachment available field on task if the attachment is added
* @input params : Trigger.New
**********************************************************************************************/
    public void updateAttachmentAvailableOnTask(List<Sobject> newList) {
        List<ContentDocumentLink> newItems = new List<ContentDocumentLink>();
        newItems = (List<ContentDocumentLink>) newList;
        List<Id> linkedEntityIdList=new List<Id>();
        Set<id> TaskIdContentDocumentId=new Set<id>();
        List<Case> taskList= new List<Case>();
        List<ContentDocumentLink> cdList=new List<ContentDocumentLink>();   
        
        for(ContentDocumentLink cl:newItems){
            linkedEntityIdList.add(cl.LinkedEntityId);
        }        
        
        if(linkedEntityIdList!=Null && linkedEntityIdList.Size()>0){
            for(case cs:[select id,Attachment_Available__c from case where id in:linkedEntityIdList AND Attachment_Available__c=FALSE AND recordTypeId=:workOrderTaskRecordType]){    
                cs.Attachment_Available__c=true;
                taskList.add(cs);     
            }
        }
        
        try{
            if(taskList!=NULL && taskList.Size()>0){
                database.update(taskList,true);
            }
        }   
        catch(exception ex){throw ex;}
    }          
    
/*********************************************************************************************
* @Description : Method call to copy Attachment to Savings Tracker from Request
* @input params : Trigger.New
**********************************************************************************************/
    public void copyAttachmentToSavingsTracker(List<sObject> newList){
        List<ContentDocumentLink> newItems = new List<ContentDocumentLink>();
        newItems = (List<ContentDocumentLink>) newList;
        
        Set<Id> linkedEntityIdSet = new Set<Id>();
        Set<Id> contentDocumentIdSet = new Set<Id>();
        List<Savings_Tracker__c> savingsTrackerList = new List<Savings_Tracker__c>();
        Set<Id> savingsTrackerIdSet = new Set<Id>();
        Map<Id,Id> requestSavingsTrackerMap = new Map<Id,Id>();
        Map<Id,Set<String>> savingsTrackerCDMap = new Map<id,Set<String>>();
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();   
        
        // extract list of LE IDs and CD IDs
        for(ContentDocumentLink cl : newItems){
            linkedEntityIdSet.add(cl.LinkedEntityId);
            contentDocumentIdSet.add(cl.ContentDocumentId);
        }        
        
        // query for Saving Trackers associated with a Request with a new CDL
        savingsTrackerList = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c IN :linkedEntityIdSet];
        
        // if no Savings Trackers found, quit
        if(savingsTrackerList == Null || savingsTrackerList.size() == 0) return;
        
        // query for CD Titles from incoming CDLs
        Map<Id,ContentDocument> contentDocumentMap = new Map<Id,ContentDocument>([SELECT Id, Title, FileType FROM ContentDocument WHERE Id IN :contentDocumentIdSet]);
		        
        // populate map from Request Id to Savings Tracker Id
        for(Savings_Tracker__c st : savingsTrackerList){
            requestSavingsTrackerMap.put(st.Request__c, st.Id);
            savingsTrackerIdSet.add(st.Id);
        }
        
        // loop through CDLs associated with found STs and populate map from STs to Set of CD Titles
        for(ContentDocumentLink cdl : [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :savingsTrackerIdSet AND ContentDocument.FileType != 'SNOTE']){    
            if(!savingsTrackerCDMap.containsKey(cdl.LinkedEntityId)){
                savingsTrackerCDMap.put(cdl.LinkedEntityId, new Set<String>{cdl.ContentDocument.Title});
            }else{
                savingsTrackerCDMap.get(cdl.LinkedEntityId).add(cdl.ContentDocument.Title);
            }
        }
        
        // loop through incoming CDLs
        // if CDL.LE in keyset of requestSavingsTrackerMap (the CDL is associated with a Request which has a Savings Tracker)
        // if CDL.CD.T not in savingsTrackerCDMap.get(requestSavingsTrackerMap.get(CDL.LE))
        // create new CDL(CD --> ST)
        for(ContentDocumentLink cdl : newItems){
            if(ContentDocumentMap.get(cdl.ContentDocumentId).FileType == 'SNOTE') continue;
            if(requestSavingsTrackerMap.keyset().contains(cdl.LinkedEntityId)
               && (!savingsTrackerCDMap.containsKey(requestSavingsTrackerMap.get(cdl.LinkedEntityId)) 
                   || !savingsTrackerCDMap.get(requestSavingsTrackerMap.get(cdl.LinkedEntityId)).contains(contentDocumentMap.get(cdl.ContentDocumentId).Title))){
                       ContentDocumentLink newCDL = new ContentDocumentLink(ContentDocumentId = cdl.ContentDocumentId,
                                                                            LinkedEntityId = requestSavingsTrackerMap.get(cdl.LinkedEntityId),
                                                                            ShareType = cdl.ShareType,
                                                                            Visibility = cdl.Visibility);
                       cdlList.add(newCDL);
                   }
        }
        
        if(cdlList.size()>0 && cdlList!=null){ 
            try{
                insert cdlList;
            } catch(exception ex){
                throw ex;
            }
        }
    }
    
/*********************************************************************************************
* @Description : Method call to delete corresponding attachments on Request and Savings Tracker
* @input params : Trigger.oldMap (because Trigger.old is not provided)
**********************************************************************************************/
    public void reconcileRequestSavingsTrackerAttachment(Map<Id,sObject> oldMap){
        Map<Id,ContentDocumentLink> oldItemsMap = new Map<Id,ContentDocumentLink>();
        oldItemsMap = (Map<Id,ContentDocumentLink>) oldMap;
        List<ContentDocumentLink> oldItems = new List<ContentDocumentLink>();
        
        Set<Id> linkedEntityIdSet = new Set<Id>();
        Set<Id> contentDocumentIdSet = new Set<Id>();
        List<Savings_Tracker__c> savingsTrackerList = new List<Savings_Tracker__c>();
        List<Request__c> requestList = new List<Request__c>();
        List<ContentDocumentLink> deleteCDLs = new List<ContentDocumentLink>();
        
        Set<Id> savingsTrackerIDs = new Set<Id>();
        Set<Id> requestIDs = new Set<Id>();
        Map<Id,Id> requestSavingsTrackerMap = new Map<Id,Id>();
        Map<Id,Set<String>> recordCDMap = new Map<Id,Set<String>>();
        List<ContentDocumentLink> requestCDLs = new List<ContentDocumentLink>();
        
        // extract list of CDLs from oldMap (essentialy creating Trigger.old)
        for(Id cdlId : oldItemsMap.keySet()){
            oldItems.add(oldItemsMap.get(cdlId));
        }
        
        // extract list of LE IDs and CD IDs
        for(ContentDocumentLink cl : oldItems){
            // needed so that we can identify list of STs and Reqs that are losing CDLs
            linkedEntityIdSet.add(cl.LinkedEntityId);
            // needed for CD Title query
            contentDocumentIdSet.add(cl.ContentDocumentId);
        }        
        
        // query for ContentDocument Title Map
        Map<Id,ContentDocument> contentDocumentMap = new Map<Id,ContentDocument>([SELECT Id, Title, FileType FROM ContentDocument WHERE Id IN :contentDocumentIdSet]);
        
        // query for Saving Trackers with a CDL that has been deleted
        savingsTrackerList = [SELECT Id, Request__c FROM Savings_Tracker__c WHERE Id IN :linkedEntityIdSet];
        
        // if any Savings Trackers found, identify any matching CDLs on the Request and delete
        // CDLs from STs are being deleted, identify matching CDL on request
        if(savingsTrackerList != Null && savingsTrackerList.size() > 0){
            
            // extract set of Request IDs
            // create Map from Req ID --> ST ID
            for(Savings_Tracker__c st : savingsTrackerList){
                savingsTrackerIDs.add(st.Id);
                requestIDs.add(st.Request__c);
                requestSavingsTrackerMap.put(st.Request__c,st.Id);
            }
            
            // populate map of ST Id --> Set of CD Titles from oldItems
            for(ContentDocumentLink cdl : oldItems){
                if(contentDocumentMap.get(cdl.ContentDocumentId).FileType == 'SNOTE') continue;
                if(savingsTrackerIDs.contains(cdl.LinkedEntityId)){
                    if(!recordCDMap.containsKey(cdl.LinkedEntityId)){
                        recordCDMap.put(cdl.LinkedEntityId, new Set<String>{contentDocumentMap.get(cdl.ContentDocumentId).Title});
                    }else{
                        recordCDMap.get(cdl.LinkedEntityId).add(contentDocumentMap.get(cdl.ContentDocumentId).Title);
                    }
                }
            }
            
            // query for CDLs on Requests associated with STs that have a CDL being deleted
            for(ContentDocumentLink cdl : [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ContentDocument.FileType, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :requestIDs AND ContentDocument.FileType != 'SNOTE']){
                if(recordCDMap.containsKey(requestSavingsTrackerMap.get(cdl.LinkedEntityId))
                    && recordCDMap.get(requestSavingsTrackerMap.get(cdl.LinkedEntityId)).contains(cdl.ContentDocument.Title)){
                    deleteCDLs.add(cdl);
                }
            }
        }
        
        // query for Requests with a CDL that has been deleted
        requestList = [SELECT Id, Savings_Tracker_Count__c FROM Request__c WHERE Id IN :linkedEntityIdSet AND Savings_Tracker_Count__c > 0];
        
        // if any Requests found, if it has a Savings Tracker, identify matching CDLs on ST and delete
        if(requestList != NULL && requestList.size() > 0){
            
            requestIDs.clear();
            recordCDMap.clear();
            
            // extract requestIds
            for(Request__c req : requestList){
                requestIDs.add(req.Id);
            }
            
            // populate map from request ID --> set of CD Titles from oldItems
            for(ContentDocumentLink cdl : oldItems){
                if(contentDocumentMap.get(cdl.ContentDocumentId).FileType == 'SNOTE') continue;
                if(requestIDs.contains(cdl.LinkedEntityId)){
                    if(!recordCDMap.containsKey(cdl.LinkedEntityId)){
                        recordCDMap.put(cdl.LinkedEntityId, new Set<String>{contentDocumentMap.get(cdl.ContentDocumentId).Title});
                    }else{
                        recordCDMap.get(cdl.LinkedEntityId).add(contentDocumentMap.get(cdl.ContentDocumentId).Title);
                    }
                }
            }
            
            // query for IDs (keyset) of STs that are associated with a Request that is losing an attachment
            Map<Id,Savings_Tracker__c> savingsTrackerIdMap = new Map<Id,Savings_Tracker__c>([SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c IN :requestIDs]);
            
            // query for CDLs assoiated with those STs
            // loop through those CDLs and if the corresponding CDL is being deleted from the Req, delete it from the ST
            for(ContentDocumentLink cdl : [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ContentDocument.FileType, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :savingsTrackerIdMap.keySet() AND ContentDocument.FileType != 'SNOTE']){
                if(recordCDMap.containsKey(savingsTrackerIdMap.get(cdl.LinkedEntityId).Request__c)
                    && recordCDMap.get(savingsTrackerIdMap.get(cdl.LinkedEntityId).Request__c).contains(cdl.ContentDocument.Title)){
                    deleteCDLs.add(cdl);
                }
            }
        }
        
        if(deleteCDLs != NULL && deleteCDLs.size() > 0){
            try{
                delete deleteCDLs;
            } catch(Exception ex){
                throw ex;
            }
        }
        
    }
    
    /*********************************************************************************************
* @Description : Method call to expose files to community users on NLG opportunities
* @input params : 
**********************************************************************************************/
    public void nlgCommunityUserAccess(Map<Id,List<ContentDocumentLink>> linkedEntityMap){
        List<Opportunity> opportunityList = new List<Opportunity>();
        
        opportunityList = [SELECT Id, RecordTypeId,StageName FROM Opportunity WHERE Id IN :linkedEntityMap.keySet() and recordtypeid in :nlgOpportunityRecordTypeSet];
        if(opportunityList.size()>0){
            for(Opportunity opp: opportunityList){
                if(linkedEntityMap.containsKey(opp.Id) && linkedEntityMap.get(opp.Id).size()>0){
                    for(ContentDocumentLink cl : linkedEntityMap.get(opp.Id)){
                        cl.Visibility ='AllUsers';
                    }
                }
            }
        }
    }
    
    private String getObjectName(Id recordId){
        String sObjectName = recordId.getSObjectType().getDescribe().getName();
        return sObjectName;
    }
}
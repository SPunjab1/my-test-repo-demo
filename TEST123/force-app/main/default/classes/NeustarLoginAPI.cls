/*********************************************************************************************
@author     : IBM
@date       : 09.06.20
@description: Class for login callout to Neustar 
@Version    : 1.0
**********************************************************************************************/
public with sharing class NeustarLoginAPI {

     
/*********************************************************************************************
* @Description : This methods prepares JSON request based on the API Type
* @input params : Circuit, ASR ,ApiType
* @Return : Map of Circuit ID to Request string
/**********************************************************************************************/   
    public static HTTPResponse sendLoginRequest()
    {
        //Get Neustar Login details from the custom setting (TMobilePh2LoginDetailsHierarchy)
        TMobilePh2LoginDetailsHierarchy__c neustarToken =  TMobilePh2LoginDetailsHierarchy__c.getOrgDefaults();
        HttpRequest req = new HttpRequest();
        //Prepare request JSON
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('domain', neustarToken.Neustar_Domain__c);
        gen.writeStringField('username', neustarToken.Neustar_Username__c);
        gen.writeStringField('password', neustarToken.Neustar_Password__c);
        gen.writeEndObject();
        String jsonString = gen.getAsString();
        //Set Endpoint
        req.setEndpoint(neustarToken.Neustar_End_Point_URL__c);
        //Set HTTP method
        req.setMethod('POST');
        //Set Content Type
        req.setHeader('Content-Type', 'application/json');
        req.setBody(jsonString);
        System.debug('Request***********'+req);
        Http http = new Http();
        //Send Http request for Login to Neustar
        HTTPResponse res = http.send(req);
        System.debug('Body =========='+res.getBody());
        if(res.getStatusCode() == 200){
            system.debug('Login sucess');    
        }
        return res;
 
    }
    
}
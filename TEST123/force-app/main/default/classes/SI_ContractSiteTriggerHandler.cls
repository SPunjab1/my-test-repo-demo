/******************************************************************************
@author      - IBM
@date        - 30.06.2020
@description - Handler Class for SI_ContractSiteTrigger 
@version     - 1.0
*******************************************************************************/
public with sharing class SI_ContractSiteTriggerHandler extends TriggerHandler{
    public static boolean isOppTriggerExecuted = FALSE ;
    private static final string OPP_API = 'Contract_Site__c';
    /***************************************************
    *@description - constructor
    ***************************************************/
    public SI_ContractSiteTriggerHandler(){
        super(OPP_API);
    }
    /***************************************************************************************
    @description - Return the name of the handler invoked
    @return - Contract Site object API name 
    ****************************************************************************************/
    public override String getName(){
        return OPP_API;
    }
    /**************************************************************************************
    @description - Trigger handlers for events
    @param newItems - new List of Contract Site
    ****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems){}
    /**************************************************************************************
    @description - Trigger handlers for events
    @param newItems - new List of Contract Sites
    @param newItemsMap - new Map of Contract Sites
    @param oldItemsMap - old Map of Contract Sites
    ****************************************************************************************/
    public override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap){}
    /**************************************************************************************
    @description - Trigger handlers for events
    @param oldItemsMap - old Map of Contract Sites
    ****************************************************************************************/
    public override void beforeDelete(Map<Id, SObject> oldItemsMap){
        
    }
    /**************************************************************************************
    @description - Trigger handlers for events
    @param oldItemsMap - old Map of Contract Sites
    ****************************************************************************************/
    public override void afterDelete(Map<Id, SObject> oldItemsMap) {
        /********* calling method whenever Contract Site is deleted**************/
        toDeleteIntakeSiteOrBlankOppOnCase(oldItemsMap.values());
        SI_ContractSiteTriggerHelper.updateLocationCMGWorkingStatusOnDelete(oldItemsMap.values());
        
    }
    /**************************************************************************************
    @description - Trigger handlers for events
    @param oldItemsMap - old Map of Contract Sites
    ****************************************************************************************/
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {}
    /**************************************************************************************
    @description - Trigger handlers for events
    @param newItems - new List of Contract Sites
    @param newItemsMap - new Map of Contract Sites
    @param oldItemsMap - old Map of Contract Sites
    ****************************************************************************************/
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
    }
    /**************************************************************************************
    @description - Trigger handlers for events
    @param newItems - new List of Contract Sites
    @param newItemsMap - new Map of Contract Sites
    ****************************************************************************************/  
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        SI_ContractSiteTriggerSupportClass.updateLocationCMGWorkingStatus((List<Contract_Site__c>)newItems,(Map<Id,Contract_Site__c>)newItemsMap);
    }

    /**************************************************************************
    * @description - To Delete Intake Site and Update Case with No Opportunity (after delete) 
    * @param oldListOfContractSites - List of Contract Sites
    **************************************************************************/
    // Private method for call inside the class ONLY
    void toDeleteIntakeSiteOrBlankOppOnCase(List<Contract_Site__C> oldListOfContractSites){
        if(SI_UtilityClass.triggerWillRun){
            Set<Id> setOfIntakeSiteIds = new Set<Id>();
            for(Contract_Site__C objContractSite : oldListOfContractSites){
                // fill Intake Site Ids (Parent Of Contract Site)
                if(String.isNotBlank(objContractSite.Intake_Site__c)){
                   setOfIntakeSiteIds.add(objContractSite.Intake_Site__c); 
                }   
            }
            if(!setOfIntakeSiteIds.isEmpty()){
                Set<Id> setOfCaseIds = new Set<Id>();
                // SOQL on Intake Site to get Case Ids
                for(Intake_Site__c objIntakeSite : [SELECT Case__c FROM Intake_Site__c WHERE Id IN: setOfIntakeSiteIds AND 
                                                    Case__c != NULL LIMIT 50000]){
                    // fill Case Ids (Grand Parent Of Contract Site / Parent of Intake Site )
                    setOfCaseIds.add(objIntakeSite.Case__c);
                }
                if(!setOfCaseIds.isEmpty()){
                    List<Case> listOfCasesToUpdate = new List<Case>();
                    List<Intake_Site__c> listOfIntakeSiteToDelete = new List<Intake_Site__c>();
                    List<Intake_Site__c> listOfIntakeSiteToDeleteTemp = new List<Intake_Site__c>();
                    // SOQL on Case Object to check total number of Intake Site Records have for each Cases
                    for(Case objCase : [SELECT Opportunity__c,(SELECT Id FROM Intake_Sites__r) FROM Case WHERE 
                                        Id IN: setOfCaseIds LIMIT 50000]){
                        if(!objCase.Intake_Sites__r.isEmpty()){
                            // When Case have ONLY one Intake Site Record (Blank its Case's Opportunity)
                            if(objCase.Intake_Sites__r.size() == Integer.valueOf(System.Label.SI_TotalIntakeSiteRecords)){
                                objCase.Opportunity__c = NULL;
                                listOfCasesToUpdate.add(objCase);
                            } else if(objCase.Intake_Sites__r.size() > Integer.valueOf(System.Label.SI_TotalIntakeSiteRecords)){
                                // When Case have more than one Intake Site Record (Delete Intake Site)
                                listOfIntakeSiteToDeleteTemp.addAll(objCase.Intake_Sites__r);
                            }
                        }
                    }
                    if(!listOfIntakeSiteToDeleteTemp.isEmpty()){
                        for(Intake_Site__c objIntakeSite : listOfIntakeSiteToDeleteTemp){
                            if(setOfIntakeSiteIds.contains(objIntakeSite.Id)){
                                listOfIntakeSiteToDelete.add(objIntakeSite);
                            }
                        }
                    }
                    try{
                        // Update Case with No Opportunity
                        if(!listOfCasesToUpdate.isEmpty()){
                            update listOfCasesToUpdate;
                        }
                        // Delete Intake Site 
                        if(!listOfIntakeSiteToDelete.isEmpty()){
                            delete listOfIntakeSiteToDelete;
                        }
                    } catch(DmlException e){
                        System.debug(LoggingLevel.ERROR,'Exception during Case Update or deletion of Intake Site Records'+e.getMessage());
                    }
                }
            }
        }
    }
}
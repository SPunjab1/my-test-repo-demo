/*********************************************************************************************
@author     : IBM
@date       : 09.06.20
@description: Sample Utility Class for Neustar listener
@Version    : 1.0
**********************************************************************************************/
/********************************************
* URL :/services/apexrest/receiveNeuStarMessage/
* JSON :
* 

******************************************/

global with sharing class NeuStarMessageRecieverUtility{
    
    public static Map<String,String>  neuStarATOMSUtilMap = new Map<String,String>();
    public static Boolean isEVC = false;
    public static Boolean isUNI = false;
    
    static{
        for(ATOMSNeuStarUtilitySettings__c utilRec : ATOMSNeuStarUtilitySettings__c.getAll().values()){
            NeuStarATOMSUtilMap.put(utilRec.Name,utilRec.Value__c);
        }
    }
    
    global class ResponseWrapper{
        public String status {get;set;}
        public String pon {get;set;}
        public List<String> integrationLogIDs {get;set;}
        public DateTime createdDate {get;set;}
        public String message {get;set;}
        public String errorMessage {get;set;}
        public Boolean comboOrder {get;set;}
        
        public ResponseWrapper(){
            
            this.status = NeuStarATOMSUtilMap.get('SuccessStatus');
            this.message = NeuStarATOMSUtilMap.get('NeuStarReceiverSuccessMsg');                
            this.createdDate = DateTime.now();
            this.errorMessage = '';
        }
        
        public ResponseWrapper(String errorMessage){
            
            for(ATOMSNeuStarUtilitySettings__c utilRec : ATOMSNeuStarUtilitySettings__c.getAll().values()){
                NeuStarATOMSUtilMap.put(utilRec.Name,utilRec.Value__c);
            }
            
            this.status = 'Failed';
            this.message = NeuStarATOMSUtilMap.get('NeuStarGenericErrorResp');
            this.errorMessage = errorMessage;              
            this.createdDate = DateTime.now();
        }
        
    } 
    
    /**************************************************************************************
    * @Description  This method process incoming REST request and creates Integration Log 
    * @Param        
    * 
    * @Return       NeuStarMessageRecieverUtility.ResponseWrapper - return wrapper of processing to calling service
    * 
    * @Example     
    * NeuStarMessageRecieverUtility.receiveMessage();
    **************************************************************************************/ 
    public static NeuStarMessageRecieverUtility.ResponseWrapper receiveMessage(){
        
        Boolean bCreateOrphanLog = true, bSendSuccessOnFail = false, bIsComboOrder = false;
        String strPonNo = '';
        String uomOrderId = '';
        
        NeuStarMessageRecieverUtility.ResponseWrapper retResponse = new NeuStarMessageRecieverUtility.ResponseWrapper();
        try{           
            
            if( !Schema.sObjectType.Integration_Log__c.iscreateable() || !Schema.sObjectType.Integration_Log__c.isupdateable()){
                retResponse = new ResponseWrapper( 'Unauthorize resource.');
                bCreateOrphanLog = true;
                return retResponse;
            }
            
            //RequestBody is empty
            if(RestContext.request.requestBody == null){            
                retResponse = new ResponseWrapper( NeuStarATOMSUtilMap.get('EmptyReqErrorMsg'));
                bCreateOrphanLog = true;
                return retResponse;
            }
            
            String requestBody = RestContext.request.requestBody.toString().trim();        
            //RequestBody is empty
            if(String.isBlank(requestBody)){            
                retResponse = new ResponseWrapper( NeuStarATOMSUtilMap.get('EmptyReqErrorMsg'));
                bCreateOrphanLog = true;
                return retResponse;
            }
            
            NeustarListenerRequest parseRequest = null;
            try{            
                parseRequest = NeustarListenerRequest.parse(requestBody);            
            }
            catch(Exception ex){
                printLogs(ex);
                retResponse = new ResponseWrapper( 'Invalid JSON, parsing issue -' + ex.getMessage());
                bCreateOrphanLog = true;
                return retResponse;
            }
            /**
            if( parseRequest.Response == null || parseRequest.Response.isEmpty()){
                retResponse = new ResponseWrapper( NeuStarATOMSUtilMap.get('EmptyReqErrorMsg'));
                bCreateOrphanLog = true;
                return retResponse;
            }
            **/
            
            if(String.isNotBlank(system.Label.Valid_TP_Responses) && parseRequest.Info != null && String.isNotBlank(parseRequest.Info.orderStatus)){
                
                String strDoStatus =  system.Label.Valid_TP_Responses.toLowerCase();
                List<String> doStatusList = strDoStatus.split(',');            
                //system.debug('doStatusList >>>'+doStatusList );
                if( !doStatusList.isEmpty() && !doStatusList.contains( parseRequest.Info.orderStatus.toLOwerCase().trim())){
                   //system.debug('Inside validTP'+parseRequest.Info.orderStatus.toLOwerCase().trim());
                    bCreateOrphanLog = false;
                    return new ResponseWrapper();
                }
            }
            else{
                    bCreateOrphanLog = false;
                    return new ResponseWrapper();
            }
            
            if( parseRequest.Info == null || parseRequest.Info.requestNumber == null || parseRequest.Info.requestNumber ==''){
                retResponse = new ResponseWrapper( NeuStarATOMSUtilMap.get('NoMatchingPONErrrorMsg'));
                bCreateOrphanLog = true;
                return retResponse;
            }
            
            strPonNo = parseRequest.Info.requestNumber;
            
            //Checking Order Type in case of Non-Combo Order
            uomOrderId = parseRequest.Info.uomOrderID != null && parseRequest.Info.uomOrderID != '' ? parseRequest.Info.uomOrderID : '';
            //system.debug('uomOrderId>>'+uomOrderId);
            if(uomOrderId != '' && uomOrderId.containsIgnoreCase('EVC')){
                isEVC = true;
            }
            if(uomOrderId != '' && uomOrderId.containsIgnoreCase('UNI')){
                isUNI = true;
            }
            //system.debug('isEVC>>>'+isEVC);
            
            Map<String, ASR__c> asrMap = new map<String, ASR__c>([SELECT Id,Order_Type_in_Neustar__c, PON__c, SubService_Type__c, Work_Order_Id__c, 
                                                                  (SELECT Id FROM Integration_Logs__r WHERE API_Type__c in ('Receive NeuStar Message', 'Update ATOMS in SFDC')) 
                                                                  FROM ASR__c WHERE Purchase_Ord_PON__c =: parseRequest.Info.requestNumber WITH SECURITY_ENFORCED LIMIT 2]);
            
            //No Associated ASR found in system.
            if(asrMap.isEmpty()){  
                //strPonNo = parseRequest.Info.requestNumber;           
                retResponse = new ResponseWrapper( NeuStarATOMSUtilMap.get('NoMatchingASRFoundMsg') + ' : ' + strPonNo);
                retResponse.pon = strPonNo;
                bCreateOrphanLog = true;
                bSendSuccessOnFail = true;
                return retResponse;
            }
            /**
            //Few PON no are not related to ASR
            if(parseRequest.Response != null && asrMap.size() != parseRequest.Response.size()){       
            
                retResponse = new ResponseWrapper( 'Invalid request - no of responses in request body does not match to no of ASR in system.');
                retResponse.pon = strPonNo;
                bCreateOrphanLog = true;
                bSendSuccessOnFail = true;
                return retResponse;
            }
            **/
            
            Map<String, NeustarListenerRequest> mapRequestByServiceType = null;            
            if( parseRequest.Response != null && parseRequest.Response.size() > 1){
                mapRequestByServiceType =  NeustarListenerRequest.slice(parseRequest);
                bIsComboOrder = true;
                
                if( !mapRequestByServiceType.containsKey('evc') || !mapRequestByServiceType.containsKey('uni')){                    
                    retResponse = new ResponseWrapper( 'Combo order should have EVC and UNI/PORT response type.');
                    retResponse.pon = strPonNo;
                    bCreateOrphanLog = true;
                    return retResponse;
                }
            }
            
            List<Integration_Log__c> logList2Upsert = new List<Integration_Log__c>();
            for(ASR__c asr : asrMap.values()){            
                
                //system.debug('TYPEOFORD7'+asr.Order_Type_in_Neustar__c);
                Integration_Log__c existingLog = null;
                if(asr.Integration_Logs__r != null && !asr.Integration_Logs__r.isEmpty()){
                    existingLog = asr.Integration_Logs__r[0];
                }
                NeustarListenerRequest neustarRequest = ( bIsComboOrder ? mapRequestByServiceType.get( asr.Order_Type_in_Neustar__c.toLowerCase()) : parseRequest);
                logList2Upsert.Add( generateLogForNeustar( asr, neustarRequest, bIsComboOrder, existingLog));
            }
            
            try{
                upsert logList2Upsert;
            }
            catch(Exception ex){
                printLogs(ex);
                bSendSuccessOnFail = true;
                retResponse = new ResponseWrapper( ex.getMessage());
                bCreateOrphanLog = true;
                return retResponse;
            }
            bCreateOrphanLog = false;
            List<String> setLogIds = new List<String>();
            for(Integration_Log__c log : logList2Upsert){
                setLogIds.Add(log.Id);
            }
            
            retResponse.integrationLogIDs = setLogIds;
            retResponse.pon = parseRequest.Info.requestNumber;
            retResponse.comboOrder = bIsComboOrder;
            
            try{
                for(Integration_Log__c log : logList2Upsert){
                    log.Response__c = JSON.serialize(retResponse);
                } 
                update logList2Upsert;
            }
            catch(exception ex){}
            
            return retResponse;
        }
        catch(exception ex){
            printLogs(ex);
            retResponse = new ResponseWrapper( ex.getMessage());
            bCreateOrphanLog = true;
            return retResponse;            
        }
        finally{
            if(bCreateOrphanLog){
                String logId = '';
                ResponseWrapper retResponseSuccess = new ResponseWrapper();
                retResponseSuccess.integrationLogIDs = new List<String>{logId};
                retResponseSuccess.pon = strPonNo;
                retResponseSuccess.comboOrder = bIsComboOrder;
                //system.debug('retResponse - '+ retResponse);
                try{ 
                    Integration_Log__c log =  generateOrphanLogForNeustar(retResponse);
                    log.Response__c = JSON.serialize(retResponseSuccess);
                    insert log;
                    logId = log.Id;
                }catch(Exception ex){}
                if(bSendSuccessOnFail){
                    retResponseSuccess.integrationLogIDs = new List<String>{logId};
                    return retResponseSuccess; 
                }
            }
        }
    }
    
    public static void printLogs(Exception ex){
        
        system.debug('ex ' + ex);
        system.debug('ex getStackTraceString' + ex.getStackTraceString());
        system.debug('ex msg' + ex.getMessage());        
    }
    
    /**************************************************************************************
    * @Description  This method generate orphan log for request based on return failure response
    * @Param        ResponseWrapper resp - failed response 
    * 
    * @Return       Integration_Log__c - Integration log orphan record with data filed ready to insert.
    * 
    * @Example     
    * NeuStarMessageRecieverUtility.generateOrphanLogForNeustar( resp);
    **************************************************************************************/ 
    public static  Integration_Log__c generateOrphanLogForNeustar( ResponseWrapper resp){
        
        Integration_Log__c log = new Integration_Log__c( DWH_Task_Status__c= null, Purchase_Ord_PON__c = resp.pon, From__c ='Neustar', To__c ='Salesforce', Error_Log__c = NeuStarATOMSUtilMap.get('GenericUserError'), Status__c = NeuStarATOMSUtilMap.get('Failure'), Failure_Reason__c = resp.errorMessage, 
                                                        API_Type__c = NeuStarATOMSUtilMap.get('NeuStarReceiverAPIType'), Pending_ATOMS_Update__c = false, Pending_Salesforce_Update__c = false);
        if(RestContext.request.requestBody != null){
            log.Request__c = RestContext.request.requestBody.toString().trim();
            //log.Response__c = JSON.serialize(resp);
        }        
        return log;
    }
    
    /**************************************************************************************
    * @Description  This method generate log for ASR based on params
    * @Param        ASR__c asr
    *               NeustarListenerRequest neustarRequest - Parsed request
    *               Boolean bIsComboOrder - Is request contain cobmo order
    *               Integration_Log__c existingLog - if existing log exist under ASR
    * 
    * @Return       Integration_Log__c - Integration log record with data filed ready to upsert.
    * 
    * @Example     
    * NeuStarMessageRecieverUtility.generateLogForNeustar( asr, neustarRequest, bIsComboOrder, existingLog);
    **************************************************************************************/ 
    public static  Integration_Log__c generateLogForNeustar( ASR__c asr, NeustarListenerRequest neustarRequest, Boolean bIsComboOrder, Integration_Log__c existingLog){
        //system.debug( neustarRequest.Info);
        //system.debug( neustarRequest.Info.orderStatus);
        //system.debug( asr.Subservice_Type__c);
        //system.debug( asr.Work_Order_Id__c);
        //system.debug( NeuStarATOMSUtilMap.get('NeuStarReceiverAPIType'));
        Integration_Log__c log = new Integration_Log__c( PON__c = asr.PON__c,DWH_Task_Status__c = null, Purchase_Ord_PON__c = neustarRequest.Info.requestNumber, TP_Response_Type__c = neustarRequest.Info.orderStatus, Type_of_Order__c = asr.Order_Type_in_Neustar__c, ASR__c = asr.Id,
                                                        Error_Log__c = '', Status__c ='success', Failure_Reason__c = '',  From__c ='Neustar', To__c ='Salesforce', Is_Combo_Order__c = bIsComboOrder, 
                                                        API_Type__c = NeuStarATOMSUtilMap.get('NeuStarReceiverAPIType'), Pending_ATOMS_Update__c = false, Pending_Salesforce_Update__c = false);
        
        if(existingLog != null){
            log.Id = existingLog.Id;
        }    

        //Populating Order Type for Single Order Case:
        /**
        if(!bIsComboOrder){
           // log.Type_of_Order__c =  isEVC ? 'EVC': 'UNI';
            
        }
        if(isEVC && isUNI){
            log.Type_of_Order__c = asr.Subservice_Type__c;
        }
        **/
        //system.debug('FINALTOORD'+log.Type_of_Order__c);
        
        log.Request__c = JSON.serialize(neustarRequest);        
        log.Pending_ATOMS_Update__c = true;
        log.Pending_Salesforce_Update__c = true;

        return log;
    }
}
@istest
public class NLG_Opp_EnhancementVerifyTest {
	@testSetup
    static void testDataSetup() {
         //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //Intialize list of SI Savings Tracker to insert records
        List<SI_Savings_Tracker__c> listSISavingTracker = new  List<SI_Savings_Tracker__c>();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
         // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = listAccount[0].Id;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        oppRecForContract.Lease_or_AMD__c = 'AMD';
        oppRecForContract.Landlord__c='Private LL';
        insert oppRecForContract;
        Lease_Enhancement__c lease = TestDataFactory.createLeaseEnhancement();
		lease.Lease_Intake__c = oppRecForContract.Id;
        insert lease;
    }
    
   
    
    @istest static void VerifyOpportunityEnhacements(){
        //TestDataFactory.createLeaseEnhancement();
        Test.startTest();
        Opportunity opportunityRec = [Select id from Opportunity limit 1];
        List<String> opplist = new  List<String>();
        opplist.add(opportunityRec.Id);
        NLG_Opp_EnhancementVerify.VerifyOpportunityEnhacements(opplist);
        Test.stopTest();
    }
     @istest static void VerifyOpportunityEnhacements1(){
        //TestDataFactory.createLeaseEnhancement();
        Test.startTest();
        Opportunity opportunityRec = [Select id,Lease_or_AMD__c,Landlord__c from Opportunity limit 1];
        opportunityRec.Lease_or_AMD__c= 'Lease';
         update opportunityRec;
         List<String> opplist = new  List<String>();
        opplist.add(opportunityRec.Id);
        NLG_Opp_EnhancementVerify.VerifyOpportunityEnhacements(opplist);
        Test.stopTest();
    }
}
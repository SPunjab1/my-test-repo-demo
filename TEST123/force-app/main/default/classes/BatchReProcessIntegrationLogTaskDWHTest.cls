/*********************************************************************************************************************
**********************************************************************************************************************
*  @Class            : BatchReProcessIntegrationLogTaskDWHTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 22.07.2019
*  @Description      : Test class for BatchReProcessIntegrationLogTaskDWH
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchReProcessIntegrationLogTaskDWHTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testMethod void TestMethod1(){
        List<Integration_Log__c> integrationLogsToInsert = new List<Integration_Log__c>();
        list<case>caseList =new List<case>();
         Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        caseList=TestDataFactory.createCase('AAV CIR Retest',1,recTypeId);
        insert caseList;
        list<case>taskList=new list<case>();
        taskList=[select id,status from case where parentid =:caseList[0].id];
        for(Integer i=0; i<5; i++){
            Integration_Log__c integrationLog = new Integration_Log__c();
            integrationLog.Status__c = 'Failure';
            integrationLog.API_Type__c='DWH Close Task';
            integrationLog.Task__c=taskList[0].id;
            integrationLogsToInsert.add(integrationLog);
        }

        Test.startTest();
        insert  integrationLogsToInsert;
        taskList[0].status=integrationLogsToInsert[0].DWH_Task_Status__c;
        update(taskList[0]);
        BatchReProcessIntegrationLogTaskDWH batch = new BatchReProcessIntegrationLogTaskDWH();
            Database.executeBatch(batch);
            system.schedule('Jobs', CRON_EXP, batch);
        Test.stopTest();
    }
    static testMethod void TestMethod2(){
        List<Integration_Log__c> integrationLogsToInsert = new List<Integration_Log__c>();
        list<case>caseList =new List<case>();
         Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        caseList=TestDataFactory.createCase('AAV CIR Retest',1,recTypeId);
        insert caseList;
        list<case>taskList=new list<case>();
        taskList=[select id,status from case where parentid =:caseList[0].id];
        for(Integer i=0; i<5; i++){
            Integration_Log__c integrationLog = new Integration_Log__c();
            integrationLog.Status__c = 'Failure';
            integrationLog.API_Type__c='DWH Close Task';
            integrationLog.Task__c=taskList[0].id;
            integrationLogsToInsert.add(integrationLog);
        }
        List<Case> updateLst =new List<Case>();
        Test.startTest();
        insert  integrationLogsToInsert;
        taskList[0].status=null;
        updateLst.add(taskList[0]);
        taskList[1].status='Close';
        updateLst.add(taskList[1]);
        update (updateLst);
            BatchReProcessIntegrationLogTaskDWH batch = new BatchReProcessIntegrationLogTaskDWH();
            Database.executeBatch(batch);
            system.schedule('Jobs', CRON_EXP, batch);
        Test.stopTest();
    }
   

}
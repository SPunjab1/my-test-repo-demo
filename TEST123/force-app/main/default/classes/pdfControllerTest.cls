/*********************************************************************************************
@Class        : pdfControllerTest
@author       : Pritesh
@date         : 11.20.2020 
@description  : This class is test class for pdfcontroller
@Version      : 1.0           
**********************************************************************************************/
@isTest
public class pdfControllerTest {
    
    @testSetup
    static void testDataSetup() {
        ID nlg_New_Site_Lease_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Site Lease').getRecordTypeId();
        ID nlg_NLG_No_POR_information_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('NLG No POR information').getRecordTypeId();
        ID nlg_Settlement_and_Utility_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Settlement and Utility').getRecordTypeId();
        ID nlg_Site_AMD_RecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Site AMD').getRecordTypeId();
        
        List<Opportunity> oppInsertList = new List<Opportunity>();
        Opportunity nlgnewIntake= new Opportunity();
        nlgnewIntake.RecordTypeId = nlg_New_Site_Lease_RecordType;
        nlgnewIntake.Name ='nlgnewIntake1';
        nlgnewIntake.StageName ='New';
        nlgnewIntake.Market_s__c ='WASHINGTON DC';
        nlgnewIntake.Region__c ='South';
        nlgnewIntake.Landlord__c ='Private LL';
        nlgnewIntake.CloseDate = System.today()+100;
        nlgnewIntake.Lease_or_AMD__c = 'AMD';
        nlgnewIntake.New_Rent_Frequency__c = 'Quarterly';
        nlgnewIntake.New_Rent__c = 10000.45;
        nlgnewIntake.Current_Rent_Frequency__c = 'Quarterly';
        nlgnewIntake.Current_Rent__c = 10000.45;
        nlgnewIntake.Total_Rent_Over_Initial_Term_Current__c=10000;
        nlgnewIntake.New_One_Time_Fees__c =10000;
        insert nlgnewIntake;
        
        Opportunity_Approval_History__c oppApprovalObj = new Opportunity_Approval_History__c (Opportunity__c = nlgnewIntake.Id, 
                                                                                              Action__c ='Approve', 
                                                                                              Approval_Stage__c = 'Regional Review',
                                                                                              Current_Approver__c=Userinfo.getUserId()
                                                                                             );
        
        
        insert oppApprovalObj;
        
    }
    
    static testmethod void testGetRegionalApprover() { 
        
        Test.startTest();
        Opportunity oppObj =[SELECT Id from Opportunity LIMIT 1];
        pdfController.DataWrapper wrapperObj = new pdfController.DataWrapper();
        wrapperObj = pdfController.getRegionalApprover(oppObj.Id);
        system.assert(null != wrapperObj);
        Test.stopTest();
    }
    
    static testmethod void testGenerateNewContentVersion() { 
        
        Test.startTest();
        Opportunity oppObj =[SELECT Id from Opportunity LIMIT 1];
        pdfController.GenerateNewContentVersion( oppObj.id, 'Test', '11-10-2020', 'test' , 'test', '1000' , 'Signing User');
        system.assert(null != oppObj.id);
        Test.stopTest();
    }
    
    static testmethod void testGetSignAuthorityData(){
        Test.startTest();
        Opportunity oppObj =[SELECT Id from Opportunity LIMIT 1];
        List<NLG_Util.signingWrapper> signingWrapperList = new List<NLG_Util.signingWrapper>();
        signingWrapperList = pdfController.getSignAuthorityData(oppObj.Id);
        system.assert(null != signingWrapperList);
        Test.stopTest();
    }
    
     static testmethod void testRentIncreaseCalculator(){
        Test.startTest();
        Opportunity oppObj =[SELECT Id, New_Rent__c, New_Rent_Frequency__c, Current_Rent__c, Current_Rent_Frequency__c FROM Opportunity LIMIT 1];
        Decimal rentIncrease = pdfController.rentIncreaseCalculator(oppObj);
        system.assert(rentIncrease == 0.00);
        Test.stopTest();
    }
    
}
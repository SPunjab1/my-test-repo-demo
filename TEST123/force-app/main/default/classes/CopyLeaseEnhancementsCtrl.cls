public class CopyLeaseEnhancementsCtrl {

    String opportunityId{get;set;}
    public CopyLeaseEnhancementsCtrl(ApexPages.StandardController ctrl){
        opportunityId=ApexPages.currentPage().getParameters().get('id');
    }
    public PageReference copyEnhancements(){
        List<Lease_Enhancement__c>lstEnhancement=[select id,name,Current_Lease__c,Current_Document_Location__c,
                                                  New_Document_Location__c,New_Lease__c from Lease_Enhancement__c
                                                  where Lease_Intake__c=:opportunityId];
        for(Lease_Enhancement__c objLeaseEnhancement:lstEnhancement){
            objLeaseEnhancement.New_Lease__c=objLeaseEnhancement.Current_Lease__c;
            objLeaseEnhancement.New_Document_Location__c=objLeaseEnhancement.Current_Document_Location__c;
        }
        if(lstEnhancement.size()>0){
            update lstEnhancement;
        }
        PageReference opportunityPage=new PageReference('/'+opportunityId);
        opportunityPage.setRedirect(true);
        return opportunityPage;
    }
    
    @AuraEnabled 
	public static void copyEnhancements(String opportunityId){
         System.debug('-->Copy enhancements 2' + opportunityId);
        List<Lease_Enhancement__c>lstEnhancement=[select id,name,Current_Lease__c,Current_Document_Location__c,
                                                  New_Document_Location__c,New_Lease__c from Lease_Enhancement__c where Lease_Intake__c=:opportunityId];
        System.debug('-->Copy enhancements 2' + lstEnhancement);
        for(Lease_Enhancement__c objLeaseEnhancement:lstEnhancement){
            objLeaseEnhancement.New_Lease__c=objLeaseEnhancement.Current_Lease__c;
            objLeaseEnhancement.New_Document_Location__c=objLeaseEnhancement.Current_Document_Location__c;
        }
        if(lstEnhancement.size()>0){
            update lstEnhancement;
            System.debug( lstEnhancement.size());
            System.debug('-->Copy enhancements 2' + lstEnhancement);
        }     
    }
}
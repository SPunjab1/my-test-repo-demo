/******************************************************************************
@author      - IBM
@date        - 17.11.2020
@description - Support class to update location data in system mode from Contract Site trigger
@version     - 1.0
*******************************************************************************/
public without sharing class SI_ContractSiteTriggerSupportClass{
    /**************************************************************************
    * @description - Method to update CMG working Status field on location(after insert).
    * @param newListOfContractSites - trigger.new
    * @param newItemsMap - trigger.newMap
    **************************************************************************/
    public static void updateLocationCMGWorkingStatus(List<Contract_Site__c> newListOfContractSites,Map<Id,Contract_Site__c> newItemsMap){
        Map<Id,String> oppMap=new Map<Id,String>();
        Set<Id> oppIdSet=new Set<Id>();
        Map<Id,Location_ID__c> mapLocIdToLocation = new Map<Id,Location_ID__c>();
        for(Contract_Site__c contractsite:newListOfContractSites){
            oppMap.put(contractsite.Id,contractsite.opportunity__c);
            oppIdSet.add(contractsite.opportunity__c);
        }
        for(opportunity opp:[select id,recordtype.developerName from opportunity where id IN:oppIdSet]){
            oppMap.put(opp.id,opp.RecordType.developerName);
        }
        for(Contract_Site__c contractsite:newListOfContractSites){
            if(oppMap.get(contractsite.Opportunity__c)==Label.SI_Backhaul_ContractRT && (contractsite.Deselect_Type__c!=Label.SI_Deselect_Type) && string.isNotBlank(contractsite.Location_ID__c)){
                Location_ID__c objLocation = new Location_ID__c();
                objLocation.Id = contractsite.Location_ID__c;      
                objLocation.CMG_Working_Status__c=Label.SI_LocationCMGStatus;
                mapLocIdToLocation.put(objLocation.Id,objLocation);
            }
        }
        if(!mapLocIdToLocation.isEmpty()){
            try{
                //update location records
                update mapLocIdToLocation.values();
            } catch(Exception e){
                System.debug(LoggingLevel.ERROR,'The following exception has occurred: ' + e.getMessage());
            }
        }
    }
}
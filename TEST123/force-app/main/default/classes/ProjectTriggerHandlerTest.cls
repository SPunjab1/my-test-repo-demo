/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ProjectTriggerHandlerTest
*  @Author           : Matthew Elias (copying IBM - LocationTriggerHandlerTest)
*  @Version History  : 1.0
*  @Creation         : 22.07.2020
*  @Description      : Test class for ProjectTriggerHandler
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class ProjectTriggerHandlerTest {
    
/***************************************************************************************************
* @Description  : Test Method to test createProjects 
***************************************************************************************************/
      static  testmethod void runTestcreateProjects() {
          List<Location_ID__c> locationList = new List<Location_ID__c>();
          List<Project__c> projectList = new List<Project__c>();
          List<ProjectId__e> ProjectEventLst = new List<ProjectId__e>();                   
          
          locationList = ProjectTestDataFactory.createLocationId('123', 'Test Location', 1);
          try{
              insert locationList;
          } catch (Exception ex){
              throw ex;
          }
          Location_ID__c testLocation = [SELECT Id, Name FROM Location_ID__c LIMIT 1];
          projectList = ProjectTestDataFactory.createProjects(200, 'Test Project', testLocation.Id);
          ProjectEventLst = ProjectTestDataFactory.createProjectEvents(200, 'Test Project', NULL);
          Test.startTest();
          insert projectList;
          EventBus.publish(ProjectEventLst);
          Test.stopTest();
          Test.getEventBus().deliver();
    } 
    
    /*
    //Still need to edit past this
    static  testmethod void runTestcreateLocationsSuccess() {
           List<Location_ID__c> locationIdList = new List<Location_ID__c>();
           List<LocationId__e> LocationEventLst = new List<LocationId__e>();                   
           
           Test.startTest();
           List<Switch_ID__c> switchIdLst = new List<switch_ID__c>();
           switchIdLst=TestDataFactory.createSwitchIds('1234','1234',1);
           insert switchIdLst;
           
           LocationId__e event = new LocationId__e();
           event.Site_cd__c = '1234';
           event.Switch_ID__c = '1234';
           
           Location_ID__c loc = new Location_ID__c();
           loc.Location_ID__c = '1234';
           loc.Switch__c = switchIdLst[0].Id;
           insert loc;
    	    Integration_Log__c log = new Integration_Log__c();
            log.API_Type__c = 'DWH Location';
            log.Location__c = loc.Id;
            insert log;
        
           EventBus.publish(event);
           Test.stopTest();
           Test.getEventBus().deliver();
    }    
    
    static  testmethod void runTestcreateLocationsFailure() {
           List<Location_ID__c> locationIdList = new List<Location_ID__c>();
           List<LocationId__e> LocationEventLst = new List<LocationId__e>();                   
           
           Test.startTest();
           List<Switch_ID__c> switchIdLst = new List<switch_ID__c>();
           switchIdLst=TestDataFactory.createSwitchIds('1234','1234',1);
           insert switchIdLst;
           
           LocationId__e event = new LocationId__e();
           event.Site_cd__c = '1234';
           event.Switch_ID__c = '1234';
           event.REGION_NAME__c = 'East1';
           
           Location_ID__c loc = new Location_ID__c();
           loc.Location_ID__c = '1234';
           loc.Switch__c = switchIdLst[0].Id;
           insert loc;
    
           Integration_Log__c log = new Integration_Log__c();
            log.API_Type__c = 'DWH Location';
            log.Location__c = loc.Id;
            insert log;
        
           EventBus.publish(event);
           Test.stopTest();
           Test.getEventBus().deliver();
    }
	*/
}
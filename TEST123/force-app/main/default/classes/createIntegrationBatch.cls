/**********************************************************************************************************************/
global class createIntegrationBatch implements Database.Batchable<sObject>,Database.Stateful{
/***************************************************************************************
* @Description  :Start Method of Batch Class to initialize count and return Case list
* @input params :Database.BatchableContext
***************************************************************************************/ 

    global Set<Id> failureIdsSet = new Set<Id>();
    global Set <id>successIdsSet=new Set<id>();
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        ID workOrderRecordTaskType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        
        string query='select id,status,(Select task__c from Integration_Logs1__r where api_type__c=\'Attachment\' ) from case where recordtypeid=:workOrderRecordTaskType AND Attachment_Required__c=true and status=\'closed\' and Migrated_Case__c=true';
        
        return Database.getQueryLocator(query);
    }
    
    /*********************************************************************************************************
* @Description  : Execute Method of Batch Class to update Provious closed task checkbox
* @input params :Database.BatchableContext,List<Case>
**********************************************************************************************************/    
    global void execute(Database.BatchableContext BC, List<case> scope) {
        
        set<id>contentDocumentId=new set<id>();
        Map<id,case>finalCaseMap=new Map<id,case>();
        Map<id,List<id>>TaskIdContentDocumentIdMap=new map<id,list<id>>();
        Map<id, ContentVersion> ContentDocumentIdContentVersionMap = new Map<id, ContentVersion>();
        List<Integration_Log__c> integrationLogsToInsert = new List<Integration_Log__c>();
        if(scope.size()>0 && scope!=null){
        for(case c:scope)
        {
            if(c.Integration_Logs1__r.size()==0 )
                finalCaseMap.put(c.id,c);
        }
       } 
       if(finalCaseMap.keyset().size()>0 && finalCaseMap.keyset()!=null){
        for(ContentDocumentLink cd: [SELECT Id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN : finalCaseMap.keySet()]){
            
            if(!TaskIdContentDocumentIdMap.containsKey(cd.LinkedEntityId))
                TaskIdContentDocumentIdMap.put(cd.LinkedEntityId,new list<id>{cd.ContentDocumentId});
            else
                TaskIdContentDocumentIdMap.get(cd.LinkedEntityId).add(cd.ContentDocumentId);
        }
       }
       if(TaskIdContentDocumentIdMap.values()!=null && TaskIdContentDocumentIdMap.values().size()>0){
        for(list<id> idlist:TaskIdContentDocumentIdMap.values())
        {   for(id i :idlist)
            contentDocumentId.add(i);
        }
       }
       if(contentDocumentId!=null && contentDocumentId.size()>0){
        for(ContentVersion cv:[select id ,title ,ContentDocumentId from ContentVersion where ContentDocumentId in :contentDocumentId]){
            ContentDocumentIdContentVersionMap.put(cv.ContentDocumentId, cv);
        }
       }
        if(finalCaseMap.values()!=null && finalCaseMap.values().size()>0){
        for (case cs:finalCaseMap.values()){
            if(TaskIdContentDocumentIdMap.get(cs.Id)!=null && ContentDocumentIdContentVersionMap.size()>0){
                for(id i:TaskIdContentDocumentIdMap.get(cs.id)){ 
                    Integration_Log__c log = new Integration_Log__c();
                    log.Task__c = cs.id;
                    log.API_Type__c = 'Attachment';
                    log.Attachment_Id__c = ContentDocumentIdContentVersionMap.get(i).Id;
                    log.Attachment_Name__c = ContentDocumentIdContentVersionMap.get(i).Title ;
                    integrationLogsToInsert.add(log);
                    
                }
            }
        }
       }
        if(integrationLogsToInsert!=null && integrationLogsToInsert.size()>0)
        {
            Database.Saveresult[] srList=database.insert (integrationLogsToInsert,false);    
            for (Integer i=0;i<srList.size();i++) {
                if (!srList.get(i).isSuccess()){
                    
                    failureIdsSet.add(integrationLogsToInsert.get(i).task__c);
                }
                else if(srList.get(i).isSuccess())
                {
                    successIdsSet.add(integrationLogsToInsert.get(i).task__c);
                }
            }
            
            
        }
    }
    /*********************************************************************************************************
* @Description  : Finish Method of Batch Class
* @input params : Database.BatchableContext
**********************************************************************************************************/ 
    global void finish(Database.BatchableContext BC) {
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors,JobItemsProcessed,TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        
        String[] toAddresses = new String[] {'artfogl1@in.ibm.com','pushpendra.kumar21@ibm.com'};
            String finalstr = 'Task Id \n';
        String attName = 'List of tasks.csv'; 
        for(id t:successIdsSet)
        {
            string recordString = '"'+t+'"\n';
            finalstr = finalstr +recordString;
        }
        efa.setFileName(attName);
        efa.setBody(Blob.valueOf(finalstr));
        mail.setToAddresses(toAddresses);
        mail.setSubject('List of tasks whose integration logs create successfully ' + a.Status);
        mail.setPlainTextBody( 'No of Records processed successfully: '+successIdsSet.size() +' with '+ failureIdsSet.size()+ ' failures.'+ '\n List of failed records are:'+failureIdsSet);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
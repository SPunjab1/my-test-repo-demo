/**
 *@Desc: A class that can be used to insert and update multiple custom metadata records in a single transaction. 
 **/
public class BulkUploadMetadata {    
    /**
     *@desc: A method to insert multiple records for custom metadata object in a single go. 
	 **/
    public static void insertbulkMetadata(string labelin, 
                                          string developername, 
                                          string WorkflowTypec, 
                                          Integer TaskOrderc,
                                          String TaskDescriptionc,
                                          Integer TaskSLAc,
                                          String TaskTypeJobFunctionc,
                                          String AssignmentGroupc,
                                          String SALESFORCEYesNoc,
                                          Boolean PierYesNoc,
                                          String PierChangeServicec,
                                          String AssignmentGroupCategoryc, 
                                          Boolean ExecutiveLevelTaskYesNoc, 
                                          String ExecutiveLevelNamec,
                                          Boolean NextPIERTaskc,	
                                          Boolean LastPIERTaskc,
                                          Boolean ApprovalRequiredc,
                                          Boolean ChgMgmtReqc,
                                          Boolean WorklogEntryRequiredc,
                                          Boolean AttachmentRequiredc,
                                          Boolean LastTaskc
                                         ){
        try{
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            String nameSpacePrefix =''; // if the metadata belongs to any package than include the namespace.                
            //First Record 
            Metadata.CustomMetadata firstMetadataRec =  new Metadata.CustomMetadata();
            
            //firstMetadataRec.fullName = nameSpacePrefix + 'Workflow_Template__mdt.TDM_Disconnects_1';
            firstMetadataRec.fullName = nameSpacePrefix + developername;
            firstMetadataRec.label = labelin;
            
            //adding values to fields
            Metadata.CustomMetadataValue customField1 = new Metadata.CustomMetadataValue();
            customField1.field = 'Workflow_Type__c';
            customField1.value = WorkflowTypec;
            firstMetadataRec.values.add(customField1); 
            
            Metadata.CustomMetadataValue customField2 = new Metadata.CustomMetadataValue();
            customField2.field = 'Task_Order__c';
            customField2.value = TaskOrderc;
            firstMetadataRec.values.add(customField2); 
            
            Metadata.CustomMetadataValue customField3 = new Metadata.CustomMetadataValue();
            customField3.field = 'Task_Description__c';
            customField3.value = TaskDescriptionc;
            firstMetadataRec.values.add(customField3); 
            
            Metadata.CustomMetadataValue customField4 = new Metadata.CustomMetadataValue();
            customField4.field = 'Task_SLA__c';
            customField4.value = TaskSLAc;
            firstMetadataRec.values.add(customField4);
            
            Metadata.CustomMetadataValue customField5 = new Metadata.CustomMetadataValue();
            customField5.field = 'Task_Type_Job_Function__c';
            customField5.value = TaskTypeJobFunctionc;
            firstMetadataRec.values.add(customField5);
            
            Metadata.CustomMetadataValue customField6 = new Metadata.CustomMetadataValue();
            customField6.field = 'Assignment_Group__c';
            customField6.value = AssignmentGroupc;
            firstMetadataRec.values.add(customField6);
            
            Metadata.CustomMetadataValue customField7 = new Metadata.CustomMetadataValue();
            customField7.field = 'SALESFORCE_Yes_No__c';
            customField7.value = SALESFORCEYesNoc;
            firstMetadataRec.values.add(customField7);
            
            Metadata.CustomMetadataValue customField8 = new Metadata.CustomMetadataValue();
            customField8.field = 'Pier_Yes_No__c';
            customField8.value = PierYesNoc;
            firstMetadataRec.values.add(customField8);
            
            Metadata.CustomMetadataValue customField9 = new Metadata.CustomMetadataValue();
            customField9.field = 'Pier_Change_Service__c';
            customField9.value = PierChangeServicec;
            firstMetadataRec.values.add(customField9);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField10 = new Metadata.CustomMetadataValue();
            customField10.field = 'Assignment_Group_Category__c';
            customField10.value = AssignmentGroupCategoryc;
            firstMetadataRec.values.add(customField10);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField11 = new Metadata.CustomMetadataValue();
            customField11.field = 'Executive_Level_Task_Yes_No__c';
            customField11.value = ExecutiveLevelTaskYesNoc;
            firstMetadataRec.values.add(customField11);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField12 = new Metadata.CustomMetadataValue();
            customField12.field = 'Executive_Level_Name__c';
            customField12.value = ExecutiveLevelNamec;
            firstMetadataRec.values.add(customField12);
            mdContainer.addMetadata(firstMetadataRec);                          
            //	
            
            Metadata.CustomMetadataValue customField13 = new Metadata.CustomMetadataValue();
            customField13.field = 'Next_PIER_Task__c';
            customField13.value = NextPIERTaskc;
            firstMetadataRec.values.add(customField13);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField14 = new Metadata.CustomMetadataValue();
            customField14.field = 'Last_PIER_Task__c';
            customField14.value = LastPIERTaskc;
            firstMetadataRec.values.add(customField14);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField15 = new Metadata.CustomMetadataValue();
            customField15.field = 'Approval_Required__c';
            customField15.value = ApprovalRequiredc;
            firstMetadataRec.values.add(customField15);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField16 = new Metadata.CustomMetadataValue();
            customField16.field = 'Chg_Mgmt_Req__c';
            customField16.value = ChgMgmtReqc;
            firstMetadataRec.values.add(customField16);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField17 = new Metadata.CustomMetadataValue();
            customField17.field = 'Worklog_Entry_Required__c';
            customField17.value = WorklogEntryRequiredc;
            firstMetadataRec.values.add(customField17);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField18 = new Metadata.CustomMetadataValue();
            customField18.field = 'Attachment_Required__c';
            customField18.value = AttachmentRequiredc;
            firstMetadataRec.values.add(customField18);
            mdContainer.addMetadata(firstMetadataRec);
            
            Metadata.CustomMetadataValue customField19 = new Metadata.CustomMetadataValue();
            customField19.field = 'Last_Task__c';
            customField19.value = LastTaskc;
            firstMetadataRec.values.add(customField19);
            mdContainer.addMetadata(firstMetadataRec);
           
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            system.debug('jobId***'+jobId);
        }
        catch(Exception ex){             
            //System.assert(false,ex.getMessage()); 
            system.debug('Error while creating modifying custom metadata.');
        }
    }
    /**
     *@desc: A method to bulk update the custom metadata records . 
     **/
    /*
    public void updateBulkMetadata(){
        try{
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            for(Bulk_Upload__mdt objMetadata :[SELECT Id, DeveloperName,
                                           		  MasterLabel, Label, RecName__c,
                                           		  NameType__c, Is_Displayed__c
                                           FROM Bulk_Upload__mdt]){
                Metadata.CustomMetadata metadataRec =  new Metadata.CustomMetadata();
                metadataRec.fullName = 'Bulk_Upload__mdt.'+objMetadata.DeveloperName;
                metadataRec.label = objMetadata.MasterLabel;
                Metadata.CustomMetadataValue customFieldtoUpdate = new Metadata.CustomMetadataValue();
                customFieldtoUpdate.field = 'Is_Displayed__c';
                customFieldtoUpdate.value = false;
                metadataRec.values.add(customFieldtoUpdate);
                mdContainer.addMetadata(metadataRec);
            }
            system.debug('mdContainer**'+mdContainer);            
            // Enqueue custom metadata deployment
            // jobId is the deployment ID
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            system.debug('jobId***'+jobId);                                   
        }catch(exception ex){
            system.debug('exception '+ex.getMessage());                                 
        }       
    }    */
}
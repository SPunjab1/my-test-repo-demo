public without sharing class OpportunityHistoryTrackerHandler {
    
    public static void CreateOpportunityHistoryTrackerRecord(Map<id,Opportunity> oldMap, Map<id,Opportunity> newMap ){
        
        Map<Id,User> opportunityNewVsUser = new Map<Id,User>();
        Map<Id,User> opportunityOldVsUser = new Map<Id,User>();
        Map<Id,Id> userIdVsOpportunityNew = new Map<Id,Id>();
        Map<Id,Id> userIdVsOpportunityOld = new Map<Id,Id>();
        
        SObjectType opportunitysObject = Schema.getGlobalDescribe().get('Opportunity');
        Map<String,Schema.SObjectField> fields = opportunitysObject.getDescribe().fields.getMap();
        List<Opportunity_History_Tracker__c> lstHistory=new List<Opportunity_History_Tracker__c>();
        
         for(Id oppId: newMap.keySet()){
            if(newMap.get(oppId).Current_Approver__c != null){
                 userIdVsOpportunityNew.put( newMap.get(oppId).Current_Approver__c,oppId);
            }
            if(oldMap.get(oppId).Current_Approver__c != null){
                 userIdVsOpportunityOld.put( oldMap.get(oppId).Current_Approver__c,oppId);
            }
        }
        for(User muser : [Select Id,name from User where Id in :userIdVsOpportunityNew.keySet()]){
            String oppId = userIdVsOpportunityNew.get(muser.Id);
            opportunityNewVsUser.put(oppId,muser);
        }
        
        for(User muser : [Select Id,name from User where Id in :userIdVsOpportunityOld.keySet()]){
            String oppId = userIdVsOpportunityOld.get(muser.Id);
            opportunityOldVsUser.put(oppId,muser);
        }
        
        for(Id oppId: newMap.keySet()){
            for(String fieldName:fields.keySet()){
                if( oldMap.get(oppId).get(fieldName)!= newMap.get(oppId).get(fieldName)){
                    if(fieldName!='IsPrivate' && fieldName!='IsClosed' && 
                       fieldName!='IsWon' && fieldName!='HasOpportunityLineItem' && 
                       fieldName!='LastModifiedDate' && fieldName!='LastModifiedById' && fieldName!='SystemModstamp' && fieldName!='LastActivityDate' &&
                       fieldName!='Submitter_Comments__c' &&
                       fieldName!='Reviewer_Comments__c' &&
                       fieldName!='Internal_Comments__c' &&
                       fieldName!='Busiess_Comments__c' )
                    {
                        if(fieldName=='current_approver__c' )
                        {
                           System.debug('--->'+ fieldName);
                            Opportunity oldOpp = oldMap.get(oppId);
                            Opportunity newOpp = newMap.get(oppId);
                            if(oldOpp.Current_Approver__c != newOpp.Current_Approver__c){
                                Opportunity_History_Tracker__c objHistory=new Opportunity_History_Tracker__c();
                                objHistory.opportunity__c=oppId;
                                objHistory.field_Name__c=fields.get(fieldName).getDescribe().getLabel();
                                if(opportunityNewVsUser.containsKey(oppId)){
                                    objHistory.new_Value__c=( newMap.get(oppId).get(fieldName)==null?'Blank':opportunityNewVsUser.get(oppId).Name);
                                }else{
                                    objHistory.new_Value__c = 'Blank';
                                }
                                if(opportunityOldVsUser.containsKey(oppId)){
                                    objHistory.old_value__c=( oldMap.get(oppId).get(fieldName)==null?'Blank':opportunityOldVsUser.get(oppId).Name);
                                }else{
                                    objHistory.old_value__c= 'Blank';
                                }
                                // objHistory.old_value__c=( oldMap.get(oppId).get(fieldName)==null?'Blank':OldusrObj.Name);
                                
                                lstHistory.add(objHistory);
                            }
                            //--->0052E00000Iboh1QAB
                           /* User OldusrObj = new User();
                            if(String.isNotBlank(oldOpp.Current_Approver__c))
                            {
                                OldusrObj= [Select Id,name from User where Id =:oldOpp.Current_Approver__c];   
                            }
                             User newusrObj =  new User();
                             if(String.isNotBlank(newOpp.Current_Approver__c))
                             {
                                 newusrObj =  [Select Id,name from User where Id =:newOpp.Current_Approver__c];
                             }
                              
                            
                            System.debug(oldOpp.Current_Approver__r.Name+'--->'+ newOpp.Current_Approver__r.Name);
                            System.debug( oldOpp.Current_Approver__c +'--->'+ newOpp.Current_Approver__c);
                            System.debug( OldusrObj.Name +'--->'+ newusrObj.Name);
                            Opportunity_History_Tracker__c objHistory=new Opportunity_History_Tracker__c();
                            objHistory.opportunity__c=oppId;
                            objHistory.field_Name__c=fields.get(fieldName).getDescribe().getLabel();
                            objHistory.old_value__c=( oldMap.get(oppId).get(fieldName)==null?'Blank':OldusrObj.Name);
                            objHistory.new_Value__c=( newMap.get(oppId).get(fieldName)==null?'Blank':newusrObj.Name);
                            lstHistory.add(objHistory);*/
                        }else if(String.valueOf(fields.get(fieldName).getDescribe().getType()).contains('REFERENCE'))
                        {
                            
                        }
                        else
                        {
                            Opportunity_History_Tracker__c objHistory=new Opportunity_History_Tracker__c();
                            objHistory.opportunity__c=oppId;
                            objHistory.field_Name__c=fields.get(fieldName).getDescribe().getLabel();
                            objHistory.old_value__c=( oldMap.get(oppId).get(fieldName)==null?'Blank':String.valueOf( oldMap.get(oppId).get(fieldName)));
                            objHistory.new_Value__c=( newMap.get(oppId).get(fieldName)==null?'Blank':String.valueOf( newMap.get(oppId).get(fieldName)));
                            lstHistory.add(objHistory);     
                        }
                    }
                    
                }
            }
        }
        if(!lstHistory.isEmpty()){
            upsert lstHistory;
        }
    }
}
/*public without sharing class OpportunityHistoryTrackerHandler {
    
    public static void CreateOpportunityHistoryTrackerRecord(Map<id,Opportunity> oldMap, Map<id,Opportunity> newMap ){
        SObjectType opportunitysObject = Schema.getGlobalDescribe().get('Opportunity');
        Map<String,Schema.SObjectField> fields = opportunitysObject.getDescribe().fields.getMap();
        List<Opportunity_History_Tracker__c> lstHistory=new List<Opportunity_History_Tracker__c>();
        for(Id oppId: newMap.keySet()){
            for(String fieldName:fields.keySet()){
                if( oldMap.get(oppId).get(fieldName)!= newMap.get(oppId).get(fieldName)){
                    if(fieldName!='IsPrivate' && fieldName!='IsClosed' && 
                       fieldName!='IsWon' && fieldName!='HasOpportunityLineItem' && 
                       fieldName!='LastModifiedDate' && fieldName!='LastModifiedById' && fieldName!='SystemModstamp' && fieldName!='LastActivityDate' &&
                       fieldName!='Submitter_Comments__c' &&
                       fieldName!='Reviewer_Comments__c' &&
                       fieldName!='Internal_Comments__c' &&
                       fieldName!='Busiess_Comments__c' )
                    {
                        Opportunity_History_Tracker__c objHistory=new Opportunity_History_Tracker__c();
                        objHistory.opportunity__c=oppId;
                        objHistory.field_Name__c=fields.get(fieldName).getDescribe().getLabel();
                        objHistory.old_value__c=( oldMap.get(oppId).get(fieldName)==null?'Blank':String.valueOf( oldMap.get(oppId).get(fieldName)));
                        objHistory.new_Value__c=( newMap.get(oppId).get(fieldName)==null?'Blank':String.valueOf( newMap.get(oppId).get(fieldName)));
                        lstHistory.add(objHistory);     
                    }
                    
                }
            }
        }
        if(!lstHistory.isEmpty()){
            upsert lstHistory;
        }
    }
}

*/
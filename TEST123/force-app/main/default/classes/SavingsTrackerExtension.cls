public class SavingsTrackerExtension {
    //API to prepare picklist options for fields which will be used in UI
    //arguments - 
    //return value - map where key is field api name and value is collection of picklist options of the field
    @AuraEnabled
    public static Map<String,Map<String,String>> prepareFieldOptions(){
        Map<String,Map<String,String>> fieldOptionsMap = new Map<String,Map<String,String>>();
        fieldOptionsMap.put('Savings_Status__c',fetchFieldOptions('Savings_Tracker__c','Savings_Status__c'));
        fieldOptionsMap.put('Type_of_Baseline_for_this_deal__c',fetchFieldOptions('Savings_Tracker__c','Type_of_Baseline_for_this_deal__c'));
        fieldOptionsMap.put('Step2_Old_Final_Payment_Terms__c',fetchFieldOptions('Savings_Tracker__c','Step2_Old_Final_Payment_Terms__c'));
        fieldOptionsMap.put('Step2_New_Final_Payment_Terms__c',fetchFieldOptions('Savings_Tracker__c','Step2_New_Final_Payment_Terms__c'));
        fieldOptionsMap.put('Step3_Forecasted_Volume_Source__c',fetchFieldOptions('Savings_Tracker__c','Step3_Forecasted_Volume_Source__c'));
        return fieldOptionsMap;
    }
    //API to retrieve Request__c record
    //arguments - Request__c/Savings_Tracker__c record id(String)
    //return value - Request__c record
    @AuraEnabled
    public static Request__c fetchRequestRecord(String requestId){
        String savingsTrackerKeyPrefix = Savings_Tracker__c.sObjectType.getDescribe().getKeyPrefix();
        Request__c request;
        if(requestId.startsWith(savingsTrackerKeyPrefix)){
            Savings_Tracker__c savingsTracker = [SELECT Id, Request__r.Id, Request__r.Name, Request__r.Title__c, Request__r.Spend_Category__c, 
                                                 Request__r.Estimated_Spend_in_first_12_months__c, Request__r.Owner.Id, Request__r.Owner.Name 
                                                 FROM Savings_Tracker__c WHERE Id=:requestId];
            request = savingsTracker.Request__r;
        }else{
            request = [SELECT Id, Name, Title__c, Spend_Category__c, Estimated_Spend_in_first_12_months__c, Owner.Name FROM Request__c WHERE Id=:requestId LIMIT 1];
        }
        return request;
    }
    //API to retrieve Savings_Tracker__c record
    //arguments - Request__c/Savings_Tracker__c record id(String)
    //return value - Savings_Tracker__c record
    @AuraEnabled
    public static Savings_Tracker__c fetchSavingsTracker(String savingsTrackerId){
        String requestKeyPrefix = Request__c.sObjectType.getDescribe().getKeyPrefix();
        Savings_Tracker__c savingsTracker;
        if(String.isNotBlank(savingsTrackerId)){
            Map<String, SObjectField> fieldMap = Savings_Tracker__c.sObjectType.getDescribe().fields.getMap();
            String soql = 'SELECT ';
            soql += String.join(new List<String>(fieldMap.keySet()), ',');
            if(savingsTrackerId.startsWith(requestKeyPrefix)){
                soql += ', CreatedBy.Name, LastModifiedBy.Name, Assigned_Sourcing_Manager__r.Name FROM Savings_Tracker__c WHERE Request__c=:savingsTrackerId LIMIT 1';
            }else{
                soql += ', CreatedBy.Name, LastModifiedBy.Name, Assigned_Sourcing_Manager__r.Name FROM Savings_Tracker__c WHERE Id=:savingsTrackerId LIMIT 1';
            }
            system.debug(LoggingLevel.ERROR,'soql##'+soql);
            List<Savings_Tracker__c> savingsTrackerList = Database.query(soql);
            if(savingsTrackerList.isEmpty()){
                savingsTracker = new Savings_Tracker__c();
            }else{
                savingsTracker = savingsTrackerList[0];
            }
            system.debug(LoggingLevel.ERROR,'savingsTracker##'+savingsTracker);
        }else{
            savingsTracker = new Savings_Tracker__c();
        }
        
        if(savingsTracker.Id == null){
            savingsTracker.savings_status__c = 'Estimate';
        }
        return savingsTracker;
    }
    //API to save Savings_Tracker__c record
    //arguments - Savings_Tracker__c record(Savings_Tracker__c)
    //return value - Savings_Tracker__c record
    @AuraEnabled
    public static Savings_Tracker__c saveSavingsTracker(Savings_Tracker__c savingsTracker){
        upsert savingsTracker;
        return savingsTracker;
    }
    //API to retrieve logged in user's record
    //arguments - 
    //return value - User record
    @AuraEnabled
    public static User fetchLoggedInUser(){
        String loggedInUserId = UserInfo.getUserId();
        User loggedInUser = [SELECT Id, Name FROM User WHERE Id=:loggedInUserId LIMIT 1];
        return loggedInUser;
    }
    //API to prepare picklist options of a field in object
    //arguments - object api name(String), field api name(String)
    //return value - //return value - map where key is field picklist option api name and value is picklist option label
    private static Map<String,String> fetchFieldOptions(String objectAPIName, String fieldAPIName){
        Map<String,String> options = new Map<String,String>();
        Schema.sObjectType sObjType = Schema.getGlobalDescribe().get(objectAPIName).newSObject().getSObjectType();
        Map<String, Schema.SObjectField> sObjFieldMap = sObjType.getDescribe().fields.getMap();
        List<Schema.PicklistEntry> fieldPickListEntryList = sObjFieldMap.get(fieldAPIName).getDescribe().getPickListValues();
        for(Schema.PicklistEntry fieldPickListEntry : fieldPickListEntryList){
            if(fieldPickListEntry.isActive()){
                options.put(fieldPickListEntry.getValue(),fieldPickListEntry.getLabel());
            }
        }
        return options;
    }
}
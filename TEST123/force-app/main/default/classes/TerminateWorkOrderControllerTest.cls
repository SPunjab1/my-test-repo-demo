/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : TerminateWorkOrderControllerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 26.07.2019
*  @Description      :Test class for TerminateWorkOrderController
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class TerminateWorkOrderControllerTest {

/*************************************************************************************
* @Description :Test Method for CheckWOstatus
*************************************************************************************/
    static testmethod void testCheckWOstatus() { 
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> casesWithShiftTaskList = new List<Case>();
        String WorkflowTmpName='OM New AAV & DAS Install';
        Integer NumOfRec=1;
        casesWithShiftTaskList= TestDataFactory.createCase(WorkflowTmpName,NumOfRec,recTypeId);
        Test.startTest();
        User u = TestDataFactory.createSourcingUser();
        System.runAs(u){
            Insert casesWithShiftTaskList;
            Boolean flag = TerminateWorkOrderController.checkWOstatus(casesWithShiftTaskList[0].Id);
            Test.stopTest();
            System.assertEquals(flag,false);
        }
    }

/*************************************************************************************
* @Description :Test Method for CheckWOstatus
*************************************************************************************/
    static testmethod void testCheckWOstatusNegative() { 
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> casesWithShiftTaskList = new List<Case>();
        String WorkflowTmpName='OM New AAV & DAS Install';
        Integer NumOfRec=1;
        casesWithShiftTaskList= TestDataFactory.createCase(WorkflowTmpName,NumOfRec,recTypeId);
        User u = TestDataFactory.createSourcingUser();
        System.runAs(u){
            Test.startTest();
            Insert casesWithShiftTaskList; 
            Case cs =new Case(Id =  casesWithShiftTaskList[0].Id,status = 'Terminated');
            update cs;
            Boolean flag = TerminateWorkOrderController.checkWOstatus(casesWithShiftTaskList[0].Id);
            Test.stopTest();
            System.assertEquals(flag,true);
        }
    }
/*************************************************************************************
* @Description :Test Method for TerminateCurrentWorkOrder
*************************************************************************************/
    static testmethod void testTerminateCurrentWorkOrder() { 
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> casesWithShiftTaskList = new List<Case>();
        String WorkflowTmpName='OM New AAV & DAS Install';
        Integer NumOfRec=1;
        casesWithShiftTaskList= TestDataFactory.createCase(WorkflowTmpName,NumOfRec,recTypeId);
        User u = TestDataFactory.createSourcingUser();
        System.runAs(u){
        	Test.startTest();
            Insert casesWithShiftTaskList; 
            TerminateWorkOrderController.terminateCurrentWorkOrder(casesWithShiftTaskList[0].Id);    
        }
        Test.stopTest();
    }
}
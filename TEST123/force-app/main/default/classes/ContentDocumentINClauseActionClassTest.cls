/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : ContentDocumentINClauseActionClassTest
*  @Author           : Matthew Elias
*  @Version History  : 1.0
*  @Creation         : 02.09.20
*  @Description      : Test class for ContentDocumentINClauseActionClass
**********************************************************************************************************************
*********************************************************************************************************************/
@isTest(SeeAllData=false)
public class ContentDocumentINClauseActionClassTest {
/***************************************************************************************************
* @Description : Test Method to test ContentDocumentINClauseActionClass Method
***************************************************************************************************/
    @isTest static void filterRecordsTest(){
        
        Test.startTest();
        
        List<ContentDocumentINClauseActionClass.FlowInputParameters> listInput = new List<ContentDocumentINClauseActionClass.FlowInputParameters>();
        ContentDocumentINClauseActionClass.FlowInputParameters input = new ContentDocumentINClauseActionClass.FlowInputParameters();
        
        SavingsTrackerTestDataFactory.createRequest(1);
        Map<Id,Request__c> reqMap = new Map<Id,Request__c>([SELECT Id FROM Request__c]);
        Id reqID = NULL;
        
        SavingsTrackerTestDataFactory.createContentDocuments(4);
        Map<Id,ContentDocument> cdMap = new Map<Id,ContentDocument>([SELECT Id FROM ContentDocument]);
        
        Map<Id,Set<Id>> cdlMap = new Map<Id,Set<Id>>();
        for(Id req : reqMap.keySet()){
            cdlMap.put(req,cdMap.keySet());
            reqID = req;
        }
        SavingsTrackerTestDataFactory.createContentDocumentLink(cdlMap);
        
        ContentNote testNote = new ContentNote();
        testNote.Title = 'Test Note';
        testNote.Content = Blob.valueOf('Test Note Content');
        insert testNote;
        
        cdlMap.put(reqID, new Set<Id>{testNote.Id});
        SavingsTrackerTestDataFactory.createContentDocumentLink(cdlMap);
        
        Map<Id,ContentDocumentLink> cdlsMap = new Map<Id,ContentDocumentLink>([SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :reqID]);
        
        System.assertEquals(5,cdlsMap.size());
        
        String[] filteringValues = new List<String>();
        for(Id cdl : cdlsMap.keySet()){
            filteringValues.add(cdlsMap.get(cdl).ContentDocumentId);
        }
        
        input.filteringValuesCollection = filteringValues;
        listInput.add(input);
        
        // call the invocable method directly
        List<ContentDocumentINClauseActionClass.FlowOutputParameters> filteredList = ContentDocumentINClauseActionClass.filterRecords(listInput);
        System.assertEquals(4,filteredList[0].records.size());
        
        Test.stopTest();
        
    }
}
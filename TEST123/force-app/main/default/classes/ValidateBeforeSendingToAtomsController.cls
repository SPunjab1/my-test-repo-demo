/*********************************************************************************************
@author       : IBM
@date         : 14.09.20 
@description  : This is the class for updating OM Integration Status = Start Integration after checking validations
@Version      : 1.0        
**********************************************************************************************/


public with sharing class ValidateBeforeSendingToAtomsController{
    private ApexPages.StandardSetController standardController;
    public Set<String>  errorMsgSet { get; set; } 
    public List<Case> errorIdLst { get; set; } 
    Public static Map<string,ATOMSNeuStarUtilitySettings__c> mapConstants = ATOMSNeuStarUtilitySettings__c.getAll(); 
    public ValidateBeforeSendingToAtomsController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
        errorMsgSet = new Set<String>();
        errorIdLst = new List<Case>();
    }
    public PageReference StartIntegration()
    {       
        List<Case> selectedCases = (List<Case>) standardController.getSelected();
        List<Case> startIntTaskLst = new List<Case>();
        Set<Id> caseIdSet = new Set<Id>();
        List<String> profilesToValidateLst = Label.UsersinProfilesCanStartAtomsInt.split(',');
        for (User uRole : [select Profile.Name, Id from user where Id = :UserInfo.getUserId() AND Profile.Name NOT IN: profilesToValidateLst]) {
           errorMsgSet.add(mapConstants.get('InsufficientAccessRole').Value__c);
           return null;
        }
           
        if(selectedCases.size() == 0){
            errorMsgSet.add(mapConstants.get('Selectatleastonerecord').Value__c);
            return null;
        }
        for(Case selCase : selectedCases){
            caseIdSet.add(selCase.Id);
        }
        if(caseIdSet.Size() > 0){
            for(Case cs : [SELECT Id,OM_Integration_Status__c from Case where Id IN: caseIdSet AND RecordType.DeveloperName =: mapConstants.get('Work_Order_Task').Value__c]){
                if(cs.OM_Integration_Status__c !=  mapConstants.get('StartIntegration').Value__c){
                    cs.OM_Integration_Status__c = mapConstants.get('StartIntegration').Value__c;
                    startIntTaskLst.add(cs);
                }
            }
        }
        try{
            if(startIntTaskLst != Null && startIntTaskLst.Size() > 0){
                // Create a savepoint
                Savepoint sp = Database.setSavepoint();
                Database.SaveResult[] updateResults = Database.update(startIntTaskLst,false);
                for(Integer i=0;i<updateResults.size();i++){
                    if(!updateResults.get(i).isSuccess()){
                         String errorMsg ='';
                         for(Database.Error error : updateResults.get(i).getErrors()){errorMsg += error.getMessage() +' ';}
                         errorMsgSet.add(errorMsg);
                    }
                }
            }
        }catch(Exception ex){}
        return new ApexPages.Action('{!List}').invoke();      
    }
    public PageReference back()
    {
        PageReference cancel = standardController.cancel();
        return cancel;
    }


}
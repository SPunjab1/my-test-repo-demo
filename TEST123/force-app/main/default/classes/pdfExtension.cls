/*********************************************************************************************
@Class        : pdfExtension
@author       : Pritesh
@date         : 11.20.2020 
@description  : This class is to for NLG Abtract PDF generation functionality
@Version      : 1.0           
**********************************************************************************************/
public class pdfExtension {
    
    public ApexPages.StandardController stdCntrlr {get; set;}
    public date constDate {set;get;}
    public string acqVal {get;set;}
    public string agrmtHighlights {get;set;}
    public string oppId{get;set;}
    public string rrapp{get;set;}
    public string rent{get;set;}
    public string sign{get;set;}
    public String recId{get;set;}
    public Decimal totalcommitment_fee{get;set;}
    public Decimal rentincrease{get;set;}
    
    public pdfExtension(ApexPages.StandardController controller) { 
        
        stdCntrlr = controller;
        recId = stdCntrlr.getId();
        oppId  = ApexPages.CurrentPage().getparameters().get('Id');
        rrapp = apexpages.currentpage().getparameters().get('rrapp');
        rent = apexpages.currentpage().getparameters().get('rent');
        sign = apexpages.currentpage().getparameters().get('sign');
        Opportunity oppObj = [ SELECT Id,New_Rent__c, New_Rent_Frequency__c, Current_Rent__c, Current_Rent_Frequency__c, New_One_Time_Fees__c, Total_Commitment__c,Exception_List__c FROM Opportunity WHERE Id =:recId ];
        rentincrease = pdfController.rentIncreaseCalculator(oppObj);
        totalcommitment_fee = ( oppObj.New_One_Time_Fees__c != null ? oppObj.New_One_Time_Fees__c :0.00 ) + ( oppObj.Total_Commitment__c != null ? oppObj.Total_Commitment__c :0.00 );
        
    }
    
    public pageReference saveFile(){
        /* Validation for input fields*/
    /*    if(acqVal == null || acqVal=='' ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.fatal,'Site Acq vendor Missing'));
            return null;
        }
        
        if( constDate == null ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.fatal,'Construction Date Missing'));
            return null;
        }
        
        if(agrmtHighlights == null || agrmtHighlights=='' ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.fatal,'Agreement Highlight Missing'));
            return null;
        }*/
        
      //  String dateSring = String.valueof(constDate);
         String dateSring = '';
        if( null != constDate)
        	dateSring = constDate.format();
        
        String returnUrl = '/lightning/cmp/c__NLGAbstractCMP?c__recordId='+oppId+'&c__acqVal='+acqVal + '&c__dateSring=' + dateSring +'&c__agrmtHighlights='+agrmtHighlights+'&c__rrapp='+rrapp+'&c__rent='+rent+'&c__sign='+sign;
        PageReference pgReturnPage = new PageReference(returnUrl);
        pgReturnPage.setRedirect(true);
        return pgReturnPage;        
        
    }
    
    public List<NLG_Util.signingWrapper> getFinalSigningList(){
         return pdfController.getSignAuthorityData(recId);
       
    }
    
}
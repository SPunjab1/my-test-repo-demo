/*********************************************************************************************
@Version    : 2.0 -- June 23 2020
@description: Implemented the logic to call for 
creating/Upgrading/Disconnecting ASR         
@author     : IBM          
**********************************************************************************************/
global class BatchForASRIntergation implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{ 
    
    static List<String> createAsrType = new List<String>();
    static List<String> upgradeAsrType = new List<String>();
    static List<String> updateAsrType = new List<String>();
    static List<String> disconnectAsrType = new List<String>();
    public Boolean bProcessSpecificRcd = false;
    public String asrId {get;set;}

    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query;
        Id thisClassId = Label.ASRIntegrationClass;
        Id thisJobId;
        if(!Test.isRunningTest())
        	thisJobId = bc.getJobId();
        List<String> jobStatus = Label.AtomsJobStatus.Split(',');
        AsyncApexJob[] jobs = [select id from AsyncApexJob where id!=:thisJobId AND status IN: jobStatus AND ApexClassId=:thisClassId AND JobType='BatchApex'];
        if (jobs !=null && jobs.size() > 0) {
            query = 'Select Id from ASR__c LIMIT 0';
        }
        else {
           query = 'SELECT Id, Case__c,'+
            'PON__c,VLAN__c, VLAN_2__c,Order_Number__c,Circuit_Id__r.Purchase_Order_Id__c,API_Status__c,Order_Destination__c,SubService_Type__c,case__r.parent.caseNumber,Case__r.parent.PIER_Work_Order_ID__c,'+
            'ATOMs_ASR_Id__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,'+
            'Circuit_Id__c,Delivered_Date__c,Change_Type__c,Change_Value__c,Forecast_Version__c,Deliver__c,Desired_Due_Date__c,TPE_Request_Number__c,Target_Download_Speed__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c, ASR_Type__c, ' +
            'Circuit_Id__r.NNI_ID_Primary_Key__c, Circuit_Id__r.RecordType.Name'+
            ' from ASR__C WHERE (( (Circuit_Id__r.Circuit_ID_Primary_Key__c!=null OR Circuit_Id__r.NNI_ID_Primary_Key__c != NULL) AND Circuit_Id__r.API_Status__c = \'Successful\' AND ATOMs_ASR_Id__c = null '+
            'AND (Is_Atoms_Creation_Failed__c = false OR Is_Atoms_Creation_Failed__c = null)) OR ((Circuit_Id__r.Circuit_ID_Primary_Key__c!=null OR Circuit_Id__r.NNI_ID_Primary_Key__c != NULL )  AND '+
            'Circuit_Id__r.API_Status__c = \'Successful\')) AND '+
            'Case__r.RecordType.DeveloperName = \'Work_Order_Task\' AND API_Status__c != \'Successful\'';
        
        if(bProcessSpecificRcd){
            query = 'SELECT Id, Case__c,'+
            'PON__c,Order_Number__c,VLAN__c, VLAN_2__c,case__r.parent.caseNumber,Circuit_Id__r.Purchase_Order_Id__c,API_Status__c,Order_Destination__c,SubService_Type__c,Case__r.parent.PIER_Work_Order_ID__c,'+
            'ATOMs_ASR_Id__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,'+
            'Circuit_Id__c,Delivered_Date__c,Forecast_Version__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c, ASR_Type__c, ' +
            'Circuit_Id__r.NNI_ID_Primary_Key__c, Circuit_Id__r.RecordType.Name'+
            ' from ASR__C WHERE (( (Circuit_Id__r.Circuit_ID_Primary_Key__c!=null OR Circuit_Id__r.NNI_ID_Primary_Key__c != NULL) AND Circuit_Id__r.API_Status__c = \'Successful\' AND ATOMs_ASR_Id__c = null '+
            'AND (Is_Atoms_Creation_Failed__c = false OR Is_Atoms_Creation_Failed__c = null)) OR (Circuit_Id__r.Circuit_ID_Primary_Key__c!=null AND '+
            'Circuit_Id__r.API_Status__c = \'Successful\')) AND '+
            'Case__r.RecordType.DeveloperName = \'Work_Order_Task\' AND Id = \''+asrId+'\'' ;
        }
        //System.debug('query >>>>>>>>'+query );
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(SchedulableContext SC) {
        if(!Test.isRunningTest())    
            Database.executeBatch(new BatchForASRIntergation(), integer.valueOf(Label.ASRBatchSize));
    }
    
    global void execute(Database.BatchableContext BC, List<ASR__c> scope) {
        //Declare Variables
        Map<Id,List<ASR__c>> createASRMap = new Map<Id,List<ASR__c>>();
        Map<Id,List<ASR__c>> upgradeASRMap = new Map<Id,List<ASR__c>>();
        Map<Id,List<ASR__c>> disconnectASRMap = new Map<Id,List<ASR__c>>();
        List<Case> updateASROrderIds = new List<Case>();
        List<Case> rehomeOrderIds = new List<Case>();
        List<Case> disconnectOrderIds = new List<Case>();
        //Boolean createASR = false;
        //Boolean upgradeASR = false;
        //Boolean updateASROrderStatus = false;
        //Boolean disconnectASR = false;
        Boolean createASR = false;
        Boolean upgradeASR = false;
        Boolean updateASROrderStatus = false;
        Boolean disconnectASR = false;
        Boolean notContainCircuitType = false;
        Set<Id> purchaseOrderSet = new Set<Id>();
        Map<Id, List<Integration_Log__c>> case_ILMap = new Map<Id, List<Integration_Log__c>>();
        getAsrTypeValues();
        Map<String, SwitchAPICallOuts__c> switchSetting = SwitchAPICallOuts__c.getAll();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRCreateDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRUpgradeDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRDisconnectDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        //Iterate through loop and create separate list for each API
        for(ASR__c asrObj: scope){
            //Boolean createASR = false;
            //Boolean upgradeASR = false;
            //Boolean updateASROrderStatus = false;
            //Boolean disconnectASR = false;
            //System.debug('************'+asrObj.ASR_Type__c);
            if(asrObj.ASR_Type__c!=null){  
                if(createAsrType.CONTAINS(asrObj.ASR_Type__c)){
                    createASR = true;
                    if(createASRMap.containsKey(asrObj.Case__c)){
                        createASRMap.get(asrObj.Case__c).add(asrObj);
                    }
                    else{
                        createASRMap.put(asrObj.Case__c, new List<ASR__c>{ asrObj});
                    }
                    
                }else if(upgradeAsrType.CONTAINS(asrObj.ASR_Type__c) || asrObj.ASR_Type__c == 'In Service'){
                    //system.debug('@@@@@upgradeAsrType -----'+ upgradeASR );
                    upgradeASR = true;
                    if(upgradeASRMap.containsKey(asrObj.Case__c)){
                        upgradeASRMap.get(asrObj.Case__c).add(asrObj);
                    }
                    else{
                        upgradeASRMap.put(asrObj.Case__c, new List<ASR__c>{ asrObj});
                    }
                }else if(disconnectAsrType.CONTAINS(asrObj.ASR_Type__c)){
                    disconnectASR = true;
                    if(disconnectASRMap.containsKey(asrObj.Case__c)){
                        disconnectASRMap.get(asrObj.Case__c).add(asrObj);
                    }
                    else{
                        disconnectASRMap.put(asrObj.Case__c, new List<ASR__c>{ asrObj});
                    }            
                }
                else if(updateAsrType.CONTAINS(asrObj.ASR_Type__c)){
                    updateASROrderStatus = true;                 
                }
            }
        }
        //system.debug('@@@@@upgradeASR -----'+ upgradeASR );
           if(createASR == true){
                //system.debug('Inside createASR');
                listASRCreateDMLWrapper.add(AtomsIntegrationAPI.sendASRRequestToAtoms(createASRMap,'CreateASR',case_ILMap));
                
            }if(upgradeASR == true){
                //system.debug('Inside upgradeASR');
                listASRUpgradeDMLWrapper.add(AtomsIntegrationAPI.sendASRRequestToAtoms(upgradeASRMap,'UpgradeASR',case_ILMap));
                
            }if(disconnectASR == true){
                listASRDisconnectDMLWrapper.add(AtomsIntegrationAPI.sendASRRequestToAtoms(disconnectASRMap,'DisconnectASR',case_ILMap));
            } 
        
        if(listASRCreateDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRCreateDMLWrapper);
        }
        if(listASRUpgradeDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRUpgradeDMLWrapper);
        }
        if(listASRDisconnectDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRDisconnectDMLWrapper);
        }
        
        /* if(createASR == true || updateASROrderStatus == true){
AtomsIntegrationAPI.sendASRRequestToAtoms(createASRMap,'CreateASR',case_ILMap);
}
if(upgradeASR == true || updateASROrderStatus == true){
AtomsIntegrationAPI.sendASRRequestToAtoms(upgradeASRMap,'UpgradeASR',case_ILMap);
}
if(disconnectASR == true || updateASROrderStatus == true) {
AtomsIntegrationAPI.sendASRRequestToAtoms(disconnectASRMap,'DisconnectASR',case_ILMap);
}*/
    }
    
    global void finish(Database.BatchableContext BC){
         database.executebatch(new BatchForNeustarIntegration(),20);     
    }
    
    /********************************************************************************
@Description :This method gets required picklist values from the Circuit/ASR objects
/******************************************************************************/
    
    public static void getAsrTypeValues(){
        Schema.DescribeFieldResult asrTypeFieldResult = ASR__c.Asr_Type__c.getDescribe();
        List<Schema.PicklistEntry> asrTypePle = asrTypeFieldResult.getPicklistValues();
        //system.debug('@@@@@-----asrTypePle '+ asrTypePle);
        for( Schema.PicklistEntry pickListVal : asrTypePle){
            if(pickListVal.getLabel().contains('New Service:')){
                createAsrType.add(pickListVal.getLabel());
            }
            if(pickListVal.getLabel().contains('BW Change:')){
                upgradeAsrType.add(pickListVal.getLabel());
            }
           /* if(pickListVal.getLabel().contains('In Service:')){
                upgradeAsrType.add(pickListVal.getLabel());
                system.debug('@@@@@-----'+ upgradeAsrType);
            }*/
            if(pickListVal.getLabel().contains('Disconnect:')){
                disconnectAsrType.add(pickListVal.getLabel());
            }
            if(pickListVal.getLabel().contains('Update ASR')){
                updateAsrType.add(pickListVal.getLabel());
            }
        }
        //system.debug('-----'+ upgradeAsrType);
        //system.debug('-----'+ disconnectAsrType);       
        
    }
    
}
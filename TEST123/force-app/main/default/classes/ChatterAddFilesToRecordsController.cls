/*********************************************************************************************
@author       : IBM
@date         : 12.06.19 
@description  : This is the class for for bulk upload of files to selected work orders
@Version      : 1.0        
**********************************************************************************************/
public with sharing class ChatterAddFilesToRecordsController{
    
/***************************************************************************************************
* @Description  : Constructor and properties are defined here
***************************************************************************************************/
    public List<ID> recordIds { get; private set; }
    public ChatterAddFilesToRecordsController( ApexPages.StandardSetController stdController ) {
        this.recordIds = new List<ID>();
        for ( SObject obj : stdController.getSelected() ) {
            recordIds.add( obj.id );
        }
        if ( recordIds.size() == 0 ) {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'Please select one or more records.' ) );
        }
    }
    
/***************************************************************************************************
* @Description  : Method to get the sobject describe information from the record ids. 
* @input params : recordId
***************************************************************************************************/
    @AuraEnabled
    public static SObjectDescribeWrapper getSObjectDescribe( List<ID> recordIds ) {
        if ( recordIds.size() > 0 ) {
            return new SObjectDescribeWrapper( recordIds[0].getSObjectType() );
        }
        return null;
    }
    
/***************************************************************************************************
* @Description  : Method to Create ContentDocumentLinks between each record id and file id.. 
* @input params : recordId,fileIds,caseComments
***************************************************************************************************/
    @AuraEnabled
    public static void attachFilesToRecords( List<ID> recordIds, List<ID> fileIds, String caseComments) {
        try {
            Map<String, ContentDocumentLink> existingFileSharesMap = new Map<String, ContentDocumentLink>();
            for ( ContentDocumentLink cdl : [
                SELECT
                id, linkedEntityId, contentDocumentId
                FROM
                ContentDocumentLink
                WHERE
                linkedEntityId IN :recordIds
                AND
                contentDocumentId IN :fileIds
            ]) {
                existingFileSharesMap.put( cdl.linkedEntityId + '_' + cdl.contentDocumentId, cdl );
            }
            // get latest published version id of the selected file ids
            Map<ID, ContentDocument> contentDocumentsMap = new MAp<ID, ContentDocument>([
                SELECT
                id, latestPublishedVersionId
                FROM
                ContentDocument
                WHERE
                id IN :fileIds
            ]);
            List<ContentDocumentLink> fileLinks = new List<ContentDocumentLink>();
            for ( ID recordId : recordIds ) {
                for ( ID fileId : fileIds ) {
                    if ( !existingFileSharesMap.containsKey( recordId + '_' + fileId ) ) {
                        fileLinks.add( new ContentDocumentLink(
                            linkedEntityId = recordId,
                            contentDocumentId = fileId,
                            shareType = 'V'
                        ));
                    } else {
                        
                    }
                }
            }
            List<Worklog_Entry__c> caseCommentsToBeUpdated = new List<Worklog_Entry__c>();
            if(caseComments != Null){
                for(ID caseId : recordIds){
                    caseCommentsToBeUpdated.add(new Worklog_Entry__c(Task__c= caseId, Comment__c = caseComments));
                }
            }
            if ( fileLinks.size() > 0 ) {
                insert fileLinks;
            }
            
            if(caseCommentsToBeUpdated.size() > 0){
                insert caseCommentsToBeUpdated;
            }
        } catch ( Exception e ) {
            // to have message shown to the user instead of swallowed
            throw new AuraHandledException( e.getMessage() );
        }
    }
    
/***************************************************************************************************
* @Description  : Method to get recently viewed files. 
* @input params : page,pageSize
***************************************************************************************************/
    @AuraEnabled
    public static PaginatedContentDocumentWrapper getRecentlyViewedFiles( Integer page, Integer pageSize ) {
        try {
            // There is a bug with lightning components that Integer arguments
            // cannot be used as-is but must be re-parsed with Integer.valueOf.
            // http://michaelwelburn.com/2016/12/06/lightning-components-integers-script-thrown-exceptions/
            // http://salesforce.stackexchange.com/questions/108355/limit-expression-must-be-of-type-integer-error-when-using-apex-variable-in-soq/108423#108423
            page = Integer.valueOf ( page );
            pageSize = Integer.valueOf( pageSize );
            Integer skipRecords = ( page - 1 ) * pageSize;
            Integer maxRecords = pageSize + 1; // grab one more so can tell if there's a next page to get
            Map<ID, RecentlyViewed> recentlyViewedMap = new Map<ID, RecentlyViewed>([
                SELECT
                id
                FROM
                RecentlyViewed
                WHERE
                type = 'ContentDocument'
            ]);
            List<ContentDocument> files = new List<ContentDocument>([
                SELECT
                id, title, fileExtension, fileType, owner.name, lastModifiedDate
                FROM
                ContentDocument
                WHERE
                id IN :recentlyViewedMap.keySet()
                ORDER BY
                lastModifiedDate DESC
                LIMIT
                :maxRecords
                OFFSET
                :skipRecords
            ]);
            PaginatedContentDocumentWrapper wrapper = buildWrapper( files, page, pageSize, ( skipRecords > 0 ), ( files.size() > pageSize ) );
            return wrapper;
        } catch ( Exception e ) {
            // to have message shown to the user instead of swallowed
            throw new AuraHandledException( e.getMessage() );
        }
    }
    
/***************************************************************************************************
* @Description  : Method to search all files 
* @input params : searchTerm,page,pageSize
***************************************************************************************************/
    @AuraEnabled
    public static PaginatedContentDocumentWrapper searchAllFiles( String searchTerm, Integer page, Integer pageSize ) {
        try {
            // There is a bug with lightning components that Integer arguments
            // cannot be used as-is but must be re-parsed with Integer.valueOf.
            // http://michaelwelburn.com/2016/12/06/lightning-components-integers-script-thrown-exceptions/
            // http://salesforce.stackexchange.com/questions/108355/limit-expression-must-be-of-type-integer-error-when-using-apex-variable-in-soq/108423#108423
            page = Integer.valueOf ( page );
            pageSize = Integer.valueOf( pageSize );
            Integer skipRecords = ( page - 1 ) * pageSize;
            Integer maxRecords = pageSize + 1; // grab one more so can tell if there's a next page to get
            List<ContentDocument> files = new List<ContentDocument>();
            if ( String.isNotBlank( searchTerm ) ) {
                List<List<SObject>> searchResults = new List<List<SObject>>([
                    FIND :searchTerm IN NAME FIELDS
                    RETURNING
                    ContentDocument (
                        id, title, fileExtension, fileType, owner.name, lastModifiedDate
                        ORDER BY
                        lastModifiedDate DESC
                        LIMIT
                        :maxRecords
                        OFFSET
                        :skipRecords
                    )
                ]);
                files = (List<ContentDocument>) searchResults[0];
            } else {
                files = new List<ContentDocument>([
                    SELECT
                    id, title, fileExtension, fileType, owner.name, lastModifiedDate
                    FROM
                    ContentDocument
                    ORDER BY
                    lastModifiedDate DESC
                    LIMIT
                    :maxRecords
                    OFFSET
                    :skipRecords
                ]);
            }
            PaginatedContentDocumentWrapper wrapper = buildWrapper( files, page, pageSize, ( skipRecords > 0 ), ( files.size() > pageSize ) );
            return wrapper;
        } catch ( Exception e ) {
            // to have message shown to the user instead of swallowed
            throw new AuraHandledException( e.getMessage() );
        }
    }
    
/***************************************************************************************************
* @Description  : Method for Pagination 
* @input params : files,page,pageSize,hasPrevious,hasNext
***************************************************************************************************/  
    private static PaginatedContentDocumentWrapper buildWrapper( List<ContentDocument> files, Integer page, Integer pageSize, Boolean hasPrevious, Boolean hasNext ) {
        PaginatedContentDocumentWrapper wrapper = new PaginatedContentDocumentWrapper();
        wrapper.page = page;
        wrapper.pageSize = pageSize;
        wrapper.hasPrevious = hasPrevious;
        wrapper.hasNext = hasNext;
        if ( hasNext ) {
            List<ContentDocument> filesToReturn = new List<ContentDocument>( files );
            filesToReturn.remove( filesToReturn.size() - 1 );
            wrapper.files = wrap( filesToReturn );
        } else {
            wrapper.files = wrap( files );
        }
        return wrapper;
    }
    
/***************************************************************************************************
* @Description  : Method for returning ContentDocumentWrapper
* @input params : files
***************************************************************************************************/
    private static List<ContentDocumentWrapper> wrap( List<ContentDocument> files ) {
        List<ContentDocumentWrapper> wrappers = new List<ContentDocumentWrapper>();
        for ( ContentDocument file : files ) {
            wrappers.add( new ContentDocumentWrapper( file ) );
        }
        return wrappers;
    }  
    
/***************************************************************************************************
* @Description  : Class SObjectDescribeWrapper
***************************************************************************************************/
    public class SObjectDescribeWrapper {
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String labelPlural { get; set; }
        @AuraEnabled
        public String keyPrefix { get; set; }
        public SObjectDescribeWrapper( SObjectType sobjectType ) {
            DescribeSObjectResult describe = sobjectType.getDescribe();
            this.name = describe.getName();
            this.label = describe.getLabel();
            this.labelPlural = describe.getLabelPlural();
            this.keyPrefix = describe.getKeyPrefix();
        }
    }
    
/***************************************************************************************************
* @Description  : Class PaginatedContentDocumentWrapper
***************************************************************************************************/
    public class PaginatedContentDocumentWrapper {
        @AuraEnabled
        public Integer page { get; set; }
        @AuraEnabled
        public Integer pageSize { get; set; }
        @AuraEnabled
        public Boolean hasPrevious { get; set; }
        @AuraEnabled
        public Boolean hasNext { get; set; }
        @AuraEnabled
        public List<ContentDocumentWrapper> files { get; set; }
    }
    
/***************************************************************************************************
* @Description  : Class ContentDocumentWrapper
***************************************************************************************************/
    public class ContentDocumentWrapper {
        @AuraEnabled
        public ContentDocument file { get; set; }
        @AuraEnabled
        public Boolean selected { get; set; }
        @AuraEnabled
        public String iconName { get; set; }
        public ContentDocumentWrapper( ContentDocument file ) {
            this( file, false );
        }
        public ContentDocumentWrapper( ContentDocument file, Boolean selected ) {
            this.file = file;
            this.selected = selected;
            this.iconName = 'doctype:attachment';
        }
    }
}
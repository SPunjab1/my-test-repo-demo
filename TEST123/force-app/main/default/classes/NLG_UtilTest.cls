@istest
public class NLG_UtilTest {
    
    @testSetup
    static void testDataSetup() {
        //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //Intialize list of SI Savings Tracker to insert records
        List<SI_Savings_Tracker__c> listSISavingTracker = new  List<SI_Savings_Tracker__c>();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
        // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = listAccount[0].Id;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        insert oppRecForContract;
        List<Site_Lease_Escalation__c> siteLeaseList = new List<Site_Lease_Escalation__c>();
        Site_Lease_Escalation__c siteLease = TestDataFactory.createSiteLEaseEscalation(oppRecForContract.Id);
        //lease.Lease_Intake__c = oppRecForContract.Id;
        siteLease.Lease_Version__c ='New';
        siteLease.Escalation_Type__c ='One Time';
        siteLease.Escalator__c=10000;
        siteLeaseList.add(siteLease);
        Site_Lease_Escalation__c siteLease1 = TestDataFactory.createSiteLEaseEscalation(oppRecForContract.Id);
        //lease.Lease_Intake__c = oppRecForContract.Id;
        siteLease1.Lease_Version__c ='Current';
        siteLease1.Escalation_Type__c ='PCT';
        siteLease1.Escalator__c=3.0;
        siteLease1.Effective_Date__c =system.today().addMonths(12);
        
        siteLeaseList.add(siteLease1);
        insert siteLeaseList;
    }
    
    @istest static void nlgOppCalculatorTest(){
        Test.startTest();
        Opportunity opportunityRec = [Select id,New_Rent__c,Initial_Term_NewLease__c,Remaining_Current_Term_Years__c from Opportunity limit 1];
        opportunityRec.Lease_or_AMD__c ='AMD';
        opportunityRec.New_Rent__c=40000;
        opportunityRec.New_Escalator_Text__c='Escalator';
        opportunityRec.New_Escalator_Frequency__c='Annual';
        opportunityRec.New_Rent_Frequency__c = 'Monthly';
        opportunityRec.New_Rent_Commencement_Date__c = Date.today();   
        opportunityRec.New_Final_Expiration_New_Lease_Date__c= Date.today()+700;
        opportunityRec.Current_Rent_Frequency__c='Monthly';
        opportunityRec.Current_Term_Start_Date__c=Date.today(); 
        opportunityRec.Initial_Term_NewLease__c = 1;
        opportunityRec.Current_Term_End_Date__c = Date.today()+380;
        opportunityRec.Current_Escalator_Frequency__c='Annual';
        opportunityRec.Current_Escalator_txt__c = 'Escalator';
        opportunityRec.Current_Rent__c = 40000;
        
        opportunityRec.LL_Proposed_Rent__c =41000;
        opportunityRec.LL_Proposed_Rent_Frequency__c ='Monthly';
        opportunityRec.LL_Proposed_Escalator__c=3.0;
        opportunityRec.LL_Proposed_Escalator_Frequency__c ='Annual';
        opportunityRec.Revenue_Share__c =1000;
        update opportunityRec;
        opportunityRec = [Select Id,Lease_or_AMD__c,New_Rent__c, New_Total_Term_Length_inc_renewals_Year__c,New_Rent_Commencement_Date__c,Remaining_Current_Term_Years__c, 
                          New_Rent_Frequency__c, New_Escalator_Frequency__c,New_Escalator_Text__c,Current_Rent__c,Current_Total_Term_Length__c, 
                          Current_Rent_Frequency__c, Current_Escalator_Frequency__c,Current_Escalator_txt__c,Rent_Commencement_Date__c,Total_Rent_over_initial_term__c,
                          Total_Rent_over_total_term__c,NPV_over_term__c,Lease_Liablity__c,Avg_Monthly_Rent_o_Term__c,Current_Lease_Liabilty__c,
                          LL_Proposed_Rent__c,LL_Proposed_Rent_Frequency__c,LL_Proposed_Escalator_Frequency__c,New_One_Time_Fees__c,
                          LL_Proposed_Escalator__c,Avoidance__c,Initial_Term_NewLease__c,New_Final_Expiration_New_Lease_Date__c,Final_Expiration_Date_Current_Lease__c,
                          Current_Term_Start_Date__c,Current_Term_End_Date__c,Negotiating_Vendor__c,Number_of_EscalationsCurrent__c,Exception_List__c,Rent_Commencement_Type__c,One_Time_Fees_Amount__c,NSC_Status__c,
                          Revenue_Share__c,Assumed_Rent__c
                          From Opportunity limit 1];
        List<String> oppId = new List<String>();
        oppId.add(opportunityRec.Id);
        NLG_Util.nlgOppCalculator(oppId);
        NLG_Util.calcNewLeaseLiablity(opportunityRec);
        NLG_Util.calcCurrentLeaseLiablity(opportunityRec);
        Map<String,Opportunity> oppMap = new  Map<String,Opportunity>();
        oppMap.put(opportunityRec.Id,opportunityRec);
        NLG_Util.nlgCalculatorList(oppMap);//leaseType=='Current'
        NLG_Util.getEscalation(Date.today(),'Current',30000,'Annual');
        Map<Date,Decimal>rentMap = new Map<Date,Decimal>();
        NLG_Util.calcLiablity(Date.today(),30000,12,10,'Percent',Date.today()+10,rentMap,null,null);
        NLG_Util.addRents(rentMap,30000,12,Date.today());
        NLG_Util.getNPV(2000,2000,1200,12);
        NLG_Util.calculateCurrentBaseRent(2000,2000,1200);
        Test.stopTest();
    }
}
/*********************************************************************************************
@author       : Matthew Elias (copying IBM - LocationTriggerHandler)
@date         : 10.07.2020
@description  : This is the trigger handler to create projects. This is fired from Platform event ProjectId.
@Version      : 1.0         
**********************************************************************************************/
public with sharing class ProjectTriggerHandler {
    
/*********************************************************************************************
@Description :Method to create the project records by listening to Platform event
**********************************************************************************************/
    public static void createProjects(List<ProjectId__e> newItems){
        List<ProjectId__e> newProjectLst = (List<ProjectId__e>) newItems;
        List<Project__c> projectsToBeCreatedLst = new List<Project__c>();
        Map<String, Project__c> ProjectIdMap = new Map<String, Project__c>();
        Map<String, ProjectId__e> newProjectMap = new Map<String, ProjectId__e>();
        List<Integration_Log__c> logLstToInsert = new List<Integration_Log__c>();
        Set<Id> projectIdSet = new Set<Id>();
        Map<Id,Id> projectLogMap = new Map<Id,Id>();
        Set<String> locationIdNewSet = new Set<String>();
        Map<String, Id> locationIdExistingSet = new Map<String, Id>();
        
        //Populate map from Project External ID to Incoming Project Event
        //Populate set of Location External IDs from Incoming Project Events
        for(ProjectId__e event : newItems) {
            newProjectMap.put(event.Project_ID__c, event);
            if(event.Location_ID__c != null && event.Location_ID__c != ''){
            	locationIdNewSet.add(event.Location_ID__c);
            }
        }    
        
        //Populate map from Project External ID to existing Project Internal ID from Query
        //Populate set of existing Project Internal IDs
        if(newProjectMap != Null && newProjectMap.Size() > 0){
            for(Project__c project : [SELECT Project_ID__c, Id FROM Project__c WHERE Project_ID__c IN : newprojectMap.keySet()]){
                projectIdMap.put(project.project_ID__c, project);
                
                projectIdSet.add(project.Id);
            }
        }
        
		//Populate map of existing Location records from Location External ID to Location Internal ID
        if(locationIdNewSet != Null && locationIdNewSet.Size() > 0){
            for(Location_ID__c locId : [SELECT Location_ID__c, Id FROM Location_ID__c WHERE Location_ID__c IN : locationIdNewSet]){
                locationIdExistingSet.put(locId.Location_ID__c, locId.Id);
            }
        }
        
        //Create new Project records 
        for (projectId__e event : newProjectLst) {
            Project__c locObj = new Project__c();
            
            //Set Location ID to Location Internal ID found in map from Location External ID to Location Internal ID
            if(event.Location_ID__c == Null){
                locObj.Location_ID__c = Null;
            } else if(locationIdExistingSet.get(event.Location_ID__c) != Null){
                locObj.Location_ID__c = locationIdExistingSet.get(event.Location_ID__c);
            } else if(locationIdExistingSet.get(event.Location_ID__c) == Null){
                event.addError('Invalid Location ID');
                continue;
            }
            
            //Set the rest of the Project fields from the event fields
            locObj.Project_ID__c = event.Project_ID__c;
            locObj.Name = event.Project_Code__c;
            locObj.CMG_Vetting_Status__c = event.CMG_Vetting_Status__c;
            locObj.Design_Approved_Date__c = event.Design_Approved_Date__c;
            locObj.Milestone_On_Air_Date__c = event.Milestone_On_Air_Date__c;
            locObj.On_Air_Date_Status__c = event.On_Air_Date_Status__c;
            locObj.POR_Status__c = event.POR_Status__c;
            
            projectsToBeCreatedLst.add(locObj);
            
        } 
        
        //Create Map task to Integration log id
        if(projectIdSet.size() > 0){
            for(Integration_Log__c log : [SELECT Id, project__c FROM Integration_Log__c WHERE project__c IN: projectIdSet AND API_Type__c= 'DWH project']){
                projectLogMap.put(log.project__c, log.Id);
            }
        }
        
        if(projectsToBeCreatedLst.size()>0){
            //upsert projectsToBeCreatedLst;
            List<Database.UpsertResult> updateResults = database.upsert(projectsToBeCreatedLst, false);
            
            
            for(Integer i=0;i<updateResults.size();i++){
                Integration_Log__c errorLog;
                if(updateResults.get(i).isSuccess()){
                    if(projectLogMap.get(projectsToBeCreatedLst.get(i).Id) != null){
                        errorLog = new Integration_Log__c(Id = projectLogMap.get(projectsToBeCreatedLst.get(i).Id));
                        errorLog.Status__c = 'Success';
                        errorLog.Response_Message__c = 'project Updated Successfully';
                        errorLog.Request__c = String.valueOf(newprojectMap.get(projectsToBeCreatedLst.get(i).project_ID__c));
                        errorLog.API_Type__c = 'DWH project';
                        errorLog.Project__c = projectsToBeCreatedLst.get(i).Id;
                        logLstToInsert.add(errorLog);
                    }
                }
                else{ 
                    if(projectLogMap.get(projectsToBeCreatedLst.get(i).Id) != null){
                        errorLog = new Integration_Log__c(Id = projectLogMap.get(projectsToBeCreatedLst.get(i).Id));
                    }
                    else{
                        errorLog = new Integration_Log__c();
                    }
                    
                    // DML operation failed
                    String errorMsg = '';
                    for(Database.Error error : updateResults.get(i).getErrors()){
                        errorMsg += error.getMessage() +'\n';
                    }
                    errorLog.Status__c = 'Failure';
                    errorLog.Response_Message__c = errorMsg; 
                    errorLog.Request__c = String.valueOf(newprojectMap.get(projectsToBeCreatedLst.get(i).project_ID__c));
                    errorLog.API_Type__c = 'DWH project';
                    errorLog.project__c = projectsToBeCreatedLst.get(i).Id;
                    logLstToInsert.add(errorLog);
                }
                
            }   
            if(logLstToInsert != null && logLstToInsert.size() > 0){
                database.upsert(logLstToInsert, false);
            }
        }	
    }
}
/******************************************************************************
@author      : IBM
@date        : 19.10.2020
@description : Test class for SI_UpdateCaseLocation apex class
@Version     : 1.0
*******************************************************************************/
@isTest
public class SI_UpdateCaseLocationTest {
    
    /**************************************************************************
    * @description Method to set up test data
    **************************************************************************/
    @testSetup
    private static void testDataSetup() {
        
        //create location records
        List<Location_ID__c> listOfLocations = TestDataFactory.createLocationId('Test','Test',1);
        listOfLocations[0].AAV_Secondary_Contract_ID__c= 'test1';
        listOfLocations[0].Secondary_Vendor_Name__c= 'testName';
        listOfLocations[0].Secondary_Vendor_Id__c= 'test2';
        listOfLocations[0].Internally_Contracted__c= true;
        listOfLocations[0].AAV_Primary_Contract_ID__c= '';
        listOfLocations[0].Primary_Vendor_Id__c= '';
        listOfLocations[0].Primary_Vendor_Name__c= '';
        insert listOfLocations;
        
        //fetch Work Order Task record type
        Id workOrderRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',
                                                           Label.SI_CaseRT.split(';')[3]);
        //fetch Backhaul Internal record type
        Id backhaulInternalRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',
                                                           Label.SI_CaseRT.split(';')[2]);
        //create case records
        List<Case> listOfBackhaulCases = TestDataFactory.createCaseSingleIntake(1, backhaulInternalRecTypeId);     
        listOfBackhaulCases[0].Type = 'Backhaul Internal';
        listOfBackhaulCases[0].Internally_Contracted__c = 'No';
        insert listOfBackhaulCases; 
        
        List<Case> listOfParentCase = TestDataFactory.createCaseSingleIntake(1, workOrderRecTypeId);
        listOfParentCase[0].Location_Ids__c = listOfLocations[0].Id;
        listOfParentCase[0].Workflow_Template__c = 'OM New AAV & DAS Install';
        listOfParentCase[0].Task_Description__c = 'Activate Site';
        listOfParentCase[0].Status = 'Closed';  
        //listOfParentCase[0].TPE_Request_Number__c = listOfBackhaulCases[0].Id;
        insert listOfParentCase; 
        
        List<Case> listOfCases = TestDataFactory.createCaseSingleIntake(1, workOrderRecTypeId);
        listOfCases[0].Workflow_Template__c = 'OM New AAV & DAS Install';
        listOfCases[0].Task_Description__c = 'Activate Site';
        listOfCases[0].Status = 'Closed';  
        listOfCases[0].ParentId = listOfParentCase[0].Id;
        insert listOfCases;
    }
    
    /**************************************************************************
    * @description To test updateLocation method
    **************************************************************************/
    @isTest
    private static void testLocationUpdate() {
        List<Case> listOfCases = [Select Id,ParentId,Parent.TPE_Request_Number__c,Parent.TPE_Request_Number__r.Single_Intake_Team__c,
                                  Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c,Parent.Location_IDs__r.Secondary_Vendor_Id__c,
                                  Parent.Location_IDs__r.Secondary_Vendor_Name__c,Parent.Location_IDs__r.AAV_Primary_Contract_ID__c,
                                  Parent.Location_IDs__r.Primary_Vendor_Id__c,Parent.Location_IDs__r.Primary_Vendor_Name__c,
                                  Parent.Location_IDs__r.Internally_Contracted__c
                                  from Case where ParentId != null LIMIT 1];
        System.assertEquals(1, listOfCases.size(),'There should be one Case.');
        //System.assertEquals('test1', listOfCases[0].Parent.Location_IDs__r.AAV_Primary_Contract_ID__c,'Primary ContractId should be assigned same as Secondary ContractId.');        
        //System.assertEquals(null, listOfCases[0].Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c,'Secondary ContractId should be null.');        
        Test.startTest();
        SI_UpdateCaseLocation.doUpdateAction(listOfCases);
        Test.stopTest();        
        //System.assertEquals('test1', listOfCases[0].Parent.Location_IDs__r.AAV_Primary_Contract_ID__c,'Primary ContractId should be assigned same as Secondary ContractId.');        
        //System.assertEquals(null, listOfCases[0].Parent.Location_IDs__r.AAV_Secondary_Contract_ID__c,'Secondary ContractId should be null.');        
        
    }
}
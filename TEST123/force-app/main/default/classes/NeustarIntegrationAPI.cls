/*********************************************************************************************
@author     : IBM
@date       : 19.06.2020
@description: Neustar Integration API
@Version    : 1.0
**********************************************************************************************/
public with sharing class NeustarIntegrationAPI {
    
    /*********************************************************************************************
* @Description : This methods calls the APIs based on API-Type
/**********************************************************************************************/    
    public static NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout executeAPIs(List<Case> lstCaseIds, String apiType,Map<Id, List<Integration_Log__c>> caseILMap)
    {
        NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout dmlActionsWrapper = new NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout();
        if(apiType == 'CreateOrder') {
            dmlActionsWrapper = createOrderInNeustar(lstCaseIds,apiType,caseILMap);
        }
        return dmlActionsWrapper;
    }
    
    /*********************************************************************************************
* @Description : This method callout for creating order in Neustar
/**********************************************************************************************/ 
    public static NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout createOrderInNeustar(List<Case> lstCaseIds, String apiType,Map<Id, List<Integration_Log__c>> caseILMap)
    {
        NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout dmlActionsWrapper = new NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout();
        Map<String, SwitchAPICallOuts__c> switchSetting = SwitchAPICallOuts__c.getAll();
        Map<Id,List<Purchase_Order__c>> caseIdToPoLstMap = new Map<Id,List<Purchase_Order__c>>();
        Map<Id,Purchase_Order__c> allPOMap = new Map<Id,Purchase_Order__c>();
        for(case cs :lstCaseIds){
            for(Purchase_Order__c poOrder : cs.Purchase_Orders__r){
                if(switchSetting.get('Create Purchase Order') != NULL && switchSetting.get('Create Purchase Order').Execute_API_Call__c == True){
                    allPOMap.put(poOrder.Id,poOrder);
                    if(caseIdToPoLstMap.containsKey(cs.Id)){
                        caseIdToPoLstMap.get(cs.Id).add(poOrder);
                    }
                    else{
                        caseIdToPoLstMap.put(cs.Id, new List<Purchase_Order__c>{poOrder});
                    }
                }
            }
        }
        System.debug('**********caseIdToPoLstMap'+caseIdToPoLstMap);
        Map<Id,Purchase_Order__c> idToPOrderMap = new Map<Id,Purchase_Order__c>();
        if(caseILMap!=null && caseILMap.Values().Size() > 0){
            System.debug('in if..Reprocess');
            for(Id caseId : caseILMap.KeySet()){
                for(Integration_Log__c intLog : caseILMap.get(caseId)){
                    if(allPOMap!=null && allPOMap.keySet().size()>0 && allPOMap.containsKey(intLog.Purchase_Order__c)){
                        idToPOrderMap.put(intLog.Purchase_Order__c,allPOMap.get(intLog.Purchase_Order__c));    
                    }
                }
            }    
        }
        else{
            System.debug('in else..not Reprocess');
            String tradingPartnerToExclude=System.Label.Excluded_Trading_Partner;
            List<String> tradingPartnerList=new List<String>();
            tradingPartnerList=tradingPartnerToExclude.split(',');
            system.debug('tradingPartnerList:'+tradingPartnerList);
            if(caseIdToPoLstMap!=null && caseIdToPoLstMap.keySet().size()>0){
                for(Id caseId : caseIdToPoLstMap.KeySet()){
                    for(Purchase_Order__c pOrder : caseIdToPoLstMap.get(caseId)){
                        if((pOrder.API_Status__c == 'New' || pOrder.API_Status__c == null) && (pOrder.Number_of_ASRs__c == pOrder.Number_of_Successful_ASR__c) && pOrder.Number_of_ASRs__c >0 && (!tradingPartnerList.contains(pOrder.Trading_Partner__c))){
                            idToPOrderMap.put(pOrder.Id,pOrder);
                        }
                    }
                }
            }    
        }
        System.debug('idToPOrderMap********'+idToPOrderMap);
        //NeustarIntegrationAPIHelper.generateCreateOrderJSON(idToPOrderMap);
        dmlActionsWrapper = NeustarIntegrationAPIHelper.generateCreateOrderJSONwithFiveFields(idToPOrderMap);
        //NeustarIntegrationAPIHelper.updateCaseAPIStatus(lstCaseIds); //Method added in batch
        return dmlActionsWrapper;
    }
    
    
}
/*********************************************************************************************************************
*  @Class            : CustomChevronViewController
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 09.07.2019
*  @Description      : Apex class of Custom Chevron View lightning component
**********************************************************************************************************************/
public with sharing class CustomChevronViewController {
    
/***************************************************************************************
* @Description : Get Data to display chevron view based on Executive_Level_Name__c field.
* @input params :RecordId
***************************************************************************************/
    @AuraEnabled
    public static String getChevronData(String recId){
        Map<String, String> newMap =new Map<String, String> ();
        String selVal;
        List<Case> updateCases=[select Id, Executive_Level_Name__c,Status,Task_Number__c FROM Case WHERE Parent.Id =:recId Order By Task_Number__c ASC];
        for(Case cs : updateCases )
        {
            if(cs.Executive_Level_Name__c!=NULL){
          
                if(label.Chevron_Exec_Level_Status.contains(cs.Status)){
                    newMap.put(cs.Task_Number__c+'-'+cs.Executive_Level_Name__c,'Closed');
                }
                else{
                    if(selVal==Null)
                    {
                        selVal=cs.Task_Number__c+'-'+ cs.Executive_Level_Name__c;
                    }        
                    newMap.put(cs.Task_Number__c+'-'+ cs.Executive_Level_Name__c,'NotClosed');
                } 
            }
        }
        Boolean curValMatched = false;
        List<chevronData> lstRet = new List<chevronData>();
        if(newMap.size()>0){
            Integer widthPerItem = 100/newMap.size();
            for( String execName : newMap.keyset())
            {
                chevronData obj = new chevronData();
                obj.val = execName;
                obj.width = widthPerItem+'%';
                if(obj.val == selVal){
                    obj.cssClass = 'active';
                    curValMatched = true;
                }
                else if(curValMatched){
                    obj.cssClass = '';
                }else{
                    obj.cssClass = 'visited'; 
                }
                lstRet.add(obj);
            }     
        }
        return JSON.serialize(lstRet);
    }    
/***************************************************************************************
* @Description : Wrapper class -Checvron Data
***************************************************************************************/    
    public class chevronData{
        public String val{get;set;}
        public String cssClass{get;set;}
        public String width {get;set;}
    }
    
}
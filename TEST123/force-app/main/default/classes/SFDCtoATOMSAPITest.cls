/*********************************************************************************************
@author     : IBM
@date       : 14 Aug 2020
@description: Test class for Batch class SFDCToATOMSAPI
@Version    : 1.0           
**********************************************************************************************/
@isTest
public class SFDCtoATOMSAPITest {
     Public Static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
     Public Static Map<Id,List<ASR__c>> asrMAPtobecreated = new Map<Id,List<ASR__c>>();
     Public Static Map<Id,List<ASR__c>> asrUpgradetobecreated = new Map<Id,List<ASR__c>>();
     Public Static Map<Id,List<ASR__c>> asrDisconnecttobecreated = new Map<Id,List<ASR__c>>();
     Public Static Map<Id,List<ASR__c>> asrUpdateasrtobecreated = new Map<Id,List<ASR__c>>();
     Public Static Map<Id,List<Integration_Log__c>> caseILMaptobeCreated = new Map<Id,List<Integration_Log__c>>();
     Public Static List<ASR__c> asrList = new List<ASR__c>();
     @testSetup
     static void testDataSetup(){
        List<SwitchAPICallOuts__c> switchAPIMapSet = new List<SwitchAPICallOuts__c>();
        switchAPIMapSet = TestDataFactoryOMph2.createSwitchAPICalloutCustSet();
        
        insert switchAPIMapSet;
        
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        TestDataFactoryOMph2.createNeuStarMappingCustSet();
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        system.debug('atomsUtilitySet>>'+atomsUtilitySet);
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        system.debug('WORD>>'+workOrdTaskList);
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',1,workOrdTaskList[0].Id);
        insert purOrderList;
        system.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        circuitList[0].Circuit_ID_Primary_Key__c = '5235346436';
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Existing';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,3);
        System.assertEquals('New Service: Ethernet', asrList[0].ASR_Type__c);
        Integer tempASR = 0;
        for(ASR__c asrTemp:asrList){
            tempASR++;
            asrTemp.Circuit_Id__c = circuitList[0].Id; 
            asrTemp.ATOMs_ASR_Id__c = '4567457474';
            Switch ON tempASR {
                WHEN 1{ asrTemp.ASR_Type__c = 'New Service: Ethernet';}
                WHEN 2{ asrTemp.ASR_Type__c = 'BW Change: BB';
                    asrTemp.Circuit_Category__c = 'CORENET';
                    asrTemp.Service_Type__c = 'Broadband';
                    asrTemp.SubService_Type__c = 'Broadband';
                    asrTemp.Ethernet_Technology_Type__c = 'AsymE';
                    asrTemp.Bandwidth__c = '10Gbe';
                    asrTemp.AAV_Type__c = 'Yes';
                }
                WHEN 3{ asrTemp.ASR_Type__c = 'Disconnect: SAT';
                    asrTemp.Circuit_Category__c = 'CORENET';
                    asrTemp.Service_Type__c = 'Satellite';
                    asrTemp.SubService_Type__c = 'UNI';
                    asrTemp.Ethernet_Technology_Type__c = 'AAV';
                    asrTemp.Bandwidth__c = '10Gbe';
                    asrTemp.AAV_Type__c = 'Yes';      
                }
                WHEN else { asrTemp.ASR_Type__c = 'NS';} 
            }
            asrTemp.CIR__c = 100;
            asrTemp.PON__c = 'Test4567457474';
            asrTemp.Ordered_Date__c = System.today();
            //asrTemp.SubService_Type__c = 'EVC';
            //asrTemp.Circuit_Id__r.RecordType.Name = 'NNI';
            asrTemp.Order_Destination__c = 'Vendor E-Bonded';
            //asrTemp.ASR_Type__c = 'New Service: Ethernet';
            //asrTemp.Service_Type__c = 'Ethernet';
            //asrTemp.Ethernet_Technology_Type__c = 'AAV 1.95';
            //asrTemp.Ethernet_Technology_Type__c = 'AAV 2.0';
            //asrTemp.Ethernet_Technology_Type__c = 'Layer 1';
            //asrTemp.AAV_Type__c = 'Ethernet';
            asrTemp.VLAN__c = 100;
            asrTemp.VLAN_2__c = 1000;
            asrTemp.Target_Download_Speed__c = 1000;
            //asrTemp.Ethernet_Technology_Type__c = 'AAV';
            //asrTemp.AAV_Type__c = 'EVC';
            asrTemp.Test_CIR__c = 45674574;
            asrTemp.Delivered_Date__c= System.today();
            asrTemp.Deliver__c = System.today();
         }
        INSERT  asrList;
        List<Integration_Log__c> intLogList = new List<Integration_Log__c>();
         //Integration_Log__c intlog1 = new Integration_Log__c();
         //Integration_Log__c intlog2 = new Integration_Log__c();
         //Integration_Log__c intlog3 = new Integration_Log__c();
         //intlog1 = TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'CreateASR',1)[0];
         //intlog2 = TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'UpgradeASR',1)[0];
         //intlog3 = TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'DisconnectASR',1)[0];
         //intLogList.add(intlog1);
         //intLogList.add(intlog2);
         intLogList = TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'DisconnectASR',3);
        Integer tempInt = 0;
        for(Integration_Log__c intval :intLogList){
           Switch ON tempInt {
                WHEN 0{ intval.API_Type__c = 'CreateASR';}
                WHEN 1{ intval.API_Type__c = 'UpgradeASR';}
                WHEN 2{ intval.API_Type__c = 'DisconnectASR';}
                WHEN else { intval.API_Type__c = '';} 
            }
             
            intval.Status__c = 'Success';
            intval.From__c = 'SFDC';
            intval.To__c = 'ATOMS';
            intval.Purchase_Ord_PON__c = purOrderList[0].PON__c;
            intval.PON__c = asrList[tempInt].PON__c;
            intval.ASR__c = asrList[tempInt].Id;
            intval.Pending_ATOMS_Update__c = true;
            tempInt++;
          }
         INSERT  intLogList;
         System.debug('intLogList!!!!'+intLogList);
     }
   
    static testMethod void testsendASRRequestToAtoms(){
         Test.setMock(HttpCalloutMock.class, new AtomsServiceTest.MockAtomsCallouts());
         TMobilePh2LoginDetailsHierarchy__c atomsToken = TMobilePh2LoginDetailsHierarchy__c.getOrgDefaults();
         SFDCtoAtomsAPI.returnParamsToBatchAfterCallout dmlActionsWrapper;
         List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> asrWrapperList = new List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
         SFDCtoAtomsAPI.ASRResponseWrapper ASRResponseWrapper;
         date mydate;
         String dateTemp = '2020-05-12';
         list<ASR__c> asrlistTemp = [Select id,Case__c,Bandwidth__c,PON__c,Change_Type__c,Change_Value__c,VLAN__c,Forecast_Version__c,Deliver__c,Target_Download_Speed__c,Desired_Due_Date__c,TPE_Request_Number__c,VLAN_2__c,Circuit_Id__r.NNI_ID_Primary_Key__c,Order_Number__c,SubService_Type__c,Case__r.parent.PIER_Work_Order_ID__c,ATOMs_ASR_Id__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Order_Destination__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,Circuit_Id__c,Delivered_Date__c,Circuit_Id__r.RecordType.Name,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c, ASR_Type__c from ASR__c WHERE ATOMs_ASR_Id__c = '4567457474'];
         list<Integration_Log__c> IntLogTemp = [SELECT Id,ASR__c,ASR__r.Case__c,Request__c,Response__c,API_Type__c,Response_Message__c,Reprocess__c, Error_Type__c from Integration_Log__c];
         System.debug('asrlistTemp>>>>>>>>'+asrlistTemp);
         System.debug('IntLogTemp>>>>>>>>'+IntLogTemp);
          for(ASR__c asrmap:asrlistTemp){
            System.debug('asrType>>>>>>>>'+asrmap.ASR_Type__c);
            if(asrmap.ASR_Type__c == 'New Service: Ethernet'){
                asrMAPtobecreated.put(asrmap.Case__c,new list<ASR__c>{asrmap});
                System.debug('asrMAPtobecreated>>>>>>>>'+asrMAPtobecreated);
            }
            
            if(asrmap.ASR_Type__c == 'BW Change: BB'){
                asrUpgradetobecreated.put(asrmap.Case__c,new list<ASR__c>{asrmap});
                
            }
            if(asrmap.ASR_Type__c == 'Disconnect: SAT'){
                asrDisconnecttobecreated.put(asrmap.Case__c,new list<ASR__c>{asrmap});
                System.debug('asrDisconnecttobecreated>>>>>>>>'+asrDisconnecttobecreated);
            }
        }
        //caseILMaptobeCreated.put(IntLogTemp[0].ASR__r.Case__c,new List<Integration_Log__c>());
        //System.debug('caseILMaptobeCreated>>>>>>>'+caseILMaptobeCreated);
        //for(Integration_Log__c intTemp:IntLogTemp){
            //caseILMaptobeCreated.put(intTemp.ASR__r.Case__c,new list<Integration_Log__c>{intTemp});
            //System.debug('caseILMaptobeCreated>>>>>>>>'+caseILMaptobeCreated.Values().Size());
        //}
         Test.startTest();
         dmlActionsWrapper = SFDCtoAtomsAPI.createASRtoAtoms(asrMAPtobecreated,'CreateASR',atomsToken,caseILMaptobeCreated);
         asrWrapperList.add(dmlActionsWrapper);
         dmlActionsWrapper = new SFDCtoAtomsAPI.returnParamsToBatchAfterCallout();
         dmlActionsWrapper = SFDCtoAtomsAPI.upgradeASRtoAtoms(asrUpgradetobecreated,'UpgradeASR',atomsToken,caseILMaptobeCreated);
         asrWrapperList.add(dmlActionsWrapper);
         dmlActionsWrapper = new SFDCtoAtomsAPI.returnParamsToBatchAfterCallout();
         //for(Integration_Log__c intTemp:IntLogTemp){
            //caseILMaptobeCreated.put(intTemp.ASR__r.Case__c,new list<Integration_Log__c>{intTemp});
         //}
         dmlActionsWrapper = SFDCtoAtomsAPI.disconnectASRtoAtoms(asrDisconnecttobecreated,'DisconnectASR',atomsToken,caseILMaptobeCreated);
         asrWrapperList.add(dmlActionsWrapper);
         //dmlActionsWrapper = new SFDCtoAtomsAPI.returnParamsToBatchAfterCallout();
         System.debug('asrWrapperList.............'+asrWrapperList);
         SFDCtoAtomsAPI.performASRDMLs(asrWrapperList);
         System.debug('dateTemp+++++++++'+dateTemp);
         System.debug('asrUpdateasrtobecreated+++++++++'+asrUpdateasrtobecreated);
         mydate = SFDCtoAtomsAPI.setStringToDateFormat(dateTemp);
         for(Integration_Log__c intTemp:IntLogTemp){
            caseILMaptobeCreated.put(intTemp.ASR__r.Case__c,new list<Integration_Log__c>{intTemp});
         }
         dmlActionsWrapper = SFDCtoAtomsAPI.updateASR2AtomsUsingLogsORAsr(asrUpdateasrtobecreated,caseILMaptobeCreated);
         dmlActionsWrapper = SFDCtoAtomsAPI.updateASRtoAtoms(asrlistTemp,IntLogTemp);
         Date dateTempRet;
         //dateTempRet = SFDCtoAtomsAPI.formatDatetoPST(System.today()-1);
         Test.stopTest(); 
     }
    
    
    static testMethod void randomTest(){
        
         SFDCtoAtomsAPI.ASRWrapper obj = new SFDCtoAtomsAPI.ASRWrapper();
            obj.ASRType = 'abc';
            obj.PON = 'abc';
            obj.OrderDest = 'abc';
            obj.dtOrdered = 'abc';
            obj.CRNum = 'abc';
            obj.SubserviceType = 'abc';
            obj.ServiceType = 'abc';
            obj.TechnologyType = 'abc';
            obj.AAVType = 'abc';
            obj.CIR = 123;
            obj.TestCIR = 123;
            obj.CircuitID = 'abc';
            obj.NNIID = 'abc';
            obj.ManualOrder = 'abc';
            obj.dtDelivered = 'abc';
            obj.ForecastVersion = 'abc';
            obj.OrderNumber = 'abc';
            obj.VLAN = 123;
            obj.VLANTwo = 123;
            obj.dtBillingEnd = 'abc';
            obj.DownSpeed = 123;
            obj.TPENum = 'abc';
            obj.dtDue = 'abc';
            obj.ChangeField = 'abc';
            obj.ChangeValue = 'abc';
        
        SFDCtoAtomsAPI.ASRResponseWrapper obj1 = new SFDCtoAtomsAPI.ASRResponseWrapper();
            obj1.asrRespFields =new Map<string,string>();
    
        SFDCtoAtomsAPI.ResponseWrapperForAtoms obj2 = new SFDCtoAtomsAPI.ResponseWrapperForAtoms();
            obj2.Code = 'abc';
            obj2.Details = 'abc';
    }
}
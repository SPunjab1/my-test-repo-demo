/*********************************************************************************************
@author     : IBM
@date       : July 13 2020
@description: Scheduler class To execute the BatchManualReprocessAPICallout batch every 1 min 
@Version    : 1.0           
**********************************************************************************************/
global class ScheduleReprocessBatch implements Schedulable {
    // This scheduler schedules BatchManualReprocessAPICallout at the interval of every one min
    global void execute(SchedulableContext sc) {
       BatchManualReprocessAPICallout reprocessBatch = new BatchManualReprocessAPICallout();
       integer interval = Integer.valueOf(Label.ReprocessBatchInterval);
       for(integer i=0;i<60;i=i+interval){
            string cron1 = '0 '+ i +' * * * ?';
            string schName1 = 'Reprocess Batch executes in--'+i+'---Min--';
            if(!Test.isRunningTest()) system.schedule(schName1, cron1, reprocessBatch);
        }
    }
}
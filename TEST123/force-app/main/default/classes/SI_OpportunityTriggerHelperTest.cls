/******************************************************************************
@author      - IBM
@date        - 17.06.2020
@description - Test class for OpportunityTriggerHelper apex class
@version     - 1.0
*******************************************************************************/
// private class
@isTest
public class SI_OpportunityTriggerHelperTest {
    
    /**************************************************************************
    * @description - Method to setup test data
    **************************************************************************/
    @testSetup
    private static void testDataSetup() {
        TriggerManager__c triggerManager = new TriggerManager__c(SetupOwnerId = UserInfo.getOrganizationId(), 
                                                                 DisabledObjects__c = 'Opportunity,Case',
                                                                 MuteAllTriggers__c = FALSE);
        insert triggerManager;
        
        // Opportunity RecordType
        Id backhaulContractRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Id strOppCoreRetailRT = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_OpportunityRT.split(';')[1]);
        //Case Record type
        Id macroDeselectRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        //Pico Backhaul Record Type
        Id picoBackhaulRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[1]);
        //Intake Record type
        Id intakeCIRRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Intake_Site__c',Label.SI_IntakeSiteRT);
        
        Test.startTest();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0];
        objAccount.TCM_vendor_Name__c = 'Test Acc';
        insert objAccount;
        
        //create Account records
        Account satelliteVendorAcc = TestDataFactory.createAccountSingleIntake(1)[0];
        objAccount.TCM_vendor_Name__c = 'Test Acc1';
        satelliteVendorAcc.Satellite_Vendor__c = true;
        insert satelliteVendorAcc;
        
        //Contact RecordType
        List<Contact> conObj = TestDataFactory.createContactSingleIntake(4,objAccount.id);
        insert conObj;
        
        // Insert record for ValidationRuleManager__c custom setting        
        insert new ValidationRuleManager__c(SetupOwnerId =UserInfo.getOrganizationId(), 
                                            Run_Validation__c = false);
        
        //Intialize list of Opportunity to insert records
        List<Opportunity> listOfOpportunitiesToInsert = new List<Opportunity>();
        
        //create Opportunity for Region
        Opportunity oppRecForRegion = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForRegion.Region__c = 'West';
        oppRecForRegion.ETL_MRC__c = 250000.00;
        oppRecForRegion.ETL_NRC__c = 250000.00;
        oppRecForRegion.Contract_Start_Date__c = System.today();
        oppRecForRegion.AccountId = objAccount.Id;
        oppRecForRegion.Contract_ID__c = '1234';
        oppRecForRegion.File_Name__c = 'FileName1';
        oppRecForRegion.CPM_Tracker_ID__c = '1234';
        listOfOpportunitiesToInsert.add(oppRecForRegion);
        
        //create Opportunity for Type
        Opportunity oppRecForType = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForType.Type = 'IPX';
        oppRecForType.ETL_MRC__c = 10000000.00;
        oppRecForType.ETL_NRC__c = 11000000.00;
        oppRecForType.Contract_Start_Date__c = System.today();
        oppRecForType.File_Name__c = 'Sample File' + System.now();
        oppRecForType.AccountId = objAccount.Id;
        oppRecForType.Contract_ID__c = '12344';
        oppRecForType.File_Name__c = 'FileName2';
        oppRecForType.CPM_Tracker_ID__c = '12344';
        listOfOpportunitiesToInsert.add(oppRecForType);
        
        //create Opportunity for location update
        Opportunity oppRecForLocationUpdate = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForLocationUpdate.StageName = System.Label.SI_OppStageDefaultValue;
        oppRecForLocationUpdate.Intake_Request_Submitter__c = conObj[0].id;
        oppRecForLocationUpdate.Vendor_Contact_lookup__c = conObj[0].id;
        oppRecForLocationUpdate.Document_Type__c = System.Label.SI_OppDocType;
        oppRecForLocationUpdate.Contract_Signatures_Complete__c = true;
        oppRecForLocationUpdate.Type = System.Label.SI_OppType.split(',')[1];
        oppRecForLocationUpdate.Region__c = system.label.SI_OppRegion.split(',')[0];
        oppRecForLocationUpdate.Contract_Start_Date__c = System.today();
        oppRecForLocationUpdate.AccountId = objAccount.Id;
        oppRecForLocationUpdate.Contract_ID__c = '12345';
        oppRecForLocationUpdate.File_Name__c = 'FileName3';
        oppRecForLocationUpdate.CPM_Tracker_ID__c = '12345';
        listOfOpportunitiesToInsert.add(oppRecForLocationUpdate);
        
        //create Opportunity for location update LOI
        Opportunity oppRecForLocationUpdateLOI = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForLocationUpdateLOI.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForLocationUpdateLOI.Document_Type__c = System.Label.SI_OppDocumentType;
        oppRecForLocationUpdateLOI.Contract_Start_Date__c = System.today();
        oppRecForLocationUpdateLOI.AccountId = objAccount.Id;
        oppRecForLocationUpdateLOI.Contract_ID__c = '12346';
        oppRecForLocationUpdateLOI.File_Name__c = 'FileName4';
        oppRecForLocationUpdateLOI.CPM_Tracker_ID__c = '12346';
        listOfOpportunitiesToInsert.add(oppRecForLocationUpdateLOI);
        
        //create Opportunity for Contract Backhaul
        Opportunity oppRecForContract = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = objAccount.Id;
        oppRecForContract.Document_Type__c = System.Label.SI_OppDocumentType;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        oppRecForContract.Contract_ID__c = '12347';
        oppRecForContract.File_Name__c = 'FileName5';
        oppRecForContract.CPM_Tracker_ID__c = '12347';
        listOfOpportunitiesToInsert.add(oppRecForContract);
        
        //create Opportunity for Contract VICC CoreRetail
        Opportunity oppRecForContractCoreRetail = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForContractCoreRetail.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContractCoreRetail.AccountId = objAccount.Id;
        oppRecForContractCoreRetail.Contract_Start_Date__c = System.today();
        oppRecForContractCoreRetail.Term_mo__c = 1.00;
        oppRecForContractCoreRetail.Contract_ID__c = '12348';
        oppRecForContractCoreRetail.File_Name__c = 'FileName6';
        oppRecForContractCoreRetail.CPM_Tracker_ID__c = '12348';
        listOfOpportunitiesToInsert.add(oppRecForContractCoreRetail);
        
        //create Opportunity for Contract VICC InterVoice
        Opportunity oppRecForContractInterVoice = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForContractInterVoice.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContractInterVoice.AccountId = objAccount.Id;
        oppRecForContractInterVoice.Contract_Start_Date__c = System.today();
        oppRecForContractInterVoice.Term_mo__c = 1.00;
        oppRecForContractInterVoice.Contract_ID__c = '12349';
        oppRecForContractInterVoice.File_Name__c = 'FileName7';
        oppRecForContractInterVoice.CPM_Tracker_ID__c = '12349';
        listOfOpportunitiesToInsert.add(oppRecForContractInterVoice);   
        
        //create Backhaul Opportunity for Contract 
        Opportunity oppRecForContractId = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContractId.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContractId.AccountId = objAccount.Id;
        oppRecForContractId.Contract_Start_Date__c = System.today();
        oppRecForContractId.Term_mo__c = 1.00;
        oppRecForContractId.Contract_ID__c = '12340';
        oppRecForContractId.File_Name__c = 'FileName8';
        oppRecForContractId.CPM_Tracker_ID__c = '12340';
        listOfOpportunitiesToInsert.add(oppRecForContractId);
        
        //create Opportunity for Contract Small Cell FrontHaul
        Opportunity oppRecForMSOContract = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForMSOContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForMSOContract.AccountId = objAccount.Id;
        oppRecForMSOContract.Contract_Start_Date__c = System.today();
        oppRecForMSOContract.Term_mo__c = 1.00;
        oppRecForMSOContract.Contract_ID__c = '1230';
        oppRecForMSOContract.File_Name__c = 'FileName9';
        oppRecForMSOContract.CPM_Tracker_ID__c = '1230';
        listOfOpportunitiesToInsert.add(oppRecForMSOContract);
        
        //create Opportunity for Contract Small Cell FrontHaul
        Opportunity oppRecForFSOContract = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForFSOContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForFSOContract.AccountId = objAccount.Id;
        oppRecForFSOContract.Contract_Start_Date__c = System.today();
        oppRecForFSOContract.Term_mo__c = 1.00;
        oppRecForFSOContract.Type = 'Retail Services';
        oppRecForFSOContract.Manual_Signature_Required__c = true;
        oppRecForFSOContract.TCM_Upload_Required__c = true;
        oppRecForFSOContract.Contract_ID__c = '12310';
        oppRecForFSOContract.File_Name__c = 'FileName10';
        oppRecForFSOContract.CPM_Tracker_ID__c = '12310';
        listOfOpportunitiesToInsert.add(oppRecForFSOContract);
        
        Opportunity oppRecForFSOContract1 = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForFSOContract1.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForFSOContract1.AccountId = objAccount.Id;
        oppRecForFSOContract1.Contract_Start_Date__c = System.today();
        oppRecForFSOContract1.Term_mo__c = 1.00;
        oppRecForFSOContract1.Manual_Signature_Required__c = true;
        oppRecForFSOContract1.Contract_Site_Attachement__c = true;
        oppRecForFSOContract1.TCM_Upload_Required__c = true;
        oppRecForFSOContract1.Contract_ID__c = '123101';
        oppRecForFSOContract1.File_Name__c = 'FileName11';
        oppRecForFSOContract1.CPM_Tracker_ID__c = '123101';
        listOfOpportunitiesToInsert.add(oppRecForFSOContract1);
        
        Opportunity oppRecForFSOContract2 = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForFSOContract2.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForFSOContract2.AccountId = objAccount.Id;
        oppRecForFSOContract2.Contract_Start_Date__c = System.today();
        oppRecForFSOContract2.Term_mo__c = 1.00;
        oppRecForFSOContract2.TCM_Upload_Required__c = true;
        oppRecForFSOContract2.Contract_ID__c = '123102';
        oppRecForFSOContract2.File_Name__c = 'FileName12';
        oppRecForFSOContract2.CPM_Tracker_ID__c = '123102';
        listOfOpportunitiesToInsert.add(oppRecForFSOContract2);
        
        Opportunity oppRecForFSOContract3 = TestDataFactory.createOpportunitySingleIntake(1, strOppCoreRetailRT)[0];
        oppRecForFSOContract3.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForFSOContract3.AccountId = objAccount.Id;
        oppRecForFSOContract3.Contract_Start_Date__c = System.today();
        oppRecForFSOContract3.Term_mo__c = 1.00;
        oppRecForFSOContract3.Contract_Site_Attachement__c = true;
        oppRecForFSOContract3.TCM_Upload_Required__c = true;
        oppRecForFSOContract3.Contract_ID__c = '123104';
        oppRecForFSOContract3.File_Name__c = 'FileName13';
        oppRecForFSOContract3.CPM_Tracker_ID__c = '123104';
        listOfOpportunitiesToInsert.add(oppRecForFSOContract3);
        
        Opportunity oppRecForContractRejected = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContractRejected.Name = 'Lost Opp';
        oppRecForContractRejected.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContractRejected.AccountId = objAccount.Id;
        oppRecForContractRejected.Contract_Start_Date__c = System.today();
        oppRecForContractRejected.Term_mo__c = 1.00;
        listOfOpportunitiesToInsert.add(oppRecForContractRejected);
        
         //create Opportunity for Contract Backhaul
        Opportunity oppRecForSatelliteVendor = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForSatelliteVendor.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForSatelliteVendor.AccountId = satelliteVendorAcc.Id;
        oppRecForSatelliteVendor.Contract_Start_Date__c = System.today();
        oppRecForSatelliteVendor.Term_mo__c = 1.00;
        oppRecForSatelliteVendor.Contract_ID__c = '123105';
        oppRecForSatelliteVendor.File_Name__c = 'FileName14';
        oppRecForSatelliteVendor.CPM_Tracker_ID__c = '123105';
        listOfOpportunitiesToInsert.add(oppRecForSatelliteVendor);
        
         //create Opportunity for Contract Backhaul
        Opportunity oppRecForNonSatelliteVendorPrimary = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForNonSatelliteVendorPrimary.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForNonSatelliteVendorPrimary.AccountId = objAccount.Id;
        oppRecForNonSatelliteVendorPrimary.Contract_Start_Date__c = System.today();
        oppRecForNonSatelliteVendorPrimary.Term_mo__c = 1.00;
        oppRecForNonSatelliteVendorPrimary.Contract_ID__c = '123106';
        oppRecForNonSatelliteVendorPrimary.File_Name__c = 'FileName15';
        oppRecForNonSatelliteVendorPrimary.CPM_Tracker_ID__c = '123106';
        listOfOpportunitiesToInsert.add(oppRecForNonSatelliteVendorPrimary);
        
        //create Opportunity for Contract Backhaul
        Opportunity oppRecForNonSatelliteVendorSecondary = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForNonSatelliteVendorSecondary.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForNonSatelliteVendorSecondary.AccountId = objAccount.Id;
        oppRecForNonSatelliteVendorSecondary.Contract_Start_Date__c = System.today();
        oppRecForNonSatelliteVendorSecondary.Term_mo__c = 1.00;
        oppRecForNonSatelliteVendorSecondary.Contract_ID__c = '123107';
        oppRecForNonSatelliteVendorSecondary.File_Name__c = 'FileName16';
        oppRecForNonSatelliteVendorSecondary.CPM_Tracker_ID__c = '123107';
        listOfOpportunitiesToInsert.add(oppRecForNonSatelliteVendorSecondary);
        
        //create Opportunity for Contract Backhaul
        Opportunity oppRecForNonSatelliteVendorSecondaryMacro = 
            TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForNonSatelliteVendorSecondaryMacro.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForNonSatelliteVendorSecondaryMacro.AccountId = objAccount.Id;
        oppRecForNonSatelliteVendorSecondaryMacro.Contract_Start_Date__c = System.today();
        oppRecForNonSatelliteVendorSecondaryMacro.Term_mo__c = 1.00;
        oppRecForNonSatelliteVendorSecondaryMacro.Contract_ID__c = '123108';
        oppRecForNonSatelliteVendorSecondaryMacro.File_Name__c = 'FileName19';
        oppRecForNonSatelliteVendorSecondaryMacro.CPM_Tracker_ID__c = '123108';
        listOfOpportunitiesToInsert.add(oppRecForNonSatelliteVendorSecondaryMacro);
        
        //insert Opportunity records
        insert listOfOpportunitiesToInsert;
        
        //Intialize list of Location to insert records
        List<Location_ID__c> listLocation = new List<Location_ID__c>();
        //Create records for  Loction records
        Location_ID__c objLocation = TestDataFactory.createLocationId('Test0','Test0',1)[0];
        listLocation.add(objLocation);
        
        //Create records for Location records with Eligibility status
        Location_ID__c ringLocation = TestDataFactory.createLocationId('Testt5','Testt5',1)[0];
        ringLocation.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        ringLocation.Ethernet_Installed__c = false;
        ringLocation.AAV_Status__c = 'Available';
        ringLocation.Ring_ID__c = objLocation.Id;
        listLocation.add(ringLocation);
        
        //Create records for  Loction records with Eligibility status
        Location_ID__c objLocationEligiStatus = TestDataFactory.createLocationId('Test1','Test1',1)[0];
        objLocationEligiStatus.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[0];
        objLocationEligiStatus.Ethernet_Installed__c = true;
        objLocationEligiStatus.Ring_ID__c = objLocation.Id;
        objLocationEligiStatus.IsRingRecord__c = true;
        objLocationEligiStatus.Primary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationEligiStatus);
        
        Location_ID__c objLocationEligiStatus1 = TestDataFactory.createLocationId('Test2','Test2',1)[0];
        objLocationEligiStatus1.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[0];
        objLocationEligiStatus1.Ethernet_Installed__c = true;
        objLocationEligiStatus1.Ring_ID__c = objLocation.Id;
        objLocationEligiStatus1.IsRingRecord__c = true;
        objLocationEligiStatus1.Primary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationEligiStatus1);
        
        Location_ID__c objLocationEligiStatus2 = TestDataFactory.createLocationId('Test3','Test3',1)[0];
        objLocationEligiStatus2.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[0];
        objLocationEligiStatus2.Ethernet_Installed__c = true;
        objLocationEligiStatus2.Ring_ID__c = objLocation.Id;
        objLocationEligiStatus2.IsRingRecord__c = true;
        objLocationEligiStatus2.Primary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationEligiStatus2);
        
        Location_ID__c objLocationEligiStatus3 = TestDataFactory.createLocationId('Test4','Test4',1)[0];
        objLocationEligiStatus3.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[0];
        objLocationEligiStatus3.Ethernet_Installed__c = true;
        objLocationEligiStatus3.Ring_ID__c = objLocation.Id;
        objLocationEligiStatus3.IsRingRecord__c = true;
        objLocationEligiStatus3.Primary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationEligiStatus3);
        
        Location_ID__c objLocationEligiStatus4 = TestDataFactory.createLocationId('Test5','Test5',1)[0];
        objLocationEligiStatus4.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[0];
        objLocationEligiStatus4.Ethernet_Installed__c = true;
        objLocationEligiStatus4.Ring_ID__c = objLocation.Id;
        objLocationEligiStatus4.IsRingRecord__c = true;
        objLocationEligiStatus4.Primary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationEligiStatus4);
        
        //Create records for Location records with Eligibility status
        Location_ID__c objLocationIneligiStatus = TestDataFactory.createLocationId('Test6','Test6',1)[0];
        objLocationIneligiStatus.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationIneligiStatus.Ethernet_Installed__c = false;
        objLocationIneligiStatus.AAV_Status__c = 'Available';
        objLocationIneligiStatus.Ring_ID__c = objLocation.Id;
        objLocationIneligiStatus.Primary_Vendor_Name__c = 'Test Acc1';
        objLocationIneligiStatus.Secondary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationIneligiStatus);
        
        Location_ID__c objLocationIneligiStatus1 = TestDataFactory.createLocationId('Test7','Test7',1)[0];
        objLocationIneligiStatus1.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationIneligiStatus1.Ethernet_Installed__c = false;
        objLocationIneligiStatus1.AAV_Status__c = 'Available';
        objLocationIneligiStatus1.Ring_ID__c = objLocation.Id;
        objLocationIneligiStatus1.Primary_Vendor_Name__c = 'Test Acc1';
        objLocationIneligiStatus1.Secondary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationIneligiStatus1);
        
        Location_ID__c objLocationIneligiStatus2 = TestDataFactory.createLocationId('Test8','Test8',1)[0];
        objLocationIneligiStatus2.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationIneligiStatus2.Ethernet_Installed__c = false;
        objLocationIneligiStatus2.AAV_Status__c = 'Available';
        objLocationIneligiStatus2.Ring_ID__c = objLocation.Id;
        objLocationIneligiStatus2.Primary_Vendor_Name__c = 'Test Acc1';
        objLocationIneligiStatus2.Secondary_Vendor_Name__c = 'Test Acc';
        listLocation.add(objLocationIneligiStatus2);
        
        //Create records for Location records with Eligibility status
        Location_ID__c objLocationPrimary = TestDataFactory.createLocationId('Test9','Test9',1)[0];
        objLocationPrimary.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationPrimary.Ethernet_Installed__c = false;
        objLocationPrimary.AAV_Status__c = 'Available';
        objLocationPrimary.AAV_Primary_Contract_ID__c = '';
        objLocationPrimary.Backhual_Universe__c = true;
        listLocation.add(objLocationPrimary);

        Location_ID__c objLocationPrimary1 = TestDataFactory.createLocationId('Test10','Test10',1)[0];
        objLocationPrimary1.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationPrimary1.Ethernet_Installed__c = false;
        objLocationPrimary1.AAV_Status__c = 'Available';
        objLocationPrimary1.AAV_Primary_Contract_ID__c = '';
        objLocationPrimary1.Backhual_Universe__c = true;
        listLocation.add(objLocationPrimary1);        
        //Create records for Location records with Eligibility status
        Location_ID__c objLocationSecondary = TestDataFactory.createLocationId('Test11','Test11',1)[0];
        objLocationSecondary.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationSecondary.Ethernet_Installed__c = false;
        objLocationSecondary.AAV_Status__c = 'Available';
        objLocationSecondary.AAV_Primary_Contract_ID__c = '12344';
        objLocationSecondary.Primary_Vendor_Id__c = '12344';
        objLocationSecondary.Primary_Vendor_Name__c = 'Acc';
        objLocationSecondary.AAV_Secondary_Contract_ID__c = '';
        objLocationSecondary.Backhual_Universe__c = true;
        listLocation.add(objLocationSecondary);

        //Create records for Location records with Eligibility status
        Location_ID__c objLocationSecondaryMacro = TestDataFactory.createLocationId('TestMacro','TestMacro',1)[0];
        objLocationSecondaryMacro.Eligibility_Status__c =  System.Label.SI_LocationEligibilityStatus.split(';')[1];
        objLocationSecondaryMacro.Ethernet_Installed__c = false;
        objLocationSecondaryMacro.AAV_Status__c = 'Available';
        objLocationSecondaryMacro.AAV_Primary_Contract_ID__c = '12344';
        objLocationSecondaryMacro.Primary_Vendor_Id__c = '12344';
        objLocationSecondaryMacro.Primary_Vendor_Name__c = 'Test Acc';
        objLocationSecondaryMacro.AAV_Secondary_Contract_ID__c = '';
        objLocationSecondaryMacro.Backhual_Universe__c = true;
        listLocation.add(objLocationSecondaryMacro);
        
        //Create records for Location records with Eligibility status
        Location_ID__c objLocationPrimaryTMO = TestDataFactory.createLocationId('TestTMO','TestTMO',1)[0];
        objLocationPrimaryTMO.AAV_Primary_Contract_ID__c = '';
        objLocationPrimaryTMO.AAV_Primary_Contract_ID__c = '12345';
        objLocationPrimaryTMO.Primary_Vendor_Name__c = 'T-Mobile';
        objLocationPrimaryTMO.Secondary_Vendor_Name__c = 'Sample';
        objLocationPrimaryTMO.Secondary_Vendor_Id__c = '1234';
        objLocationPrimaryTMO.AAV_Secondary_Contract_ID__c = '1234';
        objLocationPrimaryTMO.Backhual_Universe__c = true;
        listLocation.add(objLocationPrimaryTMO);
        
        //insert location
        insert listLocation;
        
        //Create records for case
        List<Case> listOfCase = TestDataFactory.createCaseSingleIntake(3,macroDeselectRecTypeId);
        listOfCase[0].Opportunity__c = oppRecForLocationUpdate.Id;
        listOfCase[0].Deselect_Reason__c = '13 - TMO - Temporary Site/ COW';
        listOfCase[1].Opportunity__c = oppRecForLocationUpdateLOI.Id;
        listOfCase[1].Deselect_Reason__c = '55 - AAV - No space on the tower';
        listOfCase[2].Opportunity__c = oppRecForNonSatelliteVendorSecondaryMacro.Id;
        listOfCase[2].Deselect_Reason__c = '55 - AAV - No space on the tower';
        insert listOfCase;
        
        //Create records for case
        List<Case> listOfPicoBackhaulCase = TestDataFactory.createCaseSingleIntake(3,picoBackhaulRecTypeId);
        listOfPicoBackhaulCase[0].Opportunity__c = oppRecForSatelliteVendor.Id;
        listOfPicoBackhaulCase[0].Location_Ids__c = objLocationPrimary.Id;
        listOfPicoBackhaulCase[0].CIQ_Attached__c = true;
        listOfPicoBackhaulCase[1].Opportunity__c = oppRecForNonSatelliteVendorPrimary.Id;
        listOfPicoBackhaulCase[1].Location_Ids__c = objLocationPrimary.Id;
        listOfPicoBackhaulCase[1].CIQ_Attached__c = true;
        listOfPicoBackhaulCase[2].Opportunity__c = oppRecForNonSatelliteVendorSecondary.Id;
        listOfPicoBackhaulCase[2].Location_Ids__c = objLocationSecondary.Id;
        listOfPicoBackhaulCase[2].CIQ_Attached__c = true;
        insert listOfPicoBackhaulCase;
        
        List<case> listOfCaseRecs = new List<Case>();
        for(Case caseRec: [Select Id FROM Case]) {
            caseRec.Status = 'New-Submitted';
            listOfCaseRecs.add(caseRec);
        }
        update listOfCaseRecs;
        
        //Create records for Intake
        List<Intake_Site__c> listOfIntakeSites = 
            TestDataFactory.createIntakeSiteSingleIntake(8,listOfCase[0].Id,intakeCIRRecTypeId);
        listOfIntakeSites[0].Case__c = listOfCase[1].Id;
        listOfIntakeSites[0].Deselect_Type__c = System.Label.SI_Deselect_Type;
        listOfIntakeSites[1].Deselect_Type__c = System.Label.SI_Deselect_Type;
        listOfIntakeSites[3].Case__c = listOfPicoBackhaulCase[0].Id;
        listOfIntakeSites[4].Case__c = listOfPicoBackhaulCase[1].Id;
        listOfIntakeSites[5].Case__c = listOfPicoBackhaulCase[2].Id;
        listOfIntakeSites[6].Case__c = listOfCase[2].Id;
        listOfIntakeSites[6].Deselect_Type__c = System.Label.SI_Deselect_Type;
        listOfIntakeSites[7].Case__c = listOfPicoBackhaulCase[2].Id;
        insert listOfIntakeSites;
        
        List<Contract_Site__c> listOfContractSite = new List<Contract_Site__c>();
        //Create records for Contract site 
        Contract_Site__c objContractSite = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForLocationUpdate.id)[0];
        objContractSite.Location_ID__c = objLocationEligiStatus.Id;
        objContractSite.Intake_Site__c = listOfIntakeSites[2].Id;
        listOfContractSite.add(objContractSite);
 
        //Create records for Contract site 
        Contract_Site__c objContractSite1 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForContract.id)[0];        
        objContractSite1.Location_ID__c = objLocationIneligiStatus.Id;
        objContractSite1.Intake_Site__c = listOfIntakeSites[2].Id;
        listOfContractSite.add(objContractSite1);
        
        //Create records for Contract site 
        Contract_Site__c objContractSite2 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForContract.id)[0];        
        objContractSite2.Location_ID__c = objLocationEligiStatus1.Id;
        objContractSite2.Intake_Site__c = listOfIntakeSites[2].Id;
        listOfContractSite.add(objContractSite2);
        
        //Create records for Contract site 
        Contract_Site__c objContractSite3 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForContract.id)[0];        
        objContractSite3.Location_ID__c = objLocationEligiStatus2.Id;
        objContractSite3.Intake_Site__c = listOfIntakeSites[2].Id;
        listOfContractSite.add(objContractSite3);
        
        Contract_Site__c objContractSite4 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForLocationUpdateLOI.id)[0];        
        objContractSite4.Location_ID__c = objLocationEligiStatus3.Id;
        objContractSite4.Intake_Site__c = listOfIntakeSites[1].Id;
        listOfContractSite.add(objContractSite4);
        
        Contract_Site__c objContractSite5 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForLocationUpdateLOI.id)[0];        
        objContractSite5.Location_ID__c = objLocationIneligiStatus1.Id;
        objContractSite5.Intake_Site__c = listOfIntakeSites[1].Id;
        listOfContractSite.add(objContractSite5);
        
        Contract_Site__c objContractSite6 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForLocationUpdateLOI.id)[0];        
        objContractSite6.Location_ID__c = objLocationEligiStatus4.Id;
        objContractSite6.Intake_Site__c = listOfIntakeSites[0].Id;
        listOfContractSite.add(objContractSite6);
        
        Contract_Site__c objContractSite7 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForContractRejected.id)[0];        
        objContractSite7.Location_ID__c = objLocationIneligiStatus2.Id;
        objContractSite7.Intake_Site__c = listOfIntakeSites[2].Id;
        listOfContractSite.add(objContractSite7);
        
        Contract_Site__c objContractSite8 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForSatelliteVendor.id)[0];        
        objContractSite8.Location_ID__c = objLocationPrimary.Id;
        objContractSite8.Intake_Site__c = listOfIntakeSites[3].Id;
        listOfContractSite.add(objContractSite8);
        
        Contract_Site__c objContractSite9 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForNonSatelliteVendorPrimary.id)[0];        
        objContractSite9.Location_ID__c = objLocationPrimary1.Id;
        objContractSite9.Intake_Site__c = listOfIntakeSites[4].Id;
        listOfContractSite.add(objContractSite9);
        
        Contract_Site__c objContractSite10 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForNonSatelliteVendorSecondaryMacro.id)[0];        
        objContractSite10.Location_ID__c = objLocationSecondaryMacro.Id;
        objContractSite10.Intake_Site__c = listOfIntakeSites[6].Id;
        listOfContractSite.add(objContractSite10);
        
        Contract_Site__c objContractSite11 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForNonSatelliteVendorSecondary.id)[0];
        objContractSite11.Location_ID__c = objLocationSecondary.Id;
        objContractSite11.Intake_Site__c = listOfIntakeSites[5].Id;
        listOfContractSite.add(objContractSite11);
        
        Contract_Site__c objContractSite12 = 
            TestDataFactory.createConractSiteSingleIntake(1,oppRecForNonSatelliteVendorPrimary.id)[0];        
        objContractSite12.Location_ID__c = objLocationPrimaryTMO.Id;
        objContractSite12.Intake_Site__c = listOfIntakeSites[7].Id;
        listOfContractSite.add(objContractSite12);
        
        insert listOfContractSite; 
        
        
        List<ContentVersion> listOfContentVersion = new List<ContentVersion>();
        ContentVersion contentVersion1 = new ContentVersion(
        Title = System.Label.SI_ContractTitle,
        PathOnClient = System.Label.SI_ContractTitle+'.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        listOfContentVersion.add(contentVersion1);
        
        ContentVersion contentVersion2 = new ContentVersion(
        Title = System.label.SI_CoversheetTitle.split(';')[0],
        PathOnClient = System.label.SI_CoversheetTitle.split(';')[0]+'.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        listOfContentVersion.add(contentVersion2);
        
        ContentVersion contentVersion3 = new ContentVersion(
        Title = System.label.SI_ContractTitle,
        PathOnClient = System.Label.SI_ContractTitle+'.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        listOfContentVersion.add(contentVersion3);
        
        ContentVersion contentVersion4 = new ContentVersion(
        Title = System.label.SI_AttachmentName.split(';')[0],
        PathOnClient = System.label.SI_AttachmentName.split(';')[0]+'.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        listOfContentVersion.add(contentVersion4);
        
        ContentVersion contentVersion5 = new ContentVersion(
        Title = System.label.SI_SmallCellAttchment.split(';')[0],
        PathOnClient = System.label.SI_SmallCellAttchment.split(';')[0]+'.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        listOfContentVersion.add(contentVersion5);
        
        insert listOfContentVersion;
        
        List<dsfs__DocuSign_Status__c> listOfDocuSign = 
            TestDataFactory.createDocuSignStatusSingleIntake(2,objAccount.Id, oppRecForLocationUpdate.Id);
        for(dsfs__DocuSign_Status__c docuSign : listOfDocuSign) {
            docuSign.dsfs__Envelope_Status__c = System.Label.SI_Contract_Status;
        }
        insert listOfDocuSign;
        
        List<ContentDocumentLink> lstContentDoclink = new List<ContentDocumentLink>();
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = listOfDocuSign[0].Id;
        contentlink.ContentDocumentId = documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        lstContentDoclink.add(contentlink);
        
        ContentDocumentLink contentlink1 = new ContentDocumentLink();
        contentlink1.LinkedEntityId = listOfDocuSign[1].Id;
        contentlink1.ContentDocumentId = documents[1].Id;
        contentlink1.Visibility = 'AllUsers'; 
        lstContentDoclink.add(contentlink1);
        
        ContentDocumentLink contentlink3 = new ContentDocumentLink();
        contentlink3.LinkedEntityId = oppRecForFSOContract.Id;
        contentlink3.ContentDocumentId = documents[2].Id;
        contentlink3.Visibility = 'AllUsers'; 
        lstContentDoclink.add(contentlink3);
        
        ContentDocumentLink contentlink4 = new ContentDocumentLink();
        contentlink4.LinkedEntityId = oppRecForFSOContract1.Id;
        contentlink4.ContentDocumentId = documents[2].Id;
        contentlink4.Visibility = 'AllUsers'; 
        lstContentDoclink.add(contentlink4);
        
        ContentDocumentLink contentlink5 = new ContentDocumentLink();
        contentlink5.LinkedEntityId = oppRecForFSOContract1.Id;
        contentlink5.ContentDocumentId = documents[3].Id;
        contentlink5.Visibility = 'AllUsers';
        lstContentDoclink.add(contentlink5); 
        
        ContentDocumentLink contentlink6 = new ContentDocumentLink();
        contentlink6.LinkedEntityId = oppRecForFSOContract3.Id;
        contentlink6.ContentDocumentId = documents[3].Id;
        contentlink6.Visibility = 'AllUsers'; 
        lstContentDoclink.add(contentlink6);
        
        ContentDocumentLink contentlink7 = new ContentDocumentLink();
        contentlink7.LinkedEntityId = oppRecForFSOContract.Id;
        contentlink7.ContentDocumentId = documents[4].Id;
        contentlink7.Visibility = 'AllUsers'; 
        lstContentDoclink.add(contentlink7);
        
        insert lstContentDoclink;
        
        triggerManager.DisabledObjects__c = '';
        update triggerManager;
        Test.stopTest();
    }
    
    /**************************************************************************
    * @description - to test trigger functionality
    **************************************************************************/
    @isTest
    private static void testTrigger() {
        List<Opportunity> listOfLostOpportunity = new List<Opportunity>();
        List<Opportunity> listOfWonOpportunity = new List<Opportunity>();
        List<Opportunity> listOfOpportunity = [SELECT Id,Name
                                               FROM Opportunity];
        //Intialize list of Opportunity to update records
        List<Opportunity> lstOfOpportunitiesToUpdate = new List<Opportunity>();
        Test.startTest();
        for(Opportunity objOpp : listOfOpportunity) {
            objOpp.ETL_MRC__c = 10000000.00;
            objOpp.ETL_NRC__c = 11000000.00;
            objOpp.TCM_Upload_Required__c = true;
            objOpp.FSO_Required__c = 'Yes';
            objOpp.Contract_Signatures_Complete__c = true;
            if(objOpp.Name == 'Lost Opp') {
                objOpp.StageName = System.Label.SI_OppStageName.split(';')[2];
                objOpp.Reason_for_Cancel__c = 'Cancelled by Business';
                listOfLostOpportunity.add(objOpp);
            } else {
                objOpp.StageName = System.Label.SI_OppStageName.split(';')[0];
                listOfWonOpportunity.add(objOpp);
            }
            lstOfOpportunitiesToUpdate.add(objOpp);
        }
        // update Opportunity Records
        update lstOfOpportunitiesToUpdate;
        Test.stopTest();
        System.assertEquals(true,!lstOfOpportunitiesToUpdate.isEmpty(),'List of Total Opportunitnies is not Blank');
        System.assertEquals(true,!listOfLostOpportunity.isEmpty() && listOfLostOpportunity[0].StageName == System.Label.SI_OppStageName.split(';')[2],'List of Closed Lost Opportunitnies is not Blank');
        System.assertEquals(true,!listOfWonOpportunity.isEmpty() && listOfWonOpportunity[0].StageName == System.Label.SI_OppStageName.split(';')[0],'List of Closed Won Opportunitnies is not Blank');
    } 
}
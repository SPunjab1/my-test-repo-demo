/*********************************************************************************************************************
*  @Class            : BatchLoginToNeustarAPITest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 16.07.2020
*  @Description      : Test class for BatchLoginToNeustarAPI
**********************************************************************************************************************/
@isTest(SeeAllData=false)
public class BatchLoginToNeustarAPITest {
   // public static String CRON_EXP = '5 ? * 0 0 0';
    static testMethod void testBatchForLogin(){
        TMobilePh2LoginDetailsHierarchy__c setting = new TMobilePh2LoginDetailsHierarchy__c();
        setting.Neustar_Domain__c = 'Test';
        setting.Neustar_Username__c = 'UOMUsername';
        setting.Neustar_Password__c = 'test';
        setting.Neustar_End_Point_URL__c ='www.testurl.com';
        insert setting;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        BatchLoginToNeustarAPI batch = new BatchLoginToNeustarAPI();
        Database.executeBatch(batch,1);
        //system.schedule('Jobs', CRON_EXP, batch);
        SchedularForNeustarLogin loginSch =  new SchedularForNeustarLogin();
		loginSch.execute(null);
        Test.stopTest();

        TMobilePh2LoginDetailsHierarchy__c authTokCustomSetting = [SELECT Id,Atoms_Access_Token__c,Neustar_Access_Token__c
                                                                   FROM TMobilePh2LoginDetailsHierarchy__c];
        
        system.assertNotEquals(null,authTokCustomSetting.Neustar_Access_Token__c);
        
        

    }
    
}
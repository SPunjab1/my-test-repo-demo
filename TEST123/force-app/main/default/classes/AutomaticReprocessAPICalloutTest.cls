@isTest(SeeAllData = false)
public class AutomaticReprocessAPICalloutTest {
     Public Static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
    
     @TestSetup
    static void testDataSetup(){
        
        TMobilePh2LoginDetailsHierarchy__c atomsToken = new TMobilePh2LoginDetailsHierarchy__c();
        atomsToken.Atoms_Client_Id__c = 'testClientID';
        atomsToken.Atoms_Client_Secret__c = 'testClientSec';
        atomsToken.Atoms_End_Point__c = 'https://testokta.com';
        insert atomsToken;

        List<SwitchAPICallOuts__c> switchAPIMapSet = new List<SwitchAPICallOuts__c>();
        switchAPIMapSet = TestDataFactoryOMph2.createSwitchAPICalloutCustSet();
        insert switchAPIMapSet;
        
        List<ATOMSNeuStarUtilitySettings__c> atomsUtilitySet = new List<ATOMSNeuStarUtilitySettings__c>();
        atomsUtilitySet = TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert atomsUtilitySet;
        
        system.debug('atomsUtilitySet>>'+atomsUtilitySet);
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId(); 
        
        List<Case> workOrdTaskList = new List<Case>();
        workOrdTaskList = TestDataFactoryOMph2.createCase(null,1,recTypeId);
        workOrdTaskList[0].OM_Integration_Status__c = 'Start Integration';
        insert workOrdTaskList;
        system.debug('WORD>>'+workOrdTaskList);
        
        List<Purchase_Order__c> purOrderList = new List<Purchase_Order__c>();
        purOrderList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',1,workOrdTaskList[0].Id);
        insert purOrderList;
        system.debug('PO>>'+purOrderList);
        
        List<Circuit__c> circuitList = new List<Circuit__c>();
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[0].Circuit_Type__c = 'Circuit: Ethernet';
        
        circuitList.addAll(TestDataFactoryOMph2.createCircuitRecords(workOrdTaskList[0].Id,1));
        circuitList[1].Circuit_Type__c = 'Existing';
        insert circuitList;
        system.debug('circuitList>>'+circuitList);
        
        
        List<ASR__c> asrList = new List<ASR__c>();
        asrList = TestDataFactoryOMph2.createASRRecords(workOrdTaskList[0].Id,purOrderList[0].Id,null,1);
        asrList[0].Circuit_Id__c = circuitList[0].Id;
        insert asrList;
        System.assertEquals('New Service: Ethernet', asrList[0].ASR_Type__c);
        system.debug('ASR>>'+asrList);
        
        List<Integration_Log__c> intLogList = new List<Integration_Log__c>();
        intLogList = TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Update ATOMS in SFDC',2);
        intLogList[1].Status__c = 'Failure';
        intLogList[1].Reprocess__c = true;
        intLogList[1].From__c = 'Neustar';
        intLogList[1].To__c = 'Salesforce';
        intLogList[1].TP_Response_Type__c = 'TP CONFIRMED';
        intLogList[1].Purchase_Ord_PON__c = purOrderList[0].PON__c;
        intLogList[1].PON__c = asrList[0].PON__c;
        intLogList[1].ASR__c = asrList[0].Id;
        intLogList[1].Pending_ATOMS_Update__c = true;
        intLogList[1].Pending_Salesforce_Update__c = false;
        intLogList[1].Request__c = '{"universalOrder":[],"response":[{"responseType":"TP CONFIRMED","sections":[{"sectionName":"Administration Section","fields":[{"fieldName":"CDTSENT","guiAliasName":"CD/TSENT","path":["CN_Form","cn_adminsection"],"value":"06-21-2020-0548PM"},{"fieldName":"CDTSENT","guiAliasName":"CD/TSENT","path":["CN_Form","cn_adminsection"],"value":"06-22-2099-0548PM"},{"fieldName":"PROJECT","guiAliasName":"PROJECT","path":["CN_Form","cn_adminsection"],"value":"TMO-EVCUPSEPT"},{"fieldName":"ASR_NO","guiAliasName":"ASR_NO","path":["CN_Form","cn_adminsection"],"value":"8773888"},{"fieldName":"DD","guiAliasName":"DD","path":["CN_Form","cn_adminsection"],"value":"09-09-2060"}],"index":1,"isEnabled":"y","subSections":[]},{"sectionName":"Service Section","fields":[{"fieldName":"ORD","guiAliasName":"ORD","path":["CN_Form","servicesection"],"value":"C2UL22SEPT"}],"index":1,"isEnabled":"y","subSections":[]},{"sectionName":"Circuit Section","fields":[],"index":1,"isEnabled":"y","subSections":[{"sectionName":"Virtual Circuit Section","fields":[{"fieldName":"VCID","guiAliasName":"VCID","path":["CN_Form","circuitsection","virtualcircuitsection"],"value":"AA.VLXW.04372SEPTT"},{"fieldName":"VCORD","guiAliasName":"VC ORD","path":["CN_Form","circuitsection","virtualcircuitsection"],"value":"C2UL7947"}],"index":1,"isEnabled":"y","subSections":[]}]}]}],"info":{"productCatalogName":"EVC PROTECTED CHANGE","productCatalogId":140951,"uomOrderKey":1459251,"uomOrderID":"TUOMUNI-N00025781","requestNumber":"5RA0737C-EVC1","ver":"01","orderStatus":"TP CONFIRMED","activityId":"C","activityType":"Change","application":"ASR_SEND","tradingPartner":"BAS","project":"TMO-EVCUPGRADE","owner":"no288020","createdDate":"2020-06-22 17:03:14.585","lastStatusChangeDate":"2020-06-22 17:52:22.117","sellerID":"NJ90","buyerID":"OPT","responseSeqId":2986}}';

        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Add Circuit',1));
        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Get Circuit',1));
        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Update Circuit in ATOMS',1));
        intLogList[2].Circuit__c = circuitList[0].Id;
        intLogList[2].Status__c = 'Failure';
        intLogList[2].Reprocess__c = true;
        intLogList[3].Circuit__c = circuitList[0].Id;
        intLogList[3].Status__c = 'Failure';
        intLogList[3].Reprocess__c = true;
        intLogList[4].Circuit__c = circuitList[0].Id;
        intLogList[4].Status__c = 'Failure';
        intLogList[4].Reprocess__c = true;
        
        
        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'CreateASR',1));
        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'UpgradeASR',1));
        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'DisconnectASR',1));
        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Update ASR in ATOMS',1));
        intLogList[5].ASR__c = asrList[0].Id;
        intLogList[5].Status__c = 'Failure';
        intLogList[5].Reprocess__c = true;
        intLogList[6].ASR__c = asrList[0].Id;
        intLogList[6].Status__c = 'Failure';
        intLogList[6].Reprocess__c = true;
        intLogList[7].ASR__c = asrList[0].Id;
    intLogList[7].Status__c = 'Failure';
        intLogList[7].Reprocess__c = true;        
        intLogList[8].ASR__c = asrList[0].Id;
        intLogList[8].Status__c = 'Failure';
        intLogList[8].Reprocess__c = true;

        intLogList.addAll(TestDataFactoryOMph2.createIntegrationLogs(workOrdTaskList[0].Id,'Create Purchase Order',1));
    intLogList[9].Purchase_Order__c = purOrderList[0].Id;
        intLogList[9].Status__c = 'Failure';
        intLogList[9].Reprocess__c = true;

        system.debug('intLogList>>'+intLogList);
        insert intLogList;
        
     
     }
     
     static testMethod void testCircuitOGValidation(){
         Test.startTest();
			AutomaticReprocessAPICallout obj = new AutomaticReprocessAPICallout();
            DataBase.executeBatch(obj); 
        Test.stopTest();
     
     }

}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : CloseWorkOrderTaskTriggerHandlerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 21.06.2019
*  @Description      : Test class for CloseWorkOrderTaskTriggerHandler
**********************************************************************************************************************
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class CloseWorkOrderTaskTriggerHandlerTest {
    
    /***************************************************************************************************
* @Description  : Test Method to test closeWorkOrderTask 
***************************************************************************************************/
    static  testmethod void runTestcloseWorkOrderTask() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='Not Required';
        workOrderTaskLst[0].PIER_Work_Order_ID__c='';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Closed';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);        
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    static  testmethod void runTestcloseWorkOrderTask1() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_ID__c='Not Required';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Completed';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);
        workOrderTaskLst[0].PIER_Work_Order_ID__c ='12345';  
        update workOrderTaskLst[0];
        Test.stopTest();
        //System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    
    static  testmethod void runTestcloseWorkOrderTask2() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_ID__c='12345';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Deployment Ready';        
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);
        workOrderTaskLst[0].status = workOrdereventLst[0].Work_Order_Task_Status__c;
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;        
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    
    static  testmethod void runTestcloseWorkOrderTaskErrorLog()
    {
        List<Case> workOrderTaskLst = new List<Case>();        
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_ID__c='7890';
        workOrderTaskLst[0].status='New';
        workOrderTaskLst[0].Attachment_Required__c=true;
        workOrderTaskLst[0].No_Attachment_Reason__c=null;
        Test.startTest();
        insert workOrderTaskLst;
        
        Close_Work_Order__e workOrdereventLst = new Close_Work_Order__e();
        workOrdereventLst.Work_Order_Task_Status__c = 'Not Required'; 
        workOrdereventLst.Work_Order_Task_Id__c = '7890';
        EventBus.publish(workOrdereventLst);
        Test.stopTest();
    }
    
    static  testmethod void runTestcloseWorkOrderTaskException()
    {
        List<Case> workOrderTaskLst = new List<Case>();        
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_ID__c='7890';
        workOrderTaskLst[0].status='New';
        Test.startTest();
        insert workOrderTaskLst;
        
        Integration_Log__c log = new Integration_Log__c();
        log.API_Type__c = 'DWH Close Task';
        log.Task__c = workOrderTaskLst[0].Id;
        insert log;
        
        Close_Work_Order__e workOrderevent = new Close_Work_Order__e();
        workOrderevent.Work_Order_Task_Status__c = 'Draft'; 
        workOrderevent.Work_Order_Task_Id__c = '7890';
        EventBus.publish(workOrderevent);
        Test.stopTest();
    }
    
    static  testmethod void runTestInProgressWorkOrderTaskException(){
        List<Case> workOrderTaskLst = new List<Case>();        
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='7890';
        workOrderTaskLst[0].status='New';
        Test.startTest();
        insert workOrderTaskLst;
        
        Integration_Log__c log = new Integration_Log__c();
        log.API_Type__c = 'DWH Close Task';
        log.Task__c = workOrderTaskLst[0].Id;
        insert log;
        
        Close_Work_Order__e workOrderevent = new Close_Work_Order__e();
        workOrderevent.Work_Order_Task_Status__c = 'InProgress'; 
        workOrderevent.Work_Order_Task_Id__c = '7890';
        EventBus.publish(workOrderevent);
        Test.stopTest();
    }
    static  testmethod void runTestDeniedWorkOrderTaskException(){
        List<Case> workOrderTaskLst = new List<Case>();        
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_ID__c='7890';
        workOrderTaskLst[0].status='New';
        Test.startTest();
        insert workOrderTaskLst;
        
        Integration_Log__c log = new Integration_Log__c();
        log.API_Type__c = 'DWH Close Task';
        log.Task__c = workOrderTaskLst[0].Id;
        insert log;
        
        Close_Work_Order__e workOrderevent = new Close_Work_Order__e();
        workOrderevent.Work_Order_Task_Status__c = 'Denied'; 
        workOrderevent.Work_Order_Task_Id__c = '7890';
        EventBus.publish(workOrderevent);
        Test.stopTest();
    }

    static  testmethod void runTestInProgresssWorkOrderTask() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='Not Required';
        workOrderTaskLst[0].PIER_Work_Order_ID__c='';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'InProgress';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);        
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    
    
    static  testmethod void runTestWorkingWorkOrderTask() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='Not Required';
        workOrderTaskLst[0].PIER_Work_Order_ID__c='';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Waiting';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);        
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    
    static  testmethod void runTestwaitingWorkOrderTask() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='Not Required';
        workOrderTaskLst[0].PIER_Work_Order_ID__c='';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Terminated';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);        
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }  
    
    static  testmethod void runTestResolvedWorkOrderTask() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='Not Required';
        workOrderTaskLst[0].PIER_Work_Order_ID__c='';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Resolved';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);        
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    
    static  testmethod void runTestDeniedWorkOrderTask() {
        List<Case> workOrderTaskLst = new List<Case>();        
        List<Close_Work_Order__e> workOrdereventLst = new List<Close_Work_Order__e>();
        String pierId='12345';
        integer numofRec=1;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        workOrderTaskLst=TestDataFactory.createCase('TIFS AAV CIR Retest',numofRec,recTypeId );
        workOrderTaskLst[0].PIER_Work_Order_Task_ID__c='Not Required';
        workOrderTaskLst[0].PIER_Work_Order_ID__c='';
        insert workOrderTaskLst;
        workOrdereventLst=TestDataFactory.createWorkOrderIds(pierId);
        workOrdereventLst[0].Work_Order_Task_Status__c = 'Denied';
        Test.startTest();
        EventBus.publish(workOrdereventLst);
        delete(workOrderTaskLst);
        undelete(workOrderTaskLst);        
        workOrderTaskLst[0].PIER_Work_Order_ID__c = workOrdereventLst[0].Work_Order_Task_Id__c;
        update workOrderTaskLst[0];
        Test.stopTest();
        // System.assertequals(workOrderTaskLst[0].PIER_Work_Order_Task_ID__c,workOrdereventLst[0].Work_Order_Task_Id__c);        
    }
    
    
}
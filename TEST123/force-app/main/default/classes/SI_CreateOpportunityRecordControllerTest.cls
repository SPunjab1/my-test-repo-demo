/******************************************************************************
@author      : IBM
@date        : 12.06.2020
@description : Test class for SI_CreateOpportunityRecordPageController apex class
@Version     : 1.0
*******************************************************************************/
@isTest
private class SI_CreateOpportunityRecordControllerTest {
    
    /**************************************************************************
    * @description Method to setup test data
    **************************************************************************/
    @testSetup
    private static void testDataSetup(){
        //Intialize list of users
        List<User> listOfUsers = new List<User>();
        //create single intake user
        User intakeUser = TestDataFactory.createUserByProfileName(Label.SI_IntakeUserProfileName);
        listOfUsers.add(intakeUser);
        //insert users
        insert listOfUsers;
        //Intialize list of opportunity records
        List<Opportunity> listOfOppRecords = new List<Opportunity>();
        //Fetching record type id for opportunity object
        Id macroDeselectRecTypeId1 = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',Label.SI_OpportunityRT.split(';')[0]); 
        Id macroDeselectRecTypeId2 = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',Label.SI_OpportunityRT.split(';')[3]);
        Id macroDeselectRecTypeId3 = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',Label.SI_OpportunityRT.split(';')[1]);
        Id macroDeselectRecTypeId4 = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',Label.SI_OpportunityRT.split(';')[2]);
        //create opportunity for single intake user
        Opportunity mdOppRec1 = TestDataFactory.createOpportunitySingleIntake(1,macroDeselectRecTypeId1)[0];
        listOfOppRecords.add(mdOppRec1);
        Opportunity mdOppRec2 = TestDataFactory.createOpportunitySingleIntake(1,macroDeselectRecTypeId2)[0];
        listOfOppRecords.add(mdOppRec2);
        Opportunity mdOppRec3 = TestDataFactory.createOpportunitySingleIntake(1,macroDeselectRecTypeId3)[0];
        listOfOppRecords.add(mdOppRec3);
        Opportunity mdOppRec4 = TestDataFactory.createOpportunitySingleIntake(1,macroDeselectRecTypeId4)[0];
        listOfOppRecords.add(mdOppRec4);
        //insert opportunity records
        insert listOfOppRecords;
        //Intialize list of case records
        List<Case> listOfCaseRecords = new List<Case>();
        //Fetching record type id for case object
        Id macroDeselectRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]); 
        Id smallCellDesRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case', Label.SI_CaseSmallCellDesRT.split(';')[0]);
        Id retailRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case', Label.SI_Opp_VICC_Case_RT.split(';')[0]);
        Id smallCellRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case', Label.SI_CaseSmallCellRT.split(';')[0]);
        Id caseMLARecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case', Label.SI_CaseMLART.split(';')[0]);
        //create case for Backhaul RT
        Case mdCaseRec1 = TestDataFactory.createCaseSingleIntake(1,macroDeselectRecTypeId)[0];
        mdCaseRec1.OwnerId = intakeUser.Id;
        Account accRec=TestDataFactory.createAccountSingleIntake(1)[0];
        insert accRec;
        if(accRec!=null){
        mdCaseRec1.Selected_Vendor__c=accRec.Id;
        mdCaseRec1.Deselect_Reason__c=Label.SI_DeselectReasonTest;
        }
        listOfCaseRecords.add(mdCaseRec1); 
        //create case for Retail RT
        Case mdCaseRec2 = TestDataFactory.createCaseSingleIntake(1,retailRecTypeId)[0];
        mdCaseRec2.OwnerId = intakeUser.Id;
        mdCaseRec2.Deselect_Reason__c=Label.SI_DeselectReasonTest;
        listOfCaseRecords.add(mdCaseRec2);
        //create case for Small Cell RT
        Case mdCaseRec3 = TestDataFactory.createCaseSingleIntake(1,smallCellRecTypeId)[0];
        mdCaseRec3.OwnerId = intakeUser.Id;
        mdCaseRec3.Deselect_Reason__c=Label.SI_DeselectReasonTest;
        listOfCaseRecords.add(mdCaseRec3);
        //create case for MLA RT
        Case mdCaseRec4 = TestDataFactory.createCaseSingleIntake(1,caseMLARecTypeId)[0];
        mdCaseRec4.OwnerId = intakeUser.Id;
        mdCaseRec4.Deselect_Reason__c=Label.SI_DeselectReasonTest;
        listOfCaseRecords.add(mdCaseRec4);
        //create case for Small Cell Deselect RT
        Case mdCaseRec5 = TestDataFactory.createCaseSingleIntake(1,smallCellDesRecTypeId)[0];
        mdCaseRec5.OwnerId = intakeUser.Id;
        mdCaseRec5.Deselect_Reason__c=Label.SI_DeselectReasonTest;
        listOfCaseRecords.add(mdCaseRec5);
        //create case with opportunity
        Case mdCaseRec6 = TestDataFactory.createCaseSingleIntake(1,retailRecTypeId)[0];
        mdCaseRec6.OwnerId = intakeUser.Id;
        mdCaseRec6.opportunity__c=mdOppRec2.Id;
        mdCaseRec6.Deselect_Reason__c=Label.SI_DeselectReasonTest;
        listOfCaseRecords.add(mdCaseRec6);
        //insert Case records
        insert listOfCaseRecords;
        
        ContentVersion contentVersion1 = new ContentVersion(
        Title = System.label.SI_SmallCellAttchment.split(';')[0],
        PathOnClient = System.label.SI_SmallCellAttchment.split(';')[0]+'.jpg',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        
        insert contentVersion1;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = listOfCaseRecords[0].Id;
        contentlink.ContentDocumentId = documents[0].Id;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
    }
    
    /**************************************************************************************
    * @description To test createOpportunityRecord method functionality for Backhaul RT
    ***************************************************************************************/
    @isTest
    private static void testCreateOpportunityRecord1() {
        //Fetch single intake user details
        User intakeUser = [SELECT Id
                           FROM User
                           WHERE Username LIKE '%@sitestuser.com%'
                           AND Name = 'SITest User'
                           AND Profile.Name = :Label.SI_IntakeUserProfileName];
        //Fetch record type ID's for case and opportunity
        Id caseRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:caseRecTypeId];
        Test.startTest();
        //Run apex class in context of internal user
        System.runAs(intakeUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.id);
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
            sc.createOpportunityRecord();
            sc.backToCaseRecord();
        }
        Test.stopTest();
        //Fetch updated case record
        List<Case> updatedCaseRec = [SELECT Id, Opportunity__c FROM Case
                                     WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(1, updatedCaseRec.size(), 'record size differs');
    }
    
    /********************************************************************************************************
    * @description To test createOpportunityRecord method functionality for Backhaul Small Cell Deselect RT 
    *********************************************************************************************************/
    @isTest
    private static void testCreateOpportunityRecord2() {
        //Fetch single intake user details
        User intakeUser = [SELECT Id
                           FROM User
                           WHERE Username LIKE '%@sitestuser.com%'
                           AND Name = 'SITest User'
                           AND Profile.Name = :Label.SI_IntakeUserProfileName];
        //Fetch record type ID's for case and opportunity
        Id caseRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseSmallCellDesRT.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:caseRecTypeId];
        Test.startTest();
        //Run apex class in context of internal user
        System.runAs(intakeUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.id);
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
            sc.createOpportunityRecord();
            sc.backToCaseRecord();
        }
        Test.stopTest();
        //Fetch updated case record
        List<Case> updatedCaseRec = [SELECT Id, Opportunity__c FROM Case
                                     WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(1, updatedCaseRec.size(), 'record size differs');
    }
    
    /***************************************************************************************
    * @description To test createOpportunityRecord method functionality for VICC Retail RT 
    ****************************************************************************************/
    @isTest
    private static void testCreateOpportunityRecord3() {
        //Fetch single intake user details
        User intakeUser = [SELECT Id
                           FROM User
                           WHERE Username LIKE '%@sitestuser.com%'
                           AND Name = 'SITest User'
                           AND Profile.Name = :Label.SI_IntakeUserProfileName];
        Id retailRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case', Label.SI_Opp_VICC_Case_RT.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:retailRecTypeId and opportunity__c!=null];
        Test.startTest();
        //Run apex class in context of internal user
        System.runAs(intakeUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.id);
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
            sc.createOpportunityRecord();
            sc.backToCaseRecord();
        }
        Test.stopTest();
        //Fetch updated case record
        List<Case> updatedCaseRec = [SELECT Id, Opportunity__c
                                     FROM Case
                                     WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(1, updatedCaseRec.size(), 'record size differs');
    }
    
    /***************************************************************************************
    * @description To test createOpportunityRecord method functionality for VICC Retail RT 
    ****************************************************************************************/
    @isTest
    private static void testCreateOpportunityRecord4() {
        //Fetch single intake user details
        User intakeUser = [SELECT Id
                           FROM User
                           WHERE Username LIKE '%@sitestuser.com%'
                           AND Name = 'SITest User'
                           AND Profile.Name = :Label.SI_IntakeUserProfileName];
        Id retailRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case', Label.SI_Opp_VICC_Case_RT.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:retailRecTypeId and opportunity__c=null];
        Test.startTest();
        //Run apex class in context of internal user
        System.runAs(intakeUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.id);
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
            sc.createOpportunityRecord();
            sc.backToCaseRecord();
        }
        Test.stopTest();
        //Fetch updated case record
        List<Case> updatedCaseRec = [SELECT Id, Opportunity__c
                                     FROM Case
                                     WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(1, updatedCaseRec.size(), 'record size differs');
    }
    
    /*********************************************************************************************
    * @description To test createOpportunityRecord method functionality for Small Cell RT
    *********************************************************************************************/
    @isTest
    private static void testCreateOpportunityRecord5() {
        //Fetch single intake user details
        Id voiceRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseSmallCellRT.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:voiceRecTypeId];
        Test.startTest();
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the opportunity id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.id);
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
            sc.createOpportunityRecord();
            sc.backToCaseRecord();
        Test.stopTest();
        //Fetch updated case record
        List<Case> updatedCaseRec = [SELECT Id, Opportunity__c
                                     FROM Case
                                     WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(1, updatedCaseRec.size(), 'record size differs');
    }
    
    /*********************************************************************************************
    * @description To test createOpportunityRecord method functionality for MLA RT
    *********************************************************************************************/
    @isTest
    private static void testCreateOpportunityRecord6() {
        Id voiceRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseMLART.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:voiceRecTypeId];
        Test.startTest();
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the opportunity id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.id);
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
            sc.createOpportunityRecord();
            sc.backToCaseRecord();
        Test.stopTest();
        //Fetch updated case record
        List<Case> updatedCaseRec = [SELECT Id, Opportunity__c
                                     FROM Case
                                     WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(1, updatedCaseRec.size(), 'record size differs');
    }
    
    /**************************************************************************************
    * @description To test createOpportunityRecord method functionality for exception
    ***************************************************************************************/
    @isTest
    private static void testConstuctorException() {
        Id voiceRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case
                        WHERE RecordTypeId=:voiceRecTypeId];
        Test.startTest();
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_CreateOpportunityRecordPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id','Random');
            try{
            SI_CreateOpportunityRecordPageController sc = new SI_CreateOpportunityRecordPageController(new ApexPages.StandardController(caseRec));
                sc.createOpportunityRecord(); 
                sc.backToCaseRecord();
            }
            catch(DmlException e) {
                system.assertEquals(false,e.getMessage().contains('Random'),'exception handled');
            }
        Test.stopTest();
    }
}
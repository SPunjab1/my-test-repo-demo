@isTest
public class APIHandlerTest {
    
    static testmethod void GetToken(){
        APIHandler.Tokenwrapper tokenWrapObj=new APIHandler.Tokenwrapper();
        tokenWrapObj.id_token='id_token';
        tokenWrapObj.scope='scope';
        tokenWrapObj.expires_in='expires_in';
        tokenWrapObj.access_token='access_token';
        tokenWrapObj.refresh_token='refresh_token';
        tokenWrapObj.token_type='token_type';

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PierIntegrationMock(200, '{"id_token":"eyJraWQiOiI0NDY3MzUxNy04","access_token":"eyJraWQiOiI0NDY3MzUxNy04","expires_in":3600000,"token_type":"Bearer","resource":"qd97tVnm3TZZD8ArMmkVt9XWgHcD5eTz","refresh_token":"","scope":""}'));
        string tkn = APIHandler.getApigeeToken();
        
        Map<String,String> participantMap = New Map<String,String>();
        participantMap = APIHandler.getPIERParticipants(true);
        Test.stopTest();
        
    }
    
    static testmethod void TestServiceWorklogAndAttachments(){
        Id WOTRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();
        Id WORecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Work Order').getRecordTypeId();
        List<Case> insertcaseList = New List<Case>();
        List<Case> cases = New List<Case>();
        case csp = New case(Status ='New',Origin = 'Intake Request',recordtypeid = WORecordTypeId);
        insert csp;
        
        case cs1 = New case(recordtypeid = WOTRecordTypeId,Project_Name__c = 'test project',User_Description__c = 'test UD', ParentId = csp.Id,Task_Number__c = 1,Origin = 'Intake Request', Work_Log_Entry__c = 'Test worklog');
        insertcaseList.add(cs1);
        
        case cs2 = New case(recordtypeid = WOTRecordTypeId,Origin = 'Intake Request', ParentId = csp.Id, attachmentsFromTasks__c = '1',WorklogsFromTasks__c ='1',Task_Number__c = 2);
        insertcaseList.add(cs2);
        cases.add(cs2);
        insert insertcaseList;
        
        ContentVersion cv = new ContentVersion(Title = 'Test Document',PathOnClient = 'TestDocument.pdf',VersionData = Blob.valueOf('Test Content'),IsMajorVersion = true);
        Insert cv;
        
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = cs1.Id;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        Insert cdl;
        
        List<Id> ilogIds = new List<Id>();
        Integration_Log__c ilog = new Integration_Log__c();
        ilog.Work_Order__c = cs2.Id;
        ilog.API_Type__c = 'NA Task';
        insert ilog;
        ilogIds.add(ilog.Id);
        
        Worklog_Entry__c wlog = New Worklog_Entry__c();
        wlog.Task__c = cs1.Id;
        wlog.Comment__c = 'Test worklog';
        Insert wlog;
            
        Map<Id,Case> caseMap = new Map<Id,Case>(cases);
        
        Test.StartTest(); 
       // PierIntegrationAPI.GetAttchmentsFromIntegrationLog_Service(ilog,caseMap);
        PierIntegrationAPI.GetWorkLogsFromIntegrationLog_Service(ilog,caseMap);
        Test.StopTest();
        
    }
    
}
@isTest
public class EntitlementCaptureWithRelated_test {
    @isTest
    public static void testECCapture(){
        test.starttest();
		EntitlementCapturesWithRelated_Ctrl ec = new EntitlementCapturesWithRelated_Ctrl();
        // Contract Record Type
        Id strContractRT = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Contract','Site_Lease');
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0];
        insert objAccount;
         List<Location_Id__c> sites =  TestDataFactory.createLocationId('site','site',2);
        insert sites;
        //insert Contract 
        List<Contract> lstContract = TestDataFactory.createContract(2,strContractRT);
        for(Contract objCon : lstContract){
            objCon.AccountId = objAccount.Id;
            objCon.Site_ID__c = sites[0].Id; 
            objCon.Siterra_Status__c = 'Active';
        }
        insert lstContract;
        
        
        Equipment__c equ = new Equipment__c();
        equ.Name = 'a2c0U000001bsUK';
        equ.EquipmentModel__c = '2D4WC-21';
        equ.EquipmentManufacturer__c = 'ROSENBERGER';
        equ.Weight__c = 66.1;
        equ.Width__c = 22.8;
        equ.Length__c = 48;
        equ.Depth__c = 7.4;
		equ.EquipmentType__c = 'Antenna';
        insert equ;

        Equipment__c equ1 = new Equipment__c();
        equ1.Name = 'a2c0U000001bsUT';
        equ1.EquipmentModel__c = '110045';
        equ1.EquipmentManufacturer__c = 'CELLMAX';
        equ1.Weight__c = 66.1;
        equ1.Width__c = 22.8;
        equ1.Length__c = 48;
        equ1.Depth__c = 5.2;
		equ1.EquipmentType__c = 'Antenna';
        insert equ1;

        
        EntitlementCapturesWithRelated_Ctrl.fetchLeaseinfo(lstContract[0].ID);
        EntitlementCapturesWithRelated_Ctrl.getLeaseId(lstContract[0].ID);
        String e1 = '{"SiteID__c":"'+sites[0].Id+'","Agreement__c":"New Lease","Audit_Status__c":"Audit Submitted","Landlord__c":"deepika","LeaseId__c":"'+lstContract[0].ID+'","Antennas__c":2,"Coax__c":0,"TMA__c":0,"Hybrid__c":0,"RRU__c":0,"J_Box__c":0,"MWDish__c":0,"ODU__c":0,"Diplexer__c":0}';
        string e2= '[{"Count":"2","EquipmentId":"'+equ.Id+'","EquipmentName":"110010","EquipmentType":"Antenna","Length":81,"Width":7.7,"Depth":3.6,"Weight":29,"LTimesW":"623.70","TotalSurface":"1247.40","TotalWeight":"58.00","equipment":{"Id":"'+equ1.Id+'","Name":"a2c0U000001bsTp","EquipmentModel__c":"110010","EquipmentManufacturer__c":"CELLMAX","Weight__c":29,"Width__c":7.7,"Length__c":81,"Depth__c":3.6,"EquipmentType__c":"Antenna"},"Index":0}]';
        string e3 = '[{"RADCenter":"Primary RAN","RADSqft":"2","SelectedType":"RAD Center"},{"SelectedType":"Ground Space","GroundSpaceType":"Fuel Tank","GroundSpace":"2"},{"SelectedType":"Generator","generatorType":"Natural Gas","GeneratorTank":"Yes"}]';
        EntitlementCapturesWithRelated_Ctrl.SaveECnECLnRADGrnd(e1,e2,e3);
        EntitlementCapturesWithRelated_Ctrl.checkCreateAccess();
         EntitlementCapturesWithRelated_Ctrl.getEquipements('110045');
        
        //String objectName, String searchString ,String value
        CustomLookupController.fetchRecords('Equipment__c','2D4WC-2',null);
        CustomLookupController.fetchRecords(null,'2D4WC-2',null);
        test.stopTest();
    }
}
/*********************************************************************************************
@author       : IBM
@date         : 17.09.2020
@description  : This is the trigger handler to create Integration records. This is fired from Platform event LocationTriggerHandler
@Version      : 1.0         
**********************************************************************************************/
Public with sharing class LocationIntegrationLogHandler{
    
    /*********************************************************************************************
* @Description :Method to create Integration Log records if Ring Id is not found in Salesforce
**********************************************************************************************/
    
    public static void insertUpdateIntegrationLogRecords(List<Location_ID__c> locToBeInserted, Map<String,String> ringIdForReason){
   
        List<Database.UpsertResult> updateResults;
        List<Integration_Log__c> logLstToInsert = new List<Integration_Log__c>();
       
        if(locToBeInserted.size()>0){

            //Inserting Location coming from event
            updateResults=SI_UtilityClass.callDMLDatabaseOperations(locToBeInserted,'Upsert');
            
            //Loop to insert failure and Success records in Integration Log object
            for(Integer i=0;i<updateResults.size();i++){
                Integration_Log__c errorLog;
                if(updateResults.get(i).isSuccess()){
                    if(locToBeInserted.get(i).Ring_ID__c==NULL  && ringIdForReason.get(locToBeInserted.get(i).Location_ID__c)!=NULL ){
                        
                        errorLog = new Integration_Log__c();
                        //Update the status with Success
                        errorLog.Status__c = Label.SI_Success;
                        //updating response with 'Location updated successfully '
                        errorLog.Response_Message__c = Label.SI_LocUpdSuccess;
                        //Updating failure message with Ring Id doesn't exist in Salesforce
                        errorlog.Failure_Reason__c= ringIdForReason.get(locToBeInserted.get(i).Location_ID__c) + Label.SI_RingIdNotInSf;
                        //Updating APItype with DWH Location
                        errorLog.API_Type__c = Label.SI_DWHLocation;
                        errorLog.Location__c = locToBeInserted.get(i).Id;
                        logLstToInsert.add(errorLog);
                    }
                }
                else{
                    errorLog = new Integration_Log__c();
                    //Updating status with 'Failure'
                    errorLog.Status__c = Label.SI_Failure;
                    //Updating all the error's withcoma seperated in the Response message.
                    errorLog.Response_Message__c =String.join(updateResults.get(i).getErrors(), ','); 
                     //Updating APItype with DWH Location
                    errorLog.API_Type__c = Label.SI_DWHLocation;
                    errorLog.Location__c = locToBeInserted.get(i).Id;
                    logLstToInsert.add(errorLog);
                }
            }
        }
        //Inserting/Updating the list of Integration Log Object
        if(logLstToInsert != null && logLstToInsert.size() > 0){
            //Calling utility class for upsert operation
            SI_UtilityClass.callDMLDatabaseOperations(logLstToInsert,'Upsert');
        }
    }
}
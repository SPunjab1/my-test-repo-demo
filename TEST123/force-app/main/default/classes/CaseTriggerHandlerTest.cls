/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : CaseTriggerHandlerTest
*  @Author           : Archana
*  @Version History  : 1.0
*  @Creation         : 06.06.2019
*  @Description      : Test class for CaseTriggerHandler
**********************************************************************************************************************
****************************************************************************************************************/
@isTest(SeeAllData=false)
Public class CaseTriggerHandlerTest{
/***************************************************************************************************
* @Description  : Test Method to test checkTaskDependency & Add attachment Validation Scenario
***************************************************************************************************/

static  testmethod void runTestcheckTaskDependency() {
   
        DmlException expectedException;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        List<Case> WorkOrderTaskList= new List<Case>();
        List<Case> WorkOrderTaskListToUpdate= new List<Case>();
        Integer NumOfCases=50;
        String WorkflowTmpName1='AAV CIR Retest';
        caseList= TestDataFactory.createCase(WorkflowTmpName1,NumOfCases,recTypeId);
        
        Sobject obj = new Account(name = 'Test');
        TriggerDispatcher.skipObjectfromRecursiveCall(obj);
        
        Test.startTest();
        system.runas(TestDataFactory.createSourcingUser()){  
        try{
        Insert(caseList); 
        for(Case cs: caseList){
            cs.Status='Closed';
            cs.Worklog_Required__c=true;
            cs.Attachment_Required__c = true;    
            cs.No_Attachment_Reason__c=null;
            cs.Work_Log_Entry__c='Test comment';
            WorkOrderTaskListToUpdate.add(cs);
        }   
        Update(WorkOrderTaskListToUpdate);
        
        }catch(Exception ex){}  
       }   
        Test.stopTest();
         
}    
/***************************************************************************************************
* @Description  : Method to test closeTaskAutomatically for Task number 6,7,8,9 when 5th task is closed
***************************************************************************************************/
    static  testmethod void runTestcloseTaskAutomatically() {
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();   
        Id  recTypeIdTask = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> WorkOrderTaskList= new List<Case>();
        List<Case> WorkOrderTaskListToUpdate= new List<Case>();
        List<Case> WorkOrderTaskListUpdate= new List<Case>();
        String WorkflowTmpName ='OM CMG_Retail Broadband Dep';
        Integer NumOfCases=1;
        caseList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        Insert(caseList);
        Test.startTest();
        WorkOrderTaskList =[SELECT id, Status,ParentId from Case where  ParentId in :caseList AND Task_Number__c=5 ];
        for(Case c:WorkOrderTaskList ){
            c.Status='N/A';
            c.Work_Log_Entry__c='Test';
            WorkOrderTaskListToUpdate.add(c);
        }
        Update WorkOrderTaskListToUpdate;
        Test.stopTest();
    }       
/***************************************************************************************************
* @Description  : Test Method to test updateScheduleAndAssign 
***************************************************************************************************/        
    static  testmethod void runTestupdateScheduleAndAssign(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId(); 
        Id  recTypeIdTask = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> caseListToUpdate= new List<Case>();   
        Integer NumOfCases=2;
        String WorkflowTmpName='TIFS AAV CIR Retest';
        caseList = TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        Insert(caseList); 
        Test.startTest();   
        List<Case> taskList=[select id,Task_Number__c,Worklog_Required__c ,Assign_Schedule_Pier__C,No_Attachment_Reason__c from case where task_number__c in (1) AND ParentId IN: caseList LIMIT 50];
        for(Case cs: taskList){ 
            cs.status = 'Closed'; 
            cs.Next_PIER_Task__c=true;
            cs.Worklog_Required__c =false;
            cs.IsStopped=True;
			cs.No_Attachment_Reason__c = 'testData';	
            caseListToUpdate.add(cs);
        }    
        Update(caseListToUpdate);                
        Test.stopTest(); 
    }
/***************************************************************************************************
* @Description  : Test Method to test createIntegrationLogs  for Add adhoc task
***************************************************************************************************/
    static  testmethod void runTestcreateIntegrationLogs(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> WorkOrderTaskList= new List<Case>();
        List<Case> WorkOrderTaskListToUpdate= new List<Case>();
        Integer NumOfCases=50;
        String WorkflowTmpName1='TIFS AAV CIR Retest';
        caseList= TestDataFactory.createCase(WorkflowTmpName1,NumOfCases,recTypeId);
        Test.startTest();        
        Insert(caseList); 
        
        for(Case cs: caseList){
            cs.Status='Closed';
            cs.Worklog_Required__c=false;
            cs.Attachment_Required__c = false;    
            cs.Approval_Required__c=true;
            cs.Executable_Task_in_PIER__c=true;
            cs.All_Approve_Task_API_Processed__c=true;
            cs.Total_Approval_Required_Tasks__c=1;
            cs.Process_Update_Task_API__c=true;
            cs.Migrated_Case__c = true;  
            WorkOrderTaskListToUpdate.add(cs);
        }   
        Update(WorkOrderTaskListToUpdate);
        Test.stopTest();
    }   
    
/***************************************************************************************************
* @Description  : Test Method to test createIntegrationLogs  for Add adhoc task
***************************************************************************************************/
    static  testmethod void runTestcreateIntegrationLogs_ForAdhocTask(){
        Id  recTypeIdTask = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> caseListToUpdate= new List<Case>();
        Integer NumOfCases=200;
        String WorkflowTmpName='TIFS AAV CIR Retest';
        caseList= TestDataFactory.createCasesIntegrationLogs_ForAdhocTask(WorkflowTmpName,NumOfCases,recTypeIdTask);
        Test.startTest();
        insert(caseList);
        for(case c:caseList){
            c.status = 'New';
            c.Case_Reopen_Reason__c = 'Busy CFA';
            //c.Previous_Task_Closed__c  = true;

            caseListToUpdate.add(c);
        }
        update(caseListToUpdate); 
        Test.stopTest();
    }
    
/***************************************************************************************************
* @Description  : Test Method to test createIntegrationLogs to close the task in PIER
***************************************************************************************************/
    static  testmethod void runTestcreateIntegrationLogs_ForCloseTask(){
        Id  recTypeIdTask = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> caseListToUpdate= new List<Case>();
        Integer NumOfCases=50;
        String WorkflowTmpName='TIFS AAV CIR Retest';
        caseList= TestDataFactory.createCasesIntegrationLogs_ForCloseTask(WorkflowTmpName,NumOfCases,recTypeIdTask);
        Test.startTest();        
        insert(caseList);
        for(case c:caseList){
            c.status = 'Closed';
            caseListToUpdate.add(c);
        }
        update(caseListToUpdate);
        Test.stopTest();
    }    
    
/***************************************************************************************************
* @Description  : Test Method to test closeWorkOrderWhenLastTaskClosed 
***************************************************************************************************/        
     static  testmethod void runTestcloseWorkOrderWhenLastTaskClosed(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();
        List<Case> taskList= new List<Case>();
        List<Case> taskListToUpdate= new List<Case>(); 
        String WorkflowTmpName='TIFS AAV CIR Retest';
        Integer NumOfCases=1;
        caseList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);       
        Insert(caseList); 
        Test.startTest();        

            //caseList[0].Stop_Work_Order_Task_SLA__c=true;
            //update caseList
           
            taskList=[SELECT  id,status FROM Case Where ParentId = :caseList[0].id AND Task_Number__c!=1];
            delete taskList;
            for(case c: [select id, status from case where ParentId =:caseList[0].id]){
                c.Last_Task__c=false;
                c.Worklog_Required__c=false;
                c.Send_In_Progress_Task_To_Pier__c=false;
                c.Process_Update_Task_API__c=false;
                c.Previous_Task_Closed__c= true;
                c.No_Attachment_Reason__c='no attach';
                c.Case_Reopen_Reason__c='test reason';
                c.status='Closed';
                taskListToUpdate.add(c);
            }       
            //Update(taskListToUpdate);   
            //system.assertequals(taskListToUpdate[0].status,'Closed');
                                                                      
        Test.stopTest();       
    }               
/***************************************************************************************************
* @Description  : Test Method to test stopSLAFieldUpdateCheck method 
***************************************************************************************************/        
     static  testmethod void runTeststopSLAFieldUpdateCheck(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();       
        List<Case> caseListToUpdate= new List<Case>(); 
        List<Case> caseListToUpdatefalse= new List<Case>(); 
        String WorkflowTmpName='TIFS AAV CIR Retest';
        
        Integer NumOfCases=2;
        caseList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);             
		Insert(caseList); 
            Test.startTest(); 
            List<Case> csNewList=[select id from case where Stop_Work_Order_Task_SLA__c=false];
            for( case cs: csNewList){          
                cs.Stop_Work_Order_Task_SLA__c=true;
                caseListToUpdate.add(cs);
            }
            update caseListToUpdate;  
                                 
        Test.stopTest();       
    }             
/***************************************************************************************************
* @Description  : Test Method to test stopSLAFieldUpdateCheck method 
***************************************************************************************************/        
     static  testmethod void runTeststopSLAFieldUpdateCheckFalse(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> caseList= new List<Case>();       
        List<Case> caseListToInsert= new List<Case>(); 
        List<Case> caseListToUpdatefalse= new List<Case>(); 
        String WorkflowTmpName='TIFS AAV CIR Retest';
        
        Integer NumOfCases=2;
        caseList= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        for( case cs: caseList){
            cs.Stop_Work_Order_Task_SLA__c=true;
            caseListToInsert.add(cs);
            }
            Insert caseListToInsert;  
        Test.startTest();        
            List<Case> csNewList=[select id from case];
            for(case csnew : csNewList){
            csnew.Stop_Work_Order_Task_SLA__c=false;
            caseListToUpdatefalse.add(csnew);
            }
            
            update caseListToUpdatefalse;                     
        Test.stopTest();       
    }               
    
 
    
            
}
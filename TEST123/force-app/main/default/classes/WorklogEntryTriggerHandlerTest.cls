/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : WorklogEntryTriggerHandlerTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 27.08.2019
*  @Description      : Test class for WorklogEntryTriggerHandler
**********************************************************************************************************************
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class WorklogEntryTriggerHandlerTest {
    /***************************************************************************************************
* @Description  : Test Method to test createIntegrationLogs
***************************************************************************************************/
    static  testmethod void runCreateIntegrationLogs() {
        
        Id recTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        Worklog_Entry__c workLogEntryToInsert = new Worklog_Entry__c();
        List<case>caseList=TestDataFactory.createCase('OM AAV_Small Cell CIR Change',1,recTypeId);
        caseList[0].Executable_Task_in_PIER__c = true;
        insert caseList;
        workLogEntryToInsert.Comment__c='<img alt="User-added image" src="https://teemo-dev--ibmdev1--c.documentforce.com/servlet/rtaImage?eid=a1P560000022z3Z&amp;feoid=00N56000003w9sM&amp;refid=0EM560000007a38"></img>';
        workLogEntryToInsert.Task__c=caseList[0].id;
        Test.startTest();
        insert workLogEntryToInsert;
        Test.stopTest();
    }
    static testmethod void testWithoutImageScenario()
    {
        Id recTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        Worklog_Entry__c workLogEntryToInsert = new Worklog_Entry__c();
        List<case>caseList=TestDataFactory.createCase('OM AAV_Small Cell CIR Change',1,recTypeId);
        caseList[0].Migrated_Case__c = true;
        insert caseList;
        workLogEntryToInsert.Comment__c='test comment';
        workLogEntryToInsert.Task__c=caseList[0].id;
        Test.startTest();
        insert workLogEntryToInsert;
        Test.stopTest() ;
    }
}
/******************************************************************************
@author      - IBM
@date        - 22.10.2020
@description - Support class to update location data in system mode from opportunity trigger
@version     - 1.0
*******************************************************************************/
public without sharing class SI_OpportunityTriggerSupportClass {
    
    //0: Contracted, 1: LOI - Contract Ready, 2: LOI - Not Eligible
    static List<String> listOfCMGWorkingStatus = System.Label.SI_LocationCMGWorkingStatus.split(';');
    //0: Eligible, 1: Ineligible
    static List<String> listOfEligibiltyStatus = System.Label.SI_LocationEligibilityStatus.split(';');
    //0: Not Available, 1: Available
    static List<String> listOfAAVStatus = System.Label.SI_AAV_Status.split(';');
    
    /**************************************************************************
    * @description To update location fields when opp stage is Closed - Contract Executed
    * @param mapOfWonOpportunityIds - map Of closed Contract Executed opportunities
    * @param ringLocationMap - ring id to location record map
    * @param conSiteList - list of contract sites for won opportunities
    * @return mapOfIdToLocationForUpdate - Recturn map of Location Ids
    **************************************************************************/
    public static Map<Id,Location_ID__c> updateLocationForClosedExecuted(Map<Id,Opportunity> mapOfWonOpportunityIds,
                                                                         Map<String, List<Location_Id__c>> ringLocationMap,
                                                                         List<Contract_Site__c> conSiteList) { 
        Map<Id,Location_ID__c> mapOfIdToLocationForUpdate = new Map<Id,Location_ID__c>();
        List<Location_Id__c> locDataList1 = new List<Location_Id__c>();
        
        for(Contract_Site__c conSite : conSiteList) {
            if(ringLocationMap.containsKey(conSite.Location_ID__r.Ring_ID__c)) {
                locDataList1.addAll(ringLocationMap.get(conSite.Location_ID__r.Ring_ID__c));
            }
            Location_ID__c objLocation = new Location_ID__c();
            objLocation.Id = conSite.Location_ID__c;
            // Deselect type if blank
            if(String.isBlank(conSite.Deselect_Type__c)) {
                objLocation = updateLocationAAVDetails(mapOfWonOpportunityIds, conSite, objLocation);
                //If opportunity document type not equal to LOI ,else opportunity document type  equal to LOI
                if(conSite.Opportunity__r.Document_Type__c != System.Label.SI_OppDocumentType) {
                    objLocation.AAV_Status__c = System.Label.SI_Location_AVV_Status;
                    objLocation.CMG_Working_Status__c = listOfCMGWorkingStatus[0];
                }
                //If opportunity document type is LOI
                else if(conSite.Opportunity__r.Document_Type__c == System.Label.SI_OppDocumentType) {
                    objLocation.AAV_Status__c = System.Label.SI_Location_AVV_Status;
                    //If IsRingRecord is TRUE on actual location
                    if(conSite.Location_ID__r.IsRingRecord__c == true) {
                        objLocation.CMG_Working_Status__c = listOfCMGWorkingStatus[0];
                    }
                    //If IsRingRecord is FALSE on actual location
                    else if(conSite.Location_ID__r.IsRingRecord__c == false) {
                        //If contract site location eligibility status,else Ineligible
                        if(conSite.Location_ID__r.Eligibility_Status__c == listOfEligibiltyStatus[0]) {
                            objLocation.CMG_Working_Status__c = listOfCMGWorkingStatus[1];
                        } else if(conSite.Location_ID__r.Eligibility_Status__c == listOfEligibiltyStatus[1]) {
                            objLocation.CMG_Working_Status__c = listOfCMGWorkingStatus[2];
                        } else {
                            objLocation.CMG_Working_Status__c = listOfCMGWorkingStatus[2];
                        }
                    }
                }
            }// Deselect Type is 'Macro Deselect' (path - Contract Site > Intake Site field Deselect_Type__c)
            else if(conSite.Deselect_Type__c == System.Label.SI_Deselect_Type) {
                if(String.isNotBlank(conSite.Location_ID__r.Primary_Vendor_Name__c) &&
                   conSite.Location_ID__r.Primary_Vendor_Name__c.equalsIgnoreCase(conSite.Opportunity__r.Account.TCM_vendor_Name__c)) {
                    objLocation.AAV_Primary_Contract_ID__c = '';
                    objLocation.Primary_Vendor_Name__c = '';
                    objLocation.Primary_Vendor_Id__c = '';
                } else if(String.isNotBlank(conSite.Location_ID__r.Secondary_Vendor_Name__c) &&
                          conSite.Location_ID__r.Secondary_Vendor_Name__c.equalsIgnoreCase(conSite.Opportunity__r.Account.TCM_vendor_Name__c)) {
                    objLocation.AAV_Secondary_Contract_ID__c = '';
                    objLocation.Secondary_Vendor_Name__c = '';
                    objLocation.Secondary_Vendor_Id__c = '';
                } else {
                    mapOfWonOpportunityIds.get(conSite.Opportunity__c).addError(Label.SI_LocationVendorUpdateErrorMacroDeselect);
                }
                //if Deslect Results is 'Site becomes Unavail'
                //(path - Contract Site > Intake Site > Case field Deselect_Reason__c)
                if(conSite.Deselect_Result__c == Label.SI_Deselect_Results.split(';')[0]) {
                    // update AAV Status to 'Not Available'
                    objLocation.AAV_Status__c = listOfAAVStatus[0];
                    if(!conSite.Location_ID__r.Ethernet_Installed__c){
                        // Update CMG Working status to 'BAU, Not Available'
                        objLocation.CMG_Working_Status__c = Label.SI_CMGWorkingStatus.split(';')[0];
                    } else {
                        // Update CMG Working status to 'Delivered, Not Available'
                        objLocation.CMG_Working_Status__c = Label.SI_CMGWorkingStatus.split(';')[1];
                    }
                } // if Deslect Results is 'Site becomes Avail' (path - Contract Site > Intake Site > Case field Deselect_Reason__c)
                else if(conSite.Deselect_Result__c == Label.SI_Deselect_Results.split(';')[1]) {
                    // update  AAV Status to 'Available' and Blank CMG Working Status
                    objLocation.AAV_Status__c = listOfAAVStatus[1];
                    objLocation.CMG_Working_Status__c = '';
                }
            } else if(conSite.Deselect_Type__c != System.Label.SI_Deselect_Type) {
                //if Contract Site deselect type is not macro deselect
                objLocation = updateLocationAAVDetails(mapOfWonOpportunityIds, conSite, objLocation);
            }
            mapOfIdToLocationForUpdate.put(objLocation.id,objLocation);
        }
        
        if (!locDataList1.isEmpty()) {
            for(Location_ID__c locRecord : locDataList1) {
                //checking eligibility status of candidate records & updating CMG Status
                if(locRecord.Eligibility_Status__c == listOfEligibiltyStatus[1]) {
                    locRecord.CMG_Working_Status__c = listOfCMGWorkingStatus[2];
                }
                else if(locRecord.Eligibility_Status__c == listOfEligibiltyStatus[0]) {
                    locRecord.CMG_Working_Status__c = listOfCMGWorkingStatus[1];
                }
                mapOfIdToLocationForUpdate.put(locRecord.id,locRecord);
            }
        }
        return mapOfIdToLocationForUpdate;
    }
    
    /**************************************************************************
    * @description To update location fields when opp stage is Closed - Contract Executed
    * @param mapOfWonOpportunityIds - map Of closed Contract Executed opportunities
    * @param objContractSite - contract site record
    * @param objLocation - Location records to process
    * @return objLocation - Return location
    **************************************************************************/
    private static Location_ID__c updateLocationAAVDetails(Map<Id,Opportunity> mapOfWonOpportunityIds,
                                                           Contract_Site__c objContractSite,
                                                           Location_Id__c objLocation) {
        if(objContractSite.Opportunity__r.Account.Satellite_Vendor__c == true) {
            objLocation.Secondary_Vendor_Id__c = objContractSite.Ordering_Guide_ID__c;
            objLocation.Secondary_Vendor_Name__c = objContractSite.Opportunity__r.Account.TCM_vendor_Name__c;
            objLocation.AAV_Secondary_Contract_ID__c = objContractSite.Ordering_Guide_ID__c;
        } else {
            if(String.isBlank(objContractSite.Location_ID__r.AAV_Primary_Contract_ID__c)
               || (String.isNotBlank(objContractSite.Location_ID__r.Primary_Vendor_Name__c) &&
                   objContractSite.Location_ID__r.Primary_Vendor_Name__c.equalsIgnoreCase(objContractSite.Opportunity__r.Account.TCM_vendor_Name__c))) {
                objLocation.Primary_Vendor_Id__c = objContractSite.Ordering_Guide_ID__c;
                objLocation.Primary_Vendor_Name__c = objContractSite.Opportunity__r.Account.TCM_vendor_Name__c;
                objLocation.AAV_Primary_Contract_ID__c = objContractSite.Ordering_Guide_ID__c;
            } else if(String.isBlank(objContractSite.Location_ID__r.AAV_Secondary_Contract_ID__c)
                      || (String.isNotBlank(objContractSite.Location_ID__r.Secondary_Vendor_Name__c) &&
                          objContractSite.Location_ID__r.Secondary_Vendor_Name__c.equalsIgnoreCase(objContractSite.Opportunity__r.Account.TCM_vendor_Name__c))){
                objLocation.Secondary_Vendor_Id__c = objContractSite.Ordering_Guide_ID__c;
                objLocation.Secondary_Vendor_Name__c = objContractSite.Opportunity__r.Account.TCM_vendor_Name__c;
                objLocation.AAV_Secondary_Contract_ID__c = objContractSite.Ordering_Guide_ID__c;
            } else if(String.isNotBlank(objContractSite.Location_ID__r.Primary_Vendor_Name__c)
                      && objContractSite.Location_ID__r.Primary_Vendor_Name__c.containsIgnoreCase(Label.SI_PrimaryVendorName)) {
                objLocation.Primary_Vendor_Id__c = objContractSite.Ordering_Guide_ID__c;
                objLocation.Primary_Vendor_Name__c = objContractSite.Opportunity__r.Account.TCM_vendor_Name__c;
                objLocation.AAV_Primary_Contract_ID__c = objContractSite.Ordering_Guide_ID__c;
            } else {
                mapOfWonOpportunityIds.get(objContractSite.Opportunity__c).addError(Label.SI_LocationVendorUpdateError);
            }
        }
        return objLocation;
    }
    
     /**************************************************************************
    * @description To update location fields when opp stage is Closed - Contract Executed
    * @param mapOfLocationRecordsToUpdate - map Of closed Contract Executed opportunities
    **************************************************************************/
    public static void updateLocations(Map<Id,Location_ID__c> mapOfLocationRecordsToUpdate) {
        try {
            update mapOfLocationRecordsToUpdate.values();
        } catch(DMLException ex) {
            System.debug(LoggingLevel.ERROR,'Exception in Custom Label Values'+ex.getMessage());
        }
    }
}
/********************************************************************************
* @author         IBM
* @date           06/07/2020
* @description    To reprocess API call using Reprocess Batch on Integration Log
* @group          Retry machanism to reprocess the API callout        
*
********************************************************************************/

public class AutomaticReprocessAPICallout implements Database.Batchable<sObject>, Database.Stateful, Schedulable, Database.AllowsCallouts {
    //Get the custom setting values which store the constant values
    Public static Map<string,ATOMSNeuStarUtilitySettings__c> mapConstants = ATOMSNeuStarUtilitySettings__c.getAll();
    //Declare public variables
    public Map<Id,List<ASR__c>> caseIdASRListMap = new Map<Id,List<ASR__c>>();
    public Map<Id,List<Circuit__c>> caseIdCktListMap = new Map<Id,List<Circuit__c>>();
    
    //Query the Integration Log object to get all the Failed Callouts for whicg Reprocess = True
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id,Type_of_Order__c,API_Type__c,Status__c, Request__c,TP_Response_Type__c, Failure_Reason__c, Response_Message__c, Error_Log__c, Reprocess__c,Error_Type__c,Pending_ATOMS_Update__c,Pending_Salesforce_Update__c,PON__c,Purchase_Order__c,ASR__r.case__c, Purchase_Order__r.Work_Order_Task__c, Circuit__c,ASR__c,Work_Order__c,Circuit__r.Case__c, Task__c FROM Integration_Log__c WHERE  Status__c = \'Failure\' AND API_Type__c IN (\'Add Circuit\',\'Add Circuit : Get ASiteID\',\'Add Circuit : Get ZSiteID\',\'Add Circuit : OG GetDetails\',\'Add Circuit : OG GetSiteAssociation_A\',\'Add Circuit : OG GetSiteAssociation_Z\',\'CreateASR\',\'UpgradeASR\',\'DisconnectASR\',\'Create Purchase Order\',\'Update ATOMS in SFDC\',\'Update Circuit in ATOMS\',\'Update ASR in ATOMS\',\'Get Circuit\',\'Add NNI\',\'Get NNI\',\'Update NNI in ATOMS\',\'Add NNI: Get ASiteID\',\'Add NNI: Get ZSiteID\',\'Add NNI: Get VendorNameID\')';
        return Database.getQueryLocator(query);
    }
    
    public void execute(SchedulableContext sc) {
        if(!Test.isRunningTest())    
            Database.executeBatch(new AutomaticReprocessAPICallout(), integer.valueOf(Label.ReprocessBatchSize));
    }
    
    public void execute(Database.BatchableContext bc, List<Integration_Log__c> scope) {
        //Declare variables to store map of CaseId to List Of Integration Log
        Map<Id, List<Integration_Log__c>> caseASRSetCreate = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> caseASRSetUpgrade = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> caseASRSetDisconnect = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> caseCircuitIds = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> casePOSet = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> caseUpdateCircuitSet = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> caseGetCircuitSet = new Map<Id, List<Integration_Log__c>>();
        Map<Id, List<Integration_Log__c>> caseASRSetUpdate = new Map<Id, List<Integration_Log__c>>();
        Map<Id,List<Circuit__c>> circuitMapCreation = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Circuit__c>> circuitMapGet = new Map<Id,List<Circuit__c>>();
        Map<Id,List<Circuit__c>> circuitMapUpdate = new Map<Id,List<Circuit__c>>();
        Map<Id,List<ASR__c>> asrAddMap = new Map<Id,List<ASR__c>>();
        Map<Id,List<ASR__c>> asrUpgradeMap = new Map<Id,List<ASR__c>>();
        Map<Id,List<ASR__c>> asrDisconnectMap = new Map<Id,List<ASR__c>>();
        Map<Id,List<ASR__c>> asrUpdateMap = new Map<Id,List<ASR__c>>();
        
        //Declare variables to Set of Case Ids
        Set<Id> createCircuitIDSet = new Set<Id>();
        Set<Id> createASRIDSet = new Set<Id>();
        Set<Id> upgradeASRIDSet = new Set<Id>();
        Set<Id> disconnectASRIDSet = new Set<Id>();
        Set<Id> updateCircuitIDSet = new Set<Id>();
        Set<Id> getCircuitIDSet = new Set<Id>();
        Set<Id> caseSetAll = new Set<Id>();
        Set<Id> updateASRIDSet = new Set<Id>();
        List<Integration_Log__c> caseUpdateToAtomsLst = new List <Integration_Log__c>();
        
        //Declare variables to List of Cases
        List<Case> caseList = new List <Case>();
        List<Case> caseASRListCreate = new List <Case>();
        List<Case> caseASRListUpgrade = new List <Case>();
        List<Case> caseASRListUpdate = new List <Case>();
        List<Case> caseASRListDisconnect = new List <Case>();
        List<Case> caseCircuitList= new List <Case>();
        List<Case> casePOList = new List <Case>();
        List<Case> caseUpdateCircuitLst = new List <Case>();
        List<Case> caseGetCircuitLst = new List <Case>();
        UpdateNeuStarDetailsToATOMSUtility.responseUpdWrapper respWrapper = new UpdateNeuStarDetailsToATOMSUtility.responseUpdWrapper();
        
        //Iterate through all Integration Log to seggregate the IL based on API Type
        for(Integration_Log__c intLog : Scope){
            //Store map of CaseId to List of Int.Log for Add Circuit
            if(intLog.Circuit__c!=null && (intLog.API_Type__c != mapConstants.get('Update Circuit').Value__c && intLog.API_Type__c != mapConstants.get('Get Circuit').Value__c && intLog.API_Type__c != mapConstants.get('Get-NNI').Value__c)){
               if(caseCircuitIds.ContainsKey(intLog.Circuit__r.Case__c)){
                   caseCircuitIds.get(intLog.Circuit__r.Case__c).add(intLog);
               }else{
                   caseCircuitIds.put(intLog.Circuit__r.Case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.Circuit__r.Case__c);
               createCircuitIDSet.add(intLog.Circuit__c);

           }
           //Store map of CaseId to List of Int.Log for Create ASR
           if(intLog.ASR__c!=null && intLog.API_Type__c == mapConstants.get('CreateASR').Value__c){
               if(caseASRSetCreate.ContainsKey(intLog.ASR__r.case__c)){
                   caseASRSetCreate.get(intLog.ASR__r.case__c).add(intLog);
               }else{
                   caseASRSetCreate.put(intLog.ASR__r.case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.ASR__r.case__c);
               createASRIDSet.add(intLog.ASR__c);
           }
           
           //Store map of CaseId to List of Int.Log for Upgrade ASR
           if(intLog.ASR__c!=null && intLog.API_Type__c == mapConstants.get('UpgradeASR').Value__c){
               if(caseASRSetUpgrade.ContainsKey(intLog.ASR__r.case__c)){
                   caseASRSetUpgrade.get(intLog.ASR__r.case__c).add(intLog);
               }else{
                   caseASRSetUpgrade.put(intLog.ASR__r.case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.ASR__r.case__c);
               upgradeASRIDSet.add(intLog.ASR__c);
           }
           
           //Store map of CaseId to List of Int.Log for Update ASR
           if(intLog.ASR__c!=null && intLog.API_Type__c == mapConstants.get('UpdateASRinATOMS').Value__c){
               if(caseASRSetUpdate.ContainsKey(intLog.ASR__r.case__c)){
                   caseASRSetUpdate.get(intLog.ASR__r.case__c).add(intLog);
               }else{
                   caseASRSetUpdate.put(intLog.ASR__r.case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.ASR__r.case__c);
               updateASRIDSet.add(intLog.ASR__c);
           }
           //Store map of CaseId to List of Int.Log for Disconnect ASR
           if(intLog.ASR__c!=null && intLog.API_Type__c == mapConstants.get('DisconnectASR').Value__c){
               if(caseASRSetDisconnect.ContainsKey(intLog.ASR__r.case__c)){
                   caseASRSetDisconnect.get(intLog.ASR__r.case__c).add(intLog);
               }else{
                   caseASRSetDisconnect.put(intLog.ASR__r.case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.ASR__r.case__c);
               disconnectASRIDSet.add(intLog.ASR__c);
           }
           //Store map of CaseId to List of Int.Log for Create Purchase Order
           if(intLog.Purchase_Order__c!=null && intLog.API_Type__c == mapConstants.get('Create Purchase Order').Value__c){
               if(casePOSet.ContainsKey(intLog.Purchase_Order__r.Work_Order_Task__c)){
                   casePOSet.get(intLog.Purchase_Order__r.Work_Order_Task__c).add(intLog);
               }else{
                   casePOSet.put(intLog.Purchase_Order__r.Work_Order_Task__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.Purchase_Order__r.Work_Order_Task__c);
           }
           //Store List of Work Order Task for Update to Atoms
           if(intLog.API_Type__c == mapConstants.get('APITypeUpdateATOMSNeuStar').Value__c){
               caseUpdateToAtomsLst.add(intLog);
               caseSetAll.add(intLog.Purchase_Order__r.Work_Order_Task__c);
           }
           //Store map of Work Order Task to List of Int.Log for Update circuit
           if(intLog.Circuit__c!=null && (intLog.API_Type__c == mapConstants.get('Update Circuit').Value__c || intLog.API_Type__c == mapConstants.get('Update-NNI').Value__c)){
               if(caseUpdateCircuitSet.ContainsKey(intLog.Circuit__r.Case__c)){
                   caseUpdateCircuitSet.get(intLog.Circuit__r.Case__c).add(intLog);
               }else{
                   caseUpdateCircuitSet.put(intLog.Circuit__r.Case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.Circuit__r.Case__c);
               updateCircuitIDSet.add(intLog.Circuit__c);
           }
           //Store map of Work Order Task to List of Int.Log for Get circuit
           if(intLog.Circuit__c!=null && (intLog.API_Type__c == mapConstants.get('Get Circuit').Value__c || intLog.API_Type__c == mapConstants.get('Get-NNI').Value__c)){
               if(caseUpdateCircuitSet.ContainsKey(intLog.Circuit__r.Case__c)){
                   caseGetCircuitSet.get(intLog.Circuit__r.Case__c).add(intLog);
               }else{
                   caseGetCircuitSet.put(intLog.Circuit__r.Case__c, new List<Integration_Log__c>{ intLog });
               }
               caseSetAll.add(intLog.Circuit__r.Case__c);
               getCircuitIDSet.add(intLog.Circuit__c);
           }
        }
        
        //Query WOT and its related objects i.e PO, Circuit, ASR under the WOT present in the set caseSetAll
          if(caseSetAll.size() > 0){    
            caseList = [SELECT Id,OM_Integration_Status__c,ContactId,OwnerId, 
                        (Select Id, Name, BAN__c, A_Site_ID__c, A_Site_ID__r.Name, A_Site_Primary_Key__c, 
                         Vendor_ID_Primary_Key__c, User_NNI_ID__c, Comments__c, ESP__c, NNI_ID_Primary_Key__c, 
                         Billable_NNI__c,Bandwidth__c, Billing_Circuit_Id__c, Ordering_Guide_ID__c, Case__r.OwnerId, 
                         Circuit_Type__c, Category__c,Is_Meet_Point__c,LOA_Provided__c,Purchase_Order_Id__c,
                         User_Ciruit_ID__c,Vendor_Name__c,Z_Site_ID__c, Z_Site_ID__r.Name, Z_Site_Primary_key__c, 
                         Vendor_Name_Formula__c,Controlling_Site__c, Required_Fields_For_Add_Circuit_Present__c,
                         Circuit_ID_Primary_Key__c, RecordType.Name,Upload_Speed__c, NNI_Status__c, User_Site_Id__c, Site_Market__c,Site_Region__c, Site_Type__c from Circuits1__r),
                        (SELECT Number_of_ASRs__c,Number_of_Successful_ASR__c,API_Status__c,Work_Order_Task__c,
                         Work_Order__c,DDD__c,PON__c,Product_Name__c,Trading_Partner__c,Activity__c from 
                         Purchase_Orders__r), (SELECT Id, Case__c,
                         PON__c,Order_Number__c,VLAN__C, VLAN_2__c, Circuit_Id__r.Purchase_Order_Id__c,API_Status__c,Order_Destination__c,Case__r.parent.CaseNumber,SubService_Type__c,Case__r.parent.PIER_Work_Order_ID__c,
                                    ATOMs_ASR_Id__c,Forecast_Version__c,Is_Atoms_Creation_Failed__c,Ordered_Date__c,Service_Type__c,Ethernet_Technology_Type__c,AAV_Type__c,CIR__c,Test_CIR__c,Circuit_Id__c,Delivered_Date__c,Circuit_Id__r.Circuit_ID_Primary_Key__c,Circuit_Id__r.API_Status__c,deliver__c, ASR_Type__c, 
                                    Circuit_Id__r.NNI_ID_Primary_Key__c, Circuit_Id__r.RecordType.Name, TPE_Request_Number__c, Desired_Due_Date__c, Target_Download_Speed__c, Is_Supplement__c, Variable_Term_Agreement__c,
                                     Project__c,Auto_Renew__c,Auto_Reterm_Period__c, ASR_Number__c,SUP_Date__c, RPON__c, Accepted_Date__c, Date_Confirmed__c,Scheduled_MRC__c, FOC_Date__c, Cancelled_Date__c,SUP__c,Order_Confirmed__c,Is_Delivered__c,Is_Cancelled__c,Previous_Version__c,Version_Number__c,Date_Received__c,Purchase_Ord_PON__c, Change_Type__c, Change_Value__c from ASRs__r) 
                                     from Case WHERE Id IN: caseSetAll AND RecordType.DeveloperName =: mapConstants.get('Work_Order_Task').Value__c];
            //system.debug('caseList==========>'+caseList);
            for(Case caseObj : caseList){
                if(caseASRSetCreate.containsKey(caseObj.Id)){
                    caseASRListCreate.add(caseObj);
                }
                if(caseASRSetUpgrade.containsKey(caseObj.Id)){
                    caseASRListUpgrade.add(caseObj);
                }
                if(caseASRSetUpdate.containsKey(caseObj.Id)){
                    caseASRListUpdate.add(caseObj);
                }
                if(caseASRSetDisconnect.containsKey(caseObj.Id)){
                    caseASRListDisconnect.add(caseObj);
                }
                if(caseCircuitIds.containsKey(caseObj.Id)){
                    caseCircuitList.add(caseObj);
                }
                if(casePOSet.containsKey(caseObj.Id)){
                    casePOList.add(caseObj);
                }
                if(caseUpdateCircuitSet.containsKey(caseObj.Id)){
                    caseUpdateCircuitLst.add(caseObj);
                }
                if(caseGetCircuitSet.containsKey(caseObj.Id)){
                    caseGetCircuitLst.add(caseObj);
                }
                
                
            }
         }
         
         //Create map of case to list Circuit which will be sent to utiliy class for callout for all the API callouts
        for(Case caseTemp:caseCircuitList){
            for(Circuit__c circuitTemp : caseTemp.Circuits1__r){
                if(createCircuitIDSet.contains(circuitTemp.Id)){
                    if(!circuitMapCreation.containsKey(caseTemp.Id))
                        circuitMapCreation.put(caseTemp.Id,new List<Circuit__c>{circuitTemp});
                    else
                        circuitMapCreation.get(caseTemp.Id).add(circuitTemp);
                }
            }
        }
        
        for(Case caseTemp:caseGetCircuitLst){
            for(Circuit__c circuitTemp : caseTemp.Circuits1__r){
                if(getCircuitIDSet.contains(circuitTemp.Id)){
                    if(!circuitMapGet.containsKey(caseTemp.Id))
                        circuitMapGet.put(caseTemp.Id,new List<Circuit__c>{circuitTemp});
                    else
                        circuitMapGet.get(caseTemp.Id).add(circuitTemp);
                    
                }
            }
        }
        for(Case caseTemp : caseUpdateCircuitLst){
            for(Circuit__c circuitTemp : caseTemp.Circuits1__r){
                if(updateCircuitIDSet.contains(circuitTemp.Id)){
                    if(!circuitMapUpdate.containsKey(caseTemp.Id))
                        circuitMapUpdate.put(caseTemp.Id,new List<Circuit__c>{circuitTemp});
                    else
                        circuitMapUpdate.get(caseTemp.Id).add(circuitTemp);
                    
                }
            }
        }
        for(Case caseTemp : caseASRListCreate){
            for(ASR__c asrObj : caseTemp.ASRs__r){
                if(createASRIDSet.contains(asrObj.Id)){
                    if(!asrAddMap.containsKey(caseTemp.Id))
                        asrAddMap.put(caseTemp.Id,new List<ASR__c>{asrObj});
                    else
                        asrAddMap.get(caseTemp.Id).add(asrObj);
                    
                }
            }
        }
        for(Case caseTemp : caseASRListUpgrade){
            for(ASR__c asrObj : caseTemp.ASRs__r){
                if(upgradeASRIDSet.contains(asrObj.Id)){
                    if(!asrUpgradeMap.containsKey(caseTemp.Id))
                        asrUpgradeMap.put(caseTemp.Id,new List<ASR__c>{asrObj});
                    else
                        asrUpgradeMap.get(caseTemp.Id).add(asrObj);
                }
            }
        }
        for(Case caseTemp : caseASRListUpdate){
            for(ASR__c asrObj : caseTemp.ASRs__r){
                if(updateASRIDSet.contains(asrObj.Id)){
                    if(!asrUpdateMap.containsKey(caseTemp.Id))
                        asrUpdateMap.put(caseTemp.Id,new List<ASR__c>{asrObj});
                    else
                        asrUpdateMap.get(caseTemp.Id).add(asrObj);
                    
                }
            }
        }
        for(Case caseTemp : caseASRListDisconnect){
            for(ASR__c asrObj : caseTemp.ASRs__r){
                if(disconnectASRIDSet.contains(asrObj.Id)){
                    if(!asrDisconnectMap.containsKey(caseTemp.Id))
                        asrDisconnectMap.put(caseTemp.Id,new List<ASR__c>{asrObj});
                    else
                        asrDisconnectMap.get(caseTemp.Id).add(asrObj);
                        
                 }
            }
        }
        
        // Retry Machanism for Add Circuit callout
        //system.debug('circuitMapCreation====>'+circuitMapCreation);
        AtomsIntegrationAPI.returnParamsToBatchAfterCallout dmlActionsWrapperCircuit;
        AtomsIntegrationAPI.returnParamsToBatchAfterCallout dmlActionsWrapperCirUpdate;
        AtomsIntegrationAPI.returnParamsToBatchAfterCallout dmlActionsWrapperCircGet;
        NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout dmlActionsWrapperPO ;
        SFDCtoAtomsAPI.returnParamsToBatchAfterCallout returnDMLActionsWrapper = new SFDCtoAtomsAPI.returnParamsToBatchAfterCallout();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRUpgradeDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRCreateDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRDisconnectDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        List<SFDCtoAtomsAPI.returnParamsToBatchAfterCallout> listASRUpdateDMLWrapper = new List <SFDCtoAtomsAPI.returnParamsToBatchAfterCallout>();
        
        if(circuitMapCreation!= NULL && !circuitMapCreation.IsEmpty()){
            dmlActionsWrapperCircuit = AtomsIntegrationAPI.sendCreateCircuitReqToAtoms(circuitMapCreation,mapConstants.get('Add Circuit').Value__c, caseCircuitIds);
        }
        
        // Retry Machanism for CreateASR callout
        //system.debug('asrAddMap====>'+asrAddMap);
        if(caseASRListCreate !=null && caseASRListCreate.Size()>0){
           returnDMLActionsWrapper = AtomsIntegrationAPI.sendASRRequestToAtoms(asrAddMap,mapConstants.get('CreateASR').Value__c,caseASRSetCreate);
        }
        
        if(returnDMLActionsWrapper!=null){
            listASRCreateDMLWrapper.add(returnDMLActionsWrapper);      
        }
         // Retry Machanism for UpgradeASR callout
         //system.debug('asrUpgradeMap====>'+asrUpgradeMap);
        if(caseASRListUpgrade !=null && caseASRListUpgrade.Size()>0){
           returnDMLActionsWrapper = AtomsIntegrationAPI.sendASRRequestToAtoms(asrUpgradeMap,mapConstants.get('UpgradeASR').Value__c, caseASRSetUpgrade);
        }
        
        if(returnDMLActionsWrapper!=null){
            listASRUpgradeDMLWrapper.add(returnDMLActionsWrapper);     
        }
        
         // Retry Machanism for DisconnectASR callout
         //system.debug('asrDisconnectMap====>'+asrDisconnectMap);
        if(caseASRListDisconnect !=null && caseASRListDisconnect.Size()>0){
          returnDMLActionsWrapper = AtomsIntegrationAPI.sendASRRequestToAtoms(asrDisconnectMap,mapConstants.get('DisconnectASR').Value__c, caseASRSetDisconnect);
        }
        
         if(returnDMLActionsWrapper!=null){
            listASRDisconnectDMLWrapper.add(returnDMLActionsWrapper);      
        }
        
        //system.debug('asrUpdateMap====>'+caseASRSetUpdate);
        if(caseASRListUpdate !=null && caseASRListUpdate.Size()>0){
           returnDMLActionsWrapper = SFDCtoAtomsAPI.updateASR2AtomsUsingLogsORAsr(Null, caseASRSetUpdate);
        }
        
        if(returnDMLActionsWrapper!=null){
            listASRUpdateDMLWrapper.add(returnDMLActionsWrapper);      
        }
        
        // Retry Machanism for Add Circuit callout
        //system.debug('circuitMapUpdate====>'+circuitMapUpdate);
        if(caseUpdateCircuitSet!= NULL && !caseUpdateCircuitSet.IsEmpty()){
            dmlActionsWrapperCirUpdate = AtomsIntegrationAPI.sendUpdateCircuitReqToAtoms(circuitMapUpdate, caseUpdateCircuitSet);
        }
        
        // Retry Machanism for Add Circuit callout
        //system.debug('caseUpdateToAtomsLst====>'+caseUpdateToAtomsLst);
        if(caseUpdateToAtomsLst!= NULL && caseUpdateToAtomsLst.Size() > 0){
            respWrapper = UpdateNeuStarDetailsToATOMSUtility.updateNeuStarToSalesforce(caseUpdateToAtomsLst, true, true);
            if(respWrapper != null){
                if(respWrapper.caseIdASRLstMap != null){
                    //system.debug('respWrappeASRr>>'+respWrapper.caseIdASRLstMap);
                    caseIdASRListMap.putAll(respWrapper.caseIdASRLstMap);
                }
                if(respWrapper.caseIdCktLstMap != null){
                    //system.debug('respWrapperCKT>>'+respWrapper.caseIdCktLstMap);
                    caseIdCktListMap.putAll(respWrapper.caseIdCktLstMap);
                }
            }
        }

        // Retry Machanism for create order callout
        //system.debug('casePOList====>'+casePOList);
        if(casePOList!= NULL && casePOList.size() > 0){
            dmlActionsWrapperPO = NeustarIntegrationAPI.executeAPIs(casePOList,mapConstants.get('CreateOrder').Value__c,casePOSet);
        }
        
        // Retry Machanism for Get Circuit callout
        //system.debug('casePOList====>'+casePOList);
        if(circuitMapGet!= NULL && circuitMapGet.size() > 0){
            dmlActionsWrapperCircGet = AtomsIntegrationAPI.getCircuitAPIToAtoms(circuitMapGet,mapConstants.get('Get Circuit').Value__c, caseGetCircuitSet);
        }

        //Call DML Methods for all callouts
        if(dmlActionsWrapperCircuit != NULL){
            AtomsIntegrationAPI.performCircuitDMLs(dmlActionsWrapperCircuit);
        }
        if(dmlActionsWrapperCirUpdate != NULL){
            AtomsIntegrationAPI.performCircuitDMLs(dmlActionsWrapperCirUpdate);
        }
        if(dmlActionsWrapperCircGet != NULL){
            AtomsIntegrationAPI.performCircuitDMLs(dmlActionsWrapperCircGet);
        }

        if(listASRCreateDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRCreateDMLWrapper);
        }
        
        if(listASRUpgradeDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRUpgradeDMLWrapper);
        }
        
        if(listASRDisconnectDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRDisconnectDMLWrapper);
        }
        //system.debug('listASRUpdateDMLWrapper===>'+listASRUpdateDMLWrapper);
        if(listASRUpdateDMLWrapper!=null){
            SFDCtoAtomsAPI.performASRDMLs(listASRUpdateDMLWrapper);
        }
        
        //system.debug('dmlActionsWrapperPO===>'+dmlActionsWrapperPO);
        //Call DMLs for Purchase Order
        if(dmlActionsWrapperPO != NULL){
           NeustarIntegrationAPIHelper.performCreatePOrderDMLs(dmlActionsWrapperPO);
        }
        if(casePOList.size() > 0)
            NeustarIntegrationAPIHelper.updateCaseAPIStatus(casePOList);
        
    }

    public void finish(Database.BatchableContext bc){
         //Calling Queuable Method to update ASR and Circuit in ATOMS:
        //system.debug('caseIdASRListMap>>FINISH'+caseIdASRListMap);
        //system.debug('caseIdCktListMap>>FINISH'+caseIdCktListMap);
        if((caseIdASRListMap != null || caseIdCktListMap != null)){
            ID jobID = System.enqueueJob(new CallATOMSUpdateAPIQueueable(caseIdASRListMap,caseIdCktListMap));
        }
    }
    

}
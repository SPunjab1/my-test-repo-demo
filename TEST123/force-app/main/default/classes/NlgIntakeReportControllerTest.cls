@istest
public class NlgIntakeReportControllerTest {
	@testSetup
    static void testDataSetup() {
         //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        //Intialize list of SI Savings Tracker to insert records
        List<SI_Savings_Tracker__c> listSISavingTracker = new  List<SI_Savings_Tracker__c>();
        //create Account records
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
         // Opportunity RecordType
        Id backhaulContractRecTypeId = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',System.Label.SI_Backhaul_ContractRT);
        Opportunity oppRecForContract = TestDataFactory.createOpportunitySingleIntake(1, backhaulContractRecTypeId)[0];
        oppRecForContract.StageName = System.Label.SI_OppStageDefaultValue;  
        oppRecForContract.AccountId = listAccount[0].Id;
        oppRecForContract.Contract_Start_Date__c = System.today();
        oppRecForContract.Term_mo__c = 1.00;
        insert oppRecForContract;
        List<Location_ID__c> locationId = TestDataFactory.createLocationId('test','sample',1);
        insert locationId;
        oppRecForContract.Site_ID__c = locationId[0].Id;
        update  oppRecForContract;
    }
    
    @istest static void GetSiteIDTest(){
         Test.startTest();
         Opportunity opportunityRec = [Select id from Opportunity limit 1];
         NlgIntakeReportController.GetSiteID(opportunityRec.Id);
         Test.stopTest();
    }
}
/******************************************************************************
@author      : IBM
@date        : 03.06.2020
@description : Test class for SI_SubmitIntakeRequest apex class
@Version     : 1.0
*******************************************************************************/
@isTest
private class SI_SubmitIntakeRequestTest {
    
    /**************************************************************************
    * @description Method to setup test data
    **************************************************************************/
    @testSetup
    private static void testDataSetup() {
        //Intialize list of users
        List<User> listOfUsers = new List<User>();
        
        //create single intake user
        User intakeUser = TestDataFactory.createUserByProfileName(Label.SI_IntakeUserProfileName);
        listOfUsers.add(intakeUser);
        //insert users
        insert listOfUsers;
        
        // Insert record for ValidationRuleManager__c custom setting        
        insert new ValidationRuleManager__c(SetupOwnerId=UserInfo.getOrganizationId(), Run_Validation__c=true);
        Id macroDeselectRecTypeId =
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        
        //Intialize list of case records
        List<Case> listOfCaseRecords = new List<Case>();
        //create case for single intake user
        Case mdCaseRec = TestDataFactory.createCaseSingleIntake(1,macroDeselectRecTypeId)[0];
        mdCaseRec.RecordTypeId = macroDeselectRecTypeId;
        mdCaseRec.OwnerId = intakeUser.Id;
        listOfCaseRecords.add(mdCaseRec);
        
        //insert Case records
        insert listOfCaseRecords;
    }
    
    /****************************************************************************
    * @description To test updateStatus method functionality for internal user
    *****************************************************************************/
    @isTest
    private static void testUpdateStatusForInternalUser() {
        //Fetch single intake user details
        User intakeUser = [SELECT Id
                           FROM User
                           WHERE Username LIKE '%@sitestuser.com%'
                           AND Name = 'SITest User'
                           AND Profile.Name = :Label.SI_IntakeUserProfileName];
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case];
        
        Test.startTest();
        //Run apex class in context of community user
        //System.runAs(intakeUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_SubmitIntakeRequestFlowPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id',caseRec.Id);
            SI_SubmitIntakeRequest siRequest = new SI_SubmitIntakeRequest();
            siRequest.updateCaseStatus();
            siRequest.backToCaseRecord();
       // }
        Test.stopTest();
        
        //Fetch updated case record
        Case updatedCaseRec = [SELECT Id, Status, OwnerId
                               FROM Case
                               WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(Label.SI_CaseStatusNewSubmitted, updatedCaseRec.Status,
                            'Case Status should be New-Submitted');
        System.assertNotEquals(String.valueOf(intakeUser.Id), updatedCaseRec.OwnerId,
                               'Case Owner should be changed');
    }
    
    /****************************************************************************
    * @description To test updateStatus method functionality for community user
    *****************************************************************************/
    @isTest
    private static void testBlankCaseIdError() {
        //Fetch community user details
        User communityUser = [SELECT Id
                              FROM User
                              WHERE Username LIKE '%@sitestuser.com%'
                              AND Name = 'SITest User'
                              AND Profile.Name = :Label.SI_IntakeUserProfileName];
        
        Test.startTest();
        //Run apex class in context of community user
        System.runAs(communityUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_SubmitIntakeRequestFlowPage;
            Test.setCurrentPage(submitIntakePage);
            SI_SubmitIntakeRequest siRequest = new SI_SubmitIntakeRequest();
            siRequest.updateCaseStatus();
            siRequest.backToCaseRecord();
        }
        Test.stopTest();
        
        //Fetch updated case record
        Case updatedCaseRec = [SELECT Id, Status, OwnerId
                               FROM Case
                               WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertNotEquals(Label.SI_CaseStatusNewSubmitted, updatedCaseRec.Status,
                               'Case Status should not be New-Submitted');
    }
    
    /****************************************************************************
    * @description To test updateStatus method functionality for community user
    *****************************************************************************/
    @isTest
    private static void testNonDraftStatusError() {
        //Fetch case record
        Case caseRec = [SELECT Id
                        FROM Case];
        caseRec.Status = Label.SI_CaseStatusNewSubmitted;
        update caseRec;
        
        Test.startTest();
        //Set vf page as current page for testing
        PageReference submitIntakePage = Page.SI_SubmitIntakeRequestFlowPage;
        Test.setCurrentPage(submitIntakePage);
        //put the case id as a parameter
        ApexPages.currentPage().getParameters().put('id',caseRec.Id);
        new SI_SubmitIntakeRequest();
        Test.stopTest();
        
        //Fetch updated case record
        Case updatedCaseRec = [SELECT Id, Status, OwnerId
                               FROM Case
                               WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(Label.SI_CaseStatusNewSubmitted, updatedCaseRec.Status,
                            'Case Status should not be updated');
    }
    
    /****************************************************************************
    * @description To test updateStatus method functionality for community user
    *****************************************************************************/
    @isTest
    private static void testConstuctorException() {
        //Fetch community user details
        User intakeUser = [SELECT Id
                           FROM User
                           WHERE Username LIKE '%@sitestuser.com%'
                           AND Name = 'SITest User'
                           AND Profile.Name = :Label.SI_IntakeUserProfileName];
        
        Test.startTest();
        //Run apex class in context of community user
        System.runAs(intakeUser) {
            //Set vf page as current page for testing
            PageReference submitIntakePage = Page.SI_SubmitIntakeRequestFlowPage;
            Test.setCurrentPage(submitIntakePage);
            //put the case id as a parameter
            ApexPages.currentPage().getParameters().put('id','Random');
            SI_SubmitIntakeRequest siRequest = new SI_SubmitIntakeRequest();
            siRequest.backToCaseRecord();
        }
        Test.stopTest();
        //Fetch updated case record
        Case updatedCaseRec = [SELECT Id, Status, OwnerId
                               FROM Case
                               WHERE RecordType.DeveloperName = :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        System.assertEquals(Label.SI_CaseStatusDraft, updatedCaseRec.Status,
                            'Case Status should not be updated');
    }
}
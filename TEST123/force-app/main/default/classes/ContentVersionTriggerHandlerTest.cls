/*********************************************************************************************
@author     : IBM
@date       : 31.05.19
@description: This is the test class for ContentVersionTrigger and ContentVersionTriggerHandler
@Version    : 1.0
**********************************************************************************************/
@isTest
public with sharing class ContentVersionTriggerHandlerTest {
    @TestSetup
    static void testDataSetup(){
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
        String WorkflowTmpName1='TIFS AAV CIR Change';
        List<case> caseList= TestDataFactory.createCase(WorkflowTmpName1,2,recTypeId);
        caseList[0].PIER_Work_Order_Id__c = '4562288';
        caseList[0].Schedule_Required__c=true;
        Case task = new Case();
        task.Process_Update_Task_API__c = true;
        task.assignment_group__c= null;
        task.PIER_Work_Order_Task_ID__c= '12344';
        task.project_name__c= 'TM';
        task.Workflow_Template__c= 'TIFS AAV CIR Change' ;
        task.PIER_Approval__c= true;
        task.Attachment_Required__c= true;
        task.Change_Management_Required__c=false ;
        task.User_Description__c= 'Desc';
        task.Executable_Task_in_PIER__c= true;
        task.Task_Description__c= 'Desc';
        task.Worklog_Required__c= true;
        task.ParentId = caseList[0].Id;
        caseList.add(task);

        insert caseList;

    }

    @isTest
    static void testUploadAttachment_ShouldCreateIntegrationLog() {
        List<ContentDocument> listToDelete = new List<ContentDocument>();
        ContentVersion cv = new ContentVersion();
        cv.Title = 'ABC';
        cv.PathOnClient = 'test';
        cv.ContentLocation = 'S';
        cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;

        List<Case> caseLst = [SELECT Id FROM Case WHERE PIER_Work_Order_Task_ID__c= '12344'];
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;


        ContentDocumentLink cDLink = new ContentDocumentLink();
        cDLink.ContentDocumentId = conDocId;
        cDLink.LinkedEntityId = caseLst[0].Id;
        cDLink.ShareType = 'I';
        insert cDLink;

        Test.startTest();
             PierIntegrationMock fakeResponse = new PierIntegrationMock(200,'{"transactionId":"8f96e746-7b40-4ba8-a31a-f7d394b866b7","invokingUser":"SPunjab1","timestamp":"2020-07-13T19:25:22.144Z","data":[{"id":157987,"templateId":1314,"description":"Test","longDescription":"Testing ignore","status":"active","manager":"","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:20.86834497Z","closedAt":null,"sourceSystem":"","sourceRecordId":"","tasks":{"0":{"data":{"id":873597,"workflow_id":157987,"name":"","description":"Verify Port Speed / Router Swap Complete","displayOrder":1,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.158974347Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":14,"timestamp":"2020-07-13 19:25:21.157605355 +0000 UTC","transactionId":""},"1":{"data":{"id":873598,"workflow_id":157987,"name":"","description":"Execute RFC2544, log results in ATOMs","displayOrder":2,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.276423755Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":22,"timestamp":"2020-07-13 19:25:21.275555477 +0000 UTC","transactionId":""},"2":{"data":{"id":873599,"workflow_id":157987,"name":"","description":"Execute BURST tests, log results in ATOMs, Activate Site","displayOrder":3,"taskType":"Admin Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.403841799Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":40,"timestamp":"2020-07-13 19:25:21.401872845 +0000 UTC","transactionId":""},"3":{"data":{"id":873600,"workflow_id":157987,"name":"","description":"Generate/Execute Script","displayOrder":4,"taskType":"CR Task","StartDate":null,"EndDate":null,"startedAt":null,"completedAt":null,"assignedGroup":"Data Delivery & Support","assignee":"","status":"Open","createdBy":"SPunjab1","createdAt":"2020-07-13T19:25:21.556928581Z","config_item":"","associated_record":"","active_flag":true},"version":"1.0.25","invokingUser":"","duration":20,"timestamp":"2020-07-13 19:25:21.54619578 +0000 UTC","transactionId":""}}}],"version":null,"duration":1482}');
        List<Id> integrationLogIds = new List<Id>();
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(integrationLogIds,'CreateWorkflow');
        ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
            ContentVersion cv1 = new ContentVersion();
            cv1.Title = 'ABC';
            cv1.PathOnClient = 'test';
            cv1.ContentLocation = 'S';
            cv1.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            cv1.ContentDocumentId = conDocId;
            insert cv1;


        Test.stopTest();
    }
    
     @isTest
    static void testGeneratePublicUrl() {
        List<ContentDocument> listToDelete = new List<ContentDocument>();
        List<ContentVersion> cList = new List<ContentVersion>();
        ContentVersion cv = new ContentVersion();
        cv.Title = 'ABC';
        cv.PathOnClient = 'test';
        cv.ContentLocation = 'S';
        cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
        cList.add(cv);
        insert cList;

        List<Case> caseLst = [SELECT Id FROM Case WHERE PIER_Work_Order_Task_ID__c= '12344'];
        List<ContentVersion> conDocId = [SELECT ContentDocumentId,Title FROM ContentVersion WHERE Id =:cList[0].Id];
        ContentDocumentLink cDLink = new ContentDocumentLink();
        cDLink.ContentDocumentId = conDocId[0].ContentDocumentId;
        cDLink.LinkedEntityId = caseLst[0].Id;
        cDLink.ShareType = 'I';
        insert cDLink;
		List<integration_log__c> integrationLogIds = new List<integration_log__c>();
        integrationLogIds.add(new integration_log__c(Work_Order__c=caseLst[0].id,API_Type__c= ''));
        insert integrationLogIds;
        Test.startTest();
         ContentVersionTriggerHandler.generatePublicLinks(conDocId);  
        Test.stopTest();
    }

}
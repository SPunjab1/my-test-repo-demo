/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : WorkOrderCreationBatchTest
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 09.06.2019
*  @Description      : Test class for WorkOrderCreationBatch
**********************************************************************************************************************
**********************************************************************************************************************/
@isTest
public class WorkOrderCreationBatchTest 
{
/*************************************************************************************
* @Description :Test Method for ExecuteBatch
*************************************************************************************/  
    static testMethod void testMethod1() 
    {
        List<Case> csLst = new List<Case>();
        List<Intake_Request__C> intakeReqList = TestDataFactory.createIntakeRequest('TIFS Core Facility Add','Yes');
        insert intakeReqList;
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();   
        String WorkflowTmpName ='OM CMG_Retail Broadband Dep';
        Integer NumOfCases=200;
        csLst= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        
        Test.startTest();
        WorkOrderCreationBatch obj = new WorkOrderCreationBatch(csLst,intakeReqList[0].Id);
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
/*************************************************************************************
* @Description :Test Method for finsh logic of batch :to update failure numer of records.
*************************************************************************************/ 
    static testMethod void testMethod2() 
    {
        List<Case> csLst = new List<Case>();
        List<Intake_Request__C> intakeReqList = TestDataFactory.createIntakeRequest('TIFS AAV CIR Retest','Yes');
        Test.startTest();
        insert intakeReqList;
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();   
        String WorkflowTmpName ='AAV CIR Retest';
        Integer NumOfCases=200;
        csLst= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        insert(csLst); 
        List<Case> WorkOrderTaskList= new List<Case>();
        List<Case> WorkOrderTaskListToUpdate= new List<Case>();
        WorkOrderTaskList =[SELECT id, Status,Worklog_Required__c,Attachment_Required__c,No_Attachment_Reason__c from Case where  (Parent.Id IN:csLst AND Task_Dependency__c!=NULL) OR (Parent.Id IN:csLst AND Last_Task__C=TRUE) limit 100];
        for(Case cs: WorkOrderTaskList){
            cs.Status='Closed';
            cs.Worklog_Required__c=true;
            cs.Attachment_Required__c = true;    
            cs.No_Attachment_Reason__c=null;
            WorkOrderTaskListToUpdate.add(cs);
        }  
        update(WorkOrderTaskListToUpdate);
        WorkOrderCreationBatch obj = new WorkOrderCreationBatch(csLst,intakeReqList[0].Id);
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
    
}
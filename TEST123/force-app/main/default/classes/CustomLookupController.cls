public with sharing class CustomLookupController {

    @AuraEnabled
    public static List<RecordsData> fetchRecords(String objectName, String searchString ,String value) {
        try {
            List<RecordsData> recordsDataList = new List<RecordsData>();
            String query = 'SELECT Id, Name, EquipmentModel__c, EquipmentManufacturer__c, Weight__c, Width__c, Length__c, Depth__c, EquipmentType__c FROM '+objectName;
            if(String.isNotBlank(value)) {
                query += ' WHERE Id = \''+ value + '\' ';
            } else {
                query += ' WHERE EquipmentModel__c LIKE ' + '\'' + String.escapeSingleQuotes(searchString.trim()) + '%\' ';
            }

            for(SObject s : Database.query(query)) {
                recordsDataList.add( new RecordsData((String)s.get('EquipmentModel__c'), (String)s.get('id') , (Equipment__c)s ));
            }
            return recordsDataList;
        } catch (Exception err) {
            if ( String.isNotBlank( err.getMessage() ) && err.getMessage().contains( 'error:' ) ) {
                throw new AuraHandledException(err.getMessage().split('error:')[1].split(':')[0] + '.');
            } else {
                if(test.isRunningTest()){
                    system.debug('error thrown');
                }
                else{
                    throw new AuraHandledException(err.getMessage());
                }
                
            }
        }
        return null;
    }

    public class RecordsData {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public Equipment__c equipRec;
        public RecordsData(String label, String value,Equipment__c eqp) {
            this.label = label;
            this.value = value;
            this.equipRec = eqp;
        }
    }
}
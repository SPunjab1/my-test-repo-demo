/******************************************************************************
@author      - Shiva Goli
@date        - 09.17.2020
@description - Trigger Handler Class for Opportunity Approval History
@version     - 1.0
*******************************************************************************/
public class OpportunityApprovalHistoryTriggerHandler extends TriggerHandler{
    public static boolean isOppTriggerExecuted = FALSE ;
    private static final string OPP_API = 'Opportunity_Approval_History__c';
    /***************************************************
    *@description - constructor
    ***************************************************/
    public OpportunityApprovalHistoryTriggerHandler() {
        super(OPP_API);
    }
    /***************************************************************************************
    @description : Return the name of the handler invoked
    @return - API name of Opportunity Approval History object
    ****************************************************************************************/
    public override String getName() {
        return OPP_API;
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    ****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems){
        List<Opportunity_Approval_History__c> newApprovalHistory = new List<Opportunity_Approval_History__c>();
        List<Opportunity_Approval_History__c> newOppAHMap= new List<Opportunity_Approval_History__c>();
        newOppAHMap = (List<Opportunity_Approval_History__c>) newItems;
        //Selecting all active BusinessHours records
        BusinessHours businessHrsDefault = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true And Name ='NLG Business Hours'];
        
        for(Opportunity_Approval_History__c history: newOppAHMap){
            if (history.Start_Date__c != NULL && history.End_Date__c != NULL) {
                    decimal result = BusinessHours.diff(businessHrsDefault.Id, history.Start_Date__c, history.End_Date__c);
                    Decimal resultingHours = result / (60 * 60 * 1000);
                    history.SLA_Hours__c = resultingHours.setScale(2);
            }
        }
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    @param newItemsMap - Map of new Opportunities
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap){
        
        List<Opportunity_Approval_History__c> newOpps= new List<Opportunity_Approval_History__c>();
        newOpps = (List<Opportunity_Approval_History__c>) newItems;
        Map<Id,Opportunity_Approval_History__c> newMap=new Map<Id,Opportunity_Approval_History__c>();
        newMap=(Map<Id,Opportunity_Approval_History__c>)newItemsMap;
        Map<Id,Opportunity_Approval_History__c> oldMap=new Map<Id,Opportunity_Approval_History__c>();
        oldMap=(Map<Id,Opportunity_Approval_History__c>)oldItemsMap;
        Map<Id,Opportunity_Approval_History__c> newNlgOpps = new Map<Id,Opportunity_Approval_History__c>();        
        
        //Selecting all active BusinessHours records
        BusinessHours businessHrsDefault = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true And Name ='NLG Business Hours'];
        
        for(Opportunity_Approval_History__c history: newOpps){
            //fill in lists to process to various business processes.
            if ((history.Start_Date__c != NULL && history.End_Date__c != NULL) && ((history.Start_Date__c != oldMap.get(history.id).Start_Date__c) || (history.End_Date__c != oldMap.get(history.id).End_Date__c))) {
                decimal result = BusinessHours.diff(businessHrsDefault.Id, history.Start_Date__c, history.End_Date__c);
                    Decimal resultingHours = result / (60 * 60 * 1000);
                    history.SLA_Hours__c = resultingHours.setScale(2);
            }
        }
        
        
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap){}
    /**************************************************************************************
    @description : Trigger handlers for events
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {}
    /**************************************************************************************
    @description : Trigger handlers for events
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {}
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    @param newItemsMap - Map of new Opportunities
    @param oldItemsMap - Map of old Opportunities
    ****************************************************************************************/
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
        
        
    }
    /**************************************************************************************
    @description : Trigger handlers for events
    @param newItems - List of new Opportunities
    @param newItemsMap - Map of new Opportunities
    ****************************************************************************************/
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        
    }

}
/******************************************************************************
@author      - IBM
@date        - 26.06.2020
@description - Test class for SI_CaseTriggerHandlerHelper
@Version     - 1.0
*******************************************************************************/
@isTest(seeAllData=false)
public class SI_CaseTriggerHandlerHelperTest {
    
    /**************************************************************************
    * @description  Method to setup test data
    **************************************************************************/
    @testSetup
    static void testDataSetup() {
        List<Case> caseList= new List<Case>();
        List<Case> caseBackhaul= new List<Case>();
        List<Case> caseListMacro= new List<Case>();
        List<Case> caseListWorkOrder= new List<Case>();
        List<Opportunity> oppList= new List<Opportunity>();
        List<Intake_Site__c> intakeList= new List<Intake_Site__c>();
        List<Contract_Site__c> contractSiteList = new List<Contract_Site__c>();
        List<Location_ID__c> listLocation = new List<Location_ID__c>();
        Integer numOfRecords=2;
        Id macroDeselectRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',
                                                           Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]);
        Id oppBackhaulRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity',Label.SI_Backhaul_ContractRT);
        Id intakeCIRRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Intake_Site__c',Label.SI_IntakeSiteRT);
        Id orderManagementRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case', 'Order_Management');
        Id caseMacroDeRT =
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseRT.split(';')[0]);
        Id caseWorkOrderRT =
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseRT.split(';')[1]);
        // record type id for backhaul internal case     
        Id backhaulRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',System.Label.SI_CaseRT.split(';')[2]);
        //calling insert method
        caseBackhaul =  TestDataFactory.createCaseSingleIntake(numOfRecords,backhaulRecTypeId);
         // Insert record for ValidationRuleManager__c custom setting        
        insert new ValidationRuleManager__c(SetupOwnerId =UserInfo.getOrganizationId(), 
                                            Run_Validation__c = false);
        //Calling Method to Insert Location
        listLocation = TestDataFactory.createLocationId('Test','Test',numOfRecords);
        listLocation[0].Backhual_Universe__c = true;
        listLocation[1].Backhual_Universe__c = true;
        Location_Id__c locationForOm = TestDataFactory.createLocationId('Test1','Test1',1)[0];
        locationForOm.Region__c = 'South';
        locationForOm.Backhual_Universe__c = true;
        insert locationForOm;
        
        // Calling Method to Insert Case
        caseList =  TestDataFactory.createCaseSingleIntake(numOfRecords,macroDeselectRecTypeId);
        caseListMacro = TestDataFactory.createCaseSingleIntake(numOfRecords,caseMacroDeRT);
        List<Case> listOfOmCases = TestDataFactory.createCaseSingleIntake(numOfRecords,orderManagementRecTypeId);
        caseListWorkOrder = TestDataFactory.createCaseSingleIntake(numOfRecords,caseWorkOrderRT);
        // Calling Method to Insert Opportunity
        oppList = TestDataFactory.createOpportunitySingleIntake(numOfRecords,oppBackhaulRecTypeId);
        Test.startTest();
        insert listLocation;
        for(case objCase : caseListMacro){
            objCase.Location_IDs__c = listLocation[0].Id;
            objCase.status = 'New-Submitted';
        }
        for(case objCase : caseListWorkOrder){
            objCase.Location_IDs__c = listLocation[0].Id;
            objCase.status = 'New-Submitted';
        }
        for(Case caseRec : listOfOmCases) {
            caseRec.Status = 'New-Submitted';
            caseRec.Location_Ids__c = locationForOm.Id;
        }
       
        insert caseBackhaul; 
        insert caseList;
        insert caseListMacro;
        insert caseListWorkOrder;
        insert oppList;
        insert listOfOmCases;
        List<case> listOfCaseRecs = new List<Case>();
       
        //Calling Method to Insert Intake Site
        intakeList = TestDataFactory.createIntakeSiteSingleIntake(numOfRecords,caseList[0].Id,intakeCIRRecTypeId);
        insert intakeList;
        
        intakeList[0].Billable_Circuit__c = 'Yes';
        update intakeList[0];
        contractSiteList = TestDataFactory.createConractSiteSingleIntake(numOfRecords, oppList[0].Id);
        for(Contract_Site__c objCS : contractSiteList){
            objCS.Opportunity__c = oppList[0].Id;
            objCS.Intake_Site__c = intakeList[0].Id;
        }
        insert contractSiteList;
        //Calling Method to Insert Account
        List<Account> accountList = TestDataFactory.createAccountSingleIntake(5);
        insert accountList;
        List<Entitlement> entitlementList = TestDataFactory.createEntitlementSingleIntake(5, accountList[0].Id);
        //retrieve Default Entitlement Process information
        List<SlaProcess> entitlementProcessId = [SELECT Id 
                                                 FROM SlaProcess 
                                                 WHERE SObjectType = 'Case'];
        for(Entitlement objEntile : entitlementList) {
            objEntile.SlaProcessId = entitlementProcessId[0].id;
        }
        insert entitlementList;
        
        for(Case caseRec: [Select Id FROM Case]) {
            caseRec.Status = 'New-Submitted';
            listOfCaseRecs.add(caseRec);
        }
        update listOfCaseRecs;
        
        Test.stopTest();
    }
    
    /**************************************************************************
    * @description  Test Method to test stopSLAFieldUpdateCheck method
    **************************************************************************/
    static testmethod void updateCaseMilestoneSITest(){
        List<Case> caseList = new List<Case>();
        List<Entitlement> listOfEntitlement = [SELECT Id
                                              FROM Entitlement
                                               WHERE Name LIKE 'Test%'];
        List<Opportunity> listOfOppty = [SELECT Id
                                        FROM Opportunity 
                                        WHERE RecordType.DeveloperName = :Label.SI_Backhaul_ContractRT];
        List<Case> listOfCases = [SELECT Id
                                  FROM Case 
                                  WHERE RecordType.DeveloperName = 
                                  :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        Test.startTest();
        listOfCases[0].Opportunity__c = listOfOppty[0].Id;
        update listOfCases[0];
        listOfOppty[0].StageName = 'Submitted to CPM';
        listOfOppty[0].Contract_Action__c = 'New';
        listOfOppty[0].Term_mo__c = 1;
        listOfOppty[0].Description = 'Test description';
        update listOfOppty[0];
        for(Case caseObj : listOfCases) {
            caseObj.Status = System.Label.SI_CaseStatus.split(',')[0] ;
            caseObj.EntitlementId  =  listOfEntitlement[0].id;
            caseObj.SlaStartDate = System.Now();
            caseObj.IsStopped = false;
            caseObj.Description = 'test';
            caseObj.Provide_Justification__c = 'test1';
            caseList.add(caseObj);
        }
        update caseList;
        Test.stopTest();
        System.assertEquals(false, caseList.isEmpty(),'Milestones should be completed');
    }
    
    /**************************************************************************
    * @description   Method to test createContractSiteSI
    **************************************************************************/
    static testmethod void runTestcreateContractSiteSI(){
        
        List<Opportunity> oppList = [SELECT Id FROM Opportunity];
        List<Case> caseList = new List<Case>();
        for(Case cs : [SELECT Opportunity__c FROM Case]){
            cs.Opportunity__c = oppList[0].Id;
            caseList.add(cs);
        }
        update caseList;
        System.assertEquals(true,String.isNotBlank(caseList[0].Opportunity__c),'Opportunity Should not blank');
    }
    
    /**************************************************************************
    * @description   Method to test sendEmailToWorkOrderRecipients
    **************************************************************************/
    static testmethod void testSendEmailToWorkOrderRecipients(){
        // Record Type Id macro deselect
        Id caseMacroDeRT =
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseRT.split(';')[0]);
        //Intialize List of case
        List<Case> lstCase = new List<Case>();
        //Intialize List of custom setting
        List<SI_Configuration__c> lstSetting = new List<SI_Configuration__c>();
        lstSetting.add(new SI_Configuration__c(Name = 'user 1 Manager' , key__c = System.Label.SI_UserKey.split(',')[1],
                                               Value__c = 'Test@Manager.com.invalid'));
        Test.startTest();
        //Insert custom setting
        insert lstSetting;
        System.assertEquals(true, !lstSetting.isEmpty(),'List should not be empty');
        //Loop through marco deselect case
        for(Case objCase : [SELECT Status FROM Case
                            WHERE RecordTypeId =: caseMacroDeRT]) {
            objCase.Status =  System.Label.SI_CaseStatus.split(',')[2];
            lstCase.add(objCase);
        }
        //Update case with OM ready status
        update lstCase;
        System.assertEquals(System.Label.SI_CaseStatus.split(',')[2],
                            lstCase[0].Status,'Case Status Should be OM Ready');
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.assertEquals(true, invocations > 0, 'An email has been sent');
    }
    
    /**************************************************************************
    * @description   Method to test sendEmailToWorkOrderRecipients
    **************************************************************************/
    static testmethod void testSendEmailToWorkOrderRecipientRegWork(){
        // Record Type Id macro deselect and work order
        Id caseMacroDeRT =
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseRT.split(';')[0]);
        Id caseWorkOrderRT =
            TestDataFactory.getRecordTypeIdByDeveloperName('Case',Label.SI_CaseRT.split(';')[1]);
        //Intialize list of case
        List<Case> lstCase = new List<Case>();
        List<Case> lstCaseWorkOrder = new List<Case>();
        List<SI_Configuration__c> lstSetting = new List<SI_Configuration__c>();
        lstSetting.add(new SI_Configuration__c(Name = 'user 1' , key__c = System.Label.SI_UserKey.split(',')[0],
                                               Value__c = 'Test@User.com.invalid'));
        Test.startTest();
        insert lstSetting;
        System.assertEquals(true, !lstSetting.isEmpty(),'List should not be empty');
        List<Location_ID__c> lstLocation = new List<Location_ID__c>();
        //Loop through Location
        for(Location_ID__c objLocation : [SELECT Region__c FROM Location_ID__c  ]){
            objLocation.Region__c = 'CENTRAL';
            objLocation.Billable_Circuit__c = true;
            lstLocation.add(objLocation);
        }
        //Update location with region
        update lstLocation;
        System.assertEquals('CENTRAL', lstLocation[0].Region__c,'Location Region Should be CENTRAL');
        //Loop through marco deselect case
        for(Case objCase : [SELECT Workflow_Template__c,Location_IDs__c FROM Case
                            WHERE RecordTypeId =: caseWorkOrderRT]){
                                objCase.Workflow_Template__c = 'OM CMG AAV Microwave';
                                lstCaseWorkOrder.add(objCase);
                            }
        //Update case with workflow template
        update lstCaseWorkOrder;
        for(Case objCase : [SELECT Status  FROM Case
                            WHERE RecordTypeId =: caseMacroDeRT]) {
            objCase.Status =  System.Label.SI_CaseStatus.split(',')[2];
            lstCase.add(objCase);
        }
        //Update case with status OM ready
        update lstCase;
        System.assertEquals(System.Label.SI_CaseStatus.split(',')[2],
                            lstCase[0].Status,'Case Status Should be OM Ready');
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.assertEquals(true, invocations > 0, 'An email has been sent');
    }
    
    /**************************************************************************
    * @description  To test closeMileStoneForOpenCases method functionality
    **************************************************************************/
    @isTest
    static void closeMileStoneForOpenCasesTest() {
        List<Case> caseList = new List<Case>();
        List<Entitlement> listOfEntitlement = [SELECT Id
                                              FROM Entitlement
                                               WHERE Name LIKE 'Test%'];
        List<Opportunity> listOfOppty = [SELECT Id
                                        FROM Opportunity 
                                        WHERE RecordType.DeveloperName = :Label.SI_Backhaul_ContractRT];
        
        List<Case> listOfCases = [SELECT Id
                                  FROM Case 
                                  WHERE RecordType.DeveloperName = 
                                  :Label.SI_Opp_Backhaul_Contract_Case_RT.split(';')[0]];
        listOfCases[0].Opportunity__c = listOfOppty[0].Id;
        update listOfCases[0];
        
        listOfOppty[0].StageName = 'Submitted to CPM';
        listOfOppty[0].Contract_Action__c = 'New';
        listOfOppty[0].Term_mo__c = 1;
        listOfOppty[0].Description = 'Test description';
        update listOfOppty[0];
        for(Case caseObj : listOfCases) {
            caseObj.Opportunity_Stage__c = 'CPM Review - Complete';
            caseObj.EntitlementId  =  listOfEntitlement[0].id;
            caseObj.SlaStartDate = System.Now();
            caseObj.IsStopped = false;
            caseObj.Description = 'test';
            caseObj.Provide_Justification__c = 'test1';
            caseList.add(caseObj);
        }
        Test.startTest();
        update caseList;
        Test.stopTest();
        System.assertEquals(false, caseList.isEmpty(),'Milestones should be completed');
    }
  
    /**************************************************************************
    * @description  To test closeMileStoneForOpenCases method functionality
    **************************************************************************/
    @isTest
    static void changeCaseStatusToOMReadyTest() {  
        Id intakeCIRRecTypeId = 
            TestDataFactory.getRecordTypeIdByDeveloperName('Intake_Site__c',Label.SI_IntakeSiteRT);
        List<Case> listOfCases = [SELECT Id, OwnerId
                                  FROM Case 
                                  WHERE RecordType.DeveloperName = 'Order_Management'];
        List<Intake_Site__c> intakeList = 
            TestDataFactory.createIntakeSiteSingleIntake(1,listOfCases[0].Id,intakeCIRRecTypeId);
        
        for(Case caseObj : listOfCases) {
            caseObj.Status = 'OM Ready';
        }
        Test.startTest();
        insert intakeList;
        update listOfCases;
        Test.stopTest();
        List<Case> listOfCasesUpdated = [SELECT Id, Status, OwnerId
                                         FROM Case 
                                         WHERE RecordType.DeveloperName = 'Order_Management'];
        System.assertEquals('OM Ready', listOfCasesUpdated[0].status, 'Status should be updated to OM Ready');
        System.assertEquals(true, listOfCasesUpdated[0].OwnerId != listOfCases[0].OwnerID, 'Owner should be updated');
    }
}
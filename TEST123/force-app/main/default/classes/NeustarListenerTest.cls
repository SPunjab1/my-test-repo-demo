@isTest
public class NeustarListenerTest {
    
    @testSetup static void setup() { 
        
        TestDataFactoryOMph2.TMobilePh2LoginDetailsHierarchyCreate();
        insert TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        List<Case> lstCase = TestDataFactoryOMph2.createCase( 'AAV CIR Retest', 1, Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId());
        insert lstCase;
        List<Purchase_Order__c> lstPurchase_Order = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT', 'COMCAST', 'Disconnect ', 1, null);
        lstPurchase_Order[0].PON__c = 'VG08184A-RHM100';
        insert lstPurchase_Order;
        List<circuit__c> lstCircuit = TestDataFactoryOMph2.createCircuitRecords( lstCase[0].Id, 1);
        insert lstCircuit; 
        
        List<Asr__c> lstAsr = TestDataFactoryOMph2.createASRRecords( null, lstPurchase_Order[0].Id, lstCircuit[0].Id, 2);
        lstAsr[0].SubService_Type__c = 'EVC';
        lstAsr[1].SubService_Type__c = 'UNI';
        insert lstAsr;
    }
    
    @isTest
    static void NeuStarMessageReciever() {
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(NeustarListenerRequest.testParse()));
        
        RestContext.request = req;
        RestContext.response= res;
        
        Test.startTest();
        NeuStarMessageRecieverUtility.ResponseWrapper resp = NeuStarMessageReciever.updateServiceRequest();
        Test.stopTest();
        
        List<Asr__c> lstAsr = [Select Id From Asr__c limit 2];
        lstAsr[0].SubService_Type__c = '';
        lstAsr[1].SubService_Type__c = '';
        update lstAsr;
        
        resp = NeuStarMessageReciever.updateServiceRequest();
        delete lstAsr[1];
        
        resp = NeuStarMessageReciever.updateServiceRequest();
        delete lstAsr[0];
        resp = NeuStarMessageReciever.updateServiceRequest();
        
        req.requestBody = Blob.valueOf('');
        resp = NeuStarMessageReciever.updateServiceRequest();
        req.requestBody = null;
        resp = NeuStarMessageReciever.updateServiceRequest();
        
        NeustarListenerRequest requestBody = NeustarListenerRequest.testParse();
        req.requestBody = Blob.valueOf(JSON.serialize(requestBody)+'as');
        resp = NeuStarMessageReciever.updateServiceRequest();
        
        requestBody.response = null;
        req.requestBody = Blob.valueOf(JSON.serialize(requestBody));
        resp = NeuStarMessageReciever.updateServiceRequest();
        
        requestBody = NeustarListenerRequest.testParse();        
        requestBody.Info = null;
        req.requestBody = Blob.valueOf(JSON.serialize(requestBody));
        resp = NeuStarMessageReciever.updateServiceRequest();
    }
    
    @isTest
    static void NeustarListenerRequestWrapper(){
        
        NeustarListenerRequest requestBody = NeustarListenerRequest.testParse();
        NeustarListenerRequest.Response resp = requestBody.response[0];
        resp.responseType = '';
        NeustarListenerRequest.Sections sec = resp.sections[0];
        sec.sectionName = '';
        sec.isEnabled = '';
        sec.index = 1;
        
		NeustarListenerRequest.Fields fld = sec.Fields[0];
        fld.guiAliasName = '';
        fld.path = null;
        fld.value = '';
        fld.fieldName = '';
        
        NeustarListenerRequest.Info info = requestBody.Info;
        info.productCatalogName = '';
        info.productCatalogId = 1;
        info.uomOrderKey = 1;
        info.uomOrderID = '';
        info.requestNumber = '';
        info.ver = '';
        info.orderStatus = '';
        info.activityId = '';
        info.activityType = '';
        info.application = '';
        info.tradingPartner = '';
        info.owner = '';
        info.createdDate = '';
        info.lastStatusChangeDate = '';
        info.sellerID = '';
        info.buyerID = '';
        info.transactionId = '';
        info.responseSeqId = 1;
    }
}
/*********************************************************************************************
@Class        : TestDataFactory
@author       : Archana
@date         : 07.06.2019 
@description  : This class is to create records of Diferrent Entities which can be used across all the TestClasses
@Version      : 1.0           
**********************************************************************************************/
@IsTest
public class TestDataFactory {
    Public static id recordTypeId(string obj,string recName){
        id recTypeId;
        if(obj!= null && recName != null){
            recTypeId= Schema.getGlobalDescribe().get(obj).getDescribe().getRecordTypeInfosByName().get(recName).getRecordTypeId();
        }
        return recTypeId;
    }
    
/***************************************************************************************
@Description : Method to create USER test classes
****************************************************************************************/  
    public static User createUser(){
        try{
            Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            date tDate = date.today();
            date uDate = Date.today().addDays(30);
            String orgId = UserInfo.getOrganizationId();
            String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
            Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
            String uniqueName = orgId + dateString + randomInt;
            User u = new User(); 
            u.ProfileId = profile1.Id;   
            u.alias = 'standt';
            u.email='standarduser6@testorg.com';
            u.emailencodingkey='UTF-8';
            u.firstname='fake';
            u.lastname='Testing';
            u.languagelocalekey='en_US';
            u.localesidkey='en_US';
            u.timezonesidkey='America/Los_Angeles';
            u.username=uniqueName +'standarduser6@testorg.com';
            return u;
        }   
        catch(Exception ex){
            throw ex;
        }           
    }
    
    
    
/***************************************************************************************
@Description : Method to create USER test classes
****************************************************************************************/  
    public static User createSourcingUser(){
        try{
            Profile profile1 = [Select Id from Profile where name = 'Sourcing Manager'];
            date tDate = date.today();
            date uDate = Date.today().addDays(30);
            String orgId = UserInfo.getOrganizationId();
            String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
            Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
            String uniqueName = orgId + dateString + randomInt;
            User u = new User(); 
            u.ProfileId = profile1.Id;   
            u.alias = 'standt';
            u.email='standarduser6@testorg.com';
            u.emailencodingkey='UTF-8';
            u.firstname='fake';
            u.lastname='Testing';
            u.languagelocalekey='en_US';
            u.localesidkey='en_US';
            u.timezonesidkey='America/Los_Angeles';
            u.username=uniqueName +'standarduser6@testorg.com';
            return u;
        }   
        catch(Exception ex){
            throw ex;
        }           
    }
    
/***************************************************************************************
@Description : Method to create Case data for test classes
****************************************************************************************/ 
    public static List<Case> createCase(String WorkFlowTmpName, Integer numberOfCases,Id recType) {
        
       try{
            List<Case> caseList = new List<Case>();
            for ( Integer i = 0 ; i < numberOfCases ; i++ ) {           
                Case cs = new Case( Workflow_Template__c= WorkFlowTmpName);  
                cs.RecordTypeId=recType ; 
                caseList.add(cs);            
            }
            return caseList;     
        }            
        catch(Exception ex){
            throw ex;  
        }             
    }
    
/***************************************************************************************
@Description : Method to create Case data for test classes
****************************************************************************************/ 
    public static List<Case> createCase1(String WorkFlowTmpName, Integer numberOfCases,Id recType,Id parId) {
        
       try{
            List<Case> caseList = new List<Case>();
            for ( Integer i = 0 ; i < numberOfCases ; i++ ) {           
                Case cs = new Case( Workflow_Template__c= WorkFlowTmpName);  
                cs.RecordTypeId=recType ;
                cs.ParentId = parId;
                caseList.add(cs);            
            }
            return caseList;     
        }            
        catch(Exception ex){
            throw ex;  
        }             
    } 
/***************************************************************************************
@Description : Method to create Case data for test classes for close task scenario
****************************************************************************************/ 
    public static List<Case> createCasesIntegrationLogs_ForCloseTask( String WorkFlowTmpName,Integer numberOfCases,Id recType) {
        
        try{
            List<Case> caseList = new List<Case>();
            for ( Integer i = 0 ; i < numberOfCases ; i++ ) {           
                Case cs = new Case( Workflow_Template__c= WorkFlowTmpName);  
                cs.RecordTypeId=recType;
                cs.task_number__c=1;
                caseList.add(cs);
                cs.Send_Status_Update_To_PIER__c =  true;
                cs.status='New';                
            }
            return caseList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    } 
    
/***************************************************************************************
@Description : Method to create Case data for test classes for Ad hoc task scenario
****************************************************************************************/ 
    public static List<Case> createCasesIntegrationLogs_ForAdhocTask( String WorkFlowTmpName,Integer numberOfCases,Id recType) {
        
        try{
            List<Case> caseList = new List<Case>();
            for ( Integer i = 0 ; i < numberOfCases ; i++ ) {           
                Case cs = new Case( Workflow_Template__c= WorkFlowTmpName);  
                cs.RecordTypeId=recType;
                caseList.add(cs);
                cs.Executable_Task_in_PIER__c =  true;
                cs.status='Closed';                
            }
            return caseList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    } 
    
/***************************************************************************************
@Description : Method to create Intake Request 
****************************************************************************************/ 
    public static List<Intake_Request__C> createIntakeRequest( string WorkflowTmpName, String multiSiteReq) {
        Integer numberOfIntakeReq = 1;
        try{
            List<Intake_Request__C> intakeReqList = new List<Intake_Request__C>();
            for ( Integer i = 0 ; i < numberOfIntakeReq ; i++ ) {           
                Intake_Request__C intReq = new Intake_Request__C(Workflow_Template__c=WorkflowTmpName,Multiple_Site_Request__c= multiSiteReq);                              
                intakeReqList.add(intReq);            
            }
            return intakeReqList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }
    
/***************************************************************************************
@Description : Method to create Location ID 
****************************************************************************************/ 
    public static List<Switch_ID__c> createSwitchIds( String switchId,string switchName,Integer numberOfRecords) {
        try{
            List<Switch_ID__c> switchIdList = new List<Switch_ID__c>();
            for(Integer i = 0 ; i < numberOfRecords ; i++ ) {           
                Switch_ID__c sId = new Switch_ID__c(Switch_ID__c = switchId+i, Name= switchName+i);                              
                switchIdList.add(sId);            
            }
            return switchIdList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                     
    } 
/***************************************************************************************
@Description : Method to create Location ID 
****************************************************************************************/ 
public static List<Location_ID__c> createLocationId( String locId,string locName,Integer numberOfRecords) {
        try{
            List<Location_ID__c> locationIdList = new List<Location_ID__c>();
            for ( Integer i = 0 ; i < numberOfRecords ; i++ ) {           
                Location_ID__c loc = new Location_ID__c(Location_ID__c=locId+i,Name= locName+i);                              
                locationIdList.add(loc);            
            }
            return locationIdList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }     
/***************************************************************************************
@Description : Method to create Location ID for platform event
****************************************************************************************/ 
    public static List<LocationId__e> createLocationIds( String locId) {
        Integer numberOfLocationId=250;
        try{
            List<LocationId__e> locationIdList = new List<LocationId__e>();
            for ( Integer i = 0 ; i < numberOfLocationId ; i++ ) {           
                LocationId__e loc = new LocationId__e(Site_cd__c=locId);                              
                locationIdList.add(loc);            
            }
            return locationIdList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }
    
/***************************************************************************************
@Description : Method to create work order id for platform event
****************************************************************************************/ 
    public static List<Close_Work_Order__e> createWorkOrderIds( String pierId) {
        Integer numberOfWorkOrderId=1;
        try{
            List<Close_Work_Order__e> workOrderList = new List<Close_Work_Order__e>();
            for ( Integer i = 0 ; i < numberOfWorkOrderId ; i++ ) {           
                Close_Work_Order__e woi = new Close_Work_Order__e(Work_Order_Task_Id__c=pierId);                              
                workOrderList.add(woi);            
            }
            return workOrderList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }
/***************************************************************************************
@Description : Method to create Integration Log 
****************************************************************************************/ 
    public static List<Integration_Log__c> createIntegrationLogs( String WorkFlowTmpName,string API_Type) {
        Integer numberOfIntegrationLog=1;
        Integer numOfCase=1;
        ID workOrderRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDER_RECORD_TYPE).getRecordTypeId();
        List<Case> woList = new List<Case>();
        try{
            woList=createCase(WorkFlowTmpName,numOfCase,workOrderRecordType);
            
            List<Integration_Log__c> intLogList = new List<Integration_Log__c>();
            for ( Integer i = 0 ; i < numberOfIntegrationLog ; i++ ) {           
                Integration_Log__c intLog = new Integration_Log__c( Work_Order__c=woList[0].id,API_Type__c= API_Type);                              
                intLogList.add(intLog);            
            }
            return intLogList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }  
    
     /***************************************************************************************
@Description : Method to create worklog for platform event
****************************************************************************************/ 
    public static List<WorkLog_Event__e> createWorklog( String notes) {
        Integer numberOfNotes=250;
        
            List<WorkLog_Event__e> worklogList = new List<WorkLog_Event__e>();
            for ( Integer i = 0 ; i < numberOfNotes ; i++ ) {           
                WorkLog_Event__e wle = new WorkLog_Event__e(WL_Description__c=notes);                              
                worklogList.add(wle);            
            }
            return worklogList;     
                   
    }                    

    /***************************************************************************************
    @Description : Method to create community user
    ****************************************************************************************/  
    public static User createUserByProfileName(String profileName){
        try {
            String nameToUse = profileName.replace(' ','');
            Profile profileRec = [Select Id from Profile where name = :profileName];
            User userRec = new User(); 
            userRec.ProfileId = profileRec.Id;   
            userRec.alias = 'standt';
            userRec.email = 'standarduser6@testorg.com';
            userRec.emailencodingkey = 'UTF-8';
            userRec.firstname = 'SITest';
            userRec.lastname = 'User';
            userRec.languagelocalekey = 'en_US';
            userRec.localesidkey = 'en_US';
            userRec.timezonesidkey = 'America/Los_Angeles';
            userRec.Email = nameToUse + '@sitestuser.com';
            userRec.username = nameToUse + '@sitestuser.com' + System.currentTimeMillis();
            return userRec;
        }   
        catch(Exception ex) {
            throw ex;
        }           
    }
    
    /***************************************************************************************
    @Description : Method to create Case data for test classes
    ****************************************************************************************/ 
    public static List<Case> createCaseSingleIntake(Integer numberOfCases, Id recType) {
       try {
            List<Case> caseList = new List<Case>();
            for (Integer i=0; i<numberOfCases; i++) {           
                Case cs = new Case();  
                cs.RecordTypeId = recType;
                cs.Status = Label.SI_CaseStatusDraft;
                caseList.add(cs);            
            }
            return caseList;     
        }            
        catch(Exception ex) {
            throw ex;  
        }             
    }
    
    /***************************************************************************************
    @Description : Method to create Intake Site 
    ****************************************************************************************/ 
    public static List<Intake_Site__c> createIntakeSiteSingleIntake(Integer numberOfIntakeSite ,String caseId, Id recType) {
        
        try{
            List<Intake_Site__c> intakeSiteList = new List<Intake_Site__c>();
            for ( Integer i = 0 ; i < numberOfIntakeSite ; i++ ) {           
                Intake_Site__c intReq = new Intake_Site__c(Case__c=caseId ,RecordTypeId=recType);                              
                intakeSiteList.add(intReq);            
            }
            return intakeSiteList;     
        }            
        catch(Exception ex){
            throw ex;  
        }                    
    }
 /***************************************************************************************
    @Description : Method to create Opportunity data for test classes
    ****************************************************************************************/ 
    public static List<Opportunity> createOpportunitySingleIntake(Integer numberOfOpportunity, Id recType) {
       try {
            List<Opportunity> oppList = new List<Opportunity>();
            for (Integer i=0; i<numberOfOpportunity; i++) {           
                Opportunity opp = new Opportunity();  
                opp.RecordTypeId = recType;
                opp.Name = 'Test'+i;
                opp.CloseDate = System.today()+30;
                opp.StageName = Label.SI_OppStageDefaultValue;
                oppList.add(opp);            
            }
            return oppList;     
        }            
        catch(Exception ex) {
            throw ex;  
        }             
    }
    
    /****************Method to create Contract Site ************/
     public static List<Contract_Site__c > createConractSiteSingleIntake(Integer numberOfContractSite,Id oppId) {
       try {
            List<Contract_Site__c > contractSiteList = new List<Contract_Site__c >();
            for (Integer i=0; i<numberOfContractSite; i++) {           
                Contract_Site__c  oppContract = new Contract_Site__c ();  
                oppContract.Opportunity__c = oppId;
                contractSiteList.add(oppContract);            
            }
            return contractSiteList;     
        }            
        catch(Exception ex) {
            throw ex;  
        }             
    }
    
    /****************Method to create Account ************/
     public static List<Account > createAccountSingleIntake(Integer numberOfAccounts) {
       try {
            List<Account> accountList = new List<Account>();
            for (Integer i=0; i<numberOfAccounts; i++) {           
                Account  accList = new Account();  
                accList.Name = 'Test'+i;
                accList.SSI_Vendor__c  = true;
                accountList.add(accList);            
            }
            return accountList;     
        }            
        catch(Exception ex) {
            throw ex;  
        }             
    }
    
    /****************Method to create Entitlement************/
     public static List<Entitlement> createEntitlementSingleIntake(Integer numberOfAccounts, Id accountId) {
       try {
            List<Entitlement> entitlementList = new List<Entitlement>();
            for (Integer i=0; i<numberOfAccounts; i++) {           
                Entitlement entiList = new Entitlement();  
                entiList.Name = 'Test'+i;
                entiList.AccountId = accountId;
                entitlementList.add(entiList);            
            }
            return entitlementList;     
        }            
        catch(Exception ex) {
            throw ex;  
        }             
    }
    /***************************************************************************************
@Description : Method to create Intake Site 
****************************************************************************************/
    public static List<SI_Savings_Tracker__c> createSISavingTrackerSingleIntake(Integer numberOfSISavingTrackers, Id recType ) {
       try{
        List<SI_Savings_Tracker__c> lstSISavingRecords = new List<SI_Savings_Tracker__c>();
        for(Integer i=0; i<numberOfSISavingTrackers; i++){
            SI_Savings_Tracker__c objSISavingTrackers = new SI_Savings_Tracker__c();
            objSISavingTrackers.name= 'Test' + i;
            objSISavingTrackers.Status__c='Draft';
            lstSISavingRecords.add(objSISavingTrackers);
        }
        return lstSISavingRecords;
       }
        catch(Exception ex) {
            throw ex;  
        }             
    }
      /***************************************************************************************
@Description : Method to create Contract
****************************************************************************************/
    public static List<Contract> createContract(Integer numberOfContracts, Id recType ) {
       try{
        List<Contract> lstContracts = new List<Contract>();
        for(Integer i=0; i<numberOfContracts; i++){
            Contract objContracts = new Contract();
            objContracts.name= 'Test' + i;
            objContracts.Status='Draft';
            lstContracts.add(objContracts);
        }
        return lstContracts;
       }
        catch(Exception ex) {
            throw ex;  
        }             
    }
    
    /***************************************************************************************
    @Description : Method to fetchRecordTypeId
    ****************************************************************************************/
    public static Id getRecordTypeIdByDeveloperName(String obj, String recDevName){
        Id recTypeId;
        if(String.isNotBlank(obj) && String.isNotBlank(recDevName)){
            recTypeId = Schema.getGlobalDescribe().get(obj).getDescribe()
                .getRecordTypeInfosByDeveloperName().get(recDevName).getRecordTypeId();
        }
        return recTypeId;
    }
/***************************************************************************************
@Description : Method to create Contact
****************************************************************************************/
    public static List<Contact> createContactSingleIntake(Integer numberOfContacts,Id accountId) {
       try{
        List<Contact> lstContracts = new List<Contact>();
        for(Integer i=0; i<numberOfContacts; i++){
            Contact objContact = new Contact();
            objContact.Lastname = 'Test' + i;
            objContact.AccountId = accountId;
            objContact.Email = 'test@test.com';
            lstContracts.add(objContact);
        }
        return lstContracts;
        }
        catch(Exception ex) {
            throw ex;  
        }
     }
/***************************************************************************************
@Description : Method to create Docusign
****************************************************************************************/
    public static List<dsfs__DocuSign_Status__c> createDocuSignStatusSingleIntake(Integer numberOfDocuSignStatus,Id accountId,Id OpprtunityId) {
       try{
        List<dsfs__DocuSign_Status__c> lstDocuSignStatus = new List<dsfs__DocuSign_Status__c>();
        for(Integer i=0; i<numberOfDocuSignStatus; i++){
            dsfs__DocuSign_Status__c objDocuSignStatus = new dsfs__DocuSign_Status__c();
            objDocuSignStatus.dsfs__DocuSign_Envelope_ID__c = '1001A123-1234-5678-1D84-F8D44652A382';
            objDocuSignStatus.dsfs__Company__c = accountId;
            objDocuSignStatus.dsfs__Opportunity__c = OpprtunityId;
            objDocuSignStatus.dsfs__Envelope_Status__c = System.Label.SI_Contract_Status;
            objDocuSignStatus.dsfs__Subject__c = 'Document for eSignature';
            objDocuSignStatus.dsfs__Completed_Date_Time__c = system.Now();
            lstDocuSignStatus.add(objDocuSignStatus);
        }
        return lstDocuSignStatus;
        }
        catch(Exception ex) {
            throw ex;  
        }     
    }
    
/***************************************************************************************
@Description : Method to create Project Records
****************************************************************************************/
    public static List<Project__c> createProjects(Integer numberOfProjects, String projectCode, Id locationId){
        try{
            List<Project__c> projectList = new List<Project__c>();
            for(Integer i=0;i<numberOfProjects;i++){
                Project__c proj = new Project__c(Name=projectCode + i, Location_ID__c = locationId);
                projectList.add(proj);
            }
            return projectList;
        } catch(Exception ex){
            throw ex;
        }
    }
    
/***************************************************************************************
@Description : Method to create Project Platform Event Instances
****************************************************************************************/
    public static List<ProjectId__e> createProjectEvents(Integer numberOfProjects, String projectCode, Id locationId){
        try{
            List<ProjectId__e> projectEventList = new List<ProjectId__e>();
            for(Integer i=0;i<numberOfProjects;i++){
                ProjectId__e proj = new ProjectId__e(Project_Code__c = projectCode, Location_ID__c = locationId);
                projectEventList.add(proj);
            }
            projectEventList.add(new ProjectId__e(Project_Code__c = projectCode, Location_ID__c = NULL));
            projectEventList.add(new ProjectId__e(Project_Code__c = projectCode, Location_ID__c = 'fakeID'));
            return projectEventList;
        } catch(Exception ex){
            throw ex;
        }
    }
    
    
    
    //************** Method to create  Lease_Enhancement__c  ****************************/
    public static Lease_Enhancement__c createLeaseEnhancement(){
        Lease_Enhancement__c leaseEnhancement = new Lease_Enhancement__c();
        leaseEnhancement.Current_Lease__c = 'Yes';
        leaseEnhancement.Current_Document_Location__c ='sample';
        return leaseEnhancement;
    }
    
    //************** Method to create  Lease_Enhancement__c  ****************************/
    public static Site_Lease_Escalation__c createSiteLEaseEscalation(String opportunityId){
        Site_Lease_Escalation__c siteLease = new Site_Lease_Escalation__c();
        siteLease.Site_Lease__c = opportunityId;
        siteLease.Effective_Date__c=System.today();
        return siteLease;
    }
       
       
       
       
       
       
       
}
/*********************************************************************************************
@author     : IBM
@date       : July 07 2020
@description: Scheduled class To execute the batch every 5 Seconds 
@Version    : 1.0           
**********************************************************************************************/
global class scheduledBatchableAtoms implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchForAtomNeustarIntergation AtomsCls = new BatchForAtomNeustarIntergation();
         for(integer i=0;i<60;i=i+Integer.valueOf(Label.AtomsBatchInterval)){
            string cron1 = '0 '+ i + ' * * * ?';
            string schName1 = 'Atoms Callouts Batch start--'+i+'---Min--';
            if(!Test.isRunningTest()) system.schedule(schName1, cron1, AtomsCls);
        }
    }
}
/*********************************************************************************************
@author       : IBM
@date         : 14.06.2019
@description  : This is the trigger handler to create locations. This is fired from Platform event LocationId.
@Version      : 1.0         
**********************************************************************************************/
Public with sharing class LocationTriggerHandler{
    
    /*********************************************************************************************
* @Description :Method to create he location records by listening to Platform event
**********************************************************************************************/       
    public static void createLocations(List<LocationId__e> newItems){
        List<LocationId__e> newLocationLst = (List<LocationId__e>) newItems;
        List<Location_ID__c> locationsToBeCreatedLst = new List<Location_ID__c>();
        // List<Location_ID__c> RinglocationsToBeCreatedLst = new List<Location_ID__c>();
        Map<String, Location_ID__c> locationIdMap = new Map<String, Location_ID__c>();
        Map<String, LocationID__e> newLocationMap = new Map<String, LocationID__e>();
        List<Integration_Log__c> logLstToInsert = new List<Integration_Log__c>();
        Set<Id> locationIdSet = new Set<Id>();
        
        Map<Id,Id> locationLogMap = new Map<Id,Id>();
        Set<String> switchIdNewSet = new Set<String>();
        Set<String> ringIdNewSet = new Set<String>();
        Map<String, Id> switchIdExistingSet = new Map<String, Id>();
        Map<String, Location_Id__c> ringIdExistingSet = new Map<String, Location_Id__c>();
        Map<String,String> ringIdForReason = new Map<String,String>();
        Map<String, Location_Id__c> ringIdForIntegration = new Map<String, Location_Id__c>();
        for (LocationId__e event : newItems){
            newLocationMap.put(event.Site_cd__c,event);
            if(event.Switch_Id__c != null && event.Switch_Id__c != '')
            {
                switchIdNewSet.add(event.Switch_Id__c);
            }
            if(event.Ring_cd__c != null && event.Ring_cd__c != '')
            {
                //Add the unique  Ring Id into a set
                
                ringIdNewSet.add(event.Ring_cd__c.toUpperCase());
            }
        }
        List<User> userListOwner=[SELECT Id FROM User WHERE Email =: System.Label.SI_Platform_Owner_Upd  AND ISACTIVE=TRUE AND PROFILE.NAME=: System.Label.SI_AdminProfile LIMIT 1];
        if(RingIdNewSet != Null && RingIdNewSet.Size() > 0){
            //Check how many Ring Id'S exist in Salesforce and put those in a map
            for(Location_ID__c  loc: [SELECT Location_ID__c,Id,AAV_Status__c,CMG_Working_Status__c FROM Location_ID__c WHERE Location_ID__c IN : ringIdNewSet]){
                ringIdExistingSet.put(loc.Location_ID__c,loc);
              ringIdForIntegration.put(loc.Location_ID__c.toUpperCase(),loc);
                
            }
            
        }
        
        if(newLocationMap != Null && newLocationMap.Size() > 0){
            for(Location_ID__c location : [SELECT Location_ID__c, Id,AAV_Status__c,AAV_Secondary_Contract_ID__c,CMG_Working_Status__c,Site_Qualified__c,Internally_Contracted__c,AAV_Primary_Contract_ID__c,Region__c, Market__c, Site_Status__c, Longitude__c, Latitude__c, Location_On_Air_Date__c, Location_Type_Description__c FROM Location_ID__c WHERE Location_ID__c IN : newLocationMap.keySet()]){
                locationIdMap.put(location.Location_ID__c, location);
                locationIdSet.add(location.Id);
            }
        }
        
        if(switchIdNewSet != Null && switchIdNewSet.Size() > 0){
            for(Switch_ID__c swtId : [SELECT Id, Switch_ID__c FROM Switch_ID__c WHERE Switch_ID__c IN : switchIdNewSet]){
                switchIdExistingSet.put(swtId.Switch_ID__c, swtId.Id);
            }
        }
        
        List<Switch_ID__c> swtIdLstToBeInserted = new List<Switch_ID__c>();
        for (String switchId : switchIdNewSet) {
            if(!switchIdExistingSet.containsKey(switchId)){
                swtIdLstToBeInserted.add(new Switch_ID__c(Name = switchId, Switch_ID__c = switchId));
            }
        }
        
        if(swtIdLstToBeInserted.Size() > 0 ){
            List<Database.SaveResult> updateResults = database.insert(swtIdLstToBeInserted, false);
            for(Integer i=0;i<updateResults.size();i++){ 
                if(updateResults.get(i).isSuccess()){
                    if(!switchIdExistingSet.ContainsKey(swtIdLstToBeInserted.get(i).Switch_ID__c)){
                        switchIdExistingSet.put(swtIdLstToBeInserted.get(i).Switch_ID__c, swtIdLstToBeInserted.get(i).Id);
                    }
                }
            }   
        }
        for (LocationId__e event : newLocationLst) {
            Location_ID__c locObj = new Location_ID__c();
            if(locationIdMap.ContainsKey(event.Site_cd__c)){
                locObj.id = locationIdMap.get(event.Site_cd__c).Id;
               
                locObj=getSiUpdatesForLocationUpdate(locationIdMap,event,locObj,ringIdExistingSet);
            }
            else{
                
                locObj=getSiUpdatesForLocationInsert(event,locObj,ringIdExistingSet);
            }
            if(event.Switch_ID__c == Null){
                locObj.Switch__c = Null;
            }
            else if(switchIdExistingSet.get(event.Switch_ID__c) != Null){
                locObj.Switch__c = switchIdExistingSet.get(event.Switch_ID__c);
            }
            
            
            locObj.Name = event.Site_cd__c;
            locObj.Location_ID__c = event.Site_cd__c;
            locObj.Region__c = event.Region_name__c;
            locObj.Market__c = event.site_mkt_name__c;
            locObj.Site_Status__c = event.Site_Status_Desc__c;
            locObj.Latitude__c = event.Site_Lat__c;
            locObj.Longitude__c = event.Site_Long__c;
            locObj.Location_On_Air_Date__c = event.Site_On_Air_Date__c;
            locObj.Location_Type_Description__c = event.Site_Type_Description__c;
            locObj.Street__c = event.Addr_Desc__c;
            locObj.City__c = event.City_Name__c;
            locObj.State__c = event.State_Cd__c;
            locObj.Zip_Code__c = event.Zip_Cd__c;
            if(event.RING_ID_RECORD_FLAG__c=='1')
            {
                locObj.IsRingRecord__c=True;  
            }
            else if(event.RING_ID_RECORD_FLAG__c=='0'){
                locObj.IsRingRecord__c=False; 
            }
            // locObj.Primary_Vendor_Name__c = event.Primary_Vendor_Name__c ;
            // locObj.Primary_Vendor_Id__c = event.Primary_Vendor_Id__c;
            // locObj.Primary_Contact_Id__c = event.Primary_Contact_Id__c;
            //  locObj.Secondary_Vendor_Name__c = event.Secondary_Vendor_Name__c;
            // locObj.Secondary_Vendor_Id__c = event.Secondary_Vendor_ID__c;
            // locObj.Secondary_Contact_Id__c = event.Secondary_Contact_Id__c;
            locObj.Location_Market_ID__c = event.Site_Mkt_Cd__c;
            locObj.Ring_Status__c = event.Ring_Status_Desc__c;
            locObj.Build_Status__c = event.Build_Status_Desc__c;
            locObj.Switch_ID__c = event.Switch_ID__c;
            //Added By Arpit Gupta
            locObj.Eligibility_Status__c=event.eligibility_Status__c;
            locObj.Eligibility_Comments__c=event.Eligibility_Comments__c;
            locObj.Ethernet_Installed__c=event.Ethernet_Installed__c;
            locObj.Backhaul_ETL__c=event.Backhaul_ETL__c;
            locObj.Site_Type__c=event.Site_type__c;
            locObj.Backhaul_CIR__c=event.Backhaul_CIR__c;
            locObj.POR__c=event.POR__c;
            locObj.Billable_Circuit__c=event.Billable_Circuit__c;
            locObj.Microwave_Recipient__c=event.Microwave_Recipient__c;
            locObj.Microwave_Donor__c=event.Microwave_Donor__c;
            locObj.Location_Coordinates__Latitude__s=event.Site_Lat__c;
            locObj.Location_Coordinates__Longitude__s=event.Site_Long__c;
            locObj.Refresh_Date__c= System.Now();
            locObj.County__c=event.County__c;
            locObj.POR_Status_Active__c=event.POR_Status_Active__c;
            locObj.NSD_Zoning_Approved_Date__c=event.NSD_Zoning_Approved_Date__c;
            locObj.NSD_Zoning_Approved_Status__c=event.NSD_Zoning_Approved_Status__c;
            locObj.NSD_BP_Approved_Date__c=event.NSD_BP_Approved_Date__c;
            locObj.NSD_BP_Approved_Status__c=event.NSD_BP_Approved_Status__c;
            locObj.NSD_Entitlement_Complete_Date__c=event.NSD_Entitlement_Complete_Date__c;
            locObj.NSD_Entitlement_Complete_Status__c=event.NSD_Entitlement_Complete_Status__c;
            locObj.NSD_Construction_Start_Date__c=event.NSD_Construction_Start_Date__c;
            locObj.NSD_Construction_Start_Status__c=event.NSD_Construction_Start_Status__c;
            locObj.NSD_Construction_Complete_Date__c=event.NSD_Construction_Complete_Date__c;
            locObj.NSD_Construction_Complete_Status__c=event.NSD_Construction_Complete_Status__c;
            locObj.Modernization_Plan__c=event.Modernization_Plan__c;
            locObj.Decom_Ring_associated__c=event.Decom_Ring_associated__c;
            locObj.Legacy_Sprint_ID__c=event.Legacy_Sprint_ID__c;
            locObj.ASide_per_Granite__c=event.ASide_per_Granite__c;
            locObj.Granite_Segment_Category__c=event.Granite_Segment_Category__c;
            locObj.CMG_AAV_confidence_level__c=event.CMG_AAV_confidence_level__c;
            locObj.CMG_Notes__c=event.CMG_Notes__c;
            locObj.CMG_Recommended_BH_Soln__c=event.CMG_Recommended_BH_Soln__c;
            locObj.NSD_Latidude__c=event.NSD_Latidude__c;
            locObj.NSD_Longitude__c=event.NSD_Longitude__c;
            locObj.NSD_Distance_mi_from_Site_Coordinates__c=event.NSD_Distance_mi_from_Site_Coordinates__c;
            locObj.Available_Aging_Details__c=event.Available_Aging_Details__c;
            locObj.SOURCE__c=event.SOURCE__c;
            locObj.Eligibility_Fail_Reason__c=event.Eligibility_Fail_Reason__c;
            //locObj.CMG_Comments__c=event.CMG_Comments__c;
            locObj.Naming_Convention_Site_Class__c=event.Naming_Convention_Site_Class__c;
            locObj.Legacy_Carrier__c=event.Legacy_Carrier__c;
            locObj.Backhual_Universe__c=event.Backhual_Universe__c;
            locObj.Retail_Site_status__c=event.Retail_Site_status__c;
            locObj.Retail_Site_Channel__c=event.Retail_Site_Channel__c;
            locObj.NDW_Primary_AAV_Deselect_Code__c=event.NDW_Primary_AAV_Deselect_Code__c;
            locObj.NDW_Primary_AAV_Deselect_Reason__c=event.NDW_Primary_AAV_Deselect_Reason__c;
            locObj.NDW_Primary_AAV_Ordering_Guide__c=event.NDW_Primary_AAV_Ordering_Guide__c;
            locObj.NDW_Primary_AAV_Status__c=event.NDW_Primary_AAV_Status__c;
            locObj.NDW_Primary_AAV_Vendor_Name__c=event.NDW_Primary_AAV_Vendor_Name__c;
            locObj.NDW_Secondary_AAV_Deselect_Code__c=event.NDW_Secondary_AAV_Deselect_Code__c;
            locObj.NDW_Secondary_AAV_Deselect_Reason__c=event.NDW_Secondary_AAV_Deselect_Reason__c;
            locObj.NDW_Secondary_AAV_Ordering_Guide__c=event.NDW_Secondary_AAV_Ordering_Guide__c;
            locObj.NDW_Secondary_AAV_Status__c=event.NDW_Secondary_AAV_Status__c;
            locObj.NDW_Secondary_AAV_Vendor_Name__c=event.NDW_Secondary_AAV_Vendor_Name__c;
            //Added By Aasheesh Goel
            locObj.company_code__c=event.company_code__c;
            locObj.cost_center__c=event.cost_center__c;
            locObj.Venue_Name__c=event.Venue_Name__c;
            locObj.MLA_Partner__c=event.MLA_Partner__c;
            locObj.Standardized_Venue_Classification__c=event.Venue_Classification__c;
            locObj.Microwave_POR_Flag__c=event.Microwave_POR_Flag__c;
            locObj.Secondary_AAV_ETL_Amount__c=event.Secondary_AAV_ETL_Amount__c;
            locObj.Primary_AAV_ETL_Amount__c=event.Primary_AAV_ETL_Amount__c;
            locObj.CURRENT_MRC__c=event.CURRENT_MRC__c;
            locObj.Decom_Site_as_Current_ETL__c=event.Decom_Site_as_Current_ETL__c;
            locObj.Location_type__c=event.Location_Type__c;
            locObj.Microwave_Status__c=event.Microwave_visibility_Status__c;
            locObj.SI_Microwave_Status__c=event.Microwave_visibility_Status__c;
            locObj.Ring_Description__c=event.Ring_Description__c;
            locObj.Site_Name__c=event.Site_Name__c;
            locObj.Structure_Owner__c=event.Structure_Owner__c;
            locObj.AAV_On_Net_Provider__c=event.AAV_On_Net_Provider__c;
            locObj.Sec_AAV_ETL_Remaining_Commitment_Months__c=event.Sec_AAV_ETL_Remaining_Commitment_Months__c;
            locObj.Prim_AAV_ETL_Remaining_Commitment_Months__c=event.Prim_AAV_ETL_Remaining_Commitment_Months__c;
            locObj.Secondary_AAV_ETL_Term_End_Date__c=event.Secondary_AAV_ETL_Term_End_Date__c;
            locObj.Primary_AAV_ETL_Term_End_Date__c=event.Primary_AAV_ETL_Term_End_Date__c;
            locObj.CURRENT_ACTIVE_VENDOR_NAME__c=event.CURRENT_ACTIVE_VENDOR_NAME__c;
            locObj.CURRENT_CIRCUIT_NAME_SAMPLE__c=event.CURRENT_CIRCUIT_NAME_SAMPLE__c;
            locObj.CURRENT_ACTIVE_CIRCUIT_COUNT__c=event.CURRENT_ACTIVE_CIRCUIT_COUNT__c;
            locObj.Distance_from_Switch_Miles__c=event.Distance_from_Switch_Miles__c;
            locObj.Microwave_NRC__c=event.Microwave_NRC__c;
            locObj.MLPPP_at_Site__c=event.MLPPP_at_Site__c;
            locObj.LATA__c=event.LATA__c;
            locObj.Serving_Wire_Center__c=event.Serving_Wire_Center__c;
            locObj.Fiber_Summary__c=event.Fiber_Summary__c;
            locObj.Lit_Building_Summary__c=event.Lit_Building_Summary__c;
            locObj.CableMSO__c=event.CableMSO__c;
            locObj.CLEC__c=event.CLEC__c;
            locObj.ILEC__c=event.ILEC__c;
            locObj.Census_Persons_3mi__c=event.Census_Persons_3mi__c;
            locObj.AAV_STATUS_AVAILABLE_DATE__c=event.AAV_STATUS_AVAILABLE_DATE__c;
            locObj.POR_Added_Date__c=event.POR_Added_Date__c;
            locObj.Number_of_DAS_Spokes__c=event.Number_of_DAS_Spokes__c;
            locObj.NDW_CMG_AAV_Vetting_Result__c=event.NDW_CMG_AAV_Vetting_Result__c;
            locObj.CMG_Vetting_Commit_Distance_Change_mi__c=event.CMG_Vetting_Commit_Distance_Change_mi__c;
            locObj.CMG_Vetting_Commitment_Longitude__c=event.CMG_Vetting_Commitment_Longitude__c;
            locObj.CMG_Vetting_Commitment_Latitude__c=event.CMG_Vetting_Commitment_Latitude__c;
            locObj.CMG_Vetting_Approval__c=event.CMG_Vetting_Approval__c;
            locObj.NSD_CMG_AAV_names__c=event.NSD_CMG_AAV_names__c;
            locObj.Ring_Related_Active_Contract__c=event.Ring_Related_Active_Contract__c;
            locObj.Previous_Selected_Vendor__c=event.Previous_Selected_Vendor__c;
            locObj.Location_Contracted_Status__c=event.Location_Contracted_Status__c;
            locObj.Decom_Site_AAV_Contract__c=event.Decom_Site_AAV_Contract__c;
            locObj.Decom_Site_AAV_Provider__c=event.Decom_Site_AAV_Provider__c;
            locObj.T_Builder_ID__c=event.T_Builder_ID__c;
            locObj.SAP__c=event.SAP__c;
            //locObj.AAV_Secondary_Contract_ID__c=event.AAV_Secondary_Contract_ID__c;
            locObj.Site_Class__c=event.Site_Class__c;
            locObj.Site_Qualified__c=event.Site_Qualified__c;
            locObj.Magenta_Build_Flag__c=event.Magenta_Build_Flag__c;
            locObj.X5GmmW_Flag__c=event.X5GmmW_Flag__c;
            locObj.B2B_Flag__c=event.B2B_Flag__c;
            locObj.L600_NSD_POR__c=event.L600_NSD_POR__c;
            locObj.Pico_Site__c=event.Pico_Site__c;
            locObj.OwnerId=userListOwner.get(0).Id;
            
            //Logic to Populate Ring Id in locaiton if the incoming ring Id exist in Salesforce
            if(String.isNotBlank(event.Ring_cd__c) && ringIdForIntegration.containskey(event.Ring_cd__c.toUpperCase())) {
                locObj.Ring_ID__c= ringIdForIntegration.get(event.Ring_cd__c.toUpperCase()).Id;  
            } else if(String.isNotBlank(event.Ring_cd__c)) {
                ringIdForReason.put(event.Site_cd__c, event.Ring_cd__c.toUpperCase());
            } else {
                ringIdForReason.put(event.Site_cd__c, event.Ring_cd__c);
            }
            locationsToBeCreatedLst.add(locObj);
          } 
        LocationIntegrationLogHandler.insertUpdateIntegrationLogRecords(locationsToBeCreatedLst,ringIdForReason);
     }
    /**************************************************************************
* @description - Method to update AAV status and CMG Working Status on Location updation
* @param setOppId - Location map(Exisitng),event and Location which we need to update 
* @return updated location record
**************************************************************************/   
    public static Location_ID__c  getSiUpdatesForLocationUpdate(Map<String, Location_ID__c> locationIdMethodMap,LocationId__e listEvent, Location_ID__c locList, Map<String,Location_Id__c> ringIdSetRingupd){
        
        
        if(ringIdSetRingupd.get(listEvent.ring_cd__c)!=NULL && listEvent.Eligibility_Status__c==System.Label.SI_Eligibile && listEvent.RING_ID_RECORD_FLAG__c=='0' && ringIdSetRingupd.get(listEvent.ring_cd__c).CMG_Working_Status__c==System.Label.SI_CMGContracted  ){ 
            locList.AAV_Status__c= System.Label.SI_AAVSelected; 
            locList.CMG_Working_Status__c=System.Label.SI_CMGLOIContractReady;
            
        }
        else if(ringIdSetRingupd.get(listEvent.ring_cd__c)!=NULL && listEvent.Eligibility_Status__c==System.Label.SI_InEligibile && listEvent.RING_ID_RECORD_FLAG__c=='0' &&  ringIdSetRingupd.get(listEvent.ring_cd__c).CMG_Working_Status__c==System.Label.SI_CMGContracted ){
            locList.AAV_Status__c= System.Label.SI_AAVSelected;
            locList.CMG_Working_Status__c=System.Label.SI_CMGLOIIneligible;
            
        }
        else if((locationIdMethodMap.get(listEvent.Site_cd__c).AAV_Status__c==System.Label.SI_AAVSelected) && listEvent.RING_ID_RECORD_FLAG__c=='1' && (locationIdMethodMap.get(listEvent.Site_cd__c).Site_Qualified__c==System.Label.SI_SiteQualifiedYes)  &&  (listEvent.Site_Qualified__c==System.Label.SI_SiteQualifiedNo)){
            //Logic to populate the CMG working status and AAV status
            locList.CMG_Working_Status__c=System.Label.SI_CMGConIanactive;  
        }
        if((listEvent.Eligibility_Status__c==System.Label.SI_Eligibile) && listEvent.RING_ID_RECORD_FLAG__c=='1' && (locationIdMethodMap.get(listEvent.Site_cd__c).AAV_Status__c!=System.Label.SI_AAVSelected)){
            locList.AAV_Status__c=System.Label.SI_AAVAvailable; 
        }
        if((listEvent.Eligibility_Status__c==System.Label.SI_InEligibile) && listEvent.RING_ID_RECORD_FLAG__c=='1' && (locationIdMethodMap.get(listEvent.Site_cd__c).AAV_Status__c!=System.Label.SI_AAVSelected)){
            locList.AAV_Status__c=System.Label.SI_AAVNotAvailable;
            if((locationIdMethodMap.get(listEvent.Site_cd__c).CMG_Working_Status__c!=System.Label.SI_CMGNotRequired) && listEvent.Ethernet_Installed__c==TRUE  && String.isBlank(locList.AAV_Secondary_Contract_ID__c)  && String.isBlank(locList.AAV_Primary_Contract_ID__c)){
                locList.CMG_Working_Status__c=System.Label.SI_CMGDelNotAvail;
            }
            else if((locationIdMethodMap.get(listEvent.Site_cd__c).CMG_Working_Status__c!=System.Label.SI_CMGNotRequired)  && listEvent.Ethernet_Installed__c==FALSE  && String.isBlank(locList.AAV_Secondary_Contract_ID__c)  && String.isBlank(locList.AAV_Primary_Contract_ID__c)){
                locList.CMG_Working_Status__c=System.Label.SI_CMGBAUNotAvail;
            }
        }
        
        return locList;
    }
    /**************************************************************************
* @description - Method to update AAV status and CMG Working Status on Location Insertion
* @param setOppId - Location map(Exisitng),event and Location which we need to update 
* @return updated location record
**************************************************************************/   
    public static Location_ID__c  getSiUpdatesForLocationInsert(LocationId__e listEvent, Location_ID__c locList, Map<String, Location_Id__c> ringIdSetRing){
        //Logic to populate the CMG working status and AAV status based on Ethernet_Installed__c,Eligibility_Status__c,AAV_Secondary_Contract_ID__c,AAV_Primary_Contract_ID__c
        
        if(ringIdSetRing.get(listEvent.ring_cd__c)!=NULL && listEvent.Eligibility_Status__c==System.Label.SI_Eligibile  &&  listEvent.RING_ID_RECORD_FLAG__c=='0' && ringIdSetRing.get(listEvent.ring_cd__c).CMG_Working_Status__c==System.Label.SI_CMGContracted ){ 
            locList.AAV_Status__c= System.Label.SI_AAVSelected;
            locList.CMG_Working_Status__c=System.Label.SI_CMGLOIContractReady;
            
            
        }
        else if(ringIdSetRing.get(listEvent.ring_cd__c)!=NULL && listEvent.Eligibility_Status__c==System.Label.SI_InEligibile  &&   listEvent.RING_ID_RECORD_FLAG__c=='0' && ringIdSetRing.get(listEvent.ring_cd__c).CMG_Working_Status__c==System.Label.SI_CMGContracted ){
            locList.AAV_Status__c= System.Label.SI_AAVSelected; 
            locList.CMG_Working_Status__c=System.Label.SI_CMGLOIIneligible;
            
        }
        else if(listEvent.Eligibility_Status__c==System.Label.SI_Eligibile && listEvent.RING_ID_RECORD_FLAG__c=='1'){
            locList.AAV_Status__c=System.Label.SI_AAVAvailable; 
        }
        else if(listEvent.Eligibility_Status__c==System.Label.SI_InEligibile && listEvent.RING_ID_RECORD_FLAG__c=='1'){
            locList.AAV_Status__c=System.Label.SI_AAVNotAvailable;
            if( listEvent.Ethernet_Installed__c==TRUE  && String.isBlank(locList.AAV_Secondary_Contract_ID__c)  && String.isBlank(locList.AAV_Primary_Contract_ID__c)){
                locList.CMG_Working_Status__c=System.Label.SI_CMGDelNotAvail;
            }
            else if(listEvent.Ethernet_Installed__c==FALSE && String.isBlank(locList.AAV_Secondary_Contract_ID__c)  && String.isBlank(locList.AAV_Primary_Contract_ID__c)){
                locList.CMG_Working_Status__c=System.Label.SI_CMGBAUNotAvail;
            }
        }
        
        return locList;
    }
    
}
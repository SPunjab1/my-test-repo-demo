public class CopySiteEscalationsCtrl {
   
    @AuraEnabled 
    public static void copyEscalationForOpportunity(String opportunityId){
        
        System.debug('--> Copy Escalations Called' + opportunityId);
        try{
            List<Site_Lease_Escalation__c> lstEscalations=[select id,name,Adustment_Index__c,Change_Type__c,
                                                      Current_Escalator_Text__c,Effective_Date__c,
                                                      Effective_Term__c,Escalation_Type__c,Escalator__c,
                                                      EscalatorDollar__c,Lease_Version__c,New_Amount__c,
                                                      Previous_Amount__c,Site_Lease__c from Site_Lease_Escalation__c where Site_Lease__c=:opportunityId order by Effective_Date__c asc];
            
            Opportunity CurrentOpportunity = [select id,New_Rent_Commencement_Date__c,New_Final_Expiration_New_Lease_Date__c,Initial_Term_NewLease__c
                                              from opportunity where id =: opportunityId];
            
            List<Site_Lease_Escalation__c>lstNewEscalations=new List<Site_lease_Escalation__c>();
            Integer yearIncremental=0;
            Integer currentYear=0;
            
            if(lstEscalations.size()>1){
                yearIncremental=lstEscalations.get(1).Effective_Date__c.Year()-lstEscalations.get(0).Effective_Date__c.Year();
                
                //currentYear=lstEscalations.get(lstEscalations.size()-1).Effective_Date__c.Year();
                //Start with First
                currentYear=lstEscalations.get(0).Effective_Date__c.Year();
            }else if(lstEscalations.size()>0){
                yearIncremental=1;
                currentYear=lstEscalations.get(0).Effective_Date__c.Year();
            }
            
            for(Site_Lease_Escalation__c objEscalations:lstEscalations){
                Site_Lease_Escalation__c siteEscalation=objEscalations.clone();
                siteEscalation.Lease_Version__c='New';
                siteEscalation.Previous_Amount__c=null;
                siteEscalation.New_Amount__c=null;
                
                //siteEscalation.Effective_Date__c= Date.newInstance(currentYear,objEscalations.Effective_Date__c.Month(),objEscalations.Effective_Date__c.Day());
                //currentYear=currentYear+yearIncremental;
                lstNewEscalations.add(siteEscalation);
            }
            if(lstNewEscalations.size()>0){
               insert lstNewEscalations;
            }
        }catch (Exception e)
        {
            throw new AuraHandledException(e.getMessage());
        }
        
        
        
        //PageReference opportunityPage=new PageReference('/'+opportunityId);
        //opportunityPage.setRedirect(true);
        //return opportunityPage;
    }
    
    /*
      
    
     * /
    
    
    
    
    /*
    String opportunityId{get;set;}
   
    public CopySiteEscalationsCtrl(ApexPages.StandardController ctrl){
        opportunityId=ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference copyEscalations(){
        List<Site_Lease_Escalation__c>lstEscalations=[select id,name,Adustment_Index__c,Change_Type__c,
                                                  Current_Escalator_Text__c,Effective_Date__c,
                                                      Effective_Term__c,Escalation_Type__c,Escalator__c,
                                                      EscalatorDollar__c,Lease_Version__c,New_Amount__c,
                                                      Previous_Amount__c,Site_Lease__c from Site_Lease_Escalation__c where Site_Lease__c=:opportunityId order by Effective_Date__c asc];
        List<Site_Lease_Escalation__c>lstNewEscalations=new List<Site_lease_Escalation__c>();
        Integer yearIncremental=0;
        Integer currentYear=0;
        if(lstEscalations.size()>1){
            yearIncremental=lstEscalations.get(1).Effective_Date__c.Year()-lstEscalations.get(0).Effective_Date__c.Year();
            currentYear=lstEscalations.get(lstEscalations.size()-1).Effective_Date__c.Year();
        }else if(lstEscalations.size()>0){
            yearIncremental=1;
            currentYear=lstEscalations.get(0).Effective_Date__c.Year();
        }
        for(Site_Lease_Escalation__c objEscalations:lstEscalations){
            Site_Lease_Escalation__c siteEscalation=objEscalations.clone();
            siteEscalation.Lease_Version__c='New';
            siteEscalation.Previous_Amount__c=null;
            siteEscalation.New_Amount__c=null;
            siteEscalation.Effective_Date__c= Date.newInstance(currentYear+yearIncremental,objEscalations.Effective_Date__c.Month(),objEscalations.Effective_Date__c.Day());
            currentYear=currentYear+yearIncremental;
            lstNewEscalations.add(siteEscalation);
        }
        if(lstNewEscalations.size()>0){
           insert lstNewEscalations;
        }
        PageReference opportunityPage=new PageReference('/'+opportunityId);
        opportunityPage.setRedirect(true);
        return opportunityPage;
    }

	*/
}
/*********************************************************************************************
@author       : IBM
@date         : 21.06.19 
@description  : This is the class for for bulk close the tasks from list view
@Version      : 1.0        
**********************************************************************************************/


public with sharing class CloseTaskFromListViewController{
    private ApexPages.StandardSetController standardController;
    public Set<String>  errorMsgSet { get; set; } 
    public List<Case> errorIdLst { get; set; }  
    public CloseTaskFromListViewController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
        errorMsgSet = new Set<String>();
        errorIdLst = new List<Case>();
    }
    public PageReference CloseTasks()
    {       
        List<Case> selectedCases = (List<Case>) standardController.getSelected();
        List<Case> closeTaskLst = new List<Case>();
        Set<Id> caseIdSet = new Set<Id>();
        if(selectedCases.size() == 0){
            return new ApexPages.Action('{!List}').invoke();
        }
        for(Case selCase : selectedCases){
            caseIdSet.add(selCase.Id);
        }
        if(caseIdSet.Size() > 0){
            for(Case cs : [SELECT Id,CaseNumber,Status from Case where Id IN: caseIdSet]){
                if(cs.Status != ConstantsUtility.CASE_STATUS_CLOSED ){
                    cs.status = ConstantsUtility.CASE_STATUS_CLOSED;
                    closeTaskLst.add(cs);
                }
            }
        }
        try{
            if(closeTaskLst != Null && closeTaskLst.Size() > 0){
                // Create a savepoint
                Savepoint sp = Database.setSavepoint();
                Database.SaveResult[] updateResults = Database.update(closeTaskLst,false);
                for(Integer i=0;i<updateResults.size();i++){
                    if(!updateResults.get(i).isSuccess()){
                         String errorMsg ='';
                         for(Database.Error error : updateResults.get(i).getErrors()){errorMsg += error.getMessage() +' ';}
                         errorMsgSet.add(errorMsg);
                         //errorIdLst.add(closeTaskLst.get(i).CaseNumber);
                         errorIdLst.add(closeTaskLst.get(i));   
                    }
                }
                if(errorIdLst!=null && errorIdLst.size()>0){
                    // Rollback to the previous null value
                    Database.rollback(sp);
                    return null;  
                }
            }
        }catch(Exception ex){}
        return new ApexPages.Action('{!List}').invoke();      
    }
    public PageReference back()
    {
        PageReference cancel = standardController.cancel();
        return cancel;
    }


}
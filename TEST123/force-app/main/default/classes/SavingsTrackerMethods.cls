/*********************************************************************************************
@author     : Matthew Elias
@date       : 13.08.20
@description: This contains methods concerning the Savings Tracker object to be run manually
@Version    : 1.0
**********************************************************************************************/
public class SavingsTrackerMethods {
    /*********************************************************************************************
* @Description : Method call to evaluate current state of ALL Req --> ST Attachment discrepency
* @input params : NONE
**********************************************************************************************/
    public static Integer evaluateAll(){
        System.debug('BEGIN GENERAL EVALUATION');
        
        Map<Id, Request__c> requestMap = new Map<Id,Request__c>([SELECT Id, Savings_Tracker_Count__c FROM Request__c WHERE Savings_Tracker_Count__c > 0]);
        System.debug('num specified requests: ' + requestMap.size());
        if(requestMap == NULL || requestMap.size() == 0) return 0;
        
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>(identifyMissing(requestMap));
        
        System.debug('END GENERAL EVALUATION');
        System.debug(cdlList);
        return cdlList.size();
    }
    
    /*********************************************************************************************
* @Description : Method call to evaluate current state of SPECIFIC Req --> ST Attachment discrepency
* @input params : NONE
**********************************************************************************************/
    public static Integer evaluateSpecific(Map<Id,Request__c> specificRequestMap){
        System.debug('BEGIN SPECIFIC EVALUATION');
        
        System.debug('num specified requests: ' + specificRequestMap.size());
        if(specificRequestMap == NULL || specificRequestMap.size() == 0) return 0;
        
        Map<Id, Request__c> requestMap = new Map<Id,Request__c>([SELECT Id FROM Request__c WHERE Savings_Tracker_Count__c > 0 AND Id IN :specificRequestMap.keySet()]);
        System.debug('num filtered requests: ' + requestMap.keySet().size());
        if(requestMap == NULL || requestMap.size() == 0) return 0;
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>(identifyMissing(requestMap));
        
        System.debug('END SPECIFIC EVALUATION');
        
        return cdlList.size();
    }
    
    /*********************************************************************************************
* @Description : Method call to copy Attachments to Savings Tracker from ALL Request
* @input params : NONE
**********************************************************************************************/
    public static Integer updateAll(){
        System.debug('BEGIN GENERAL UPDATE');
        
        Map<Id,Request__c> requestMap = new Map<Id,Request__c>([SELECT Id, Savings_Tracker_Count__c FROM Request__c WHERE Savings_Tracker_Count__c > 0]);
        System.debug('num specified requests: ' + requestMap.size());
        if(requestMap == NULL || requestMap.size() == 0) return 0;
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>(identifyMissing(requestMap));
        if(cdlList.size()>0 && cdlList!=null){ 
            try{
                insert cdlList;
            } catch(exception ex){
                throw ex;
            }
        }
        
        System.debug('END GENERAL UPDATE');
        return cdlList.size();
    }
    
    /*********************************************************************************************
* @Description : Method call to copy Attachments to Savings Tracker from SPECIFIED Request
* @input params : NONE
**********************************************************************************************/
    public static Integer updateSpecific(Map<Id,Request__c> specificRequestMap){
        System.debug('BEGIN SPECIFIC UPDATE');
        
        System.debug('num specified reqs: ' + specificRequestMap.size());
        if(specificRequestMap == NULL || specificRequestMap.size() == 0) return 0;
        
        Map<Id,Request__c> requestMap = new Map<Id,Request__c>([SELECT Id, Savings_Tracker_Count__c FROM Request__c WHERE Savings_Tracker_Count__c > 0 AND Id IN :specificRequestMap.keySet()]);
        System.debug('num filtered requests: ' + requestMap.size());
        if(requestMap == NULL || requestMap.size() == 0) return 0;
        
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>(identifyMissing(requestMap));
        if(cdlList.size()>0 && cdlList!=null){ 
            try{
                insert cdlList;
            } catch(exception ex){
                throw ex;
            }
        }
        
        System.debug('END SPECIFIC UPDATE');
        
        return cdlList.size();
    }
    
    /*********************************************************************************************
* @Description : Method call to Identify Missing Attachments on Savings Tracker from Request
* @input params : NONE
**********************************************************************************************/
    private static List<ContentDocumentLink> identifyMissing(Map<Id,Request__c> incomingRequestMap){
        System.debug('BEGIN IDENTIFICATION');
		
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        Map<Id, Request__c> requestMap = new Map<Id,Request__c>(incomingRequestMap);
        
        // query for CDLs on specified requests
        List<ContentDocumentLink> reqCDLs = new List<ContentDocumentLink>([SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ContentDocument.FileType, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :requestMap.keySet() AND ContentDocument.FileType != 'SNOTE']);
        System.debug('num req cdls: ' + reqCDLs.size());
        if(reqCDLs == NULL || reqCDLs.size() == 0) return cdlList;
        
        // extract list of Request IDs
        Set<Id> linkedEntityIdSet = new Set<Id>();
        for(ContentDocumentLink cdl : reqCDLs){
            linkedEntityIdSet.add(cdl.LinkedEntityId);
        }
        
        // query for corresponding STs
        Map<Id, Savings_Tracker__c> savingsTrackerMap = new Map<Id, Savings_Tracker__c>([SELECT Id, Request__c FROM Savings_Tracker__c WHERE Request__c IN :linkedEntityIdSet]);
        System.debug('num savings: ' + savingsTrackerMap.keySet().size());
        
        // create map from Request ID --> Savings Tracker ID
        Map<Id,Id> requestSavingsTrackerMap = new Map<Id,Id>();
        for(Id st : savingsTrackerMap.keySet()){
            requestSavingsTrackerMap.put(savingsTrackerMap.get(st).Request__c, st);
        }
        
        // query for CDLs on specified Savings Trackers
        List<ContentDocumentLink> stCDLs = new List<ContentDocumentLink>([SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ContentDocument.FileType, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :savingsTrackerMap.keySet() AND ContentDocument.FileType != 'SNOTE']);
        System.debug('num st cdls: ' + stCDLs.size());
        
        // create Map from ST ID --> Set of CD Titles
        Map<Id,Set<String>> STCDLMap = new Map<Id,Set<String>>();
        for(ContentDocumentLink cdl : stCDLs){
            if(!STCDLMap.containsKey(cdl.LinkedEntityId)){
                STCDLMap.put(cdl.LinkedEntityId, new Set<String>{cdl.ContentDocument.Title});
            } else{
                STCDLMap.get(cdl.LinkedEntityId).add(cdl.ContentDocument.Title);
            }
        }
        
        // loop through reqCDLs and identify those missing from STCDLMap
        for(ContentDocumentLink cdl : reqCDLs){
            if(!STCDLMap.containsKey(requestSavingsTrackerMap.get(cdl.LinkedEntityId))
               || !STCDLMap.get(requestSavingsTrackerMap.get(cdl.LinkedEntityId)).contains(cdl.ContentDocument.Title)){
                   ContentDocumentLink newCDL = new ContentDocumentLink(ContentDocumentId = cdl.ContentDocumentId,
                                                                        LinkedEntityId = requestSavingsTrackerMap.get(cdl.LinkedEntityId),
                                                                        ShareType = cdl.ShareType,
                                                                        Visibility = cdl.Visibility);
                   cdlList.add(newCDL);
               }
        }
        System.debug('num missing: ' + cdlList.size());
        
        System.debug('END IDENTIFICATION');
        
        return cdlList;
    }
}
@isTest
public class OppApprovalHistoryTriggerHandlerTest {
    
    @testSetup
    static void testDataSetup() {
        
        //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
        //SetupLocation
        List<Location_ID__c> locationList = TestDataFactory.createLocationId('TestSite1','TestSite1',1);
        Location_ID__c loc1 = locationList[0];
        loc1.Market__c ='HOUSTON TX';
        loc1.Region__c='SOUTH';
        Insert loc1;
        // Opportunity RecordType
        Id newsiteLeaseRecordType = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity','New_Site_Lease');
        Opportunity newsiteLease = TestDataFactory.createOpportunitySingleIntake(1, newsiteLeaseRecordType)[0];
        newsiteLease.StageName = 'New';  
        newsiteLease.AccountId = listAccount[0].Id;
        newsiteLease.Contract_Start_Date__c = System.today();
        newsiteLease.Market_s__c ='Houston TX';
        newsiteLease.Region__c ='SOUTH';
        newsiteLease.Term_mo__c = 1.00;
        newsiteLease.Landlord__c ='Crown Castle';
        insert newsiteLease;
    }
    
    @isTest static void approvalHistorySlaTest(){
        Test.startTest();
        Opportunity opportunityRec = [Select id,New_Rent__c,Initial_Term_NewLease__c,Remaining_Current_Term_Years__c,Market_s__c,Region__c,Landlord__c from Opportunity limit 1];
        
        Opportunity_Approval_History__c approval1 = new Opportunity_Approval_History__c();
        approval1.Action__c ='Approve';
        approval1.Approval_Stage__c='Submitter';
        approval1.Opportunity__c =opportunityRec.Id;
        approval1.Start_Date__c =System.now();
        approval1.End_Date__c = system.now().adddays(1);
        Insert approval1;
        
        approval1.End_Date__c = system.now().adddays(2);
        Update approval1;
        Test.stopTest(); 
    }

}
/***********HEADER*****************
Class: PierIIUtilitiesTest
Description: Test class for PierIIUtility Apex class.
Author: Balaji Bodicherla
Date: June 12 2020
**********************************/
@isTest
public class PierIIUtilitiesTest {
    /*************************************************************************************
* @Description :Test Method for finsh logic of batch :to update failure numer of records.
*************************************************************************************/ 
    static testMethod void testMethod2() 
    {
        List<Case> csLst = new List<Case>();
        List<Case> woTasksLst = new List<Case>();
        List<Worklog_Entry__C> lstWorkLog = New List<Worklog_Entry__C>();
        Set<Id> workOrderIds = New Set<Id>();
        Set<Id> workTaskIds = New Set<Id>();
        List<Intake_Request__C> intakeReqList = TestDataFactory.createIntakeRequest('TIFS NNI Re-home','Yes');
        Test.startTest();
        insert intakeReqList;
        
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();   
        String WorkflowTmpName ='AAV CIR Retest';
        Integer NumOfCases=2;
        csLst= TestDataFactory.createCase(WorkflowTmpName,NumOfCases,recTypeId);
        insert(csLst);
        for(case c:csLst){
            workOrderIds.add(c.Id);            
            List<case> woTasksLst1 = TestDataFactory.createCase1(WorkflowTmpName, 2,recTypeId,c.Id);
            for(case scase: woTasksLst1){
                //workTaskIds.add(scase.Id);
                woTasksLst.add(scase);
            }
        }
        insert(woTasksLst);
        for(case scase: woTasksLst){
            workTaskIds.add(scase.Id);
            Worklog_Entry__c wLog = New Worklog_Entry__c();
            wLog.Task__c = scase.Id;
            wLog.Comment__c = 'Test';
            lstWorkLog.add(wLog);
            
        ContentVersion cv = new ContentVersion();
        cv.Title = 'ABC'+ scase.Id;
        cv.PathOnClient = 'test';
        cv.ContentLocation = 'S';
        cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
		Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
        ContentDocumentLink cDLink = new ContentDocumentLink();
        cDLink.ContentDocumentId = conDocId;
        cDLink.LinkedEntityId = scase.Id;
        cDLink.ShareType = 'I';
        insert cDLink;
            
        }
        Insert(lstWorkLog);
        PierII_Utilities.pullTaskWorklogs(workTaskIds);
        PierII_Utilities.pullWorkOrderTasks(workOrderIds, false, false);
        PierII_Utilities.pullprepostTasks(workTaskIds, 'Predecessor');
        PierII_Utilities.pullprepostTasks(workTaskIds, 'following');
        PierII_Utilities.pullTaskDocs(workTaskIds);
        Test.stopTest();
    }
    
}
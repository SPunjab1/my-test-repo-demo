@isTest
private class SendtoAtomsHandlerClsTest {

    @isTest
    static void validateLoginUserProfile(){
        User sourcingUser = TestDataFactory.createSourcingUser();
        insert sourcingUser;

        system.runas(sourcingUser){
            Test.startTest();
                boolean validateProfile = SendtoAtomsHandlerCls.validateLoginUserProfile();
            Test.stopTest();

            System.assertEquals(validateProfile ,TRUE, 'method should return True');
        }
    }

    @isTest
    static void UpdateCaseStatus(){
        User sourcingUser = TestDataFactory.createSourcingUser();
        insert sourcingUser;

        system.runas(sourcingUser){
            String WorkflowTmpName ='AAV CIR Retest';
            Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();  
            List<Case> caseList = TestDataFactory.createCase(WorkflowTmpName,1,recTypeId); 
            Insert caseList; 

            Test.startTest();
                String message = SendtoAtomsHandlerCls.startAtomsIntegration(caseList[0].id);
            Test.stopTest();

            System.assertEquals(message ,'Success', 'Method should return Success');
        }

    }
}
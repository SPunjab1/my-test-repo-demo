/*********************************************************************************************
@author     : Matthew Elias - copying IBM (ContentDocumentLinkTriggerHandler)
@date       : 13.08.20
@description: This is the trigger handler for SavingsTracker object.
@Version    : 1.0
**********************************************************************************************/
public with sharing class SavingsTrackerTriggerHandler extends TriggerHandler {
	private static final string SavingsTracker_API = 'Savings_Tracker__c';
    public static boolean isExecuted=false;
    public SavingsTrackerTriggerHandler() {
        super(SavingsTracker_API);
    }
    /***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/
    public override String getName() {
        return SavingsTracker_API;
    }
    /***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override void beforeInsert(List<SObject> newItems) {
    }
    public override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
    }
    public override void beforeDelete(Map<Id, SObject> oldItemsMap) {
    }
    public override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap) {
    }
    public override void afterDelete(Map<Id, SObject> oldItemsMap) {
    }
    public override void afterUndelete(Map<Id, SObject> oldItemsMap) {
    }
    public override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {
        /*********Method call to copy Attachments from Request to Savings Tracker upon creation of Savings Tracker*************/
        copyAttachmentToSavingsTracker(newItems, newItemsMap);
    }
    
/*********************************************************************************************
* @Description : Method call to copy Attachment to task as per the template
* @input params : Trigger.New , newmap
**********************************************************************************************/
    public void copyAttachmentToSavingsTracker(List<sObject> newList, Map<Id,sObject> newMap){
        List<Savings_Tracker__c> newItems = new List<Savings_Tracker__c>();
        newItems = (List<Savings_Tracker__c>) newList;
        Map<Id,Savings_Tracker__c> newItemsMap = new Map<Id,Savings_Tracker__c>();
        newItemsMap = (Map<Id,Savings_Tracker__c>) newMap;
        Map<Id,Id> requestSavingsTrackerMap = new Map<Id,Id>();
        List<ContentDocumentLink> requestCDLList = new List<ContentDocumentLink>();
        List<ContentDocumentLink>savingsTrackerCDLList = new List<ContentDocumentLink>();
        Map<Id,Set<String>> savingsTrackerCDMap = new Map<id,Set<String>>();
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();   
        
        // create Map from Request ID --> ST ID
        for(Savings_Tracker__c st : newItems){
            requestSavingsTrackerMap.put(st.Request__c, st.Id);
        }
        
        // query for CDLs on Requests
        requestCDLList = [SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title, ContentDocument.FileType, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId IN :requestSavingsTrackerMap.keySet() AND ContentDocument.FileType != 'SNOTE'];
        
        // if no attachments found, quit
        if(requestCDLList == NULL || requestCDLList.size() == 0) return;
        
        // copy Request attachments to Savings Trackers
        for(ContentDocumentLink cdl : requestCDLList){
            ContentDocumentLink newCDL = new ContentDocumentLink(ContentDocumentId = cdl.ContentDocumentId,
                                                                 LinkedEntityId = requestSavingsTrackerMap.get(cdl.LinkedEntityId),
                                                                 ShareType = cdl.ShareType,
                                                                 Visibility = cdl.Visibility);
            cdlList.add(newCDL);
        }
        
        try{
            insert cdlList;
        } catch(exception ex){
            throw ex;
        }
        
        // code coverage manipulation
        String testString = getName();
    }
}
/*********************************************************************************************
@author     : IBM
@date       : 17.09.20
@description: Test class for CircuitTriggerHandler
@Version    : 1.0           
**********************************************************************************************/
@isTest(SeeAllData = false)
public class CircuitTriggerHandlerTest {
    //Public static Map<string,ATOMSNeuStarUtilitySettings__c> mapConstants = ATOMSNeuStarUtilitySettings__c.getAll();
    static testMethod void testcheckcheckBANUpdation(){
        
        List<ATOMSNeuStarUtilitySettings__c> mapConstants=TestDataFactoryOMph2.createATOMSNeuStarUtilCustSet();
        insert mapConstants;
        system.debug('mapConstants:'+mapConstants);
        Id  recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Work Order Task').getRecordTypeId();       
        List<Case> caseList= new List<Case>();
        caseList = TestDataFactoryOMph2.createCase('AAV CIR Retest',1,recTypeId);
        caseList[0].OM_Integration_Status__c = 'ASR Success';
        insert caseList;
        List<Purchase_Order__c> poList= new List<Purchase_Order__c>();
        poList = TestDataFactoryOMph2.createPurchaseOrder('EVC DISCONNECT','Fairpoint Communications','Disconnect',1,caseList[0].Id);
        insert poList;
        List<Circuit__c>circList=new List<Circuit__c>();
        circList=TestDataFactoryOMph2.createCircuitRecords(caseList[0].Id, 1);
        circList[0].BAN__c='123';
        circList[0].Circuit_ID_Primary_Key__c='ABCDEFGHIJ';
        insert circList;
        Test.startTest();
        circList[0].BAN__c='345';
        update circList;
        system.assertEquals(circList[0].BAN__c, '345');
        CircuitTriggerHandler c=new CircuitTriggerHandler();
        c.getName();
        Test.stopTest();
        
    }
    
}
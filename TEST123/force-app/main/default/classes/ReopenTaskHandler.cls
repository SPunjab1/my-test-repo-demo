/*********************************************************************************************
@author       : IBM
@date         : 21.06.19 
@description  : This is the class for Reopen task  lightning component
@Version      : 1.0        
**********************************************************************************************/
public class ReopenTaskHandler {
    public static boolean isReopenEnable;
    Public Static ID workOrderTaskRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ConstantsUtility.TMO_WORKORDERTASK_RECORD_TYPE).getRecordTypeId();
/***************************************************************************************************
* @Description  : Method to enable/disable reopen button based on certain conditions
* @input params : recordId
***************************************************************************************************/
    @AuraEnabled
    public static boolean findTaskNumber(List<Id> cIds){
        list<decimal> taskList = new list<decimal>();
        list <case> validTasks=new List<Case>();
        set<id>parentIds=new set<id>();
        decimal task=null;
        for(case c: [select task_number__c,parentId FROM Case WHERE Id IN:cIds and status='closed' and recordtypeid=:workOrderTaskRecordType and Executable_Task_in_PIER__c=true and Last_Task__c=false])
        {   task=c.Task_Number__c+1;
         taskList.add(task);
         parentIds.add(c.parentId);
        }
        if(taskList!= NULL && taskList.size()>0){
            for(case c:[select id ,status,task_number__c from case where task_number__c in:taskList and parentId in:parentIds and status!='closed'] ){
                validTasks.add(c);
            }
        }
        if(validTasks!=null && validTasks.size()>0){
            isReopenEnable = false;
        }
        else{
            isReopenEnable =true ;
        }
        return isReopenEnable;
    }
    
/***************************************************************************************************
* @Description  : Method to open Task onClick of Reopen Task button on Work Order Task
* @input params : recordId
***************************************************************************************************/
    @AuraEnabled
    public static String ReopenTaskNew(List<Id> cIds){
        List<Case> updateCases = new List<Case>();
        List<Case> taskList = new List<Case>();
        String success=null;
        taskList =[select Id, status,task_number__c  FROM Case WHERE Id IN:cIds and status='closed']; 
        if(taskList!=null && taskList.size()>0){
            for(Case cs : taskList)
            {
                cs.status = 'New' ;
                cs.Case_Reopen_Reason__c='Other';
                cs.Case_Reopen_Reason_Comments__c='Other';
                updateCases.add(cs);
            } 
        }
        try{
            if(updateCases!=null &&updateCases.size()>0){
                database.update(updateCases,true);
                success='true';
            }
        }
        catch(exception ex){
            throw ex;
        }
        return success;
    }
}
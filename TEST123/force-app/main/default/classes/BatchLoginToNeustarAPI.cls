/*********************************************************************************************
@author     : IBM
@date       : June 8 2020
@description: Batch class To get access token for Neustar
@Version    : 1.0           
**********************************************************************************************/
public class BatchLoginToNeustarAPI implements Database.Batchable<sObject>, Database.AllowsCallouts,Schedulable{
    public Database.QueryLocator start(Database.BatchableContext BC) {
        HTTPResponse res = NeustarLoginAPI.sendLoginRequest();
        System.debug('Response**********'+res);
        getAccessTokenFromResposne(res); 
        return Database.getQueryLocator('select Id from case Limit 1'); // dummy query as databse.querylocator should return something
    }
    public void execute(SchedulableContext SC) {
        Database.executeBatch(new BatchLoginToNeustarAPI(), 1);
    }
    public void getAccessTokenFromResposne(HttpResponse loginResponse){
        TMobilePh2LoginDetailsHierarchy__c authTokCustomSetting = [SELECT Id,Neustar_Access_Token__c
                                                                   FROM TMobilePh2LoginDetailsHierarchy__c];
        try{ 
            if(loginResponse.getStatusCode()==200){
                system.debug('Login sucess');
                String accessToken = loginResponse.getHeader('Authorization');
                if(accessToken.length() > 255){
                    authTokCustomSetting.Neustar_Access_Token__c = accessToken.substring(0,255);
                    if(accessToken.substring(255,500) != NULL){
                         authTokCustomSetting.Neustar_Access_Token2__c = accessToken.substring(255,500);  
                    }
                    if(accessToken.substring(500,710) != NULL){
                         authTokCustomSetting.Neustar_Access_Token3__c = accessToken.substring(500,710);  
                    }
                    if(accessToken.substring(710) != NULL){
                         authTokCustomSetting.Neustar_Access_Token4__c = accessToken.substring(710);  
                    }
                }
                else if(accessToken.length() <= 255){
                    authTokCustomSetting.Neustar_Access_Token__c = accessToken; 
                }
                authTokCustomSetting.Latest_Neustar_Login_Response__c = loginResponse.getBody();
                authTokCustomSetting.Latest_Neustar_Login_Status__c = 'Success';
                
            }
            else{
                system.debug('Login Failure');
                authTokCustomSetting.Latest_Neustar_Login_Response__c = loginResponse.getBody().Substring(0,255);
                authTokCustomSetting.Latest_Neustar_Login_Status__c = 'Failure';
            }
            if(authTokCustomSetting != null){
                Database.upsert(authTokCustomSetting); // upsert data into custom setting  
            }

        }
        catch(Exception ex){
            System.debug('Exception BatchLoginToNeustarAPI-getAccessTokenFromResposne'+ex);
        }
    }
    public void execute(Database.BatchableContext BC, List<TMobilePh2LoginDetailsHierarchy__c> scope) {
    }
    
    public void finish(Database.BatchableContext BC) {
    }
}
@istest
public class NLG_OpportunityBusinessProcessTest {
    
    @testSetup
    static void testDataSetup() {
        //Create Users
        List<User> usrList = new List<User>();
        User marketUser = TestDataFactory.createUser();
        marketUser.Role_NLG__c ='Market POC (NLG)';
        marketUser.Market_NLG__c ='HOUSTON TX';
        marketUser.Region_NLG__c ='SOUTH';
        marketUser.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(marketUser);
        
        User nationalMarketUser = TestDataFactory.createUser();
        nationalMarketUser.Role_NLG__c ='Market POC (NLG)';
        nationalMarketUser.MLA_NLG__c ='Crown Castle;ATC';
        nationalMarketUser.NLG_National_Team__c ='BTS/BTR Team; InBLD/DAS/Venue; NLRP/LO; National - ATC Closeout Team; Other';
        usrList.add(nationalMarketUser);
        
        User regionUser = TestDataFactory.createUser();
        regionUser.Role_NLG__c ='Regional POC (NLG)';
        regionUser.Market_NLG__c ='HOUSTON TX';
        regionUser.Region_NLG__c ='SOUTH';
        regionUser.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(regionUser);
        
        User nationalRegionUser = TestDataFactory.createUser();
        nationalRegionUser.Role_NLG__c ='Regional POC (NLG)';
        nationalRegionUser.MLA_NLG__c ='Crown Castle;ATC';
        nationalRegionUser.NLG_National_Team__c ='BTS/BTR Team; InBLD/DAS/Venue; NLRP/LO; National - ATC Closeout Team; Other';
        usrList.add(nationalRegionUser);
        
        User nlgReview = TestDataFactory.createUser();
        nlgReview.Role_NLG__c ='NLG Review';
        nlgReview.Market_NLG__c ='HOUSTON TX';
        nlgReview.Region_NLG__c ='SOUTH';
        nlgReview.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(nlgReview);
        
        User manager1 = TestDataFactory.createUser();
        manager1.Role_NLG__c ='NLG Manager1';
        manager1.Market_NLG__c ='HOUSTON TX';
        manager1.Region_NLG__c ='SOUTH';
        manager1.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(manager1);
        
        User manager2 = TestDataFactory.createUser();
        manager2.Role_NLG__c ='NLG Manager2';
        manager2.Market_NLG__c ='HOUSTON TX';
        manager2.Region_NLG__c ='SOUTH';
        manager2.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(manager2);
        
        User director1 = TestDataFactory.createUser();
        director1.Role_NLG__c ='NLG Director1';
        director1.Market_NLG__c ='HOUSTON TX';
        director1.Region_NLG__c ='SOUTH';
        director1.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(director1);
        
        User director2 = TestDataFactory.createUser();
        director2.Role_NLG__c ='NLG Director2';
        director2.Market_NLG__c ='HOUSTON TX';
        director2.Region_NLG__c ='SOUTH';
        director2.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(director2);
        
        User finance = TestDataFactory.createUser();
        finance.Role_NLG__c ='NLG Financial';
        finance.Market_NLG__c ='HOUSTON TX';
        finance.Region_NLG__c ='SOUTH';
        finance.MLA_NLG__c ='Crown Castle;ATC';
        usrList.add(finance);
        
        Insert usrList;
        //Intialize list of Account to insert records
        List<Account> listAccount = new List<Account>();
        
        Account objAccount = TestDataFactory.createAccountSingleIntake(1)[0]; 
        listAccount.add(objAccount);
        //insert Account
        insert listAccount;
        //SetupLocation
        List<Location_ID__c> locationList = TestDataFactory.createLocationId('TestSite1','TestSite1',1);
        Location_ID__c loc1 = locationList[0];
        loc1.Market__c ='HOUSTON TX';
        loc1.Region__c='SOUTH';
        Insert loc1;
        // Opportunity RecordType
        Id newsiteLeaseRecordType = TestDataFactory.getRecordTypeIdByDeveloperName('Opportunity','New_Site_Lease');
        Opportunity newsiteLease = TestDataFactory.createOpportunitySingleIntake(1, newsiteLeaseRecordType)[0];
        newsiteLease.StageName = 'New';  
        newsiteLease.AccountId = listAccount[0].Id;
        newsiteLease.Contract_Start_Date__c = System.today();
        newsiteLease.Market_s__c ='Houston TX';
        newsiteLease.Region__c ='SOUTH';
        newsiteLease.Term_mo__c = 1.00;
        newsiteLease.Landlord__c ='Crown Castle';
        newsiteLease.CLIQ_Sub_Type__c ='Lease';
        newsiteLease.Lease_or_AMD__c ='Lease';
        newsiteLease.Negotiating_Vendor__c='South Region';
        insert newsiteLease;
        List<Site_Lease_Escalation__c> siteLeaseList = new List<Site_Lease_Escalation__c>();
        Site_Lease_Escalation__c siteLease = TestDataFactory.createSiteLEaseEscalation(newsiteLease.Id);
        //lease.Lease_Intake__c = oppRecForContract.Id;
        siteLease.Lease_Version__c ='New';
        siteLease.Escalation_Type__c ='One Time';
        siteLease.Escalator__c=10000;
        siteLeaseList.add(siteLease);
        Site_Lease_Escalation__c siteLease1 = TestDataFactory.createSiteLEaseEscalation(newsiteLease.Id);
        //lease.Lease_Intake__c = oppRecForContract.Id;
        siteLease1.Lease_Version__c ='Current';
        siteLease1.Escalation_Type__c ='PCT';
        siteLease1.Escalator__c=3.0;
        siteLease1.Effective_Date__c =system.today().addMonths(12);
        
        siteLeaseList.add(siteLease1);
        insert siteLeaseList;
    }
    
    @istest static void populateOpportunityTeamTest(){
        Test.startTest();
        Opportunity opportunityRec = [Select id,New_Rent__c,Initial_Term_NewLease__c,Remaining_Current_Term_Years__c,Market_s__c,Region__c,Landlord__c,Negotiating_Vendor__c from Opportunity limit 1];
        opportunityRec.Negotiating_Vendor__c ='BTS/BTR Team';
        update opportunityRec;
        Test.stopTest();        
    }
    
}
public class RequestTriageMethods {
    //revised to add Sprint Triaging 20Aug2020 LTracey
    @InvocableMethod(label='Request Triage Logic' description='Contains the logic for routing incoming requests the the appropriate Sourcing Manager.')
    public static void routeRequests(List<Request__c> incomingRequests) {
        List<Request__c> requestList = new List<Request__c>([SELECT Id, Name,
                                                             What_is_being_sourced_or_contracted__c,
                                                             Services__c,
                                                             Sourcing_Project_Type__c,
                                                             Supplier_Name__c,
                                                             VP_Project_Owner__c,
                                                             Assigned_in_Triage__c,
                                                             Estimated_Spend_in_first_12_months__c,
                                                             Budget_Owner__c,
                                                             Requestor_Email__c,
                                                             Business_Unit__c,
                                                             OwnerId
                                                             FROM Request__c
                                                             WHERE Id IN :new Map<Id,Request__c>(incomingRequests).keySet()]);
        
        
        Id accentureID = '0052E00000Ko536QAB'; //Alexsandra Martinez
        Id specialistID = '0052E00000KqJDMQA3'; // ---this is now robert kottenbrock
        //string me = 'llamar.tracey@cognizant.com';
        list<sprint_triage__c> spr = new list<sprint_triage__c>([select id,Assigned_User__c,Functional_Group_BU__c,SrMgrSourcingDirector__c,What_s_being_sourced__c
                                                                       from sprint_triage__c]);
        

        Integer countUnassigned = 0;
        
        
        //Energy & Facilities callout
        List<String> callouts = new List<String>();
        callouts.add('Energy');
        callouts.add('Facilities');
        
        Set<ID> requestVPs = new Set<ID>();
        for (Request__c r : requestList) {
            if (!r.Assigned_in_Triage__c) {
                countUnassigned++;
                if (r.What_is_being_sourced_or_contracted__c != null && callouts.contains(r.What_is_being_sourced_or_contracted__c)) {
                    requestVPs.add(r.VP_Project_Owner__c);
                }
            }
        }
        requestVPs.remove(null);
        
        //create a map from VP ID to the set of their Alignment records
        Map<Id,Set<VP_Alignment__c>> vpAlignmentMap = new Map<Id,Set<VP_Alignment__c>>();
        Set<VP_Alignment__c> alSet = new Set<VP_Alignment__c>();
        
        //Get VP Alignments for Energy and Facilities callouts
        if (requestVPs.size() > 0) {
            for (VP_Alignment__c al : [SELECT Id, Name,
                                       VP__c,
                                       Director__c,
                                       All_Categories__c,
                                       All_Sourcing_Project_Types__c,
                                       Category__c,
                                       Sourcing_Project_Type__c,
                                       Sourcing_Manager__c,
                                       OOS_Alignment__c,
                                       Priority__C
                                       FROM VP_Alignment__c 
                                       WHERE VP__c IN :requestVPs AND Category__c IN :callouts
                                       ORDER BY Priority__c ASC]) {
                                           alSet = vpAlignmentMap.get(al.VP__c);
                                           if (alSet == null) alSet = new Set<VP_Alignment__c>();
                                           alSet.add(al);
                                           vpAlignmentMap.put(al.VP__c,alSet);
                                       }
            
            
            for (Request__c r : requestList) {
                if (!r.Assigned_in_Triage__c && callouts.contains(r.What_is_being_sourced_or_contracted__c)){
                    if (vpAlignmentMap.get(r.VP_Project_Owner__c) != null){
                        for (VP_Alignment__c al : vpAlignmentMap.get(r.VP_Project_Owner__c)) {
                            //Need to add Director check...
                            if ((r.What_is_being_sourced_or_contracted__c != null && al.Category__c.contains(r.What_is_being_sourced_or_contracted__c))
                                && (al.Director__c == null || r.Budget_Owner__c == al.Director__c)
                                && (al.All_Sourcing_Project_Types__c || (r.Sourcing_Project_Type__c != null && al.Sourcing_Project_Type__c.contains(r.Sourcing_Project_Type__c)))) {
                                    r.OwnerId = al.Sourcing_Manager__c;
                                    r.Assigned_in_Triage__c = true;
                                    countUnassigned--;
                                    break;
                                } 
                        }  
                    }
                    
                    if (!r.Assigned_in_Triage__c) { 
                // if spr contains r.requestor then do else kottenbrock
                if(r.Requestor_Email__c.contains('sprint.com')){
                    for(sprint_triage__C s:spr){
                        if(s.Functional_Group_BU__c==r.Business_Unit__c&&s.What_s_being_sourced__c.split(';').contains(r.What_is_being_sourced_or_contracted__c)){
                            r.OwnerId = s.Assigned_User__c;
                            countUnassigned--;
                        }
                    }
                }else{
                    r.OwnerId = specialistID;//////////////////////////////added Aug2020LlamarTracey////////////////////////////////////
                    r.Assigned_in_Triage__c = true;
                    countUnassigned--;
                    
                }
                
            }
                }
            }
            
        } //end OOS VPs Check
        
        if (countUnassigned == 0) {
            update requestList;
            return;
        }
        
        Set<ID> requestSuppliers = new Set<ID>();
        //pull list of (service types) sourcing project types and vps listed in incoming Requests
        for (Request__c r : requestList) {
            if (!r.Assigned_in_Triage__c) {
                requestSuppliers.add(r.Supplier_Name__c);
            }  
        }
        
        if (countUnassigned == 0) return;
        
        requestSuppliers.remove(null);
        
        //OOS Suppliers check
        if (requestSuppliers.size() > 0) {
            //Query for OOS Supplier Alignments and create a map from Supplier (Account) ID to the set of their Alignment records
            Map<Id,Set<OOS_Supplier_Alignment__c>> supplierAlignmentMap = new Map<Id,Set<OOS_Supplier_Alignment__c>>();
            Set<OOS_Supplier_Alignment__c> supSet = new Set<OOS_Supplier_Alignment__c>();
            for (OOS_Supplier_Alignment__c sup : [SELECT Id, Name,
                                                  Supplier__c,
                                                  All_Sourcing_Project_Types__c,
                                                  Sourcing_Project_Type__c,
                                                  VP__c,
                                                  Sourcing_Manager__c
                                                  FROM OOS_Supplier_Alignment__c 
                                                  WHERE Supplier__c IN :requestSuppliers
                                                  ORDER BY Priority__c ASC]) {
                                                      supSet = supplierAlignmentMap.get(sup.Supplier__c);
                                                      if (supSet == null) supSet = new Set<OOS_Supplier_Alignment__c>();
                                                      supSet.add(sup);
                                                      supplierAlignmentMap.put(sup.Supplier__c,supSet);
                                                  }        
            
            //if there are OOS Supplier Alignment records, loop through requests and match any alignments
            if (supplierAlignmentMap.size() > 0) {
                for (Request__c r : requestList) {
                    if (!r.Assigned_in_Triage__c && supplierAlignmentMap.get(r.Supplier_Name__c) != null){
                        for (OOS_Supplier_Alignment__c al : supplierAlignmentMap.get(r.Supplier_Name__c)) {
                            if ((al.All_Sourcing_Project_Types__c || (r.Sourcing_Project_Type__c != null && al.Sourcing_Project_Type__c.contains(r.Sourcing_Project_Type__c))) 
                                && (al.VP__c == null || al.VP__c == r.VP_Project_Owner__c)) {
                                    r.OwnerId = al.Sourcing_Manager__c;
                                    r.Assigned_in_Triage__c = true;
                                    countUnassigned--;
                                    break;
                                }
                        }
                    }
                }
            }
        } //end OOS suppliers check
        
        if (countUnassigned == 0) {
            update requestList;
            return;
        }
        
        requestVPs.clear();
        for (Request__c r : requestList) {
            if (!r.Assigned_in_Triage__c) requestVPs.add(r.VP_Project_Owner__c);
        }
        requestVPs.remove(null);
        
        /*
//create a map from VP ID to the set of their Alignment records
Map<Id,Set<VP_Alignment__c>> vpAlignmentMap = new Map<Id,Set<VP_Alignment__c>>();
Set<VP_Alignment__c> alSet = new Set<VP_Alignment__c>();
*/
        
        //OOS VPs Check
        if (requestVPs.size() > 0) {
            for (VP_Alignment__c al : [SELECT Id, Name,
                                       VP__c,
                                       Director__c,
                                       All_Categories__c,
                                       All_Sourcing_Project_Types__c,
                                       Category__c,
                                       Sourcing_Project_Type__c,
                                       Sourcing_Manager__c,
                                       OOS_Alignment__c,
                                       Priority__C
                                      FROM VP_Alignment__c 
                                       WHERE VP__c IN :requestVPs AND OOS_Alignment__c = TRUE
                                       ORDER BY Priority__c ASC]) {
                                           alSet = vpAlignmentMap.get(al.VP__c);
                                           if (alSet == null) alSet = new Set<VP_Alignment__c>();
                                           alSet.add(al);
                                           vpAlignmentMap.put(al.VP__c,alSet);
                                       }
            
            if (vpAlignmentMap.size() > 0) {
                for (Request__c r : requestList) {
                    if (!r.Assigned_in_Triage__c && vpAlignmentMap.get(r.VP_Project_Owner__c) != null){
                        for (VP_Alignment__c al : vpAlignmentMap.get(r.VP_Project_Owner__c)) {
                            //Need to add Director check...
                            if ((al.All_Categories__c || (r.What_is_being_sourced_or_contracted__c != null && al.Category__c.contains(r.What_is_being_sourced_or_contracted__c))) 
                                && (al.Director__c == null || r.Budget_Owner__c == al.Director__c)
                                && (al.All_Sourcing_Project_Types__c || (r.Sourcing_Project_Type__c != null && al.Sourcing_Project_Type__c.contains(r.Sourcing_Project_Type__c)))) {
                                    r.OwnerId = al.Sourcing_Manager__c;
                                    r.Assigned_in_Triage__c = true;
                                    countUnassigned--;
                                    break;
                                } 
                        }
                    }
                }
            }
        } //end OOS VPs Check
        
        if (countUnassigned == 0) {
            update requestList;
            return;
        }
        
        Set<String> requestCategories = new Set<String>();
        for (Request__c r : requestList) {
            if (!r.Assigned_in_Triage__c) requestCategories.add(r.What_is_being_sourced_or_contracted__c);
        }
        requestCategories.remove(null);
        
        //OOS Categories check
        Map<String, Id> oosCategoryMap = new Map<String, Id>();
        for (OOS_Category_Alignment__c al : [SELECT Id, Name,
                                             Sourcing_Manager__c
                                             FROM OOS_Category_Alignment__c
                                             WHERE Name IN :requestCategories]) {
                                                 oosCategoryMap.put(al.Name, al.Sourcing_Manager__c);
                                             }
        
        
        if (oosCategoryMap.size() > 0) {
            for (Request__c r : requestList) {
                if (!r.Assigned_in_Triage__c && oosCategoryMap.get(r.What_is_being_sourced_or_contracted__c) != null ){
                    r.OwnerId = oosCategoryMap.get(r.What_is_being_sourced_or_contracted__c);
                    r.Assigned_in_Triage__c = true;
                    countUnassigned--;
                }
            }
        }
        
        if (countUnassigned == 0) {
            update requestList;
            return;
        }
        
        requestVPs.clear();
        vpAlignmentMap.clear();
        Set<String> requestSourcingProjects = new Set<String>();
        for (Request__c r : requestList) {
            if (!r.Assigned_in_Triage__c) {
                requestVPs.add(r.VP_Project_Owner__c);
                requestSourcingProjects.add(r.Sourcing_Project_Type__c);
            }
        }
        requestVPs.remove(null);
        requestSourcingProjects.remove(null);
        
        //Query RTRs
        List<Request_Triage_Route__c> requestRoutes = new List<Request_Triage_Route__c>([SELECT Id, Name,
                                                                                         All_Categories__c,
                                                                                         All_Types_of_Service__c,
                                                                                         All_Sourcing_Project_Types__c,
                                                                                         Category__c,
                                                                                         Type_of_Service__c,
                                                                                         Sourcing_Project_Type__c, 
                                                                                         Specific_VP__c, 
                                                                                         End_Behavior__c, 
                                                                                         Sourcing_Manager__c,
                                                                                         Priority__c
                                                                                         FROM Request_Triage_Route__c
                                                                                         ORDER BY Priority__c ASC]);
        
        //Create a map from Sourcing Project Type (Name) to the threshold options
        Map<String,String> spendingThresholdMap = new Map<String,String>();
        if (requestSourcingProjects.size() > 0) {
            for (Accenture_Spending_Threshold__c ast : [SELECT Id, Name,
                                                        Spending_Threshold__c
                                                        FROM Accenture_Spending_Threshold__c
                                                        WHERE Name IN :requestSourcingProjects]) {
                                                            spendingThresholdMap.put(ast.Name,ast.Spending_Threshold__c);
                                                        } 
        }
        
        
        //re-Query for VP Alignments, now IN SCOPE
        if (requestVPs.size() > 0) {
            for (VP_Alignment__c al : [SELECT Id, Name,
                                       VP__c,
                                       Director__c,
                                       All_Categories__c,
                                       All_Sourcing_Project_Types__c,
                                       Category__c,
                                       Sourcing_Project_Type__c,
                                       Sourcing_Manager__c,
                                       OOS_Alignment__c,
                                       Priority__C
                                       FROM VP_Alignment__c 
                                       WHERE VP__c IN :requestVPs AND OOS_Alignment__c = FALSE
                                       ORDER BY Priority__c ASC]) {
                                           alSet = vpAlignmentMap.get(al.VP__c);
                                           if (alSet == null) alSet = new Set<VP_Alignment__c>();
                                           alSet.add(al);
                                           vpAlignmentMap.put(al.VP__c,alSet);
                                       }
        }
    
        
        //Loop through all incoming requests - Standard Traige (no OOS)
        for (Request__c r : requestList) {
            //only check requests that have not already been assigned through a previous triage process
            if (!r.Assigned_in_triage__c) {
                //loop through Request_Triage_Routes to find the first one that applies to the Request
                for (Request_Triage_Route__c rtr : requestRoutes) {
                    //check to see if RTR is appropriate - if so, try to assign it based on RTR End Behavior
                    if ((rtr.All_Categories__c || rtr.Category__c.contains(r.What_is_being_sourced_or_contracted__c))
                        && (rtr.All_Types_of_Service__c || (r.Services__c != null && rtr.Type_of_Service__c.contains(r.Services__c))) 
                        && (rtr.All_Sourcing_Project_Types__c || (r.Sourcing_Project_Type__c != null && rtr.Sourcing_Project_Type__c.contains(r.Sourcing_Project_Type__c))) 
                        && (rtr.Specific_VP__c == null || rtr.Specific_VP__c == r.VP_Project_Owner__c)) {
                            //if statement based on selected RTRs End Behavior
                            if (rtr.End_Behavior__c == 'Accenture') {
                                r.OwnerId = accentureID;
                                r.Assigned_in_Triage__c = true;
                                countUnassigned--;
                                break;
                            } else if (rtr.End_Behavior__c == 'Specialist') {
                                r.OwnerId = specialistID;
                                r.Assigned_in_Triage__c = true;
                                countUnassigned--;
                                break;
                            } else if (rtr.End_Behavior__c == 'Specific Sourcing Manager') {
                                r.OwnerId = rtr.Sourcing_Manager__c;
                                r.Assigned_in_Triage__c = true;
                                countUnassigned--;
                                break;
                            } else if (rtr.End_Behavior__c == 'Obey Thresholds') {
                                //check if there is a threshold for the current request. and if so, check if it falls
                                if (spendingThresholdMap.get(r.Sourcing_Project_Type__c) != null && r.Estimated_Spend_in_first_12_months__c != null 
                                    && spendingThresholdMap.get(r.Sourcing_Project_Type__c).contains(r.Estimated_Spend_in_first_12_months__c)) {
                                        r.OwnerId = accentureID;
                                        r.Assigned_in_Triage__c = true;
                                        countUnassigned--;
                                        break;
                                    }
                            } //end if initial end behavior
                            //if the request hasn't been assigned yet:
                            //the End Behavior is "Obey Thresholds" and the request didn't trip any thresholds
                            //OR End Behavior is "VP Alignment"
                            //Either way, they follow VP Alignment now
                            if (!r.Assigned_in_Triage__c && vpAlignmentMap.get(r.VP_Project_Owner__c) != null) {
                                //loop through set of VP Alignments that are associated with the VP of the Request
                                for (VP_Alignment__c al : vpAlignmentMap.get(r.VP_Project_Owner__c)) {
                                    //check if VP Alignment fits the request
                                    //NEED TO ADD DIRECTOR ONCE THAT IS IMPLEMENTED
                                    if ((al.All_Categories__c || (r.What_is_being_sourced_or_contracted__c != null && al.Category__c.contains(r.What_is_being_sourced_or_contracted__c))) 
                                        && (al.Director__c == null || r.Budget_Owner__c == al.Director__c)
                                        && (al.All_Sourcing_Project_Types__c || (r.Sourcing_Project_Type__c != null && al.Sourcing_Project_Type__c.contains(r.Sourcing_Project_Type__c)))) {
                                            r.OwnerId = al.Sourcing_Manager__c;
                                            r.Assigned_in_Triage__c = true;
                                            countUnassigned--;
                                            break; //breaks from vp alignment check loop, not RTR loop
                                        }
                                } //end loop vp alignment check
                            } //end if threshold - vp alignment
                        } //end if check correct RTR
                    //semi-redundant check to make sure that a request that has already been placed is not subjected to more RTRs
                    if (r.Assigned_in_Triage__c) {
                        break;
                    }
                } //end loop through RTRs
            } //end if not already assigned
            //edited to add condition for new sprint managers
            
            if (!r.Assigned_in_Triage__c) { 
                // if spr contains r.requestor then do else kottenbrock
                if(r.Requestor_Email__c.contains('sprint.com')){
                    for(sprint_triage__C s:spr){
                        if(s.Functional_Group_BU__c==r.Business_Unit__c&&s.What_s_being_sourced__c.split(';').contains(r.What_is_being_sourced_or_contracted__c)){
                            r.OwnerId = s.Assigned_User__c;
                            countUnassigned--;
                        }
                    }
                }else{
                    r.OwnerId = specialistID;//////////////////////////////added Aug2020LlamarTracey////////////////////////////////////
                    r.Assigned_in_Triage__c = true;
                    countUnassigned--;
                    
                }
                
            }

               
            
           
        } //end loop through requests
        
        update requestList;
    }  
}
/*********************************************************************************************************************
**********************************************************************************************************************    
*  @Class            : TerminateWorkOrderController
*  @Author           : IBM
*  @Version History  : 1.0
*  @Creation         : 26.07.2019
*  @Description      : Apex class of Terminate Wotrk Order lightning component
**********************************************************************************************************************
**********************************************************************************************************************/
public with sharing class TerminateWorkOrderController {
    
/***************************************************************************************
* @Description :Method to check WOrk order satus is Terminated or not
* @input params :workOrderId
***************************************************************************************/  
    @AuraEnabled
    public static Boolean checkWOstatus(String workOrderId)
    {
        Boolean flag = false;
        if(workOrderId!=null){
            Case workOrder = [Select id,status from Case where Id =: workOrderId limit 1];
            if(workOrder!=null && workOrder.Status == 'Terminated'){
                flag = true;        
            }
        }
        return flag;
    }
/***************************************************************************************
* @Description :Method to terminate work order and terminate related related work order task
* @input params :workOrderId
***************************************************************************************/  
    @AuraEnabled
    public static void terminateCurrentWorkOrder(String workOrderId)
    {
        List<Case> updateLst = new List<Case>();
        if(workOrderId!=null){
            Case wo =new Case(Id = workOrderId,status ='Terminated',Terminate_Reason_Notes__c = 'Terminate in SFDC',Terminate_Reason__c = 'invalid_cr');
            updateLst.add(wo);
            List<Case> taskLst = [Select id,status from Case where Parentid =: workOrderId];
            if(taskLst!=null && taskLst.size()>0){
                for(Case tsk : taskLst){
                    if(tsk.status!='Closed'){
                    	Case cs = new Case(Id = tsk.Id,status ='Terminated');
                    	updateLst.add(cs);    
                    }
                }  
            }
            if(updateLst!=null && updateLst.size()>0){
                try{
                    Database.update(updateLst,true);    
                }
                catch(Exception ex){throw ex;}
            }
        }
    }
}
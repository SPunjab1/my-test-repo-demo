@isTest
Global class PierIntegrationMock implements HttpCalloutMock {
    Public Integer status;
    Public String body;
    
    public PierIntegrationMock(Integer status, String body) {
        this.status = status;
        this.body = body;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(body);
        res.setStatusCode(status);
        return res;
    }
}
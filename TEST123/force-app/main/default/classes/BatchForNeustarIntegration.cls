/*********************************************************************************************
@author     : IBM
@date       : July 8 2020
@description: Batch class to implement callouts for Neustar
@Version    : 1.0  
**********************************************************************************************/
public class BatchForNeustarIntegration implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{ 
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        Id thisJobId;
        Id thisClassId = Label.NeustarBatchID; 
        if(!Test.isRunningTest())
            thisJobId = bc.getJobId();
        List<String> jobStatus = Label.AtomsJobStatus.Split(',');
        AsyncApexJob[] jobs = [select id from AsyncApexJob where id!=:thisJobId AND status IN: jobStatus AND ApexClassId=:thisClassId AND JobType='BatchApex'];
        if (jobs !=null && jobs.size() > 0) {
            query = 'Select Id from Case LIMIT 0';
        }
        else {
            integer lmt = integer.ValueOf(Label.POQueryLimit);
            //String query = 'SELECT Id,OM2_Status__c,OM_Integration_Status__c,ContactId,OwnerId,(SELECT ACNA__c,Activity__c,ACTL__c,Address_Line_1__c,API_Status__c,ASC_EC__c,BAN__c,BDW_UNI1_LEVEL1__c,BDW_UNI1_LEVEL2__c,BDW_UNI2_LEVEL1__c,BDW_UNI2_LEVEL2__c,BDW_UNI3_LEVEL1__c,BDW_UNI3_LEVEL2__c,CCNA__c,City__c,CreatedById,CreatedDate,CUST__c,DDD__c,D_TSENT__c,ECCKT__c,Email__c,End_User_ID__c,ESP__c,EVCCKR__c,EVCID__c,EVCI__c,EVC_NUM__c,Existing_End_User__c,FUSF__c,ICSC_ASC_EC__c,ICSC_OEC__c,ICSC__c,Id,IsDeleted,JS__c,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,LOSACT_UNI1_LEVEL1__c,LOSACT_UNI1_LEVEL2__c,LOSACT_UNI2_LEVEL1__c,LOSACT_UNI2_LEVEL2__c,LOSACT_UNI3_LEVEL1__c,LOSACT_UNI3_LEVEL2__c,LOS_UNI1_LEVEL1__c,LOS_UNI1_LEVEL2__c,LOS_UNI2_LEVEL1__c,LOS_UNI2_LEVEL2__c,LOS_UNI3_LEVEL1__c,LOS_UNI3_LEVEL2__c,LREF_UNI1_LEVEL1__c,LREF_UNI1_LEVEL2__c,LREF_UNI2_LEVEL1__c,LREF_UNI2_LEVEL2__c,LREF_UNI3_LEVEL1__c,LREF_UNI3_LEVEL2__c,Name,NCI_UNI1__c,NCI_UNI2__c,NCI_UNI3__c,NC__c,Neustar_Link__c,New_End_User__c,Number_of_ASRs_with_Delivered_Complete__c,Number_of_ASRs_with_FOC_Complete__c,Number_of_ASRs__c,NUT__c,OwnerId,PIU__c,PI__c,PON__c,PRILOC__c,Product_Name__c,QSA__c,QTY__c,Redirect_Hyperlink__c,REQTYP__c,Response__c,RTR__c,RUID_UNI1__c,RUID_UNI2__c,RUID_UNI3__c,S25__c,SEI__c,Site_ID__c,State__c,SystemModstamp,Trading_Partner__c,UACT_UNI1__c,UACT_UNI2__c,UACT_UNI3__c,UREF_UNI1__c,UREF_UNI2__c,UREF_UNI3__c,Work_Order_Task__c,Work_Order__c,ZipCode__c  from Purchase_Orders__r) from Case WHERE OM_Integration_Status__c = \'Start Neustar Integration\' AND RecordType.DeveloperName = \'Work_Order_Task\'';
            //String query = 'SELECT Id,OM_Integration_Status__c,ContactId,OwnerId,(SELECT API_Status__c,Work_Order_Task__c,Work_Order__c,DDD__c,PON__c,Product_Name__c,Trading_Partner__c,Activity__c from Purchase_Orders__r) from Case WHERE OM_Integration_Status__c = \'Start Neustar Integration\' AND RecordType.DeveloperName = \'Work_Order_Task\'';
            query = 'SELECT Id,OM_Integration_Status__c,ContactId,OwnerId,(SELECT Number_of_ASRs__c,Number_of_Successful_ASR__c,API_Status__c,Work_Order_Task__c,Work_Order__c,DDD__c,PON__c,Product_Name__c,Trading_Partner__c,Activity__c from Purchase_Orders__r LIMIT :lmt) from Case WHERE OM_Integration_Status__c IN (\'ASR Success\',\'ASR Partial Success\') AND RecordType.DeveloperName = \'Work_Order_Task\'';
            System.debug('query >>>>>>>>'+query);
        }
        return Database.getQueryLocator(query);
    }
    public void execute(SchedulableContext bc) {
        if(!Test.isRunningTest()) {
            Database.executeBatch(new BatchForNeustarIntegration(), integer.valueOf(Label.NeustarBatchSize));
        }
    }
    
    public void execute(Database.BatchableContext bc, List<Case> scope) {
        List<Case> caseLst = new List<Case>();
        for(Case cs : scope){
            caseLst.add(cs);
        }
        NeustarIntegrationAPIHelper.ReturnParamsToBatchAfterCallout dmlActionsWrapper;
        if(caseLst!=null && caseLst.size()>0) {
            dmlActionsWrapper = NeustarIntegrationAPI.executeAPIs(caseLst,'CreateOrder',null);
        }
        //Call DML Methods
        system.debug('dmlActionsWrapper===>'+dmlActionsWrapper);
        if(dmlActionsWrapper != NULL){
            NeustarIntegrationAPIHelper.performCreatePOrderDMLs(dmlActionsWrapper);
        }
        NeustarIntegrationAPIHelper.updateCaseAPIStatus(caseLst);
    }
    public void finish(Database.BatchableContext bc){
        
    }
}
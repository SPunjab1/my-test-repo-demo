/*********************************************************************************************
@author     : IBM
@date       : 09.06.19 
@description: This is the trigger handler for Integration Log object.
@Version    : 1.0
Author	version Date			Description
BalajiB   2.0   June 23 2020	Added code to support retry mechanism for create service/change request.
**********************************************************************************************/
public with sharing class IntegrationLogTriggerHandler extends TriggerHandler {
    
    private static final string INTEGRATION_LOG_API = 'Integration_Log__c';     
    
    public IntegrationLogTriggerHandler() {
        super(INTEGRATION_LOG_API);
    }
/***************************************************************************************
@Description : Return the name of the handler invoked
****************************************************************************************/    
    public override String getName() {    
        return INTEGRATION_LOG_API;
    }
/***************************************************************************************
@Description : Trigger handlers for events
****************************************************************************************/
    public override  void beforeInsert(List<SObject> newItems){        
        
    }    
    public  override void beforeUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap){
        
    }    
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap){
            
    }     
    public  override void afterUpdate(List<SObject> newItems, Map<Id, SObject> newItemsMap, Map < Id, SObject > oldItemsMap){        
        List<Integration_Log__c> newIntegrationLogs = (List<Integration_Log__c>) newItems;
        Map<Id,Integration_Log__c> oldIntegrationLogMap = (Map<Id,Integration_Log__c>) oldItemsMap;        
        //callGetTasks(newIntegrationLogs,oldIntegrationLogMap); //Commented by Sunil - No longer needed for PIER - for first 6 templates
        reprocessRequests(newIntegrationLogs, oldIntegrationLogMap);
        checkUpdateTaskAPIProcessing(newItems, newItemsMap, oldItemsMap);    
        //Added by Kirti
        UpdateTaskForApproverAPI(newItems, newItemsMap, oldItemsMap);    
        checkApproverTaskAPIProcessing(newItems, newItemsMap, oldItemsMap);
    }    
    public  override void afterDelete(Map<Id, SObject> oldItemsMap){
        
    }    
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
        
    }    
    public  override void afterInsert(List<Sobject> newItems, Map<Id, Sobject> newItemsMap) {        
        List<Integration_Log__c> integrationLogs = (List<Integration_Log__c>) newItems;
        callPierIntegrationAPI(integrationLogs);        
    }  
           
/*********************************************************************************************
* @Description :Method to call PIER API
* @input params : Trigger.New ,newmap
**********************************************************************************************/ 

    public void callPierIntegrationAPI(List<Integration_Log__c> integrationLogs){
        List<Id> createWorkOrderIds = new List<Id>();
        List<Id> createChangeRequestIds = new List<Id>();
        List<Id> createServiceRequestIds = new List<Id>();
        List<Id> createWorkflowIds = new List<Id>();
        List<Id> updateTaskIds = new List<Id>();
        List<Id> updateWOIds = new List<Id>();
        List<Id> closeTaskIds = new List<Id>();
        List<Id> assignAndUpdateTaskIds = new List<Id>();
        List<Id> createAdhocTaskId = new List<Id>();
        List<Id> approveTasksId = new List<Id>();
        List<Id> attachmentTasksId = new List<Id>();
        List<Id> terminateWoId = new List<Id>();
        List<Id> NaTasksId = new List<Id>();
        List<Id> taskWorklogId = new List<Id>();
        List<Id> updateWOlogId = new List<Id>();
        List<Id> taskInProgressId = new List<Id>();
        
        for(Integration_Log__c item : integrationLogs){
            switch on item.API_Type__c {
                when 'Create Work Order' {
                    createWorkOrderIds.add(item.id);
                }when 'Create Change Request' {
                    createChangeRequestIds.add(item.id);
                }when 'Create Service Request' {
                    createServiceRequestIds.add(item.id);
                }when 'Create Workflow' {
                    createWorkflowIds.add(item.id);
                } when 'Update Tasks' {
                    updateTaskIds.add(item.id);
                } when 'Open Work Order','Closed Work Order','Update Work Order'{ //Added by PK
                    updateWOIds.add(item.id);
                } when 'Close Task' {
                    closeTaskIds.add(item.id);
                } when 'Assign And Schedule Task' {
                    assignAndUpdateTaskIds.add(item.id);
                } when 'Create Adhoc Task' {
                    createAdhocTaskId.add(item.id);
                } when 'Approve Task' {
                    approveTasksId.add(item.id);
                } when 'Attachment' {  //Added by PK
                    attachmentTasksId.add(item.id);
                } when 'Terminate Work Order' {  
                    terminateWoId.add(item.id);
                }when 'NA Task' {  
                    NaTasksId.add(item.id);
                }when 'Task Worklog' {  
                    taskWorklogId.add(item.id);
                }when 'Task In Progress' {  
                    taskInProgressId.add(item.id);
                }
            }
        }
        if(createChangeRequestIds!=null && createChangeRequestIds.size()>0) {
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(createChangeRequestIds,'CreateChangeRequest');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(createServiceRequestIds!=null && createServiceRequestIds.size()>0) {
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(createServiceRequestIds,'CreateServiceRequest');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(createWorkflowIds!=null && createWorkflowIds.size()>0) {
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(createWorkflowIds,'CreateWorkflow');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(updateWOIds!=null && updateWOIds.size()>0) {
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(updateWOIds,'UpdateWorkOrder');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        
        if(createWorkOrderIds!=null && createWorkOrderIds.size()>0) {
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(createWorkOrderIds,'CreateWorkOrder');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }        
        if(updateTaskIds!=null && updateTaskIds.size()>0) {
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(updateTaskIds,'UpdateTasks');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(closeTaskIds!=null && closeTaskIds.size()>0) {  //Added by PK
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(closeTaskIds,'CloseTask');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(assignAndUpdateTaskIds!=null && assignAndUpdateTaskIds.size()>0) {  //Added by Sandhya
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(assignAndUpdateTaskIds,'AssignScheduleTasks');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(createAdhocTaskId!=null && createAdhocTaskId.size()>0) {  //Added by PK
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(createAdhocTaskId,'createAdhocTask');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(approveTasksId!=null && approveTasksId.size()>0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(approveTasksId,'ApprovalTasks');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(attachmentTasksId!=null && attachmentTasksId.size()>0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(attachmentTasksId,'Attachment');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(terminateWoId!=null && terminateWoId.size()>0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(terminateWoId,'Terminate');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(NaTasksId!=null && NaTasksId.size()>0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(NaTasksId,'NA Task');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(taskWorklogId!=null && taskWorklogId.size()>0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(taskWorklogId,'TaskWorklog');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(taskInProgressId!=null && taskInProgressId.size()>0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(taskInProgressId,'TaskInProgress');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        
    }
 
/*********************************************************************************************
* @Description :Method to call Get Task API
* @input params : Trigger.New ,newmap,trigger.old,oldmap
**********************************************************************************************/ 
   
    public void callGetTasks(List<Integration_Log__c> newIntegrationLogs, Map<Id,Integration_Log__c> oldIntegrationLogMap ){
        List<Id> updatedIntegrationLogs = new List<Id>();
        
        for(Integration_Log__c integrationLog:newIntegrationLogs){
            if(integrationLog.Reprocess__c==false && integrationLog.API_Type__c == 'Create Work Order' && (integrationLog.Status__c=='Success' && integrationLog.Status__c != oldIntegrationLogMap.get(integrationLog.Id).Status__c)){
                updatedIntegrationLogs.add(integrationLog.Id);
            }
        }
        
        if(updatedIntegrationLogs!=null && updatedIntegrationLogs.size()>0){            
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(updatedIntegrationLogs,'GetTasks'));
        }
    }
    
/***************************************************************************************
* @Description :Method to reprocess requests to Pier
* @input params :newItem,newmap,trigger.old,oldmap
***************************************************************************************/ 
    
    Public void reprocessRequests(List<Integration_Log__c> newIntegrationLogs, Map<Id,Integration_Log__c> oldItemsMap){
        List<Id> reprocessCreateWOLogIds = new List<Id>();
        List<Id> reprocessGetTasksLogIds = new List<Id>();
        List<Id> reprocessUpdateTasksLogIds = new List<Id>();
        List<Id> reprocessOpenWOLogIds = new List<Id>();
        List<Id> reprocessScheduleAssignWOLogIds = new List<Id>();
        List<Id> reprocessApproveTasksId = new List<Id>();
        List<Id> reprocessCloseTaskIds = new List<Id>();
        List<Id> reprocessCreateAdhocTaskId = new List<Id>();
        List<Id> reprocessAttachmentTasksId = new List<Id>();
        List<Id> reprocessTerminateId = new List<Id>();
        List<Id> reprocessNAId = new List<Id>();
        List<Id> reprocesstaskWorklogId = new List<Id>();
        List<Id> reprocesstaskInProgressId = new List<Id>();
        //Start 2.0: Balaji B: Added the below to support the Pier2 API retry mechanism:
        List<Id> reprocessCreateServiceRequestId = new List<Id>();
        List<Id> reprocessCreateChangeRequestId = new List<Id>();
        //End
        
        for(Integration_Log__c log : newIntegrationLogs){
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Create Work Order'){
                reprocessCreateWOLogIds.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Get Tasks'){
                reprocessGetTasksLogIds.add(log.id);
            }
            //start 2.0: Balaji added this block to suport the retry mechanism of the create service request & create request API calls.
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Create Service Request'){
                reprocessCreateServiceRequestId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Create Change Request'){
                reprocessCreateChangeRequestId.add(log.id);
            }
            //Balaji End
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Update Tasks'){
                reprocessUpdateTasksLogIds.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && (log.API_Type__c == 'Open Work Order' || log.API_Type__c =='Closed Work Order' || log.API_Type__c =='Update Work Order')){
                reprocessOpenWOLogIds.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Assign And Schedule Task'){
                reprocessScheduleAssignWOLogIds.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Approve Task'){
                reprocessApproveTasksId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Close Task'){
                reprocessCloseTaskIds.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Create Adhoc Task'){
                reprocessCreateAdhocTaskId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Attachment'){
                reprocessAttachmentTasksId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Terminate Work Order'){
                reprocessTerminateId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'NA Task'){
                reprocessNAId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Task Worklog'){
                reprocesstaskWorklogId.add(log.id);
            }
            if(oldItemsMap.get(log.Id).Reprocess__c != log.Reprocess__c && log.Reprocess__c == true && log.API_Type__c == 'Task In Progress'){
                reprocesstaskInProgressId.add(log.id);
            }

        }
        //start 2.0: Balaji added this block to suport the retry mechanism of the create service request & create request API calls.
        if(reprocessCreateServiceRequestId != Null && reprocessCreateServiceRequestId.Size() > 0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(reprocessCreateServiceRequestId,'CreateServiceRequest');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi);
        }
        if(reprocessCreateChangeRequestId != Null && reprocessCreateChangeRequestId.Size() > 0){
            PierIntegrationAPI pierIntegrationApi = new PierIntegrationAPI(reprocessCreateChangeRequestId,'CreateChangeRequest');
            ID createTaskJobId = System.enqueueJob(pierIntegrationApi); 
        }
        //End

        if(reprocessCreateWOLogIds != Null && reprocessCreateWOLogIds.Size() > 0){
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessCreateWOLogIds,'CreateWorkOrder'));  
        }
        
        if(reprocessGetTasksLogIds != Null && reprocessGetTasksLogIds.Size() > 0){
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessGetTasksLogIds,'GetTasks'));
        }
        
        if(reprocessUpdateTasksLogIds != Null && reprocessUpdateTasksLogIds.Size() > 0){
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessUpdateTasksLogIds,'UpdateTasks'));
        }
        
        if(reprocessOpenWOLogIds != Null && reprocessOpenWOLogIds.Size() > 0){
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessOpenWOLogIds,'UpdateWorkOrder'));
        }
        
        if(reprocessScheduleAssignWOLogIds != Null && reprocessScheduleAssignWOLogIds.Size() > 0){
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessScheduleAssignWOLogIds,'AssignScheduleTasks'));
        }
        
        if(reprocessApproveTasksId!=null && reprocessApproveTasksId.size()>0){
            ID getTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessApproveTasksId,'ApprovalTasks'));
        }
        
        if(reprocessCloseTaskIds!=null && reprocessCloseTaskIds.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessCloseTaskIds,'CloseTask'));
        }
        
        if(reprocessCreateAdhocTaskId!=null && reprocessCreateAdhocTaskId.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessCreateAdhocTaskId,'createAdhocTask'));
        }
        
        if(reprocessAttachmentTasksId!=null && reprocessAttachmentTasksId.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessAttachmentTasksId,'Attachment'));
        }
        
        if(reprocessTerminateId !=null && reprocessTerminateId.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessTerminateId,'Terminate'));
        }
        if(reprocessNAId !=null && reprocessNAId.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocessNAId,'NA Task'));
        }
        if(reprocesstaskWorklogId !=null && reprocesstaskWorklogId.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocesstaskWorklogId,'TaskWorklog'));
        }
        if(reprocesstaskInProgressId !=null && reprocesstaskInProgressId.size()>0) {
            ID createTaskJobId = System.enqueueJob(new PierIntegrationAPI(reprocesstaskInProgressId,'TaskInProgress'));
        }
    }
    
    

/***************************************************************************************
* @Description :Method to update counter field(Count_of_Update_Task_API_Processed__c)on Workorder according to processsed update task API.
                And if all PIER tasks are processed then update checkbox (All_Update_Task_API_Processed__c) on Workorder. 
* @input params :newItem,newItemsMap,oldItemsMap
***************************************************************************************/  
      public static void checkUpdateTaskAPIProcessing(List<Sobject> newItem, Map<Id, Sobject> newItemsMap,Map<Id, Sobject> oldItemsMap){
        List<Integration_Log__c> newItems= (List<Integration_Log__c>) newItem;
        List<Integration_Log__c> successLogsLst= new List<Integration_Log__c>();
        Map<Id,Integration_Log__c> newMap = (Map<Id,Integration_Log__c>)newItemsMap;
        Map<Id,Integration_Log__c> oldMap = (Map<Id,Integration_Log__c>)oldItemsMap;
        List<String> parentIdLst = new List<String>();
     
        for(Integration_Log__c log : newItems){
            if(log.API_Type__c == 'Update Tasks' &&  
               ((oldMap.get(log.id).Status__c==null || oldMap.get(log.id).Status__c=='' || oldMap.get(log.id).Status__c=='Failure') && log.Status__c =='Success')){
                   if(!parentIdLst.contains(log.Parent__c)){
                       parentIdLst.add(log.Parent__c);    
                   }
                   successLogsLst.add(log);
               }
        }
        if(parentIdLst!=null && parentIdLst.size()>0){
            Map<Id,Case> parentCaseMap =new Map<Id,Case>();
            for(Case caseObj :[select id,Total_PIER_task_in_Workflow__c,Count_of_Update_Task_API_Processed__c from Case where id in :parentIdLst /*and Total_Approval_Required_Tasks__c IN (null,0)*/]){
                parentCaseMap.put(caseObj.id,caseObj);      
            }
            Map<Id,Integer> cntMap =new Map<Id, Integer>();
            if(parentCaseMap!=null && parentCaseMap.size()>0){
                for(Integration_Log__c log : successLogsLst){
                    if(parentCaseMap!=null && parentCaseMap.containsKey(log.Parent__c) && parentCaseMap.get(log.Parent__c).Count_of_Update_Task_API_Processed__c!=null){
                        if(cntMap.containsKey(log.Parent__c)){
                            Integer cnt = cntMap.get(log.Parent__c);
                            cntMap.put(log.Parent__c,cnt+1);
                        }
                        else{
                            cntMap.put(log.Parent__c,Integer.valueof(parentCaseMap.get(log.Parent__c).Count_of_Update_Task_API_Processed__c)+1);            
                        }
                    }
                    else{
                        if(cntMap.containsKey(log.Parent__c)){
                            Integer cnt = cntMap.get(log.Parent__c);
                            cntMap.put(log.Parent__c,cnt+1);
                        }
                        else{
                             cntMap.put(log.Parent__c,1);
                        } 
                    }
                }
            }
            if(cntMap!=null && cntMap.size()>0){
                List<Case> updateCaseLst = new List<Case>();
                for(Id parentCaseId: cntMap.keyset()) {
                    if(parentCaseId != null){
                        Case cs =new Case();
                        cs.id = parentCaseId;
                        if(parentCaseMap!=null && parentCaseMap.size()>0 && parentCaseMap.get(parentCaseId) != Null){
                            cs.Count_of_Update_Task_API_Processed__c = cntMap.get(parentCaseId);
                            if(cs.Count_of_Update_Task_API_Processed__c == parentCaseMap.get(parentCaseId).Total_PIER_task_in_Workflow__c){
                                cs.All_Update_Task_API_Processed__c = true;    
                            }
                        }
                        updateCaseLst.add(cs);
                    }
                }
                if(updateCaseLst!=null && updateCaseLst.size()>0){
                    try{
                        database.update(updateCaseLst,true);   
                    }
                    catch(Exception ex){
                        if(!Test.isRunningTest()){
                            throw ex;
                        }
                    }
                }
            }
        }
    }


    /***************************************************************************************
    * @Description :Method to update Trigger_Approval_Required_API__c = true when Update Tasks API is success
    * @input params :newItem,newItemsMap,oldItemsMap
    ***************************************************************************************/  
      public static void UpdateTaskForApproverAPI(List<Sobject> newItem, Map<Id, Sobject> newItemsMap,Map<Id, Sobject> oldItemsMap){
        List<Integration_Log__c> newItems= (List<Integration_Log__c>) newItem;
        Map<Id,Integration_Log__c> newMap = (Map<Id,Integration_Log__c>)newItemsMap;
        Map<Id,Integration_Log__c> oldMap = (Map<Id,Integration_Log__c>)oldItemsMap; 
        Map<Id, List<Integration_Log__c>> wo_IntLogMap = new Map<Id, List<Integration_Log__c>> ();
        Set<String> parentIdSet = new Set<String>();
        Boolean isSuccess = false;
        Set<Id> woIdSet = new Set<Id>();
        List<Case> workOrderToBeUpdatedLst = new List<Case>();
        
          //Get all the parentId for Update Tasks
          for(Integration_Log__c log : newItems){
            if(log.API_Type__c == 'Update Tasks' &&  
               ((oldMap.get(log.id).Status__c==null || oldMap.get(log.id).Status__c=='' || oldMap.get(log.id).Status__c=='Failure') && newMap.get(log.id).Status__c =='Success')){
                     if(!parentIdSet.contains(log.Parent__c)){
                       parentIdSet.add(log.Parent__c);    
                   }
               }
          }
          
          //Query all Integration Logs linked to parent and create a map
          if(parentIdSet.Size() > 0){
             for(Integration_Log__c log : [Select id, Status__c,Parent__c from Integration_Log__c where Parent__c IN: parentIdSet and API_Type__c = 'Update Tasks'] ){
                if(wo_IntLogMap.containskey(log.Parent__c)){
                    wo_IntLogMap.get(log.Parent__c).add(log);
                }
                else
                {
                    wo_IntLogMap.put(log.Parent__c,new List<Integration_Log__c>{log});
                }    
             }
               
               string logId;
             //Check if all Logs for the parent has status as success. If all IL of Update task is success for a WO then save that parentId
             for(Integration_Log__c log : newItems){
                 if(wo_IntLogMap.get(log.Parent__c) != Null){
                     isSuccess = false;
                     for(Integration_Log__c newLog : wo_IntLogMap.get(log.Parent__c)){
                         if(newLog.Status__c == 'Success'){
                             isSuccess = true;
                         }else if(newLog.Status__c == 'Failure'){
                             isSuccess = false;
                             break;
                         }
                     }
                     if(isSuccess == true){
                         woIdSet.add(log.Parent__c);
                     }
                 }
             }
         
             //woIdSet is the set of Work Order Id for which Update task api is successful. Now update the Trigger_Approval_Required_API__c = true to trigger Approver api
             for(case cs : [Select Id, Trigger_Approval_Required_API__c from case where ParentId IN : woIdSet and Approval_Required__c = true]){
                 cs.Trigger_Approval_Required_API__c = true;
                 workOrderToBeUpdatedLst.add(cs);
             }
             
             try{
                 if(workOrderToBeUpdatedLst != null && workOrderToBeUpdatedLst.size() > 0)
                    database.update(workOrderToBeUpdatedLst,true);
             }catch(Exception ex){
                 throw ex;
             }
         }
      }


/***************************************************************************************
* @Description :Method to update counter field(Count_of_Approval_Required_API_Processed__c)on Workorder according to processsed update task API.
                And if all PIER tasks are processed then update checkbox (All_Approve_Task_API_Processed__c) on Workorder. 
* @input params :newItem,newItemsMap,oldItemsMap
***************************************************************************************/  
      public static void checkApproverTaskAPIProcessing(List<Sobject> newItem, Map<Id, Sobject> newItemsMap,Map<Id, Sobject> oldItemsMap){
        List<Integration_Log__c> newItems= (List<Integration_Log__c>) newItem;
        Map<Id,Integration_Log__c> newMap = (Map<Id,Integration_Log__c>)newItemsMap;
        Map<Id,Integration_Log__c> oldMap = (Map<Id,Integration_Log__c>)oldItemsMap;
        List<Integration_Log__c> successLogsLst= new List<Integration_Log__c>();
        List<String> parentIdLst = new List<String>();
     
        for(Integration_Log__c log : newItems){
            if(log.API_Type__c == 'Approve Task' &&  
               ((oldMap.get(log.id).Status__c==null || oldMap.get(log.id).Status__c=='' || oldMap.get(log.id).Status__c=='Failure') && newMap.get(log.id).Status__c =='Success')){
                   if(!parentIdLst.contains(log.Parent__c)){
                       parentIdLst.add(log.Parent__c);    
                   }
                   successLogsLst.add(log);
               }
        }
        
        if(parentIdLst!=null && parentIdLst.size()>0){
            Map<Id,Case> parentCaseMap =new Map<Id,Case>();
            for(Case caseObj :[select id,Total_PIER_task_in_Workflow__c,Count_of_Approval_Required_API_Processed__c,Total_Approval_Required_Tasks__c from Case where id in :parentIdLst]){
                parentCaseMap.put(caseObj.id,caseObj);      
            }
            Map<Id,Integer> cntMap =new Map<Id, Integer>();
            for(Integration_Log__c log : successLogsLst){
                if(parentCaseMap!=null && parentCaseMap.containsKey(log.Parent__c) && parentCaseMap.get(log.Parent__c).Count_of_Approval_Required_API_Processed__c!=null){
                    if(cntMap.containsKey(log.Parent__c)){
                        Integer cnt = cntMap.get(log.Parent__c);
                        cntMap.put(log.Parent__c,cnt+1);
                    }
                    else{
                        cntMap.put(log.Parent__c,Integer.valueof(parentCaseMap.get(log.Parent__c).Count_of_Approval_Required_API_Processed__c)+1);            
                    }
                }
                else{
                    if(cntMap.containsKey(log.Parent__c)){
                        Integer cnt = cntMap.get(log.Parent__c);
                        cntMap.put(log.Parent__c,cnt+1);
                    }
                    else{
                         cntMap.put(log.Parent__c,1);
                    } 
                }
            }
            if(cntMap!=null && cntMap.size()>0){
                List<Case> updateCaseLst = new List<Case>();
                for(Id parentCaseId: cntMap.keyset()) {
                    Case cs =new Case();
                    cs.id = parentCaseId;
                    if(parentCaseMap!=null && parentCaseMap.size()>0){
                        cs.Count_of_Approval_Required_API_Processed__c = cntMap.get(parentCaseId);
                        if(cs.Count_of_Approval_Required_API_Processed__c == parentCaseMap.get(parentCaseId).Total_Approval_Required_Tasks__c){
                            cs.All_Approve_Task_API_Processed__c = true;    
                        }
                    }
                    updateCaseLst.add(cs);
                }
                if(updateCaseLst!=null && updateCaseLst.size()>0){
                    try{
                        database.update(updateCaseLst,true);   
                    }
                    catch(Exception ex){
                        if(!Test.isRunningTest()){
                            throw ex;
                        }
                    }
                }
            }
        }
    }
}
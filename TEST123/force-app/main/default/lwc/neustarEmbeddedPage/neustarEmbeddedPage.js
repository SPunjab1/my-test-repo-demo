import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import PON_FIELD from '@salesforce/schema/Purchase_Order__c.PON__c';
import VERSION_FIELD from '@salesforce/schema/Purchase_Order__c.Version__c';

import NEUSTAR_BASEURL from '@salesforce/label/c.Neustar_Base_URL';
import NEUSTAR_SUFFIXURL from '@salesforce/label/c.Neustar_Suffix_URL';

const fields = [
    PON_FIELD,
    VERSION_FIELD
];

export default class NeustarWeblinklwc extends LightningElement {
    @api recordId;
    
    // Get the Purchase Order field values
    @wire(getRecord, {
        recordId: '$recordId',
        fields
    })
    purchaseOrderObj;

    //method to prepare and return the Neustar UOM URL
    get uomGUIUrl(){
        const pon = getFieldValue(this.purchaseOrderObj.data, PON_FIELD);
        const version = getFieldValue(this.purchaseOrderObj.data, VERSION_FIELD);
        return NEUSTAR_BASEURL+pon+'/'+version +NEUSTAR_SUFFIXURL;
    }
}
({
	copyEnhancement : function (component, event, helper) {
        var action = component.get("c.copyEnhancements");
        var recordid =component.get("v.recordId")
        action.setParams({opportunityId: recordid});
        
        console.log('Calling Copy Enhancements:'+recordid);
        
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('Refreshing view and closing popup'+state);
            //$A.get("e.force:closeQuickAction").fire();
            
            
            
            if(state == 'SUCCESS') {
                $A.get('e.force:refreshView').fire();
                //component.set('v.countries', response.getReturnValue());
                
                 
            }
        });
        
        
        $A.enqueueAction(action);
        
        // Close the action panel
        //var dismissActionPanel = $A.get("e.force:closeQuickAction");
        //dismissActionPanel.fire();
        
        
        
    }
});
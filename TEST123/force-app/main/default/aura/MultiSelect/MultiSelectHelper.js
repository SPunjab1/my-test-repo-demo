({
    fetchAllLocationDetails : function(component, event, helper,locIdTempLst) {
        var locLst = component.get("v.LocationIdLst");
        console.log('locationlist-->'+locLst);
        var secLocId = component.get("v.secLocId");
        var action = component.get("c.validateLocationLst");
        action.setParams({ locationIds : locLst,
                          secLocId :secLocId,
                          locIdTempLst : locIdTempLst
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS" && !$A.util.isEmpty(result)){
                component.set('v.locationValidationMap',response.getReturnValue());
                component.set("v.spinner",false);
                var validMap = component.get('v.locationValidationMap')['Valid'];
                var invalidMap = component.get('v.locationValidationMap')['Invalid'];
                var blankMap = component.get('v.locationValidationMap')['Blank'];
                var validIdLst = new Array();
                var invalidIdLst = new Array();
                var blankIdLst = new Array();
                
                for(var idval in locLst) {
                    if(validMap[locLst[idval]]){
                        validIdLst.push(validMap[locLst[idval]]);   
                    } 
                    else if(locLst[idval].includes('BLANK')){
                        blankIdLst.push(locLst[idval]);     
                    }
                        else{
                            invalidIdLst.push(locLst[idval]);     
                        }
                }
                if(invalidIdLst.length >0){
                    component.set("v.spinner",false);
                    var msg = $A.get("$Label.c.Location_Id_Error")+invalidIdLst;
                    helper.showToast('Error',msg,'error');  
                    component.set("v.spinner",false);
                    var uploaderComp = component.find('uploaderCmp');
                    uploaderComp.clearData();
                }
                else{
                    console.log('location list after validate-->'+validIdLst);
                    component.set("v.LocationLst",validIdLst);
                    var secValidMap = component.get('v.locationValidationMap')['SecValid'];
                    var secInvalidMap = component.get('v.locationValidationMap')['SecInvalid'];
                    var secValidIdLst = new Array();
                    var secInvalidIdLst = new Array();
                    for(var idval in secLocId) {
                        if(secValidMap[secLocId[idval]]){
                            secValidIdLst.push(secValidMap[secLocId[idval]]);   
                        } 
                        else{
                            secInvalidIdLst.push(secLocId[idval]);     
                        }
                    }
                    if(secInvalidIdLst.length >0){
                        component.set("v.spinner",false);
                        var msg = $A.get("$Label.c.Secondary_Location_Id_Error")+secInvalidIdLst;
                        helper.showToast('Error',msg,'error');  
                        component.set("v.spinner",false);
                        var uploaderComp = component.find('uploaderCmp');
                        uploaderComp.clearData();
                    }
                    else{
                        component.set("v.SecLocationLst",secValidIdLst);
                        if(component.get("v.switchIdLst").length >0){
                            helper.validateSwitchId(component, event, helper,blankIdLst);   
                        }
                        else{
                            if(component.get("v.tpeValidation")){
                                component.set("v.duplicateDataMap",component.get('v.locationValidationMap')['Duplicate']);
                                if(blankIdLst.length >0){
                                    component.set("v.BlankIdLst",blankIdLst);
                                    var uploaderComp = component.find('uploaderCmp');
                                    uploaderComp.displayDarkSiteData(blankIdLst);
                                }
                                else{
                                    if(component.get("v.duplicateDataMap")!=null && !$A.util.isEmpty(component.get("v.duplicateDataMap"))){
                                        var uploaderComp = component.find('uploaderCmp');
                                        var temp1 = component.get("v.duplicateDataMap");
                                        console.log(JSON.stringify('duplicate date==>'+temp1));
                                        uploaderComp.displayDuplicatesFromDatabase(component.get("v.duplicateDataMap"));	    
                                    }
                                    else{
                                        this.bulkDupCheck(component, event, helper);
                                        //helper.createRecords(component, event, helper);     
                                    }
                                }
                            }else{
                                
                            }  
                        }
                        
                    }
                }               
            }
            else{
                console.log('Error in MutltiSelect : fetchAllLocationDetails'); 
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);
    },
    bulkDupCheck :function(component, event, helper) {
        component.set("v.spinner",true);
        let data = component.get('v.fileContentData');
        console.log('component1--- '+data);
        var allTextLines = data.split(/\r\n|\n/);
        let allLocIds = '';
        let templateNamesArray = [];
        let allLocsArray = [];
        let cirTempMap = new Map();
        let finalMap = new Map();
        var cirLstFlag = [];
        let allTextLinesWOHeader = [...allTextLines];
        var locationIdiIndex;
        var templateNameindex;
        var cirIndex;
        var cirNullFlag =[];
        let myNullSet = new Set();
        let myNumSet = new Set();
        let cirReq =$A.get("$Label.c.CIR_Required");
        console.log('cirReq'+cirReq);
        let CirReqTemp = cirReq.split(',');
        console.log('crittemp'+CirReqTemp);
        console.log('allTextLinesWOHeader--->>'+allTextLinesWOHeader);
        let headers = allTextLinesWOHeader[0].split(',');
        for(var i =0;i<headers.length ;i++){
            if(headers[i] == $A.get("$Label.c.CSVCol_LocationID")){
                locationIdiIndex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_TemplateName")){
                templateNameindex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_CIR")){
                cirIndex =  i;  
            }
        }
        
        allTextLinesWOHeader.shift();
        console.log('allTextLinesWOHeader1--->>'+allTextLinesWOHeader);
        for(let row of allTextLinesWOHeader){
            let rowArray = row.split(',');
            
            if(rowArray && rowArray!== "" && rowArray!== undefined){
                console.log('row0--'+rowArray[0]);
                console.log('row1--'+rowArray[1]);
                console.log('row2--'+rowArray[2]);
                console.log('row3--'+rowArray[3]);
                console.log('row4--'+rowArray[4]);
                console.log('row5--'+rowArray[5]);
                console.log('rows template--'+rowArray[templateNameindex]);
                console.log('rows location--'+rowArray[locationIdiIndex]);
                templateNamesArray.push(rowArray[templateNameindex]);
                allLocsArray.push(rowArray[locationIdiIndex]);
                //check if rowArray[cirIndex] is 'undefined'
                //then add it to the map.
                let t = rowArray[cirIndex];
                console.log('!!!--'+t);
                if(t == 0 || t>100000){
                    console.log('in cir myNumSet');
                    myNumSet.add(rowArray[templateNameindex]);
                }
                if(!t){
                    console.log('in cir undefined--');
                    myNullSet.add(rowArray[templateNameindex]);
                }
                
            }
        }	
        console.log('mySet---++'+myNumSet +' '+myNullSet);
        for(var labelval of CirReqTemp){
            console.log('key-->'+labelval);
            console.log('setvaluekey-->'+myNullSet.has(labelval));
            // let t = cirTempMap.get(key);
            if(myNullSet.has(labelval)){
                console.log('values cirNullFlag-->'+labelval);
                cirNullFlag.push(labelval);
                console.log('! cirNullFlag-->'+labelval);
            }
            if(myNumSet.has(labelval)){
                cirLstFlag.push(labelval);
                console.log('!!cirNullFlag> '+cirLstFlag);
            }
        }
        
        console.log('cirNullFlag'+JSON.stringify(cirNullFlag));
        if(cirNullFlag!=null && cirNullFlag.length >0){
            console.log('in null error'+cirNullFlag);
            var msg = "Cir value is required for this template "+cirNullFlag.slice(0, 2);
            helper.showToast('error',msg,'error');
            component.set("v.spinner",false);
            var uploaderComp = component.find('uploaderCmp');
            uploaderComp.clearData();
            return;
        }
        console.log('cirLstFlag'+JSON.stringify(cirLstFlag));
        if(cirLstFlag!=null && cirLstFlag.length >0){
            console.log('in error'+cirLstFlag);
            var msg = "Cir value should be between 1 to 10,000 "+cirLstFlag.slice(0, 2);
            helper.showToast('error',msg,'error');
            component.set("v.spinner",false);
            var uploaderComp = component.find('uploaderCmp');
            uploaderComp.clearData();
            return;
        }
        
        console.log(' \TEMPLATENAMES-- '+templateNamesArray);
        console.log(' \\ LOCATION IDS-- '+allLocsArray);
        
        var action = component.get("c.checkLocations");
        console.log(' \\ action'+action);
        action.setParams({
            "templateNames" : templateNamesArray,
            "locationIds" : allLocsArray
        });
        console.log(' after making callout');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' state ut'+state);
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                console.log('RETURN VALLUES '+returnValue);
                if(returnValue!=null && !$A.util.isEmpty(returnValue)){
                    let respKeys = Object.keys(returnValue);
                    if(respKeys.length > 0){
                        component.set( 'v.shouldCreateRecords', false);
                        console.log('dup location record values-->'+returnValue);
                        //if(returnValue!=null || returnValue!=''){
                        var dataRows = allTextLines.length-1;
                        var headers = allTextLines[0].split(',');
                        var lines = [];
                        var index = 0;
                        var filecontentdata;
                        component.set("v.spinner",false);
                        component.set("v.isOpenDuplicateInDatabase",true);
                        //component.set("v.isOpenDuplicate", true);
                        var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
                        content += "<thead><tr class=\"slds-text-title--caps\">";
                        content += '<th scope=\"col"\>Index</th>';
                        content += '<th scope=\"col"\>Template Name</th>';
                        content += '<th scope=\"col"\>Location Id</th>';
                        content += '<th scope=\"col"\>CRCount</th>';
                        content += '<th scope=\"col"\>Secondary Location Id</th>';                    
                        content += '<th scope=\"col"\>Advance RANT Tranmission</th>';                    
                        content += '<th scope=\"col"\>Enterprise Transmission Design</th>';                    
                        content += '<th scope=\"col"\>Backhaul Planning & Implementation</th>';                    
                        content += '<th scope=\"col"\>Transmission Deployment Walker</th>';
                        content += "</tr></thead>";
                        for(let x in returnValue){
                            index++;
                            content +="<tr>";
                            content +='<td>'+index+'</td>';
                            content +='<td>'+returnValue[x].Workflow_Template__c+'</td>';
                            content +='<td>'+returnValue[x].Pier_Location_ID__c+'</td>';
                            //content +='<td>'+returnValue[x].CRCount__c+'</td>';
                            //content +='<td>'+returnValue[x].Secondary_Location_ID__c+'</td>';
                            content +='<td></td>';
                            content +='<td></td>';                            
                            content +='<td>'+returnValue[x].Advance_RAN_Transmission__c+'</td>';
                            content +='<td>'+returnValue[x].Enterprise_Transmission_Design__c+'</td>';
                            content +='<td>'+returnValue[x].Backhaul_Planning_Implementation__c+'</td>';
                            content +='<td>'+returnValue[x].Transmission_Deployment_Walker__c+'</td>';
                            
                            content +="</tr>";
                        }
                        
                        
                        content += "</table>";
                        console.log(content);
                        
                        console.log('values of modal'+component.get("v.isOpenDuplicateInDatabase"));
                        component.set("v.DuplicateDataFromDatabase",content);
                        console.log('values of te modal'+component.get("v.DuplicateDataFromDatabase"));
                        
                    }
                }else{
                    helper.createRecords(component, event, helper);
                }
                
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.spinner",false);
                console.log(errors);
            }
        });
        $A.enqueueAction(action);
    },
    createRecords :function(component, event, helper) {
        component.set("v.spinner",true);
        var locations = component.get("v.LocationLst");
        console.log('locations===='+locations);
        var filecon = component.get("v.fileContentMap");
        console.log('Filecontentmap--> '+JSON.stringify(filecon));
        var action = component.get("c.createMultiSelectIntakeRec");
        action.setParams({ "intakeReq" : component.get("v.IntakeRequest"),
                          "locations" : locations,
                          "secLocId" : component.get("v.SecLocationLst"),
                          "fileContentMap1" : component.get("v.fileContentMap"),
                          "blankIdLst" : component.get("v.BlankIdLst"),
                          "switchIdLst" : component.get("v.SwitchLst")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.spinner",false);
                var result = response.getReturnValue();
                if(!$A.util.isEmpty(result)) {
                    for(var key in result){
                        if(key == 'Yes' || key == 'No'){ 
                            var uploaderComp = component.find('uploaderCmp');
                            uploaderComp.addAttchment(result[key]);
                            console.log('attach values'+result[key]);
                        }
                    }
                    var recordId;
                    for(var key in result){
                        if(key == 'Yes'){
                            var msg =$A.get("$Label.c.Intake_Successful_Message");
                            helper.showToast('Success',msg,'success');
                            recordId = result[key];
                        }
                        else if(key == 'No'){
                            var msg =$A.get("$Label.c.BatchJobRunningMsg");
                            helper.showToast('Warning',msg,'Warning');
                            recordId = result[key];
                        }
                    }
                    component.set("v.spinner",false); 
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId,
                        "slideDevName": "related"
                    });
                    navEvt.fire(); 
                    $A.get('e.force:refreshView').fire();
                }
            }
            else{
                console.log('Error in createRecords of mutiselect');
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();  
    },
    validateSwitchId :function(component, event, helper,blankIdLst) {
        var switchIdlst = component.get("v.switchIdLst");
        var action = component.get("c.validateSwitchIdLst");
        action.setParams({ switchIdlst : switchIdlst });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS" && !$A.util.isEmpty(result)){
                component.set('v.switchValidationMap',response.getReturnValue());
                var validMap = component.get('v.switchValidationMap')['Valid'];
                var invalidMap = component.get('v.switchValidationMap')['Invalid'];
                var validIdLst = new Array();
                var invalidIdLst = new Array();
                for(var idval in switchIdlst) {
                    if(validMap[switchIdlst[idval]]){
                        validIdLst.push(validMap[switchIdlst[idval]]);   
                    }
                    else{
                        invalidIdLst.push(switchIdlst[idval]);     
                    }
                }
                if(invalidIdLst.length >0){
                    component.set("v.spinner",false);
                    var msg = $A.get("$Label.c.Switch_Id_Error")+invalidIdLst;
                    helper.showToast('Error',msg,'error');  
                    component.set("v.spinner",false);
                    var uploaderComp = component.find('uploaderCmp');
                    uploaderComp.clearData();
                }
                else{
                    component.set("v.SwitchLst",validIdLst);
                    component.set("v.duplicateDataMap",component.get('v.locationValidationMap')['Duplicate']);
                    if(blankIdLst.length >0){
                        component.set("v.BlankIdLst",blankIdLst);
                        var uploaderComp = component.find('uploaderCmp');
                        uploaderComp.displayDarkSiteData(blankIdLst);
                    }
                    else{
                        if(component.get("v.duplicateDataMap")!=null && !$A.util.isEmpty(component.get("v.duplicateDataMap"))){
                            var uploaderComp = component.find('uploaderCmp');
                            uploaderComp.displayDuplicatesFromDatabase(component.get("v.duplicateDataMap"));	    
                        }
                        else{
                            helper.createRecords(component, event, helper);     
                        }
                    }
                }                  
            }
            else{
                console.log('Error in MutltiSelect : validateSwitchId'); 
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);    
    },
    validateTPERequestNumbers : function(component,event, helper){
        var action = component.get("c.getTpeRequestNumberMap");
        action.setParams({ fileContentMap : component.get("v.fileContentMap"),
                          CaseNumberSingle : null
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS" && !$A.util.isEmpty(result)){
                component.set("v.validateTpeRequestNumbers", result );
                var validateTpeRequestNumMap = component.get("v.validateTpeRequestNumbers");
                for(var key in component.get("v.validateTpeRequestNumbers")){
                    if(key == 'caseMisMatch'){
                        console.log('validateTpeRequestNumMap.CaseMismtach======', validateTpeRequestNumMap.caseMisMatch);
                        var msg = $A.get("$Label.c.TPERequestNumber_Error")+validateTpeRequestNumMap.caseMisMatch;
                        helper.showToast('Error',msg,'error');  
                        component.set("v.spinner",false);
                        var uploaderComp = component.find('uploaderCmp');
                        uploaderComp.clearData();
                    }else{
                        component.set("v.tpeValidation", true);
                    }
                }
            }else{
                if($A.util.isEmpty(result)){
                    console.log('No TPERequestNumbers to validate'); 
                    component.set("v.tpeValidation", true); 
                }else{
                    console.log('Error in MutltiSelect : validateTPERequestNumbers'); 
                    component.set("v.spinner",false);
                }
            }
        });
        $A.enqueueAction(action);
    }
})
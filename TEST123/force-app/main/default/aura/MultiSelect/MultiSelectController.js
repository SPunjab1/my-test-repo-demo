({
    init: function(component, event, helper) {
        var action = component.get("c.getSampleFileId");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS" && !$A.util.isEmpty(result)){
                console.log('call init****');
                
                component.set("v.samplefileURL",'/servlet/servlet.FileDownload?file='+result);
            }
            else {
                console.log('Error in MutltiSelect : validateSwitchId'); 
                component.set("v.spinner",false);
            } 
        });
        $A.enqueueAction(action);
      },  
    handleLocationIDLstEvent : function(component, event, helper) {
        component.set("v.spinner",true);
        event.stopPropagation();
        component.set("v.LocationIdLst",event.getParam("locationIdLst"));
        component.set("v.fileContentMap",event.getParam("fileContentMap"));
        component.set("v.fileContentData",event.getParam("fileContentData"));
        component.set("v.fileName",event.getParam("fileName")); 
        component.set("v.fileType",event.getParam("fileType")); 
        component.set("v.secLocId",event.getParam("secLocIdLst"));
        component.set("v.switchIdLst",event.getParam("switchIdLst"));
        var locIdTempLst = new Array();
        for(var key in component.get("v.fileContentMap")){
            console.log('map keys-->'+ key);
        	locIdTempLst.push(key);   
        }
        helper.validateTPERequestNumbers(component, event, helper);
        helper.fetchAllLocationDetails(component, event, helper,locIdTempLst);
        
	},
    handleDarkSite : function(component, event, helper) {
        event.stopPropagation();
        if(component.get("v.duplicateDataMap")!=null && !$A.util.isEmpty(component.get("v.duplicateDataMap"))){
            var uploaderComp = component.find('uploaderCmp');
            uploaderComp.displayDuplicatesFromDatabase(component.get("v.duplicateDataMap"));	    
        }
        else{
            helper.createRecords(component, event, helper);     
        }
    },
    handleDuplicateData :function(component, event, helper) {
        event.stopPropagation();
        helper.createRecords(component, event, helper);
    },
        closeDatabaseDuplicateModel : function(component, event, helper) {
        component.set("v.isOpenDuplicateInDatabase", false);
        var uploaderComp = component.find('uploaderCmp');
        uploaderComp.clearData();
    },
    downloadSampleFile:function(component, event, helper){
        var url="/servlet/servlet.FileDownload?file=0152h0000005ILR";
        var link=document.createElement(a);
        link.setAttribute("href",url);
        link.setAttribute("download","IntakeFormTemplate.csv");
        link.style.display="none";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        
        //0152h0000005ILR
          /*   */ 
}
})
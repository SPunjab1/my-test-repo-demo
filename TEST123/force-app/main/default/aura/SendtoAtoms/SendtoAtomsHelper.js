({
    doInit:function(component, event, helper){
        var parentCaseId=component.get("v.recordId");
        var action = component.get('c.validateLoginUserProfile');
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                console.log("res--->>> " + res);
                component.set('v.isDisable', res);   
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error Message: " + errors[0].message);
                    }
                }
                else{
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    updateCaseStatushelper:function(component,event,helper){
        var action = component.get('c.startAtomsIntegration');
        action.setParams({taskId : component.get("v.recordId")});
      
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(!$A.util.isEmpty(result)) {  
                    $A.get('e.force:refreshView').fire();
                    console.log("result-->"+result);
                    var msg =$A.get("$Label.c.Case_OM_Integration_Status_Update");
                    helper.showToast('success',msg,'success');
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error Message: " + errors[0].message);
                    }
                }
                else{
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();  
    },
})
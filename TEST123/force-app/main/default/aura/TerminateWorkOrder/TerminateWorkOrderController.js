({
    doInit  : function(component, event, helper){
        helper.checkWOstatus(component, event, helper);
    },
    openModelAction : function(component, event, helper) {
		component.set("v.isOpen", true);	
	},
    closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
    },
    terminateWorkOrder : function(component, event, helper) {
      helper.terminateWorkOrderAction(component, event, helper);
    }
})
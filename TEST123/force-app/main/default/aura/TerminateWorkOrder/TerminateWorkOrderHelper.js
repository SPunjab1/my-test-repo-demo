({
    checkWOstatus : function(component, event, helper){
        var workOrderId = component.get("v.recordId");
        var action = component.get('c.checkWOstatus');
        action.setParams({
            workOrderId : workOrderId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                if(res){
                	component.set("v.isDisable", true);     
                }
                else{
                	component.set("v.isDisable", false);      
                }
            }
            else{
            	  console.log("Error in checkWOstatus");    
            }
        });
        $A.enqueueAction(action);
	},
    terminateWorkOrderAction : function(component, event, helper) {
        var workOrderId = component.get("v.recordId");
        var action = component.get('c.terminateCurrentWorkOrder');
        action.setParams({
            workOrderId : workOrderId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var msg = $A.get("$Label.c.WO_Termination_success_msg");
                helper.showToast('Success',msg,'success');
                $A.get('e.force:refreshView').fire();
                component.set("v.isDisable", true);
                component.set("v.isOpen", false);
            }
            else if(state === "ERROR") {
                console.log("Error in terminateWorkOrderAction");
                component.set("v.isOpen", false);
            }
        });
        $A.enqueueAction(action);
    },
    showToast: function(title,message,type) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          title : title,
          message: message,
          type: type,
          mode: 'pester'
      });
      toastEvent.fire(); 
  }
})
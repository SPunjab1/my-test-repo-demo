({
    invoke : function(component, event, helper) {
        // Get the record ID attributes
        var record = component.get("v.recordId");
        
        // Get the Lightning event that opens a record in a new tab
        
        $A.get('e.force:refreshView').fire();
        var redirect = $A.get("e.force:navigateToSObject");
        
        redirect.setParams({
            "recordId": record
        });
        
        // Open the record
        redirect.fire();
        // Pass the record ID to the event
    }
})
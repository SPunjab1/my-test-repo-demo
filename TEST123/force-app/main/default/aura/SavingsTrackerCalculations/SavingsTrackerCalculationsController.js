({
    init: function(component, event, helper){
        component.set("v.columnWidth",65/(component.get("v.years").length));
    },
    
    yearsChanged: function(component, event, helper){
        component.set("v.columnWidth",65/(component.get("v.years").length));
        var yearColumnElements = document.getElementsByClassName("year-column");
        for (var i=0; i<yearColumnElements.length; i++) {
            yearColumnElements[i].style.width = component.get("v.columnWidth")+"%";
        }
    },
    
	calculateOriginalVolumeTotals: function(component, event, helper){
        helper.calculateOriginalVolumeTotals(component,helper);
    },
    
    calculateRevisedVolumeTotals: function(component, event, helper){
        helper.calculateRevisedVolumeTotals(component,helper);
    },
    
    calculateBaselineUnitPriceTotals: function(component, event, helper){
        helper.calculateBaselineUnitPriceTotals(component,helper);
    },
    
    calculateNewUnitPriceTotals: function(component, event, helper){
        helper.calculateNewUnitPriceTotals(component,helper);
    },
    
    calculateOneOffCreditsOrRebatesTotals: function(component, event, helper){
        helper.calculateOneOffCreditsOrRebatesTotals(component,helper);
    },
    
    calculateOtherCostAvoidanceTotals: function(component, event, helper){
        helper.calculateOtherCostAvoidanceTotals(component,helper);
    },
    
    calculateEarlyPaymentDiscountTotals: function(component, event, helper){
        helper.calculateEarlyPaymentDiscountTotals(component,helper);
    },
    
    calculateDealValueOrSpendTotals: function(component, event, helper){
        helper.calculateDealValueOrSpendTotals(component,helper);
    },
    
    baselineSpendCapexShareChanged: function(component, event, helper){
        helper.baselineSpendCapexShareChanged(component,helper);
    },
    
    hardSavingsCapexShareChanged: function(component, event, helper){
        helper.hardSavingsCapexShareChanged(component,helper);
    },
    
    costAvoidanceCapexShareChanged: function(component, event, helper){
        helper.costAvoidanceCapexShareChanged(component,helper);
    }
    
})
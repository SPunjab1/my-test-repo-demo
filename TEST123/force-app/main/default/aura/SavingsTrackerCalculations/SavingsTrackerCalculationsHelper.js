({
	calculateOriginalVolumeTotals: function(component, event){
        this.calculateTotals(component, event, 'Step3_Volume_Forecast_Original_Y', 'Step3_Total_Original_Volume_Forecast__c');
    },
    
    calculateRevisedVolumeTotals: function(component, event){
        this.calculateTotals(component, event, 'Step3_Volume_Forecast_Revised_Y', 'Step3_Total_Revised_Volume_Forecast__c');
    },
    
    calculateBaselineUnitPriceTotals: function(component, event){
        this.calculateTotals(component, event, 'Step3_Baseline_Unit_Price_Y', 'Step3_Total_Baseline_Unit_Price__c');
    },
    
    calculateNewUnitPriceTotals: function(component, event){
        this.calculateTotals(component, event, 'Step3_New_Unit_Price_Y', 'Step3_Total_New_Unit_Price__c');
    },
    
    calculateOneOffCreditsOrRebatesTotals: function(component, event){
        this.calculateTotals(component, event, 'Step3_One_Off_Credits_or_Rebates_Y', 'Step3_Total_One_Off_Credits_or_Rebates__c');
    },
    
    calculateOtherCostAvoidanceTotals: function(component, event){
        this.calculateTotals(component, event, 'Free_Scope_Price_Delay_Other_Y', 'Total_Free_Scope_Price_Delay_Other__c');
    },
    
    calculateEarlyPaymentDiscountTotals: function(component, event){
        this.calculateTotals(component, event, 'Step3_Early_Payment_Discount_Savings_Y', 'Total_Early_Payment_Discount_Savings__c');
    },
    
    calculateTotals: function(component, event, yearPartialFieldName, totalFieldName){
        debugger;
        var savingsTracker = component.get("v.savingsTracker");
        var years = component.get("v.years");
        var total = 0;
        for(var i=0; i<years.length; i++){
            total += (savingsTracker[yearPartialFieldName+(i+1)+'__c'] ? parseFloat(savingsTracker[yearPartialFieldName+(i+1)+'__c']) : 0);
        }
        component.find(totalFieldName.replace('__c','')).set("v.value",total);
        savingsTracker[totalFieldName] = total;
        component.set("v.savingsTracker",savingsTracker);
        console.log('Testig calc total saving tracker'+ savingsTracker);
    },
    
    calculateDealValueOrSpendTotals: function(component, event){
        debugger;
        var savingsTracker = component.get("v.savingsTracker");
        console.log('Testing Savings tracker' + savingsTracker);
        savingsTracker['Total_Spend_Difference_Baseline_vs_New__c'] = 0;
        savingsTracker['Total_Price_Impact_Approximate__c'] = 0;
        savingsTracker['Total_Volume_Impact_Approximate__c'] = 0;
        savingsTracker['Total_Price_Impact_Effective__c'] = 0;
        savingsTracker['Total_Volume_Impact_Effective__c'] = 0;
        savingsTracker['Total_Price_Component_Incremental__c'] = 0;
        savingsTracker['Total_Volume_Component_Incremental__c'] = 0;
        savingsTracker['Total_Hard_Savings_Incremental_Savings__c'] = 0;
        savingsTracker['Total_Price_Component_Non_Incremental__c'] = 0;
        savingsTracker['Total_Cost_Avoidance_Non_Incremental__c'] = 0;
        savingsTracker['Total_Volume_Component_Non_Incremental__c'] = 0;
        savingsTracker['Total_Early_Payment_Discount_Savings__c'] = 0;
   
        var years = component.get("v.years");
        var baselineSpendTotal = 0, newSpendTotal = 0, grossSavingsTotal = 0, totalHardSavings = 0, totalCostAvoidance = 0, totalTotalSavings = 0;
        for(var i=0; i<years.length; i++){
            var volumeForecastOriginal = savingsTracker['Step3_Volume_Forecast_Original_Y'+(i+1)+'__c'] ? parseFloat(savingsTracker['Step3_Volume_Forecast_Original_Y'+(i+1)+'__c']) : 0;
            var baselineUnitPrice = savingsTracker['Step3_Baseline_Unit_Price_Y'+(i+1)+'__c'] ? parseFloat(savingsTracker['Step3_Baseline_Unit_Price_Y'+(i+1)+'__c']) : 0;
            var baselineSpendY = volumeForecastOriginal*baselineUnitPrice;
            baselineSpendTotal += baselineSpendY;
            savingsTracker['Baseline_Spend_Y'+(i+1)+'__c'] = baselineSpendY;
            
            var volumeForecastRevised = savingsTracker['Step3_Volume_Forecast_Revised_Y'+(i+1)+'__c'] ? parseFloat(savingsTracker['Step3_Volume_Forecast_Revised_Y'+(i+1)+'__c']) : 0;
            var newUnitPrice = savingsTracker['Step3_New_Unit_Price_Y'+(i+1)+'__c'] ? parseFloat(savingsTracker['Step3_New_Unit_Price_Y'+(i+1)+'__c']) : 0;
            var newSpendY = volumeForecastRevised*newUnitPrice;
            newSpendTotal += newSpendY;
            savingsTracker['New_Spend_Y'+(i+1)+'__c'] = newSpendY;
            grossSavingsTotal += (baselineSpendY-newSpendY);
            savingsTracker['Step3_Gross_Savings_Y'+(i+1)+'__c'] = baselineSpendY-newSpendY;
            var earlyPaymentDiscountSavingsRateDelta = (savingsTracker['Step2_New_Early_Payment_Discount_Rate__c']-savingsTracker['Step2_Old_Early_Payment_Discount_Rate__c']) || 0;
            console.log('Testing payment disc'+( savingsTracker['Step3_Early_Payment_Discount_Savings_Y'+(i+1)+'__c'] = (savingsTracker['New_Spend_Y'+(i+1)+'__c']*earlyPaymentDiscountSavingsRateDelta/100) || 0));
            savingsTracker['Step3_Early_Payment_Discount_Savings_Y'+(i+1)+'__c'] = (savingsTracker['New_Spend_Y'+(i+1)+'__c']*earlyPaymentDiscountSavingsRateDelta/100) || 0;
            console.log('Testing payment disc'+( savingsTracker['Step3_Early_Payment_Discount_Savings_Y'+(i+1)+'__c'] = (savingsTracker['New_Spend_Y'+(i+1)+'__c']*earlyPaymentDiscountSavingsRateDelta/100) || 0));
            savingsTracker['Total_Early_Payment_Discount_Savings__c'] += savingsTracker['Step3_Early_Payment_Discount_Savings_Y'+(i+1)+'__c'];
            savingsTracker['Step3_Total_Difference_in_Volume_in_Y'+(i+1)+'__c'] = volumeForecastRevised-volumeForecastOriginal;
            savingsTracker['Step3_Total_Difference_in_Price_in_Y'+(i+1)+'__c'] = baselineUnitPrice-newUnitPrice;
            if(i == 0){
                savingsTracker['Step3Incremental_Difference_in_Volume_Y'+(i+1)+'__c'] = savingsTracker['Step3_Total_Difference_in_Volume_in_Y'+(i+1)+'__c'];
                savingsTracker['Incremental_Difference_in_Price_in_Y'+(i+1)+'__c'] = savingsTracker['Step3_Total_Difference_in_Price_in_Y'+(i+1)+'__c'];
            }else{
                savingsTracker['Step3Incremental_Difference_in_Volume_Y'+(i+1)+'__c'] = savingsTracker['Step3_Total_Difference_in_Volume_in_Y'+(i+1)+'__c']-savingsTracker['Step3_Total_Difference_in_Volume_in_Y'+(i)+'__c'];
                savingsTracker['Incremental_Difference_in_Price_in_Y'+(i+1)+'__c'] = savingsTracker['Step3_Total_Difference_in_Price_in_Y'+(i+1)+'__c']-savingsTracker['Step3_Total_Difference_in_Price_in_Y'+(i)+'__c'];
            }
            savingsTracker['Spend_Difference_Baseline_vs_New_Y'+(i+1)+'__c'] = baselineSpendY-newSpendY;
            savingsTracker['Total_Spend_Difference_Baseline_vs_New__c'] += savingsTracker['Spend_Difference_Baseline_vs_New_Y'+(i+1)+'__c'];
            savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'__c'] = savingsTracker['Step3_Total_Difference_in_Price_in_Y'+(i+1)+'__c']*volumeForecastOriginal;
            savingsTracker['Total_Price_Impact_Approximate__c'] += savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'__c'];
            savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'__c'] = savingsTracker['Step3_Total_Difference_in_Volume_in_Y'+(i+1)+'__c']*baselineUnitPrice;
            savingsTracker['Total_Volume_Impact_Approximate__c'] += savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'__c'];
            savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'_percent__c'] = (savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'__c']/(savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'__c']+savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'__c'])*100) || 0;
            savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'_percent__c'] = (savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'__c']/(savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'__c']+savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'__c'])*100) || 0;
            savingsTracker['Price_Impact_Effective_Y'+(i+1)+'__c'] = savingsTracker['Spend_Difference_Baseline_vs_New_Y'+(i+1)+'__c']*savingsTracker['Price_Impact_Approximate_Y'+(i+1)+'_percent__c']/100;
            savingsTracker['Total_Price_Impact_Effective__c'] += savingsTracker['Price_Impact_Effective_Y'+(i+1)+'__c'];
            savingsTracker['Volume_Impact_Effective_Y'+(i+1)+'__c'] = savingsTracker['Spend_Difference_Baseline_vs_New_Y'+(i+1)+'__c']*savingsTracker['Volume_Impact_Approximate_Y'+(i+1)+'_percent__c']/100;
            savingsTracker['Total_Volume_Impact_Effective__c'] += savingsTracker['Volume_Impact_Effective_Y'+(i+1)+'__c'];
            if(i == 0){
                savingsTracker['Price_Component_Incremental_Savings_Y'+(i+1)+'__c'] = savingsTracker['Price_Impact_Effective_Y'+(i+1)+'__c'];
                savingsTracker['Volume_Component_Incremental_Y'+(i+1)+'__c'] = savingsTracker['Volume_Impact_Effective_Y'+(i+1)+'__c'];
            }else{
                savingsTracker['Price_Component_Incremental_Savings_Y'+(i+1)+'__c'] = savingsTracker['Incremental_Difference_in_Price_in_Y'+(i+1)+'__c']*volumeForecastRevised;
				savingsTracker['Volume_Component_Incremental_Y'+(i+1)+'__c'] = savingsTracker['Step3Incremental_Difference_in_Volume_Y'+(i+1)+'__c']*newUnitPrice;
            }
            savingsTracker['Total_Price_Component_Incremental__c'] += savingsTracker['Price_Component_Incremental_Savings_Y'+(i+1)+'__c'];
            savingsTracker['Total_Volume_Component_Incremental__c'] += savingsTracker['Volume_Component_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Hard_Savings_Incremental_Savings_Y'+(i+1)+'__c'] = savingsTracker['Price_Component_Incremental_Savings_Y'+(i+1)+'__c'] + savingsTracker['Volume_Component_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Total_Hard_Savings_Incremental_Savings__c'] += savingsTracker['Hard_Savings_Incremental_Savings_Y'+(i+1)+'__c'];
            savingsTracker['Price_Component_Non_Incremental_Y'+(i+1)+'__c'] = savingsTracker['Price_Impact_Effective_Y'+(i+1)+'__c']-savingsTracker['Price_Component_Incremental_Savings_Y'+(i+1)+'__c'];
            savingsTracker['Total_Price_Component_Non_Incremental__c'] += savingsTracker['Price_Component_Non_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Volume_Component_Non_Incremental_Y'+(i+1)+'__c'] = savingsTracker['Volume_Impact_Effective_Y'+(i+1)+'__c']-savingsTracker['Volume_Component_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Total_Volume_Component_Non_Incremental__c'] += savingsTracker['Volume_Component_Non_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Cost_Avoidance_Non_Incremental_Y'+(i+1)+'__c'] = savingsTracker['Price_Component_Non_Incremental_Y'+(i+1)+'__c'] + savingsTracker['Volume_Component_Non_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Total_Cost_Avoidance_Non_Incremental__c'] += savingsTracker['Cost_Avoidance_Non_Incremental_Y'+(i+1)+'__c'];
            savingsTracker['Step3_Net_Savings_Hard_Savings_Y'+(i+1)+'__c'] = savingsTracker['Hard_Savings_Incremental_Savings_Y'+(i+1)+'__c']+(parseInt(savingsTracker['Step3_One_Off_Credits_or_Rebates_Y'+(i+1)+'__c']) || 0)+(parseInt(savingsTracker['Step3_Early_Payment_Discount_Savings_Y'+(i+1)+'__c']) || 0);
            savingsTracker['Step3_Net_Savings_Cost_Avoidance_Y'+(i+1)+'__c'] = savingsTracker['Cost_Avoidance_Non_Incremental_Y'+(i+1)+'__c']+(parseInt(savingsTracker['Free_Scope_Price_Delay_Other_Y'+(i+1)+'__c']) || 0);
            savingsTracker['Step3_Net_Savings_Total_Savings_Y'+(i+1)+'__c'] = savingsTracker['Step3_Net_Savings_Hard_Savings_Y'+(i+1)+'__c']+savingsTracker['Step3_Net_Savings_Cost_Avoidance_Y'+(i+1)+'__c'];
            
            var blDeal = component.get("v.baselineDeal");
        console.log('base line value '+blDeal);
            if(blDeal !== 'Historical Cost' && blDeal !== 'RFP Average'){
       		  //if(blDeal !== 'Historical Cost'){
               totalHardSavings = 0;
                totalCostAvoidance += savingsTracker['Step3_Net_Savings_Cost_Avoidance_Y'+(i+1)+'__c']  + savingsTracker['Step3_Net_Savings_Hard_Savings_Y'+(i+1)+'__c'];
                if(i===0){
                    console.log('in if');
                    
                    savingsTracker['Step3_Net_Savings_Cost_Avoidance_Y'+(i+1)+'__c'] = savingsTracker['Step3_Net_Savings_Hard_Savings_Y'+(i+1)+'__c'];
                    savingsTracker['Step3_Net_Savings_Hard_Savings_Y'+(i+1)+'__c'] = 0;
                    
                }
            }
            else{
                 console.log('in else part');
                totalHardSavings += savingsTracker['Step3_Net_Savings_Hard_Savings_Y'+(i+1)+'__c'];
            	totalCostAvoidance += savingsTracker['Step3_Net_Savings_Cost_Avoidance_Y'+(i+1)+'__c'];
                console.log('in else totalHardSavings' +totalHardSavings);
                console.log('in else totalCostAvoidance'+totalCostAvoidance);
            }
           
            totalTotalSavings += savingsTracker['Step3_Net_Savings_Total_Savings_Y'+(i+1)+'__c'];            
        }
        var counter = component.get("v.counter");
        counter++;
        
        if(component.find('Baseline_Spend_Total_Value')){
        	component.find('Baseline_Spend_Total_Value').set("v.value",baselineSpendTotal);
        }
        savingsTracker['Baseline_Spend_Total_Value__c'] = baselineSpendTotal;
        
        if(component.find('Total_New_Spend')){
        	component.find('Total_New_Spend').set("v.value",newSpendTotal);
        }
        savingsTracker['Total_New_Spend__c'] = newSpendTotal;
        
        component.find('Step3_Total_Gross_Savings').set("v.value",grossSavingsTotal);
        savingsTracker['Step3_Total_Gross_Savings__c'] = grossSavingsTotal;
        savingsTracker['Step3_Total_Hard_Savings__c'] = totalHardSavings;
        savingsTracker['Step3_Total_Cost_Avoidance__c'] = totalCostAvoidance;
        savingsTracker['Step3_Total_of_Total_Savings__c'] = totalTotalSavings;
        savingsTracker['Hard_Savings_Total_Value__c'] = totalHardSavings;
        savingsTracker['Cost_Avoidance_Total_Value__c'] = totalCostAvoidance;
        savingsTracker['Step4Final_Bid_of_Awarded_Supplier__c'] = savingsTracker['Total_New_Spend__c'];
        savingsTracker['Step4_Price_Compression__c'] = (savingsTracker['Step4First_Valid_Bid_of_Awarded_Supplier__c'] || 0) - savingsTracker['Step4Final_Bid_of_Awarded_Supplier__c'];
        savingsTracker['Step4_Price_Compression_percent__c'] = savingsTracker['Step4First_Valid_Bid_of_Awarded_Supplier__c']?savingsTracker['Step4_Price_Compression__c']/savingsTracker['Step4First_Valid_Bid_of_Awarded_Supplier__c']*100:0;
        this.calculateSummaryFields(component, event);
        this.baselineSpendCapexShareChanged(component, event);
        this.hardSavingsCapexShareChanged(component, event);
        this.costAvoidanceCapexShareChanged(component, event);
        component.set("v.savingsTracker",savingsTracker);
        this.rerenderReadOnlyComponents(component, event, 'baseline-spend-year');
        this.rerenderReadOnlyComponents(component, event, 'new-spend-year');
        this.rerenderReadOnlyComponents(component, event, 'gross-savings-year');
        this.rerenderReadOnlyComponents(component, event, 'total-diff-in-volume-year');
        this.rerenderReadOnlyComponents(component, event, 'total-diff-in-price-year');
        this.rerenderReadOnlyComponents(component, event, 'incremental-diff-in-volume-year');
        this.rerenderReadOnlyComponents(component, event, 'incremental-diff-in-price-year');
        this.rerenderReadOnlyComponents(component, event, 'spend-diff-baseline-vs-new-year');
        this.rerenderReadOnlyComponents(component, event, 'price-impact-approx-year');
        this.rerenderReadOnlyComponents(component, event, 'volume-impact-approx-year');
        this.rerenderReadOnlyComponents(component, event, 'price-impact-percent-approx-year');
        this.rerenderReadOnlyComponents(component, event, 'volume-impact-percent-approx-year');
        this.rerenderReadOnlyComponents(component, event, 'price-impact-effective-year');
        this.rerenderReadOnlyComponents(component, event, 'volume-impact-effective-year');
        this.rerenderReadOnlyComponents(component, event, 'hard-savings-icremental-year');
        this.rerenderReadOnlyComponents(component, event, 'price-component-incremental-year');
        this.rerenderReadOnlyComponents(component, event, 'volume-component-incremental-year');
        this.rerenderReadOnlyComponents(component, event, 'cost-avoidance-non-icremental-year');
        this.rerenderReadOnlyComponents(component, event, 'price-component-non-incremental-year');
        this.rerenderReadOnlyComponents(component, event, 'volume-component-non-incremental-year');
        this.rerenderReadOnlyComponents(component, event, 'hard-savings-net-savings-year');
        this.rerenderReadOnlyComponents(component, event, 'cost-avoidance-net-savings-year');
        this.rerenderReadOnlyComponents(component, event, 'total-savings-net-savings-year');
        this.rerenderReadOnlyComponents(component, event, 'early-payment-discount-savings-year');
    },
    
    rerenderReadOnlyComponents: function(component, event, cmpAuraId){
        for(var i in component.find(cmpAuraId)){
            component.find(cmpAuraId)[i].readOnlyValueChanged();
        }
    },
    
    baselineSpendCapexShareChanged: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        savingsTracker.Baseline_Spend_Capex__c = savingsTracker.Baseline_Spend_Total_Value__c*(parseInt(savingsTracker.Baseline_Spend_Capex_Share__c) || 0)/100;
        savingsTracker.Baseline_Spend_Opex_Share__c = 100-(parseInt(savingsTracker.Baseline_Spend_Capex_Share__c) || 0);
        savingsTracker.Baseline_Spend_Opex__c = savingsTracker.Baseline_Spend_Total_Value__c-savingsTracker.Baseline_Spend_Capex__c;
        this.calculateSummaryFields(component, event);
        component.set("v.savingsTracker",savingsTracker);
    },
    
    hardSavingsCapexShareChanged: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        savingsTracker.Hard_Savings_Capex__c = savingsTracker.Hard_Savings_Total_Value__c*(parseInt(savingsTracker.Hard_Savings_Capex_Share__c) || 0)/100;
        savingsTracker.Hard_Savings_Opex_Share__c = 100-(parseInt(savingsTracker.Hard_Savings_Capex_Share__c) || 0);
        savingsTracker.Hard_Savings_Opex__c = savingsTracker.Hard_Savings_Total_Value__c-savingsTracker.Hard_Savings_Capex__c;
        this.calculateSummaryFields(component, event);
        component.set("v.savingsTracker",savingsTracker);
    },
    
    costAvoidanceCapexShareChanged: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        savingsTracker.Cost_Avoidance_Capex__c = savingsTracker.Cost_Avoidance_Total_Value__c*(parseInt(savingsTracker.Cost_Avoidance_Capex_Share__c) || 0)/100;
        savingsTracker.Cost_Avoidance_Opex_Share__c = 100-(parseInt(savingsTracker.Cost_Avoidance_Capex_Share__c) || 0);
        savingsTracker.Cost_Avoidance_Opex__c = savingsTracker.Cost_Avoidance_Total_Value__c-savingsTracker.Cost_Avoidance_Capex__c;
        this.calculateSummaryFields(component, event);
        component.set("v.savingsTracker",savingsTracker);
    },
       
    calculateSummaryFields: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        var syn1 = savingsTracker['Step_5_Synergy_Savings_Yr_1__c'];
        var syn2 = savingsTracker['Step_5_Synergy_Savings_Yr_2__c'];
        var syn3 = savingsTracker['Step_5_Synergy_Savings_Yr_3__c'];
        var syn4 = savingsTracker['Step_5_Synergy_Savings_Yr_4__c'];
        var syn5 = savingsTracker['Step_5_Synergy_Savings_Yr_5__c'];
        var syn6 = savingsTracker['Step_5_Synergy_Savings_Yr_6__c'];

        var blDeal = component.get("v.baselineDeal");
        for (var ii=1;ii<7;ii++){
            if(savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c']==null){
                savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c']=0;
            }else{
                savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c']=savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c'];
            }
        }
                
        //if((blDeal !== "Historical Cost") || (blDeal !== "X')){
        if(blDeal !== 'Historical Cost' && blDeal !== 'RFP Average'){
            savingsTracker['Hard_Savings_Amount__c'] = 0;
            savingsTracker['Cost_Avoidance_Amount__c'] = savingsTracker['Step3_Total_Cost_Avoidance__c'] + savingsTracker['Step3_Total_Hard_Savings__c'];
            savingsTracker['Total_Synergy_Savings__c'] =parseFloat(syn1) +parseFloat(syn2)+parseFloat(syn3)+parseFloat(syn4)+parseFloat(syn5)+parseFloat(syn6);


        }else{ 
			savingsTracker['Hard_Savings_Amount__c'] = savingsTracker['Step3_Total_Hard_Savings__c'];
            savingsTracker['Cost_Avoidance_Amount__c'] = savingsTracker['Step3_Total_Cost_Avoidance__c'];

        }
     
        savingsTracker['Total_Savings_Amount__c'] = savingsTracker['Step3_Total_Hard_Savings__c']+savingsTracker['Step3_Total_Cost_Avoidance__c'];
        savingsTracker['Price_Compression_Amount__c'] = savingsTracker['Step4_Price_Compression__c'];
        savingsTracker['Hard_Savings_Percentage__c'] = savingsTracker['Step3_Total_Hard_Savings__c']*100/savingsTracker['Baseline_Spend_Total_Value__c'];
        savingsTracker['Cost_Avoidance_Percentage__c'] = savingsTracker['Step3_Total_Cost_Avoidance__c']*100/savingsTracker['Baseline_Spend_Total_Value__c'];
        savingsTracker['Total_Savings_Percentage__c'] = savingsTracker['Hard_Savings_Percentage__c']+savingsTracker['Cost_Avoidance_Percentage__c'];
        savingsTracker['Price_Compression_Percentage__c'] = savingsTracker['Step4_Price_Compression_percent__c'];
        savingsTracker['Total_Synergy_Savings__c'] =parseFloat(syn1) +parseFloat(syn2)+parseFloat(syn3)+parseFloat(syn4)+parseFloat(syn5)+parseFloat(syn6);


    }
    
})
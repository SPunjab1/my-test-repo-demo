({
    doInit : function(component, event, helper) {
        //helper.fetchLocationDetails(component, event, helper,"DoInit");
    },
    checkLocationAndSwitch : function(component, event, helper) {
       if(helper.validateForm(component)) {
           helper.validateCIR(component, event, helper);
            //helper.fetchLocationDetails(component, event, helper,"Check");
       }
    },
    handleSubmit:function(component, event, helper) {
        component.set("v.spinner",true);
        helper.createIntakerequestRecord(component, event, helper);
    },
    validateSwitchId : function(component, event, helper) {
        component.set("v.spinner",true);
        var action = component.get("c.getSwitchDetails");
        action.setParams({switchId : component.get("v.IntakeRequest.Switch__c")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {   
            var result = response.getReturnValue();
                if($A.util.isEmpty(result) && component.get("v.IntakeRequest.Switch__c")!=null){
                    var msg = $A.get("$Label.c.Switch_id_not_matching");
                    msg = msg.replace('@switchId',component.get("v.IntakeRequest.Switch__c"));
                    helper.showToast('Warning',msg,'warning');
                    component.set("v.IntakeRequest.Switch__c",null);
                }
                else if(component.get("v.IntakeRequest.Switch__c")==null){
                	var msg = $A.get("$Label.c.Switch_Id_not_present");
                    helper.showToast('Error',msg,'error');     
                }
                else{
                	var msg =$A.get("$Label.c.Switch_id_validate_msg");
                    helper.showToast('Success',msg,'success');       
                }
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);
    },
     validateCaseNumber: function(component, event, helper) {
        component.set("v.spinner",true);
        var action = component.get("c.getTPEReqDetails");
        action.setParams({CaseNumberTemp : component.get("v.CaseObj")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {   
                var result = response.getReturnValue();
                if($A.util.isEmpty(result) && component.get("v.CaseObj")!=null){
                    var msg = $A.get("$Label.c.Case_Number_not_matching");
                    msg = msg.replace('@caseId',component.get("v.CaseObj"));
                    helper.showToast('Warning',msg,'warning');
                    component.set("v.CaseObj",null);
                }
                else if(component.get("v.CaseObj")==null){
                    var msg = $A.get("$Label.c.Case_Number_is_not_present");
                    helper.showToast('Error',msg,'error');     
                }
                else {
                    var msg =$A.get("$Label.c.Case_Number_validate_msg");
					component.set("v.CaseObj",result.CaseNumber);
                    helper.showToast('Success',msg,'success');       
                 }
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);  
    },
    
    handleCancel:function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    closeDatabaseDuplicateModel : function(component, event, helper) {
        component.set("v.isOpenDuplicateInDatabase", false);
        component.set("v.IntakeRequest.Location_ID__c",null);
        component.set("v.IntakeRequest.Workflow_Template__c",null);
        component.set("v.IntakeRequest.Project_Name__c",null);
        component.set("v.IntakeRequest.Project_Description__c",null);
        component.set("v.IntakeRequest.Task_Description__c",null);
        component.set("v.IntakeRequest.Advance_RAN_Transmission__c",false);
        component.set("v.IntakeRequest.Enterprise_Transmission_Design__c",false);
        component.set("v.IntakeRequest.Backhaul_Planning_Implementation__c",false);
        component.set("v.IntakeRequest.Transmission_Deployment_Walker__c",false);
        component.set("v.IntakeRequest.CIR__c",null);
        
    },
    continueActionForDatabaseDuplicate : function(component, event, helper) {
        component.set("v.spinner",true);
        component.set("v.isOpenDuplicateInDatabase", false);
        helper.createIntakerequestRecord(component, event, helper);
    }

})
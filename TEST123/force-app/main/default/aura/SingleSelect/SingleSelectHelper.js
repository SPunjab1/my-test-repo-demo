({
    fetchLocationDetails : function(component, event, helper,Str) {
        var action = component.get("c.getLocationDetails");
        action.setParams({ locationID : component.get("v.IntakeRequest.Location_ID__c"),
                          templateName :component.get("v.IntakeRequest.Workflow_Template__c")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {   
                var resultLoc = response.getReturnValue();
                if(!$A.util.isEmpty(resultLoc)) {
                    if(component.get("v.IntakeRequest.Switch__c")!=null){
                    	helper.fetchSwitchDetails(component, event, helper,resultLoc,Str);    
                    }
                    else{
                        for(var key in resultLoc) {
                            if(key == 'Yes' && Str == 'Check'){
                                var msg = $A.get("$Label.c.Duplicate_LocId_and_template");
                                msg = msg.replace('@locId',component.get("v.IntakeRequest.Location_ID__c"));
                                msg = msg.replace('@template',component.get("v.IntakeRequest.Workflow_Template__c"));
                                component.set("v.duplicateErrorMessgae", msg);
                                component.set("v.isOpenDuplicateInDatabase", true);
                                component.set("v.Location", resultLoc[key]);
                                component.set("v.isLocPresent", true);
                            }
                            else{
                                component.set("v.Location", resultLoc[key]);
                                component.set("v.isLocPresent", true);
                            }
                        }    
                    }
                }
                else{
                    component.set("v.isLocPresent", false);	
                    if(Str == 'Check' && component.get("v.IntakeRequest.Location_ID__c")!=null){
                        var msg = $A.get("$Label.c.Location_id_not_matching");
                        msg = msg.replace('@locId',component.get("v.IntakeRequest.Location_ID__c"));
                        helper.showToast('Warning',msg,'warning');
                        component.set("v.IntakeRequest.Location_ID__c",null);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchSwitchDetails : function(component, event, helper,resultLoc,Str) {
        var action = component.get("c.getSwitchDetails");
        action.setParams({switchId : component.get("v.IntakeRequest.Switch__c")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {   
                var result = response.getReturnValue();
                if(!$A.util.isEmpty(result)){
                    for(var key in resultLoc) {
                        if(key == 'Yes' && Str == 'Check'){
                            var msg = $A.get("$Label.c.Duplicate_LocId_and_template");
                            msg = msg.replace('@locId',component.get("v.IntakeRequest.Location_ID__c"));
                            msg = msg.replace('@template',component.get("v.IntakeRequest.Workflow_Template__c"));
                            component.set("v.duplicateErrorMessgae", msg);
                            component.set("v.isOpenDuplicateInDatabase", true);
                            component.set("v.Location", resultLoc[key]);
                            component.set("v.Switch", result);
                            component.set("v.isLocPresent", true);
                             component.set("v.isSwitchPresent", true);	
                        }
                        else{
                            component.set("v.Location", resultLoc[key]);
                            component.set("v.Switch", result);
                            component.set("v.isLocPresent", true);
                            component.set("v.isSwitchPresent", true);
                        }
                    }
                }
                else{
                    component.set("v.isSwitchPresent", false);	
                    if(Str == 'Check' && component.get("v.IntakeRequest.Switch__c")!=null){
                        var msg = $A.get("$Label.c.Switch_id_not_matching");
                        msg = msg.replace('@switchId',component.get("v.IntakeRequest.Switch__c"));
                        helper.showToast('Warning',msg,'warning');
                        component.set("v.IntakeRequest.Switch__c",null);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    createIntakerequestRecord :function(component, event, helper) {
        var switchId;
        if(component.get("v.Switch")!=null){
        	switchId = component.get("v.Switch");
        }
        else{
        	switchId = null;    
        }
        var action = component.get("c.createSingleSelectIntakeRec");
        action.setParams({ intakeReq : component.get("v.IntakeRequest"),
                          location : component.get("v.Location"),
                          switchId : switchId,
                          CaseNumberTemp : component.get("v.CaseObj")
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state ' +state);
            if (state === "SUCCESS") { 
                var result = response.getReturnValue();
                console.log('res '+result);
                if(!$A.util.isEmpty(result)) {  
                    var msg =$A.get("$Label.c.Intake_Successful_Message");
                    helper.showToast('Success',msg,'success');
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": result,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                    component.set("v.spinner",false);
                    $A.get('e.force:refreshView').fire();
                }
            }
            else{
                console.log('ERROR IN Create Intake request');
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);    
    },
    showToast: function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();  
    },
    validateForm : function(component){
        var validate = true;  
        var allValid = component.find('IntakeField').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        if(allValid){
            return(validate);       
        }
    },
    searchHelper : function(component,event,getInputkeyWord) {
        var action = component.get("c.fetchTPERequestNumber");
        action.setParams({
            'searchKeyWord': getInputkeyWord
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                component.set("v.spinner",false);
                if (response.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.listOfSearchRecords", response);
            }
            
        });
        $A.enqueueAction(action);        
    },
    
    validateCIR :function(component, event, helper) {
        var validate;
        var cirvalueExe;
        var cirtemplates = $A.get("$Label.c.CIR_Required");
        console.log('templatename'+cirtemplates);
        var templist = cirtemplates.split(',');
        console.log('tempt-->'+templist);
        console.log('tempt--> 0'+templist[0]);
        var wfTName = component.get("v.IntakeRequest.Workflow_Template__c");
        console.log('wft'+wfTName);
        var cirvalue = component.get("v.IntakeRequest.CIR__c");
        console.log('cirvalue'+cirvalue);
        for(var i =0;i<templist.length ;i++){
            console.log('in for loop');
            if(templist[i] == wfTName && (cirvalue== null || cirvalue == 'undefined')){
                console.log('in templist');
                validate = true;
            }
            if(templist[i] == wfTName && (cirvalue== 0 || cirvalue>100000)){
                console.log('in templist1');
                cirvalueExe = true;
               }
        }
        if(validate){
             console.log('svxbb');
            var msg = "CIR value is required for selected template";
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
            
        }else if(cirvalueExe==true){
            console.log('in cirvalue >');
            var msg = "CIR value should be between 1 to 10,000";
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
        }
        else{ 
            helper.fetchLocationDetails(component, event, helper,"Check");
        }
    }        
    
})
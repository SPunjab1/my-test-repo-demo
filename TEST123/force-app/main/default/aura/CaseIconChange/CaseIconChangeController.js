({
    doInit : function(cmp, event, helper) {
        var workspaceAPI = cmp.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId; 
            var action = cmp.get("c.getIcon");
            action.setParams({ caseId: response.recordId});
            action.setCallback(this,
                               $A.getCallback(function(response1)
                                              {
                                                  var state = response1.getState();    
                                                  console.log('state'+state);
                                                  if (state == "SUCCESS")  
                                                  {
                                                      var result= response1.getReturnValue();
                                                      console.log('result'+result);
                                                      var finalString=result.split("-");
                                                      workspaceAPI.setTabIcon({tabId: focusedTabId, icon: "action:"+ finalString[0], iconAlt: finalString[1]});   
                                                  }
                                              }
                ));
            $A.enqueueAction(action);       
            
        })
        
    }
})
({
    doInit: function(component,event,helper) {
        var action = component.get('c.getECLineItems');   
        action.setParams({
            'entitlementId' : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var result = response.getReturnValue();
            if(response.getState() === 'SUCCESS') {
                console.log(result);                
                component.set("v.EntitleWrapperlist",result.lstOfExiECLines);
                component.set("v.RADCenterList",result.lstOfExiRADCenterList);
                component.set("v.GroundSpaceList",result.lstOfExiGroundSpaceList);
                component.set("v.GeneratorList",result.lstOfExiGeneratorList);
                helper.calculateTotals(component,event);
                var action2 = component.get("c.getLeaseId");
                action2.setParams({
                    'siteID' : result.EcRecord.SiteID__c
                });
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var result2 = response.getReturnValue();
                        var leaseidmap = [];
                        for(var key in result2){
                            leaseidmap.push({key: key, value: result2[key]});
                        }
                        component.set("v.leaseidmap", leaseidmap);
                        component.set("v.selectedLeaseid",result.EcRecord.LeaseID__c);
                    }
                });
                $A.enqueueAction(action2);
            }
        });
        $A.enqueueAction(action);
    },
    addRow: function(component, event, helper) {
        helper.addEquipnEntitlementRecord(component, event);
    },
    addRADCenterList: function(component, event,helper) {
        var isValid = true;
        var reqfelds = [].concat(component.find('RADCenter'));
        var RADSqft = [].concat(component.find('RADSqft'));
        
        if(!$A.util.isEmpty(reqfelds) && reqfelds != null && reqfelds!=''){
            for(var i=0;i<reqfelds.length;i++){
                if($A.util.isEmpty(reqfelds[i].get("v.value")) || $A.util.isEmpty(RADSqft[i].get("v.value"))){
                    isValid = false;
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        mode: 'sticky',
                        message: 'Please fill all blank RAD Centers to add new row',
                        type : 'error'                
                    });
                    toastEvent.fire();
                    break; 
                }
            }
        }
        
        if(isValid){
            var RADCenterList = component.get("v.RADCenterList");
            var rad = component.get("v.RADGroundSpace");
            //rad.Type__c = 'RAD Center';
            RADCenterList.push(rad);
            component.set("v.RADCenterList", RADCenterList);
        }
        
    },
    addGroundSpaceList: function(component, event,helper) {
        var isValid = true;
        var grndType = [].concat(component.find('GroundSpaceType'));
        var grndSpace = [].concat(component.find('GroundSpace'));
        console.log('grndSpace***'+grndSpace);
        if(!$A.util.isEmpty(grndType) && grndType != null && grndType!=''){
            for(var i=0;i<grndType.length;i++){
                if(grndType[i].get("v.value")==null || grndSpace[i].get("v.value") == null){
                    isValid = false;
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        mode: 'sticky',
                        message: 'Please fill all blank Ground Spaces to add new row',
                        type : 'error'                
                    });
                    toastEvent.fire();
                    break; 
                }
            }
        }
        if(isValid){
            var GroundSpaceList = component.get("v.GroundSpaceList");
            var groundSpace = component.get("v.RADGroundSpace");
            //groundSpace.Type__c = 'Ground Space';
            GroundSpaceList.push(groundSpace);
            component.set("v.GroundSpaceList", GroundSpaceList);
        }
        
    },
    addGeneratorList: function(component, event,helper) {
        var isValid = true;
        //var generator = [].concat(component.find('Generator'));
        var generatorType = [].concat(component.find('GeneratorType'));
        var generatorTank = [].concat(component.find('GeneratorTank'));
        console.log('generatorType***'+generatorType);
        if(!$A.util.isEmpty(generatorType) && generatorType != null && generatorType != ''){
            for(var i=0;i<generatorType.length;i++){
                if(generatorType[i].get("v.value")==null || generatorTank[i].get("v.value") == null ){
                    isValid = false;
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error',
                        mode: 'sticky',
                        message: 'Please fill all blanks in Generators to add new row',
                        type : 'error'                
                    });
                    toastEvent.fire();
                    break; 
                }
            }
        }
        if(isValid){
            var GeneratorList = component.get("v.GeneratorList");
            var generator = component.get("v.RADGroundSpace");
            GeneratorList.push(generator);
            component.set("v.GeneratorList", GeneratorList);
        }
        
    },
    
    
    
    getleaseIdPicklist: function(component, event) {
        var SiteId = component.find("SiteId").get("v.value");
        if(!$A.util.isEmpty(SiteId) && SiteId != null && SiteId !=''){
            var action = component.get("c.getLeaseId");
            action.setParams({
                'siteID' : SiteId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    var leaseidmap = [];
                    for(var key in result){
                        leaseidmap.push({key: key, value: result[key]});
                    }
                    component.set("v.leaseidmap", leaseidmap);
                }
            });
            $A.enqueueAction(action);
        }
        else{
            alert('removing site id will revert leaseid and landlord');
            var leaseidmap = [];
            component.set("v.leaseidmap", leaseidmap);
            component.find('landlord').set('v.value', '');
        }
    },
    /* handleComponentEvent : function(component, event,helper) {
        // get the selected Equipment record from the COMPONENT event 	 
        var selectedEquipGetFromEvent = event.getParam("recordByEvent");
        var custinst = event.getParam("custinst");
        var EntitlementList = component.get("v.EntitleWrapperlist");
        EntitlementList[addRow.Index] = custinst;
        console.log(JSON.stringify(custinst));
        console.log('$$$$'+JSON.stringify(EntitlementList));
        component.set("v.EntitleWrapperlist", EntitlementList);
        helper.calculations(component, event);
        helper.calculateTotals(component,event);
    },*/
    handleComponentEvent : function(component, event,helper) {
        // get the selected Equipment record from the COMPONENT event 	 
        var selectedEquipGetFromEvent = event.getParam("recordByEvent");
        var custinst = event.getParam("custinst");
        var EntitlementList = component.get("v.EntitleWrapperlist");
        
        var equipmentlist =[] ;
        for (var i = 0; i < EntitlementList.length; i++) {
            var equpnam = EntitlementList[i].EquipmentName;
            if (!$A.util.isEmpty(equpnam) && !$A.util.isUndefinedOrNull(equpnam) ) {
                equipmentlist.push(equpnam);
            }
        }
        if(equipmentlist.includes(custinst.EquipmentName)){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                mode: 'sticky',
                message: 'Equipment cannot be duplicate',
                type : 'error'                
            });
            toastEvent.fire();
            var emptyinst= {};
            EntitlementList.splice(custinst.Index, 1);
            EntitlementList[custinst.Index] = emptyinst;
            component.set("v.EntitleWrapperlist", EntitlementList);
        }
        else{
            EntitlementList[custinst.Index] = custinst;
            console.log(JSON.stringify(custinst));
            console.log('$$$$'+JSON.stringify(EntitlementList));
            component.set("v.EntitleWrapperlist", EntitlementList);
            helper.calculations(component, event,EntitlementList);
            helper.calculateTotals(component,event);
        }
    },
    handleECPopEvent: function(component, event,helper) {
        var Indexpopup = event.getParam("Index");
        component.set("v.Indexpopup",Indexpopup);
        var Countpopup = event.getParam("Count");
        component.set("v.Countpopup",Countpopup);
        component.set("v.isOpen",true);
        setTimeout( function() {
            component.find( 'equipSearch' ).focus();
        }, 250);
    },
    
    CountChanged : function(component, event,helper){
        var index = event.getSource().get('v.name');
        var value = event.getSource().get('v.value');
        //alert(index+'$$'+value)
        var EntitlementList = component.get("v.EntitleWrapperlist");
        var lenght = EntitlementList.length;
        if(lenght>=(index+1)){
            var custinst;
            if(typeof EntitlementList[index] === "object"){
                custinst = EntitlementList[index];
            }
            else{
                EntitlementList[index] = {};
                custinst = EntitlementList[index];
            }
            //inst.TotalSurface= (!$A.util.isEmpty(value) && !$A.util.isUndefinedOrNull(value)) ? (parseFloat(inst.count) * parseFloat(inst.Length) * parseFloat(inst.Width)): 0;
            
            var tsf = (!$A.util.isEmpty(value) && !$A.util.isUndefinedOrNull(value)) ? parseFloat(value)*parseFloat(custinst.Length)*parseFloat(custinst.Width):0;
            var tw = (!$A.util.isEmpty(value) && !$A.util.isUndefinedOrNull(value)) ? parseFloat(value)*parseFloat(custinst.Weight):0;
            if(!(tsf==0 && tw ==0)){
                custinst.TotalSurface = tsf>0?tsf.toFixed(2):0;
                custinst.TotalWeight = tsf>0?tw.toFixed(2):0;
            }
            custinst.Count=value;
            EntitlementList[custinst.Index] = custinst;
            component.set("v.EntitleWrapperlist", EntitlementList);
        }
        helper.calculations(component, event);
        helper.calculateTotals(component,event);
    },
    /*removeRow: function(component, event, helper) {
        var EntitlementList = component.get("v.EntitleWrapperlist");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        EntitlementList.splice(index, 1);
        component.set("v.EntitleWrapperlist", EntitlementList);
        helper.calculations(component, event);
        helper.calculateTotals(component,event);
    },*/
    removeRow: function(component, event, helper) {
        var EntitlementList = component.get("v.EntitleWrapperlist");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        let deleteAction =  component.get("c.deleteRecord");
        deleteAction.setParams({recordId:EntitlementList[index].entitleid});
        deleteAction.setCallback(this, function(response) {
            //$A.get("e.force:refreshView").fire();
        });
        $A.enqueueAction(deleteAction);
        EntitlementList.splice(index, 1);
        
        console.log('Remove::',JSON.stringify(EntitlementList));
        //#########################################33
        var length = EntitlementList.length;
        for (var i = 0; i < EntitlementList.length; i++) {
            if(EntitlementList[i].Index > index){     
                EntitlementList[i].Index = EntitlementList[i].Index-1;
            }
        }
        console.log('Remove::'+JSON.stringify(EntitlementList));
        //alert('Remove::'+JSON.stringify(EntitlementList));
        component.set("v.EntitleWrapperlist", EntitlementList);
        helper.calculations(component, event,EntitlementList);
        helper.calculateTotals(component,event);
    },
    leaseidChange: function(component, event, helper) {
        //var leaseid = component.find("leaseid").get("v.value");
        var leaseid = component.get("v.selectedLeaseid");
        var action = component.get('c.fetchLeaseinfo');
        action.setParams({
            'leaseId' : leaseid
        });
        action.setCallback(this,function(response){
            var result = response.getReturnValue();
            //alert('result**'+result);
            if(response.getState() === 'SUCCESS') {
                component.find('landlord').set('v.value', result);
            } else { 
                // If server throws any error
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    component.set('v.message', errors[0].message);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    checkAggreement : function(component,event,helper){
        var agree = component.find("Agreement").get("v.value");
        if(!$A.util.isEmpty(agree) && agree != null && agree !='' && agree !== 'New Lease'){
            component.set("v.leaseRequBool",true);
        }
        else{
            component.set("v.leaseRequBool",false);
        }
    },    
    save: function(component, event, helper) {
        var SiteId = component.find("SiteId").get("v.value");
        var Agreement = component.find("Agreement").get("v.value");
        var Status = component.find("AgreementStatus").get("v.value");
        var Audit = component.find("Audit").get("v.value");
        var landlord = component.find("landlord").get("v.value");
        var leaseid = component.get("v.selectedLeaseid");
        var Antennas = component.find("Antennas").get("v.value");
        var Coax = component.find("Coax").get("v.value");
        var TMA = component.find("TMA").get("v.value");
        var Hybrid = component.find("Hybrid").get("v.value");
        var RRU = component.find("RRU").get("v.value");
        var JBox = component.find("JBox").get("v.value");
        var MWDish = component.find("MWDish").get("v.value");
        var ODU = component.find("ODU").get("v.value");
        var Diplexer = component.find("Diplexer").get("v.value");
        if(SiteId != null && Agreement != null && Status != null && ((Agreement !== 'New Lease' && leaseid !=null && leaseid != '' )|| (Agreement != null && Agreement === 'New Lease'))){
            var newECRecord = {
                SiteID__c:SiteId,
                Agreement__c:Agreement,
                Agreement_Status__c:Status,
                Audit_Status__c:Audit,
                Landlord__c:landlord,
                /*LeaseId__c:leaseid,*/
                Antennas__c:Antennas,
                Coax__c:Coax,
                TMA__c:TMA,
                Hybrid__c:Hybrid,
                RRU__c:RRU,
                J_Box__c:JBox,                        
                MWDish__c:MWDish,
                ODU__c:ODU,
                Diplexer__c:Diplexer
            }; 
            if(component.get("v.recordId") != null && component.get("v.recordId") != undefined){
                newECRecord.Id = component.get("v.recordId");       
            }             
            component.set("v.EntitlementCapture",newECRecord);
            
            var EntitlementList = component.get("v.EntitleWrapperlist");
            
            var isValid = true;
            for (var i = 0; i < EntitlementList.length; i++) {
                var equpnam = EntitlementList[i].EquipmentName;
                var count =   EntitlementList[i].Count;
                //alert('count***'+count);
                if (!$A.util.isEmpty(equpnam) && !$A.util.isUndefinedOrNull(equpnam) && !$A.util.isUndefinedOrNull(count) && !$A.util.isEmpty(count)) {
                    isValid = true;
                    //alert('true1');
                }
                else{
                    isValid = false;
                    //alert('false');
                }
            }    
            
            if(isValid){ 
                var ECRecordString = JSON.stringify(newECRecord);
                var ECLRecordString = JSON.stringify(EntitlementList);
                helper.radGroundGenInst(component,event);
                console.log('test#####');
                var action = component.get("c.SaveECnECLnRADGrnd");
                var RADGroundWrapperList = component.get("v.RADGroundWrapperList");
                console.log('#############');
                var RADGroundStrings = JSON.stringify(RADGroundWrapperList);  
                console.log('ECRecordString$$$$'+ECRecordString);
                console.log('ECLRecordString$$$$'+ECLRecordString);
                console.log('RADGroundStrings$$$$'+RADGroundStrings);
                action.setParams({
                    "ECRecordString":ECRecordString,
                    "ECLRecordsStrings":ECLRecordString,
                    "RADGroundStrings":RADGroundStrings
                });
                
                action.setCallback(this, function(result){
                    var state = result.getState();
                    if (component.isValid() && state === "SUCCESS"){
                        //alert('Success in calling server side action');
                        var ECRecord = result.getReturnValue();
                        //alert('ECRecord####'+ECRecord);
                        
                        /*
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                          "url": "/lightning/r/Entitlement_Capture__c/"+ECRecord+"/view"
                        });
                        urlEvent.fire();
                        */
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": ECRecord,
                            "slideDevName": "related"
                        });
                        navEvt.fire();
                        window.setTimeout(
                            $A.getCallback(function() {
                                $A.get('e.force:refreshView').fire();
                            }), 2000
                        );
                    }
                    
                    else if(state == "ERROR"){
                        var errors = action.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                //alert(errors[0].message);
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title : 'Error',
                                    mode: 'sticky',
                                    message: errors[0].message,
                                    type : 'error'                
                                });
                                toastEvent.fire();
                            }
                        }
                    }
                        else if (status === "INCOMPLETE") {
                            alert('No response from server or client is offline.');
                        }
                });
                $A.enqueueAction(action);
                
            }else{
                //alert('fill all blank row');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    mode: 'sticky',
                    message: 'Please fill required fields on Equipment to save',
                    type : 'error'                
                });
                toastEvent.fire();
                //$A.get('e.force:refreshView').fire();
            }
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                mode: 'sticky',
                message: 'Please fill all Required Values of Entitilement Capture',
                type : 'error'                
            });
            toastEvent.fire();
        }  
        //$A.get("e.force:refreshView").fire();
    },
    cancel: function (component, event, helper) {
         $A.get("e.force:refreshView").fire();
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/lightning/r/Entitlement_Capture__c/"+component.get("v.recordId")+"/view"
        });
        urlEvent.fire();
    },
    calculateWidth : function(component, event, helper) {
        var childObj = event.target
        var parObj = childObj.parentNode;
        var count = 1;
        //parent element traversing to get the TH
        while(parObj.tagName != 'TH') {
            parObj = parObj.parentNode;
            count++;
        }
        console.log('final tag Name'+parObj.tagName);
        //to get the position from the left for storing the position from where user started to drag
        var mouseStart=event.clientX; 
        component.set("v.mouseStart",mouseStart);
        component.set("v.oldWidth",parObj.offsetWidth);
    },
    
    setNewWidth : function(component, event, helper) {
        var childObj = event.target
        var parObj = childObj.parentNode;
        var count = 1;
        //parent element traversing to get the TH
        while(parObj.tagName != 'TH') {
            parObj = parObj.parentNode;
            count++;
        }
        var mouseStart = component.get("v.mouseStart");
        var oldWidth = component.get("v.oldWidth");
        //To calculate the new width of the column
        var newWidth = event.clientX- parseFloat(mouseStart)+parseFloat(oldWidth);
        parObj.style.width = newWidth+'px';//assign new width to column
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
        
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        
    },
    
    keyPressController : function(component, event, helper) {
        var getInputkeyWord = component.get("v.SearchKeyWord");
        var action = component.get("c.getEquipements");
        action.setParams({
            "searchString":getInputkeyWord
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                var EquipRecord = result.getReturnValue();
                component.set("v.EquipmntList",EquipRecord);
                component.set("v.SearchKeyWord",null);
            }
            else if(state == "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        //alert(errors[0].message);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error',
                            mode: 'sticky',
                            message: errors[0].message,
                            type : 'error'                
                        });
                        toastEvent.fire();
                    }
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    selectEquiSelect: function(component, event, helper) {
        //alert('###');
        var selectedId = event.target.getAttribute('data-recid'); 
        //alert('record id is '+recordid);
        
        // var selectedId = event.target.dataset.id;  
        var Index = component.get("v.Indexpopup");
        var Count = component.get("v.Countpopup");
        
        //var selectedId = event.currentTarget.dataset.id; //it will return thisDiv
        //var getTargetedButton = component.find(id); //it will return the source button
        
        //ECPopupeventResponse
        var appEvent = $A.get("e.c:ECPopupeventResponse");
        var selectedRecords = [];
        var EquipmntList = component.get("v.EquipmntList");
        var selectedRec = component.get("v.recordByEvent") 
        for (var i = 0; i < EquipmntList.length; i++) {
            console.log('$$$$'+EquipmntList[i].Id+'%%%%'+selectedId);
            if (EquipmntList[i].Id === selectedId) {
                selectedRec= EquipmntList[i];
            }
        }
        
        //alert('selectedRec***'+selectedRec+'##'+Index+'$$'+Count);
        component.set("v.isOpen",false);
        component.set("v.EquipmntList",null);
        appEvent.setParams({
            "selectedRecord" : selectedId,
            "Index" : Index,
            "Count": Count,
            "recordByEvent":selectedRec
        })
        appEvent.fire();
    } 
})
({
    addEquipnEntitlementRecord: function(component, event) {
        var EntitlementList = component.get("v.EntitleWrapperlist");
        // var lenght = EntitlementList.length;
        
        var isValid = true;
        var entitleList = component.get("v.EntitlementLine");
        for (var i = 0; i < EntitlementList.length; i++) {
            var equpnam = EntitlementList[i].EquipmentName;
            var count =   EntitlementList[i].Count;
            //alert('count***'+count);
            if (!$A.util.isEmpty(equpnam) && !$A.util.isUndefinedOrNull(equpnam) && !$A.util.isUndefinedOrNull(count) && !$A.util.isEmpty(count)) {
                isValid = true;
            }
            else{
                isValid = false;
            }
            
        }
        if(isValid){
            //var rpWrapperInstance = component.get("v.EntitleWrapper");
            var emptyinst= {};
            EntitlementList.push(emptyinst);
            component.set("v.EntitleWrapperlist", EntitlementList);
        }
        else{
            //alert('fill all blank row');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                mode: 'sticky',
                message: 'Please fill/remove all blank rows to add new row',
                type : 'error'                
            });
            toastEvent.fire();
        }
        
    },
    
    radGroundGenInst : function(component,event){
        console.log('22222222###');
        var RADGroundWrapperList = component.get("v.RADGroundWrapperList");
        
        //RAD instance
        var RADCenter = [].concat(component.find('RADCenter'));
        var RADSqft = [].concat(component.find('RADSqft'));
        var RAD = [].concat(component.find('RADCenterForm'));
        if(!$A.util.isEmpty(RADCenter) && RADCenter != null && RADCenter!=''){
            for(var i=0;i<RADCenter.length;i++){
                if(RADCenter[i].get("v.value")==null || RADCenter[i].get("v.value")){
                    var inst = {};
                    inst.RADCenter = RADCenter[i].get("v.value");
                    inst.RADSqft = RADSqft[i].get("v.value");
                    inst.SelectedType = 'RAD Center';
                    if(RAD[i].get("v.recordId") != null && RAD[i].get("v.recordId") != undefined){
                    	inst.recordId = RAD[i].get("v.recordId");    
                    }
                    RADGroundWrapperList.push(inst);
                }
            }
        }
        
        //Ground Space instance
        var GroundSpaceType = [].concat(component.find('GroundSpaceType'));
        var GroundSpace = [].concat(component.find('GroundSpace'));
        var GroundSp = [].concat(component.find('GroundSpaceForm'));
        console.log('GroundSpace***'+GroundSpace);
        if(!$A.util.isEmpty(GroundSpaceType) && GroundSpaceType != null && GroundSpaceType !=''){
            for(var i=0;i<GroundSpaceType.length;i++){
                if(GroundSpaceType[i].get("v.value")==null || GroundSpace[i].get("v.value")){
                    var inst = {};
                    inst.SelectedType = 'Ground Space';
                    inst.GroundSpaceType = GroundSpaceType[i].get("v.value");
                    inst.GroundSpace = GroundSpace[i].get("v.value");
                    if(GroundSp[i].get("v.recordId") != null && GroundSp[i].get("v.recordId") != undefined){
                    	inst.recordId = GroundSp[i].get("v.recordId");    
                    }
                    RADGroundWrapperList.push(inst);
                }
            }
        }
        //Generator instance
        //var generator = [].concat(component.find('Generator'));
        var generatorType = [].concat(component.find('GeneratorType'));
        var generatorTank = [].concat(component.find('GeneratorTank'));
        var generator = [].concat(component.find('GeneratorForm'));
        console.log('generatorType***'+generatorType);
        if(!$A.util.isEmpty(generatorType) && generatorType != null && generatorType !=''){
            for(var i=0;i<generatorType.length;i++){
                if(generatorType[i].get("v.value") || generatorTank[i].get("v.value") ){
                    var inst = {};
                    inst.SelectedType = 'Generator';
                    //inst.generator = generator[i].get("v.value");
                    inst.generatorType = generatorType[i].get("v.value");
                    inst.GeneratorTank = generatorTank[i].get("v.value");
                    if(generator[i].get("v.recordId") != null && generator[i].get("v.recordId") != undefined){
                    	inst.recordId = generator[i].get("v.recordId");    
                    }
                    RADGroundWrapperList.push(inst);
                }
            }
        }
        console.log('2.5.5.5.5###');
        component.set("v.RADGroundWrapperList", RADGroundWrapperList);  
        console.log('3333333###');
       // alert(JSON.stringify(RADGroundWrapperList));
        
    },    
    
    calculateTotals : function(component, event) {
        
        var EntitlementList = component.get("v.EntitleWrapperlist");
        var listLength = EntitlementList.length;
        var totalsurface = 0.00;
        var totalWeight = 0.00;
        for (var i = 0; i < listLength; i++) {
            totalsurface = totalsurface+ parseFloat(EntitlementList[i].TotalSurface);
            totalWeight = totalWeight + parseFloat(EntitlementList[i].TotalWeight);
            
        }
		var totalsurfaceSummery = (!$A.util.isEmpty(totalsurface) && !$A.util.isUndefinedOrNull(totalsurface) && totalsurface !== 0) ? parseFloat(totalsurface).toFixed(2) :0;
        var totalWeightSummery = (!$A.util.isEmpty(totalWeight) && !$A.util.isUndefinedOrNull(totalWeight) && totalWeight !== 0) ? parseFloat(totalWeight).toFixed(2) :0;
        
        component.set("v.totalSurface",totalsurfaceSummery);
        component.set("v.totalWeight",totalWeightSummery);
    },
    calculations : function(component, event) {
        
        var EntitlementList = component.get("v.EntitleWrapperlist");
        console.log(JSON.stringify(EntitlementList));
        var listLength = EntitlementList.length;
        var TMA = 0;
        var Anteena = 0;
        var Hybrid = 0;
        var MWDish = 0;
        var Coax = 0;
        var RRU = 0;
        var ODU = 0;
        var JBox = 0;
        var Diplexer = 0;
        for (var i = 0; i < listLength; i++) {
            var count = EntitlementList[i].Count != 'undefined' &&  EntitlementList[i].Count != null ? parseInt(EntitlementList[i].Count) : 0;
            console.log('equipment Type***'+EntitlementList[i].EquipmentType);
            var equiptype = EntitlementList[i].EquipmentType;
            console.log('###'+equiptype);
            
            if(equiptype == 'TMA'){
                //console.log('in TMA');
                TMA = TMA + count;  
            }
            else if(equiptype == 'Antenna'){
                console.log('in Antenna'+count);
                Anteena = Anteena + count;
                console.log('in Antenna**'+Anteena);
                
            }
                else if(EntitlementList[i].EquipmentType == 'Hybrid/Fiber'){
                    Hybrid = Hybrid + count;
                }
                    else if(EntitlementList[i].EquipmentType == 'Microwave'){
                        MWDish = MWDish + count;
                    }
                        else if(EntitlementList[i].EquipmentType == 'Coax'){
                            Coax = Coax + count;
                        }
                            else if(EntitlementList[i].EquipmentType == 'RRU'){
                                RRU = RRU + count;
                            }
                                else if(EntitlementList[i].EquipmentType == 'ODU'){
                                    ODU = ODU + count;
                                }
                                    else if(EntitlementList[i].EquipmentType == 'Junction Box'){
                                        JBox = JBox + count;
                                    }
                                        else if(EntitlementList[i].EquipmentType == 'Diplexer'){
                                            Diplexer = Diplexer + count;
                                        }
            //alert(EntitlementList[i].EquipmentType); // which will return specific field
        }
        //var Antennas = component.find("Antennas").get("v.value");
        console.log('TMA**'+TMA);
        console.log('Anteena**'+Anteena);
        console.log('Hybrid**'+Hybrid);
        console.log('JBox**'+JBox);
        console.log('Coax**'+Coax);
        component.find('TMA').set('v.value', TMA);
        component.find('Antennas').set('v.value', Anteena);
        component.find('Coax').set('v.value', Coax);
        component.find('Hybrid').set('v.value', Hybrid);
        component.find('RRU').set('v.value', RRU);
        component.find('JBox').set('v.value', JBox);
        component.find('MWDish').set('v.value', MWDish);
        component.find('ODU').set('v.value', ODU);
        component.find('Diplexer').set('v.value', Diplexer);
        component.set("v.EntitleWrapperlist", EntitlementList);
    },
    validateform: function(component) {
        console.log('Concat***'+[].concat);
        var reqfelds = [].concat(component.find('RADCenter'));
        console.log('reqfelds***'+reqfelds);
        var RADSqft = [].concat(component.find('RADSqft'));
        console.log('RADSqft***'+RADSqft);
        for(var i=0;i<reqfelds.length;i++){
            console.log('###'+(reqfelds[i].get("v.value")));
        }
        return true;
    },
    showerrorToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Required Field Error!",
            "type": "error",
            "message": "add your standard error message."
        });
        toastEvent.fire();
    },
    /*validateform: function(component) {
        var reqfelds = [].concat(component.find('count'));
        console.log('reqfelds***'+reqfelds);
        var EquipModel = [].concat(component.find('EquipModel'));
        var ECline = [];
        var Equiptype = [].concat(component.find('Equiptype'));
        var Length = [].concat(component.find('Length'));
        var width = [].concat(component.find('width'));
        var depth = [].concat(component.find('depth'));
        var weight = [].concat(component.find('weight'));
        var ltimesw = [].concat(component.find('ltimesw'));
        var totalsurface = [].concat(component.find('totalsurface'));
        var totalweight = [].concat(component.find('totalweight'));
        
        for(var i=0;i<EquipModel.length;i++){
            console.log('####'+EquipModel[i].get("v.value"));
            console.log('reqfelds####'+reqfelds[i].get("v.value"));
            ECline.push({'sobjectType':'Entitlement_Line_Items__c',
                         'Count__c':reqfelds[i].get("v.value"),
                         'Equipment_Model__c':EquipModel[i].get("v.value"),
                         'Equipment_Type__c' :Equiptype[i].get("v.value"),
                         'Length__c':Length[i].get("v.value"),
                         'Width__c':width[i].get("v.value"),
                         'Depth__c':depth[i].get("v.value"),
                         'Weight__c':weight[i].get("v.value"),
                         'L_Times_W__c':ltimesw[i].get("v.value"),
                         'Total_Surface__c':totalsurface[i].get("v.value"),
                         'Total_Weight__c':totalweight[i].get("v.value"),
                        });
            
            if($A.util.isEmpty(EquipModel[i].get("v.value")) || $A.util.isEmpty(reqfelds[i].get("v.value"))){
                console.log('@@@@');
                alert('Row cannot be blank on row number ' + (i + 1));
                return false;
            } 
            
        }
        component.set("v.EntitlementLine",ECline);
        return true;
    },
    showerrorToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Required Field Error!",
            "type": "error",
            "message": "add your standard error message."
        });
        toastEvent.fire();
    },*/
    validateEquipList: function(component, event) {
        //Validate all Entitlement records
        var isValid = true;
        var entitleList = component.get("v.EntitlementLine");
        for (var i = 0; i < entitleList.length; i++) {
            if (entitleList[i].Name == '') {
                isValid = false;
                alert('Row cannot be blank on row number ' + (i + 1));
            }
        }
        return isValid;
    }
})
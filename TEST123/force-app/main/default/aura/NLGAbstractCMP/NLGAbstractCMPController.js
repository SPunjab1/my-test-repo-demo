({
	savepdf : function(component, event, helper) {
       var pgref = component.get("v.pageReference");
       var recordId = pgref.state.c__recordId; 
	   var acqVal = pgref.state.c__acqVal; 
       var dateSring = pgref.state.c__dateSring; 
       var agrmtHighlights = pgref.state.c__agrmtHighlights; 
       var rrapp = pgref.state.c__rrapp;
       var rent =  pgref.state.c__rent;
       var sign = pgref.state.c__sign;
       
        
        
       helper.savepdfHelper(component,event,recordId,acqVal,dateSring,agrmtHighlights,rrapp,rent,sign);
		
	},
    
    closeAction :  function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})
({
    savepdfHelper : function(component,event,recordId,acqVal,dateSring,agrmtHighlights,rrapp,rent,sign) {
        
        
        var action = component.get("c.generateNewContentVersion");
        action.setParams({"oppId" : recordId,
                          "acqValapex" : acqVal,
                          "dateSringapex" : dateSring, 
                          "agrmtHighlightsapex" : agrmtHighlights, 
                          "rrappapex" : rrapp, 
                          "rentincreaseapex" : rent,
                          "signapex"  : sign
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type" : "success",
                    "message": "NLG Abstract PDF generated & saved successfully"
                });
                toastEvent.fire();
                /*    component.find("navigation")
                .navigate({
                    "type" : "standard__recordPage",
                    "attributes": {
                        "recordId"      : recordId,
                        "actionName"    :  "view" 
                    }
                }, true);*/
                window.setTimeout(
                    $A.getCallback(function() {
                        window.open('/lightning/r/Opportunity/'+recordId+ '/view', '_self');
                    }), 4000
                );
                
                //    $A. get('e. force:refreshView'). fire();
            }
            else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type" : "error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})
({
    saveAndClose : function(component, event) {
        var helper = this;
        helper.save(component, event, true);
    },
    
    saveAndContinue : function(component, event) {
        var helper = this;
        helper.save(component, event, false);
    },
    
    save: function(component, event, isClose){
        
        var helper = this;
        var requestId = component.get("v.requestId");
        var savingsTrackerId = component.get("v.savingsTrackerId");
        var savingsTracker = component.get("v.savingsTracker");
        if(!savingsTracker.Id && requestId){
            savingsTracker.Request__c = requestId;
        }
        
        if(savingsTracker.One_List_Project_id__c==null&&(savingsTracker.Step_5_Synergy_Savings_Yr_6__c>0||savingsTracker.Step_5_Synergy_Savings_Yr_5__c>0||savingsTracker.Step_5_Synergy_Savings_Yr_4__c>0||savingsTracker.Step_5_Synergy_Savings_Yr_3__c>0||savingsTracker.Step_5_Synergy_Savings_Yr_2__c>0||savingsTracker.Step_5_Synergy_Savings_Yr_1__c>0)){
            alert("You must enter One list project id if there are synergy savings to save this value");
        }
        else if(savingsTracker.One_List_Project_id__c>0&&((savingsTracker.Step_5_Synergy_Savings_Yr_6__c<=0 ||savingsTracker.Step_5_Synergy_Savings_Yr_6__c==null)
                                                              &&(savingsTracker.Step_5_Synergy_Savings_Yr_5__c<=0||savingsTracker.Step_5_Synergy_Savings_Yr_5__c==null) 
                                                              && (savingsTracker.Step_5_Synergy_Savings_Yr_4__c<=0 ||savingsTracker.Step_5_Synergy_Savings_Yr_4__c==null)
                                                              && (savingsTracker.Step_5_Synergy_Savings_Yr_3__c<=0 ||savingsTracker.Step_5_Synergy_Savings_Yr_3__c==null)
                                                              && (savingsTracker.Step_5_Synergy_Savings_Yr_2__c<=0 ||savingsTracker.Step_5_Synergy_Savings_Yr_2__c==null)
                                                              && (savingsTracker.Step_5_Synergy_Savings_Yr_1__c<=0||savingsTracker.Step_5_Synergy_Savings_Yr_1__c==null))){
            alert("Synergy Savings must be >$0 or must be populated to have one list id save");
            savingsTracker.One_List_Project_id__c=null;
            
        }



        else if(savingsTracker.One_List_Project_id__c>9999 ){
            alert("One List project id must be up to 4 digits long to save this value");

        }
        
        var action = component.get("c.saveSavingsTracker");
        action.setParams({savingsTracker:savingsTracker});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var savingsTrackerResponse = response.getReturnValue();

            if(state === "SUCCESS"){
                if(isClose){
                    helper.close(component, event);
                }else{
                    if(!savingsTrackerId){
                        component.set("v.savingsTrackerId", savingsTrackerResponse.Id);
                        savingsTracker.Id = savingsTrackerResponse.Id;
                        component.get("v.savingsTracker",savingsTracker);
                    }
                    component.set("v.message","Form saved successfully!");
                    component.set("v.messageType","confirm");
                    component.set("v.messageTitle","SUCCESS");
                }
            }else if(state === "ERROR"){
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    var message = '';
                    for(var err in errors[0].pageErrors){
                        message += message ? '<br/>'+errors[0].pageErrors[err].message : errors[0].pageErrors[err].message;
                    }
                    for(var fieldErr in errors[0].fieldErrors){
                        for(var err in errors[0].fieldErrors[fieldErr]){
                            message += message ? '<br/>'+errors[0].fieldErrors[fieldErr][err].message : errors[0].fieldErrors[fieldErr][err].message;
                        }
                    }
                    component.set("v.message",message);
                }
                component.set("v.messageType","error");
                component.set("v.messageTitle","ERROR");
            }
        });
        $A.enqueueAction(action);
    },
    
    /*
    countRequestAttachments: function(component, event) {
        var helper = this;
        var requestId = component.get("v.requestId");
        var action = component.get("c.countRequestAttachments");
        action.setParams({requestID:requestId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var count = response.getReturnValue();
            if(state === "SUCCESS"){
                if(count > 4){
                    helper.goToSavingsTrackerDetail(component, event);
                }else{
                    var message = 'A Request must have 5 Attachments in order to submit the associated Savings Tracker.<br/>Current Attachment Count: ';
                    message += count;
                    component.set("v.message",message);
                    component.set("v.messageType","error");
                    component.set("v.messageTitle","ERROR");
                }
            }else if(state === "ERROR"){
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    var message = '';
                    for(var err in errors[0].pageErrors){
                        message += message ? '<br/>'+errors[0].pageErrors[err].message : errors[0].pageErrors[err].message;
                    }
                    for(var fieldErr in errors[0].fieldErrors){
                        for(var err in errors[0].fieldErrors[fieldErr]){
                        	message += message ? '<br/>'+errors[0].fieldErrors[fieldErr][err].message : errors[0].fieldErrors[fieldErr][err].message;
                        }
                    }
                    component.set("v.message",message);
                }
                component.set("v.messageType","error");
                component.set("v.messageTitle","ERROR");
            }
        });
        $A.enqueueAction(action);
    },*/
    
    goToSavingsTrackerDetail: function(component, event){
        var savingsTrackerId = component.get("v.savingsTrackerId");
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent.setParams({"recordId": savingsTrackerId,"slideDevName": "detail"});
        sObjectEvent.fire();
        if(sforce && sforce.one){
            sforce.one.navigateToSObject(savingsTrackerId,"detail");
        }
    },
    
    close: function(component, event){
        var requestId = component.get("v.requestId");
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent.setParams({"recordId": requestId,"slideDevName": "detail"});
        sObjectEvent.fire();
        if(sforce && sforce.one){
            sforce.one.navigateToSObject(requestId,"detail");
        }
    },
    
    onChangeDealDate: function(component, event) {
        var years = [];
        var savingsTracker = component.get("v.savingsTracker");
        if(savingsTracker.Step1_Deal_Start_Date__c && savingsTracker.Step2_Deal_End_Date__c){
            var startDate = new Date(savingsTracker.Step1_Deal_Start_Date__c);
            var endDate = new Date(savingsTracker.Step2_Deal_End_Date__c);
            var dealEndDateField = component.find('dealEndDate');
            if(startDate>endDate){
                dealEndDateField.setCustomValidity('Deal End Date should be greater than Deal Start Date.');
                return;
            }
            //calculate months difference
            var durationInMonths;
            durationInMonths = (endDate.getFullYear() - startDate.getFullYear()) * 12;
            durationInMonths += endDate.getMonth()-startDate.getMonth()-(endDate.getDate()>startDate.getDate()?0:1);
            durationInMonths = durationInMonths < 0 ? 0 : durationInMonths+1;
            savingsTracker.Step1_Deal_Duration__c = durationInMonths;
            //calculate years difference
            var yearsDiff = Math.ceil(durationInMonths/12);
            if(yearsDiff>10){
                dealEndDateField.setCustomValidity('Difference between Deal End Date & Deal Start Date should not be more than 10 years.');
                return;
            }
            dealEndDateField.setCustomValidity('');
            for(var i=1; i<=yearsDiff; i++){
                years.push(i);
            }
        }
        //effective date is same as start date
        // if(savingsTracker.Step1_Deal_Start_Date__c){
        // savingsTracker.Savings_Effective_Date__c = savingsTracker.Step1_Deal_Start_Date__c;
        // }
        component.set("v.years",years);
        component.set("v.yearsDiff", yearsDiff);
        component.set("v.savingsTracker",savingsTracker);
    },
    
    prepareFieldOptions: function(component, event) {
        var helper = this;
        var action = component.get("c.prepareFieldOptions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsOptions = response.getReturnValue();
                component.set("v.fieldsOptions",fieldsOptions);
                component.set("v.savingsStatusFieldOptions",helper.parseFieldOptions(fieldsOptions,'Savings_Status__c'));
                component.set("v.typeOfBusinessFieldOptions",helper.parseFieldOptions(fieldsOptions,'Type_of_Baseline_for_this_deal__c'));
                component.set("v.oldFinalPaymentTermsFieldOptions",helper.parseFieldOptions(fieldsOptions,'Step2_Old_Final_Payment_Terms__c'));
                component.set("v.newFinalPaymentTermsFieldOptions",helper.parseFieldOptions(fieldsOptions,'Step2_New_Final_Payment_Terms__c'));
                component.set("v.forecastedVolumeSourceFieldOptions",helper.parseFieldOptions(fieldsOptions,'Step3_Forecasted_Volume_Source__c'));
            }
        });
        $A.enqueueAction(action);
    },
    
    parseFieldOptions: function(fieldsOptions, fieldName){
        var fieldOptionsMap = fieldsOptions[fieldName];
        var fieldOptionsKeys = Object.keys(fieldOptionsMap);
        var fieldOptions = [];
        for(var i=0; i<fieldOptionsKeys.length; i++){
            fieldOptions.push({value:fieldOptionsKeys[i],label:fieldOptionsMap[fieldOptionsKeys[i]]});
        }
        return fieldOptions;
    },
    
    fetchRequestRecord: function(component, event) {
        var helper = this;
        var action = component.get("c.fetchRequestRecord");
        action.setParams({requestId:(component.get("v.requestId") || component.get("v.recordId"))});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.request",response.getReturnValue());
                component.set("v.requestId",response.getReturnValue().Id);
                var request = component.get("v.request");
                var savingsTracker = component.get("v.savingsTracker");
                if(!savingsTracker.Id){
                    savingsTracker.Savings_Initiative_Category__c = request.Spend_Category__c;
                    savingsTracker.Assigned_Sourcing_Manager__c = request.Owner.Id;
                    savingsTracker.Assigned_Sourcing_Manager__r = request.Owner;
                    //savingsTracker.Spend_in_first_12_months__c = request.Estimated_Spend_in_first_12_months__c;
                    savingsTracker.Estimated_spend_in_the_first_12_Months__c = request.Estimated_Spend_in_first_12_months__c;
                    savingsTracker.Title__c = request.Title__c;
                    component.set("v.savingsTracker",savingsTracker);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchSavingsTracker: function(component, event) {
        var action = component.get("c.fetchSavingsTracker");
        action.setParams({savingsTrackerId:(component.get("v.savingsTrackerId") || component.get("v.recordId") || component.get("v.requestId"))});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var request = component.get("v.request");
                var savingsTracker = response.getReturnValue();
                if(request.Id && !savingsTracker.Id){
                    savingsTracker.Savings_Initiative_Category__c = request.Spend_Category__c;
                    savingsTracker.Assigned_Sourcing_Manager__c = request.Owner.Id;
                    savingsTracker.Assigned_Sourcing_Manager__r = request.Owner;
                    //savingsTracker.Spend_in_first_12_months__c = request.Estimated_Spend_in_first_12_months__c;
                    savingsTracker.Estimated_spend_in_the_first_12_Months__c = request.Estimated_Spend_in_first_12_months__c;
                    savingsTracker.Title__c = request.Title__c;
                }
                component.set("v.savingsTracker",savingsTracker);
                component.set("v.savingsTrackerId",savingsTracker.Id);
                var savingsTracker = component.get("v.savingsTracker");
                if(savingsTracker.Step1_Deal_Duration__c){
                    var years = [];
                    var yearsDiff = Math.ceil(savingsTracker.Step1_Deal_Duration__c/12);
                    for(var i=1; i<=yearsDiff; i++){
                        years.push(i);
                    }
                    component.set("v.years",years);
                    component.set("v.yearsDiff", yearsDiff);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchLoggedInUser: function(component, event) {
        var action = component.get("c.fetchLoggedInUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.loggedInUser",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    calculateCompressions: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        savingsTracker['Step4Final_Bid_of_Awarded_Supplier__c'] = savingsTracker['Total_New_Spend__c'];
        savingsTracker['Step4_Price_Compression__c'] = (savingsTracker['Step4First_Valid_Bid_of_Awarded_Supplier__c'] || 0) - savingsTracker['Step4Final_Bid_of_Awarded_Supplier__c'];
        savingsTracker['Step4_Price_Compression_percent__c'] = savingsTracker['Step4First_Valid_Bid_of_Awarded_Supplier__c']?savingsTracker['Step4_Price_Compression__c']/savingsTracker['Step4First_Valid_Bid_of_Awarded_Supplier__c']*100:0;
        savingsTracker['Price_Compression_Amount__c'] = savingsTracker['Step4_Price_Compression__c'];
        savingsTracker['Price_Compression_Percentage__c'] = savingsTracker['Step4_Price_Compression_percent__c'];
        component.set("v.savingsTracker",savingsTracker);
        
    },
    calculateSynergy: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        
        
        var syn1 = savingsTracker['Step_5_Synergy_Savings_Yr_1__c'];
        var syn2 = savingsTracker['Step_5_Synergy_Savings_Yr_2__c'];
        var syn3 = savingsTracker['Step_5_Synergy_Savings_Yr_3__c'];
        var syn4 = savingsTracker['Step_5_Synergy_Savings_Yr_4__c'];
        var syn5 = savingsTracker['Step_5_Synergy_Savings_Yr_5__c'];
        var syn6 = savingsTracker['Step_5_Synergy_Savings_Yr_6__c'];
        
        
        
        
        for(var ii=1;ii<7;ii++){
            if(savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c']==null){
                savingsTracker['Total_Synergy_Savings__c']=0;
            }else{
                savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c']=savingsTracker['Step_5_Synergy_Savings_Yr_'+ii+'__c'];
            }
            syn1 = savingsTracker['Step_5_Synergy_Savings_Yr_1__c'] ? parseFloat(savingsTracker['Step_5_Synergy_Savings_Yr_1__c']) : 0;
            syn2 = savingsTracker['Step_5_Synergy_Savings_Yr_2__c'] ? parseFloat(savingsTracker['Step_5_Synergy_Savings_Yr_2__c']) : 0;
            syn3 = savingsTracker['Step_5_Synergy_Savings_Yr_3__c'] ? parseFloat(savingsTracker['Step_5_Synergy_Savings_Yr_3__c']) : 0;
            syn4 = savingsTracker['Step_5_Synergy_Savings_Yr_4__c'] ? parseFloat(savingsTracker['Step_5_Synergy_Savings_Yr_4__c']) : 0;
            syn5 = savingsTracker['Step_5_Synergy_Savings_Yr_5__c'] ? parseFloat(savingsTracker['Step_5_Synergy_Savings_Yr_5__c']) : 0;
            syn6 = savingsTracker['Step_5_Synergy_Savings_Yr_6__c'] ? parseFloat(savingsTracker['Step_5_Synergy_Savings_Yr_6__c']) : 0;
            savingsTracker['Total_Synergy_Savings__c'] =parseFloat(syn1) +parseFloat(syn2)+parseFloat(syn3)+parseFloat(syn4)+parseFloat(syn5)+parseFloat(syn6);
            component.set("v.savingsTracker",savingsTracker);
            
        }
        
    }
    
})
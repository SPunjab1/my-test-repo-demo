({
    init : function(component, event, helper) {
        component.set("v.createdDate",$A.localizationService.formatDate(new Date(),"YYYY-MM-DD"));
        //helper.initiateSavingsTracker(component, helper); // Initiating all the read only fields
        helper.prepareFieldOptions(component, event);
        helper.fetchSavingsTracker(component, event);
        helper.fetchRequestRecord(component, event);
        helper.fetchLoggedInUser(component, event);  
    },
    
    onRender : function(component, event, helper) {
      
       let temp = component.get('v.savingsTracker');
        var checkSavigStatus = component.get('v.savingsTracker.Savings_Status__c');

        if(checkSavigStatus === 'Approved'){
            component.set( 'v.saveDisableFlag', true);
        }
        var baselineDeal = component.find("dealBasicsBaseline");
        let bd = baselineDeal.get("v.value");
        component.set("v.baselineDeal", bd);
        
        console.log('b------>d'+bd);
    },
    
    save : function(component, event, helper) {
        helper.save(component, event);
    },
    
    saveAndClose : function(component, event, helper) {
        helper.saveAndClose(component, event);
    },
    
    saveAndContinue : function(component, event, helper) {
        helper.saveAndContinue(component, event);
    },
    
    saveAndReview : function(component, event, helper) {
        helper.save(component, event);
        //Need to prevent this if Request has fewer than 5 Attachments
        //replace this function call with "countRequestAttachments"
        //helper.countRequestAttachments(component, event);
        helper.goToSavingsTrackerDetail(component, event);
    },
    
    close : function(component, event, helper) {
        helper.close(component, event);
    },
    
    onChangeDealDate: function(component, event, helper) {
        helper.onChangeDealDate(component, event);
    },
    
    calculateCompressions: function(component, event, helper){
        helper.calculateCompressions(component, event);
    },
    calculateSynergy: function(component, event, helper){
        helper.calculateSynergy(component, event);
    },
    
    openBaselineGuidelinesModal: function(component, event, helper){
        component.set("v.baselineModalFlag",true);
    },
    
    handleClickHere: function(component, event, helper){
        console.log('Here');
        var savingTrackerBaselineTree = $A.get("$Label.c.SavingTrackerBaselineTree");
        console.log('savingTrackerBaselineTree'+savingTrackerBaselineTree);
        window.open(savingTrackerBaselineTree);
    },
    
    closeBaselineGuidelinesModal: function(component, event, helper){
        component.set("v.baselineModalFlag",false);
    },
    onChange: function (component, event, helper) {
        var baselineDeal = component.find("dealBasicsBaseline").get("v.value");
        console.log('------------------1'+baselineDeal);
        component.set("v.baselineDeal", baselineDeal);
        var childCmp = component.find("savingsTrackerCalculation");
        var retnMsg = childCmp.calculateDealValueOrSpendTotals();
    },
    handleStatusChange: function (component, event, helper) {
        var savingStatus = component.get('v.savingsTracker.Savings_Status__c');
        
        if(savingStatus === 'Approved'){
            component.set( 'v.saveDisableFlag', true);
        }
        
    }
})
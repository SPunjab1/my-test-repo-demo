({
	fetchOppSiteId : function(component,event) {
        
        //debugger;
		var action=component.get("c.GetSiteID");
        action.setParams({OpportunityId: component.get("v.recordId")});
        
        action.setCallback(this,function(response){
            var state= response.getState();
            if(state=='SUCCESS')
            {
                var siteId= response.getReturnValue();
                //var siteURL= "https://app.powerbi.com/reportEmbed?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27&reportId=1170fd20-f1a0-437b-bd34-b43217b7f4fd&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D%3Ffilter%3DSite%2Fsite_cd%20eq%20%271AT8775A%27";
                //var siteURL= "https://app.powerbi.com/reportEmbed?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27&reportId=1170fd20-f1a0-437b-bd34-b43217b7f4fd&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D%3Ffilter%3DSite%2Fsite_cd%20eq%20%271AT8775A%27";
                
                
                //Latest URL from Swetha -- Sep 17
                //https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/843cf81b-c95f-4f88-bb5a-2b2941f967e2/ReportSection?refreshAccessToken=true
                
                //URL from SIT
                //"https://app.powerbi.com/reportEmbed?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27&reportId=843cf81b-c95f-4f88-bb5a-2b2941f967e2&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D";
                // 
                //From: Anil Sep 17
                //https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/843cf81b-c95f-4f88-bb5a-2b2941f967e2/ReportSectiond6dc30e069068ae6ad63?filter=Site%2Fsite_cd%20eq%20%2711TESTAA%27
                //"https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/843cf81b-c95f-4f88-bb5a-2b2941f967e2/ReportSectiond6dc30e069068ae6ad63?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27";
                var siteURL= "https://app.powerbi.com/reportEmbed?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27&reportId=843cf81b-c95f-4f88-bb5a-2b2941f967e2&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D";
                component.set("v.siteId", siteURL);
            }
        });
        
        $A.enqueueAction(action);
	}
})
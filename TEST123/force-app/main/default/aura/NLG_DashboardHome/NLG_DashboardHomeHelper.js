({
	init : function(component,event,helper) {
		var action = component.get("c.getReports");
            
            action.setCallback(this,function(response){
                if(response.getState()==="SUCCESS"){
                    console.log('response:'+response.getReturnValue());
                    var resp = response.getReturnValue();
                    if(resp !=null){
                        component.set("v.reports",resp);
                    }
                } else {
                    console.log('error:'+response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
	}
})
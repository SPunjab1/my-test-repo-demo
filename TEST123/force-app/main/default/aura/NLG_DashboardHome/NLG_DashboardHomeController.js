({
	openUrl : function(component, event, helper) {
        var button = event.getSource().get("v.name");
        console.log(button);
        switch(button) {
            case 'dashboard1':
                window.open('https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/42683dd5-b914-4152-a823-5224fb0bc71a/ReportSectiond48a7dd677e580a0e8a9?ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c', '_blank');
                break;
            case 'dashboard2':
                window.open('https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/42683dd5-b914-4152-a823-5224fb0bc71a/ReportSection9beb918700a70369a572?ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c', '_blank');
                break;
           	case 'dashboard3':
                window.open('https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/42683dd5-b914-4152-a823-5224fb0bc71a/ReportSectiond48a7dd677e580a0e8a9?ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c', '_blank');
                break;
            case 'dashboard4':
                window.open('https://app.powerbi.com/groups/me/apps/418e608d-8874-4519-9be4-6ad45f01970d/reports/42683dd5-b914-4152-a823-5224fb0bc71a/ReportSection9beb918700a70369a572?ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c', '_blank');
                break;
            default:
                // code block
        } 
		window.open('https://yourUrl.com?param=${someParamValue}', '_blank');
	},
    doInit : function(component, event, helper) {
        var siteId ='PH40129E';
        var dashboardUrl = component.get("v.dashboardUrl");
       
        //var siteURL= "https://app.powerbi.com/reportEmbed?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27&reportId=843cf81b-c95f-4f88-bb5a-2b2941f967e2&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D";
                
        var siteURL= "https://app.powerbi.com/reportEmbed?reportId=3a6e0de0-68be-4048-a15d-8fc139d836de&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D";
        if(dashboardUrl !=null && dashboardUrl!= undefined){
            siteURL=dashboardUrl;
        }
        component.set("v.url", siteURL);
        console.log('url:',component.get("v.url"));
        helper.init(component, event, helper);
	}
})
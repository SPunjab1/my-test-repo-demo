({
	init : function(component, event, helper) {
        var savingsTracker = component.get("v.savingsTracker");
        var fieldName = component.get("v.fieldName");
		component.set("v.value",savingsTracker[fieldName]);
	},
    
    valueChanged: function(component, event, helper){
        var value = component.get("v.value");
        var savingsTracker = component.get("v.savingsTracker");
        var fieldName = component.get("v.fieldName");
        
        if((value || savingsTracker[fieldName]) && value !== savingsTracker[fieldName]){
        	savingsTracker[fieldName] = value;
            component.set("v.savingsTracker",savingsTracker);
            component.get("v.parentComponent").calculateOriginalVolumeTotals();
            component.get("v.parentComponent").calculateRevisedVolumeTotals();
            component.get("v.parentComponent").calculateBaselineUnitPriceTotals();
            component.get("v.parentComponent").calculateNewUnitPriceTotals();
            component.get("v.parentComponent").calculateOneOffCreditsOrRebatesTotals();
            component.get("v.parentComponent").calculateOtherCostAvoidanceTotals();
            component.get("v.parentComponent").calculateEarlyPaymentDiscountTotals();
            component.get("v.parentComponent").calculateDealValueOrSpendTotals();
        }   
    },
    
    readOnlyValueChanged: function(component, event){
        var savingsTracker = component.get("v.savingsTracker");
        var fieldName = component.get("v.fieldName");
		component.set("v.value",savingsTracker[fieldName]);
    }
})
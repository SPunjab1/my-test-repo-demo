({
	copyEscalations : function (component, event, helper) {
        var action = component.get("c.copyEscalationForOpportunity");
        var recordid =component.get("v.recordId");        
        action.setParams({opportunityId: recordid});
        
        console.log('Copy Escalations Button Clicked:'+ recordid);
        
        action.setCallback(this, function(response){    
             var state = response.getState();
            console.log('copy Escalation CB Completed--' + state);
 			
 			if(state == 'SUCCESS') {
                console.log('RefreshView:');
                $A.get('e.force:refreshView').fire();   
                helper.showInfo(component, event, helper);
            }
            else if(state == "ERROR"){
                  var errors = response.getError();  
                  let message = 'Error in Copying Escalations'; // Default error message
                  if (errors && Array.isArray(errors) && errors.length > 0) {
					   message = errors[0].message;
    				}
				  // Display the message
				  console.error(message);
                  helper.handleErrors(errors);
                  //component.set("v.showErrors",true);
                  //component.set("v.errorMessage",errors[0].message);
                           
           }
        });

        $A.enqueueAction(action);
         
    }    
    
});
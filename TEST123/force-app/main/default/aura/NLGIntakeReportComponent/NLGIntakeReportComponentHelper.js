({
	fetchOppSiteId : function(component,event) {
        
        //debugger;
		var action=component.get("c.GetSiteID");
        action.setParams({OpportunityId: component.get("v.recordId")});
        
        action.setCallback(this,function(response){
            var state= response.getState();
            if(state=='SUCCESS')
            {
                var siteId= response.getReturnValue();
                var siteURL= "https://app.powerbi.com/reportEmbed?filter=Site%2Fsite_cd%20eq%20%27"+siteId +"%27&reportId=1170fd20-f1a0-437b-bd34-b43217b7f4fd&autoAuth=true&ctid=be0f980b-dd99-4b19-bd7b-bc71a09b026c&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXdlc3QtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D%3Ffilter%3DSite%2Fsite_cd%20eq%20%271AT8775A%27";
                component.set("v.siteId", siteURL);
            }
        });
        
        $A.enqueueAction(action);
	}
})
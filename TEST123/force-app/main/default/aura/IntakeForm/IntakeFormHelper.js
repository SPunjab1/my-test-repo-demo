({
	fetchPicklistValueWorkflowTemp :function(component, event, helper) {
        
        var action = component.get("c.getCustomPickListValues");
        action.setParams({ object_name : component.get("v.IntakeRequest") ,
                          field_name : 'Workflow_Template__c'
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {   
                var result = response.getReturnValue();
                component.set("v.WorkflowTemplate", result);
                
            }
        });
        $A.enqueueAction(action);
    },
    fetchPicklistValueMutisite :function(component, event, helper) {
        
        var action = component.get("c.getCustomPickListValues");
        action.setParams({ object_name : component.get("v.IntakeRequest") ,
                          field_name : 'Multiple_Site_Request__c'
                         });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {   
                var result = response.getReturnValue();
                component.set("v.Multisite", result);
                
            }
        });
        $A.enqueueAction(action);
    }
})
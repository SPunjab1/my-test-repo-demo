({
	doInit : function(component, event, helper) {
		  helper.fetchPicklistValueWorkflowTemp(component, event, helper);	
          helper.fetchPicklistValueMutisite(component, event, helper);	
	},
    changeMutlisite:function(component, event, helper) {
        var selected = component.find("IntakeFieldMultiSelect").get("v.value");
        if(selected === "Yes"){
        	component.set("v.isMultiSelect",true);  
        }
        else if(selected === "No"){
        	component.set("v.isMultiSelect",false);	   
        } 
    }
})
({ doInit:function(component, event, helper){
    var parentCaseId=component.get("v.recordId");
    var action = component.get('c.findTaskNumber');
    action.setParams({cIds : parentCaseId});
    action.setCallback(this, function(response){
        var state = response.getState();
        if(state === "SUCCESS"){
            var res = response.getReturnValue();
            console.log("res--->>> " + res);
            component.set('v.isDisable', res);   
        }
        else if(state === "ERROR") {
            var errors = response.getError();
            if(errors){
                if(errors[0] && errors[0].message){
                    console.log("Error Message: " + errors[0].message);
                }
            }
            else{
                console.log("Unknown Error");
            }
        }
    });
    $A.enqueueAction(action);
},
  ReopenTask : function(component, event, helper){
      var parentCaseId=component.get("v.recordId");
      var action = component.get('c.ReopenTaskNew');
      action.setParams({cIds : parentCaseId});
      action.setCallback(this, function(response){
          var state = response.getState();
          if(state === "SUCCESS"){
              var result = response.getReturnValue();
              if(!$A.util.isEmpty(result)) {  
                  var msg =$A.get("$Label.c.Reopen_Task_Message");
                  helper.showToast('Success',msg,'success');
                  // $A.get('e.force:refreshView').fire();
                  component.set("v.isOpen", false);
                  console.log("result-->"+result);
              }
          }
          else if(state === "ERROR") {
              var errors = response.getError();
              if(errors){
                  if(errors[0] && errors[0].message){
                      console.log("Error Message: " + errors[0].message);
                  }
              }
              else{
                  console.log("Unknown Error");
              }
          }
      });
      $A.enqueueAction(action);
  },
  showToast: function(title,message,type) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          title : title,
          message: message,
          type: type,
          mode: 'pester'
      });
      toastEvent.fire(); 
  }
  
 })
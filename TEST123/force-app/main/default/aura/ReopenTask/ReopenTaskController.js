({   doInit : function(component, event, helper)
  {
      helper.doInit(component, event, helper);
  },
  ReopenTask : function(component, event, helper)
  {
      helper.ReopenTask(component, event, helper);
  },
  openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
  },
  
  closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
  },
 })
({
    doInit  : function(component, event, helper){        
        var parentCaseId=component.get("v.recordId");
        var action = component.get('c.enableTaskShiftOrder');
        action.setParams({cIds : parentCaseId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var res = response.getReturnValue();
                console.log("res---1>>> " + res);
                component.set('v.isDisable', res);   
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error Message: " + errors[0].message);
                    }
                }
                else{
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    ShiftTaskOrder : function(component, event, helper){
        
        var parentCaseId=component.get("v.recordId");
        var action = component.get('c.updateShiftTaskOrder');
        
        action.setParams({cIds : parentCaseId, revertFlag : false});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var allValues = response.getReturnValue();
                var msg=$A.get("$Label.c.Shift_Task_Order_Sucess_Msg");
                component.set("v.openModal", false);
                helper.showToast('Success',msg,'success');
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error Message: " + errors[0].message);
                    }
                }
                else{
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    }, 
    
    handleRevertTaskOrder : function(component, event, helper){
        
        var parentCaseId=component.get("v.recordId");
        var action = component.get('c.updateShiftTaskOrder');
        
        action.setParams({cIds : parentCaseId, revertFlag : true});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var allValues = response.getReturnValue();
                var msg=$A.get("$Label.c.Shift_Task_Order_Sucess_Msg");
                component.set("v.openModal", false);
                helper.showToast('Success',msg,'success');
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error Message: " + errors[0].message);
                    }
                }
                else{
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
        component.set( 'v.showRevertModal', false);
    },
    
    handleCloseModal: function(component, event, helper) {
        //For Close Modal, Set the "openModal" attribute to "fasle"  
        component.set("v.openModal", false);
        component.set( 'v.showRevertModal', false);
    },
    
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.openModal", true);
    },
    
    openReverModel: function(component, event, helper) {
        
        component.set("v.openModal", true);
        component.set( 'v.showRevertModal', true);
    }
    
})
({
	prepareColumns : function(component, event) {
		var columns = [];
        columns.push({label:'',field:'name'});
        columns.push({label:'Cost Reduction',field:'costReduction'});
        columns.push({label:'Cost Avoidance',field:'costAvoidance'});
        columns.push({label:'Total Savings',field:'totalSavings'});
        columns.push({label:'Price Compression',field:'priceCompression'});
        columns.push({label:'Total Synergy Savings',field:'totalSynergySavings'});

        
        component.set("v.columns",columns);
	}
})
({
    
    doInit : function(component, event, helper) {
        var action = component.get('c.getLatestNotes');
        action.setCallback(this, function(a){
            var ReleaseNoteblank = a.getReturnValue().Release_Notes__c;
            //alert('ReleaseNoteblank:'+ ReleaseNoteblank);
            if(!ReleaseNoteblank) {
                component.set('v.ShowNotification', false);
            }          
            else{
                component.set('v.ShowNotification', true);
                component.set('v.NotificationText', a.getReturnValue().Release_Short_Description__c);
                component.set('v.ReleaseNotes', a.getReturnValue().Release_Notes__c);
            }
        });
        $A.enqueueAction(action);
    },
    handleClose :function(component, event, helper) {
        var action = component.get('c.HideReleaseNotes'); 
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.ShowNotification', false);
                component.set('v.ShowNotesSection', false);
            }
        });
        $A.enqueueAction(action);        
    },
    Morelink :function(component, event, helper) {
        component.set('v.ShowNotification', false);
        component.set('v.ShowNotesSection', true);
    }    
})
({
    // To prepopulate the seleted value pill if value attribute is filled
	doInit : function( component, event, helper ) {
    	$A.util.toggleClass(component.find('resultsDiv'),'slds-is-open');
		if( !$A.util.isEmpty(component.get('v.value')) ) {
			helper.searchRecordsHelper( component, event, helper, component.get('v.value') );
		}
	},
    // When a keyword is entered in search box
	searchRecords : function( component, event, helper ) {
        if( !$A.util.isEmpty(component.get('v.searchString')) ) {
		    helper.searchRecordsHelper( component, event, helper, '' );
        } else {
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        } 
	},
    
    handleApplicationEvent : function(component,event,helper){
        	//alert('incontroller');
            var selectedRecord = event.getParam("selectedRecord");
         	var recordByEvent = event.getParam("recordByEvent");
        	
            var Index = event.getParam("Index");
        	var Count = event.getParam("Count");
           // alert('Index***'+Index + '@@@@@@@@@@@'+component.get("v.Index"));
        if(Index == component.get("v.Index")){
            // set the handler attributes based on event data
            /*cmp.set("v.messageFromEvent", message);
            var numEventsHandled = parseInt(cmp.get("v.numEvents")) + 1;
            cmp.set("v.numEvents", numEventsHandled);*/
            
            //index = parseInt(Index);
            //alert('count%%%%'+Count);
            Count = $A.util.isUndefinedOrNull(Count)?null:parseInt(Count);
            //Count = parseInt(Count);
            //alert('parse int%%%%'+Count);
            component.set('v.selectedRecord',selectedRecord);
            //alert('333*******'+selectedRecord.value);
            var inst = {};
            //alert(component.get("v.Count"));
            inst.Count = Count;
            inst.EquipmentId = recordByEvent.Id;
            inst.EquipmentName= recordByEvent.EquipmentModel__c;
            inst.EquipmentType = recordByEvent.EquipmentType__c;
            inst.Length = recordByEvent.Length__c;
            inst.Width= recordByEvent.Width__c;
            inst.Depth= recordByEvent.Depth__c;
            inst.Weight = recordByEvent.Weight__c;
            inst.LTimesW = recordByEvent.Length__c*recordByEvent.Width__c !== 0 ? parseFloat(recordByEvent.Length__c*recordByEvent.Width__c).toFixed(2) : 0;
            inst.TotalSurface= (!$A.util.isEmpty(inst.Count) && !$A.util.isUndefinedOrNull(inst.Count)) ? ((parseFloat(inst.Count) * parseFloat(inst.Length) * parseFloat(inst.Width)).toFixed(2)): 0;
            inst.TotalWeight= (!$A.util.isEmpty(inst.Count) && !$A.util.isUndefinedOrNull(inst.Count))? ((parseFloat(inst.Count)*parseFloat(inst.Weight)).toFixed(2)):0;
            inst.equipment = recordByEvent;
            inst.Index = Index;
            
            component.set('v.custinst',Index);
            
            var compEvent = component.getEvent("oSelectedRecordEvent");
            compEvent.setParams({"recordByEvent" : recordByEvent,
                                 "custinst" :inst });  
            
            compEvent.fire();
            component.set('v.value',recordByEvent.EquipmentModel__c);
            //alert('444*******');
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');  
        }
            
        
        
    },

    // When an item is selected
	selectItem : function( component, event, helper ) {
       // alert('inselected old iteam section')
        if(!$A.util.isEmpty(event.currentTarget.id)) {
            //alert('*******');
    		var recordsList = component.get('v.recordsList');
    		var index = recordsList.findIndex(x => x.value === event.currentTarget.id)
            if(index != -1) {
                var selectedRecord = recordsList[index];
            }
            //alert('222*******');
            component.set('v.selectedRecord',selectedRecord);
            //alert('333*******'+selectedRecord.value);
           	var inst = {};
            //alert(component.get("v.Count"));
            //
            inst.Count = component.get("v.Count");
            inst.EquipmentId = selectedRecord.equipRec.Id;
            inst.EquipmentName= selectedRecord.equipRec.EquipmentModel__c;
            inst.EquipmentType = selectedRecord.equipRec.EquipmentType__c;
            inst.Length = selectedRecord.equipRec.Length__c;
            inst.Width= selectedRecord.equipRec.Width__c;
            inst.Depth= selectedRecord.equipRec.Depth__c;
            inst.Weight = selectedRecord.equipRec.Weight__c;
            inst.LTimesW = selectedRecord.equipRec.Length__c*selectedRecord.equipRec.Width__c !== 0 ? parseFloat(selectedRecord.equipRec.Length__c*selectedRecord.equipRec.Width__c).toFixed(2) : 0;
            inst.TotalSurface= (!$A.util.isEmpty(inst.Count) && !$A.util.isUndefinedOrNull(inst.Count)) ? ((parseFloat(inst.Count) * parseFloat(inst.Length) * parseFloat(inst.Width)).toFixed(2)): 0;
            inst.TotalWeight= (!$A.util.isEmpty(inst.Count) && !$A.util.isUndefinedOrNull(inst.Count))? ((parseFloat(inst.Count)*parseFloat(inst.Weight)).toFixed(2)):0;
            inst.equipment = selectedRecord.equipRec;
            inst.Index = component.get("v.Index");
               
            component.set('v.custinst',inst);
             var compEvent = component.getEvent("oSelectedRecordEvent");
            compEvent.setParams({"recordByEvent" : selectedRecord,
                                 "custinst" :inst });  
            
            compEvent.fire();
            component.set('v.value',selectedRecord.value);
            //alert('444*******');
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
	},
    
    showRecords : function( component, event, helper ) {
        //component.set("v.isOpen",true);
        var count = component.get("v.Count");
        var Index = component.get("v.Index");
        
        count = $A.util.isUndefinedOrNull(count)?null:count;
        
        var compEvent = component.getEvent("oECPopupevent");
        compEvent.setParams({"isPopupOpen" : true,
                             "Index" :Index,
                             "Count":count});  
        //alert('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        compEvent.fire();
    	if(!$A.util.isEmpty(component.get('v.recordsList')) && !$A.util.isEmpty(component.get('v.searchString'))) {
            $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
        }
	},

    // To remove the selected item.
	removeItem : function( component, event, helper ){
        var count = component.get("v.Count");
        var Index = component.get("v.Index");
        //alert(count+'##########'+Index);
        component.set('v.selectedRecord','');
        component.set('v.value','');
        component.set('v.searchString','');
        setTimeout( function() {
            component.find( 'inputLookup' ).focus();
        }, 250);
        var inst = {};
        inst.Index = Index;
        //inst.Count = null;
        inst.TotalSurface=0;
        inst.TotalWeight=0;
        var compEvent = component.getEvent("oSelectedRecordEvent");
        compEvent.setParams({"recordByEvent" : null,
                             "custinst" :inst });  
        
        compEvent.fire();
    },

    // To close the dropdown if clicked outside the dropdown.
    blurEvent : function( component, event, helper ){
    	$A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
    },
    
    
 
})
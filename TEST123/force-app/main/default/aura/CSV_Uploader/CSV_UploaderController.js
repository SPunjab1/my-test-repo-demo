({
    getFileDetails: function(component, event, helper) {
        var fileName = 'No File Selected..';
        var fileType;
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
            fileType = event.getSource().get("v.files")[0]['type'];
            console.log(event.getSource().get("v.files")[0]);
        }
        component.set("v.fileName", fileName);
        component.set("v.fileType", fileType);
    },
    display: function(component, event, helper) {
        component.set("v.spinner",true);
        var file=component.find("fileId").get("v.files");
        if(file!=null){
            if(file.length > 0) {
                helper.readFile(component, helper, file);
            }else {
                var msg =$A.get("$Label.c.Valid_File_Error");
                helper.showToast('Error',msg,'error');
                component.set("v.spinner",false);
            }    
        }
        else{
        	var msg =$A.get("$Label.c.File_not_uploaded_error_msg");
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);    
        }
        
    },
    fireEvent :function(component, event, helper){
        component.set("v.spinner",true);
        var fList=component.find("fileId").get("v.files");
        
        let file = [...fList];
        if(file!=null){
            if (file.length > 0) {
                helper.fireLocationIdEvent(component, helper, file);
                /**setTimeout(function(){
                    helper.getLocationData(component, helper, file ,component.get("v.fileContentData"));
                }, 1000); **/
            }else {
                var msg =$A.get("$Label.c.Valid_File_Error");
                helper.showToast('Error',msg,'error');
                component.set("v.spinner",false);
            }    
        }
        else{
			var msg =$A.get("$Label.c.File_not_uploaded_error_msg");
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);             
        }
       
    },
    addAttchment : function(component, event, helper) {
        var file=component.find("fileId").get("v.files");
        var params = event.getParam('arguments');
        var intakeId;
        var fileContents;
        var objFileReader;
        var file;
        if(params){
            intakeId = params.intakeId;
        }
        if (file.length > 0) {
            file=file[0];
            objFileReader = new FileReader(); 
            var actionLine = component.get("c.saveChunk");
            objFileReader.onload = function(e) {
                fileContents = objFileReader.result;
                var base64 = 'base64,';
                var dataStart = fileContents.indexOf(base64) + base64.length;
                fileContents = fileContents.substring(dataStart);
                helper.uploadProcess(component, file, fileContents,intakeId,actionLine);
                component.set("v.spinner",false);  
            }
            objFileReader.readAsDataURL(file);
        }else {
            var msg =$A.get("$Label.c.Valid_File_Error");
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
        }    
    },
    displayDarkSiteData : function(component, event, helper) {
        var data =component.get("v.fileContentData");
        var allTextLines = data.split(/\r\n|\n/);
        var dataRows = allTextLines.length-1;
        var headers = allTextLines[0].split(',');
        var params = event.getParam('arguments');
        var blankIdLst;
        if(params){
            blankIdLst = params.blankIdLst;
            var blankIds =new Array();
            for(var s in blankIdLst){
                blankIds.push(parseInt(blankIdLst[s].substring(5)));    
            }
            component.set("v.isOpen", true);
            var lines = [];
            var filecontentdata;
            var index = 0;
            var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
            content += "<thead><tr class=\"slds-text-title--caps\">";
            content += '<th scope=\"col"\>Index</th>';
            for(i=0;i<headers.length; i++){
                content += '<th scope=\"col"\>'+headers[i]+'</th>';
            }
            content += "</tr></thead>";
            for (var i=1; i<allTextLines.length; i++) {
                if(blankIds.includes(i)){
                    index++;
                    filecontentdata = allTextLines[i].split(',');
                    if(filecontentdata[0]!=''){
                        content +="<tr>";
                        content +='<td>'+index+'</td>';
                        for(var j=0;j<filecontentdata.length;j++){
                            content +='<td>'+filecontentdata[j]+'</td>';
                        }
                        content +="</tr>";
                    }
                }
            }
            content += "</table>";
            component.set("v.DarkSiteContent",content);
        }
    },
    closeModel: function(component, event, helper) {
        var msg = $A.get("$Label.c.Remove_Dark_Site_Message");
        helper.showToast('Warning',msg,'warning');
        component.set("v.isOpen", false);
        helper.clearDataAfterError(component, event, helper);
    },
    continueAction: function(component, event, helper) {
        component.set("v.isOpen", false);
        var cmpEvent = component.getEvent("createWorkOrderwithDarkSite");
        cmpEvent.fire(); 
    },
    closeDuplicateModel: function(component, event, helper) {
        component.set("v.isOpenDuplicate", false);
        helper.clearDataAfterError(component, event, helper);
    },
    clearData :function(component, event, helper) {
        component.set("v.fileName", '');
        component.set("v.TargetFileName", '');
        component.set("v.TableContent",'');
        component.set(component.find("fileId").get("v.files"),'');
    },
    displayDuplicatesFromDatabase : function(component, event, helper){
        var params = event.getParam('arguments');
        var duplicateMap;
        if(params){
            duplicateMap = params.duplicateMap;
            var duplicateKeys=new Array();
            for(var key in duplicateMap){
                duplicateKeys.push(key);    
            }
            var data = component.get("v.fileContentData");
            var allTextLines = data.split(/\r\n|\n/);
            var dataRows = allTextLines.length-1;
            var headers = allTextLines[0].split(',');
            component.set("v.isOpenDuplicateInDatabase", true);
            var lines = [];
            var filecontentdata;
            var index = 0;
            var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
            content += "<thead><tr class=\"slds-text-title--caps\">";
            content += '<th scope=\"col"\>Index</th>';
            for(i=0;i<headers.length; i++){
                content += '<th scope=\"col"\>'+headers[i]+'</th>';
            }
            content += "</tr></thead>";
            for (var i=1; i<allTextLines.length; i++) {
                for(var key in duplicateKeys){
                    var locId = duplicateKeys[key].split('|')[0];
                    var template = duplicateKeys[key].split('|')[1];
                    if(allTextLines[i].includes(locId) && allTextLines[i].includes(template)){
                        index++;
                        filecontentdata = allTextLines[i].split(',');
                        if(filecontentdata[0]!=''){
                            content +="<tr>";
                            content +='<td>'+index+'</td>';
                            for(var j=0;j<filecontentdata.length;j++){
                                content +='<td>'+filecontentdata[j]+'</td>';
                            }
                            content +="</tr>";
                        }
                        break;
                    }
                }
            }
            content += "</table>";
            component.set("v.DuplicateDataFromDatabase",content);
        }  
    },
    closeDatabaseDuplicateModel : function(component, event, helper) {
        component.set("v.isOpenDuplicateInDatabase", false);
        helper.clearDataAfterError(component, event, helper);
    },
    continueActionForDatabaseDuplicate : function(component, event, helper) {
        component.set("v.isOpenDuplicateInDatabase", false);
        var cmpEvent = component.getEvent("createWorkorderwithDuplicateData");
        cmpEvent.fire(); 
    }
})
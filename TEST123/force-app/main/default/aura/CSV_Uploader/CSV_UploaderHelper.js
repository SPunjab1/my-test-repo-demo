({
    readFile: function(component, helper, file) {
        var file=file[0];
        if (!file) return;
        console.log('filename'+file.name);
        if(!file.name.match(/\.(csv||CSV)$/)){
            var msg =$A.get("$Label.c.Supported_File_Error");
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
            helper.clearDataAfterError(component, event, helper);
            return;
        }else{
            reader = new FileReader();
            reader.onerror =function errorHandler(evt) {
                switch(evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        component.set("v.spinner",false);
                        var msg =$A.get("$Label.c.File_Not_Found_Error");
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        component.set("v.spinner",false);
                        var msg =$A.get("$Label.c.File_Unreadable_Error");
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.ABORT_ERR:
                        component.set("v.spinner",false);
                        break;
                    default:
                        component.set("v.spinner",false);
                        var msg =$A.get("$Label.c.Read_File_Error");
            			helper.showToast('Warning',msg,'warning');
                };
            }
            reader.onabort = function(e) {
                component.set("v.spinner",false);
                var msg =$A.get("$Label.c.File_Read_Cancel_Error");
                helper.showToast('Warning',msg,'warning');
            };
            reader.onloadstart = function(e) { 
                var output = '<ui type=\"disc\"><li><strong>'+file.name +'</strong> ('+file.type+')- '+file.size+'bytes, last modified: '+file.lastModifiedDate.toLocaleDateString()+'</li></ui>';
                component.set("v.filename",file.name);
                component.set("v.TargetFileName",output);
            };
            reader.onload = function(e) {
                var data=e.target.result;
                component.set("v.fileContentData",data);
                console.log("file data"+JSON.stringify(data));
                var allTextLines = data.split(/\r\n|\n/);
                var dataRows=allTextLines.length-1;
                var headers = allTextLines[0].split(',');
                /*var validHeaders ="TemplateName,LocationID,CRUserDescription,ProjectName,CRCount,SecondaryLocationID";
                for(var i =0;i<headers.length ;i++){
                    if(!validHeaders.includes(headers[i])){
                        var msg ='Please correct header:Header name should be TemplateName,LocationID,CRUserDescription,ProjectName,CRCount,SecondaryLocationID';
            	    	helper.showToast('Error',msg,'error');
                        component.set("v.spinner",false);
                        return;
                    }
                }*/
                if(dataRows <= 0){
                    var msg ='File Rows should be grater than 1.';
            	    helper.showToast('Warning',msg,'warning');
                    component.set("v.spinner",false);
                    component.set("v.showMain",true);
                    helper.clearDataAfterError(component, event, helper);
                } 
                else{
                    var lines = [];
                    var filecontentdata;
                    var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
                    content += "<thead><tr class=\"slds-text-title--caps\">";
                    content += '<th scope=\"col"\>Index</th>';
                    for(i=0;i<headers.length; i++){
                        content += '<th scope=\"col"\>'+headers[i]+'</th>';
                    }
                    content += "</tr></thead>";
                    for (var i=1; i<allTextLines.length; i++) {
                        filecontentdata = allTextLines[i].split(',');
                        if(filecontentdata[0]!=''){
                            content +="<tr>";
                            content +='<td>'+i +'</td>';
                            for(var j=0;j<filecontentdata.length;j++){
                                content +='<td>'+filecontentdata[j]+'</td>';
                            }
                            content +="</tr>";
                        }
                    }
                    content += "</table>";
                    
                    component.set("v.TableContent",content);
                    component.set("v.showMain",false);                   
                }
            }
            reader.readAsText(file);
        }
        var reader = new FileReader();
        reader.onloadend = function() {
        };
        reader.readAsDataURL(file);
        component.set("v.spinner",false);
    },
    getLocationData : function(component, helper, file, data) {
        var allTextLines = data.split(/\r\n|\n/);
        var dataRows=allTextLines.length-1;
        var headers = allTextLines[0].split(',');
        var locationIdiIndex;
        var templateNameindex;
        var userDescIndex;
        var prjnameIndex;
        var crCountIndex;
        var secondaryLocationID;
        var trasmissionDesign;
        var ranTrasmission;
        var backhaulPlanImp;
        var TansDepWal;
        var switchId;
        var tpeRequestNumber;
        var CIR;
        for(var i =0;i<headers.length ;i++){
            if(headers[i] == $A.get("$Label.c.CSVCol_LocationID")){
                locationIdiIndex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_TemplateName")){
                templateNameindex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_CRUserDescription")){
                userDescIndex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_ProjectName")){
                prjnameIndex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_CRCount")){
                crCountIndex =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_SecondaryLocationID")){
                secondaryLocationID =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_AdvanceRANTransmission")){
                trasmissionDesign =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_EnterpriseTransmissionDesign")){
                ranTrasmission =  i;  
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_SwitchId")){
            	switchId = i;    
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_TPERequestNumber")){
	            	tpeRequestNumber = i;    
	        }
            if(headers[i] == $A.get("$Label.c.CSVCol_BackhaulPlanningImp")){
                backhaulPlanImp = i;    
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_TransmissionDeploymentWalker")){
                TansDepWal = i;    
            }
            if(headers[i] == $A.get("$Label.c.CSVCol_CIR")){
                CIR = i;    // ADDED BY FARAZ FOR CIR
            }
        }
        var fileContentMap = new Map();
        var locationIdLst = [];
        var switchIdLst = [];
        var duplicateDataRowNum = [];
        var secLocIdLst = [];
        var priflag = false;
        var secflag = false;
        var switchIdFlag = false;
        var tpeRequestNumberLst = [];
        for(var i = 1 ; i<allTextLines.length ;i++)
        {
            var rowDataMap = new Map();
            var rowData = allTextLines[i].split(',');
            if(rowData[templateNameindex]!= $A.get("$Label.c.Migration_Template_Name"))
            {
                rowDataMap[$A.get("$Label.c.CSVCol_LocationID")] = rowData[locationIdiIndex];
                rowDataMap[$A.get("$Label.c.CSVCol_TemplateName")] = rowData[templateNameindex];
                rowDataMap[$A.get("$Label.c.CSVCol_CRUserDescription")] = rowData[userDescIndex];
                rowDataMap[$A.get("$Label.c.CSVCol_ProjectName")] = rowData[prjnameIndex];
                rowDataMap[$A.get("$Label.c.CSVCol_CRCount")] = rowData[crCountIndex];
                rowDataMap[$A.get("$Label.c.CSVCol_SecondaryLocationID")] = rowData[secondaryLocationID];
                rowDataMap[$A.get("$Label.c.CSVCol_AdvanceRANTransmission")] = rowData[trasmissionDesign];
                rowDataMap[$A.get("$Label.c.CSVCol_EnterpriseTransmissionDesign")] = rowData[ranTrasmission];
                rowDataMap[$A.get("$Label.c.CSVCol_BackhaulPlanningImp")] = rowData[backhaulPlanImp];
                rowDataMap[$A.get("$Label.c.CSVCol_TransmissionDeploymentWalker")] = rowData[TansDepWal];
                rowDataMap[$A.get("$Label.c.CSVCol_SwitchId")] = rowData[switchId];
                rowDataMap[$A.get("$Label.c.CSVCol_TPERequestNumber")] = rowData[tpeRequestNumber];
                rowDataMap[$A.get("$Label.c.CSVCol_CIR")] = rowData[CIR];
                var locationId;
                if(rowData[locationIdiIndex]){
                    locationId = rowData[locationIdiIndex];
                }
                else{
                    if(rowData[templateNameindex]){
                        console.log('Blank --'+i);
                    	priflag = true;
                    	locationId = 'BLANK'+i;
                    }
                    else{
                    	console.log('in else empty row....');	    
                    }   
                }
                //Faraz Added userdesc and projectname as combination in key.
                var key = locationId+ '|'+ rowData[templateNameindex]+ '|'+  rowData[prjnameIndex]+ '|'+ rowData[userDescIndex]
               // alert('combination--> '+key);
                /**Commented by Faraz
                 * if(key in fileContentMap){
                    duplicateDataRowNum.push(i);
                }
                else{*/
                if(rowData[locationIdiIndex] && rowData[templateNameindex]){
                    console.log('------' + key);
                    fileContentMap[key] = rowDataMap;        
                }
                if(rowData[locationIdiIndex]){
                    locationIdLst.push(rowData[locationIdiIndex]);    
                }
                //}
                //locationIdLst.push(locationId);
               // alert('locationlist --'+locationIdLst);
                if(rowData[templateNameindex] == $A.get("$Label.c.TemplateNameForSecLocation")){
                    if(rowData[secondaryLocationID]){
                    	secLocIdLst.push(rowData[secondaryLocationID]);    
                    }
                    else{
                    	secflag = true;   
                    }
                }
                if(rowData[switchId]){
                	switchIdLst.push(rowData[switchId]);    
                }
                if(rowData[tpeRequestNumber]){
	                    tpeRequestNumberLst.push(rowData[tpeRequestNumber]);
	            }
               /* else{
                    if(rowData[templateNameindex]){
                        switchIdFlag = true;   
                    }
                }*/
            }
        }
/*
 * commeneted by faraz
        if(duplicateDataRowNum.length>0){
        	helper.displayDuplicateRecords(component, helper,duplicateDataRowNum);	    
        }*/
        console.log('<---------'+JSON.stringify(fileContentMap));
         if(priflag){
        	var msg = 'Primary location id can not be empty, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
            helper.clearDataAfterError(component, event, helper);    
        }
        else if(secflag){
        	var msg = 'Secondary location id can not be empty for template ' +$A.get("$Label.c.TemplateNameForSecLocation") +' Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
            helper.clearDataAfterError(component, event, helper);
        }
        /*else if(switchIdFlag){
        	var msg = 'Switch ID can not be empty, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
            helper.clearDataAfterError(component, event, helper);    
        }*/
        else{
        	 if(locationIdLst!=null && locationIdLst.length>0){
                var cmpEvent = component.getEvent("locationIDLstEvent");
                 //('event---'+locationIdLst);
                cmpEvent.setParams({ "locationIdLst" : locationIdLst,
                                     "fileContentMap" :fileContentMap,
                                     "fileContentData": component.get("v.fileContentData"),
                                     "fileName" : component.get("v.fileName"),
                                     "fileType" : component.get("v.fileType"),
                                     "secLocIdLst" : secLocIdLst,
                                     "switchIdLst" : switchIdLst
                                    });
                cmpEvent.fire();
                component.set("v.spinner",false);
        	}    
        }
    },
    fireLocationIdEvent: function(component, helper, file) {
        var index =0;
        var filecontentdata;
        var file=file[0];
        if (!file) return;
        console.log('filename...'+file.name);
        if(!file.name.match(/\.(csv||CSV)$/)){
			var msg =$A.get("$Label.c.Supported_File_Error");
            helper.showToast('Error',msg,'error');
            component.set("v.spinner",false);
            helper.clearDataAfterError(component, event, helper);
            returns;
        }else{
            reader = new FileReader();
            reader.onerror =function errorHandler(evt) {
                switch(evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        var msg =$A.get("$Label.c.File_Not_Found_Error");
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        var msg =$A.get("$Label.c.File_Unreadable_Error");
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.ABORT_ERR:
                        break; // noop
                    default:
                        var msg =$A.get("$Label.c.Read_File_Error");
            			helper.showToast('Warning',msg,'warning');
                };
                component.set("v.spinner",false);
            }
            reader.onabort = function(e) {
                var msg =$A.get("$Label.c.File_Read_Cancel_Error");
                helper.showToast('Warning',msg,'warning');
                component.set("v.spinner",false);
            };
            reader.onload = function(e) {
                var data=e.target.result;
                component.set("v.fileContentData",data);
                var allTextLines = data.split(/\r\n|\n/);
                var dataRows=allTextLines.length-1;
                var headers = allTextLines[0].split(',');
               /* var validHeaders ="TemplateName,LocationID,CRUserDescription,ProjectName,CRCount";
                for(var i =0;i<headers.length ;i++){
                    if(!validHeaders.includes(headers[i])){
                        var msg ='Please correct header:Header name should be TemplateName,LocationID,CRUserDescription,ProjectName,CRCount';
            	    	helper.showToast('Error',msg,'error');
                        component.set("v.spinner",false);
                        return;
                    }
                }*/
                var validTemplateName = $A.get("$Label.c.Valid_Template_Name");
                var validTemplateNameArray = validTemplateName.split(',');
				console.log(validTemplateNameArray);
                
                for(var i =1;i<allTextLines.length;i++){
					var filesNotValid = false;
                    index++;
                    console.log(allTextLines[i]);
                    filecontentdata = allTextLines[i].split(',');
                   
                    var trimmedData = filecontentdata[0].trim();
                    
                    if(trimmedData === ""){
                        continue;
                    }
                    
                    
                    if(!(validTemplateNameArray.includes(trimmedData))){
                        console.log(filecontentdata[0].trim());
                        filesNotValid = true;
                        var msg ='Please correct TemplateName: '+trimmedData;
                        helper.showToast('Error',msg,'error');
                        component.set("v.spinner",false);
                        //component.set("v.showMain",true);
                        
                        helper.clearDataAfterError(component, event, helper);
                        return;                        
                    }   
                }

                if(dataRows == 0){
                    var msg ='File Rows should be grater than 1.';
            	    helper.showToast('Warning',msg,'warning');
                    component.set("v.spinner",false);
                    component.set("v.showMain",true);
                    helper.clearDataAfterError(component, event, helper);
                }
                if(!filesNotValid){
                    helper.getLocationData(component, helper, file ,component.get("v.fileContentData"));
                    
                }
            }
            reader.readAsText(file);
        }
        var reader = new FileReader();
        reader.onloadend = function() {
        };
        reader.readAsDataURL(file);
    },
     showToast: function(title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();  
    },
    uploadProcess: function(component,file,fileContents,intakeId,actionLine) {
        var startPosition = 0;   
        var endPosition = Math.min(fileContents.length,750000);
        this.uploadInChunk(component, file, fileContents, startPosition, 75000, '',intakeId,actionLine);
    },
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId,intakeId,actionLine) {
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = actionLine;
        action.setParams({
            parentId: intakeId,
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });
        action.setCallback(this, function(response) {
            attachId = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + 75000);
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId,actionLine);
                } else {
                     component.set("v.spinner",false);
                }       
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
                component.set("v.spinner",true);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);
    },
    displayDuplicateRecords: function(component, helper,duplicateDataRowNum){
        var allTextLines = component.get("v.fileContentData").split(/\r\n|\n/);
        var dataRows = allTextLines.length-1;
        var headers = allTextLines[0].split(',');
        var lines = [];
        var index = 0;
        var filecontentdata;
        component.set("v.spinner",false);
        component.set("v.isOpenDuplicate", true);
        var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
        content += "<thead><tr class=\"slds-text-title--caps\">";
        content += '<th scope=\"col"\>Index</th>';
        for(i=0;i<headers.length; i++){
            content += '<th scope=\"col"\>'+headers[i]+'</th>';
        }
        content += "</tr></thead>";
        for (var i=1; i<allTextLines.length; i++) {
            if(duplicateDataRowNum.includes(i)){
                index++;
                filecontentdata = allTextLines[i].split(',');
                if(filecontentdata[0]!=''){
                    content +="<tr>";
                    content +='<td>'+index+'</td>';
                    for(var j=0;j<filecontentdata.length;j++){
                        content +='<td>'+filecontentdata[j]+'</td>';
                    }
                    content +="</tr>";
                }
            }
        }
        content += "</table>";
        component.set("v.DuplicateData",content);
	},
    clearDataAfterError :function(component, event, helper) {
       component.set("v.fileName",'');
       component.set("v.TargetFileName",'');
       component.set("v.TableContent",'');
       component.set(component.find("fileId").get("v.files"),'');
   }
    
})
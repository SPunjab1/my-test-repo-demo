({
    doInit : function(component, event, helper) {
        var action = component.get("c.getRegionalApprover");
        
        action.setParams({"oppId" : component.get('v.recordId')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
               
                var accessProfiles = $A.get("$Label.c.NLG_Abstract_Profiles");
                
                if(accessProfiles.includes(res.loggedinUserProfile)){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": '/apex/NLGAbstractEditable?Id='+component.get('v.recordId') + '&rrapp='+res.regionalApprover + '&rent=' + res.rentIncrease+ '&sign=' + res.signAuthority
                    });
                    urlEvent.fire();
                }else{
                    
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type" : "error",
                            "message": 'You Do not have Permissions to complete the necessary Action. Please contact your system Administrator.'
                        });
                        toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                
            }
            else if (state === "ERROR") {
                
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type" : "error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);	
    }
})
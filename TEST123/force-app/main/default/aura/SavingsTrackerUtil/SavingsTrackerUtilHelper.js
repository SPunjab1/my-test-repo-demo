({
    toggleSection: function(component, event) {
        var sectionAuraId = event.target.getAttribute("data-auraId");
        var sectionContentDiv = component.find(sectionAuraId+'Content');
        $A.util.toggleClass(sectionContentDiv, 'slds-is-open');
	}
})
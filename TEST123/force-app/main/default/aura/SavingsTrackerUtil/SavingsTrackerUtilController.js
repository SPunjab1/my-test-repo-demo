({
    toggleSection: function(component, event, helper) {
        helper.toggleSection(component, event);
	},
    
    showSpinner: function(component, event, helper) {
		component.set("v.spinner", true); 
   	},
    
    hideSpinner : function(component, event, helper){   
		component.set("v.spinner", false);
    },
})
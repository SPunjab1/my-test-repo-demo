({
	loadChevron : function(component, event, helper) {
        var action = component.get("c.getChevronData");
         action.setParams({
            recId : component.get("v.recordId")
        });       
        action.setCallback(this, function(res) { 
            helper.handleCallback(component, event, helper,res); 
        }); 
        $A.enqueueAction(action); 
	},
    handleCallback : function(component, event, helper,res){
        if (res.getState() === 'SUCCESS') { 
            var retJSON = JSON.parse(res.getReturnValue());
            component.set("v.records",retJSON);
        }
        else{  
            console.log('Error...');
        }
    }

})
({
    doInit: function(component,event,helper){
        var recordId = component.get("v.recordId");
        console.log('recordId:'+recordId);
        component.set('v.columns', [
            {label: 'Opportunity Name', 
             fieldName: 'oppUrl',
             type: 'url', 
             typeAttributes: { 
                 label:{
                     fieldName: 'Lease_Intake__c'
                 },
                 target:'_blank'
             }},
            
            {label: 'Landlord', fieldName: 'Landlord__c', type: 'text'},
            
            {label: 'CLIQId', fieldName: 'CLIQ_ID__c', type: 'text'},
            {label: 'CLIQSubType', fieldName: 'CLIQ_Sub_Type__c', type: 'text'},
            {label: 'Total Rent', fieldName: 'Total_Rent_over_total_term__c', type: 'number',typeAttributes: {
                                                                                   maximumFractionDigits: 3}},
            {label: 'Savings', fieldName: 'Gross_Savings__c', type: 'number',typeAttributes: {
                                                                                   maximumFractionDigits: 3}},
            {label: 'Stage', fieldName: 'StageName', type: 'text'},
            {label: 'Review/Reject Reason', fieldName: 'Approve_Reject_Reason__c', type: 'text'}
        ]);
        if(recordId != ''){
            var action = component.get("c.getOpportunities");
            action.setParams({"recordId":recordId});
            action.setCallback(this,function(response){
                if(response.getState()==="SUCCESS"){
                    console.log('response:'+response.getReturnValue());
                    var jsonResp = response.getReturnValue();
                    var resp = JSON.parse(jsonResp);
                    
                    if(resp.oppList !=null && resp.oppList !=undefined){
                        resp.oppList.forEach(function(item) {
                            item.oppUrl= '/lightning/r/Opportunity/' + item['Id'] + '/view';
                            console.log(item);
                        });
                        component.set("v.data",resp.oppList);
                        
                        
                        if(resp.AvoidanceOpp){
                            console.log('AvoidanceOpp:'+resp.AvoidanceOpp);
                            var selected_rows_mod = [];
                            selected_rows_mod.push(resp.AvoidanceOpp);
                            component.set("selectedRows",selected_rows_mod);
                            console.log('selectedRows:'+selected_rows_mod);
                        }
                        
                    }
                } else {
                    console.log('error:'+response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }
        
    },
    updateOpportunity:function(component,event,helper){
        var recordId = component.get("v.recordId");
        var selectedIds = component.get("v.selectedOpp");
        console.log('MarkAvoidance:',selectedIds);
        if(selectedIds !=null && selectedIds.length>0){
            var selectedId = selectedIds[0].Id;
            console.log('selectedId:',selectedId);
            var action = component.get("c.updateOpportunity");
            action.setParams({oppId:recordId,avoidanceOppId:selectedId});
            action.setCallback(this,function(response){
                if(response.getState() ==='SUCCESS'){
                    if(response.getReturnValue()=='Success'){
                        console.log('Success');
                        this.showToast('success','Successfully marked Opportunity as Avoidance.','Success');
                        $A.get('e.force:refreshView').fire();
                    } else {
                        console.log('Error:', response.getReturnValue());
                        this.showToast('error',response.getReturnValue(),'Error');
                    }
                } else {
                    console.log('Exception:', response.getReturnValue());
                    this.showToast('error',response.getReturnValue(),'Error');
                }
            });
            $A.enqueueAction(action);
        } else {
           this.showToast('warning','Please select a Opportunity to mark for Avoidance.','Warning'); 
        }
    },
    showToast:function(type,message,title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            title:title,
            message: message,
            type:''+type+''
        });
        toastEvent.fire();
    }
})
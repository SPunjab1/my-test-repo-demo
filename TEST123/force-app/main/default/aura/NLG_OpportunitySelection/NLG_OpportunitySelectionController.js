({
	doInit : function(component, event, helper) {
        helper.doInit(component,event,helper);
    },
    getSelected : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows'); 
        var setRows = [];
        for ( var i = 0; i < selectedRows.length; i++ ) {
            setRows.push(selectedRows[i]);
        }
        component.set("v.selectedOpp", setRows);
         console.log('SelectedOpp:'+setRows);
    },
    markAvoidance: function(component,event,helper){
        helper.updateOpportunity(component,event,helper);
    }
})
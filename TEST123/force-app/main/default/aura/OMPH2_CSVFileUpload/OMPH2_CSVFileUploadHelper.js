({
	showToast: function(title,message,type) {
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            type: type,
            mode: (type == 'error' ? 'sticky' :'pester'),
            
        });
        toastEvent.fire();  
    },
    getFileDetailsJs : function(component, event, helper) {
        var fileName = 'No File Selected..';
        var fileType;
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
            fileType = event.getSource().get("v.files")[0]['type'];
            console.log(event.getSource().get("v.files")[0]);
        }
        component.set("v.fileName", fileName);
        component.set("v.fileType", fileType);
    },
    displayFileJS : function(component, event, helper) {
        component.set("v.spinner",true);
		var file = component.find("fileId").get("v.files");
        if(file!=null){
            if(file.length > 0) {
                helper.readFile(component, helper, file);
            }else {
                component.set("v.spinner",false);
                //var msg = $A.get("$Label.c.Valid_File_Error");
                var msg = 'Please Select a Valid File..';
                helper.showToast('Error',msg,'error');
            }    
        }
        else{
            component.set("v.spinner",false);
        	//var msg =$A.get("$Label.c.File_not_uploaded_error_msg");
            var msg = 'File is not uploaded, Please upload file.';
            helper.showToast('Error',msg,'error');
        }
	},
    uploadFileJS : function(component, event, helper) {
        component.set("v.spinner",true);
        var file = component.find("fileId").get("v.files");
        if(file!=null){
            if(file.length > 0) {
                helper.validateFileType(component, event, helper, file);
                setTimeout(function(){
                    helper.getCSVData(component, helper, file ,component.get("v.fileContentData"));
                }, 5000);
            }else {
                component.set("v.spinner",false);
                //var msg = $A.get("$Label.c.Valid_File_Error");
                var msg = 'Please Select a Valid File..';
                helper.showToast('Error',msg,'error');
            }    
        }
        else{
            component.set("v.spinner",false);
        	//var msg =$A.get("$Label.c.File_not_uploaded_error_msg");
            var msg = 'File is not uploaded, Please upload file.';
            helper.showToast('Error',msg,'error'); 
        }
    },
    readFile: function(component, helper, file) {
        var file = file[0];
        if (!file) return;
        console.log('filename'+file.name);
        if(!file.name.match(/\.(csv||CSV)$/)){
            component.set("v.spinner",false);
            //var msg =$A.get("$Label.c.Supported_File_Error");
            var msg = 'Upload supports CSV file only, Please check and upload CSV file.';
            helper.showToast('Error',msg,'error');
           // helper.clearDataAfterError(component, event, helper);
            return;
        }else{
            reader = new FileReader();
            reader.onerror =function errorHandler(evt) {
                switch(evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        component.set("v.spinner",false);
                        //var msg =$A.get("$Label.c.File_Not_Found_Error");
                        var msg = 'File Not Found!';
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        component.set("v.spinner",false);
                        //var msg =$A.get("$Label.c.File_Unreadable_Error");
                        var msg ='File is not readable!';
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.ABORT_ERR:
                        component.set("v.spinner",false);
                        break;
                    default:
                         component.set("v.spinner",false);
                        //var msg =$A.get("$Label.c.Read_File_Error");
                        var msg ='An error occurred reading this file.';
            			helper.showToast('Warning',msg,'warning');
                };
            }
            reader.onabort = function(e) {
                component.set("v.spinner",false);
                //var msg =$A.get("$Label.c.File_Read_Cancel_Error");
                var msg = 'File read cancelled.';
                helper.showToast('Warning',msg,'warning');
            };
            reader.onloadstart = function(e) { 
                var output = '<ui type=\"disc\"><li><strong>'+file.name +'</strong> ('+file.type+')- '+file.size+'bytes, last modified: '+file.lastModifiedDate.toLocaleDateString()+'</li></ui>';
                component.set("v.filename",file.name);
                component.set("v.TargetFileName",output);
            };
            reader.onload = function(e) {
                var data=e.target.result;
                component.set("v.fileContentData",data);
                console.log("file data"+JSON.stringify(data));
                var allTextLines = data.split(/\r\n|\n/);
                var dataRows = allTextLines.length-1;
                var headers = allTextLines[0].split(',');
                if(dataRows <= 0){
                    component.set("v.spinner",false);
                    var msg ='File Rows should be greater than 1.';
            	    helper.showToast('Warning',msg,'warning');
                    helper.clearDataAfterError(component, event, helper);
                } 
                else{
                    var lines = [];
                    var filecontentdata;
                    var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
                    content += "<thead><tr class=\"slds-text-title--caps\">";
                    content += '<th scope=\"col"\>Index</th>';
                    for(i=0;i<headers.length; i++){
                        content += '<th scope=\"col"\>'+headers[i]+'</th>';
                    }
                    content += "</tr></thead>";
                    for (var i=1; i<allTextLines.length-1; i++) {
                        filecontentdata = allTextLines[i].split(',');
                        //if(filecontentdata[0]!=''){
                            content +="<tr>";
                            content +='<td>'+i +'</td>';
                            for(var j=0;j<filecontentdata.length;j++){
                                content +='<td>'+filecontentdata[j]+'</td>';
                            }
                            content +="</tr>";
                        //}
                    }
                    content += "</table>";
                    component.set("v.TableContent",content);           
                }
            }
            reader.readAsText(file);
        }
        var reader = new FileReader();
        reader.onloadend = function() {
        };
        reader.readAsDataURL(file);
        component.set("v.spinner",false);
    },
    clearDataAfterError :function(component, event, helper) {
       component.set("v.fileName",'');
       component.set("v.TargetFileName",'');
       component.set("v.TableContent",null);
       component.set("v.fileContentData",null);
       component.set(component.find("fileId").get("v.files"),'');
   },
   validateFileType : function(component, event, helper, file) {
        var file = file[0];
        if (!file) return;
        console.log('filename'+file.name);
        if(!file.name.match(/\.(csv||CSV)$/)){
            component.set("v.spinner",false);
            //var msg =$A.get("$Label.c.Supported_File_Error");
            var msg = 'Upload supports CSV file only, Please check and upload CSV file.';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
            return;
        }else{ 
            reader = new FileReader();
            reader.onerror =function errorHandler(evt) {
                switch(evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        component.set("v.spinner",false);
                        //var msg =$A.get("$Label.c.File_Not_Found_Error");
                        var msg = 'File Not Found!';
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        component.set("v.spinner",false);
                        //var msg =$A.get("$Label.c.File_Unreadable_Error");
                        var msg ='File is not readable!';
            			helper.showToast('Error',msg,'error');
                        break;
                    case evt.target.error.ABORT_ERR:
                        component.set("v.spinner",false);
                        break;
                    default:
                        component.set("v.spinner",false);
                        //var msg =$A.get("$Label.c.Read_File_Error");
                        var msg ='An error occurred reading this file.';
            			helper.showToast('Warning',msg,'warning');
                };
            }
            reader.onabort = function(e) {
                component.set("v.spinner",false);
                //var msg =$A.get("$Label.c.File_Read_Cancel_Error");
                var msg = 'File read cancelled.';
                helper.showToast('Warning',msg,'warning');
            };
            reader.onloadstart = function(e) { 
                var output = '<ui type=\"disc\"><li><strong>'+file.name +'</strong> ('+file.type+')- '+file.size+'bytes, last modified: '+file.lastModifiedDate.toLocaleDateString()+'</li></ui>';
                component.set("v.filename",file.name);
                component.set("v.TargetFileName",output);
            };
            reader.onload = function(e) {
                var data=e.target.result;
                component.set("v.fileContentData",data);
                console.log("file data"+JSON.stringify(data));
                var allTextLines = data.split(/\r\n|\n/);
                var dataRows = allTextLines.length-1;
                var headers = allTextLines[0].split(',');
                if(dataRows <= 0){
                    component.set("v.spinner",false);
                    var msg ='File Rows should be greater than 1.';
            	    helper.showToast('Warning',msg,'warning');
                    helper.clearDataAfterError(component, event, helper);
                } 
            }
            reader.readAsText(file);
        }
        var reader = new FileReader();
        reader.onloadend = function() {
        };
        reader.readAsDataURL(file);
    },
    getCSVData : function(component, helper, file, data) {
        var allTextLines = data.split(/\r\n|\n/);
        var dataRows = allTextLines.length-1;
        var headers = allTextLines[0].split(',');
		var ponIndex;
		var prodCatNameIndex;
		var tradingPartIndex;
		var activityIndex;
		var workOrderTaskIndex;
        var startIntegrationIndex;
		var CircuitType1Index;
        var CircuitType2Index;
		var circuitId1Index;
        var circuitId2Index;
		var controlSite1Index;
        var controlSite2Index;
		var orderingGuide1Index;
        var orderingGuide2Index;
		var asrType1Index;
        var asrType2Index;
        var asrPON1Index;
        var asrPON2Index;
        //var subServiceType1Index;
        //var subServiceType2Index;
        var BillingCircuitId1Index;
        var BillingCircuitId2Index;
        var ASiteId1Index;
        var ASiteId2Index;
        var ZSiteId1Index;
        var ZSiteId2Index;
        var Bandwidth1Index;
        var Bandwidth2Index;
        var UserCircuitId1Index;
        var UserCircuitId2Index;
        var BAN1Index;
        var BAN2Index;
        var Category1Index;
        var Category2Index;
        var IsMeetingPoint1Index;
        var IsMeetingPoint2Index;
        var LOAProvided1Index;
        var LOAProvided2Index;
        var OrderedDate1Index;
        var OrderedDate2Index;
        var VLAN1Index;
        var VLAN2Index;
        var VLAN21Index;
        var VLAN22Index;
        var ChangeType1Index;
        var ChangeType2Index;
        var ChangeValue1Index;
        var ChangeValue2Index;
        var EthernetTechonlogyType1Index;
        var EthernetTechonlogyType2Index;
        var EthernetCircuitType1Index;
        var EthernetCircuitType2Index;
        //var AAVType1Index;
        //var AAVType2Index;
        var CIR1Index;
        var CIR2Index;
        var TestCIR1Index;
        var TestCIR2Index;
        var ForecastVersion1Index;
        var ForecastVersion2Index;
        var OrderNumber1Index;
        var OrderNumber2Index;
        var OrderDestination1Index;
        var OrderDestination2Index;
        var ServiceType1Index;
        var ServiceType2Index;
        var TragetDownloadSpeed1Index;
        var TragetDownloadSpeed2Index;
        var AAV1Index;
        var AAV2Index;
        var DDDIndex;
        var productTypeToNeustar1Index;
        var productTypeToNeustar2Index
       // var VLANUNI1Index;
       // var VLANUNI2Index;
       // var EBD1Index;
       // var EBD2Index;
         
        
	   
        for(var i =0;i<headers.length ;i++){
            if(headers[i] == 'PON'){
                ponIndex =  i;  
            }
            if(headers[i] == 'ProductCatalogName'){
                prodCatNameIndex =  i;  
            }
            if(headers[i] == 'TradingPartner'){
                tradingPartIndex =  i;  
            }
            if(headers[i] == 'Activity'){
                activityIndex =  i;  
            }
            if(headers[i] == 'DDD'){
                DDDIndex = i;
            }
            if(headers[i] == 'WorkOrderTask'){
                workOrderTaskIndex =  i;  
            }
            if(headers[i] == 'Start Integration'){
                startIntegrationIndex = i;
            }
            if(headers[i] == 'CircuitType1'){
                CircuitType1Index =  i;  
            }
            if(headers[i] == 'CircuitType2'){
                CircuitType2Index =  i;  
            }
            if(headers[i] == 'CircuitId1'){
                circuitId1Index =  i;  
            }
            if(headers[i] == 'CircuitId2'){
                circuitId2Index =  i;  
            }
            if(headers[i] == 'ControllingSite1'){
                controlSite1Index =  i;  
            }
            if(headers[i] == 'ControllingSite2'){
                controlSite2Index =  i;  
            }
            if(headers[i] == 'OrderingGuideId1'){
            	orderingGuide1Index = i;    
            }
            if(headers[i] == 'OrderingGuideId2'){
            	orderingGuide2Index = i;    
            }
            if(headers[i] == 'BillingCircuitId1'){
            	BillingCircuitId1Index = i;    
            }
            if(headers[i] == 'BillingCircuitId2'){
            	BillingCircuitId2Index = i;    
            }
            if(headers[i] == 'ASiteId1'){
            	ASiteId1Index = i;    
            }
            if(headers[i] == 'ASiteId2'){
            	ASiteId2Index = i;    
            }
            if(headers[i] == 'ZSiteId1'){
            	ZSiteId1Index = i;    
            }
            if(headers[i] == 'ZSiteId2'){
            	ZSiteId2Index = i;    
            }
            if(headers[i] == 'Bandwidth1'){
            	Bandwidth1Index = i;    
            }
            if(headers[i] == 'Bandwidth2'){
            	Bandwidth2Index = i;    
            }
            if(headers[i] == 'UserCircuitId1'){
            	UserCircuitId1Index = i;    
            }
            if(headers[i] == 'UserCircuitId2'){
            	UserCircuitId2Index = i;    
            }
            if(headers[i] == 'BAN1'){
            	BAN1Index = i;    
            }
            if(headers[i] == 'BAN2'){
            	BAN2Index = i;    
            }
            if(headers[i] == 'Category1'){
            	Category1Index = i;    
            }
            if(headers[i] == 'Category2'){
            	Category2Index = i;    
            }
            if(headers[i] == 'IsMeetingPoint1'){
            	IsMeetingPoint1Index = i;    
            }
            if(headers[i] == 'IsMeetingPoint2'){
            	IsMeetingPoint2Index = i;    
            }
            if(headers[i] == 'LOAProvided1'){
            	LOAProvided1Index = i;    
            }
            if(headers[i] == 'LOAProvided2'){
            	LOAProvided2Index = i;    
            }
            if(headers[i] == 'ASRType1'){
            	asrType1Index = i;    
            }
            if(headers[i] == 'ASRType2'){
            	asrType2Index = i;    
            }
            if(headers[i] == 'ASRPON1'){
            	asrPON1Index = i;    
            }
            if(headers[i] == 'ASRPON2'){
            	asrPON2Index = i;    
            }
            /*if(headers[i] == 'SubServiceType1'){
            	subServiceType1Index = i;    
            }
            if(headers[i] == 'SubServiceType2'){
            	subServiceType2Index = i;    
            }*/
            if(headers[i] == 'OrderedDate1'){
            	OrderedDate1Index = i;    
            }
            if(headers[i] == 'OrderedDate2'){
            	OrderedDate2Index = i;    
            }
            if(headers[i] == 'VLAN1'){
            	VLAN1Index = i;    
            }
            if(headers[i] == 'VLAN2'){
            	VLAN2Index = i;    
            }
            if(headers[i] == 'VLAN 21'){
                VLAN21Index =i;
            }
            if(headers[i] == 'VLAN 22'){
                VLAN22Index =i;
            }
            if(headers[i] == 'ChangeType1'){
            	ChangeType1Index = i;    
            }
            if(headers[i] == 'ChangeType2'){
            	ChangeType2Index = i;    
            }
            if(headers[i] == 'ChangeValue1'){
            	ChangeValue1Index = i;    
            }
            if(headers[i] == 'ChangeValue2'){
            	ChangeValue2Index = i;    
            }
            if(headers[i] == 'TechonlogyType1'){
            	EthernetTechonlogyType1Index = i;    
            }
            if(headers[i] == 'TechonlogyType2'){
            	EthernetTechonlogyType2Index = i;    
            }
            if(headers[i] == 'CircuitTypeOnASR1'){
            	EthernetCircuitType1Index = i;    
            }
            if(headers[i] == 'CircuitTypeOnASR2'){
            	EthernetCircuitType2Index = i;    
            }
            /*if(headers[i] == 'AAVType1'){
            	AAVType1Index = i;    
            }
            if(headers[i] == 'AAVType2'){
            	AAVType2Index = i;    
            }*/
            if(headers[i] == 'CIR1'){
            	CIR1Index = i;    
            }
            if(headers[i] == 'CIR2'){
            	CIR2Index = i;    
            }
            if(headers[i] == 'TestCIR1'){
            	TestCIR1Index = i;    
            }
            if(headers[i] == 'TestCIR2'){
            	TestCIR2Index = i;    
            }
            if(headers[i] == 'ForecastVersion1'){
            	ForecastVersion1Index = i;    
            }
            if(headers[i] == 'ForecastVersion2'){
            	ForecastVersion2Index = i;    
            }
            if(headers[i] == 'OrderNumber1'){
            	OrderNumber1Index = i;    
            }
            if(headers[i] == 'OrderNumber2'){
            	OrderNumber2Index = i;    
            }
            if(headers[i] == 'OrderDestination1'){
            	OrderDestination1Index = i;    
            }
            if(headers[i] == 'OrderDestination2'){
            	OrderDestination2Index = i;    
            }
            if(headers[i] == 'ServiceType1'){
            	ServiceType1Index = i;    
            }
            if(headers[i] == 'ServiceType2'){
            	ServiceType2Index = i;    
            }
            if(headers[i] == 'TragetDownloadSpeed1'){
            	TragetDownloadSpeed1Index = i;    
            }
            if(headers[i] == 'TragetDownloadSpeed2'){
            	TragetDownloadSpeed2Index = i;    
            }
            if(headers[i] == 'AAV1'){
            	AAV1Index = i;    
            }
            if(headers[i] == 'AAV2'){
            	AAV2Index = i;    
            }
            if(headers[i] == 'ProductTypeToNeustar1'){
            	productTypeToNeustar1Index = i;    
            }
            if(headers[i] == 'ProductTypeToNeustar2'){
            	productTypeToNeustar2Index = i;    
            }
           
            /*if(headers[i] == 'VLAN-UNI1'){
            	VLANUNI1Index = i;    
            }
            if(headers[i] == 'VLAN-UNI2'){
            	VLANUNI2Index = i;    
            }
            if(headers[i] == 'EBD1'){
            	EBD1Index = i;    
            }
            if(headers[i] == 'EBD2'){
            	EBD2Index = i;    
            }*/
        }
        var fileContentMap = new Map();
        var poMap = new Map();
        var circuitMap = new Map();
        var asrMap = new Map();
        var woTaskStrtIntegrationMap = new Map();
        
		var duplicatePONNum = [];
        var duplicateASRPONNum= [];
        var ASRPONNumList=[];
		var ponNotPresentFlag = false;
        var asrponNotPresentFlag = false;
        var allRequiredFieldsNotProvided=false;
        var fieldsNotProvidedForExisCircuit=false;
        var isBlankCircuit1=false;
        var woTaskNotPresentFlag = false;
        var uniEvcComboNotValid=false;
        var startIntegrationNotPresent=false;
        var vlanRequiredNotPresent=false;
        var cirRequiredNotPresent=false;
        var forecastVersionNotPresent=false;
        var meetingPointNotProvided=false;
        var LOANotProvided=false;
        var targetDownloadSpeedNotProvided=false;
        var changeTypeValueNotPresent=false;
        var dateNotValid=false;
        var wOTaskLst = [];
        var locationLst=[];
        var productTypeToNeustarRequired=false;
        var AAVTypeRequired=false;
        var serviceTypeRequired=false;
        var circuitTypeonASRRequired=false;
        var technologyTypeRequired=false;
        var dependentCircuitTypeRequired=false;
        var dependentServiceTypeRequired=false;
        for(var i = 1 ; i<allTextLines.length-1 ;i++)
        { 
            var rowDataMap = new Map();
            var rowPoMap = new Map();
            var rowCircuitMap = new Map();
            var rowAsrMap = new Map();
            var rowData = allTextLines[i].split(',');
            var count=0;
            rowDataMap['PON'] = rowData[ponIndex];
			rowDataMap['ProductCatalogName'] = rowData[prodCatNameIndex];
			rowDataMap['TradingPartner'] = rowData[tradingPartIndex];
			rowDataMap['Activity'] = rowData[activityIndex];
			rowDataMap['WorkOrderTask'] = rowData[workOrderTaskIndex];
            rowDataMap['StartIntegration'] = rowData[startIntegrationIndex];
			rowDataMap['CircuitType1'] = rowData[CircuitType1Index];
            rowDataMap['CircuitType2'] = rowData[CircuitType2Index];
			rowDataMap['CircuitId1'] = rowData[circuitId1Index];
            rowDataMap['CircuitId2'] = rowData[circuitId2Index];
			rowDataMap['ControllingSite1'] = rowData[controlSite1Index];
            rowDataMap['ControllingSite2'] = rowData[controlSite2Index];
			rowDataMap['OrderingGuideId1'] = rowData[orderingGuide1Index];
            rowDataMap['OrderingGuideId2'] = rowData[orderingGuide2Index];
            rowDataMap['BillingCircuitId1'] = rowData[BillingCircuitId1Index];
            rowDataMap['BillingCircuitId2'] = rowData[BillingCircuitId2Index];
            rowDataMap['ASiteId1'] = rowData[ASiteId1Index];
            rowDataMap['ASiteId2'] = rowData[ASiteId2Index];
            rowDataMap['ZSiteId1'] = rowData[ZSiteId1Index];
            rowDataMap['ZSiteId2'] = rowData[ZSiteId2Index];
            rowDataMap['Bandwidth1'] = rowData[Bandwidth1Index];
            rowDataMap['Bandwidth2'] = rowData[Bandwidth2Index];
            rowDataMap['UserCircuitId1'] = rowData[UserCircuitId1Index];
            rowDataMap['UserCircuitId2'] = rowData[UserCircuitId2Index];
            rowDataMap['BAN1'] = rowData[BAN1Index];
            rowDataMap['BAN2'] = rowData[BAN2Index];
            rowDataMap['Category1'] = rowData[Category1Index];
            rowDataMap['Category2'] = rowData[Category2Index];
            rowDataMap['IsMeetingPoint1'] = rowData[IsMeetingPoint1Index];
            rowDataMap['IsMeetingPoint2'] = rowData[IsMeetingPoint2Index];
            rowDataMap['LOAProvided1'] = rowData[LOAProvided1Index];
            rowDataMap['LOAProvided2'] = rowData[LOAProvided2Index];
			rowDataMap['ASRtype1'] = rowData[asrType1Index];
            rowDataMap['ASRtype2'] = rowData[asrType2Index];
            rowDataMap['ASRPON1'] = rowData[asrPON1Index];
            rowDataMap['ASRPON2'] = rowData[asrPON2Index];
            //rowDataMap['SubServiceType1'] = rowData[subServiceType1Index];
            //rowDataMap['SubServiceType2'] = rowData[subServiceType2Index];
            rowDataMap['OrderedDate1'] = rowData[OrderedDate1Index];
            rowDataMap['OrderedDate2'] = rowData[OrderedDate2Index];
            rowDataMap['VLAN1'] = rowData[VLAN1Index];
            rowDataMap['VLAN2'] = rowData[VLAN2Index];
            rowDataMap['VLANTWO1'] = rowData[VLAN21Index];
            rowDataMap['VLANTWO2'] = rowData[VLAN22Index];
            rowDataMap['ChangeType1'] = rowData[ChangeType1Index];
            rowDataMap['ChangeType2'] = rowData[ChangeType2Index];
            rowDataMap['ChangeValue1'] = rowData[ChangeValue1Index];
            rowDataMap['ChangeValue2'] = rowData[ChangeValue2Index];
            rowDataMap['EthernetTechonlogyType1'] = rowData[EthernetTechonlogyType1Index];
            rowDataMap['EthernetTechonlogyType2'] = rowData[EthernetTechonlogyType2Index];
            rowDataMap['EthernetCircuitType1'] = rowData[EthernetCircuitType1Index];
            rowDataMap['EthernetCircuitType2'] = rowData[EthernetCircuitType2Index];
            //rowDataMap['AAVType1'] = rowData[AAVType1Index];
           // rowDataMap['AAVType2'] = rowData[AAVType2Index];
            rowDataMap['CIR1'] = rowData[CIR1Index];
            rowDataMap['CIR2'] = rowData[CIR2Index];
            rowDataMap['ForecastVersion1'] = rowData[ForecastVersion1Index];
            rowDataMap['ForecastVersion2'] = rowData[ForecastVersion2Index];
            rowDataMap['TestCIR1'] = rowData[TestCIR1Index];
            rowDataMap['TestCIR2'] = rowData[TestCIR2Index];
            rowDataMap['OrderNumber1'] = rowData[OrderNumber1Index];
            rowDataMap['OrderNumber2'] = rowData[OrderNumber2Index];
            rowDataMap['OrderDestination1'] = rowData[OrderDestination1Index];
            rowDataMap['OrderDestination2'] = rowData[OrderDestination2Index];
            rowDataMap['ServiceType1'] = rowData[ServiceType1Index];
            rowDataMap['ServiceType2'] = rowData[ServiceType2Index];
            rowDataMap['TragetDownloadSpeed1'] = rowData[TragetDownloadSpeed1Index];
            rowDataMap['TragetDownloadSpeed2'] = rowData[TragetDownloadSpeed2Index];
            rowDataMap['AAV1'] = rowData[AAV1Index];
            rowDataMap['AAV2'] = rowData[AAV2Index];
            rowDataMap['DDD'] = rowData[DDDIndex];
            rowDataMap['productTypeToNeustar1'] = rowData[productTypeToNeustar1Index];
            rowDataMap['productTypeToNeustar2'] = rowData[productTypeToNeustar2Index];
            //rowDataMap['VLANUNI1'] = rowData[VLANUNI1Index];
            //rowDataMap['VLANUNI2'] = rowData[VLANUNI2Index];
            //rowDataMap['EBD1'] = rowData[EBD1Index];
            //rowDataMap['EBD2'] = rowData[EBD2Index];
            
            console.log(rowDataMap);
            //check if order is combo or not
            if(rowData[CircuitType1Index] && rowData[asrPON1Index] )
                count=1;
            if(rowData[CircuitType2Index] && rowData[asrPON2Index])
                count=2;
              rowDataMap['count']=count;  
            
            //Create Purchase Order Map
            rowPoMap['PON'] = rowData[ponIndex];
            rowPoMap['ProductCatalogName'] = rowData[prodCatNameIndex];
            rowPoMap['TradingPartner'] = rowData[tradingPartIndex];
            rowPoMap['Activity'] = rowData[activityIndex];
            rowPoMap['DDD'] = rowData[DDDIndex];
            rowPoMap['WorkOrderTask'] = rowData[workOrderTaskIndex];
            rowPoMap['StartIntegration'] = rowData[startIntegrationIndex];

            //Create Circuit Map
            rowCircuitMap['PON'] = rowData[ponIndex];
            rowCircuitMap['CircuitType1'] = rowData[CircuitType1Index];
            rowCircuitMap['CircuitType2'] = rowData[CircuitType2Index];
            rowCircuitMap['CircuitId1'] = rowData[circuitId1Index];
            rowCircuitMap['CircuitId2'] = rowData[circuitId2Index];
            rowCircuitMap['ControllingSite1'] = rowData[controlSite1Index];
            rowCircuitMap['ControllingSite2'] = rowData[controlSite2Index];
            rowCircuitMap['OrderingGuideId1'] = rowData[orderingGuide1Index];
            rowCircuitMap['OrderingGuideId2'] = rowData[orderingGuide2Index];
            rowCircuitMap['BillingCircuitId1'] = rowData[BillingCircuitId1Index];
            rowCircuitMap['BillingCircuitId2'] = rowData[BillingCircuitId2Index];
            rowCircuitMap['ASiteId1'] = rowData[ASiteId1Index];
            rowCircuitMap['ASiteId2'] = rowData[ASiteId2Index];
            rowCircuitMap['ZSiteId1'] = rowData[ZSiteId1Index];
            rowCircuitMap['ZSiteId2'] = rowData[ZSiteId2Index];
            rowCircuitMap['Bandwidth1'] = rowData[Bandwidth1Index];
            rowCircuitMap['Bandwidth2'] = rowData[Bandwidth2Index];
            rowCircuitMap['UserCircuitId1'] = rowData[UserCircuitId1Index];
            rowCircuitMap['UserCircuitId2'] = rowData[UserCircuitId2Index];
            rowCircuitMap['BAN1'] = rowData[BAN1Index];
            rowCircuitMap['BAN2'] = rowData[BAN2Index];
            rowCircuitMap['Category1'] = rowData[Category1Index];
            rowCircuitMap['Category2'] = rowData[Category2Index];
            rowCircuitMap['IsMeetingPoint1'] = rowData[IsMeetingPoint1Index];
            rowCircuitMap['IsMeetingPoint2'] = rowData[IsMeetingPoint2Index];
            rowCircuitMap['LOAProvided1'] = rowData[LOAProvided1Index];
            rowCircuitMap['LOAProvided2'] = rowData[LOAProvided2Index];
            rowCircuitMap['count']=count;
            
            //Create ASR Map 
            rowAsrMap['PON'] = rowData[ponIndex];
            rowAsrMap['ASRtype1'] = rowData[asrType1Index];
            rowAsrMap['ASRtype2'] = rowData[asrType2Index];
            rowAsrMap['ASRPON1'] = rowData[asrPON1Index];
            rowAsrMap['ASRPON2'] = rowData[asrPON2Index];
            //rowAsrMap['SubServiceType1'] = rowData[subServiceType1Index];
            //rowAsrMap['SubServiceType2'] = rowData[subServiceType2Index];
            rowAsrMap['OrderedDate1'] = rowData[OrderedDate1Index];
            rowAsrMap['OrderedDate2'] = rowData[OrderedDate2Index];
            rowAsrMap['VLAN1'] = rowData[VLAN1Index];
            rowAsrMap['VLAN2'] = rowData[VLAN2Index];
            rowAsrMap['VLANTWO1'] = rowData[VLAN21Index];
            rowAsrMap['VLANTWO2'] = rowData[VLAN22Index];
            rowAsrMap['ChangeType1'] = rowData[ChangeType1Index];
            rowAsrMap['ChangeType2'] = rowData[ChangeType2Index];
            rowAsrMap['ChangeValue1'] = rowData[ChangeValue1Index];
            rowAsrMap['ChangeValue2'] = rowData[ChangeValue2Index];
            rowAsrMap['EthernetTechonlogyType1'] = rowData[EthernetTechonlogyType1Index];
            rowAsrMap['EthernetTechonlogyType2'] = rowData[EthernetTechonlogyType2Index];
            rowAsrMap['EthernetCircuitType1'] = rowData[EthernetCircuitType1Index];
            rowAsrMap['EthernetCircuitType2'] = rowData[EthernetCircuitType2Index];
            //rowAsrMap['AAVType1'] = rowData[AAVType1Index];
            //rowAsrMap['AAVType2'] = rowData[AAVType2Index];
            rowAsrMap['CIR1'] = rowData[CIR1Index];
            rowAsrMap['CIR2'] = rowData[CIR2Index];
            rowAsrMap['ForecastVersion1'] = rowData[ForecastVersion1Index];
            rowAsrMap['ForecastVersion2'] = rowData[ForecastVersion2Index];
            rowAsrMap['TestCIR1'] = rowData[TestCIR1Index];
            rowAsrMap['TestCIR2'] = rowData[TestCIR2Index];
            rowAsrMap['OrderNumber1'] = rowData[OrderNumber1Index];
            rowAsrMap['OrderNumber2'] = rowData[OrderNumber2Index];
            rowAsrMap['OrderDestination1'] = rowData[OrderDestination1Index];
            rowAsrMap['OrderDestination2'] = rowData[OrderDestination2Index];
            rowAsrMap['ServiceType1'] = rowData[ServiceType1Index];
            rowAsrMap['ServiceType2'] = rowData[ServiceType2Index];
            rowAsrMap['TragetDownloadSpeed1'] = rowData[TragetDownloadSpeed1Index];
            rowAsrMap['TragetDownloadSpeed2'] = rowData[TragetDownloadSpeed2Index];
            rowAsrMap['AAV1'] = rowData[AAV1Index];
            rowAsrMap['AAV2'] = rowData[AAV2Index];
            rowAsrMap['productTypeToNeustar1'] = rowData[productTypeToNeustar1Index];
            rowAsrMap['productTypeToNeustar2'] = rowData[productTypeToNeustar2Index];
            //rowAsrMap['VLANUNI1'] = rowData[VLANUNI1Index];
            //rowAsrMap['VLANUNI2'] = rowData[VLANUNI2Index];
            //rowAsrMap['EBD1'] = rowData[EBD1Index];
            //rowAsrMap['EBD2'] = rowData[EBD2Index];
            rowAsrMap['count']=count;
			//check for duplicate PON in CSV
			var key = rowData[ponIndex];
            var woKey =rowData[workOrderTaskIndex];
            //Check if PON number present or not on each row
            if(rowData[ponIndex]){
			}
			else{
                key = 'BLANK'+i;
				ponNotPresentFlag = true;
			}
            //store ASR PON in list to check duplicates
            if(rowData[CircuitType1Index] && rowData[asrPON1Index]){
            ASRPONNumList.push(rowData[asrPON1Index]);
            }
            if(rowData[CircuitType2Index] && rowData[asrPON2Index]){
            ASRPONNumList.push(rowData[asrPON2Index]);
            }
            console.log('ASRPONNumList:'+ASRPONNumList);
            
            //check if circuit 1 is blank 
			if(!rowData[CircuitType1Index]  )
                isBlankCircuit1=true;
            
            //Check if ASR PON1 number present or not if circuit type 1 is present on each row
            if(rowData[CircuitType1Index]  && ! rowData[asrPON1Index] ){
                asrponNotPresentFlag = true;
            }
            
            
            //Check if ASR PON2 number present or not if circuit type 2 is present on each row
            if(rowData[CircuitType2Index] && ! rowData[asrPON2Index]){
                asrponNotPresentFlag = true;
            }
            //check if two UNI or two EVC present for combo order
            if((rowData[EthernetCircuitType1Index]=='UNI' && rowData[EthernetCircuitType2Index]=='UNI') ||(rowData[EthernetCircuitType1Index]=='EVC' && rowData[EthernetCircuitType2Index]=='EVC')){
                uniEvcComboNotValid=true;
            }
            
            //Check if VLAN1 is required or not
            if((rowData[asrType1Index]=='New Service: SAT' && !rowData[VLAN1Index]) || (rowData[asrType1Index]=='New Service: Ethernet' && !rowData[VLAN1Index]))
                vlanRequiredNotPresent=true;
            
            //Check if VLAN2 is required or not
            if((rowData[asrType2Index]=='New Service: SAT' && !rowData[VLAN2Index]) || (rowData[asrType2Index]=='New Service: Ethernet' && !rowData[VLAN2Index]))
                vlanRequiredNotPresent=true;
            
            //check for CIR 
            if((rowData[asrType1Index]=='BW Change: Ethernet' && !rowData[CIR1Index]) || (rowData[asrType2Index]=='BW Change: Ethernet' && !rowData[CIR2Index]))
                cirRequiredNotPresent=true;
            
            //check for Forecast version
            if((rowData[asrType1Index]=='BW Change: Ethernet' && !rowData[ForecastVersion1Index]) || (rowData[asrType2Index]=='BW Change: Ethernet' && !rowData[ForecastVersion2Index]))
                forecastVersionNotPresent=true;
            
            //check for changeType
            if((rowData[asrType1Index]=='In Service Change' && !rowData[ChangeType1Index])|| (rowData[asrType2Index]=='In Service Change' && !rowData[ChangeType2Index]) )
                changeTypeValueNotPresent=true;
            
            //check for changeValue
            if((rowData[asrType1Index]=='In Service Change' && !rowData[ChangeValue1Index])|| (rowData[asrType2Index]=='In Service Change' && !rowData[ChangeValue2Index]) )
                changeTypeValueNotPresent=true;
            
            //check if targetDownloadSpeed1 is required or not
            if((rowData[asrType1Index]=='New Service: BB' && !rowData[TragetDownloadSpeed1Index])|| (rowData[asrType1Index]=='BW Change: BB' && !rowData[TragetDownloadSpeed1Index]))
                targetDownloadSpeedNotProvided=true;
            
            //check if targetDownloadSpeed2 is required or not
            if((rowData[asrType2Index]=='New Service: BB' && !rowData[TragetDownloadSpeed2Index])|| (rowData[asrType2Index]=='BW Change: BB' && !rowData[TragetDownloadSpeed2Index]))
                targetDownloadSpeedNotProvided=true;
            
            //check if productTypeToNeustar is required or not
            if(rowData[asrType1Index]== 'New Service: NNI' || rowData[asrType1Index] =='Disconnect: NNI' )
            {
                rowData[productTypeToNeustar1Index] ='UNI';
                console.log(rowData[productTypeToNeustar1Index]);
            }
            else if(!rowData[productTypeToNeustar1Index])
                productTypeToNeustarRequired=true;
            
            if(rowData[asrType2Index] && (rowData[asrType2Index]== 'New Service: NNI' || rowData[asrType2Index] =='Disconnect: NNI'))
            {
                rowData[productTypeToNeustar2Index] ='UNI';
                console.log(rowData[productTypeToNeustar1Index]);
            }
            else if(rowData[asrType2Index] && !rowData[productTypeToNeustar2Index])
                productTypeToNeustarRequired=true;
            
            rowAsrMap['productTypeToNeustar1'] = rowData[productTypeToNeustar1Index];
            rowAsrMap['productTypeToNeustar2'] = rowData[productTypeToNeustar2Index];
            //check for required fields if circuit type1 is Existing
            if(rowData[CircuitType1Index] == 'Existing' && (!rowData[circuitId1Index] || !rowData[asrType1Index] || !rowData[OrderedDate1Index] || !rowData[OrderDestination1Index] ))
                 fieldsNotProvidedForExisCircuit=true;
            
            //check for required fields if circuit type2 is Existing
            if(rowData[CircuitType2Index] == 'Existing' && (!rowData[circuitId2Index] || !rowData[asrType2Index] || !rowData[OrderedDate2Index] || !rowData[OrderDestination2Index] ))
                 fieldsNotProvidedForExisCircuit=true;
            
			//check if all fields for circuit 1 and Asr1 is provided or not if circuitType 1 is provided
            if(rowData[CircuitType1Index] && rowData[CircuitType1Index] != 'Existing' && (!rowData[circuitId1Index] || !rowData[controlSite1Index] || !rowData[orderingGuide1Index] || !rowData[asrType1Index] || !rowData[BillingCircuitId1Index] || !rowData[ASiteId1Index] || !rowData[ZSiteId1Index] || !rowData[Bandwidth1Index] || !rowData[UserCircuitId1Index] || !rowData[BAN1Index] || !rowData[Category1Index] || !rowData[OrderedDate1Index] || !rowData[OrderDestination1Index]))
            {
                allRequiredFieldsNotProvided=true;
            }
            // check if ordered date is greater than current date
           /* var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
            console.log(today);
            if(rowData[OrderedDate1Index]){
            var date1 = $A.localizationService.formatDate(new Date(), rowData[OrderedDate1Index]);
            console.log(date1);
               if(date1>today)
               dateNotValid=true;
            }
            if(rowData[OrderedDate2Index]){
            var date2 = $A.localizationService.formatDate(new Date(), rowData[OrderedDate2Index]);
            console.log(date2);
                if(date2>today)
               dateNotValid=true;
            }*/
            
            
            //check if all fields for circuit 2 and asr 2 is provided or not if circuitType 2 is provided
            if(rowData[CircuitType2Index] && rowData[CircuitType2Index] != 'Existing' && (!rowData[circuitId2Index] || !rowData[controlSite2Index] || !rowData[orderingGuide2Index] || !rowData[asrType2Index]||!rowData[BillingCircuitId2Index] || !rowData[ASiteId2Index] || !rowData[ZSiteId2Index] || !rowData[Bandwidth2Index] || !rowData[UserCircuitId2Index] || !rowData[BAN2Index] || !rowData[Category2Index]  || !rowData[OrderedDate2Index] || !rowData[OrderDestination2Index]))
            {
               allRequiredFieldsNotProvided=true;
            } 
            //check if service type is provided or not if asr is not 'in service change'
            if((rowData[asrType1Index] !='In Service Change' && !rowData[ServiceType1Index]) ||(rowData[asrType2Index] && rowData[asrType2Index] !='In Service Change' && !rowData[ServiceType2Index]))
                serviceTypeRequired=true;
            //check if AAV is required --not required for 'in service change'
            if((rowData[asrType1Index] !='In Service Change' && !rowData[AAV1Index]) ||(rowData[asrType2Index] && rowData[asrType2Index] !='In Service Change' && !rowData[AAV2Index]))
                AAVTypeRequired=true;
            //check if Technology type is required --not required for 'in service change'
            if((rowData[asrType1Index] !='In Service Change' && !rowData[EthernetTechonlogyType1Index]) ||(rowData[asrType2Index] && rowData[asrType2Index] !='In Service Change' && !rowData[EthernetTechonlogyType2Index]))
                technologyTypeRequired=true;
            //check if CircuitType On ASR is required--not requird for 'in service change'
            if((rowData[asrType1Index] !='In Service Change' && !rowData[EthernetCircuitType1Index]) ||(rowData[asrType2Index] && rowData[asrType2Index] !='In Service Change' && !rowData[EthernetCircuitType2Index]))
                circuitTypeonASRRequired=true;
            //dependent check 
            if(rowData[asrType1Index] && rowData[EthernetTechonlogyType1Index] && !rowData[EthernetCircuitType1Index] && rowData[asrType1Index] =='In Service Change' )
                dependentCircuitTypeRequired=true;
            if(rowData[asrType1Index] && rowData[EthernetCircuitType1Index] && !rowData[ServiceType1Index] && rowData[asrType1Index] =='In Service Change')
                dependentServiceTypeRequired=true;
            if(rowData[asrType2Index] && rowData[EthernetTechonlogyType2Index] && !rowData[EthernetCircuitType2Index] && rowData[asrType2Index] =='In Service Change' )
                dependentCircuitTypeRequired=true;
            if(rowData[asrType2Index] && rowData[EthernetCircuitType2Index] && !rowData[ServiceType2Index] && rowData[asrType2Index] =='In Service Change')
                dependentServiceTypeRequired=true;
            //check for boolean value IsMeetingPoint -assign false if blank
            if((rowData[IsMeetingPoint1Index] && rowData[IsMeetingPoint1Index]!='TRUE') || (rowData[IsMeetingPoint2Index] && rowData[IsMeetingPoint2Index]!='TRUE'))
            {
                meetingPointNotProvided=true;
            }
            if(!rowData[IsMeetingPoint1Index])
            {
                rowData[IsMeetingPoint1Index]='FALSE';
            }
            if(!rowData[IsMeetingPoint2Index])
            {
                rowData[IsMeetingPoint2Index]='FALSE';
            }
            
            //check for boolean value LOAProvided-assign false if blank
            if((rowData[LOAProvided1Index] && rowData[LOAProvided1Index]!='TRUE') || (rowData[LOAProvided2Index] && rowData[LOAProvided2Index]!='TRUE'))
            {
                LOANotProvided=true;
            }
            if(!rowData[LOAProvided1Index])
            {
                rowData[LOAProvided1Index]='FALSE';
            }
            if(!rowData[LOAProvided2Index])
            {
                rowData[LOAProvided2Index]='FALSE';
            }
            if(rowData[startIntegrationIndex] && rowData[startIntegrationIndex]!='Y' ){
                console.log('row data:'+rowData[startIntegrationIndex]);
                startIntegrationNotPresent=true;
            }
            //store the workordertask and startIntegration in map
            if(rowData[workOrderTaskIndex] && rowData[startIntegrationIndex]){
                woTaskStrtIntegrationMap[woKey]=rowData[startIntegrationIndex];
            }
            else if(rowData[workOrderTaskIndex] && !rowData[startIntegrationIndex])
            {
                woTaskStrtIntegrationMap[woKey]='N';
            }
            console.log('woTaskStrtIntegrationMap:'+JSON.stringify(woTaskStrtIntegrationMap));
           
            if(key in fileContentMap){
				duplicatePONNum.push(i);
			}
			else{
				fileContentMap[key] = rowDataMap; 
                poMap[key] = rowPoMap;
                circuitMap[key] = rowCircuitMap;
                asrMap[key] = rowAsrMap;
            }
            
            //get the list of Work order Task
			if(rowData[workOrderTaskIndex] ){
				wOTaskLst.push(rowData[workOrderTaskIndex]);    
            }
            else{
                woTaskNotPresentFlag = true;
            }
            
            //get the list of ASite Id and Zsite Id
            if(rowData[ASiteId1Index] && !locationLst.includes(rowData[ASiteId1Index])){
                locationLst.push(rowData[ASiteId1Index]);
            }
            if(rowData[ASiteId2Index] && !locationLst.includes(rowData[ASiteId2Index])){
                locationLst.push(rowData[ASiteId2Index]);
            }
            if(rowData[ZSiteId1Index] && !locationLst.includes(rowData[ZSiteId1Index])){
                locationLst.push(rowData[ZSiteId1Index]);
            }
            if(rowData[ZSiteId2Index] && !locationLst.includes(rowData[ZSiteId2Index])){
                locationLst.push(rowData[ZSiteId2Index]);
            }
             console.log('locationLst'+locationLst);   
        }
        
        for(var i=0;i<ASRPONNumList.length-1;i++)
        {
            for(var j=i+1;j<=ASRPONNumList.length-1;j++){
            if(ASRPONNumList[i] == ASRPONNumList[j] && !duplicateASRPONNum.includes(ASRPONNumList[i])){
                duplicateASRPONNum.push(ASRPONNumList[i]);
            }
          }
        }
        console.log('duplicateASRPONNum in CSV-->'+duplicateASRPONNum);
        component.set("v.fileContentMap",fileContentMap);
        component.set("v.poMap",poMap);
        component.set("v.circuitMap",circuitMap);
        component.set("v.asrMap",asrMap);
        component.set("v.wOTaskLst",wOTaskLst);
        component.set("v.locationLst",locationLst);
        component.set("v.woTaskStrtIntegrationMap",woTaskStrtIntegrationMap);
        if(ponNotPresentFlag){
            component.set("v.spinner",false);
        	var msg = 'PON Number on any row in CSV cannnot be empty, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
        }
        else if(asrponNotPresentFlag){
            component.set("v.spinner",false);
        	var msg = 'ASR PON Number should be provided for respective circuits for all rows, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
        }
        else if(isBlankCircuit1){
            component.set("v.spinner",false);
        	var msg = 'CircuitType1 on any row cannot be blank, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
        }
        else if(uniEvcComboNotValid){
            component.set("v.spinner",false);
        	var msg = 'Two UNI or Two EVC for Circuit Type On ASR is not allowed. It should always be one UNI and one EVC for a Combo Order., Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
                
        }
        else if(vlanRequiredNotPresent){
            component.set("v.spinner",false);
            var msg = ' VLAN is required field if ASR Type is New Service: SAT or New Service: Ethernet on any row, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(cirRequiredNotPresent){
            component.set("v.spinner",false);
            var msg = ' CIR is required field if ASR Type is BW Change: Ethernet on any row, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
                
        }
        else if(forecastVersionNotPresent){
            component.set("v.spinner",false);
            var msg = ' Forecast Version is required field if ASR Type is BW Change: Ethernet on any row, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
                
        }
        
       else if(changeTypeValueNotPresent){
            component.set("v.spinner",false);
            var msg = ' ChangeType and ChangeValue is required field if ASR Type is In Service Change on any row, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
       else if(targetDownloadSpeedNotProvided){
            component.set("v.spinner",false);
            var msg = ' TargetDownloadSpeed is required field if ASR Type is New Service: BB or BW Change: BB on any row, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if (meetingPointNotProvided){
            component.set("v.spinner",false);
            var msg = ' Please provide IsMeetingPoint field value as TRUE or keep it blank for FALSE';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
            
        }
        else if(productTypeToNeustarRequired){
            component.set("v.spinner",false);
            var msg = 'Product Type to Neustar value is required if ASR type is not NNI';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
       else if(serviceTypeRequired){
            component.set("v.spinner",false);
            var msg = 'Service Type is required if ASR type is not In Service Change';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(AAVTypeRequired){
            component.set("v.spinner",false);
            var msg = 'AAV is required if ASR type is not In Service Change';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(circuitTypeonASRRequired){
            component.set("v.spinner",false);
            var msg = 'Circuit Type on ASR is required if ASR type is not In Service Change';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(technologyTypeRequired){
            component.set("v.spinner",false);
            var msg = 'Technology Type on ASR is required if ASR type is not In Service Change';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(dependentCircuitTypeRequired){
            component.set("v.spinner",false);
            var msg = 'Circuit Type on ASR is required if Technology Type is provided';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(dependentServiceTypeRequired){
            component.set("v.spinner",false);
            var msg = 'Service Type on ASR is required if Circuit Type is provided';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if (LOANotProvided){
            component.set("v.spinner",false);
            var msg = ' Please provide LOAProvided field value as TRUE or keep it blank for FALSE';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
            
        }
        /*else if(dateNotValid){
            component.set("v.spinner",false);
            var msg = ' Future date is not allowed in Ordered date.Please provide Date smaller than or equal to current date.';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper); 
        }*/
        else if(fieldsNotProvidedForExisCircuit){
            component.set("v.spinner",false);
            var msg = 'Required fields for Exisitng Circuit Type: CircuitId, ASRType, ASRPON, OrderedDate, OrderDestination.\nPlease check if these fields are provided for all rows in CSV and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
            }
            
         else if(allRequiredFieldsNotProvided){
            component.set("v.spinner",false);
            var msg = 'Required fields for Circuit: CircuitId, ControllingSite, OrderingGuideId, BillingCircuitId, ASiteId, ZSiteId, Bandwidth, UserCircuitId, BAN, Category.\n Required fields for ASR: ASRType, ASRPON, OrderedDate, OrderDestination.\nPlease check if these fields are provided for all rows in CSV and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
        }
        
        else if(duplicateASRPONNum.length>0){
            component.set("v.spinner",false);
            var msg = 'Following duplicates based on ASR PON Number are found in uploaded CSV, please provide Unique ASRPON value across the CSV and upload again:'+ duplicateASRPONNum;
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);
        }
        else if(woTaskNotPresentFlag){
            component.set("v.spinner",false);
        	var msg = 'Work Order Task on any row in CSV cannnot be empty, Please correct it and upload CSV again';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
        }
        else if(startIntegrationNotPresent){
            component.set("v.spinner",false);
        	var msg = 'Please provide correct value for Start Integration as Y or keep it as blank for all the rows in csv.';
            helper.showToast('Error',msg,'error');
            helper.clearDataAfterError(component, event, helper);    
        }
        else if(duplicatePONNum.length>0){
        	helper.displayDuplicateRecords(component, helper,duplicatePONNum);	    
        }
        else{
        	helper.validateDataInDb(component, event, helper, fileContentMap,ASRPONNumList);	
        }
    },
    displayDuplicateRecords: function(component, helper, duplicatePONNum){
        var allTextLines = component.get("v.fileContentData").split(/\r\n|\n/);
        var dataRows = allTextLines.length-1;
        var headers = allTextLines[0].split(',');
        var lines = [];
        var index = 0;
        var filecontentdata;
        component.set("v.isOpenDuplicate", true);
        var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
        content += "<thead><tr class=\"slds-text-title--caps\">";
        content += '<th scope=\"col"\>Index</th>';
        for(i=0;i<headers.length; i++){
            content += '<th scope=\"col"\>'+headers[i]+'</th>';
        }
        content += "</tr></thead>";
        for (var i=1; i<allTextLines.length; i++) {
            if(duplicatePONNum.includes(i)){
                index++;
                filecontentdata = allTextLines[i].split(',');
                if(filecontentdata[0]!=''){
                    content +="<tr>";
                    content +='<td>'+index+'</td>';
                    for(var j=0;j<filecontentdata.length;j++){
                        content +='<td>'+filecontentdata[j]+'</td>';
                    }
                    content +="</tr>";
                }
            }
        }
        content += "</table>";
        component.set("v.DuplicateData",content);
        component.set("v.spinner",false);
	},
    validateDataInDb :function(component, event, helper, fileContentMap,ASRPONNumList){
        var action = component.get("c.checkDuplicateRecords");
        action.setParams({ "fileContentMap" : fileContentMap});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if(state === "SUCCESS" && !$A.util.isEmpty(result)) { 
                var duplicatePonNumberLst = response.getReturnValue();
                var message='For below highlighted PON , Purchase orders are already present in database, Please provide unique PON Number in CSV and upload it again.';
                helper.displayDatabaseDuplicateRecords(component, event, helper, duplicatePonNumberLst,message);
            }
            else if(state === "SUCCESS" && $A.util.isEmpty(result)){
               // helper.validatekWOTask(component, event, helper,fileContentMap);
                helper.validateASRPONInDb(component, event, helper,ASRPONNumList,fileContentMap);
            }
            else{
            	console.log('Error in validateDataInDb method :OMPH2_CSVFileUpload'); 
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);   
       // helper.showToast('Validation','Validating purchase orders.','info');
    },
    
    validateASRPONInDb :function(component, event, helper, ASRPONNumList,fileContentMap){
        var action = component.get("c.checkDuplicateASRPONnum");
        action.setParams({ "asrPONNumList" : ASRPONNumList});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if(state === "SUCCESS" && !$A.util.isEmpty(result)) { 
                var duplicateASRPonNumberLst = response.getReturnValue();
                var message='For below highlighted ASR PONS, ASRs are already present in the database, Please provide unique ASR PON Number in CSV and upload it again.';
                console.log('duplicateASRPonNumberin db:'+duplicateASRPonNumberLst);
                helper.displayDatabaseDuplicateRecords(component, event, helper, duplicateASRPonNumberLst,message);
            }
            else if(state === "SUCCESS" && $A.util.isEmpty(result)){
                console.log('calling validate wo task');
                
                helper.validatekWOTask(component, event, helper,fileContentMap);
               }
            else{
            	console.log('Error in validateASRPONInDb method :OMPH2_CSVFileUpload'); 
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);    
       // helper.showToast('Validation','Validating ASR PON.','info');
    },
    
    displayDatabaseDuplicateRecords: function(component, event, helper, duplicatePonNumberLst,message){
    	var allTextLines = component.get("v.fileContentData").split(/\r\n|\n/);
        var dataRows = allTextLines.length-1;
        var headers = allTextLines[0].split(',');
        component.set("v.isOpenDuplicateInDatabase", true);
        component.set("v.message",message);
        var lines = [];
        var filecontentdata;
        var index = 0;
        var duplicateFound=false;
        var content = "<table class=\"table slds-table slds-table--bordered slds-table--cell-buffer\">";
        content += "<thead><tr class=\"slds-text-title--caps\">";
        content += '<th scope=\"col"\>Index</th>';
        for(i=0;i<headers.length; i++){
            content += '<th scope=\"col"\>'+headers[i]+'</th>';
        }
        content += "</tr></thead>";
        
        for (var i=1; i<allTextLines.length; i++) {
            for(var key in duplicatePonNumberLst){
                
                if(allTextLines[i].includes(duplicatePonNumberLst[key])){
                    index++;
                    filecontentdata = allTextLines[i].split(',');
                    if(filecontentdata[0]!=''){
                        content +="<tr>";
                        content +='<td>'+index+'</td>';
                        for(var j=0;j<filecontentdata.length;j++){
                            duplicateFound=false;
                            console.log('filecontentdata[j]:'+filecontentdata[j]);
                            console.log('duplicatePonNumberLst[key]:'+duplicatePonNumberLst[key]);
                            for(var k=0;k<duplicatePonNumberLst.length;k++){
                                if(filecontentdata[j]==duplicatePonNumberLst[k])
                                {
                                    content +='<td>'+'<b style=\"background-color:#ff0\">'+filecontentdata[j]+'</b>'+'</td>';
                                    duplicateFound=true;
                                }
                            }
                            if(duplicateFound == false)
                                content +='<td>'+filecontentdata[j]+'</td>';
                        }
                        content +="</tr>";
                    }
                    break;
                }
            }
        }
        content += "</table>";
        component.set("v.DuplicateDataFromDatabase",content); 
        component.set("v.spinner",false);
	},
    validateLocation:function(component, event, helper){
        //alert('in validate loc');
        var action = component.get("c.validateLocationId");
        action.setParams({ "locationLst" : component.get("v.locationLst")});
        console.log('locationLst'+component.get("v.locationLst"))
         action.setCallback(this, function(response) {
            var state = response.getState();
            var result =  response.getReturnValue();
              if(state === "SUCCESS") {
                var locLst = component.get("v.locationLst");
                var validMap = result['Valid'];
                var invalidMap = result['Invalid'];
                var validIdLst = new Array();
                var invalidIdLst = new Array();
                for(var idval in component.get("v.locationLst")) {
                   if(validMap[locLst[idval]]){
                        validIdLst.push(validMap[locLst[idval]]);   
                   } 
                   else{
                   		invalidIdLst.push(locLst[idval]);     
                   }
                }
                if($A.util.isEmpty(result['Valid'])){
                    component.set("v.validLocationMap",null); 
                }	
                else{
                    var validMap = result['Valid'];
                    component.set("v.validLocationMap",validMap);  
                }
                if(invalidIdLst.length >0){
                    component.set("v.spinner",false);
                    
                    var msg = 'Following Location Ids are not matching with database, please check again and correct it. Location Ids:\n';
                    if(invalidIdLst.length >25 ){
                    for(var i=0;i<invalidIdLst.length;i++){
                        if(i==invalidIdLst.length-1){
                            msg+=invalidIdLst[i]+'.';
                        }
                        else
                            msg+=invalidIdLst[i]+',';
                        if( (i%25==0 && i!=0)){
                            msg+='\n';}
                        }
                        
                    }
                    else if(invalidIdLst.length <=25){
                        console.log('in if');
                        msg+=invalidIdLst;
                    }
                    helper.showToast('Error',msg,'error');
                    helper.clearDataAfterError(component, event, helper); 
                }
                else{
                    console.log('validLocationMap ---'+JSON.stringify(component.get("v.validLocationMap")));
                    console.log('before calling create location spinner:'+component.get("v.spinner"));
                    helper.createRecords(component, event, helper);    
                }
            }
            else{
            	console.log('Error in validateLocationId method :OMPH2_CSVFileUpload');  
                component.set("v.spinner",false);
            }
             });
        $A.enqueueAction(action);  
       // helper.showToast('Validation','Validating locations.','info');
    },
    validatekWOTask: function(component, event, helper, fileContentMap){ 
        var action = component.get("c.validateWorkOrderTask");
        action.setParams({ "wOTaskLst" : component.get("v.wOTaskLst")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result =  response.getReturnValue();
            if(state === "SUCCESS") {
                var woTaskLst = component.get("v.wOTaskLst");
                var validMap = result['Valid'];
                var invalidMap = result['Invalid'];
                var validIdLst = new Array();
                var invalidIdLst = new Array();
                for(var idval in component.get("v.wOTaskLst")) {
                   if(validMap[woTaskLst[idval]]){
                        validIdLst.push(validMap[woTaskLst[idval]]);   
                   } 
                   else if(!invalidIdLst.includes(woTaskLst[idval])){
                   		invalidIdLst.push(woTaskLst[idval]);     
                   }
                }
                if($A.util.isEmpty(result['Valid'])){
                    component.set("v.validWoTaskMap",null); 
                }	
                else{
                    var validMap = result['Valid'];
                    component.set("v.validWoTaskMap",validMap);  
                }
                if(invalidIdLst.length >0){
                    component.set("v.spinner",false);
                    var msg = 'Following WorkOrderTask Numbers are not matching with database, please check again and correct it. WorkOrderTask: '+ invalidIdLst;
                    helper.showToast('Error',msg,'error');
                    helper.clearDataAfterError(component, event, helper); 
                }
                else{
                	  
                    console.log('POMap ---'+JSON.stringify(component.get("v.poMap")));
                    console.log('CircuitMap ---'+JSON.stringify(component.get("v.circuitMap")));
                    console.log('ASRMap ---'+JSON.stringify(component.get("v.asrMap")));
                    console.log('validWoTaskMap ---'+JSON.stringify(component.get("v.validWoTaskMap")));
                    console.log('before calling validate location spinner:'+component.get("v.spinner"));
                    helper.validateLocation(component, event, helper);
                      
                }
            }
            else{
            	console.log('Error in validatekWOTask method :OMPH2_CSVFileUpload');  
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);   
      //  helper.showToast('Validation','Validating Work order task.','info');
    },
    createRecords: function(component, event, helper){ 
        //component.set("v.spinner",true);
        console.log('create records spinner:'+component.get("v.spinner"));
        //component.set("v.loadingText",true);
        console.log(component.get("v.loadingText"));
        var action = component.get("c.createRecords");
        action.setParams({ "poMap" : component.get("v.poMap"),
                           "circuitMap" : component.get("v.circuitMap"),
                           "asrMap" : component.get("v.asrMap"),
                           "validWoTaskMap" : component.get("v.validWoTaskMap"),
                           "validLocationMap":component.get("v.validLocationMap"),
                           "woTaskStrtIntegrationMap":component.get("v.woTaskStrtIntegrationMap")});
        action.setCallback(this, function(response) {
           
           // console.log(component.get("v.loadingText"));
            var state = response.getState();
            var result =  response.getReturnValue();
            console.log('result:'+result);
            if(state === "SUCCESS" && result === "success") {
                 component.set("v.loadingText",false);
                var msg = 'Records created successfully';
                helper.showToast('Success',msg,'Success');
                component.set("v.spinner",false);
                helper.clearDataAfterError(component, event, helper); 
                
            }
            else if(state === "SUCCESS" && result !== "success"){
                component.set("v.spinner",false);
                var msg = result;
                alert(msg);
                helper.clearDataAfterError(component, event, helper);
            }
           
            else{
            	 console.log('Error in createRecords method :OMPH2_CSVFileUpload'); 
                 component.set("v.spinner",false);
            }
        });
        component.set("v.spinner",false);
        $A.enqueueAction(action);
       // helper.showToast('Create','Creating records operation started.','info');
    }
})
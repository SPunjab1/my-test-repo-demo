({
    getFileDetails: function(component, event, helper) {
        helper.getFileDetailsJs(component, event, helper);
    },
    displayFile: function(component, event, helper) {
        helper.displayFileJS(component, event, helper);
    },
    uploadFile: function(component, event, helper) {
        helper.uploadFileJS(component, event, helper);
    },
    closeDuplicateModel: function(component, event, helper) {
        component.set("v.isOpenDuplicate", false);
        helper.clearDataAfterError(component, event, helper);
    },
    closeDatabaseDuplicateModel :function(component, event, helper) {
        component.set("v.isOpenDuplicateInDatabase", false);
        helper.clearDataAfterError(component, event, helper);
    }    
})